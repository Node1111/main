use ahash::HashSet;
use index_debcargo::Index;
use quick_error::quick_error;
use smartstring::alias::String as SmolStr;
use std::path::Path;
use std::sync::RwLock;
use std::sync::RwLockWriteGuard;

quick_error! {
    #[derive(Debug)]
    pub enum Error {
        Git(err: index_debcargo::Error) {
            display("{}", err)
            from()
            source(err)
        }
        Other(s: &'static str) {
            display("{}", s)
            from()
        }
    }
}

pub struct DebcargoList {
    index: Index,
    list: RwLock<HashSet<SmolStr>>,
}

// *Hopefully* `git2::Repository` is safe to use?
unsafe impl Sync for DebcargoList {}
unsafe impl Send for DebcargoList {}

impl DebcargoList {
    pub fn new(cache_dir: &Path) -> Result<Self, Error> {
        let index = Index::new(cache_dir)?;

        let list = Self { index, list: RwLock::default() };
        Ok(list)
    }

    pub fn update(&self) -> Result<(), Error> {
        self.index.update(None)?;
        let mut locked_list = self.list.write().unwrap();
        locked_list.clear();
        self.fill_list(locked_list)?;
        Ok(())
    }

    fn fill_list(&self, mut locked_list: RwLockWriteGuard<HashSet<SmolStr>>) -> Result<(), Error> {
        if !locked_list.is_empty() {
            return Ok(());
        }

        let data = self.index.list_all()?;
        if data.is_empty() {
            return Err("unexpectedly empty".into());
        }

        locked_list.extend(data.into_iter().map(|e| SmolStr::from(e.name)));
        Ok(())
    }

    pub fn has(&self, crate_name: &str) -> Result<bool, Error> {
        let mut l = self.list.read().unwrap();
        if l.is_empty() {
            drop(l);
            let locked_list = self.list.write().unwrap();
            self.fill_list(locked_list)?;
            l = self.list.read().unwrap();
        }
        Ok(l.contains(crate_name))
    }
}

#[test]
fn is_send() {
    fn check<T: Send + Sync>() {}
    check::<DebcargoList>();
}

#[test]
fn debcargo_test() {
    std::fs::create_dir_all("/tmp/debcargo-conf-test").unwrap();
    let l = DebcargoList::new(Path::new("/tmp/debcargo-conf-test")).unwrap();
    assert!(l.has("rand").unwrap());
    assert!(!l.has("").unwrap());
    assert!(!l.has("/").unwrap());
    assert!(!l.has("..").unwrap());
    assert!(!l.has("borkedcratespam").unwrap());
    assert!(l.has("rgb").unwrap());
}
