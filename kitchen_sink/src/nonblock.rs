use std::future::Future;
use std::pin::Pin;
use std::task::Poll;

use crate::stopped;

pub struct NonBlock<S, F> {
    future: F,
    label: S,
}

impl<S: AsRef<str>, F: Future> NonBlock<S, F> {
    #[inline]
    pub fn new(label: S, future: F) -> Self {
        Self { future, label }
    }
}

impl<S: AsRef<str>, F: Future> Future for NonBlock<S, F> {
    type Output = F::Output;
    fn poll(mut self: Pin<&mut Self>, ctx: &mut std::task::Context<'_>) -> Poll<Self::Output> {
        let start = std::time::Instant::now();
        let res = blocking::watch(format!("longpoll! {}", self.label.as_ref()), || {
            unsafe { self.as_mut().map_unchecked_mut(|s| &mut s.future) }.poll(ctx)
        });
        let elapsed = start.elapsed();
        if elapsed.as_secs() >= 1 {
            let label = self.label.as_ref();
            eprintln!("blocking poll: {label} took {}ms", elapsed.as_millis() as u32);
        }
        match res {
            ready @ Poll::Ready(_) => ready,
            pending => {
                if stopped() {
                    let label = self.label.as_ref();
                    panic!("Aborting {label}; the process is being stopped");
                }
                pending
            },
        }
    }
}
