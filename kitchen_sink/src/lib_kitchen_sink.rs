#[macro_use]
extern crate log;

mod ablocklist;
mod yearly;

use rich_crate::extract_doc_comments;
use feat_extractor::is_bad_dependency;
use feat_extractor::is_deprecated_requirement;
use anyhow::bail;
use anyhow::Context;
use core::sync::atomic::Ordering::AcqRel;
use std::collections::TryReserveError;
use crate_db::builddb::is_compat_relevant_target;
use crate_db::builddb::RustcMinorVersion;
use crate_git_checkout::FoundManifest;
use event_log::EventLog;
use futures::TryFutureExt;
use itertools::Itertools;
use manifest::IdentifiedBinFile;
use parking_lot::RwLockWriteGuard;
use std::collections::hash_map::Entry;
use std::panic::RefUnwindSafe;
use std::panic::UnwindSafe;
use std::sync::atomic::AtomicU32;
use blocking::block_in_place;
use tokio::time::Instant;
use udedokei::synscrape::extract_idents_enabled_by_features;
use udedokei::synscrape::TextFromSourceCode;
use util::CowAscii;
use util::pick_top_n_unstable_by;
use util::PushInCapacity;
use util::sort_top_n_unstable_by;
pub use crate_db::CrateOwnerStat;
pub use crate::ablocklist::*;
pub use crate::yearly::*;
pub use deps_index::*;
use futures::future::BoxFuture;
use futures::FutureExt;
use tokio::task::spawn_blocking;
pub mod filter;

mod ctrlcbreak;
pub use crate::ctrlcbreak::*;
mod nonblock;
pub use crate::nonblock::*;
mod rendctx;
pub use crate::rendctx::*;

pub use categories::Synonyms;
pub use crate_db::builddb::Compat;
pub use crate_db::builddb::CompatByCrateVersion;
pub use crate_db::builddb::CompatRanges;
pub use crate_db::CrateOwnerRow;
pub use crates_io_client::CrateDepKind;
pub use crates_io_client::CrateDependency;
use crates_io_client::CrateMetaFile;
pub use crates_io_client::CrateMetaVersion;
pub use crates_io_client::CrateOwner;
pub use crates_io_client::OwnerKind;
pub use creviews::Level;
pub use creviews::Rating;
pub use creviews::Review;
pub use creviews::vet;
pub use creviews::security::Advisory;
pub use creviews::security::Severity;
pub use github_info::Org;
pub use github_info::User;
pub use github_info::UserOrg;
pub use github_info::UserType;
pub use github_info::SocialAccount;
pub use rich_crate::DependerChangesMonthly;
pub use rich_crate::TractionStats;
pub use rich_crate::Edition;
pub use rich_crate::Derived;
pub use rich_crate::MaintenanceStatus;
use rich_crate::ManifestExt;
pub use rich_crate::Markup;
pub use rich_crate::Origin;
pub use rich_crate::RichCrate;
pub use rich_crate::RichCrateVersion;
pub use rich_crate::RichDep;
pub use rich_crate::{Cfg, Target};
pub use semver::Version as SemVer;

use tarball::CrateFilesSummary;
use cargo_toml::Manifest;
use cargo_toml::Package;
use categories::Category;
use chrono::prelude::*;
use chrono::DateTime;
use crate_db::{builddb::BuildDb, CrateDb, CrateVersionData, RepoChange};
use creviews::Creviews;
use double_checked_cell_async::DoubleCheckedCell;
use futures::future::join_all;
use futures::stream::StreamExt;
use futures::Future;
use github_info::GitCommitAuthor;
use github_info::GitHubRepo;
use github_info::MinimalUser;
use once_cell::sync::OnceCell;
use parking_lot::RwLock;
use rayon::prelude::*;
use repo_url::Repo;
use repo_url::SimpleRepo;
pub use rich_crate::Author;
pub use rich_crate::CrateVersion;
use rich_crate::CrateVersionSourceData;
use rich_crate::Readme;
pub use semver::VersionReq;
use simple_cache::SimpleCache;
use simple_cache::TempCache;
use std::cmp::Ordering;
use std::cmp::Reverse;
use std::collections::hash_map::Entry::{Occupied, Vacant};
use std::panic::catch_unwind;
use ahash::HashMap;
use ahash::HashSet;
use serde::{Serialize, Deserialize};
use std::env;
use std::path::{Path, PathBuf};
use std::pin::Pin;
use std::sync::Mutex;
use std::time::Duration;
use std::time::SystemTime;
use triomphe::Arc;
use util::SmolStr;
use ahash::HashMapExt;
use ahash::HashSetExt;
use util::crate_name_fuzzy_eq;

pub type ArcRichCrateVersion = Arc<RichCrateVersion>;

pub type CError = anyhow::Error;
pub type CResult<T> = Result<T, CError>;
pub type KResult<T> = Result<T, KitchenSinkErr>;
pub type Warnings = HashSet<Warning>;

#[derive(Debug, Clone, Serialize, thiserror::Error, Deserialize, Hash, Eq, PartialEq)]
pub enum Warning {
    #[error("`Cargo.toml` doesn't have `repository` property")]
    NoRepositoryProperty,
    #[error("`Cargo.toml` doesn't have `[package]` section")]
    NotAPackage,
    #[error("`Cargo.toml` doesn't have `readme` property")]
    NoReadmeProperty,
    #[error("`readme` property points to a file that hasn't been published")]
    NoReadmePackaged,
    #[error("Can't find README in repository: {}", _0)]
    NoReadmeInRepo(Box<str>),
    #[error("Readme has a problematic path: {}", _0)]
    EscapingReadmePath(Box<str>),
    #[error("Could not clone repository: {}", _0)]
    ErrorCloning(Box<str>),
    #[error("The crate is not in the repository")]
    NotFoundInRepo,
    #[error("{} URL is a broken link: {}", _0, _1)]
    BrokenLink(Box<str>, Box<str>),
    #[error("Bad category: {}", _0)]
    BadCategory(Box<str>),
    #[error("No categories specified")]
    NoCategories,
    #[error("No keywords specified")]
    NoKeywords,
    #[error("Edition {:?}, but MSRV {}", _0, _1)]
    EditionMSRV(Edition, u16),
    #[error("Bad MSRV: needs {}, but has {}", _0, _1)]
    BadMSRV(u16, u16),
    #[error("docs.rs did not build")]
    DocsRs,
    #[error("Dependency {} is no longer needed: {}", _0, _1)]
    ObsoleteDependency(Box<str>, Box<str>),
    #[error("Dependency {} v{} is outdated ({}%)", _0, _1, _2)]
    OutdatedDependency(Box<str>, Box<str>, u8),
    #[error("Dependency {} v{} is not recommended", _0, _1)]
    DeprecatedDependency(Box<str>, Box<str>),
    #[error("Dependency {} v{} is not used any more", _0, _1)]
    UnpopularDependency(Box<str>, Box<str>),
    #[error("Dependency {} has bad requirement {}", _0, _1)]
    BadRequirement(Box<str>, Box<str>),
    #[error("Dependency {} has exact requirement {}", _0, _1)]
    ExactRequirement(Box<str>, Box<str>),
    // bool = is breaking semver
    #[error("Dependency {} has imprecise requirement {}", _0, _1)]
    LaxRequirement(Box<str>, Box<str>, bool),
    #[error("Version {} does not parse: {}", _0, _1)]
    BadSemVer(Box<str>, Box<str>),
    #[error("The crate is classified as a cryptocurrency-related")]
    CryptocurrencyBS,
    #[error("The crate tarball is big: {}MB", _0 / 1000 / 1000)]
    Chonky(u64),
    #[error("A *-sys crates without links property")]
    SysNoLinks,
    #[error("Squatted name (identified by: {})", _0)]
    Reserved(Box<str>),
    #[error("License is not an SPDX expression")]
    LicenseSpdxSyntax,
    #[error("Reported security advisory: {}", _0)]
    Advisory(Box<str>, Box<str>),
    #[error("Crate tarball contains binaries ({})", _0)]
    ContainsBinaries(Box<str>),
    #[error("Crate exports implicit Cargo features that may be unintended")]
    ImplicitFeatures(Vec<SmolStr>),
    /// last arg is severity 1-n
    #[error("It's been {} days since the last {}release", _0, if *_1 {"stable "} else {"pre"})]
    StaleRelease(u32, bool, u8)
}

#[derive(Debug, Clone, thiserror::Error)]
pub enum KitchenSinkErr {
    #[error("git checkout: {}", _0)]
    GitCheckoutFailed(String),
    #[error("category count not found in crates db: {}", _0)]
    CategoryNotFound(String),
    #[error("category query failed")]
    CategoryQueryFailed,
    #[error("crate not found: {:?}", _0)]
    CrateNotFound(Origin),
    #[error("author not found: {}", _0)]
    AuthorNotFound(SmolStr),
    #[error("error getting GH data: {}", _0)]
    GitHub(String),
    #[error("crate {} not found in repo {}", _0, _1)]
    CrateNotFoundInRepo(Box<str>, Box<str>),
    #[error("crate is not a package: {:?}", _0)]
    NotAPackage(Origin),
    #[error("data not found, wanted {}", _0)]
    DataNotFound(String),
    #[error("tarball unarchiving error in {}", _0)]
    UnarchiverError(String, #[source] Arc<tarball::UnarchiverError>),
    #[error("crate has no versions")]
    NoVersions,
    #[error("cached data has different version than the index")]
    CacheExpired,
    #[error("Environment variable CRATES_DATA_DIR is not set.\nChoose a dir where it's OK to store lots of data, and export it like CRATES_DATA_DIR=/var/lib/crates.rs")]
    CratesDataDirEnvVarMissing,
    #[error("{} does not exist\nPlease get data files from https://lib.rs/data and put them in that directory, or set CRATES_DATA_DIR to their location.", _0)]
    CacheDbMissing(String),
    #[error("Error when parsing verison")]
    SemverParsingError,
    #[error("Db error {}", _0)]
    Db(#[from] #[source] Arc<crate_db::Error>),
    #[error("The server is being shut down")]
    Stopped,
    #[error("Deps not available (timeout?)")]
    DepsNotAvailable,
    #[error("Crate data timeout")]
    DataTimedOut,
    #[error("{} timed out after {}s", _0, _1)]
    TimedOut(Box<str>, u16),
    #[error("Crate derived cache failed")]
    DerivedDataMissing,
    #[error("Missing github login for crate owner")]
    OwnerWithoutLogin,
    #[error("Git index parsing failed: {}", _0)]
    GitIndexParse(String),
    #[error("Git index {:?}: {}", _0, _1)]
    GitIndexFile(PathBuf, String),
    #[error("Git crate '{:?}' can't be indexed, because it's not on the list", _0)]
    GitCrateNotAllowed(Origin),
    #[error("Deps err: {}", _0)]
    Deps(#[from] DepsErr),
    #[error("Bad rustc compat data")]
    BadRustcCompatData,
    #[error("bad cache: {}", _0)]
    BorkedCache(String),
    #[error("Event log error")]
    Event(#[from] #[source] Arc<event_log::Error>),
    #[error("RustSec: {}", _0)]
    RustSec(#[from] #[source] Arc<creviews::security::Error>),
    #[error("No disk space left")]
    NoDiskSpace,
    #[error("OOM")]
    OOM,
    #[error("Internal error: {}", _0)]
    Internal(std::sync::Arc<dyn std::error::Error + Send + Sync>),
}

impl From<crates_io_client::Error> for KitchenSinkErr {
    #[cold]
    fn from(e: crates_io_client::Error) -> Self {
        match e {
            crates_io_client::Error::NotInCache => Self::CacheExpired,
            other => Self::BorkedCache(other.to_string()),
        }
    }
}

impl From<TryReserveError> for KitchenSinkErr {
    #[cold]
    fn from(_: TryReserveError) -> Self {
        Self::OOM
    }
}

#[derive(Debug, Clone)]
pub struct DownloadWeek {
    pub date: NaiveDate,
    pub total: usize,
    pub downloads: HashMap<Option<usize>, usize>,
}

/// bucket (like age, number of releases) -> (number of crates in the bucket, sample of those crate names)
pub type StatsHistogram = HashMap<u32, (u32, Vec<String>)>;

static GLOBAL_INSTANCES: AtomicU32 = AtomicU32::new(0);

pub struct CachedCategory {
    origins: Arc<Vec<Origin>>,
    expires: std::time::Instant,
}

/// This is a collection of various data sources. It mostly acts as a starting point and a factory for other objects.
pub struct KitchenSink {
    index: Index,
    crates_io: crates_io_client::CratesIoClient,
    docs_rs: docs_rs_client::DocsRsClient,
    url_check_cache: TempCache<(bool, u8)>,
    readme_check_cache: TempCache<()>,
    repo_checkout_cache: TempCache<()>,
    canonical_http_of_crate_at_version_cache: TempCache<String>,
    pub crate_db: CrateDb,
    derived_storage: SimpleCache,
    pub extra_derived_storage: SimpleCache,
    user_db: user_db::UserDb,
    pub gh: github_info::GitHub,
    loaded_rich_crate_version_cache: RwLock<HashMap<Origin, ArcRichCrateVersion>>,
    category_crate_counts: DoubleCheckedCell<Option<HashMap<String, (u32, f64)>>>,
    top_crates_cached: Mutex<HashMap<String, Arc<DoubleCheckedCell<CachedCategory>>>>,
    git_checkout_path: PathBuf,
    yearly: AllDownloads,
    category_and_spam_overrides: HashMap<SmolStr, Vec<SmolStr>>,
    crates_io_owners_cache: TempCache<Vec<CrateOwner>>,
    crates_io_ids_to_github_mapping: TempCache<u32, u32>,
    depender_changes: TempCache<Vec<DependerChanges>>,
    stats_histograms: TempCache<StatsHistogram>,
    throttle: tokio::sync::Semaphore,
    auto_indexing_throttle: tokio::sync::Semaphore,
    crev: Arc<Creviews>,
    vet: DoubleCheckedCell<creviews::vet::AuditSources>,
    rustsec: Arc<RwLock<Option<creviews::security::RustSec>>>,
    crate_rustc_compat_cache: RwLock<HashMap<Origin, CompatByCrateVersion>>,
    crate_rustc_compat_db: OnceCell<Arc<BuildDb>>,
    data_path: PathBuf,
    /// login -> reason
    ablocklist: ABlockList,
    event_log: EventLog<SharedEvent>,
    is_deprecated_crate: TempCache<String>,
    synonyms: Arc<Synonyms>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum SharedEvent {
    // Origin serialized
    CrateIndexed(String),
    // Origin serialized
    CrateNeedsReindexing(String),
    /// Newer crates.io release found
    CrateUpdated(String),
    DailyStatsUpdated,
}

/// Dunno. The whole unwind safety concept is meh.
impl UnwindSafe for KitchenSink {}
impl RefUnwindSafe for KitchenSink {}

impl KitchenSink {
    /// Use env vars to find data directory and config
    pub async fn new_default() -> CResult<Self> {
        let github_token = match env::var("GITHUB_TOKEN") {
            Ok(t) => t,
            Err(_) => {
                warn!("warning: Environment variable GITHUB_TOKEN is not set.\nGet token from https://github.com/settings/tokens and export GITHUB_TOKEN=…\nWithout it some requests will fail and new crates won't be analyzed properly.");
                String::new()
            },
        };
        let data_path = Self::data_path().context("can't get data path")?;
        Self::new(&data_path, &github_token).await
    }

    #[inline(never)]
    pub async fn new(data_path: &Path, github_token: &str) -> CResult<Self> {
        let _ = env_logger::Builder::from_default_env()
            .filter(Some("html5ever"), log::LevelFilter::Error)
            .filter(Some("html5ever::tree_builder"), log::LevelFilter::Error)
            .filter(Some("html5ever::tokenizer::char_ref"), log::LevelFilter::Error)
            .filter(Some("tantivy"), log::LevelFilter::Error)
            .try_init();

        if let Ok(size) = fs4::available_space(data_path) {
            let free_mib = size / (1024 * 1024);
            info!("free disk space: {}MiB", free_mib);
            if free_mib < 500 {
                error!("Refusing to start due to lack of free disk space on {}", data_path.display());
                return Err(KitchenSinkErr::NoDiskSpace.into())
            }
        }

        let ((crates_io, gh), crev) = tokio::task::spawn_blocking({
            let data_path = data_path.to_owned();
            let github_token = github_token.to_owned();
            let ghdb = data_path.join("github.db");
            move || {
                rayon::join(|| rayon::join(
                         || crates_io_client::CratesIoClient::new(&data_path),
                    move || github_info::GitHub::new(&ghdb, &github_token)),
                Creviews::new)
            }
        }).await?;

        let crev = Arc::new(crev.context("crev")?);
        std::thread::spawn({
            let crev = crev.clone();
            move || {
                let _ = crev.setup().map_err(|e| error!("crev setup failed: {e}"));
            }
        });

        let synonyms = Arc::new(Synonyms::new(data_path)?);

        let prev = GLOBAL_INSTANCES.fetch_add(1, AcqRel);
        if prev > 0 {
            warn!("There are already {prev} instances of KitchenSink");
        }

        block_in_place("kinit", move || Ok(Self {
            crev,
            vet: DoubleCheckedCell::new(),
            rustsec: Default::default(),
            crates_io: crates_io.context("crates_io")?,
            index: Index::new(data_path)?,
            url_check_cache: TempCache::new(data_path.join("url_check2.db"), Duration::from_secs(3600*24*11)).context("urlcheck")?,
            readme_check_cache: TempCache::new(data_path.join("readme_check.db"), Duration::from_secs(3600*24*31*6)).context("readmecheck")?,
            repo_checkout_cache: TempCache::new(data_path.join("repo_checkout.db"), Duration::from_secs(3600*24*14)).context("repo_checkout.db")?,
            canonical_http_of_crate_at_version_cache: TempCache::new(data_path.join("canonical_http_url_at.db"), Duration::from_secs(3600*24*365)).context("readmecheck")?,
            docs_rs: docs_rs_client::DocsRsClient::new(data_path.join("docsrs2.db")).context("docs")?,
            crate_db: CrateDb::new_with_synonyms(&Self::assert_exists(data_path.join("crate_data.db"))?, Arc::clone(&synonyms)).context("db")?,
            synonyms,
            derived_storage: SimpleCache::new(data_path.join("derived.db"), true).context("derived-db")?,
            extra_derived_storage: SimpleCache::new(data_path.join("syn_src_text.db"), true).context("syn-db")?,
            user_db: user_db::UserDb::new(Self::assert_exists(data_path.join("users.db"))?).context("udb")?,
            gh: gh.context("gh")?,
            loaded_rich_crate_version_cache: RwLock::new(HashMap::default()),
            git_checkout_path: data_path.join("git"),
            category_crate_counts: DoubleCheckedCell::new(),
            top_crates_cached: Mutex::new(HashMap::default()),
            yearly: AllDownloads::new(data_path),
            category_and_spam_overrides: Self::load_category_and_spam_overrides(&data_path.join("category_overrides.txt")).context("cat")?,
            ablocklist: ABlockList::new(&data_path.join("ablocklist.csv")).context("abl")?,
            crates_io_owners_cache: TempCache::new(data_path.join("cio-owners.tmp"), Duration::from_secs(3600*24*14)).context("tmp1")?,
            crates_io_ids_to_github_mapping: TempCache::new(data_path.join("cio-ids.tmp"), Duration::ZERO).context("tmp1")?,
            depender_changes: TempCache::new(data_path.join("deps-changes3.tmp"), Duration::ZERO).context("tmp2")?,
            stats_histograms: TempCache::new(data_path.join("stats-histograms.tmp"), Duration::from_secs(3600*24*31*3)).context("tmp3")?,
            is_deprecated_crate: TempCache::new(data_path.join("deprecated2.tmp"), Duration::ZERO).context("tmpdp")?,
            throttle: tokio::sync::Semaphore::new(40),
            auto_indexing_throttle: tokio::sync::Semaphore::new(4),
            crate_rustc_compat_cache: RwLock::default(),
            crate_rustc_compat_db: OnceCell::new(),
            event_log: EventLog::new(data_path.join("event_log.db")).context("events")?,
            data_path: data_path.into(),
        }))
    }

    fn assert_exists(path: PathBuf) -> Result<PathBuf, KitchenSinkErr> {
        if !path.exists() {
            Err(KitchenSinkErr::CacheDbMissing(path.display().to_string()))
        } else {
            Ok(path)
        }
    }

    pub fn event_log(&self) -> &EventLog<SharedEvent> {
        &self.event_log
    }

    pub fn data_path() -> Result<PathBuf, KitchenSinkErr> {
        match env::var("CRATES_DATA_DIR") {
            Ok(d) => {
                if !Path::new(&d).join("crate_data.db").exists() {
                    return Err(KitchenSinkErr::CacheDbMissing(d));
                }
                Ok(d.into())
            },
            Err(_) => {
                for path in &["../data", "./data", "/var/lib/crates.rs/data", "/www/crates.rs/data"] {
                    let path = Path::new(path);
                    if path.exists() && path.join("crate_data.db").exists() {
                        return Ok(path.to_owned());
                    }
                }
                Err(KitchenSinkErr::CratesDataDirEnvVarMissing)
            },
        }
    }

    pub fn main_data_dir(&self) -> &Path {
        &self.data_path
    }

    fn load_category_and_spam_overrides(path: &Path) -> CResult<HashMap<SmolStr, Vec<SmolStr>>> {
        let p = std::fs::read_to_string(path)?;
        let mut out = HashMap::<SmolStr, Vec<SmolStr>>::new();
        let mut had_dupes = false;
        for line in p.lines() {
            let mut parts = line.split('#').next().unwrap().splitn(2, ':');
            let crate_name = parts.next().unwrap().trim();
            if crate_name.is_empty() {
                continue;
            }
            let mut categories: Vec<_> = parts.next().expect("overrides broken").split(',')
                .map(|s| s.trim()).filter(|s| !s.is_empty()).map(From::from).collect();
            if categories.is_empty() {
                continue;
            }
            categories.iter().for_each(|k| debug_assert!(k == "hidden" || k == "spam" || categories::CATEGORIES.from_slug(k).1, "'{k}' is invalid for override '{crate_name}'"));
            match out.entry(crate_name.into()) {
                Occupied(mut e) => {
                    had_dupes = true;
                    e.get_mut().append(&mut categories);
                },
                Vacant(e) => {
                    e.insert(categories);
                },
            }
        }
        if had_dupes && cfg!(debug_assertions) {
            let mut s = String::new();
            for (k, v) in &mut out {
                v.dedup();
                s.push_str(k); s.push_str(": ");
                s.push_str(&v.join(", ")); s.push('\n');
            }
            std::fs::write(path, s.as_bytes()).unwrap();
        }
        Ok(out)
    }

    pub fn crates_io_login_on_blocklist(&self, login: &str) -> Option<&ABlockReason> {
        self.ablocklist.get(login)
    }

    #[inline(never)]
    pub async fn crate_blocklist_actions<'a>(&'a self, k: &RichCrate) -> (bool, Vec<ABlockAction<'a>>) {
        let owners = self.crate_owners(k.origin(), CrateOwners::All).await.map_err(|e| {
            warn!("can't check owners of {:?}: {e}", k.origin());
        }).unwrap_or_default();
        self.ablocklist.actions_for_owners(&owners)
    }

    /// Don't make requests to crates.io
    pub fn cache_only(&mut self, no_net: bool) -> &mut Self {
        self.crates_io.cache_only(no_net);
        self
    }

    pub fn total_year_downloads(&self, year: u16) -> KResult<[u64; 366]> {
        Ok(self.yearly.total_year_downloads(year)?)
    }

    #[inline]
    fn summed_year_downloads(&self, crate_name: &str, curr_year: u16) -> KResult<[u32; 366]> {
        let curr_year_data = self.yearly.get_crate_year(crate_name, curr_year)?.unwrap_or_default();
        let mut summed_days = [0; 366];
        for (_, days) in curr_year_data {
            for (sd, vd) in summed_days.iter_mut().zip(days.0.iter()) {
                *sd += *vd;
            }
        }
        Ok(summed_days)
    }

    // Get top n crates-io crates with most sharply increasing downloads
    #[inline(never)]
    pub async fn trending_crates(self: std::sync::Arc<Self>, top_n: usize, rend: &RendCtx) -> KResult<Vec<(Origin, f64)>> {
        let crates = self.clone();
        let deadline = Instant::now() + Duration::from_secs(40);
        let mut top = blocking::awatch("trb", spawn_blocking(move || {
            static CPU_HEAVY: Mutex<()> = Mutex::new(());
            let _c = CPU_HEAVY.lock().unwrap();
            if Instant::now() > deadline {
                panic!("trb took too long");
            }
            running()?;
            crates.trending_crates_raw(top_n)
        })).await.unwrap()?;

        let stats = self.index()?.deps_stats()?;
        // apply some rank weight to downgrade spam, and pull main crates before their -sys or -derive
        for (origin, score) in &mut top {
            let (rank, traction) = futures::join!(
                self.crate_db.crate_rank(origin),
                self.traction_stats(origin, stats),
            );
            *score *= 0.2 + rank.unwrap_or(0.);
            if let Ok(Some(t)) = traction {
                // it's easy to double if you had only 1 user
                let growth_smoothed = 100. + t.growth.min(2.) * f64::from((1 + t.active_users).min(50));
                *score *= growth_smoothed * t.growth.min(1.05).powf(3.) * t.external_usage * t.former_glory;
            }
        }
        top.sort_unstable_by(|a, b| b.1.total_cmp(&a.1));

        watch("tr-dupe", self.knock_duplicates(&mut top, rend)).await;
        top.truncate(top_n);
        Ok(top)
    }


    /// When opening lib.rs/foo is foo a useful crate `true`, or squatted garbage `false`?
    pub async fn is_crate_good_enough_for_short_url(&self, origin: &Origin) -> CResult<bool> {
        if self.crate_db.crate_rank(origin).await? > 0.23 {
            return Ok(true);
        }
        Ok(!self.is_crate_obsolete(&self.rich_crate_async(origin).await?).await)
    }

    /// Does not check for deprecation directly, just ranking and age. Used for visual tweak of crate page.
    pub async fn is_crate_obsolete(&self, k: &RichCrate) -> bool {
        let Ok(score) = self.crate_db.crate_rank(k.origin()).await else { return false; };
        if score > 0.5 {
            return false;
        }
        if score < 0.05 && score != 0. { // 0 = new unindexed crate
            return true;
        }
        let age_days = Utc::now().signed_duration_since(k.most_recent_release()).num_days();
        if age_days < 365*2 {
            return false;
        }
        if age_days > 365*6 {
            return true;
        }
        age_days > 365*4 && score < 0.25
    }

    pub async fn crate_ranking_for_builder(&self, origin: &Origin) -> CResult<f64> {
        Ok(self.crate_db.crate_rank(origin).await?)
    }

    // actually gives 2*top_n…
    fn trending_crates_raw(&self, top_n: usize) -> KResult<Vec<(Origin, f64)>> {
        let mut now = Utc::now().date_naive();
        let mut day_of_year = now.ordinal0() as usize;
        if day_of_year < 7 {
            now -= chrono::Duration::days(8);
            day_of_year = now.ordinal0() as usize;
        }
        let curr_year = now.year() as u16;
        let shortlen = (day_of_year / 2).min(10);
        let longerlen = (day_of_year / 2).min(3 * 7);

        let missing_data_factor = longerlen as f32 / (3 * 7) as f32;
        let missing_data_factor = missing_data_factor.powi(2);

        fn slice_max(slice: &[u32]) -> u32 {
            slice.iter().copied().max().unwrap_or(0)
        }

        fn average_nonzero(slice: &[u32], min_days: u32, is_previous: bool) -> f32 {
            let mut sum = 0u32;
            let mut n = 0u32;
            let mut max = 0;
            for val in slice.iter().copied().filter(|&n| n > 0) {
                if is_previous {
                    // past peaks set a new baseline, artificially inflating last week's data
                    // making it less likely to be trending if it already had high/noisy traffic in the past
                    max = val.max(max);
                    sum += max;
                } else {
                    sum += val
                }
                n += 1;
            }
            sum as f32 / (n.max(min_days) as f32) // too few days are too noisy, and div/0
        }

        let index = self.index()?;
        let all_crates = blocking::block_in_place("all_c", || index.all_crates())?;
        let mut ratios = Vec::new();
        ratios.try_reserve(all_crates.len())?;

        let _ = self.summed_year_downloads("libc", curr_year); // prefetch outside of par_iter reduce locking there

        let deadline = Instant::now() + Duration::from_secs(10);
        ratios.par_extend(all_crates.par_iter().filter_map(|origin| {
            match &origin {
                Origin::CratesIo(crate_name) => {
                    if Instant::now() > deadline {
                        return None;
                    }
                    let d = self.summed_year_downloads(crate_name, curr_year).ok()?;
                    let prev_week_avg = average_nonzero(&d[day_of_year-shortlen*2 .. day_of_year-shortlen], 7, true);
                    if prev_week_avg < 70. * missing_data_factor { // it's too easy to trend from zero downloads!
                        return None;
                    }

                    let this_week = &d[day_of_year-shortlen .. day_of_year];
                    let this_week_avg = average_nonzero(this_week, 8, false);

                    if prev_week_avg * missing_data_factor >= this_week_avg {
                        return None;
                    }

                    let this_week_max = slice_max(this_week);
                    let prev_max = slice_max(&d[.. day_of_year-longerlen*2]);
                    let not_really_trending = if prev_max > this_week_max { 0.3 } else { 1. };

                    let prev_4w_avg = average_nonzero(&d[day_of_year-longerlen*2 .. day_of_year-longerlen], 7, true).max(average_nonzero(&d[.. day_of_year-longerlen*2], 7, true));
                    let this_4w_avg = average_nonzero(&d[day_of_year-longerlen .. day_of_year], 14, false);
                    if prev_4w_avg * missing_data_factor >= this_4w_avg || prev_4w_avg * missing_data_factor >= prev_week_avg || prev_4w_avg * missing_data_factor >= this_week_avg {
                        return None;
                    }


                    let ratio1 = not_really_trending * (800. + this_week_avg) / (900. + prev_week_avg) * prev_week_avg.sqrt().min(10.);
                    // 0.8, because it's less interesting
                    let ratio4 = not_really_trending * 0.8 * (700. + this_4w_avg) / (600. + prev_4w_avg) * prev_4w_avg.sqrt().min(9.);

                    // combine short term and long term trends
                    Some((origin, ratio1, ratio4))
                },
                _ => None,
            }
        }));


        if Instant::now() > deadline {
            warn!("trending cut short due to deadline");
        }

        if ratios.is_empty() {
            warn!("no trending crates");
            return Ok(Vec::new());
        }
        sort_top_n_unstable_by(&mut ratios, top_n, |a, b| b.1.partial_cmp(&a.1).unwrap_or(Ordering::Equal));
        let split_pos = top_n.min(ratios.len());
        let (top_ratios, rest_ratios) = ratios.split_at_mut(split_pos);
        let mut top: Vec<_> = top_ratios.iter().map(|&(o, s, _)| (o.clone(), f64::from(s))).collect();
        sort_top_n_unstable_by(rest_ratios, top_n, |a, b| b.2.partial_cmp(&a.2).unwrap_or(Ordering::Equal));
        top.try_reserve(rest_ratios.len().min(top_n))?;
        top.extend(rest_ratios.iter().take(top_n).map(|&(o, _, s)| (o.clone(), f64::from(s))));

        let crates_present: HashSet<_> = top.iter().filter_map(|(o, _)| match o {
            Origin::CratesIo(name) => Some(name.clone()),
            _ => None,
        }).collect();
        let stats = index.deps_stats()?;
        top.retain_mut(|(o, score)| {
            if let Origin::CratesIo(name) = o {
                if let Some(s) = stats.counts.get(&**name) {
                    // if it's a dependency of another top crate, its not trending, it's riding that crate
                    if s.rev_dep_names_default.iter().any(|parent| crates_present.contains(parent)) {
                        return false;
                    }

                    // it should be trending users, not just download hits
                    if s.counters.direct.all() > 10 {
                        *score *= 1.1;
                    }
                    if s.counters.direct.all() > 100 {
                        *score *= 1.1;
                    }
                }
            }
            true
        });
        Ok(top)
    }

    // Monthly downloads, sampled from last few days or weeks
    #[inline(never)]
    pub async fn recent_downloads_by_version(&self, origin: &Origin) -> KResult<HashMap<MiniVer, u32>> {
        let now = Utc::now().date_naive();
        let curr_year = now.year() as u16;
        let curr_year_data = match origin {
            Origin::CratesIo(name) => self.yearly.get_crate_year(name, curr_year)?.unwrap_or_default(),
            _ => return Ok(HashMap::new()),
        };

        let mut out = HashMap::new();
        let mut total = 0;
        let mut days = 0;
        let mut end_day = now.ordinal0() as usize; // we'll have garbage data in january…
        loop {
            let start_day = end_day.saturating_sub(4);
            days += end_day - start_day;

            for (ver, dl) in &curr_year_data {
                let cnt = out.entry(ver).or_insert(0);
                for d in dl.0[start_day..end_day].iter().copied() {
                    *cnt += d;
                    total += d;
                }
            }
            if start_day == 0 || total > 10000 || days >= 30 {
                break;
            }
            end_day = start_day;
        }

        // normalize data sample to be proportional to montly downloads
        let actual_downloads_per_month = self.downloads_per_month(origin).await?.unwrap_or(total as usize * 30 / days.max(3)); // avoid div/0 and extremes from incomplete data
        Ok(out.into_iter().map(|(k,v)|
            (k.clone(), (v as usize * actual_downloads_per_month / total.max(1) as usize) as u32)
        ).collect())
    }

    /// Gets cratesio download data, but not from the API, but from our local copy
    #[inline(never)]
    pub fn weekly_downloads(&self, k: &RichCrate, num_weeks: u16) -> CResult<Vec<DownloadWeek>> {
        let mut res = Vec::with_capacity(num_weeks.into());
        let mut now = Utc::now().date_naive();

        let mut curr_year = now.year() as u16;
        let mut summed_days = self.summed_year_downloads(k.name(), curr_year)?;

        let day_of_year = now.ordinal0();
        let missing_data_days = summed_days[0..day_of_year as usize].iter().copied().rev().take_while(|&s| s == 0).count().min(7);

        if missing_data_days > 0 {
            now -= chrono::Duration::days(missing_data_days as _);
        }

        for i in (1..=num_weeks).rev() {
            let date = now - chrono::Duration::weeks(i.into());
            let mut total = 0;
            let mut any_set = false;

            for d in 0..7 {
                let this_date = date + chrono::Duration::days(d);
                let year = this_date.year() as u16;
                if year != curr_year {
                    curr_year = year;
                    summed_days = self.summed_year_downloads(k.name(), curr_year)?;
                }
                let day_of_year = this_date.ordinal0() as usize;
                if summed_days[day_of_year] > 0 {
                    any_set = true;
                }
                total += summed_days[day_of_year] as usize;
            }
            if any_set {
                res.push(DownloadWeek {
                    date,
                    total,
                    downloads: HashMap::new(), // format of this is stupid, as it requires crates.io's version IDs
                });
            }
        }
        Ok(res)
    }

    #[inline(never)]
    #[allow(clippy::await_holding_lock)]
    pub async fn crates_to_reindex(&self, limit: u32) -> CResult<HashSet<Origin>> {
        let min_timestamp = self.crate_db.latest_crate_update_timestamp().await?.unwrap_or(0);
        tokio::task::yield_now().await;
        let index = self.index()?;
        let mut crates = {
            let crates_io_crates = index.crates_io_crates()?;
            futures::stream::iter(crates_io_crates.keys())
                .map(move |name| async move {
                    let origin = Origin::from_crates_io_name(name);
                    match self.rich_crate_async(&origin).await {
                        Ok(k) => {
                            let latest = k.versions().iter().map(|v| &v.created_at).max();
                            if latest.map_or(true, |created| created.timestamp() >= i64::from(min_timestamp)) {
                                Some(origin)
                            } else {
                                None
                            }
                        },
                        Err(e) => {
                            error!("to reindex {name}: {e}");
                            None
                        }
                    }
                })
                .buffer_unordered(8)
                .filter_map(|x| async {x})
                .take(limit as usize)
                .collect::<HashSet<_>>().await
        };
        let rest = self.crate_db.crates_to_reindex(limit).await?;
        crates.try_reserve(rest.len())?;
        crates.extend(rest);
        Ok(crates)
    }

    pub fn crate_exists(&self, origin: &Origin) -> bool {
        self.index().unwrap().crate_exists(origin)
    }

    /// Wrapper object for metadata common for all versions of a crate
    pub async fn rich_crate_async(&self, origin: &Origin) -> CResult<RichCrate> {
        running()?;
        match origin {
            Origin::CratesIo(name) => {
                let meta = self.crates_io_meta(name).await?;
                let versions = meta.versions().map(|c| Ok(CrateVersion {
                    num: c.num,
                    updated_at: DateTime::parse_from_rfc3339(&c.updated_at)?.with_timezone(&Utc),
                    created_at: DateTime::parse_from_rfc3339(&c.created_at)?.with_timezone(&Utc),
                    yanked: c.yanked,
                })).collect::<Result<_,chrono::ParseError>>()?;
                Ok(RichCrate::new(origin.clone(), meta.krate.name, versions))
            },
            Origin::GitHub(r) => {
                watch("repocrate", self.rich_crate_gh(origin, &r.repo, &r.package)).await
            },
            Origin::GitLab(r) => {
                watch("repocrate2", self.rich_crate_gitlab(origin, &r.repo, &r.package)).await
            },
        }
    }

    async fn rich_crate_gh(&self, origin: &Origin, repo: &SimpleRepo, package: &str) -> CResult<RichCrate> {
        let host = Repo::GitHub(repo.clone());
        let cachebust = self.cachebust_string_for_repo(&host).await.context("ghrepo")?;
        let versions = self.get_repo_versions(origin, &host, &cachebust).await?;
        Ok(RichCrate::new(origin.clone(), format!("github/{}/{package}", repo.owner).into(), versions))
    }

    async fn rich_crate_gitlab(&self, origin: &Origin, repo: &SimpleRepo, package: &str) -> CResult<RichCrate> {
        let host = Repo::GitLab(repo.clone());
        let cachebust = self.cachebust_string_for_repo(&host).await.context("ghrepo")?;
        let versions = self.get_repo_versions(origin, &host, &cachebust).await?;
        Ok(RichCrate::new(origin.clone(), format!("gitlab/{}/{package}", repo.owner).into(), versions))
    }

    #[inline(never)]
    async fn get_repo_versions(&self, origin: &Origin, repo: &Repo, cachebust: &str) -> CResult<Vec<CrateVersion>> {
        let package = match origin {
            Origin::GitLab(r) => &r.package,
            Origin::GitHub(r) => {
                let releases = self.gh.releases(&r.repo, cachebust).await?.ok_or_else(|| KitchenSinkErr::CrateNotFound(origin.clone())).context("releases not found")?;
                let versions: Vec<_> = releases.into_iter().filter_map(|r| {
                    let num_full = r.tag_name?;
                    let num = num_full.trim_start_matches(|c:char| !c.is_numeric());
                    // verify that it semver-parses
                    let _ = SemVer::parse(num).map_err(|e| warn!("{:?}: ignoring {}, {}", origin, num_full, e)).ok()?;
                    let date = r.published_at.or(r.created_at)?;
                    let date = DateTime::parse_from_rfc3339(&date)
                        .map_err(|e| warn!("{:?}: ignoring {}, {}", origin, date, e)).ok()?
                        .with_timezone(&Utc);
                    Some(CrateVersion {
                        num: num.into(),
                        yanked: r.draft.unwrap_or(false),
                        updated_at: date,
                        created_at: date,
                    })
                }).collect();
                if !versions.is_empty() {
                    return Ok(versions);
                }
                &r.package
            },
            _ => unreachable!(),
        };

        let versions: Vec<_> = self.crate_db.crate_versions(origin).await?.into_iter().map(|(num, timestamp)| {
            let date = Utc.timestamp_opt(i64::from(timestamp), 0).unwrap();
            CrateVersion {
                num,
                yanked: false,
                updated_at: date,
                created_at: date,
            }
        }).collect();
        if !versions.is_empty() {
            return Ok(versions);
        }
        let _f = self.throttle.acquire().await;
        info!("Need to scan repo {:?}", repo);
        let origin = origin.clone();
        let repo = repo.clone();
        let package = package.clone();
        let checkout = self.checkout_repo(repo.clone(), false).await?;

        let (_g, stop) = Stop::new();
        blocking::awatch(format!("checkout {origin:?}"), spawn_blocking(move || {
            let mut pkg_ver = crate_git_checkout::find_versions(&checkout, &stop)?;
            if let Some(v) = pkg_ver.remove(&*package) {
                let versions: Vec<_> = v.into_iter().map(|(num, timestamp)| {
                    let date = Utc.timestamp_opt(timestamp, 0).unwrap();
                    CrateVersion {
                        num: num.into(),
                        yanked: false,
                        updated_at: date,
                        created_at: date,
                    }
                }).collect();
                if !versions.is_empty() {
                    return Ok(versions);
                }
            }
            Err(KitchenSinkErr::CrateNotFound(origin)).context("missing releases, even tags")?
        })).await?
    }

    fn downloads_per_month_cached(&self, origin: &Origin, mut now: NaiveDate) -> KResult<Option<usize>> {
        Ok(match origin {
            Origin::CratesIo(name) => {
                let mut curr_year = now.year() as u16;
                let mut summed_days = self.summed_year_downloads(name, curr_year)?;

                let day_of_year = now.ordinal0();
                let missing_data_days = summed_days[0..day_of_year as usize].iter().copied().rev().take_while(|&s| s == 0).count().min(7);

                if missing_data_days > 0 {
                    now -= chrono::Duration::days(missing_data_days as _);
                }

                // TODO: make it an iterator
                let mut total = 0;
                for i in 0..30 {
                    let this_date = now - chrono::Duration::days(i);
                    let year = this_date.year() as u16;
                    if year != curr_year {
                        curr_year = year;
                        summed_days = self.summed_year_downloads(name, curr_year)?;
                    }
                    let day_of_year = this_date.ordinal0() as usize;
                    total += summed_days[day_of_year] as usize;
                }
                if total > 0 {
                    Some(total)
                } else {
                    None
                }
            },
            _ => None,
        })
    }

    #[inline]
    pub async fn downloads_per_month(&self, origin: &Origin) -> KResult<Option<usize>> {
        Ok(match origin {
            Origin::CratesIo(name) => {
                let now = Utc::now().date_naive();
                if let Some(total) = self.downloads_per_month_cached(origin, now)? {
                    return Ok(Some(total));
                }
                // Downloads are scraped daily, so <1 day crates need a fallback
                let meta = timeout("download counts fallback", 5, self.crates_io_meta(name)).await?;
                Some(meta.krate.recent_downloads.unwrap_or(0) / 3) // 90 days
            },
            _ => None,
        })
    }

    pub async fn downloads_per_month_or_equivalent(&self, origin: &Origin) -> CResult<Option<usize>> {
        if let Some(dl) = self.downloads_per_month(origin).await? {
            return Ok(Some(dl));
        }

        // arbitrary multiplier. TODO: it's not fair for apps vs libraries
        Ok(self.github_stargazers_and_watchers(origin).await?.map(|(stars, watch)| stars.saturating_sub(1) as usize * 40 + watch.saturating_sub(1) as usize * 125))
    }

    /// Only for GitHub origins, not for crates-io crates
    #[inline(never)]
    pub async fn github_stargazers_and_watchers(&self, origin: &Origin) -> CResult<Option<(u32, u32)>> {
        if let Origin::GitHub(r) = origin {
            let repo = Repo::GitHub(r.repo.clone());
            if let Some(gh) = Box::pin(self.github_repo(&repo)).await? {
                return Ok(Some((gh.stargazers_count, gh.subscribers_count)));
            }
        }
        Ok(None)
    }

    #[inline(never)]
    pub async fn crates_io_meta(&self, name: &str) -> KResult<CrateMetaFile> {
        debug_assert_eq!(name, name.as_ascii_lowercase());

        tokio::task::yield_now().await;
        running()?;

        let latest_in_index = {
            let index = self.index()?.crates_io_crates()?;
            let krate = index.get_res(name)?;
            krate.most_recent_version().version().to_string() // most recently published version
        };
        let not_found_err = || KitchenSinkErr::CrateNotFound(Origin::from_crates_io_name(name));
        let mut meta = watch("meta-1", self.crates_io.crate_meta(name, &latest_in_index).map_err(KitchenSinkErr::from))
            .await?.ok_or_else(not_found_err)?;
        if !meta.versions.iter().any(|v| v.num == latest_in_index) {
            warn!("Crate data missing latest version {}@{}", name, latest_in_index);
            meta = watch("meta-retry", self.crates_io.crate_meta(name, &format!("{latest_in_index}-try-again")).map_err(KitchenSinkErr::from))
                .await?.ok_or_else(not_found_err)?;
            if !meta.versions.iter().any(|v| v.num == latest_in_index) {
                error!("Error: crate data is borked {}@{}. Has only: {:?}", name, latest_in_index, meta.versions.iter().map(|v| &v.num).collect::<Vec<_>>());
                return Err(not_found_err());
            }
        }
        Ok(meta)
    }

    /// Wrapper for the latest version of a given crate.
    ///
    /// This function is quite slow, as it reads everything about the crate.
    ///
    /// There's no support for getting anything else than the latest version.
    #[tracing::instrument(level="info", skip(self))]
    #[inline(never)]
    pub fn rich_crate_version_async<'a>(&'a self, origin: &'a Origin) -> impl Future<Output = CResult<ArcRichCrateVersion>> + Send + 'a {
        watch("rcv-1", self.rich_crate_version_async_opt(origin, false, false)).map(|res| res.map(|(k,_)| k))
    }

    pub fn rich_crate_warnings<'a>(&'a self, origin: &'a Origin) -> impl Future<Output = CResult<HashSet<Warning>>> + Send + 'a {
        watch("rcv-1a", self.rich_crate_version_async_opt(origin, false, true)).map(|res| res.map(|(_, w)| w))
    }

    /// Same as rich_crate_version_async, but it won't try to refresh the data. Just fails if there's no cached data.
    #[tracing::instrument(level="debug",skip(self))]
    #[inline(never)]
    pub fn rich_crate_version_stale_is_ok<'a>(&'a self, origin: &'a Origin) -> impl Future<Output = CResult<ArcRichCrateVersion>> + Send + 'a {
        watch("r-stale", self.rich_crate_version_async_opt(origin, true, false)).map(|res| res.map(|(k,_)| k))
    }

    fn rich_crate_version_data_derived(&self, origin: &Origin, allow_stale: bool) -> KResult<Option<CachedCrate>> {
        blocking::block_in_place(format!("derived {origin:?}"), move || {
        let origin_str = origin.to_str();
        let key = (origin_str.as_str(), "");
        let maybe_data: Option<CachedCrate> = self.derived_storage.get_deserialized(key)?;

        if !allow_stale {
            if let Some(cached) = &maybe_data {
                if let Origin::CratesIo(name) = origin {
                    let expected_cache_key = self.index()?.cache_key_for_crates_io_index_state(name)?;
                    if expected_cache_key != cached.cache_key {
                        info!("Ignoring derived cache of {}, because it changed", name);
                        return Ok(None);
                    }
                }
            }
        }
        Ok(maybe_data)
        })
    }

    #[inline(never)]
    async fn rich_crate_version_async_opt(&self, origin: &Origin, allow_stale: bool, include_warnings: bool) -> CResult<(ArcRichCrateVersion, HashSet<Warning>)> {
        running()?;

        if !include_warnings {
            if let Some(krate) = self.loaded_rich_crate_version_cache.read().get(origin) {
                trace!("rich_crate_version_async HIT {:?}", origin);
                return Ok((krate.clone(), HashSet::new()));
            }
            trace!("rich_crate_version_async MISS {:?}", origin);
        }

        let mut data = match self.rich_crate_version_data_derived(origin, allow_stale)? {
            Some(data) => data,
            None => {
                if !self.crate_exists(origin) {
                    warn!("tried to fetch non-existent crate? {origin:?}");
                    return Err(KitchenSinkErr::CrateNotFound(origin.clone()).into());
                }
                if allow_stale {
                    self.event_log.post(&SharedEvent::CrateNeedsReindexing(origin.to_str()))?;
                    return Err(KitchenSinkErr::CacheExpired.into());
                } else {
                    debug!("Getting/indexing {:?}", origin);
                    let _th = timeout("autoindex-wait", 29, self.auto_indexing_throttle.acquire().map(|e| e.map_err(CError::from))).await?;
                    timeout(&format!("reindex-{origin:?}"), 59, self.index_crate_highest_version(origin, false))
                        .await.map_err(|e| { error!("{origin:?} crate reindex: {e}"); e })
                        .with_context(|| format!("reindexing {origin:?}"))?;
                }
                self.rich_crate_version_data_derived(origin, false)?.expect("crate must be in cache after indexing")
            },
        };

        self.refresh_crate_data(&mut data);

        let krate = Arc::new(RichCrateVersion::new(origin.clone(), data.manifest, data.derived));
        if !allow_stale {
            let mut cache = self.loaded_rich_crate_version_cache.write();
            if cache.len() > 6000 {
                cache.clear();
                cache.shrink_to(200);
            }
            cache.insert(origin.clone(), krate.clone());
        }
        Ok((krate, data.warnings))
    }

    // update cached data with external information that can change without reindexing
    fn refresh_crate_data(&self, data: &mut CachedCrate) {
        let package = data.manifest.package();

        // allow overrides to take effect without reindexing
        if let Some(overrides) = self.category_and_spam_overrides.get(package.name.as_str()) {
            data.derived.categories = overrides.iter()
                .filter_map(|c| {
                    if c == "hidden" {
                        data.derived.hidden = true;
                        return None;
                    }
                    if c == "spam" {
                        data.derived.spam = true;
                        return None;
                    }
                    Some((**c).into())
                }).collect();
        }
    }

    #[inline(never)]
    pub async fn changelog_url(&self, k: &RichCrateVersion) -> Option<String> {
        let repo = k.repository()?;
        if let Repo::GitHub(ref gh) = repo.host() {
            trace!("get gh changelog_url");
            let releases = self.gh.releases(gh, &self.cachebust_string_for_repo(repo).await.ok()?).await.ok()??;
            if releases.iter().any(|rel| rel.body.as_ref().map_or(false, |b| b.len() > 15)) {
                return Some(format!("https://github.com/{}/{}/releases", gh.owner, gh.repo));
            }
        }
        None
    }

    #[tracing::instrument(level="debug",skip(self))]
    #[inline(never)]
    async fn crate_files_summary_from_repo(&self, origin: Origin) -> CResult<CrateFilesSummary> {
        let (repo, package) = origin.repo().ok_or_else(|| KitchenSinkErr::GitCheckoutFailed("not a git crate".into()))?;
        let checkout = self.checkout_repo(repo.clone(), true).await?;
        let (_g, stop) = Stop::new();
        blocking::awatch(format!("repo-scan {repo:?} {origin:?}"), spawn_blocking(move || {
            let found = crate_git_checkout::path_in_repo(&checkout, &package, &stop)?
                .ok_or_else(|| {
                    let (has, err) = crate_git_checkout::find_manifests(&checkout, &stop).unwrap_or_default();
                    for e in err {
                        warn!("parse err: {}", e.0);
                    }
                    for h in has {
                        info!("has: {} -> {}", h.inner_path, h.manifest.package.as_ref().map_or("?", |p| p.name.as_str()));
                    }
                    KitchenSinkErr::CrateNotFoundInRepo(package.as_str().into(), repo.canonical_git_url().into())
                })?;


            let mut meta = tarball::read_repo(&checkout, found.tree, origin.package_name_icase(), &stop)?;
            debug_assert_eq!(meta.manifest.package, found.manifest.package);
            let package = meta.manifest.package.as_mut().ok_or(KitchenSinkErr::NotAPackage(origin))?;

            // Allowing any other URL would allow spoofing
            package.set_repository(Some(repo.canonical_git_url()));

            meta.path_in_repo = Some(found.inner_path);
            Ok::<_, CError>(meta)
        })).await?
    }

    #[inline(never)]
    async fn rich_crate_version_from_repo(&self, origin: &Origin) -> CResult<(CrateVersionSourceData, Manifest, Warnings)> {
        tokio::task::yield_now().await;
        let _f = self.throttle.acquire().await;
        running()?;

        let mut files = self.crate_files_summary_from_repo(origin.clone()).await?;

        let package = files.manifest.package.as_mut().ok_or_else(|| KitchenSinkErr::NotAPackage(origin.clone()))?;
        let mut warnings = HashSet::new();
        let has_readme = files.readme.is_some();
        if !has_readme {
            let maybe_repo = package.repository().and_then(|r| Repo::new(r).ok());
            warnings.insert(Warning::NoReadmeProperty);
            warnings.extend(Box::pin(self.add_readme_from_repo(&mut files, maybe_repo.as_ref())).await);
        }
        watch("data-common", self.rich_crate_version_data_common(origin.clone(), files, false, warnings)).await
    }

    pub async fn crate_files_summary_from_crates_io_tarball(&self, name: &str, ver: &str) -> Result<CrateFilesSummary, KitchenSinkErr> {
        let tarball = timeout("tarball fetch", 16, self.crates_io.crate_data(name, ver)
            .map_err(|e| KitchenSinkErr::DataNotFound(format!("{name}-{ver}: {e}")))).await?;

        let ugh = format!("untar1-{name}-{ver}");
        let meta = timeout(&ugh, 70, spawn_blocking({
                let name = name.to_owned();
                let ver = ver.to_owned();
                move || tarball::read_archive(&tarball[..], &name, &ver)
            })
            .map_err(|e| KitchenSinkErr::Internal(std::sync::Arc::new(e)))).await?
            .map_err(|e| KitchenSinkErr::UnarchiverError(format!("{name}-{ver}"), Arc::new(e)))?;
        Ok(meta)
    }

    #[tracing::instrument(level="debug",skip_all)]
    #[inline(never)]
    async fn rich_crate_version_data_from_crates_io(&self, name: &str, ver: &str, is_yanked: bool) -> CResult<(CrateVersionSourceData, Manifest, Warnings)> {
        debug!("Building whole crate {name}");

        let _f = timeout("data-throttle", 28, self.throttle.acquire().map_err(|_| KitchenSinkErr::DataTimedOut)).await;

        let mut warnings = HashSet::new();

        let origin = Origin::from_crates_io_name(name);
        let name_lower = name.as_ascii_lowercase();
        let name_lower = &*name_lower;

        tokio::task::yield_now().await;
        running()?;

        let (meta, crates_io_meta) = futures::join!(
            self.crate_files_summary_from_crates_io_tarball(name, ver),
            timeout("cio meta fetch", 16, self.crates_io_meta(name_lower)),
        );

        let mut files = meta?;
        let crates_io_krate = crates_io_meta?.krate;
        let package = files.manifest.package.as_mut().ok_or_else(|| KitchenSinkErr::NotAPackage(origin.clone()))?;

        // it may contain data from "nowhere"! https://github.com/rust-lang/crates.io/issues/1624
        if package.homepage().is_none() {
            if let Some(url) = crates_io_krate.homepage {
                package.set_homepage(Some(url.into()));
            }
        }
        if package.documentation().is_none() {
            if let Some(url) = crates_io_krate.documentation.as_deref() {
                package.set_documentation(Some(url.into()));
            }
        }

        if let Some(repo_url) = package.repository().and_then(|u| url::Url::parse(u).ok()) {
            let host = repo_url.host_str().unwrap_or_default();
            // definitely not a repo
            if matches!(host, "crates.io" | "docs.rs" | "lib.rs" | "localhost") {
                warnings.insert(Warning::NoRepositoryProperty);
                package.repository = None;
            }
        } else {
            // Guess repo URL if none was specified; must be done before getting stuff from the repo
            warnings.insert(Warning::NoRepositoryProperty);
            // it may contain data from nowhere! https://github.com/rust-lang/crates.io/issues/1624
            if let Some(repo) = crates_io_krate.repository {
                package.set_repository(Some(repo.into()));
            } else if package.homepage().map_or(false, Repo::looks_like_repo_url) {
                let home = package.homepage.take().and_then(Option::from);
                package.set_repository(home);
            }
        }

        // See if we've indexed the repo and know where the crate is
        if package.repository().is_none() {
            let owners = self.crate_owners(&origin, CrateOwners::Strict).await?;
            for login in owners.iter().filter_map(|o| o.github_login()) {
                if let Some(repo) = self.crate_db.github_repos_with_crate(login, name).await?.get(0) {
                    let url = repo.canonical_git_url();
                    info!("found repo {url} for crate {name}");
                    package.set_repository(Some(url));
                    break;
                }
            }
        }

        let maybe_repo = package.repository().and_then(|r| Repo::new(r).ok());
        let has_readme_file = files.readme.is_some();
        if !has_readme_file {
            let has_readme_prop = files.manifest.package.as_ref().map_or(false, |p| p.readme().is_some());
            if has_readme_prop {
                warnings.insert(Warning::NoReadmePackaged);
            } else {
                warnings.insert(Warning::NoReadmeProperty);
            }
            // readmes in form of readme="../foo.md" are lost in packaging,
            // and the only copy exists in crates.io own api
            watch("readme", self.add_readme_from_crates_io(&mut files, name, ver)).await;
            let has_readme = files.readme.is_some();
            if !has_readme {
                warnings.extend(Box::pin(self.add_readme_from_repo(&mut files, maybe_repo.as_ref())).await);
                debug!("repo fixed readme? {}", files.readme.is_some());
            }
        }

        running()?;

        if files.path_in_repo.is_none() {
            if let Some(r) = maybe_repo.as_ref() {
                files.path_in_repo = self.crate_db.path_in_repo(r, name).await?;
            }
        }

        watch("data-common", self.rich_crate_version_data_common(origin, files, is_yanked, warnings)).await
    }

    ///// Fixing and faking the data
    #[inline(never)]
    async fn rich_crate_version_data_common(&self, origin: Origin, mut files: CrateFilesSummary, is_yanked: bool, mut warnings: Warnings) -> CResult<(CrateVersionSourceData, Manifest, Warnings)> {
        running()?;


        let package = files.manifest.package.as_mut().ok_or_else(|| KitchenSinkErr::NotAPackage(origin.clone()))?;
        let maybe_repo = package.repository().and_then(|r| Repo::new(r).ok());

        let explicit_documentation_link_existed = package.documentation().is_some();

        if origin.is_crates_io() {
            // Delete the original docs.rs link, because we have our own
            // TODO: what if the link was to another crate or a subpage?
            if package.documentation().as_ref().map_or(false, |s| Self::is_docs_rs_link(s)) && self.has_docs_rs(&origin, &package.name, package.version()).await {
                package.set_documentation(None::<String>); // docs.rs is not proper docs
            }
        }

        warnings.extend(self.remove_redundant_links(package, maybe_repo.as_ref()).await);

        let mut github_topics = None;
        let mut github_description = None;
        let mut github_name = None;
        let mut github_default_branch = None;
        if let Some(crate_repo) = maybe_repo.as_ref() {
            if let Some(mut ghrepo) = Box::pin(self.github_repo(crate_repo)).await? {
                // clean data against orig repo URL, because other metadata is as old,
                // but do it after knowing the latest repo url, to fix to most up to date URLs
                let github_page_url = ghrepo.github_page_url();

                // Check if the repo is a redirect to new owner
                if let Some(ghrepo_owner) = &ghrepo.owner {
                    let old_owner_login = crate_repo.url_owner_name().expect("gh always has");
                    if !old_owner_login.eq_ignore_ascii_case(&ghrepo_owner.login) {
                        let new_repo_url = ghrepo.html_url();
                        info!("Found change in repo owner {} -> {}", old_owner_login, ghrepo_owner.login);
                        // ownership of repos can be transferred, so redirect does not imply new login
                        if let Some(new_repo_url) = new_repo_url {
                            // Fetch latest one via canonical URL, not the old redirect
                            if let Some(latest) = Box::pin(self.github_repo(&Repo::new(&new_repo_url)?)).await? {
                                ghrepo = latest;
                            }
                            package.set_repository(Some(new_repo_url));
                        }
                    }
                }
                if ghrepo.archived && files.manifest.badges.maintenance.status == MaintenanceStatus::None {
                    files.manifest.badges.maintenance.status = MaintenanceStatus::AsIs;
                }
                if package.homepage().is_none() {
                    if let Some(url) = ghrepo.homepage.as_deref() {
                        let also_add_docs = package.documentation().is_none() && github_page_url.as_deref().map_or(false, |p| p != url);
                        package.set_homepage(Some(url.into()));
                        // github pages URLs are often bad, so don't even try to use them unless documentation property is missing
                        // (especially don't try to replace docs.rs with this gamble)
                        if also_add_docs && !explicit_documentation_link_existed {
                            if let Some(url) = github_page_url {
                                package.set_documentation(Some(url));
                            }
                        }
                    } else if let Some(url) = github_page_url {
                        package.set_homepage(Some(url));
                    }
                    warnings.extend(self.remove_redundant_links(package, maybe_repo.as_ref()).await);
                }
                if package.description().is_none() {
                    package.set_description(ghrepo.description);
                } else {
                    github_description = ghrepo.description;
                }
                github_default_branch = ghrepo.default_branch;
                github_name = Some(ghrepo.name);
                github_topics = if !ghrepo.topics.is_empty() { Some(ghrepo.topics) } else { None };
            }
        }

        let mut github_keywords = None;
        // Guess keywords if none were specified
        // TODO: also ignore useless keywords that are unique db-wide
        if github_topics.is_none() {
            if let Some(repo) = maybe_repo.as_ref() {
                if let Repo::GitHub(gh) = repo.host() {
                    trace!("get gh topics");
                    github_topics = self.gh.topics(gh, &self.cachebust_string_for_repo(repo).await.context("fetch topics")?).await?
                }
            }
        }
        if let Some(mut topics) = github_topics {
            for t in &mut topics {
                if t.starts_with("rust-") || t.ends_with("-rs") {
                    *t = t.trim_start_matches("rust-").trim_end_matches("-rs").into();
                }
            }
            topics.retain(|t| match t.as_str() {
                "rust" | "rs" | "rustlang" | "rust-lang" | "crate" | "crates" | "library" |
                "built-with-rust" | "blazingly-fast" | "zlib-license" | "mit-license" => false,
                _ => !t.contains("hacktober"),
            });
            if !topics.is_empty() {
                github_keywords = Some(topics);
            }
        }


        // crates can be published with a repo sha1 that is never pushed
        let mut vcs_info_git_sha1_is_broken = false;
        let canonical_http_url_sha1_broken = match &maybe_repo {
            Some(repo) => {
                let path_in_repo = files.path_in_repo.as_deref().unwrap_or_default();
                let url = repo.canonical_http_url_sha1(path_in_repo, files.vcs_info_git_sha1);
                let result = self.check_url_is_valid(&url).await;
                if !result {
                    vcs_info_git_sha1_is_broken = true;
                    warnings.insert(Warning::BrokenLink("repository exact revision".into(), url.into()));

                    let fallback_url = if let branch @ Some(_) = github_default_branch.as_deref() {
                        repo.canonical_http_url(path_in_repo, branch)
                    } else {
                        repo.canonical_http_url_sha1(path_in_repo, None)
                    };
                    if !self.check_url_is_valid(&fallback_url).await {
                        warnings.insert(Warning::BrokenLink("repository relative path".into(), fallback_url.into()));
                        files.path_in_repo = None;
                        github_default_branch = None;
                    }
                }
                !result
            },
            None => false,
        };

        catch_unwind(move || {
            let package = files.manifest.package.as_mut().ok_or_else(|| KitchenSinkErr::NotAPackage(origin.clone()))?;

        let capitalized_name = if package.name != package.name.as_ascii_lowercase() {
            // if the crate name isn't all-lowercase, then keep it as-is
            package.name.clone()
        } else {
            // Process crate's text to guess non-lowercased name
            let mut words = vec![package.name.as_str()];
            let readme_txt;
            if let Some(ref r) = files.readme {
                readme_txt = render_readme::Renderer::new(None).visible_text_by_section(&r.markup);
                for (s, txt) in &readme_txt {
                    words.push(s);
                    words.push(txt);
                }
            }
            if let Some(ref lib) = files.lib_file {
                words.push(lib);
            }
            if let Some(s) = package.description() {words.push(s);}
            if let Some(ref s) = github_description {words.push(s);}
            if let Some(ref s) = github_name {words.push(s);}
            if let Some(s) = package.homepage() {words.push(s);}
            if let Some(s) = package.documentation() {words.push(s);}
            if let Some(s) = package.repository() {words.push(s);}

            Self::capitalized_name(&package.name, words.into_iter())
        };

        let has_buildrs = files.has("build.rs");
        let has_code_of_conduct = files.has("CODE_OF_CONDUCT.md") || files.has("docs/CODE_OF_CONDUCT.md") || files.has(".github/CODE_OF_CONDUCT.md");

        let mut readme_was_missing = true;
        let readme_is_short = self.is_readme_short(files.readme.as_ref().map(|r| &r.markup));
        let mut readme = files.readme.map(|tarball::Readme {rel_path, markup, found_relevant, git_sha1}| {
            if found_relevant {
                readme_was_missing = false;
            }
            let (base_url, base_image_url) = match maybe_repo {
                Some(repo) => {
                    // Not parsing github URL, because "aboslute" path should not be allowed to escape the repo path,
                    // but it needs to normalize ../readme paths
                    let url = url::Url::parse(&format!("http://localhost/{}", files.path_in_repo.as_deref().unwrap_or_default())).and_then(|u| u.join(&rel_path));
                    let in_repo_url_path = url.as_ref().map_or("", |u| u.path().trim_start_matches('/'));
                    let treeish_revision = if vcs_info_git_sha1_is_broken && git_sha1 == files.vcs_info_git_sha1 { github_default_branch } else { git_sha1.as_ref().map(hex::encode) };
                    (Some(repo.readme_base_url(in_repo_url_path, treeish_revision.as_deref())), Some(repo.readme_base_image_url(in_repo_url_path, treeish_revision.as_deref())))
                },
                None => (None, None),
            };
            debug!("got readme at {:?} {:?}", base_url, base_image_url);
            Readme {
                markup,
                base_url,
                base_image_url,
            }
        });

        let features_enable_by_feature = extract_idents_enabled_by_features(&files.modules);

        let text_from_source_code = feat_extractor::extract_text_from_source_code(files.modules);

        // lib file takes majority of space in cache, so remove it if it won't be used
        if !readme_was_missing && !readme_is_short {
            files.lib_file = None;
        }

        // a good lib.rs text is better than bad readme text
        if readme_was_missing && files.lib_file.as_ref().map_or(false, |txt| extract_doc_comments(txt).len() > 1000) {
            debug!("dropping bad readme! got good lib");
            readme = None;
        }

        let crate_compressed_size = files.compressed_size.min(u32::MAX as _) as u32;
        let src = CrateVersionSourceData {
            capitalized_name,
            language_stats: files.language_stats,
            crate_compressed_size,
            // sometimes uncompressed sources without junk are smaller than tarball with junk
            crate_decompressed_size: (files.decompressed_size as u32).max(crate_compressed_size),
            is_nightly: files.is_nightly,
            has_buildrs,
            has_code_of_conduct,
            readme,
            lib_file: files.lib_file,
            bin_file: files.bin_file,
            github_description,
            github_keywords,
            path_in_repo: files.path_in_repo,
            is_yanked,
            vcs_info_git_sha1: files.vcs_info_git_sha1,
            readme_was_missing,
            canonical_http_url_sha1_broken,
            text_from_source_code,
            features_comments: files.features_comments.filter(|f| !f.is_empty()),
            features_enable_items: features_enable_by_feature,
            suspicious_binary_files: files.suspicious_binary_files,
        };
        Ok((src, files.manifest, warnings))

        }).map_err(|e| anyhow::anyhow!("panic: {e:?}"))?
    }

    pub fn is_unmaintained_in_advisories(&self, origin: &Origin, advisories: &[Advisory], crev_reviews: &[Review], version: &SemVer) -> Result<bool, KitchenSinkErr> {
        let rustsec_flag = advisories.iter()
            .filter(|a| !a.withdrawn() && a.versions.is_vulnerable(version))
            .filter_map(|a| a.metadata.informational.as_ref())
            .any(|a| a.is_unmaintained());
        if rustsec_flag {
            debug!("rustsec says {origin:?} is unmaintained");
            return Ok(rustsec_flag);
        }

        let crev_flag = crev_reviews.iter()
            .filter(|r| r.version == *version).any(|r| r.unmaintained);
        if crev_flag {
            debug!("crev says {origin:?} is unmaintained");
        }
        Ok(crev_flag)
    }

    fn canonical_http_of_crate_at_version_cache_key(origin: &Origin, crate_version: &str) -> String {
        format!("{}-{crate_version}", origin.package_name_icase())
    }

    pub fn canonical_http_of_crate_at_version_cached(&self, origin: &Origin, crate_version: &str) -> Option<String> {
        self.canonical_http_of_crate_at_version_cache.get(Self::canonical_http_of_crate_at_version_cache_key(origin, crate_version).as_str()).ok().flatten()
    }

    pub async fn canonical_http_of_crate_at_version(&self, origin: &Origin, crate_version: &str) -> CResult<String> {
        if let Some(s) = self.canonical_http_of_crate_at_version_cached(origin, crate_version) {
            return Ok(s);
        }

        let ver = self.crate_files_summary_from_crates_io_tarball(origin.package_name_icase(), crate_version).await?;
        let sha = ver.vcs_info_git_sha1;
        if let Some(sha) = sha {
            let package = ver.manifest.package.ok_or_else(|| KitchenSinkErr::NotAPackage(origin.clone()))?;
            if let Some(Ok(repo)) = package.repository().map(Repo::new) {
                let path_in_repo = match ver.path_in_repo {
                    Some(p) => p,
                    None => self.crate_db.path_in_repo(&repo, &package.name).await?.unwrap_or_default(),
                };
                let url = repo.canonical_http_url_sha1(&path_in_repo, Some(sha));
                self.canonical_http_of_crate_at_version_cache.set(Self::canonical_http_of_crate_at_version_cache_key(origin, crate_version), &url)?;
                return Ok(url);
            }
        }
        let mut url = format!("https://docs.rs/crate/{crate_name}/{version}/source/", crate_name = urlencoding::Encoded(origin.package_name_icase()), version = urlencoding::Encoded(crate_version));
        if let Some(sha) = sha {
            use std::fmt::Write;
            let _ = write!(&mut url, "#{}", hex::encode(sha));
        }
        self.canonical_http_of_crate_at_version_cache.set(Self::canonical_http_of_crate_at_version_cache_key(origin, crate_version), &url)?;
        Ok(url)
    }

    #[allow(deprecated)]
    #[inline(never)]
    fn override_bad_categories(manifest: &mut Manifest, extra_keywords: &mut Vec<SmolStr>) {
        let direct_dependencies = &manifest.dependencies;
        let has_cargo_bin = manifest.has_cargo_bin();
        let package = manifest.package.as_mut().expect("pkg");
        let eq = crate_name_fuzzy_eq;

        let keywords = package.keywords.as_ref().unwrap();

        for cat in package.categories.as_mut().unwrap().iter_mut() {
            if cat.as_bytes().iter().any(|c| c.is_ascii_uppercase()) {
                cat.make_ascii_lowercase();
            }
            if has_cargo_bin && (cat == "development-tools" || cat == "command-line-utilities") && keywords.iter().any(|k| eq(k, "cargo-subcommand") || eq(k, "subcommand")) {
                *cat = "development-tools::cargo-plugins".into();
            }
            if cat == "localization" {
                // nobody knows the difference
                *cat = "internationalization".to_string();
                extra_keywords.push("localization".into());
            }
            if cat == "parsers" && (direct_dependencies.keys().any(|k| k == "nom" || k == "peresil" || k == "combine" || k == "prost") ||
                    keywords.iter().any(|k| match k.as_ascii_lowercase().as_ref() {
                        "asn1" | "tls" | "idl" | "crawler" | "xml" | "nom" | "json" | "logs" | "elf" | "uri" | "html" | "protocol" | "semver" | "ecma" |
                        "chess" | "vcard" | "exe" | "fasta" | "fastq" | "tree-sitter" | "markdown" | "tsv" | "llvm" | "bioinformatics" => true,
                        _ => false,
                    })) {
                *cat = "parser-implementations".into();
            }
            if (cat == "cryptography" || cat == "database" || cat == "finance" || cat == "rust-patterns" || cat == "development-tools") && keywords.iter().any(|k| eq(k, "bitcoin") || eq(k, "cryptocurrency") || eq(k, "solana") || eq(k, "ethereum") || eq(k, "exonum") || eq(k, "blockchain")) {
                *cat = "cryptography::cryptocurrencies".into();
            }
            // crates-io added a better category
            if cat == "game-engines" {
                *cat = "game-development".to_string();
            }
            if cat == "games" && keywords.iter().any(|k| {
                    eq(k, "game-dev") || eq(k, "game-development") || eq(k,"gamedev") || eq(k,"framework") || eq(k,"utilities") || eq(k,"parser") || eq(k,"api")
                }) {
                *cat = "game-development".into();
            }

            if cat == "data-structure" {
                *cat = "data-structures".to_string();
            }
            if cat == "serialization" {
                *cat = "encoding".to_string();
            }
            if cat == "async" {
                *cat = "asynchronous".to_string();
            }
            if cat == "no-std::no-alloc" {
                *cat = "no-std".to_string();
                extra_keywords.push("no-alloc".into());
            }
            if cat == "blockchain" {
                *cat = "cryptography::cryptocurrencies".to_string();
            }
            // useless category
            if cat == "multimedia::encoding" {
                *cat = "multimedia".to_string();
                extra_keywords.push("encoding".into());
            }
            if cat == "aerospace::simulation" {
                *cat = "simulation".to_string();
                extra_keywords.push("aerospace".into());
            }
            if cat == "aerospace::drones" {
                *cat = "science::robotics".to_string();
                extra_keywords.push("drones".into());
            }
            if cat == "science::neuroscience" {
                *cat = "science::bio".into();
                extra_keywords.push("neuroscience".into());
            }
            if cat == "aerospace::unmanned-aerial-vehicles" {
                *cat = "science::robotics".to_string();
                extra_keywords.push("unmanned-aerial-vehicles".into());
            }
            if cat == "aerospace::space-protocols" {
                *cat = "science".to_string();
                extra_keywords.push("space-protocols".into());
            }
            if cat == "aerospace::protocols" {
                *cat = "science".to_string();
                extra_keywords.push("protocols".into());
            }
            if cat == "os::linux-apis" {
                *cat = "os::unix-apis".to_string();
                extra_keywords.push("linux".into());
            }
            if cat == "os::freebsd-apis" {
                *cat = "os::unix-apis".to_string();
                extra_keywords.push("freebsd".into());
            }

            // got out of sync with crates-io
            if cat == "mathematics" {
                *cat = "science::math".to_string();
            }
            if cat == "science" || cat == "algorithms" {
                let is_nn = |k: &String| eq(k,"neural-network") || eq(k,"machine-learning") || eq(k,"neuralnetworks") || eq(k,"neuralnetwork") || eq(k,"tensorflow") || eq(k,"deep-learning");
                let is_math = |k: &String| {
                    k == "math" || eq(k,"calculus") || eq(k,"algebra") || eq(k,"linear-algebra") || eq(k,"mathematics") || eq(k,"maths") || eq(k,"number-theory")
                };
                if keywords.iter().any(|k| k == "bioinformatics" || k == "biology" || k == "dna" || eq(k, "rna-seq") || k == "fastq" || k == "fasta") {
                    *cat = "science::bio".into();
                } else if keywords.iter().any(is_nn) {
                    *cat = "science::ml".into();
                } else if keywords.iter().any(is_math) {
                    *cat = "science::math".into();
                }
            }
        }
    }

    /// Cachebust is crate name + version
    #[inline(never)]
    pub async fn github_repo(&self, crate_repo: &Repo) -> Result<Option<GitHubRepo>, KitchenSinkErr> {
        Ok(match crate_repo.host() {
            Repo::GitHub(ref repo) => {
                let cachebust = &self.cachebust_string_for_repo(crate_repo).await?; // must use this, not version, because there are multiple crates per repo
                let repo_fetch = self.gh.repo(repo, cachebust)
                    .map_err(|e| KitchenSinkErr::GitHub(format!("{crate_repo:?} {e}")));
                match timeout("gh2", 7, repo_fetch).await? {
                    Some(repo) => {
                        // take a note of the most up-to-date ID
                        if let Some(u) = &repo.owner {
                            if let Some(id) = u.id {
                                let _ = self.user_db.index_user(&User {
                                    id,
                                    login: u.login.clone(),
                                    name: u.name.clone(),
                                    avatar_url: u.avatar_url.clone(),
                                    gravatar_id: u.gravatar_id.clone(),
                                    html_url: u.html_url.clone(),
                                    blog: None,
                                    two_factor_authentication: None,
                                    user_type: u.user_type,
                                    created_at: u.created_at.clone(),
                                }, None, None,
                                SystemTime::now() - Duration::from_secs(3600*24*28), true) // that's cache length of the gh.repo - don't use cached data to override later one
                                .map_err(|e| warn!("user {id} index: {e}"));
                            }
                        }
                        Some(repo)
                    },
                    None => None,
                }
            },
            _ => None,
        })
    }

    pub fn is_readme_short(&self, readme: Option<&Markup>) -> bool {
        if let Some(r) = readme {
            match r {
                Markup::Markdown(ref s) | Markup::Rst(ref s) | Markup::AsciiDoc(ref s) | Markup::Html(ref s) => s.len() < 1000,
            }
        } else {
            true
        }
    }

    /// `(is_build, is_dev)` based only on usage stats
    #[inline(never)]
    pub fn is_build_or_dev(&self, k: &Origin) -> Result<(bool, bool), KitchenSinkErr> {
        let stats = self.index()?.deps_stats()?;
        Ok(blocking::block_in_place("depsof2", || stats.crates_io_dependents_stats_of(k))
        .map_or((false, false), |d| {
            // direct deps are more relevant, but sparse data gives wrong results
            let direct_weight = 1 + d.direct.all()/4;

            let build = u32::from(d.direct.build) * direct_weight + d.build.def * 2 + d.build.opt;
            let runtime = u32::from(d.direct.runtime) * direct_weight + d.runtime.def * 2 + d.runtime.opt;
            let dev = u32::from(d.direct.dev) * direct_weight + u32::from(d.dev) * 2;
            let is_build = build > 3 * (runtime + 15); // fudge factor, don't show anything if data is uncertain
            let is_dev = !is_build && dev > (3 * runtime + 3 * build + 15);
            (is_build, is_dev)
        }))
    }

    #[tracing::instrument(level="debug",skip_all)]
    async fn add_readme_from_repo(&self, files: &mut CrateFilesSummary, maybe_repo: Option<&Repo>) -> Warnings {
        let mut warnings = HashSet::new();
        let Some(package) = files.manifest.package.as_ref() else {
            warnings.insert(Warning::NotAPackage);
            return warnings;
        };
        if let Some(repo) = maybe_repo {
            let res = self.checkout_repo(repo.clone(), true).await
                .and_then(|res| { running()?; Ok(res) })
                .map(|checkout| block_in_place(format!("checkout {repo:?}"), || crate_git_checkout::find_readme(&checkout, package, &STOPPED)));
            match res {
                Ok(Ok(Some((mut path, markup, found_relevant, git_sha1)))) => {
                    debug!("scanned repo of {} for readme best={found_relevant}; path={}", package.name, path.display());
                    // Make the path absolute, because the readme is now absolute relative to repo root,
                    // rather than relative to crate's dir within the repo
                    if !path.is_absolute() {
                        path = Path::new("/").join(path);
                    }
                    files.readme = Some(tarball::Readme {
                        rel_path: path.display().to_string(), markup, found_relevant,
                        git_sha1,
                    });
                },
                Ok(Ok(None)) => {
                    warnings.insert(Warning::NoReadmeInRepo(repo.canonical_git_url().into()));
                },
                Ok(Err(err)) => {
                    warnings.insert(Warning::ErrorCloning(repo.canonical_git_url().into()));
                    error!("Search of {} ({}) failed: {err}", package.name, repo.canonical_git_url());
                },
                Err(err) => {
                    warnings.insert(Warning::ErrorCloning(repo.canonical_git_url().into()));
                    error!("Checkout of {} ({}) failed: {err}", package.name, repo.canonical_git_url());
                },
            }
        }
        warnings
    }

    #[tracing::instrument(level="debug",skip(self, meta))]
    #[inline(never)]
    async fn add_readme_from_crates_io(&self, meta: &mut CrateFilesSummary, name: &str, ver: &str) {
        let key = format!("{name}/{ver}");
        if let Ok(Some(_)) = self.readme_check_cache.get(key.as_str()) {
            return;
        }

        if let Ok(Some(html)) = self.crates_io.readme(name, ver).await {
            debug!("Found readme on crates.io {name}@{ver}");
            let html = String::from_utf8_lossy(&html).into_owned();
            if !html.trim_start().is_empty() {
                meta.readme = Some(tarball::Readme {
                    rel_path: meta.manifest.package().readme().as_path().and_then(|p| p.to_str()).unwrap_or_default().to_owned(),
                    markup: Markup::Html(html),
                    found_relevant: true,
                    git_sha1: meta.vcs_info_git_sha1,
                });
            }
        } else {
            let _ = self.readme_check_cache.set(key, ());
            debug!("No readme on crates.io for {name}@{ver}");
        }
    }

    #[inline(never)]
    async fn remove_redundant_links(&self, package: &mut Package, maybe_repo: Option<&Repo>) -> Warnings {
        let mut warnings = HashSet::new();

        // We show github link prominently, so if homepage = github, that's nothing new
        let homepage_is_repo = Self::is_same_url(package.homepage(), package.repository());
        let homepage_is_canonical_repo = maybe_repo
            .and_then(|repo| {
                package.homepage()
                .and_then(|home| Repo::new(home).ok())
                .map(|other| {
                    repo.canonical_git_url() == other.canonical_git_url()
                })
            })
            .unwrap_or(false);

        if homepage_is_repo || homepage_is_canonical_repo {
            package.set_homepage(None::<String>);
        }

        if Self::is_same_url(package.documentation(), package.homepage()) ||
           Self::is_same_url(package.documentation(), package.repository()) ||
           maybe_repo.map_or(false, |repo| Self::is_same_url(Some(&*repo.canonical_http_url("", None)), package.documentation())) {
            package.set_documentation(None);
        }

        if package.homepage().map_or(false, |d| Self::is_docs_rs_link(d) || d.starts_with("https://lib.rs/") || d.starts_with("https://crates.io/")) {
            package.set_homepage(None);
        }

        if let Some(url) = package.homepage() {
            if !self.check_url_is_valid(url).await {
                warnings.insert(Warning::BrokenLink("homepage".into(), url.into()));
                package.set_homepage(None);
            }
        }

        if let Some(url) = package.documentation() {
            if !self.check_url_is_valid(url).await {
                warnings.insert(Warning::BrokenLink("documentation".into(), url.into()));
                package.set_documentation(None);
            }
        }
        warnings
    }

    #[inline(never)]
    async fn check_url_is_valid(&self, url: &str) -> bool {
        let retries = 1 + if let Ok(Some((res, retries_so_far))) = self.url_check_cache.get(url) {
            if res || retries_so_far > 3 {
                return res;
            }
            retries_so_far
        } else {
            0
        };
        let res = timeout("urlchk", 10, async {
            let req = reqwest::Client::builder().build().unwrap();
            let res = match req.get(url).send().await {
                Ok(res) => res.status().is_success(),
                Err(e) => {
                    warn!("URL CHK: {} = {}", url, e);
                    false
                },
            };
            Ok::<_, KitchenSinkErr>(res)
        }).await.unwrap_or(false);
        let _ = self.url_check_cache.set(url, (res, retries)).map_err(|e| error!("url cache: {}", e));
        res
    }

    fn is_docs_rs_link(d: &str) -> bool {
        let d = d.trim_start_matches("http://").trim_start_matches("https://");
        d.starts_with("docs.rs/") || d.starts_with("crates.fyi/")
    }

    /// name is case-sensitive!
    pub async fn has_docs_rs(&self, origin: &Origin, name: &str, ver: &str) -> bool {
        if !origin.is_crates_io() {
            return false;
        }
        watch("builds", self.docs_rs.builds(name, ver)).await.unwrap_or(true) // fail open
    }

    fn is_same_url(a: Option<&str>, b: Option<&str>) -> bool {
        fn trim(s: &str) -> &str {
            let s = s.trim_start_matches("http://").trim_start_matches("https://");
            s.split('#').next().unwrap().trim_end_matches("/index.html").trim_end_matches('/')
        }

        match (a, b) {
            (Some(a), Some(b)) if crate_name_fuzzy_eq(trim(a), trim(b)) => true,
            _ => false,
        }
    }

    pub fn all_dependencies_flattened(&self, krate: &RichCrateVersion) -> Result<DepInfMap, KitchenSinkErr> {
        block_in_place(format!("fdeps of {}", krate.short_name()), || {
        let index = self.index()?;
        let locked;
        match krate.origin() {
            Origin::CratesIo(name) => {
                locked = index.crates_io_crates()?;
                let krate = locked.get_res(name)?;
                index.all_dependencies_flattened(krate).map_err(KitchenSinkErr::Deps)
            },
            _ => index.all_dependencies_flattened(krate).map_err(KitchenSinkErr::Deps),
        }
        })
    }

    pub fn all_crates_io_versions(&self, origin: &Origin) -> Result<Vec<CratesIndexVersion>, KitchenSinkErr> {
        match origin {
            Origin::CratesIo(name) => {
                Ok(self.index()?.crates_io_crates()?.get_res(name)?.versions().to_vec())
            },
            _ => Err(KitchenSinkErr::NoVersions),
        }
    }

    #[inline]
    pub fn prewarm(&self) {
        let _ = self.index().unwrap().deps_stats().map_err(|e| error!("prewarm {e}"));
        let _ = self.advisories_for_crate(&Origin::from_crates_io_name("libc")).map_err(|e| error!("prewarm {e}"));
    }

    pub fn reload_indexed_crate(&self, origin: &Origin) {
        let timeout = Duration::from_secs(25);
        self.loaded_rich_crate_version_cache.try_write_for(timeout).unwrap().remove(origin);
        self.crate_rustc_compat_cache.try_write_for(timeout).unwrap().remove(origin);
    }

    pub fn force_crate_reindexing(&self, origin: &Origin) {
        if let Origin::CratesIo(crate_name) = origin {
            let _ = self.crates_io_owners_cache.delete(&**crate_name);
        }
        let origin_str = origin.to_str();
        let _ = self.derived_storage.delete((origin_str.as_str(), ""));
        self.loaded_rich_crate_version_cache.write().remove(origin);
    }

    /// Not including crates main index
    #[tracing::instrument(level="debug",skip_all)]
    pub fn update_repositories(&self) {
        let crev = Arc::clone(&self.crev);
        let rustsec = Arc::clone(&self.rustsec);
        rayon::join(move || {
            if let Some(rustsec) = rustsec.try_write_for(Duration::from_secs(10)).unwrap().as_mut() {
                let _ = rustsec.update().map_err(|e| error!("rustsec update: {e}"));
            }
        }, move || {
            let _ = crev.update().map_err(|e| error!("crev update: {e}"));
        });
    }

    fn clear_loaded_rich_crate_version_cache(&self) {
        blocking::watch("clearcache1", move || {
        let mut locked = self.loaded_rich_crate_version_cache.try_write_for(Duration::from_secs(5)).unwrap();
        locked.clear();
        locked.shrink_to(0);
        })
    }

    #[tracing::instrument(level="debug",skip_all)]
    pub fn update_crate_index(&self) {
        self.clear_loaded_rich_crate_version_cache();
        let index = self.index().unwrap();
        let _ = index.update_crate_index().map_err(|e| error!("index update: {e}"));
        // something new could have been cached while the index has been updating
        // clearing twice is better than holding a lock over network access time
        self.clear_loaded_rich_crate_version_cache();
    }

    #[inline(never)]
    pub async fn crates_io_all_rev_deps_counts(&self) -> Result<StatsHistogram, KitchenSinkErr> {
        let index = self.index()?;
        let stats = index.deps_stats().map_err(KitchenSinkErr::Deps)?;
        let mut tmp = HashMap::new();
        for (o, r) in &stats.counts {
            let cnt = r.counters.runtime.all() + r.counters.build.all() + u32::from(r.counters.dev);
            let t = tmp.entry(cnt).or_insert((0, Vec::new()));
            t.0 += 1;
            if t.1.len() < 50 {
                t.1.push((r.counters.direct.all(), o.to_string()));
            }
        }

        Ok(tmp.into_iter().map(|(k, (cnt, mut examples))| {
            examples.sort_unstable_by_key(|(dcnt,_)| !dcnt);
            (k, (cnt, examples.into_iter().take(8).map(|(_, n)| n).collect()))
        }).collect())
    }

    /// cargo-vet reviews
    pub async fn vets_for_crate(&self, origin: &Origin) -> CResult<Vec<vet::Review<'_>>> {
        match origin {
            Origin::CratesIo(name) => {
                let all = self.vet.get_or_try_init::<_, CError>(blocking::awatch("vet", async move {
                    let v = creviews::vet::MiniVet::new();
                    Box::pin(async move {
                        let mut sources = v.fetch_registry_from_url("https://raw.githubusercontent.com/bholley/cargo-vet/main/registry.toml").await?;
                        let mut debian = v.fetch_registry(creviews::vet::Registry {
                            registry: [
                                ("debian".into(), creviews::vet::AuditsUrl { url: vec!["https://raw.githubusercontent.com/kornelski/crev-proofs/HEAD/debian.toml".into()] }),
                                ("guix".into(), creviews::vet::AuditsUrl { url: vec!["https://raw.githubusercontent.com/kornelski/crev-proofs/HEAD/guix.toml".into()] }),
                            ].into_iter().collect(),
                        }).await?;
                        sources.all.append(&mut debian.all);
                        let sources = blocking::block_in_place("vetf", || {
                            sources.normalized(&|id| {
                                if let Ok(Some(gh_id)) = self.crates_io_ids_to_github_mapping.get(&(id as u32)) {
                                    if let Ok(login) = self.user_db.login_by_github_id(gh_id.into()) {
                                        return Some(format!("\"{login}\" (https://github.com/{login})"));
                                    }
                                }
                                Some(format!("crates-io user id {id}"))
                            })
                        });
                        Ok(sources)
                    }).await
                })).await?;
                Ok(all.for_crate(name))
            },
            _ => Ok(vec![]),
        }
    }

    #[tracing::instrument(level="debug",skip(self))]
    pub fn reviews_for_crate(&self, origin: &Origin) -> Vec<creviews::Review> {
        match origin {
            Origin::CratesIo(name) => self.crev.reviews_for_crate(name).unwrap_or_default(),
            _ => vec![],
        }
    }

    /// Rustsec reviews
    #[inline(never)]
    #[tracing::instrument(level="debug",skip(self))]
    pub fn advisories_for_crate(&self, origin: &Origin) -> Result<Vec<creviews::security::Advisory>, KitchenSinkErr> {
        match origin {
            Origin::CratesIo(name) => {
                let mut r = blocking::watch("rustsecread", || self.rustsec.try_read_for(Duration::from_secs(4)))
                    .ok_or(KitchenSinkErr::TimedOut("gix has locked up: https://github.com/Byron/gitoxide/issues/1026".into(), 4))?;
                if r.is_none() {
                    drop(r);
                    let mut w = blocking::watch("rustseclock", || self.rustsec.try_write_for(Duration::from_secs(10)))
                        .ok_or_else(|| {
                            error!("Rustsec is locked up");
                            KitchenSinkErr::TimedOut("gix has locked up: https://github.com/Byron/gitoxide/issues/1026".into(), 10)
                        })?;
                    if w.is_none() {
                        let res = blocking::watch("rustsecdb", || creviews::security::RustSec::new(&self.data_path));
                        *w = Some(res.map_err(|e| {
                            error!("Rustsec failed to load: {e}");
                            Arc::from(e)
                        })?);
                    }
                    r = RwLockWriteGuard::downgrade(w);
                }
                Ok(r.as_ref().unwrap().advisories_for_crate(name).into_iter().cloned().collect())
            },
            _ => Ok(vec![]),
        }
    }

    /// (latest, pop)
    /// 0 = not used
    /// 1 = everyone uses it
    #[tracing::instrument(level="debug", skip(self, stats))]
    #[inline(never)]
    pub async fn version_popularity(&self, origin: &Origin, requirement: &VersionReq, stats: &DepsStats) -> CResult<Option<VersionPopularity>> {
        let Origin::CratesIo(crate_lowercase) = origin else { return Ok(None) };

        let mut lost_popularity = false;
        let (matches_latest, mut pop) = if is_deprecated_requirement(crate_lowercase, requirement) {
            (false, 0.)
        } else {
            let index = self.index()?;
            let crates = index.crates_io_crates()?;
            let krate = crates.get_res(crate_lowercase)?;
            match stats.version_popularity_raw(krate, requirement)? {
                Some((m, pop)) => {
                    if is_bad_dependency(krate.name(), requirement) {
                        (m, 0.)
                    } else {
                        (m, pop)
                    }
                },
                None => return Ok(None),
            }
        };

        if let Some(s) = self.traction_stats(origin, stats).await? {
            if s.former_glory < 0.5 {
                lost_popularity = true;
            }
            pop *= s.former_glory as f32;
        }
        let deprecated = if self.is_marked_deprecated(origin) {
            pop *= 0.5;
            true
        } else {
            false
        };
        Ok(Some(VersionPopularity { pop, matches_latest, lost_popularity, deprecated }))
    }

    #[inline]
    pub async fn crate_all_owners(&self) -> CResult<Vec<CrateOwnerStat>> {
        Ok(self.crate_db.crate_all_current_owners().await?)
    }

    /// "See also"
    #[inline]
    #[tracing::instrument(level="debug",skip(self))]
    pub async fn related_categories(&self, slug: &str) -> CResult<Vec<String>> {
        Ok(self.crate_db.related_categories(slug).await?)
    }

    /// Recommendations for similar and related crates
    #[tracing::instrument(level="debug",skip_all)]
    #[inline(never)]
    pub async fn related_crates(&self, krate: &RichCrateVersion, min_recent_downloads: u32, other_related: Vec<(Origin, f32)>) -> CResult<(Vec<Origin>, Vec<ArcRichCrateVersion>)> {
        let deadline_at = Instant::now() + Duration::from_secs(4);
        let (mut same_namespace, replacements, keyword_related, has_verified_repo) = futures::try_join!(
            self.related_namespace_crates(krate, deadline_at),
            self.crate_db.replacement_crates(krate.short_name()).map_err(From::from),
            self.crate_db.related_crates(krate.origin(), min_recent_downloads).map_err(From::from),
            self.has_verified_repository_link(krate).map_err(From::from),
        )?;

        // 1+s, because multiple sources agreeing is more important than crate rank
        let mut scored: HashMap<&Origin, f32> = keyword_related.iter().map(|(o, s)| (o, 1. + s)).collect();

        let in_repo;
        if !has_verified_repo {
            // it's a fork, find originals
            if let Some(repo) = krate.repository() {
                in_repo = self.crate_db.crates_in_repo(repo).await?;

                for (i, o) in in_repo.iter().enumerate() {
                    let rank = self.crate_db.crate_rank(o).await.unwrap_or(0.) as f32;
                    *scored.entry(o).or_insert(0.) += 1. + rank * (3. / (3+i) as f32);
                }
            }
        }

        for (i, o) in replacements.iter().enumerate() {
            let rank = self.crate_db.crate_rank(o).await.unwrap_or(0.) as f32;
            *scored.entry(o).or_insert(0.) += 1. + rank * (3. / (3+i) as f32);
        }

        let other_related_scale = other_related.get(0).map(|&(_, s)| s).unwrap_or(0.).max(1.);
        for (o, s) in &other_related {
            *scored.entry(o).or_insert(0.) += 1. + s / other_related_scale;
        }

        scored.remove(krate.origin());
        for k in &same_namespace {
            scored.remove(k.origin());
        }

        let mut scored: Vec<(&Origin, f32)> = scored.into_iter().collect();
        pick_top_n_unstable_by(&mut scored, 25, |a,b| b.1.total_cmp(&a.1));

        // -1 drops crates from single source
        let best_ranking = scored.get(0).map(|&(_, score)| score - 1.).unwrap_or(0.);
        let cut_off = (0.3f32).min(best_ranking / 5.);
        let mut see_also: Vec<_> = scored.into_iter()
            .filter(move |&(_, score)| score >= cut_off)
            .map(|(o, s)| (o.to_owned(), s))
            .collect();

        // there may still be related among similar, and may not even share name prefix (e.g. rand + getrandom)
        let see_also_related = self.filter_namespace_related_origins(krate, see_also.clone(), deadline_at).await;
        for r in see_also_related {
            if let Some(pos) = see_also.iter().position(|(o, _)| o == r.origin()) {
                see_also.remove(pos);
                if same_namespace.len() < 20 { same_namespace.insert(0, r); }
            }
        }

        let see_also = see_also.into_iter().take(11).map(|(o, _)| o).collect();

        Ok((see_also, same_namespace))
    }

    #[tracing::instrument(level="debug",skip_all)]
    async fn related_namespace_crates(&self, krate: &RichCrateVersion, deadline_at: Instant) -> CResult<Vec<ArcRichCrateVersion>> {
        let repo_crates = if let Some(repo) = krate.repository() {
            self.crate_db.crates_in_repo(repo).await?
        } else {
            vec![]
        };

        let dedup: HashSet<_> = repo_crates.iter().chain(Some(krate.origin())).collect();
        let valid_ns_prefixes: HashSet<_> = dedup.iter()
            .map(|o| crate_name_namespace_prefix(o.package_name_icase()))
            .collect();

        let mut candidates = Vec::with_capacity(32);
        let mut dedup2 = HashSet::new();

        let owners = self.crate_owners(krate.origin(), CrateOwners::Strict).await?;
        for github_id in owners.iter()
            .filter(|o| self.crates_io_login_on_blocklist(&o.crates_io_login).is_none())
            .filter_map(|o| o.github_id)
            .take(20) {
            let crates = self.crate_db.all_crates_of_github_id(github_id).await?;
            candidates.extend(crates.into_iter().take(1000)
                .filter(|c| c.deleted_at.is_none())
                .filter(|c| !dedup.contains(&c.origin))
                .filter(|c| {
                    let name = c.origin.package_name_icase();
                    let suffix = name.rsplit(|c:char| c == '_' || c == '-').next().unwrap();

                    suffix != "internal" && suffix != "internals" &&
                        valid_ns_prefixes.get(crate_name_namespace_prefix(name)).is_some()
                })
                .filter(|c| dedup2.insert(c.origin.clone()))
                .map(|c| {
                    // move shorter (root) crate names to the beginning
                    let hyphens = (1 + c.origin.package_name_icase().bytes().filter(|&b| b == b'_' || b == b'-').count()) as f32;
                    (c.origin, c.crate_ranking / hyphens, c.crate_ranking)
                })
                .take(30));
        }
        // Kill crates like serde_macros that is dead
        let best_ranking = candidates.iter().map(|&(_,_,ranking)| ranking).max_by(|a,b| a.total_cmp(b)).unwrap_or(0.);
        let cut_off = (0.2f32).min(best_ranking / 3.);
        candidates.retain(|&(_, _, ranking)| ranking >= cut_off);

        pick_top_n_unstable_by(&mut candidates, 20, |a, b| b.1.total_cmp(&a.1)); // needs stable sort to preserve original ranking

        let origins: Vec<_> = repo_crates.into_iter().map(|o| (o,1.))
            .chain(candidates.into_iter().map(|(a,_,c)| (a,c))).collect();
        Ok(self.filter_namespace_related_origins(krate, origins, deadline_at).await)
    }

    #[inline(never)]
    async fn filter_namespace_related_origins<'a>(&self, krate: &RichCrateVersion, origins: Vec<(Origin, f32)>, deadline_at: Instant) -> Vec<ArcRichCrateVersion> {
        let iter = origins.into_iter()
        .filter(|(o, _)| o != krate.origin())
        .map(|(origin, ranking)| async move {
            let other = deadline("rel-ns", deadline_at, self.rich_crate_version_stale_is_ok(&origin)).await.ok()?;
            if other.is_yanked() {
                return None;
            }
            Box::pin(self.are_namespace_related(krate, &other)).await.ok()?.map(|related| (other, ranking * related))
        });
        let mut tmp = futures::stream::iter(iter)
            .buffered(5)
            .filter_map(|res| async move { res })
            .take(15)
            .collect::<Vec<_>>()
            .await;
        tmp.sort_unstable_by(|a,b| b.1.total_cmp(&a.1));
        tmp.into_iter().map(|(k,_)| k).collect()
    }

    /// Returns (nth, slug)
    #[tracing::instrument(level="debug",skip_all)]
    #[inline(never)]
    pub async fn top_category(&self, krate: &RichCrateVersion, rend: &RendCtx) -> Option<(u32, Box<str>)> {
        let crate_origin = krate.origin();
        let cats = blocking::awatch("tc1", join_all(krate.category_slugs().iter().map(|slug| async move {
            let c = timeout("top category", 6, self.top_crates_in_category(slug, rend)).await?;
            Ok::<_, CError>((c, slug))
        }))).await;
        cats.into_iter().filter_map(|cats| match cats {
            Ok(c) => Some(c),
            Err(_) => {
                rend.set_incomplete();
                None
            }
        }).filter_map(|(cat, slug)| {
            cat.iter().position(|o| o == crate_origin).map(|pos| {
                (pos as u32 +1, slug)
            })
        })
        .min_by_key(|a| a.0)
        .map(|(n, s)| (n, (**s).into()))
    }

    /// Returns (nth, keyword)
    #[inline]
    #[tracing::instrument(level="debug",skip_all)]
    pub async fn top_keyword(&self, krate: &RichCrate) -> CResult<Option<(u32, String)>> {
        Ok(self.crate_db.top_keyword(krate.origin()).await?)
    }

    /// Maintenance: add user to local db index
    #[tracing::instrument(level="debug",skip_all)]
    #[inline(never)]
    pub(crate) async fn index_user_m(&self, orig_user: &MinimalUser, commit: &GitCommitAuthor, fetch_time: SystemTime) -> CResult<()> {
        running()?;
        let user = self.gh.user_by_login(&orig_user.login).await?.ok_or_else(|| KitchenSinkErr::AuthorNotFound(orig_user.login.clone()))?;
        if !self.user_db.email_has_github(&commit.email)? {
            info!("{} => {}", commit.email, user.login);
            self.user_db.index_user(&user, Some(&commit.email), commit.name.as_deref(), fetch_time, true)?;
        }
        Ok(())
    }

    /// Maintenance: add user to local db index
    #[tracing::instrument(level="debug",skip_all)]
    #[inline(never)]
    pub fn index_user(&self, user: &User, commit: &GitCommitAuthor, fetch_time: SystemTime) -> CResult<()> {
        running()?;
        if !self.user_db.email_has_github(&commit.email)? {
            info!("{} => {}", commit.email, user.login);
            self.user_db.index_user(user, Some(&commit.email), commit.name.as_deref(), fetch_time, true)?;
        }
        Ok(())
    }

    /// Maintenance: add user to local db index
    #[tracing::instrument(level="debug",skip_all)]
    #[inline(never)]
    pub async fn index_email(&self, email: &str, name: Option<&str>, fetch_time: SystemTime) -> CResult<()> {
        running()?;
        if !self.user_db.email_has_github(email)? {
            match self.gh.user_by_email(email).await {
                Ok(Some(users)) => {
                    for user in users {
                        info!("{login} == {email} ({name:?})", login = user.login);
                        self.user_db.index_user(&user, Some(email), name, fetch_time, true)?;
                    }
                },
                Ok(None) => warn!("{email} not found on github"),
                Err(e) => error!("•••• {e}"),
            }
        }
        Ok(())
    }

    #[tracing::instrument(level="debug",skip_all)]
    pub async fn index_owner(&self, owner: &CrateOwner, crates: &[CrateOwnerRow], score: f64) -> CResult<()> {
        assert!(score.is_finite());
        let Some(github_id) = owner.github_id else { bail!("owner without github ID!? {owner:?}") };
        let Some(login) = owner.github_login() else { bail!("bad login") };

        let (crate_weights, dependency_weights, coauthors_weights) = Box::pin(self.relative_trust_in_coauthors_and_crates(crates, login, github_id)).await?;

        self.user_db.set_user_crate_relevance(github_id, &crate_weights)?;
        self.user_db.set_user_dependency_trust(github_id, &dependency_weights)?;
        self.user_db.set_coauthor_trust(github_id, &coauthors_weights)?;
        self.user_db.set_owner_ranking_score(github_id, score)?;
        Ok(())
    }

    // TODO: move it elsewhere
    #[tracing::instrument(level="debug",skip(self))]
    #[inline(never)]
    pub async fn relative_trust_in_coauthors_and_crates(&self, crates: &[CrateOwnerRow], login: &str, github_id: u32) -> CResult<(HashMap<Origin, f64>, HashMap<SmolStr, f64>, HashMap<u32, f64>)> {
        let mut dependency_weights = HashMap::with_capacity(crates.len() * 2);
        let mut crate_weights = HashMap::with_capacity(crates.len());
        let mut crate_weights_total = 0.;
        let mut all_coauthors_weights = HashMap::new();
        let now = Utc::now();
        for &CrateOwnerRow { ref origin, latest_release, crate_ranking, .. } in crates {
            // swallow errors, because active owners may still own some old broken crates
            let Ok(k) = self.rich_crate_version_stale_is_ok(origin).await else { continue; };

            if k.is_yanked() || k.is_hidden() || k.is_spam() || k.maintenance() == MaintenanceStatus::Deprecated {
                continue;
            }

            let (downloads, contrib) = futures::join!(self.downloads_per_month(origin), self.all_contributors(&k));
            let downloads = downloads?.unwrap_or(1000);
            let (authors, _teams) = contrib?;

            let mut total_contribution = 0.;
            let mut owners_contribution = 0.;

            // it's crate's age, unless original owner has been kicked out
            // but it's fine to scale to oldest existing owner, since ages are to filter out noobs
            let oldest_owner_age = authors.values().filter_map(|a| a.invited_at).min()
                .map(|d| now.signed_duration_since(d).num_days().max(1)).unwrap_or(1) as f64;
            debug_assert!(oldest_owner_age > 0.);

            let mut coauthors_weights = Vec::with_capacity(authors.len());
            for a in authors.values() {
                // it's not limited to owners, because forks should not assume that owner == author

                let ownership_length_frac = a.invited_at.map(|d| now.signed_duration_since(d).num_days().max(0))
                    .unwrap_or(0) as f64 / oldest_owner_age;
                debug_assert!(ownership_length_frac.is_finite());

                debug_assert!(a.contribution.is_finite());
                let relevance_weight = (10. + a.contribution)
                    * if a.not_a_person { 0.01 } else { 1. }
                    * if a.owner { 1. } else { 0.05 } // previous authors of forked code are not necessarily trusted
                    * (1. + ownership_length_frac)
                    // this weight will be used for page-rank-like trust, so an explicit invitation is an important trust signal
                    * if a.invited_by_github_id == Some(github_id) || a.github.as_ref().map(|g| g.id) == Some(github_id) { 3. } else { 1. };

                total_contribution += relevance_weight;
                if a.github.as_ref().map_or(false, |g| g.id == github_id) {
                    owners_contribution += relevance_weight;
                }

                if let Some(a_id) = a.github.as_ref().map(|g| g.id) {
                    coauthors_weights.push((relevance_weight, a_id));
                }
            }

            // how much this user cares about this crate, since if they're not involved much,
            // then we can't assume that crate's dependencies etc are their choice and endoresement.
            let contrib_fraction = owners_contribution / total_contribution.max(1.);

            let crate_age = now.signed_duration_since(latest_release).num_days().max(0) as f64;

            // - smooth the contrib fraction (assume that owning always counts for something, even if we don't see the commits)
            // - make older releases count less, as they're probably abandoned
            let crate_importance_weight = (0.1 + contrib_fraction) / (crate_age + 365.)
                // output of this is used in crate ranking, so it could get circular, so dampen that too
                * ( 1. + crate_ranking as f64 )
                // but kick known-bad crates hard (deprecated, yanked, spam, etc. get very low scores)
                * if crate_ranking > 0.25 { 1. } else { 0.2 }
                // with big downloads comes big responsibility
                * ((10000 + downloads) as f64).log10();
            crate_weights.insert(origin.clone(), crate_importance_weight);
            crate_weights_total += crate_importance_weight;
            debug_assert!(crate_importance_weight.is_finite());

            for (coauthors_weight, github_id) in coauthors_weights {
                *all_coauthors_weights.entry(github_id).or_insert(0.) += coauthors_weight * crate_importance_weight;
            }

            let (normal, build, dev) = k.direct_dependencies();
            let deps = normal.into_iter().map(|n| (n, 1.))
                .chain(build.into_iter().map(|b| (b, 1.)))
                .chain(dev.into_iter().map(|d| (d, 0.1))); // dev deps aren't used by users
            for (dep, base_dep_weight) in deps {
                if !dep.dep.is_crates_io() {
                    continue;
                }
                let dep_weight = crate_importance_weight * base_dep_weight
                    * if dep.is_optional() { 0.5 } else { 1. }
                    * if !dep.only_for_targets.is_empty() { 0.9 } else { 1. };
                *dependency_weights.entry(dep.package).or_insert(0.) += dep_weight;
            }
        }

        // normalize to add up to 1
        if crate_weights_total > 0. {
            crate_weights.values_mut().for_each(|w| *w /= crate_weights_total);
        }

        // 1+ so that if there's only one dependency used, it doesn't dump all of the trust into it
        let dependency_weights_total = 1. + dependency_weights.values().sum::<f64>();
        dependency_weights.values_mut().for_each(|w| *w /= dependency_weights_total);

        // this intentionally keeps the self owner, because if they rarely collaborate and only trust themselves,
        // that's also an important signal (otherwise the first tiny coowner would get all the trust).
        let coauthors_weights_total = all_coauthors_weights.values().sum::<f64>();
        if coauthors_weights_total > 0. {
            all_coauthors_weights.values_mut().for_each(|w| *w /= coauthors_weights_total);
        }

        if !crate_weights.is_empty() {
            // interprets following someone on github as web-of-trust
            if let Ok(Some(following)) = Box::pin(self.gh.user_following(login)).await.map_err(|e| error!("{login} f: {e}")) {
                for (i, f) in following.iter().enumerate() {
                    // github sorts follows starting from latest, so assume more recent is a bit better
                    *all_coauthors_weights.entry(f.id).or_insert(0.) += 0.1 * 100. / (100 + i) as f64;
                }

                // renormalize
                let coauthors_weights_total = all_coauthors_weights.values().sum::<f64>();
                if coauthors_weights_total > 0. {
                    all_coauthors_weights.values_mut().for_each(|w| *w /= coauthors_weights_total);
                }
            }
        }
        // remove self now, after weights were adjusted for it (makes sql eaiser later)
        all_coauthors_weights.remove(&github_id);

        Ok((crate_weights, dependency_weights, all_coauthors_weights))
    }

    /// Maintenance: add crate to local db index
    #[tracing::instrument(level="debug",skip_all)]
    pub async fn index_crate(&self, k: &RichCrate, score: f64) -> CResult<()> {
        running()?;
        self.crate_db.index_versions(k, score, self.downloads_per_month(k.origin()).await?).await?;
        Ok(())
    }

    #[inline(never)]
    pub fn index_crate_downloads(&self, crates_io_name: &str, by_ver: &HashMap<&str, &[(NaiveDate, u32, bool)]>) -> CResult<()> {
        running()?;
        let mut year_data = HashMap::new();
        for (&version, &date_dls) in by_ver {
            let version = match MiniVer::try_from(version) {
                Ok(v) => v,
                Err(e) => {
                    warn!("Bad version: {} {} {}", crates_io_name, version, e);
                    continue;
                }
            };
            for (day, dls, overwrite) in date_dls.iter() {
                let curr_year = day.year() as u16;
                let curr_year_data = match year_data.entry(curr_year) {
                    Vacant(e) => {
                        e.insert((false, self.yearly.get_crate_year(crates_io_name, curr_year)?.unwrap_or_default()))
                    },
                    Occupied(e) => e.into_mut(),
                };

                let day_of_year = day.ordinal0() as usize;
                let year_dls = curr_year_data.1.entry(version.clone()).or_insert_with(Default::default);
                if year_dls.0[day_of_year] < *dls || *overwrite {
                    curr_year_data.0 = true;
                    year_dls.0[day_of_year] = *dls;
                }
            }
        }
        for (curr_year, (modified, curr_year_data)) in year_data {
            if modified {
                self.yearly.set_crate_year(crates_io_name, curr_year, &curr_year_data)?;
            }
        }
        Ok(())
    }

    pub fn update_deprecation_status(&self, origin: &Origin, reason: Option<&str>) {
        if let Some(reason) = reason {
            let _ = self.is_deprecated_crate.set(origin.to_str(), reason.to_string())
                .map_err(|e| error!("depr: {e}"));
        } else {
            let _ = self.is_deprecated_crate.delete(origin.to_str().as_str())
                .map_err(|e| error!("undepr: {e}"));
        }
    }

    pub fn is_marked_deprecated(&self, origin: &Origin) -> bool {
        if let Ok(Some(reason)) = self.is_deprecated_crate.get(origin.to_str().as_str()) {
            debug!("{origin:?} is deprecated because {reason}");
            true
        } else {
            false
        }
    }

    pub fn is_marked_deprecated_reason(&self, origin: &Origin) -> Option<String> {
        self.is_deprecated_crate.get(origin.to_str().as_str()).ok().flatten()
    }

    #[tracing::instrument(level="debug",skip(self))]
    #[inline(never)]
    pub async fn index_crate_highest_version(&self, origin: &Origin, maintenance_only_reindexing: bool) -> CResult<()> {
        running()?;
        info!("Indexing {:?}", origin);

        let index = self.index()?;
        if !index.crate_exists(origin) {
            let _ = self.crate_db.delete_crate(origin).await;
            debug!("reindexing non-existent create; purged {origin:?}");
            return Err(KitchenSinkErr::CrateNotFound(origin.clone()).into());
        }

        timeout("before-index", 2, self.crate_db.before_index_latest(origin).map_err(anyhow::Error::from)).await?;

        let (source_data, mut manifest, mut warnings, cache_key) = self.fetch_rich_crate_version_data(origin).await?;

        let _ = block_in_place(format!("msrv {origin:?}"), || self.index_msrv_from_manifest(origin, &manifest))
            .map_err(|e| error!("msrv {}", e));

        // direct deps are used as extra keywords for similarity matching,
        // but we're taking only niche deps to group similar niche crates together
        let raw_deps_stats = index.deps_stats().map_err(KitchenSinkErr::Deps)?;
        let mut weighed_deps = Vec::<(&str, f32)>::new();
        let all_deps = manifest.direct_dependencies();
        let all_deps = [(all_deps.0.as_slice(), 1.0), (all_deps.2.as_slice(), 0.33)];
        // runtime and (lesser) build-time deps
        for &(deps, overall_weight) in &all_deps {
            for dep in deps {
                if let Some(rev) = raw_deps_stats.counts.get(&*dep.package) {
                    let right_popularity = rev.counters.direct.all() > 2 && rev.counters.direct.all() < 200 && rev.counters.runtime.def < 500 && rev.counters.runtime.opt < 800;
                    if categories::dep_interesting_for_index(&dep.package).unwrap_or(right_popularity) {
                        let weight = overall_weight / (1 + rev.counters.direct.all()) as f32;
                        weighed_deps.push((&dep.package, weight));
                    }
                }
            }
        }
        let (is_build, is_dev) = self.is_build_or_dev(origin)?;

        let mut keywords_from_categories = Vec::new();
        Self::override_bad_categories(&mut manifest, &mut keywords_from_categories);

        let package = manifest.package();
        let repository = package.repository().and_then(|r| Repo::new(r).ok());
        let authors = package.authors().iter().map(|a| Author::new(a)).collect::<Vec<_>>();

        let mut bad_categories = Vec::new();
        let mut category_slugs = categories::Categories::fixed_category_slugs(package.categories(), &mut bad_categories);
        for c in &bad_categories {
            // categories invalid for lib.rs may still be valid for crates.io (they've drifted over time)
            if is_valid_crates_io_category_not_on_lib_rs(c) {
                continue;
            }
            warnings.insert(Warning::BadCategory(c.as_str().into()));
        }

        if category_slugs.is_empty() && bad_categories.is_empty() {
            warnings.insert(Warning::NoCategories);
        }

        if package.keywords().is_empty() {
            warnings.insert(Warning::NoKeywords);
        }

        let mut hidden = false;
        let mut spam = false;
        if let Some(overrides) = self.category_and_spam_overrides.get(origin.package_name_icase()) {
            category_slugs = overrides.iter().filter_map(|k| {
                if k == "hidden" {
                    hidden = true;
                    return None;
                } else if k == "spam" {
                    spam = true;
                    return None;
                }
                Some((**k).into())
            }).collect();
        }

        category_slugs.iter().for_each(|k| debug_assert!(categories::CATEGORIES.from_slug(k).1, "'{k}' must exist"));
        bad_categories.append(&mut keywords_from_categories);

        let renderer = render_readme::Renderer::new(None);

        let mut readme_text = source_data.readme.as_ref().map(|r| renderer.visible_text_by_section(&r.markup)).unwrap_or_default();
        let mut lib_text = source_data.lib_file.as_ref().map(|rs| {
            let lines = rs.lines().filter_map(|l| l.strip_prefix("//!")).join("\n");
            renderer.visible_text_by_section(&Markup::Markdown(lines))
        }).unwrap_or_default();
        readme_text.append(&mut lib_text);

        let ranked_phrases = self.extract_ranked_phrases(&manifest, &readme_text, source_data.github_description.as_deref(), &source_data.text_from_source_code)?;
        let extracted_auto_keywords = feat_extractor::auto_keywords(&ranked_phrases);

        let db_index = self.crate_db.index_latest(CrateVersionData {
            cache_key,
            category_slugs: &category_slugs,
            bad_categories: &bad_categories,
            authors: &authors,
            origin,
            repository: repository.as_ref(),
            deps_stats: &weighed_deps,
            is_build, is_dev,
            // there's a better function for this, but it uses already-processed crate
            is_spam_or_hidden: spam || hidden || feat_extractor::is_squatspam_partial(manifest.package().version(), manifest.package().description()).is_some(),
            manifest: &manifest,
            source_data: &source_data,
            extracted_auto_keywords: &extracted_auto_keywords,
            ranked_phrases: &ranked_phrases,
        });
        let d = timeout("db-index", 16, db_index.map_err(anyhow::Error::from)).await?;

        for w in &warnings {
            debug!("{}: {}", package.name, w);
        }

        let crate_version = manifest.package().version().to_string();
        let cached = CachedCrate {
            manifest,
            cache_key,
            derived: rich_crate::Derived {
                hidden,
                spam,
                categories: d.categories,
                keywords: d.keywords,
                path_in_repo: source_data.path_in_repo,
                vcs_info_git_sha1: source_data.vcs_info_git_sha1,
                language_stats: source_data.language_stats,
                crate_compressed_size: source_data.crate_compressed_size,
                crate_decompressed_size: source_data.crate_decompressed_size,
                is_nightly: source_data.is_nightly,
                capitalized_name: source_data.capitalized_name,
                readme: source_data.readme,
                lib_file: source_data.lib_file,
                bin_file: source_data.bin_file,
                has_buildrs: source_data.has_buildrs,
                has_code_of_conduct: source_data.has_code_of_conduct,
                is_yanked: source_data.is_yanked,
                readme_was_missing: source_data.readme_was_missing,
                canonical_http_url_sha1_broken: source_data.canonical_http_url_sha1_broken,
                features_comments: source_data.features_comments,
                features_enable_items: source_data.features_enable_items,
            },
            warnings,
        };
        let origin_str = &*origin.to_str();
        self.derived_storage.set_serialize((origin_str, ""), &cached)?;
        self.extra_derived_storage.set_serialize((origin_str, "1"), &source_data.text_from_source_code)?;
        if !source_data.suspicious_binary_files.is_empty() {
            self.extra_derived_storage.set_serialize((&format!("{origin_str}!bins"), &crate_version), &source_data.suspicious_binary_files)?;
        }

        if !maintenance_only_reindexing {
            self.event_log.post(&SharedEvent::CrateIndexed(origin.to_str()))?;
        }
        Ok(())
    }

    /// path -> type
    pub fn get_suspicious_binary_files(&self, origin: &Origin, crate_version: &str) -> KResult<Vec<IdentifiedBinFile>> {
        let origin_str = &*origin.to_str();
        if let Some(mut res) = self.extra_derived_storage.get_deserialized::<Vec<IdentifiedBinFile>>((&format!("{origin_str}!bins"), crate_version))? {
            if res.len() > 1 {
                let biggest = res.iter().enumerate().max_by_key(|(_, b)| b.size).map(|(i, _)| i).unwrap_or(0);
                if biggest > 0 {
                    res.swap(0, biggest);
                }
                // Big ones to the front, but also look sorted by path
                res[1..].sort_unstable_by(|a,b| (b.size.max(1<<14).next_power_of_two()).cmp(&(a.size.max(1<<14).next_power_of_two())).then_with(|| a.path.cmp(&b.path)));
            }
            return Ok(res);
        }
        // temporary fallback to previous key
        Ok(self.extra_derived_storage.get_deserialized((&format!("{origin_str}-bins"), crate_version))?.unwrap_or_default())
    }

    fn extract_ranked_phrases(&self, manifest: &Manifest, readme_by_section: &[(String, String)], github_description: Option<&str>, text_from_source_code: &TextFromSourceCode) -> std::io::Result<Vec<(f32, String)>> {
        let keywords_from_source_code;
        let mut readme_sections = feat_extractor::filter_sections_by_relevance(manifest, readme_by_section);

        let num_source_code_keywords = text_from_source_code.idents.len();
        if num_source_code_keywords > 0 {
            let preferred_size = (num_source_code_keywords/4).max(25).min(150);
            let chunk_size = (num_source_code_keywords + preferred_size - 1) / preferred_size;
            keywords_from_source_code = text_from_source_code.idents.chunks(chunk_size).map(|chunk| {
                let avg = (chunk.iter().map(|&(w, _)| w as f64).sum::<f64>() / chunk.len() as f64) as f32;
                let mut chunk_as_text = String::with_capacity(chunk.len()*8);
                chunk_as_text.extend(Itertools::intersperse(chunk.iter().map(|(_, k)| k.as_str()), " "));
                (avg, chunk_as_text)
            }).collect_vec();
            readme_sections.extend(keywords_from_source_code.iter().map(|(w, k)| (*w, k.as_str())));
        }

        for (w, text) in &text_from_source_code.doc {
            let w = *w as f32 / 24.;
            readme_sections.push((w.min(0.4) + (w/16.), text));
        }
        let other_text = feat_extractor::additional_keywords_for_crate(&manifest.package().name, self.main_data_dir())?;
        if !other_text.is_empty() {
            readme_sections.push((0.15, &other_text));
        }
        Ok(feat_extractor::extract_and_normalize_text_phrases(manifest, github_description, &readme_sections))
    }

    #[tracing::instrument(level="debug",skip(self))]
    async fn fetch_rich_crate_version_data(&self, origin: &Origin) -> CResult<(CrateVersionSourceData, Manifest, HashSet<Warning>, u64)> {
        let ((source_data, manifest, warnings), cache_key) = match origin {
            Origin::CratesIo(ref name) => {
                let (name, version, is_yanked, cache_key) = {
                    let index = self.index()?;
                    let crates = index.crates_io_crates()?;
                    let cache_key = index.cache_key_for_crates_io_index_state(name)?;
                    let ver = crates.highest_version_by_lowercase_name(name, false).context("rich_crate_version2")?;
                    let name = ver.name().to_string();
                    let version = ver.version().to_string();
                    let is_yanked = ver.is_yanked();
                    (name, version, is_yanked, cache_key)
                };
                let res = watch("reindexing-cio-data", self.rich_crate_version_data_from_crates_io(&name, &version, is_yanked)).await.context("rich_crate_version_data_from_crates_io")?;
                (res, cache_key)
            },
            Origin::GitHub { .. } | Origin::GitLab { .. } => {
                if !self.crate_exists(origin) {
                    let _ = self.crate_db.delete_crate(origin).await;
                    return Err(KitchenSinkErr::GitCrateNotAllowed(origin.clone()).into())
                }
                let res = blocking::awatch(&format!("reindexing-repo-{:?}", origin), self.rich_crate_version_from_repo(origin)).await?;
                (res, 0)
            },
        };
        Ok((source_data, manifest, warnings, cache_key))
    }

    #[inline(never)]
    fn capitalized_name<'a>(name: &str, source_sentences: impl Iterator<Item = &'a str>) -> String {
        let mut ch = name.chars();
        let mut first_capital = String::with_capacity(name.len());
        first_capital.extend(ch.next().unwrap().to_uppercase());
        first_capital.extend(ch.map(|c| if c == '_' { ' ' } else { c }));

        let mut words = HashMap::with_capacity(100);
        let lcname = name.as_ascii_lowercase(); let lcname = &*lcname;
        let shouty = name.as_ascii_uppercase(); let shouty = &*shouty;
        for s in source_sentences {
            for s in s.split(|c: char| !c.is_ascii_alphanumeric() && c != '-' && c != '_').filter(|&w| w != lcname && crate_name_fuzzy_eq(w, lcname)) {
                let mut points = 2;
                if lcname.len() > 2 {
                    if s[1..] != lcname[1..] {
                        points += 1;
                    }
                    if s != first_capital && s != shouty {
                        points += 1;
                    }
                }
                if let Some(count) = words.get_mut(s) {
                    *count += points;
                    continue;
                }
                words.insert(s.to_string(), points);
            }
        }

        let can_capitalize = words.get(&first_capital).is_some();
        if let Some((name, _)) = words.into_iter().max_by_key(|&(_, v)| v) {
            name
        } else if can_capitalize {
            first_capital
        } else {
            name.to_owned()
        }
    }

    #[tracing::instrument(level="debug",skip_all)]
    pub async fn inspect_repo_manifests(&self, repo: &Repo) -> CResult<Vec<FoundManifest>> {
        let checkout = self.checkout_repo(repo.clone(), true).await?;
        let (_g, stop) = Stop::new();
        let (has, _) = blocking::awatch(format!("checkout {repo:?}"),
            spawn_blocking(move || crate_git_checkout::find_manifests(&checkout, &stop))).await??;
        Ok(has)
    }

    #[tracing::instrument(level="debug",skip_all)]
    async fn checkout_repo(&self, repo: Repo, shallow: bool) -> Result<crate_git_checkout::Repository, KitchenSinkErr> {
        running()?;

        let repo_url = repo.canonical_git_url();
        let git_checkout_path = self.git_checkout_path.clone();
        timeout("checkout", 300, spawn_blocking(move || {
            crate_git_checkout::checkout(&repo, &git_checkout_path, shallow)
                .map_err(|e| KitchenSinkErr::GitCheckoutFailed(format!("{} {e}", repo.canonical_git_url())))
        }).map_err(|e| KitchenSinkErr::GitCheckoutFailed(format!("{repo_url} {e}")))).await?
    }

    #[tracing::instrument(level="debug",skip(self))]
    #[inline(never)]
    pub async fn index_repo(&self, repo: &Repo, as_of_version: &str) -> CResult<()> {
        let _f = self.throttle.acquire().await;
        running()?;
        let checkout = self.checkout_repo(repo.clone(), false).await?;
        let url = repo.canonical_git_url();

        let check_cache_key = format!("{url}/{as_of_version}");
        if let Ok(Some(_)) = self.repo_checkout_cache.get(check_cache_key.as_str()) {
            debug!("skipping already indexed {url} for v{as_of_version}");
            return Ok(());
        }

        let (_g, stop) = Stop::new();
        let (checkout, manif) = blocking::awatch(url.clone(), spawn_blocking(move || {
            let (manif, warnings) = crate_git_checkout::find_manifests(&checkout, &stop)
                .with_context(|| format!("find manifests in {url}"))?;
            for warn in warnings {
                warn!("warning: {}", warn.0);
            }
            Ok::<_, CError>((checkout, manif.into_iter().filter_map(|found| {
                let inner_path = found.inner_path;
                let manifest = found.manifest;
                let commit = found.commit;
                manifest.package.map(move |p| (inner_path, p.name, commit.to_string()))
            })))
        })).await??;
        self.crate_db.index_repo_crates(repo, manif).await.context("index rev repo")?;

        if let Repo::GitHub(ref repo) = repo {
            if let Some(commits) = watch("commits", self.repo_commits(repo, as_of_version)).await? {
                let fetch_time = SystemTime::now() - Duration::from_secs(3600*24*60); // cache length of gh.repo_commits
                for c in commits {
                    if let Some(a) = c.author {
                        self.index_user_m(&a, &c.commit.author, fetch_time).await?;
                    }
                    if let Some(a) = c.committer {
                        self.index_user_m(&a, &c.commit.committer, fetch_time).await?;
                    }
                }
            }
        }

        tokio::task::yield_now().await;
        running()?;

        let (_g, stop) = Stop::new();
        let changes = blocking::awatch(&check_cache_key, spawn_blocking({let repo = repo.clone(); move || {
            let mut changes = Vec::new();
            let url = repo.canonical_git_url();
            crate_git_checkout::find_dependency_changes(&checkout, |added, removed, age| {
                if removed.is_empty() {
                    if added.len() > 1 {
                        // Divide evenly between all recommendations
                        // and decay with age, assuming older changes are less relevant now.
                        let weight = (1.0 / added.len().pow(2) as f64) * 30.0 / (30.0 + age as f64);
                        if weight > 0.002 {
                            for dep1 in added.iter().take(8) {
                                for dep2 in added.iter().take(5) {
                                    if dep1 == dep2 {
                                        continue;
                                    }
                                    if !is_alnum(dep1) {
                                        error!("Bad crate name {} in {}", dep1, url);
                                        continue;
                                    }
                                    // Not really a replacement, but a recommendation if A then B
                                    changes.push(RepoChange::Replaced { crate_name: dep1.into(), replacement: dep2.into(), weight })
                                }
                            }
                        }
                    }
                } else {
                    for crate_name in removed {
                        if !is_alnum(&crate_name) {
                            error!("Bad crate name {} in {}", crate_name, url);
                            continue;
                        }

                        if !added.is_empty() {
                            // Assuming relevance falls a bit when big changes are made.
                            // Removals don't decay with age (if we see great comebacks, maybe it should be split by semver-major).
                            let weight = 8.0 / (7.0 + added.len() as f64);
                            for replacement in &added {
                                changes.push(RepoChange::Replaced { crate_name: crate_name.as_str().into(), replacement: replacement.into(), weight })
                            }
                            changes.push(RepoChange::Removed { crate_name: crate_name.into(), weight });
                        } else {
                            // ??? maybe use sliiight recommendation score based on existing (i.e. newer) state of deps in the repo?
                            changes.push(RepoChange::Removed { crate_name: crate_name.into(), weight: 0.95 });
                        }
                    }
                }
            }, &stop).map(|_| changes)
        }})).await??;
        self.crate_db.index_repo_changes(repo, &changes).await?;
        self.repo_checkout_cache.set(check_cache_key, ())?;
        Ok(())
    }

    #[inline]
    #[tracing::instrument(level="debug",skip(self))]
    pub fn login_by_github_id(&self, id: u64) -> CResult<SmolStr> {
        Ok(self.user_db.login_by_github_id(id)?)
    }

    #[inline]
    pub fn user_by_email(&self, email: &str) -> CResult<Option<User>> {
        self.user_db.user_by_email(email).context("user_by_email")
    }

    #[tracing::instrument(level="debug",skip(self))]
    #[inline(never)]
    pub async fn user_by_github_login(&self, wanted_github_login: &str) -> CResult<Option<User>> {
        if wanted_github_login.contains(':') {
            anyhow::bail!("bad login {wanted_github_login}");
        }
        tokio::task::yield_now().await;
        match block_in_place(wanted_github_login, || self.user_db.user_by_github_login(wanted_github_login)) {
            Ok(Some(gh)) => if gh.created_at.is_some() { // unfortunately name is optional and can be None
                return Ok(Some(gh));
            } else {
                debug!("db gh user missing created_at {wanted_github_login}");
            },
            Ok(None) => debug!("gh user cache miss {wanted_github_login}"),
            Err(e) => error!("db user {wanted_github_login}: {e}"),
        };
        let u = Box::pin(self.gh.user_by_login(wanted_github_login)).await
            .map_err(|e| { error!("gh user {wanted_github_login}: {e}"); e })?; // errs on 404
        if let Some(u) = &u {
            block_in_place(wanted_github_login, || {
                if !u.login.eq_ignore_ascii_case(wanted_github_login) {
                    let _ = self.user_db.index_outdated_login(wanted_github_login, u.id.into());
                }
                let _ = self.user_db.index_user(u, None, None, SystemTime::now() - Duration::from_secs(3600*24*7), false) // cache length of gh.user_by_login
                    .map_err(|e| error!("index user {wanted_github_login}: {e} {u:?}"));
            });
        }
        debug!("{wanted_github_login} fetched user {:?}", u);
        Ok(u)
    }

    pub fn rustc_compatibility_no_deps(&self, all: &RichCrate) -> Result<CompatByCrateVersion, KitchenSinkErr> {
        let db = self.build_db()?;
        self.rustc_compatibility_inner_non_recursive(all, &db)
    }

    fn rustc_compatibility_inner_non_recursive(&self, all: &RichCrate, db: &BuildDb) -> Result<CompatByCrateVersion, KitchenSinkErr> {
        let mut c = db.get_compat(all.origin())
            .map_err(|e| error!("bad compat: {}", e))
            .unwrap_or_default();

        // insert versions that aren't in the db, to have the full list.
        // doing so before postproc will copy data to these.
        for ver in all.versions().iter().filter(|v| !v.yanked) {
            let semver: SemVer = match ver.num.parse() {
                Ok(v) => v,
                Err(e) => {
                    error!("bad semver: {} {}", ver.num, e);
                    continue;
                },
            };
            let c = c.entry(semver).or_insert_with(Default::default);

            let created = ver.created_at;
            if let Some(expected_rust) = Self::rustc_release_from_date(&created) {
                c.add_compat(expected_rust, Compat::ProbablyWorks, Some("Assumed from release date".into()));
            }
        }
        // this is needed to copy build failures from non-matching versions to matching versions
        BuildDb::postprocess_compat(&mut c);
        Ok(c)
    }

    fn crate_rustc_compat_get_cached(&self, origin: &Origin) -> Option<CompatByCrateVersion> {
        self.crate_rustc_compat_cache.read().get(origin).cloned()
    }

    fn crate_rustc_compat_set_cached(&self, origin: Origin, val: CompatByCrateVersion) {
        let mut w = blocking::watch("compat cachew", || self.crate_rustc_compat_cache.write());
        if w.len() > 5000 {
            w.clear();
            w.shrink_to(200);
        }
        w.insert(origin, val);
    }

    #[inline(never)]
    pub fn index_msrv_from_manifest(&self, origin: &Origin, manifest: &Manifest) -> CResult<RustcMinorVersion> {
        let db = self.build_db()?;
        let package = manifest.package();
        let mut working_msrv = None;

        let (mut msrv, mut reason) = match package.edition() {
            Edition::E2015 => (1u16, "???"),
            Edition::E2018 => (31, "edition 2018"),
            Edition::E2021 => (56, "edition 2021"),
        };

        if msrv < 37 && package.default_run.is_some() {
            msrv = 37;
            reason = "default-run";
        }

        // uses Option as iter
        let mut profiles = manifest.profile.release.iter().chain(&manifest.profile.dev).chain(&manifest.profile.test).chain(&manifest.profile.bench).chain(&manifest.profile.doc);
        if msrv < 41 && profiles.any(|p| p.build_override.is_some() || !p.package.is_empty()) {
            msrv = 41;
            reason = "build-override";
        }

        if msrv < 51 && matches!(package.resolver, Some(rich_crate::Resolver::V2)) {
            msrv = 51;
            reason = "resolver = 2";
        }

        let mut profiles = manifest.profile.release.iter().chain(&manifest.profile.dev).chain(&manifest.profile.test).chain(&manifest.profile.bench).chain(&manifest.profile.doc);
        if msrv < 59 && profiles.any(|p| p.strip.is_some()) {
            msrv = 59;
            reason = "strip";
        }

        if msrv < 60 && manifest.features.values().flat_map(|f| f.iter()).any(|req| req.starts_with("dep:") || req.contains('?')) {
            msrv = 60;
            reason = "dep:feature";
        }

        if let Some(minor) = package.rust_version().and_then(|v| v.split('.').nth(1)).and_then(|minor| minor.parse().ok()) {
            // <= to prefer rust-version over other indicators
            if msrv <= minor {
                reason = "rust-version";
                msrv = minor;
                working_msrv = Some(minor);
            }
        }

        if msrv > 1 {
            debug!("Detected {:?} as msrv 1.{} ({})", origin, msrv, reason);
            let latest_bad_rustc = msrv - 1;
            let ver = SemVer::parse(package.version())?;
            db.set_compat(origin, &ver, latest_bad_rustc, Compat::DefinitelyIncompatible, reason)?;
            if let Some(working_msrv) = working_msrv {
                db.set_compat(origin, &ver, working_msrv, Compat::ProbablyWorks, "rust-version")?;
            }
        }
        Ok(msrv)
    }

    #[tracing::instrument(level="debug",skip_all)]
    pub async fn rustc_compatibility(&self, all: &RichCrate) -> Result<CompatByCrateVersion, KitchenSinkErr> {
        let in_progress = Arc::new(Mutex::new(HashSet::new()));
        Ok(self.rustc_compatibility_inner(all, in_progress, 0).await?.unwrap())
    }

    /// Relaxes heuristics to run more builds
    pub async fn rustc_compatibility_for_builder(&self, all: &RichCrate, min_relevant_rustc: u16) -> Result<CompatByCrateVersion, KitchenSinkErr> {
        let in_progress = Arc::new(Mutex::new(HashSet::new()));
        Ok(self.rustc_compatibility_inner(all, in_progress, min_relevant_rustc).await?.unwrap())
    }

    pub async fn category_first_published_timestamps(&self) -> CResult<HashMap<SmolStr, Vec<u64>>> {
        Ok(self.crate_db.category_first_published_timestamps().await?)
    }

    pub async fn all_crate_compat(&self) -> CResult<HashMap<Origin, CompatByCrateVersion>> {
        let db = self.build_db()?;
        spawn_blocking(move || {
            let mut all = db.get_all_compat_by_crate()?;
            // TODO: apply rustc_release_from_date
            all.iter_mut().for_each(|(_, v)| BuildDb::postprocess_compat(v));
            Ok(all)
        }).await?
    }

    fn build_db(&self) -> Result<Arc<BuildDb>, KitchenSinkErr> {
        running()?;

        self.crate_rustc_compat_db
            .get_or_try_init(|| Ok(Arc::new(BuildDb::new(self.data_path.join("builds.db")).map_err(|_| KitchenSinkErr::BadRustcCompatData)?)))
            .cloned()
    }

    fn rustc_compatibility_inner<'a>(&'a self, all: &'a RichCrate, in_progress: Arc<Mutex<HashSet<Origin>>>, min_relevant_rustc: u16) -> BoxFuture<'a, Result<Option<CompatByCrateVersion>, KitchenSinkErr>> { async move {
        if let Some(cached) = self.crate_rustc_compat_get_cached(all.origin()) {
            return Ok(Some(cached));
        }
        if !in_progress.lock().unwrap().insert(all.origin().clone()) {
            return Ok(None);
        }

        let db = self.build_db()?;
        let mut c = self.rustc_compatibility_inner_non_recursive(all, &db)?;

        // crates most often fail to compile because their dependencies fail
        if let Ok(vers) = self.all_crates_io_versions(all.origin()) {
            let vers = vers.iter()
            .filter_map(|v| {
                let crate_ver: SemVer = v.version().parse().ok()?;
                Some((v, crate_ver))
            })
            .map(|(v, crate_ver)| {
                let mut deps_to_check = Vec::with_capacity(v.dependencies().len());
                for dep in v.dependencies() {
                    if dep.is_optional() {
                        continue;
                    }
                    if dep.kind() == DependencyKind::Dev {
                        continue;
                    }
                    if let Some(t) = dep.target() {
                        if !is_compat_relevant_target(t) {
                            debug!("ignoring {} because target {}", dep.name(), t);
                            continue;
                        }
                    }
                    if let Ok(req) = VersionReq::parse(dep.requirement()) {
                        let dep_origin = Origin::from_crates_io_name(dep.crate_name());
                        if &dep_origin == all.origin() {
                            // recursive check not supported
                            continue;
                        }
                        deps_to_check.push((dep_origin, req));
                    }
                }
                (Arc::new(crate_ver), deps_to_check)
            });

            // group by origin to avoid requesting rich_crate_async too much
            let mut by_dep = HashMap::new();
            for (crate_ver, deps_to_check) in vers {
                for (origin, req) in deps_to_check {
                    by_dep.entry(origin).or_insert_with(Vec::new).push((crate_ver.clone(), req));
                }
            }

            // fetch crate meta in parallel
            let deps = futures::future::join_all(by_dep.into_iter().map(|(dep_origin, reqs)| {
                let in_progress = Arc::clone(&in_progress);
                async move {
                    let dep_compat = if let Some(cached) = self.crate_rustc_compat_get_cached(&dep_origin) {
                        cached
                    } else {
                        debug!("recursing to get compat of {}", dep_origin.package_name_icase());
                        let dep_rich_crate = self.rich_crate_async(&dep_origin).await.ok()?;
                        self.rustc_compatibility_inner(&dep_rich_crate, in_progress, min_relevant_rustc).await.ok()??
                    };
                    Some((dep_compat, dep_origin, reqs))
                }
            })).await;

            for (dep_compat, dep_origin, reqs) in deps.into_iter().flatten() {
                for (crate_ver, req) in reqs {
                    let c = match c.get_mut(&crate_ver) {
                        Some(c) => c,
                        None => {
                            c.insert((*crate_ver).clone(), Default::default());
                            c.get_mut(&crate_ver).unwrap()
                        },
                    };

                    // make note of dependencies that affect their dependee's msrv
                    let parent_newest_bad = c.newest_bad().unwrap_or(31);
                    // but for crates where we don't know real msrv yet, don't freak out about super old rustc compat
                    let parent_oldest_ok_lower_limit = c.oldest_ok().unwrap_or(0).saturating_sub(17); // min. 2 years back-compat assumed
                    let mut dependency_affects_msrv = false;

                    // find known-bad dep to bump msrv
                    let most_compatible_dependency = dep_compat.iter()
                        .filter(|(semver, _)| req.matches(semver))
                        .filter_map(|(semver, compat)| {
                            let n = compat.newest_bad().or_else(|| Some(compat.oldest_ok()?.saturating_sub(1)))?;
                            Some((semver, n))
                        })
                        .inspect(|&(_, n)| {
                            if n > parent_newest_bad && n >= parent_oldest_ok_lower_limit && n >= min_relevant_rustc {
                                dependency_affects_msrv = true;
                            }
                        })
                        .min_by_key(|&(v, n)| (n, Reverse(v)));

                    if let Some((dep_found_ver, best_compat)) = most_compatible_dependency {
                        let dep_newest_bad = dep_compat.iter()
                            .filter(|(semver, _)| req.matches(semver))
                            .filter_map(|(_, compat)| compat.newest_bad())
                            .min().unwrap_or(0)
                            .min(best_compat); // sparse data with only few failures may be too pessimistic, because other versions may be compatible
                        if c.newest_bad().unwrap_or(0) < dep_newest_bad {
                            if dep_newest_bad >= 25 {
                                debug!("{} {} MSRV went from {} to {} because of https://lib.rs/compat/{} {} = {}", all.name(), crate_ver, c.newest_bad().unwrap_or(0), dep_newest_bad, dep_origin.package_name_icase(), req, dep_found_ver);
                                let reason = format!("{} {req}={dep_found_ver} has MSRV {dep_newest_bad}", dep_origin.package_name_icase());
                                c.add_compat(dep_newest_bad, Compat::BrokenDeps, Some(reason.clone()));

                                // setting this will make builder skip this version.
                                // propagate problem only if the failure is certain, because dep_newest_bad
                                // may be too pessimistic if the dep lacks positive build data.
                                let dep_newest_bad_certain = dep_compat.iter()
                                    .filter(|(semver, _)| req.matches(semver))
                                    .filter_map(|(_, compat)| {
                                        compat.newest_bad_likely().or_else(|| Some(compat.oldest_ok()?.saturating_sub(1)))
                                    })
                                    .min()
                                    .unwrap_or(0);
                                if c.newest_bad().unwrap_or(0) < dep_newest_bad_certain {
                                    let _ = db.set_compat(all.origin(), &crate_ver, dep_newest_bad, Compat::BrokenDepsLikely, &reason);
                                }
                            }
                        } else if dependency_affects_msrv && best_compat >= 45 {
                            // keep track of this only if it's not reflected in `BrokenDeps` (i.e. happens sometimes, not always)
                            c.requires_dependency_version(dep_origin.package_name_icase(), dep_found_ver.clone(), best_compat);
                        }
                    }
                }
            }
        }
        // postprocess (again) to update
        BuildDb::postprocess_compat(&mut c);

        self.crate_rustc_compat_set_cached(all.origin().clone(), c.clone());
        Ok(Some(c))
    }.boxed()}

    fn rustc_release_from_date(date: &DateTime<Utc>) -> Option<u16> {
        let zero = Utc.with_ymd_and_hms(2015,5,15,0,0,0).unwrap();
        let age = date.signed_duration_since(zero);
        let weeks = age.num_weeks();
        if weeks < 0 { return None; }
        Some(((weeks+1)/6) as _)
    }

    /// List of all notable crates
    /// Returns origin, rank, last updated unix timestamp
    #[inline]
    #[tracing::instrument(level="debug",skip_all)]
    pub async fn sitemap_crates(&self) -> CResult<Vec<(Origin, f64, i64)>> {
        Ok(self.crate_db.sitemap_crates().await?)
    }

    /// If given crate is a sub-crate, return crate that owns it.
    /// The relationship is based on directory layout of monorepos.
    #[inline(never)]
    pub async fn parent_crate_same_repo_unverified(&self, child: &RichCrateVersion) -> Option<Origin> {
        if !child.has_path_in_repo() {
            return None;
        }
        let repo = child.repository()?;
        let db_query = self.crate_db.parent_crate(repo, child.short_name()).map_err(|e| KitchenSinkErr::DataNotFound(e.to_string()));
        let res = timeout("parcrate", 10, db_query).await.ok()?;
        if res.as_ref().map_or(false, |p| p == child.origin()) {
            error!("Buggy parent_crate for: {:?}", child.origin());
            return None;
        }
        res
    }

    #[tracing::instrument(level="debug",skip_all)]
    #[inline(never)]
    pub async fn parent_crate(&self, child: &RichCrateVersion) -> Option<ArcRichCrateVersion> {
        let parent_origin = self.parent_crate_same_repo_unverified(child).await.or_else(|| {
            // See if there's a crate that is a prefix for this name (even if it doesn't share a repo)
            let mut rest = child.short_name();
            while let Some((name, _)) = rest.rsplit_once(|c:char| c == '_' || c == '-') {
                let origin = Origin::try_from_crates_io_name(name)?;
                if self.crate_exists(&origin) {
                    return Some(origin);
                }
                rest = name;
            }
            None
        })?;
        // dependencies are not enough to establish relationship, because they're uni-directional,
        // and neither parent nor child may want false association

        let parent = self.rich_crate_version_stale_is_ok(&parent_origin).await.ok()?;
        if parent.is_yanked() {
            return None;
        }
        if Box::pin(self.are_namespace_related(&parent, child)).await.ok()?.is_some() {
            Some(parent)
        } else {
            None
        }
    }

    /// Do these crate belong to the same project? (what would have been a namespace)
    /// None if unrelated, float 0..1 how related they are
    ///
    /// a is more trusted here, b is matched against it.
    #[inline(never)]
    pub async fn are_namespace_related(&self, a: &RichCrateVersion, b: &RichCrateVersion) -> CResult<Option<f32>> {
        // Don't recommend cryptocrap to normal people
        let a_is_dodgy = a.category_slugs().iter().any(|s| &**s == "cryptography::cryptocurrencies");
        let b_is_dodgy = b.category_slugs().iter().any(|s| &**s == "cryptography::cryptocurrencies");
        if a_is_dodgy != b_is_dodgy {
            return Ok(None);
        }

        let mut commonalities = 0;

        let a_repo_owner = a.repository().and_then(|r| r.github_host()).map(|gh| &*gh.owner);
        let have_same_repo_owner = {
            let b_repo_owner = b.repository().and_then(|r| r.github_host()).map(|gh| &*gh.owner);
            match (a_repo_owner, b_repo_owner) {
                (Some(a), Some(b)) if a.eq_ignore_ascii_case(b) => true,
                _ => false
            }
        };
        if have_same_repo_owner {
            commonalities += 1;
        }
        let have_same_homepage = match (a.homepage(), b.homepage()) {
            (Some(a), Some(b)) if a.trim_end_matches('/') == b.trim_end_matches('/') => true,
            _ => false,
        };
        if have_same_homepage {
            commonalities += 1;
        }
        let have_same_name_prefix = {
            let a_first_word = crate_name_namespace_prefix(a.short_name());
            let b_first_word = crate_name_namespace_prefix(b.short_name());
            a_first_word == b_first_word
        };
        if have_same_name_prefix {
            commonalities += 1;
        }

        // They need to look at least a bit related (these factors can be faked/squatted, so alone aren't enough)
        if 0 == commonalities {
            return Ok(None);
        }

        let commonalities_frac = commonalities as f32 / 3.;

        // this is strong
        let (common, max_owners) = self.common_real_owners(a.origin(), b.origin()).await?;
        if common > 0 {
            debug!("{} and {} are related {common}/{max_owners} owners", a.short_name(), b.short_name());
            return Ok(Some((common+1) as f32 / (max_owners+1) as f32 * commonalities_frac));
        }
        // they're related if have same repo owner, but make sure the repo url isn't fake
        if have_same_repo_owner && self.has_verified_repository_link(a).await.unwrap_or(false) && self.has_verified_repository_link(b).await.unwrap_or(false) {
            debug!("{} and {} are related not by owners, but by repo {} + {commonalities}", a.short_name(), b.short_name(), a_repo_owner.unwrap());
            return Ok(Some(0.1 * commonalities_frac));
        }
        Ok(None)
    }

    /// (common, out of how many). A is considered more important, b is matched against it.
    async fn common_real_owners(&self, a: &Origin, b: &Origin) -> CResult<(usize, usize)> {
        let (a_owners, b_owners) = futures::try_join!(self.crate_owners(a, CrateOwners::Strict), self.crate_owners(b, CrateOwners::Strict))?;
        if a_owners.iter().chain(&b_owners).any(|o| self.crates_io_login_on_blocklist(&o.crates_io_login).is_some()) {
            return Ok((0,0));
        }
        let a_owners: HashSet<_> = a_owners.into_iter().filter_map(|o| o.github_id).collect();
        let b_owners: HashSet<_> = b_owners.into_iter().filter_map(|o| o.github_id).collect();
        let max = a_owners.len();
        let common_owners = a_owners.intersection(&b_owners).count();
        Ok((common_owners, max))
    }

    /// Crates are spilt into foo and foo-core. The core is usually uninteresting/duplicate.
    #[inline(never)]
    pub async fn is_sub_component(&self, k: &RichCrateVersion) -> bool {
        let name = k.short_name();
        if let Some(pos) = name.rfind(|c: char| c == '-' || c == '_') {
            if let Some("core" | "shared" | "runtime" | "codegen" | "private" | "internals" | "internal" |
                    "derive" | "macros" | "utils" | "util" | "lib" | "types" | "common" | "impl" | "fork" | "unofficial" | "hack") = name.get(pos+1..) {
                if let Some(parent_name) = name.get(..pos-1) {
                    if Origin::try_from_crates_io_name(parent_name).map_or(false, |name| self.crate_exists(&name)) {
                        // TODO: check if owners overlap?
                        return true;
                    }
                }
                if self.parent_crate_same_repo_unverified(k).await.is_some() {
                    return true;
                }
            }
        }
        false
    }

    /// Crates are spilt into foo and foo-core. The core is usually uninteresting/duplicate.
    #[inline(never)]
    pub fn is_internal_crate(&self, k: &RichCrateVersion) -> bool {
        if k.version().ends_with("+internal") {
            return true;
        }

        let desc = k.description().unwrap_or_default();
        if desc.starts_with("Internal crate")
            || desc.starts_with("Internal dependency ")
            || desc.starts_with("An internal crate")
            || desc.starts_with("You should not use this crate")
            || desc.starts_with("This crate should not be used directly")
            || desc.starts_with("Internal testing crate")
            || desc.starts_with("Internal helper library for")
            || desc.starts_with("Implementation detail for ")
            || desc.contains("Not intended for direct use")
            || desc.contains("Please don't use this crate directly")
            || desc.contains("not intended for general use") {
            return true;
        }

        let name = k.short_name();
        if let Some((parent_name, "private" | "internals" | "internal" | "impl" | "helpers")) = name.rsplit_once(|c: char| c == '-' || c == '_') {
            if Origin::try_from_crates_io_name(parent_name).map_or(false, |name| self.crate_exists(&name)) {
                // TODO: check if owners overlap?
                return true;
            }
        }

        if let Some(readme_text) = k.readme().map(|r| r.raw_text().trim_start()) {
            if readme_text.starts_with("This is an internal crate ") {
                return true;
            }
        }
        false
    }

    #[tracing::instrument(level="debug",skip_all)]
    #[inline(never)]
    async fn cachebust_string_for_repo(&self, crate_repo: &Repo) -> Result<String, KitchenSinkErr> {
        running()?;

        let crates_in_db = self.crate_db.crates_in_repo(crate_repo).map_err(Arc::from).await?;
        let index = &*self.index()?.crates_io_crates()?;
        let v_joined: String = crates_in_db.into_iter()
            .filter_map(move |origin| match origin {
                Origin::CratesIo(name) => index.get(&name)?.versions().last(), // Most recent
                _ => None,
            })
            .map(|v| v.version())
            .collect();
        if !v_joined.is_empty() {
            return Ok(v_joined);
        }
        let fortnights = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).expect("clock").as_secs() / (3600 * 24 * 14);
        Ok(format!("f{fortnights}"))
    }

    #[inline]
    pub async fn user_github_orgs(&self, github_login: &str) -> CResult<Option<Vec<UserOrg>>> {
        trace!("gh orgs {}", github_login);
        Ok(self.gh.user_orgs(github_login).await?)
    }

    #[inline]
    pub async fn github_org(&self, github_login: &str) -> CResult<Option<Org>> {
        trace!("gh org {}", github_login);
        Ok(self.gh.org(github_login).await?)
    }

    /// Returns (contrib, github user)
    #[tracing::instrument(level="debug",skip_all)]
    #[inline(never)]
    async fn contributors_from_repo(&self, crate_repo: &Repo, found_crate_in_repo: bool) -> CResult<HashMap<String, (f64, MinimalUser)>> {
        match crate_repo.host() {
            // TODO: warn on errors?
            Repo::GitHub(ref repo) => {
                // don't use repo URL if it's not verified to belong to the crate
                if !found_crate_in_repo {
                    return Ok(HashMap::new());
                }

                // multiple crates share a repo, which causes cache churn when version "changes"
                // so pick one of them and track just that one version
                let cachebust = self.cachebust_string_for_repo(crate_repo).await.context("contrib")?;
                debug!("getting contributors for {:?}", repo);
                let contributors = match tokio::time::timeout(Duration::from_secs(10), self.gh.contributors(repo, &cachebust)).await {
                    Ok(c) => c.context("contributors")?.unwrap_or_default(),
                    Err(_timeout) => vec![],
                };
                let mut by_login: HashMap<String, (f64, MinimalUser)> = HashMap::new();
                for contr in contributors {
                    if let Some(mut author) = contr.author {
                        if author.user_type == UserType::Bot {
                            continue;
                        }
                        let count = contr.weeks.iter()
                            .map(|w| {
                                f64::from(w.commits.max(0)) +
                                f64::from(w.added_l.abs() + w.deleted_l.abs()*2).sqrt()
                            }).sum::<f64>();
                        use std::collections::hash_map::Entry;
                        match by_login.entry(author.login.to_ascii_lowercase()) {
                            Entry::Vacant(e) => {
                                if author.github_id().is_none() {
                                    warn!("github contributors gave no ID for {}", author.login);
                                    if let Ok(Some(github)) = Box::pin(self.user_by_github_login(&author.login)).await {
                                        author.id = Some(github.id);
                                        author.user_type = github.user_type;
                                    } else {
                                        error!("github contributors gave no ID for {}", author.login);
                                        continue;
                                    }
                                }
                                e.insert((count, author));
                            },
                            Entry::Occupied(mut e) => {
                                e.get_mut().0 += count;
                            },
                        }
                    }
                }
                Ok(by_login)
            },
            Repo::BitBucket(..) |
            Repo::GitLab(..) |
            Repo::Other(_) => Ok(HashMap::new()), // TODO: could use git checkout...
        }
    }

    pub async fn has_verified_repository_link(&self, k: &RichCrateVersion) -> KResult<bool> {
        if !k.origin().is_crates_io() {
            return Ok(true); // if it's not from crates.io, then it's from a repo, so surely it exists there
        }
        let Some(repo) = k.repository() else {return Ok(false) };

        if let Ok(Some(_)) = self.crate_db.path_in_repo(repo, k.short_name()).await {
            return Ok(true);
        }

        // slim the future by removing the less-common path
        Box::pin(blocking::awatch("hvrl", async move {
        // can't use the URL, because the URL could be using an old username.
        // can't support gitlab or others, because crates-io logins are GH-only.
        let owners = self.crate_owners(k.origin(), CrateOwners::Strict).await?;
        let Some(repo) = Box::pin(self.github_repo(repo)).await? else { return Ok(false) };

        if let Some(repo_owner_id) = repo.owner.as_ref().and_then(|o| o.id) {
            if owners.iter()
                .filter_map(|o| o.github_id)
                .any(|gh_id| gh_id == repo_owner_id) {
                    return Ok(true);
                }
            // Owner may belong to an org, which owns the repo. That's close enough for link verification.
            for login in owners.iter().filter_map(|o| o.github_login()).take(3) {
                if let Ok(Some(orgs)) = self.user_github_orgs(login).await {
                    for org_id in orgs.iter().filter_map(|o| o.id) {
                        if org_id == repo_owner_id {
                            return Ok(true);
                        }
                    }
                }
            }
        }
        Ok(false)
        })).await
    }

    /// Merge authors, owners, contributors
    /// Returns users by github ID, array of teams
    #[tracing::instrument(level="debug",skip_all)]
    #[inline(never)]
    pub async fn all_contributors<'a>(&self, krate: &'a RichCrateVersion) -> CResult<(HashMap<u32, CrateAuthor>, Vec<CrateAuthor>)> {
        let owners = self.crate_owners(krate.origin(), CrateOwners::All).await?;

        let has_verified_repo = self.has_verified_repository_link(krate).await?;
        let mut contributors_by_login = match krate.repository() {
            // Only get contributors from github if the crate has been found in the repo,
            // otherwise someone else's repo URL can be used to get fake contributor numbers
            Some(crate_repo) if has_verified_repo => watch("contrib", self.contributors_from_repo(crate_repo, has_verified_repo)).await?,
            _ => HashMap::new(),
        };

        let mut authors = HashMap::with_capacity(krate.authors().len());
        let mut team_authors = Vec::new();
        for (i, author) in krate.authors().iter().enumerate() {
            let mut ca = CrateAuthor {
                nth_author: Some(i),
                contribution: 0.,
                invited_at: None,
                invited_by_github_id: None,
                info: Some(author.clone()),
                github: None,
                owner: false,
                not_a_person: false,
                trust: None,
            };
            if let Some(ref email) = author.email {
                if let Ok(Some(github)) = self.user_by_email(email) {
                    let id = github.id;
                    ca.github = Some(github);
                    authors.insert(id, ca);
                    continue;
                }
            }
            if let Some(ref url) = author.url {
                if let Some(rest) = url.as_ascii_lowercase().strip_prefix("https://github.com/") {
                    let login = rest.split(|c: char| c == '/' || c == ':').next().unwrap_or_default(); // ':' because user_by_github_login panics on it
                    if let Ok(Some(gh)) = Box::pin(blocking::awatch(login, self.user_by_github_login(login))).await {
                        let id = gh.id;
                        ca.github = Some(gh);
                        authors.insert(id, ca);
                        continue;
                    }
                }
            }
            if let Some(name) = author.name.as_deref().or_else(|| author.email.as_deref().map(|e| e.split('@').next().unwrap())) {
                let lc_ascii_name = deunicode::deunicode_with_tofu_cow(name.trim(), "");
                let lc_ascii_name = lc_ascii_name.as_ascii_lowercase();
                if let Some((contribution, contr)) = contributors_by_login.remove(&*lc_ascii_name) {
                    if let Ok(github) = User::try_from(contr) {
                        let id = github.id;
                        ca.github = Some(github);
                        ca.info = None; // was useless; just a login; TODO: only clear name once it's Option
                        ca.contribution = contribution;
                        authors.insert(id, ca);
                    }
                    continue;
                }
            }

            if let Some(name) = author.name.as_deref() {
                if author.email.is_none() && author.url.is_none() {
                    let name = &*name.as_ascii_lowercase();
                    let name = name.trim_end_matches(|c: char| !c.is_ascii_alphanumeric());
                    let looks_like_team = [" developers", " contributors", " team", " members", " inc", " inc", " corp"]
                        .into_iter().any(|s| name.ends_with(s));
                    if looks_like_team && team_authors.len() < 3 {
                        ca.not_a_person = true;
                        ca.nth_author = None;
                        team_authors.push(ca);
                    }
                }
            }
        }

        let owner_trust = self.crate_owners_trust(&owners).unwrap_or_default()
            .into_iter().map(Some).chain(std::iter::repeat(None)); // zip needs this

        let mut borked_owners_github = false;
        for (owner, trust) in owners.into_iter().zip(owner_trust) {
            let user = match self.owners_github(&owner).await {
                Ok(u) => u,
                Err(e) => {
                    // This usually happens when owner has changed their username, and we haven't fetched the new username yet
                    borked_owners_github = true;
                    warn!("discarded owner no gh {owner:?}: {e}");
                    team_authors.push(CrateAuthor {
                        owner: false,
                        invited_at: owner.invited_at,
                        invited_by_github_id: owner.invited_by_github_id,
                        nth_author: None,
                        contribution: 0.,
                        info: Some(Author {
                            name: Some(owner.name().unwrap_or(&owner.crates_io_login).into()),
                            url: None,
                            email: None,
                        }),
                        github: None, // login was 404, so can't be trusted
                        not_a_person: false,
                        trust: Some(0.),
                    });
                    continue;
                },
            };
            match authors.entry(user.id) {
                Occupied(mut e) => {
                    let e = e.get_mut();
                    if !owner.contributor_only {
                        e.owner = true;
                    }
                    let info = e.info.get_or_insert(Author { name: None, email: None, url: None, });
                    if info.name.is_none() {
                        info.name = owner.name().map(String::from);
                    }
                    if info.url.is_none() && owner.url.is_some() {
                        info.url = owner.url.as_deref().map(|u| u.into());
                    }
                    let gh = e.github.get_or_insert(user);
                    if gh.name.as_ref().map_or(false, |name| *name == gh.login) {
                        gh.name = None;
                    }
                    if gh.name.is_none() && owner.name().is_some() {
                        gh.name = owner.name().map(From::from);
                    }
                    if gh.user_type == UserType::Bot {
                        e.not_a_person = true;
                    }
                    e.trust = trust;
                },
                Vacant(e) => {
                    e.insert(CrateAuthor {
                        contribution: 0.,
                        invited_at: owner.invited_at,
                        invited_by_github_id: owner.invited_by_github_id,
                        not_a_person: owner.not_a_person || user.user_type == UserType::Bot,
                        github: Some(user),
                        info: Some(Author {
                            name: owner.name().map(String::from),
                            email: None,
                            url: owner.url.map(|u| u.to_string()),
                        }),
                        nth_author: None,
                        owner: !owner.contributor_only,
                        trust,
                    });
                },
            }
        }
        if borked_owners_github {
            debug!("discarding broken owners for {:?}", krate.origin());
            // hopefully will get it right next time when fresh data is fetched
            Box::pin(self.fallback_fetch_owners_from_crates_io(krate.short_name())).await?;
        }

        for (_, (contribution, contributor)) in contributors_by_login {
            let Some(id) = contributor.github_id() else {
                debug!("discarded contributor {contributor:?}");
                continue;
            };
            match authors.entry(id) {
                Entry::Occupied(mut e) => {
                    e.get_mut().contribution += contribution;
                },
                Entry::Vacant(e) => {
                    let Ok(github) = User::try_from(contributor) else { continue; };
                    e.insert(CrateAuthor {
                        nth_author: None,
                        invited_at: None,
                        invited_by_github_id: None,
                        contribution,
                        info: None,
                        not_a_person: github.user_type == UserType::Bot,
                        github: Some(github),
                        owner: false,
                        trust: None,
                    });
                },
            }
        }

        Ok((authors, team_authors))
    }

    // User-facing cleaned up version of all_contributors
    #[tracing::instrument(level="debug",skip_all)]
    pub async fn crate_owners_and_contributors<'a>(&self, krate: &'a RichCrateVersion, deadline: Instant) -> CResult<(Vec<CrateAuthor>, Vec<CrateAuthor>, usize)> {
        let (mut authors, team_authors) = Box::pin(blocking::awatch("alc", self.all_contributors(krate))).await?;

        for gh in authors.values_mut().filter_map(|a| a.github.as_mut()) {
            if gh.name.as_ref().map_or(false, |name| *name == gh.login) {
                gh.name = None;
            }
            if gh.name.is_none() && Instant::now() < deadline {
                let res = Box::pin(timeout("ghu", 5, self.user_by_github_login(&gh.login))).await;
                if let Ok(Some(new_gh)) = res {
                    *gh = new_gh
                }
            }
        }

        self.crate_owners_and_contributors_inner(authors, team_authors)
    }

    #[inline(never)]
    fn crate_owners_and_contributors_inner<'a>(&self, mut authors: HashMap<u32, CrateAuthor>, team_authors: Vec<CrateAuthor>) -> CResult<(Vec<CrateAuthor>, Vec<CrateAuthor>, usize)> {
        if authors.len() > 1 {
            let owner_names: HashSet<_> = authors.values()
                .filter(|a| a.owner)
                .map(|a| a.name().to_ascii_lowercase())
                .collect();
            authors.retain(move |_, a| {
                // some users have mutliple github accounts which get duped via email search or contributors
                a.owner || !owner_names.contains(&*a.name().as_ascii_lowercase())
            });
        }

        let max_author_contribution = authors
            .values()
            .map(|a| if a.owner || a.nth_author.is_some() { a.contribution } else { 0. })
            .max_by(|a, b| a.total_cmp(b))
            .unwrap_or(0.);
        let big_contribution = if max_author_contribution < 50. { 200. } else { max_author_contribution / 2. };

        let mut contributors = 0;
        let (mut authors_or_owners, mut passive_owners): (Vec<_>, Vec<_>) = authors.into_values()
            .filter(|a| if a.owner || a.nth_author.is_some() || a.contribution >= big_contribution {
                true
            } else {
                contributors += 1;
                false
            })
            .partition(|a| {
                a.nth_author.is_some() || a.contribution > 0.
            });

        pick_top_n_unstable_by(&mut authors_or_owners, 15, |a, b| {
            fn score(a: &CrateAuthor) -> f64 {
                let o = if a.owner { if !a.not_a_person { 200. } else { 10. } } else { 1. };
                o * (a.contribution + 10.) / (1 + a.nth_author.unwrap_or(99)) as f64
            }
            score(b).total_cmp(&score(a))
        });
        authors_or_owners.extend(team_authors);

        passive_owners.sort_unstable_by_key(|a| a.github.as_ref().map_or(0, |g| g.id));
        passive_owners.truncate(15); // long lists look spammy

        Ok((authors_or_owners, passive_owners, contributors.min(100)))
    }

    #[inline]
    #[tracing::instrument(level="debug", skip_all)]
    async fn owners_github(&self, owner: &CrateOwner) -> CResult<User> {
        let orig_github_id = owner.github_id().or_else(|| {
            if owner.kind != OwnerKind::User { // team IDs are not compatible with user IDs
                return None;
            }
            // owners from audit have this, but not gh id
            self.crates_io_ids_to_github_mapping.get(&(owner.crates_io_id?)).ok()?
        });
        let login = orig_github_id.and_then(|id| {
            let found = block_in_place(format!("gh{id}"), move || self.login_by_github_id(id.into())).ok()?;
            if !owner.github_login().unwrap_or("").eq_ignore_ascii_case(&found) {
                debug!("mapped {id} to {found}; orig was {:?}", owner.github_login());
            }
            Some(found)
        });

        let login = login.as_deref().or_else(|| owner.github_login())
            .ok_or(KitchenSinkErr::OwnerWithoutLogin)?;

        // This is a bit weak, since logins are not permanent
        let mut user = Box::pin(self.user_by_github_login(login)).await?
            .ok_or_else(|| KitchenSinkErr::OwnerWithoutLogin)?;

        if let Some(orig_id) = orig_github_id {
            if user.id != orig_id {
                warn!("There is confusion about owner's github ID; {orig_id} => {login} => {} [{owner:?}]", user.id);
                user.id = orig_id; // threat CrateOwner as authoritative about the ID
                // this user is weird, so clear other fields that may be wrong too
                user.avatar_url = None;
                user.gravatar_id = None;
                user.created_at = None;
            }
        }
        Ok(user)
    }

    pub async fn all_crates_of_author(&self, aut: &RichAuthor) -> CResult<Vec<CrateOwnerRow>> {
        self.all_crates_of_github_id(aut.github.id).await
    }

    #[inline]
    #[tracing::instrument(level="debug",skip(self))]
    pub async fn all_crates_of_github_id(&self, github_id: u32) -> CResult<Vec<CrateOwnerRow>> {
        Ok(self.crate_db.all_crates_of_github_id(github_id).await?)
    }

    fn owners_from_audit(current_owners: Vec<CrateOwner>, meta: CrateMetaFile) -> Vec<CrateOwner> {
        let mut current_owners_by_login: HashMap<_, _> = current_owners.into_iter().map(|o| (o.crates_io_login.to_ascii_lowercase(), o)).collect();

        // latest first
        let mut actions: Vec<_> = meta.versions.into_iter().flat_map(|v| v.audit_actions).collect();
        actions.sort_unstable_by(|a,b| b.time.cmp(&a.time));

        // audit actions contain logins which are not present in owners, probably because they're GitHub team members
        for (idx, a) in actions.into_iter().enumerate() {
            if let Some(o) = current_owners_by_login.get_mut(&*a.user.login.as_ascii_lowercase()) {
                if o.invited_at.as_ref().map_or(true, |l| l < &a.time) {
                    o.invited_at = Some(a.time);
                }
                if o.last_seen_at.as_ref().map_or(true, |l| l < &a.time) {
                    o.last_seen_at = Some(a.time);
                }
            } else {
                current_owners_by_login.insert(a.user.login.to_ascii_lowercase(), CrateOwner {
                    github_id: a.user.github_id(),
                    kind: OwnerKind::User,
                    url: Some(format!("https://github.com/{}", a.user.login).into()),
                    crates_io_login: a.user.login,
                    name: a.user.name,
                    avatar: a.user.avatar,
                    last_seen_at: Some(a.time),
                    invited_at: Some(a.time),
                    invited_by_github_id: None,
                    // most recent action is assumed to be done by an owner,
                    // but we can't be sure about past ones
                    contributor_only: idx > 0,
                    not_a_person: false,
                    crates_io_id: Some(a.user.crates_io_id as u32),
                });
            }
        }
        current_owners_by_login.into_values().collect()
    }

    /// 0-inf; 100 = pretty trusted; 10 = not nobody; 2 = median
    pub fn crate_owners_trust(&self, owners: &[CrateOwner]) -> CResult<Vec<f64>> {
        Ok(self.user_db.trust_for_owners(owners)?)
    }

    async fn fallback_fetch_owners_from_crates_io(&self, crate_name: &str) -> KResult<Vec<CrateOwner>> {
        let cachebust = format!("halfday{}", SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).expect("clock").as_secs() / (3600 * 12));
        let owners = timeout("owners-fallback", 5, self.crates_io.crate_owners(crate_name, &cachebust).map(|r| r.map_err(KitchenSinkErr::from))).await?.unwrap_or_default();
        debug!("fetched crates io owners for {crate_name}: {owners:?}");
        for o in &owners {
            let (Some(id), Some(login)) = (o.github_id, o.github_login())  else { continue };
            block_in_place(format!("owners{crate_name}"), || {
                let known_github_id = self.user_db.user_by_github_login_opt(login, true)
                    .map_err(|e| error!("find user {login}: {e}")).ok().flatten().map(|u| u.id);
                if known_github_id == Some(id) {
                    return;
                }
                let Some(u) = Self::github_user_from_crate_owner(o) else { return };
                // cachebuster length, although unsure if crates-io has up-to-date data…
                let _ = self.user_db.index_user(&u, None, None, SystemTime::now() - Duration::from_secs(3600*12), true)
                    .map_err(|e| error!("index user {login}: {e} {u:?}"));
            });
        }
        self.crates_io_owners_cache.set(crate_name, &owners)?;
        Ok(owners)
    }

    fn github_user_from_crate_owner(o: &CrateOwner) -> Option<User> {
        let login = o.github_login()?;
        let id = o.github_id()?;
        if id == 0 {
            return None;
        }
        Some(User {
            id,
            html_url: o.url.as_ref().filter(|&n| !n.is_empty()).cloned().unwrap_or_else(|| format!("https://github.com/{login}").into()),
            login: login.into(),
            name: o.name.as_ref().filter(|&n| !n.is_empty()).cloned(),
            avatar_url: o.avatar.as_ref().filter(|&n| !n.is_empty()).cloned(),
            gravatar_id: None,
            blog: None,
            two_factor_authentication: None,
            user_type: match o.kind {
                OwnerKind::Team => UserType::Org,
                OwnerKind::User => UserType::User,
            },
            created_at: None,
        })
    }

    /// true if it's verified current owner, false if may be a past owner (contributor now)
    #[tracing::instrument(level="debug",skip_all)]
    #[inline(never)]
    pub async fn crate_owners(&self, origin: &Origin, set: CrateOwners) -> KResult<Vec<CrateOwner>> {
        let mut owners = Box::pin(blocking::awatch(&format!("own {origin:?}"), self.crate_owners_inner(origin, set))).await?;
        owners.retain(|o| {
            o.crates_io_login != "rust-bus-owner" &&
            o.crates_io_login != "rust-bus" &&
            o.crates_io_login != "github::rust-bus:maintainers"
        });
        Ok(owners)
    }

    async fn crate_owners_inner(&self, origin: &Origin, set: CrateOwners) -> KResult<Vec<CrateOwner>> {
        let mut owners: Vec<_> = match origin {
            Origin::CratesIo(crate_name) => {
                let current_owners = async {
                    Ok(match self.crates_io_owners_cache.get(&**crate_name)? {
                        Some(o) => o,
                        None => Box::pin(self.fallback_fetch_owners_from_crates_io(crate_name)).await?,
                    })
                };
                if set == CrateOwners::Strict {
                    let mut owners = current_owners.await?;
                    // anyone can join rust-bus, so it's meaningless as a common owner between crates
                    owners.retain(|o| !not_a_personal_login(&o.crates_io_login));
                    owners
                } else {
                    let (current_owners, meta) = futures::try_join!(current_owners, self.crates_io_meta(crate_name))?;
                    let mut res = Self::owners_from_audit(current_owners, meta);
                    // TODO: check the db if the user was removed
                    res.iter_mut().for_each(|o| if not_a_personal_login(&o.crates_io_login) { o.not_a_person = true; });
                    res
                }
            },
            Origin::GitLab {..} => vec![],
            Origin::GitHub(r) => vec![
                CrateOwner {
                    avatar: None,
                    url: Some(format!("https://github.com/{}", r.repo.owner).into()),
                    crates_io_login: r.repo.owner.clone(),
                    kind: OwnerKind::User, // FIXME: crates-io uses teams, and we'd need to find the right team? is "owners" a guaranteed thing?
                    name: None,
                    invited_at: None,
                    github_id: None, // Fixed below
                    invited_by_github_id: None,
                    last_seen_at: None,
                    contributor_only: false,
                    not_a_person: false,
                    crates_io_id: None,
                }
            ],
        };
        let _ = join_all(owners.iter_mut().filter(|o| o.github_id.is_none()).map(move |owner| async move {
            let crates_io_lookup = owner.crates_io_id
                .filter(|_| owner.kind == OwnerKind::User) // teams have separate ID space
                .and_then(move |cid| block_in_place(format!("cid {cid}"), move || self.crates_io_ids_to_github_mapping.get(&cid).ok()));
            owner.github_id = Some(if let Some(Some(id)) = crates_io_lookup {
                id
            } else if let Some(id) = owner.github_id() {
                id
            } else if let Some(login) = owner.github_login() {
                match Box::pin(self.user_by_github_login(login)).await {
                    Ok(Some(u)) => u.id,
                    Ok(None) => {
                        warn!("crate owner of {origin:?} has login {login} without github id: {owner:?}");
                        return;
                    },
                    Err(e) => {
                        error!("can't get GH {login} id: {e}");
                        return;
                    },
                }
            } else {
                warn!("crate owner of {origin:?} without github login: {owner:?}");
                return;
            });
        })).await;
        Ok(owners)
    }

    pub fn crate_tarball_download_url(&self, k: &RichCrateVersion) -> Option<String> {
        if k.origin().is_crates_io() {
            Some(self.crates_io.crate_data_url(k.short_name(), k.version()))
        } else {
            None
        }
    }

    pub fn index_stats_histogram(&self, kind: &str, data: StatsHistogram) -> CResult<()> {
        Ok(self.stats_histograms.set(kind, data)?)
    }

    /// Made by datadump
    pub fn get_stats_histogram(&self, kind: &str) -> CResult<Option<StatsHistogram>> {
        Ok(self.stats_histograms.get(kind)?)
    }

    /// Direct reverse dependencies, but with release dates (when first seen or last used)
    pub fn index_dependers_liveness_ranges(&self, origin: &Origin, ranges: Vec<DependerChanges>) -> CResult<()> {
        self.depender_changes.set(origin.to_str(), ranges)?;
        Ok(())
    }

    #[tracing::instrument(level="debug",skip_all)]
    #[inline(never)]
    pub fn most_popular_crates(&self, limit: usize) -> CResult<Vec<(Origin, f64)>> {
        const EXCLUDED_FROM_STD_CATEGORY: &[&str] = &[
            "crates.io:structopt", // in clap
            "crates.io:serde_cbor", // dead
            "crates.io:tempdir", // deprecated
            "crates.io:serde_derive", // serde --features=derive
            "crates.io:ansi_term", // unmaintained
            "crates.io:openssl", // use ring or native-tls!
            "crates.io:fxhash", // rustc-hash
            "crates.io:getopts", // getting old now
            "crates.io:sha-1", // dupe
            "crates.io:md-5", // dupe
        ];

        let mut out = Vec::with_capacity(limit * 2);
        let mut min_users = 30;
        let mut min_users_adjusted = 20.;
        self.depender_changes.for_each(|k, v| {
            if let Some(last) = v.iter().max_by_key(|v| v.at) {
                let users = last.users_abs;
                if users > min_users {
                    // a quick way to find dying crates
                    let peak = v.iter().map(|m| m.users_abs).max().unwrap_or(0) + 1;
                    let ratio = f64::from(users) / f64::from(peak);
                    if ratio < 0.5 {
                        return;
                    }
                    let adjusted = f64::from(users) * ratio;

                    if adjusted > min_users_adjusted {
                        if EXCLUDED_FROM_STD_CATEGORY.contains(&&**k) {
                            return;
                        }

                        out.push((Origin::from_str(k), users, adjusted));
                        if out.len() == out.capacity() {
                            pick_top_n_unstable_by(&mut out, limit, |a,b| b.2.total_cmp(&a.2));
                            let last = out.last().unwrap();
                            min_users = last.1;
                            min_users_adjusted = last.2;
                        }
                    }

                }
            }
        })?;

        // people expect sorting by downloads, even though it's meh, so compromise on half-users half-downloads
        let now = Utc::now().date_naive();
        for (origin, _, popularity) in &mut out {
            *popularity *= self.downloads_per_month_cached(origin, now).unwrap_or_default().unwrap_or(0) as f64;
        }
        pick_top_n_unstable_by(&mut out, limit, |a,b| b.2.total_cmp(&a.2));
        Ok(out.into_iter().map(|(o, _, p)| (o,p)).collect())
    }

    /// Direct reverse dependencies, but with release dates (when first seen or last used)
    #[tracing::instrument(level="debug",skip(self))]
    #[inline(never)]
    pub fn depender_changes(&self, origin: &Origin) -> CResult<Vec<DependerChangesMonthly>> {
        let daily_changes = self.depender_changes.get(origin.to_str().as_str())?.unwrap_or_default();
        if daily_changes.is_empty() {
            return Ok(Vec::new());
        }

        let mut by_month = HashMap::with_capacity(daily_changes.len());
        for d in &daily_changes {
            let key = d.at.fake_month();
            let w = by_month.entry(key).or_insert(DependerChangesMonthly {
                year: key.0,
                month0: key.1,
                added: 0, added_total: 0,
                removed: 0, removed_total: 0,
                expired: 0, expired_total: 0,
                users_total: 0,
            });
            w.added += u32::from(d.added);
            w.removed += u32::from(d.removed);
            w.expired += u32::from(d.expired);
            w.users_total = d.users_abs;
        }

        let first = &daily_changes[0];
        let last = daily_changes.last().unwrap();
        let mut curr = first.at.fake_month();
        let end = last.at.fake_month();
        let mut monthly = Vec::with_capacity(by_month.len());
        let mut added_total = 0;
        let mut removed_total = 0;
        let mut expired_total = 0;
        let mut last_users_total = 0;
        while curr <= end {
            let mut e = by_month.get(&curr).copied().unwrap_or(DependerChangesMonthly {
                year: curr.0, month0: curr.1,
                added: 0, removed: 0, expired: 0,
                added_total: 0, removed_total: 0, expired_total: 0,
                users_total: last_users_total,
            });
            added_total += e.added;
            expired_total += e.expired;
            removed_total += e.removed;
            e.added_total = added_total;
            e.expired_total = expired_total;
            e.removed_total = removed_total;
            last_users_total = e.users_total;
            monthly.push(e);
            curr.1 += 1;
            if curr.1 > 11 {
                curr.0 += 1;
                curr.1 = 0;
            }
        }
        Ok(monthly)
    }

    #[inline(never)]
    pub async fn traction_stats(&self, origin: &Origin, stats: &DepsStats) -> CResult<Option<TractionStats>> {
        let (direct_rev_deps, mut res) = self.traction_stats_inner(origin, stats)?;

        if let Some(res) = &mut res {
            // intrenal crates used only/mainly by their parent never seem abandoned,
            // because they never had many users to lose in the first place.
            if direct_rev_deps > 0 && res.active_users < 5 {
                if let Some(parent) = Box::pin(self.parent_crate(&*self.rich_crate_version_stale_is_ok(origin).await?)).await {
                    if let (_, Some(parent_res)) = self.traction_stats_inner(parent.origin(), stats)? {
                        // if the sole user is dying, this crate is too
                        let w = f64::from(res.active_users);
                        if parent_res.former_glory < res.former_glory {
                            res.former_glory = (res.former_glory * w + parent_res.former_glory) / (w + 1.);
                        }
                        if parent_res.growth < res.growth {
                            res.growth = (res.growth * w + parent_res.growth) / (w + 1.);
                        }
                    }
                }
            }
        }

        Ok(res)
    }

    #[inline(never)]
    fn traction_stats_inner(&self, origin: &Origin, stats: &DepsStats) -> CResult<(u32, Option<TractionStats>)> {
        let (direct_rev_deps, indirect_to_direct_ratio) = self.indirect_to_direct_dep_ratio(origin, stats)?;
        let depender_changes = self.depender_changes(origin)?;

        Ok((direct_rev_deps, Self::traction_stats_for_changes(&depender_changes, indirect_to_direct_ratio)))
    }

    #[inline(never)]
    fn indirect_to_direct_dep_ratio(&self, origin: &Origin, stats: &DepsStats) -> Result<(u32, f64), KitchenSinkErr> {
        let deps = match stats.crates_io_dependents_stats_of(origin) {
            Some(d) => d,
            None => return Ok((0, 0.9)),
        };
        let direct_rev_deps = deps.direct.all();

        let indirect_reverse_optional_deps = (deps.runtime.def + deps.runtime.opt)
            .max(u32::from(deps.dev))
            .max(deps.build.def + deps.build.opt);

        let indirect_to_direct_ratio = 1f64.min(f64::from(direct_rev_deps * 3) / f64::from(indirect_reverse_optional_deps.max(1)));
        let indirect_to_direct_ratio = (0.9 + indirect_to_direct_ratio) * 0.5;
        Ok((direct_rev_deps, indirect_to_direct_ratio))
    }


    fn traction_stats_for_changes(depender_changes: &[DependerChangesMonthly], indirect_to_direct_ratio: f64) -> Option<TractionStats> {
        let current_active = depender_changes.last()?;

        // smooth spikes out
        let peak_active_users = depender_changes.windows(2)
            .map(|m| m[0].users_total + m[1].users_total).max().unwrap_or(0) / 2;

            // TODO: count WHEN was the peak, and discount if it was recently
        let current_active_users = current_active.users_total;
        let current_active_crates = current_active.running_total();
        // laplace smooth unpopular crates. if peak num of users was always small, assume it's noisy, and don't penalize (fltk issue).
        let min_relevant_users = 10 + 30u16.saturating_sub(peak_active_users);
        let mut former_glory = 1f64.min(f64::from(current_active_users + min_relevant_users + 1) / f64::from(peak_active_users + min_relevant_users));

        former_glory = former_glory * indirect_to_direct_ratio + (1. - indirect_to_direct_ratio);
        // if it's being mostly removed, accelerate its demise. laplace smoothed for small crates
        let removals_fraction = 1. - f64::from(current_active.expired_total + 10) / f64::from(current_active.removed_total + current_active.expired_total + 10);
        let mut powf = 1.0 + removals_fraction * 0.7;
        let mut growth = 1.0;
        // compare to last quarter, smoothed
        let prev_sample = depender_changes.len().saturating_sub(6);
        let prev_sample = prev_sample..prev_sample+2;
        let curr_sample = depender_changes.len().saturating_sub(2);
        let curr_sample = curr_sample..curr_sample+2;
        if let (Some(prev), Some(curr)) = (depender_changes.get(prev_sample), depender_changes.get(curr_sample)) {
            let prev = prev[0].users_total + prev[1].users_total;
            let curr = curr[0].users_total + curr[1].users_total;

            growth = f64::from(curr + 25) / f64::from(prev + 25);
            if current_active_users == 0 {
                growth = growth.min(1.); // having a blip last month isn't growth
            }

            // if it's clearly declining, accelerate its demise
            if prev > curr + 2 {
                powf += 0.25;
            }
        }
        // if the crate has lots of other crates using them, but they're all the same user, that looks spammy and isn't real traction
        let external_user_ratio = (f64::from(current_active_users.saturating_sub(2) + 5) * 1.6 / f64::from(current_active_crates.saturating_sub(5) + 5)).powf(1.1);
        // but if a crate is mainstream-popular it can do whatever
        let external_usage = external_user_ratio.max(f64::from(current_active_users) /400.).min(1.);
        Some(TractionStats {
            former_glory: former_glory.powf(powf),
            external_usage,
            growth,
            active_users: current_active_users,
            peak_active_users,
        })
    }

    #[tracing::instrument(level="debug",skip_all)]
    #[inline(never)]
    pub async fn index_crates_io_crate_all_owners(&self, all_owners: Vec<(Origin, Vec<CrateOwner>)>, date_of_file: DateTime<Utc>) -> CResult<()> {
        running()?;
        self.crate_db.index_crate_all_owners(&all_owners, date_of_file).await?;

        block_in_place("all owners u", || {
            let users = all_owners.iter().flat_map(|(_, owners)| owners.iter().filter_map(|o| {
                Some(User {
                    id: o.github_id?,
                    login: o.github_login()?.into(),
                    name: o.name.clone(),
                    avatar_url: o.avatar.clone(),
                    gravatar_id: None,
                    html_url: o.url.as_deref().unwrap_or("").into(),
                    blog: None,
                    user_type: match o.kind {
                        OwnerKind::Team => UserType::Org,
                        OwnerKind::User => UserType::User,
                    },
                    created_at: None,
                    two_factor_authentication: None,
                })
            })).collect::<Vec<_>>();
            self.user_db.index_users(&users, date_of_file.into(), true)
        })?;
        running()?;
        block_in_place("all owners i", move || {
            self.crates_io_ids_to_github_mapping.set_many(all_owners.iter().flat_map(|(_, owners)| owners).filter_map(|o| {
                if o.kind != OwnerKind::User {
                    return None;
                }
                Some((o.crates_io_id?, o.github_id?))
            }))?;

            self.crates_io_owners_cache.set_many(all_owners.into_iter().filter_map(|(origin, owners)| {
                let Origin::CratesIo(name) = origin else { return None };
                Some((String::from(name).into(), owners))
            }))?;
            Ok(())
        })
    }

    // Sorted from the top, returns origins
    #[tracing::instrument(level="debug",skip(self))]
    #[inline(never)]
    pub async fn top_crates_in_category(&self, slug: &str, rend: &RendCtx) -> CResult<Arc<Vec<Origin>>> {
        let cell = {
            let mut cache = self.top_crates_cached.lock().unwrap();
            Arc::clone(cache.entry(slug.to_owned()).or_insert_with(|| Arc::new(DoubleCheckedCell::new())))
        };

        let res = cell.get_or_try_init(async {
            watch("topcc", async {
                let (total_count, _) = self.category_crate_count(slug).await?;
                let wanted_num = (total_count * 2 / 3 + 50).max(105).min(3000);

                let mut crates = if slug == "uncategorized" {
                    self.crate_db.top_crates_uncategorized(wanted_num + 50).await?
                } else if slug == "std" {
                    self.most_popular_crates(250)?
                } else {
                    self.crate_db.top_crates_in_category_partially_ranked(slug, wanted_num + 50).await?
                };
                watch("dupes", self.knock_duplicates(&mut crates, rend)).await;
                let crates: Vec<_> = crates.into_iter()
                    .map(|(o, _)| o).take(wanted_num as usize)
                    .collect();
                Ok::<_, anyhow::Error>(CachedCategory {
                    origins: Arc::new(crates),
                    expires: std::time::Instant::now() + Duration::from_secs(if rend.is_incomplete() { 60 } else { 45 * 60 }),
                })
            }).await
        }).await?;

        if res.expires < std::time::Instant::now() {
            let mut cache = self.top_crates_cached.lock().unwrap();
            cache.remove(slug);
        }

        Ok(Arc::clone(&res.origins))
    }

    /// To make categories more varied, lower score of crates by same authors, with same keywords
    #[tracing::instrument(level="debug",skip_all)]
    #[inline(never)]
    async fn knock_duplicates(&self, crates: &mut Vec<(Origin, f64)>, rend: &RendCtx) {
        let with_owners = {
            let deadline = Instant::now() + Duration::from_millis(1500 + crates.len() as u64 * 8);
            futures::stream::iter(crates.drain(..)).map(|(o, relevance_score)| async move {
            if Instant::now() > deadline {
                if !rend.is_incomplete() {
                    warn!("Everything timed out in ranking");
                    rend.set_incomplete();
                }
                return Some((o, relevance_score, vec![], vec![]));
            }
            let get_crate = tokio::time::timeout_at(deadline, self.rich_crate_version_stale_is_ok(&o));
            let (k, owners) = futures::join!(get_crate, self.crate_owners(&o, CrateOwners::All));
            let keywords = match k {
                Ok(Ok(c)) => {
                    if c.is_yanked() || c.is_hidden() || c.is_spam() {
                        return None;
                    }
                    c.keywords().to_vec()
                },
                Err(_) => {
                    warn!("{:?} Timed out in ranking", o);
                    Vec::new()
                },
                Ok(Err(e)) => {
                    warn!("Skipping dedup {:?} because {}", o, e);
                    for e in e.chain() {
                        warn!(" • because {}", e);
                    }
                    return None;
                },
            };
            let mut owners = owners.unwrap_or_default();
            owners.retain(|o| !o.not_a_person && o.github_id.is_some());
            Some((o, relevance_score, owners, keywords))
        })
        .buffer_unordered(20)
        .filter_map(|x| async {x})
        .collect::<Vec<_>>().await
        };

        let mut top_keywords = HashMap::default();
        for (_, _, _, keywords) in &with_owners {
            for k in keywords {
                *top_keywords.entry(k).or_insert(0u32) += 1;
            }
        }
        let mut top_keywords: Vec<_> = top_keywords.into_iter().collect();
        let limit = (top_keywords.len() / 10).clamp(2, 10);
        pick_top_n_unstable_by(&mut top_keywords, limit, |a, b| b.1.cmp(&a.1));
        let top_keywords: HashSet<_> = top_keywords.iter().map(|(k, _)| k.as_str()).collect();

        crates.clear();
        debug_assert!(crates.capacity() >= with_owners.len());

        let mut seen_owners = HashMap::new();
        let mut seen_keywords = HashMap::new();
        let mut seen_owner_keywords = HashMap::new();
        for (origin, score, owners, keywords) in &with_owners { // borrows keywords
            let mut weight_sum = 0;
            let mut score_sum = 0.0;
            for owner in owners.iter().take(5) {
                let n = seen_owners.entry(owner.github_id.unwrap_or(0)).or_insert(0u32);
                score_sum += f64::from((*n).saturating_sub(3)); // authors can have a few crates with no penalty
                weight_sum += 2;
                *n += 2;
            }
            let primary_owner_id = owners.get(0).and_then(|o| o.github_id).unwrap_or(0);
            for keyword in keywords.iter().take(5) {
                // obvious keywords are too repetitive and affect innocent crates
                if !top_keywords.contains(keyword.as_str()) {
                    let n = seen_keywords.entry(keyword.as_str()).or_insert(0u32);
                    score_sum += f64::from((*n).saturating_sub(4)); // keywords are expected to repeat a bit
                    weight_sum += 1;
                    *n += 1;
                }

                // but same owner AND same keyword needs extra bonus for being extra boring
                let n = seen_owner_keywords.entry((primary_owner_id, keyword.as_str())).or_insert(0);
                score_sum += f64::from(*n);
                weight_sum += 2;
                *n += 3;
            }
            // it's average, because new fresh keywords should reduce penalty
            let dupe_points = score_sum / f64::from(weight_sum + 10); // +10 reduces penalty for crates with few authors, few keywords (higher chance of dupe)

            // +7 here allows some duplication, and penalizes harder only after a few crates
            // adding original score means it'll never get lower than 1/3rd
            let new_score = score * 0.5 + (score + 7.) / (7. + dupe_points);
            crates.push_in_cap((origin.clone(), new_score));
        }
        crates.sort_unstable_by(|a, b| b.1.partial_cmp(&a.1).unwrap_or(Ordering::Equal));
    }

    #[tracing::instrument(level="debug",skip_all)]
    #[inline(never)]
    pub async fn top_keywords_in_category(&self, cat: &Category) -> CResult<Vec<SmolStr>> {
        let mut keywords = self.crate_db.top_keywords_in_category(&cat.slug).await?;
        keywords.retain(|k| !cat.obvious_keywords.iter().any(|o| o == k));
        keywords.truncate(10);
        Ok(keywords)
    }

    /// true if it's useful as a keyword page
    #[inline]
    pub async fn is_it_a_keyword(&self, k: &str) -> bool {
        self.crate_db.crates_with_keyword(k).await.map(|n| n >= 5).unwrap_or(false)
    }

    /// True if there are multiple crates with that keyword. Populated first.
    /// This is for crate page display only.
    #[tracing::instrument(level="debug",skip_all)]
    pub async fn keywords_populated<'a>(&self, krate: &'a RichCrateVersion) -> Vec<(&'a str, bool)> {
        let category_slugs: HashSet<_> = krate.category_slugs().iter()
            .flat_map(|s| s.rsplit("::").next()).collect();

        let mut keywords: Vec<_> = join_all(krate.keywords().iter()
            // displaying keywords that are same as categories is redundant
            .filter(|k| !category_slugs.contains(k.as_str()))
            .map(|k| async move {
                let populated = self.crate_db.crates_with_keyword(k).await
                    .map_err(|e| error!("kw {e}")).map_or(true, |n| n >= 3);
                (k.as_str(), populated)
            })).await;
        keywords.sort_by_key(|&(_, v)| !v); // populated first; relies on stable sort
        keywords
    }

    #[inline]
    pub async fn recently_updated_crates_in_category(&self, slug: &str) -> CResult<Vec<Origin>> {
        Ok(self.crate_db.recently_updated_crates_in_category(slug).await?)
    }

    /// Case sensitive!
    #[inline(never)]
    pub fn rustacean_for_github_login(&self, login: &str) -> Option<Rustacean> {
        if !is_alnum(login) {
            return None;
        }

        let path = self.data_path.join(format!("rustaceans/data/{login}.json"));
        let json = std::fs::read(path).ok()?;
        serde_json::from_slice(&json).map_err(|e| error!("bad rustacean json for {login} {e}")).ok()
    }

    pub async fn github_social(&self, login: &str) -> Option<Vec<SocialAccount>> {
        self.gh.social(login).await.ok().flatten()
    }

    #[inline]
    #[tracing::instrument(level="debug",skip_all)]
    pub async fn notable_recently_updated_crates(&self, limit: u32, rend: &RendCtx) -> CResult<Vec<(Origin, f64)>> {
        let mut crates = self.crate_db.recently_updated_crates(limit).await?;

        if limit < 750 {
            let now = Utc::now();
            let deadline = Instant::now() + Duration::from_millis(2500 + crates.len() as u64 * 16);
            futures::stream::iter(crates.iter_mut()).for_each_concurrent(Some(20), |(o, rel)| async move {
                *rel *= 0.1 + self.notable_recently_updated_big_update_score(o, now, deadline).await;
            }).await;
            self.knock_duplicates(&mut crates, rend).await;
        }
        Ok(crates)
    }

    // An update stays relevant for time proportional to time between previous updates
    // so frequently released crates aren't shown for long, rare big releases stick for longer.
    async fn notable_recently_updated_big_update_score(&self, o: &Origin, now: DateTime<Utc>, deadline: Instant) -> f64 {
        let deadline = (Instant::now() + Duration::from_secs(2)).min(deadline);
        let Ok(Ok(all)) = tokio::time::timeout_at(deadline, self.rich_crate_async(o)).await else { return 0.25 };
        // take up to 20 most recent releases
        let versions: Vec<_> = all.versions().iter().filter_map(|release| {
            let ver = SemVer::parse(&release.num).ok()?;
            let age = now.signed_duration_since(&release.created_at).num_hours();
            Some((ver, age))
        }).take(20).collect();
        let Some(last) = versions.last() else { return 0. };

        let mut prev_patch = last;
        let mut prev_minor = last;
        let mut prev_major = last;
        let mut most_relevant = 0.;

        // iterate from the end
        for v in versions.iter().rev().skip(1) {
            // 0.x is not special-cased as major, so it makes 1.0.0 release bigger
            let time_between_releases = 1 + if prev_major.0.major < v.0.major {
                let t = prev_major.1 - v.1;
                prev_major = v;
                prev_minor = v;
                prev_patch = v;
                t
            } else if prev_minor.0.minor < v.0.minor {
                let t = prev_minor.1 - v.1;
                prev_minor = v;
                prev_patch = v;
                t
            } else {
                let t = prev_patch.1 - v.1;
                prev_patch = v;
                t
            };
            let end_of_relevance = v.1 - (time_between_releases/4);
            if end_of_relevance < 0 { // in the future
                let remaining = end_of_relevance.abs();
                let remaining_frac = remaining as f64 / time_between_releases as f64 *
                    // rare releases are much more notable than daily ones
                    if time_between_releases > 60*24 { 1. }
                    else if time_between_releases > 14*24 { 0.75 }
                    else if time_between_releases > 48 { 0.5 }
                    else { 0.25 };
                if remaining_frac > most_relevant {
                    most_relevant = remaining_frac;
                }
            }
        }
        debug!("{}: update importance = {most_relevant:0.3}", all.name());
        most_relevant
    }

    #[inline]
    pub async fn most_downloaded_crates(&self, limit: u32) -> CResult<Vec<(Origin, u32)>> {
        Ok(self.crate_db.most_downloaded_crates(limit).await?)
    }

    /// raw number, despammed weight
    #[tracing::instrument(level="debug",skip_all)]
    #[inline(never)]
    pub async fn category_crate_count(&self, slug: &str) -> Result<(u32, f64), KitchenSinkErr> {
        if slug == "uncategorized" {
            return Ok((500, 0.));
        }
        if slug == "std" {
            return Ok((self.index()?.crates_io_crates()?.len() as u32, 0.));
        }
        self.category_crate_counts
            .get_or_init(async { match Box::pin(self.crate_db.category_crate_counts()).await {
                Ok(res) => Some(res),
                Err(err) => {
                    error!("error: can't get category counts: {}", err);
                    None
                },
            }})
            .await
            .as_ref()
            .ok_or(KitchenSinkErr::CategoryQueryFailed)
            .and_then(|h| {
                h.get(slug).copied().ok_or_else(|| {
                    KitchenSinkErr::CategoryNotFound(slug.to_string())
                })
            })
    }

    #[inline]
    async fn repo_commits(&self, repo: &SimpleRepo, as_of_version: &str) -> CResult<Option<Vec<github_info::CommitMeta>>> {
        Ok(self.gh.commits(repo, as_of_version).await?)
    }

    /// Prepare for drop: purge buffers, free memory
    #[tracing::instrument(level="debug",skip_all)]
    pub fn cleanup(&self) {
        debug!("starting cleanup");
        {
            let mut c = self.crate_rustc_compat_cache.write();
            c.clear();
            c.shrink_to(0);
        }
        self.crates_io.cleanup();
        {
            let mut c = self.loaded_rich_crate_version_cache.write();
            c.clear();
            c.shrink_to(0);
        }
        let _ = self.top_crates_cached.lock().map(|mut c| {
            c.clear();
            c.shrink_to(0);
        });
        self.index.clear_cache();
        let _ = self.rustsec.try_write_for(Duration::from_secs(10)).map(|mut r| *r = None);
        self.crev.cleanup();
        // kv save is streamed, so doesn't increase memory use, so it's safe to run all saves in parallel
        rayon::in_place_scope(|s| {
            s.spawn(move |_| {
                self.gh.cleanup();
            });
            s.spawn(move |_| {
                let _ = self.crates_io_owners_cache.save();
                let _ = self.depender_changes.save();
            });
            s.spawn(move |_| {
                let _ = self.stats_histograms.save();
                let _ = self.url_check_cache.save();
                let _ = self.yearly.save();
            });
            s.spawn(move |_| {
                let _ = self.readme_check_cache.save();
                let _ = self.repo_checkout_cache.save();
            });
        });
    }

    #[tracing::instrument(level="debug",skip_all)]
    pub async fn author_by_login(&self, login: &str) -> CResult<RichAuthor> {
        let github = Box::pin(self.user_by_github_login(login)).await?.ok_or_else(|| KitchenSinkErr::AuthorNotFound(login.into()))?;
        Ok(RichAuthor { github })
    }

    #[inline]
    pub fn index(&self) -> Result<&Index, KitchenSinkErr> {
        running()?;
        Ok(&self.index)
    }

    pub fn synonyms(&self) -> &Synonyms {
        &self.synonyms
    }
}

/// Any crate can get such owner. Takes crates_io_login with host prefix
fn not_a_personal_login(login: &str) -> bool {
    // crates-io convention
    let login = login.strip_prefix("github:").and_then(|l| l.split(':').next()).unwrap_or(login);

    login.eq_ignore_ascii_case("aws-sdk-rust-ci") ||
    login.eq_ignore_ascii_case("rust-bus-owner") ||
    login.eq_ignore_ascii_case("rust-lang") ||
    login.eq_ignore_ascii_case("rust-lang-owner") ||
    login.eq_ignore_ascii_case("rust-lang-nursery") ||
    login.eq_ignore_ascii_case("rust-bus") ||
    login.eq_ignore_ascii_case("tauri-bot") ||
    login.eq_ignore_ascii_case("godotrust") || // godot-rust automation
    login.eq_ignore_ascii_case("actions-user") ||
    login.eq_ignore_ascii_case("wasmtime-publish") ||
    login.eq_ignore_ascii_case("nearprotocol-ci") ||
    login.eq_ignore_ascii_case("parity-crate-owner") ||
    login.eq_ignore_ascii_case("bgeron-typosquatting-protect") ||
    login.contains("release-automation") ||
    login.ends_with("-buildbot") ||
    login.ends_with("wasmcloud-automation") ||
    login.ends_with("-release-bot") ||
    login.ends_with("-cratesio-bot") ||
    login.ends_with("-publish-bot") // could it always be -bot?
}

/// foo & libfoo-sys && cargo-foo && rust-foo
fn crate_name_namespace_prefix(crate_name: &str) -> &str {
    crate_name
        .trim_start_matches("rust-")
        .trim_start_matches("cargo-")
        .trim_start_matches("lib")
        .split(|c: char| c == '_' || c == '-').find(|&n| n.len() > 1)
        .unwrap_or(crate_name)
}

impl Drop for KitchenSink {
    fn drop(&mut self) {
        if tokio::runtime::Handle::try_current().map_or(false, |h| h.runtime_flavor() == tokio::runtime::RuntimeFlavor::MultiThread) {
            self.cleanup();
        }
        let n = GLOBAL_INSTANCES.fetch_sub(1, AcqRel);
        info!("ks shutdown {n}");
    }
}

#[derive(Debug, Copy, Clone)]
pub struct VersionPopularity {
    pub pop: f32,
    pub matches_latest: bool,
    pub lost_popularity: bool,
    pub deprecated: bool,
}

#[derive(Debug, Clone)]
pub struct RichAuthor {
    pub github: User,
}

impl RichAuthor {
    #[must_use] pub fn name(&self) -> &str {
        match &self.github.name {
            Some(n) if !n.is_empty() => n.trim(),
            _ => &self.github.login,
        }
    }
}

#[derive(Deserialize, Debug)]
pub struct Rustacean {
    pub name: Option<String>,
    /// email address. Will appear in a mailto link.
    pub email: Option<String>,
    /// homepage URL.
    pub website: Option<String>,
    /// URL for your blog.
    pub blog: Option<String>,
    /// username on Discourse.
    pub discourse: Option<String>,
    /// username on Reddit
    pub reddit: Option<String>,
    /// username on Twitter, including the @.
    pub twitter: Option<String>,
    /// username on Mastodon, including the @.
    pub mastodon: Option<String>,
    /// any notes you lik
    pub notes: Option<String>,
}

impl Rustacean {
    /// URL, label
    #[must_use] pub fn mastodon(&self) -> Option<(String, &str)> {
        let handle = self.mastodon.as_ref()?.trim();
        let (username, host) = handle.trim_start_matches('@').split_once('@')?;
        
        
        Some((format!("https://{host}/@{username}"), handle))
    }
}

#[derive(Debug, Clone)]
pub struct CrateAuthor {
    /// Is identified as owner of the crate by crates.io API?
    pub owner: bool,
    /// owner since
    pub invited_at: Option<DateTime<Utc>>,
    pub invited_by_github_id: Option<u32>,

    /// Order in the Cargo.toml file
    pub nth_author: Option<usize>,
    /// Arbitrary value derived from number of commits. The more the better.
    pub contribution: f64,
    /// From Cargo.toml
    pub info: Option<Author>,
    /// From GitHub API and/or crates.io API
    pub github: Option<User>,

    pub not_a_person: bool,

    /// From kitchen_sink.crate_owners_trust();
    pub trust: Option<f64>,
}

impl CrateAuthor {
    #[must_use]
    pub fn github_login(&self) -> Option<&str> {
        self.github.as_ref().map(|u| u.login.as_str())
    }

    #[must_use] pub fn trusted_name(&self) -> &str {
        let chosen_name = self.name();
        if self.trust.unwrap_or(0.) > 60. {
            return chosen_name;
        }
        if let Some(gh) = &self.github {
            // allow users to capitalize their name as they wish
            if gh.login.eq_ignore_ascii_case(chosen_name) {
                return chosen_name;
            }

            if let Some(created) = gh.created_at.as_deref().and_then(|d| DateTime::parse_from_rfc3339(d).ok()) {
                let created = created.with_timezone(&Utc).date_naive();
                let today = Utc::now().date_naive();
                if today.signed_duration_since(created).num_days() > 365*2 {
                    return chosen_name;
                }
            }

            debug!("{} account is too new to display name {}", gh.login, self.name());
            return &gh.login;
        }
        chosen_name
    }

    #[must_use] pub fn name(&self) -> &str {
        if let Some(name) = self.github.as_ref().and_then(|g| g.name.as_deref()) {
            if !name.trim_start().is_empty() {
                return name;
            }
        }
        if let Some(name) = self.info.as_ref().and_then(|i| i.name.as_deref()) {
            if !name.trim_start().is_empty() {
                return name;
            }
        }
        if let Some(ref gh) = self.github {
            &gh.login
        } else if let Some(email) = self.info.as_ref().and_then(|i| i.email.as_deref()) {
            email.split('@').next().unwrap()
        } else {
            debug!("no name for user {self:?}?");
            "(unknown crate author)"
        }
    }
}

#[derive(Debug, Clone, Copy, Serialize, Hash, Deserialize, Ord, Eq, PartialEq, PartialOrd)]
pub struct MiniDate {
    /// year
    y: u16,
    /// ordinal (day of year)
    o: u16,
}

impl MiniDate {
    #[must_use] pub fn new(from: NaiveDate) -> Self {
        Self {
            y: from.year() as u16,
            o: from.ordinal0() as u16,
        }
    }

    /// Screw leap years
    #[must_use] pub fn days_later(self, days: i32) -> Self {
        let n = i32::from(self.y) * 365 + i32::from(self.o) + days;
        let y = (n/365) as u16;
        let o = (n%365) as u16;
        Self {
            y, o
        }
    }

    #[must_use] pub fn half_way(self, other: Self) -> Self {
        let diff = (i32::from(other.y) - i32::from(self.y)) * 365 + (i32::from(other.o) - i32::from(self.o));
        self.days_later(diff / 2)
    }

    // We're going to use weirdo 30-day months, with excess going into december
    // which makes data more even and pads the December's holiday drop a bit
    #[must_use] pub fn fake_month(&self) -> (u16, u16) {
        (self.y, (self.o / 30).min(11))
    }
}

#[test]
fn minidate() {
    let a = MiniDate::new(NaiveDate::from_ymd_opt(2020, 2, 2).unwrap());
    let b = MiniDate::new(NaiveDate::from_ymd_opt(2024, 4, 4).unwrap());
    let c = MiniDate::new(NaiveDate::from_ymd_opt(2022, 3, 5).unwrap());
    assert_eq!(c, a.half_way(b));

    let d = MiniDate::new(NaiveDate::from_ymd_opt(1999, 12, 31).unwrap());
    let e = MiniDate::new(NaiveDate::from_ymd_opt(2000, 1, 1).unwrap());
    assert_eq!(e, d.days_later(1));
    assert_eq!(d, e.days_later(-1));
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct DependerChanges {
    pub at: MiniDate,
    pub added: u16,
    /// Crate has released a new version without this dependency
    pub removed: u16,
    /// Crate has this dependnecy, but is not active any more
    pub expired: u16,

    /// Already aggregated number of users
    pub users_abs: u16,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct CachedCrate {
    pub manifest: Manifest,
    pub derived: Derived,
    pub cache_key: u64,
    #[serde(default)]
    pub warnings: HashSet<Warning>,
}

#[test]
fn is_build_or_dev_test() {
    let rt = tokio::runtime::Runtime::new().unwrap();
    rt.block_on(rt.spawn(async move {
        let c = KitchenSink::new_default().await.expect("uhg");
        assert_eq!((false, false), c.is_build_or_dev(&Origin::from_crates_io_name("semver")).expect("test1"));
        assert_eq!((false, true), c.is_build_or_dev(&Origin::from_crates_io_name("version-sync")).expect("test2"));
        assert_eq!((true, false), c.is_build_or_dev(&Origin::from_crates_io_name("cc")).expect("test3"));
    })).unwrap();
}

#[test]
fn fetch_uppercase_name_and_tarball() {
    let rt = tokio::runtime::Runtime::new().unwrap();
    rt.block_on(rt.spawn(async move {
        let k = KitchenSink::new_default().await.expect("Test if configured");
        let _ = k.rich_crate_async(&Origin::from_crates_io_name("Inflector")).await.unwrap();
        let _ = k.rich_crate_async(&Origin::from_crates_io_name("inflector")).await.unwrap();

        let index = k.index().unwrap();
        let testk = index.crates_io_crates().unwrap().get("dssim-core").unwrap().versions()[8].version().to_string();
        let meta = k.crate_files_summary_from_crates_io_tarball("dssim-core", &testk).await.unwrap();
        assert_eq!(meta.path_in_repo.as_deref(), Some("dssim-core"), "{meta:#?}");
        assert_eq!(meta.vcs_info_git_sha1.as_ref().unwrap(), b"\xba\x0a\x40\xd1\x3b\x1d\x11\xb0\x19\xf6\xb6\x6a\x77\x2e\xbd\xa7\xd0\xf9\x45\x0c");
    })).unwrap();
}

#[tokio::test(flavor = "multi_thread")]
async fn index_test() {
    let idx = Index::new(&KitchenSink::data_path().unwrap()).unwrap();
    let stats = idx.deps_stats().unwrap();
    assert!(stats.total > 13800);
    let lode = stats.counts.get("lodepng").unwrap();
    assert!(lode.counters.runtime.def >= 14 && lode.counters.runtime.def < 50, "{lode:?}");
}

fn is_alnum(q: &str) -> bool {
    q.as_bytes().iter().copied().all(|c| c.is_ascii_alphanumeric() || c == b'_' || c == b'-')
}

struct DropWatch<'a>(bool, &'a str);
impl Drop for DropWatch<'_> {
    fn drop(&mut self) {
        if !self.0 {
            log::warn!("Aborted: {}", self.1);
        }
    }
}

#[track_caller]
fn watch<'a, T: 'a>(label: &'a str, f: impl Future<Output = T> + Send + 'a) -> Pin<Box<dyn Future<Output = T> + Send + 'a>> {
    debug!("starting: {}", label);
    Box::pin(blocking::awatch(label, NonBlock::new(label, async move {
        let mut is_ok = DropWatch(false, label); // await dropping will run this
        tokio::task::yield_now().await;
        let res = f.await;
        is_ok.0 = true;
        res
    })))
}

#[track_caller]
fn timeout<'a, T: 'a, E: From<KitchenSinkErr> + 'a>(label: &'a str, time: u16, f: impl Future<Output = Result<T, E>> + Send + 'a) -> Pin<Box<dyn Future<Output = Result<T, E>> + Send + 'a>> {
    // yield in case something is blocking-busy and would cause a timeout just by starving the runtime
    let f = tokio::task::yield_now()
        .then(move |_| tokio::time::timeout(Duration::from_secs(time.into()), f));
    watch(label, f.map(move |r| {
        running()?;
        r.map_err(|_| {
            info!("Timed out: {label} {time}s");
            E::from(KitchenSinkErr::TimedOut(label.into(), time))
        }).and_then(|x| x)
    }))
}

#[track_caller]
fn deadline<'a, T: 'a, E: From<KitchenSinkErr> + 'a>(label: &'a str, deadline: Instant, f: impl Future<Output = Result<T, E>> + Send + 'a) -> Pin<Box<dyn Future<Output = Result<T, E>> + Send + 'a>> {
    let time_left = deadline.saturating_duration_since(Instant::now()).as_secs() as u16;
    let f = tokio::task::yield_now()
        .then(move |_| tokio::time::timeout_at(deadline, f));
    watch(label, f.map(move |r| {
        running()?;
        r.map_err(|_| {
            info!("Deadline missed: {label} after {time_left}s");
            E::from(KitchenSinkErr::TimedOut(label.into(), time_left))
        }).and_then(|x| x)
    }))
}

#[derive(Copy, Clone, Eq, PartialEq)]
pub enum CrateOwners {
    /// Includes guesswork from audit log and rust-bus
    All,
    /// Only actual owners, no guesses, no rust-bus
    Strict,
}

#[test]
fn rustc_rel_dates() {
    static RUST_RELEASE_DATES: [(u16,u8,u8, u16); 71] = [
        (2015,05,15, 0), //.0
        (2015,06,25, 1), //.0
        (2015,08,07, 2), //.0
        (2015,09,17, 3), //.0
        (2015,10,29, 4), //.0
        (2015,12,10, 5), //.0
        (2016,01,21, 6), //.0
        (2016,03,03, 7), //.0
        (2016,04,14, 8), //.0
        (2016,05,26, 9), //.0
        (2016,07,07, 10), //.0
        (2016,08,18, 11), //.0
        (2016,09,29, 12), //.0
        (2016,10,20, 12), //.1
        (2016,11,10, 13), //.0
        (2016,12,22, 14), //.0
        (2017,02,02, 15), //.0
        (2017,02,09, 15), //.1
        (2017,03,16, 16), //.0
        (2017,04,27, 17), //.0
        (2017,06,08, 18), //.0
        (2017,07,20, 19), //.0
        (2017,08,31, 20), //.0
        (2017,10,12, 21), //.0
        (2017,11,22, 22), //.0
        (2017,11,22, 22), //.1
        (2018,01,04, 23), //.0
        (2018,02,15, 24), //.0
        (2018,03,01, 24), //.1
        (2018,03,29, 25), //.0
        (2018,05,10, 26), //.0
        (2018,05,29, 26), //.1
        (2018,06,05, 26), //.2
        (2018,06,21, 27), //.0
        (2018,07,10, 27), //.1
        (2018,07,20, 27), //.2
        (2018,08,02, 28), //.0
        (2018,09,13, 29), //.0
        (2018,09,25, 29), //.1
        (2018,10,11, 29), //.2
        (2018,10,25, 30), //.0
        (2018,11,08, 30), //.1
        (2018,12,06, 31), //.0
        (2018,12,20, 31), //.1
        (2019,01,17, 32), //.0
        (2019,02,28, 33), //.0
        (2019,04,11, 34), //.0
        (2019,04,25, 34), //.1
        (2019,05,14, 34), //.2
        (2019,05,23, 35), //.0
        (2019,07,04, 36), //.0
        (2019,08,15, 37), //.0
        (2019,09,20, 38), //.0
        (2019,11,07, 39), //.0
        (2019,12,19, 40), //.0
        (2020,01,30, 41), //.0
        (2020,02,27, 41), //.1
        (2020,03,12, 42), //.0
        (2020,04,23, 43), //.0
        (2020,05,07, 43), //.1
        (2020,06,04, 44), //.0
        (2020,06,18, 44), //.1
        (2020,07,16, 45), //.0
        (2020,07,30, 45), //.1
        (2020,08,03, 45), //.2
        (2020,08,27, 46), //.0
        (2020,10,08, 47), //.0
        (2020,11,19, 48), //.0
        (2020,12,31, 49), //.0
        (2021,02,11, 50), //.0
        (2021,03,25, 51), //.0
    ];
    for (y,m,d, ver) in RUST_RELEASE_DATES.iter().copied() {
        let date = Utc.with_ymd_and_hms(i32::from(y),u32::from(m),u32::from(d),0, 0, 0).unwrap();
        assert_eq!(ver, KitchenSink::rustc_release_from_date(&date).unwrap());
    }
}

fn is_valid_crates_io_category_not_on_lib_rs(slug: &str) -> bool {
    matches!(slug,
    "aerospace::drones" |
    "aerospace::protocols" |
    "aerospace::simulation" |
    "aerospace::space-protocols" |
    "aerospace::unmanned-aerial-vehicles" |
    "aerospace" |
    "api-bindings" |
    "computer-vision" |
    "external-ffi-bindings" |
    "game-engines" |
    "graphics" |
    "no-std::no-alloc" |
    "localization" |
    "mathematics" |
    "multimedia::encoding" |
    "os::freebsd-apis" |
    "os::linux-apis" |
    "virtualization" |
    "science::neuroscience")
}


#[test]
#[allow(unused)]
fn send_futures() {
    fn is_send<T: Send>(_: T) {}

    fn bust_send(k: &KitchenSink, r: &Repo) {
        is_send(k.cachebust_string_for_repo(r));
    }
    fn fetch_send(k: &KitchenSink, r: &Origin) {
        is_send(k.fetch_rich_crate_version_data(r));
    }
}
