// use std::sync::atomic::AtomicU64;
use std::sync::atomic::AtomicBool;

/// Context for page renders
#[derive(Debug, Default)]
pub struct RendCtx {
    incomplete: AtomicBool,
    // last_modified_timestamp: AtomicU64,
}

impl RendCtx {
    pub fn new() -> Self { Self::default() }

    pub fn set_incomplete(&self) {
        self.incomplete.store(true, std::sync::atomic::Ordering::Relaxed)
    }

    pub fn is_incomplete(&self) -> bool {
        self.incomplete.load(std::sync::atomic::Ordering::Relaxed)
    }
}

