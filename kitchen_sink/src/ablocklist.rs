use util::CowAscii;
use crate::CrateOwner;
use ahash::HashMap;
use util::SmolStr;
use std::path::Path;
use std::io;

pub enum ABlockReason {
    /// Their crates are considered spam, malware, or otherwise junk.
    /// reason
    Banned(Box<str>),
    /// Their crates are OK, but they don't want to be on the site.
    /// reason, url
    Hidden(Box<str>, Option<SmolStr>),
}

#[derive(Debug, Clone)]
pub enum ABlockAction<'a> {
    Ban { why: &'a str, login: Box<str> },
    Redirect,
    Block(&'a str, Option<&'a str>),
}

impl<'a> ABlockAction<'a> {
    #[inline]
    pub fn is_ban(&self) -> bool {
        matches!(self, ABlockAction::Ban { .. })
    }

    #[inline]
    pub fn is_block(&self) -> bool {
        matches!(self, ABlockAction::Block(..))
    }

    #[inline]
    pub fn is_redirect(&self) -> bool {
        matches!(self, ABlockAction::Redirect)
    }
}

pub struct ABlockList {
    by_lc_github_login: HashMap<SmolStr, ABlockReason>,
}

impl ABlockList {
    pub fn new(path: &Path) -> io::Result<Self> {
        let list = std::fs::read_to_string(path)?;

        Ok(Self {
            by_lc_github_login: Self::parse_list(&list)?,
        })
    }

    #[must_use]
    pub fn actions_for_owners<'a>(&'a self, owners: &[CrateOwner]) -> (bool, Vec<ABlockAction<'a>>) {
        let mut all = true;
        let actions: Vec<_> = owners.iter()
            .filter_map(|owner| {
                match self.get(&owner.crates_io_login) {
                    None => {
                        all = false;
                        None
                    },
                    Some(why) => {
                        Some((why, owner))
                    },
                }
            })
            .map(|(a, owner)| match a {
                ABlockReason::Banned(s) => ABlockAction::Ban { why: s, login: owner.github_login().unwrap_or_default().into() },
                ABlockReason::Hidden(s, l) if !s.is_empty() => ABlockAction::Block(s, l.as_deref()),
                ABlockReason::Hidden(..) => ABlockAction::Redirect,
            })
            .collect();

        // some crates are co-owned by both legit and banned owners,
        // so banning by "any" would interfere with legit users' usage,
        (all, actions)
    }

    #[must_use] pub fn get(&self, username: &str) -> Option<&ABlockReason> {
        self.by_lc_github_login.get(&*username.as_ascii_lowercase())
    }

    fn parse_list(list: &str) -> io::Result<HashMap<SmolStr, ABlockReason>> {
        let mut out = HashMap::default();
        for (n, l) in list.lines().enumerate() {
            let line = l.trim();
            if line.is_empty() || line.starts_with('#') {
                continue;
            }
            let (k, v) = Self::parse_line(line).ok_or_else(|| io::Error::new(io::ErrorKind::Other, format!("ablocklist line {n} is borked: {line}")))?;
            out.insert(k, v);
        }
        Ok(out)
    }

    fn parse_line(line: &str) -> Option<(SmolStr, ABlockReason)> {
        let mut parts = line.splitn(4, ',');
        let username = parts.next()?.trim();
        debug_assert_eq!(username, username.as_ascii_lowercase());
        let kind = parts.next()?.trim();
        let url = parts.next()?.trim();
        let reason = parts.next()?.trim();

        let b = match kind {
            "b" => ABlockReason::Banned(reason.into()),
            "h" => ABlockReason::Hidden(reason.into(), (!url.is_empty()).then(|| url.into())),
            _ => return None,
        };
        Some((username.into(), b))
    }
}
