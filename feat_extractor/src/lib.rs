use ahash::HashMap;
use ahash::HashMapExt;
use ahash::HashSet;
use categories::normalize_keyword;
use heck::ToSnakeCase;
use itertools::Itertools;
use log::debug;
use once_cell::sync::Lazy;
use once_cell::sync::OnceCell;
use rich_crate::MaintenanceStatus;
use rich_crate::Manifest;
use rich_crate::Markup;
use rich_crate::RichCrateVersion;
use semver::VersionReq;
use util::SmolStr;
use util::pick_top_n_unstable_by;
use std::io;
use std::path::Path;
use std::path::PathBuf;
use synscrape::ItemKind;
use synscrape::ModuleScrape;
use synscrape::TextFromSourceCode;
use util::CowAscii;
use util::smol_fmt;

pub mod wlita;

/// ignore these as keywords
pub(crate) static IDENT_STOPWORDS: Lazy<HashSet<&str>> = Lazy::new(|| [
    "ref", "name", "entry", "iter", "new", "test", "self", "this", "struct", "vec", "string", "str", "raw", "ptr", "pos", "len",
    "u8", "f32", "f64", "u32", "usize", "impls", "impl", "bool", "cow", "count", "fmt", "borrow", "none", "delegate", "void",
    "max", "map", "tests", "build", "panic", "id", "print", "help", "usage", "main", "output", "do", "args", "cb", "callback", "function",
    "extern", "ext", "result", "error", "mut", "try", "bench", "add", "remove", "boxed", "box", "slice", "safe", "unsafe", "start", "run",
    "next", "reason", "source", "value", "values", "demo", "mod", "match", "fn", "common", "inner", "assign", "vec", "neg", "mul", "sub", "add",
    "saturating", "check", "copy", "checked", "wrapping", "invert", "size", "err", "ok", "uninit", "init", "message", "data", "static", "total", "sum",
    "unwind", "skip", "expand", "buffer", "buf", "exhaustive", "open", "close", "fix", "enumerate", "extend", "extended", "unchecked",
    "const", "update", "full", "normal", "override", "type", "kind", "cmp", "issue", "null", "unknown", "list", "feature", "filter",
    "handle", "enabled", "maximum", "minimum", "refs", "parent", "child", "children", "hint", "forget", "underlying", "util", "utils",
    "old", "remove", "insert", "apply", "all", "any", "newtype", "wrapper", "advanced", "call", "clone", "boolean", "returns", "recent",
    "internal", "private", "unwrap", "let", "wrong", "nested", "empty", "example", "start", "end", "nonexhaustive", "opts", "prelude",
    "default", "enabled", "rust", "after", "done", "partial-ord", "out", "pub", "structs", "struct", "usize", "extern", "assert",
    "equals", "two", "plus", "four",
    ].into_iter().collect());

pub(crate) static STOPWORDS: Lazy<HashSet<&str>> = Lazy::new(|| [
    "a", "sys", "ffi", "placeholder", "app", "loops", "master", "library", "rs",
    "accidentally", "additional", "adds", "against", "all", "allow", "allows",
    "already", "also", "alternative", "always", "an", "and", "any", "appropriate",
    "arbitrary", "are", "as", "at", "available", "based", "be", "because", "been",
    "both", "but", "by", "can", "certain", "changes", "comes", "contains", "core", "cost",
    "crate", "crates.io", "current", "currently", "custom", "dependencies",
    "dependency", "developers", "do", "don't", "e.g", "easily", "easy", "either",
    "enables", "etc", "even", "every", "example", "examples", "features", "feel",
    "files", "for", "from", "fully", "function", "get", "given", "had", "has",
    "have", "here", "if", "implementing", "implements", "in", "includes",
    "including", "incurring", "installation", "interested", "into", "is", "it",
    "it's", "its", "itself", "just", "known", "large", "later", "library",
    "license", "lightweight", "like", "made", "main", "make", "makes", "many",
    "may", "me", "means", "method", "minimal", "mit", "more", "mostly", "much",
    "need", "needed", "never", "new", "no", "noop", "not", "of", "on", "one",
    "only", "or", "other", "over", "plausible", "please", "possible", "program",
    "provides", "put", "readme", "release", "runs", "rust", "rust's", "same",
    "see", "selected", "should", "similar", "simple", "simply", "since", "small", "so",
    "some", "specific", "still", "stuff", "such", "take", "than", "that", "the",
    "their", "them", "then", "there", "therefore", "these", "they", "things",
    "this", "those", "to", "todo", "too", "travis", "two", "under", "us",
    "usable", "use", "used", "useful", "using", "usage", "v1", "v2", "v3", "v4", "various",
    "very", "via", "want", "way", "well", "we'll", "what", "when", "where", "which",
    "while", "will", "wip", "with", "without", "working", "works", "writing",
    "written", "yet", "you", "your", "build status", "meritbadge", "common",
    "file was generated", "easy to use", "blazingly fast", "built with rust",
    "visitor", "visit", "pure-rust", "evenly", "maybe", "off", "peculiar",
    "within", "misc", "outside", "description", "github-issue", "wanted", "hence",
    "needs", "created", "previous", "initialized", "prior", "won-t", "don-t",
    "ambiguous", "matches", "rust-lang", "how", "however", "provided", "return", "most",
    "instead", "along", "consisting", "correctly", "vs", "few", "uses", "does",
    "dont", "doesnt", "thus", "super-easy", "would", "thanks", "our", "goes", "hello-world",
    "must", "cannot", "another", "unwrap", "thank", "yourself", "implementations", "was",
    ].into_iter().collect());

// drops boilerplate sections
#[must_use] pub fn filter_sections_by_relevance<'a>(manifest: &Manifest, readme_by_section: &'a [(String, String)]) -> Vec<(f32, &'a str)> {
    let mut out = Vec::with_capacity(readme_by_section.len());

    let own_name = manifest.package().name();
    let install_boilerplate1 = format!("cargo install {own_name}");
    let install_boilerplate2 = format!("cargo add {own_name}");
    let install_boilerplate3 = format!("{own_name} = ");
    let mut seen_text = HashSet::default();
    for (section_orig, text) in readme_by_section.iter() {
        if !seen_text.insert(text) {
            continue;
        }
        let section_s = section_orig.trim().to_lowercase();
        let section = section_s
            .trim_start_matches(|c: char| c as u32 > 0xFFFF) // strip decorative emoji
            .trim_start_matches("the ").trim_start_matches("rust ")
            .trim_end_matches('s') // singular
            .trim_end_matches(" crate");

        // skip boilerplate
        if section.starts_with("minimum supported rust") || section.starts_with("minimum rust version") {
            continue;
        }
        if matches!(section, "license" | "contact" | "todo" | "get help" | "code formatting" | "contribution" | "contributing" | "copyright" | "sponsor" | "financial support" | "license and link" |
            "semantic versioning" | "compatibility policy" | "maintenance" | "building" | "msrv" | "out of scope" | "minimum rustc" |
             "version compatibility" | "build process" | "version support" | "tests" | "code of conduct" | "version policy" | "release schedule" | "supported rust version" | "get it from crates.io" | "install rust") {
            continue;
        }
        let relevance = if matches!(section, "info" | "how does it work" | "use case" | "tldr" | "motivation" | "summary" | "description" | "introduction" | "main feature" | "overview" | "how it work") || section.starts_with("about") || section == own_name { 1.0 }
        else if matches!(section, "feature flag" | "feature" | "" | "basic example") || section.starts_with("example") || section.starts_with("usage example") { 0.6 }
        else if matches!(section, "usage" | "how to use" | "how to use it") { 0.5 }
        else if matches!(section, "getting started" | "quickstart" | "documentation") || section.starts_with("compiling") { 0.3 }
        else if section.starts_with("install") || section.starts_with("contributing") || section.starts_with("license") || section_s.starts_with("rust version") ||
            matches!(section, "change" | "开发示例" | "ci" | "table of content" | "setup" | "dependencie" | "limitation" | "safety" |
            "alternative" | "other project" | "related project" | "credit" | "participating" | "acknowledgement" | "changelog" | "benchmark" | "platform" | "rust version" | "getting help" |
            "release history" | "troubleshooting" | "testing" | "current state" | "authors and acknowledgment" | "badge" | "building from source" | "see also" |
            "author" | "warning" | "versioning" | "disclaimer" | "requirement" | "recent change" | "contact") { 0.1 }
        else {
            out.push((if section_s.contains(own_name) {1.} else {0.1}, section_orig.as_str()));
            0.4
        };

        // render readme to DOM, extract nodes
        for par in text.split('\n') {
            let par_orig = par.trim_start_matches(|c: char| c.is_whitespace() || c == '#' || c == '=' || c == '*' || c == '-');
            let par = par_orig.as_ascii_lowercase();
            let par = &*par;
            if par == "cargo test" || par == "cargo build" || par.starts_with("cargo build ") {
                continue;
            }
            if par.starts_with("[dependencies") || par.contains(&install_boilerplate1) || par.contains(&install_boilerplate2) || par.starts_with(&install_boilerplate3) {
                continue;
            }
            if par.starts_with("licensed under either of") || par.starts_with("license:") || par.starts_with("this code is licensed under")
            || par.starts_with("unless you explicitly state otherwise, any contribution") || par.contains("apache license 2.0")
            || par.starts_with("add to cargo.toml") || par.starts_with("at your option") {
                continue;
            }
            if !par.trim_start().is_empty() {
                out.push((relevance, par_orig));
            }
        }
    }
    out
}

// returns an array of lowercase phrases
#[must_use] pub fn extract_and_normalize_text_phrases(manifest: &Manifest, github_description: Option<&str>, readme_by_section: &[(f32, &str)]) -> Vec<(f32, String)> {
    let mut out = Vec::with_capacity(2 + readme_by_section.len());

    if let Some(s) = manifest.package().description() {
        out.push((1.0f32, s));
    }
    if let Some(s) = github_description {
        out.push((1., s));
    }
    out.extend(readme_by_section);

    // stable sort is needed to preserve readme order
    out.sort_by(|a,b| b.0.total_cmp(&a.0));
    let mut len = 0;
    out.retain_mut(|(v, par)| if (len > 2500 && *v < 0.4) || len > 6000 {
            false
        } else {
            len += par.len();
            true
        });

    out.into_iter().map(|(v, line)| {
        let line = line.to_lowercase()
            .replace("http://", " ").replace("https://", " ")
            .replace("rust programming language", "rust");
        let mut tmp = line.trim_start_matches(|c:char| !c.is_alphanumeric());
        // trim boilerplate "this is a crate for …"
        while let Some((word, rest)) = tmp.split_once(' ') {
            if STOPWORDS.contains(word) || matches!(word,  "i" | "tool" | "was" | "going" | "software" | "package" |  "lets" |  "integrates" | "facilitates" |
              "designed" | "let" | "provide" | "support" | "for" | "help" | "helps" | "methods" | "functions" | "convenient") {
                tmp = rest;
            } else { break; }
        }
        (v, tmp.to_string())
    })
    .collect()
}

/// (names, docstrings)
#[must_use] pub fn extract_text_from_source_code(modules: Vec<ModuleScrape>) -> TextFromSourceCode {
    let mut names = HashMap::with_capacity(modules.len() * 2);
    let mut texts = Vec::with_capacity(modules.len() * 2);

    for m in modules {
        for item in m.items {
            // ModuleScrape adds name of struct or trait before '.'
            let (prefix, name) = item.name.rsplit_once('.')
                .map(|(prefix, name)| (if !item.is_test { Some(categories::normalize_keyword(prefix)).filter(|w| !IDENT_STOPWORDS.contains(&**w)) } else { None }, name))
                .unwrap_or((None, item.name.as_str()));
            let prefix = prefix.as_deref().map(chop3words);
            let weight = if item.public { 4u32 } else { 2 } + match item.kind {
                ItemKind::Macro => 1, // Dunno, these are guesses
                ItemKind::Module => 1, // Dunno, these are guesses
                ItemKind::Fn => 4,
                ItemKind::Const => 2,
                ItemKind::Struct => 2,
                ItemKind::Trait => 1,
                ItemKind::Type => 2,
                ItemKind::Field => 3,
            };
            let name = name.to_snake_case();
            let name = ["_", "get_", "set_", "is_", "as_", "read_", "non_", "write_", "partial_", "max_", "num_", "bench_", "test_", "async_",  "should_", "my_", "to_", "try_", "into_", "from_", "map_", "with_", "new_"].into_iter().fold(name.as_str(), |name, pref| name.strip_prefix(pref).unwrap_or(name));
            let name = ["_", "_ref", "_mut", "_async", "_t", "_get", "_opt", "_generic", "_eq", "_ext", "_str", "_iter", "_bench", "_test", "_slice", "_only", "_tests", "_at"].into_iter().fold(name, |name, pref| name.strip_suffix(pref).unwrap_or(name));

            let mut prev_word = None;
            for word in name.split('_').filter(|w| !w.is_empty() && !STOPWORDS.contains(w) && !IDENT_STOPWORDS.contains(w) && w.bytes().any(|b| !b.is_ascii_digit())) {
                if word.len() > 1 {
                    *names.entry(SmolStr::from(word)).or_insert(0) += 2 * weight;
                }
                if let Some(prev) = prev_word {
                    if prev != word {
                        *names.entry(smol_fmt!("{prev}-{word}")).or_insert(0) += 4 * weight;
                    }
                }
                if let Some(prefix) = prefix {
                    if !prefix.contains(word) {
                        *names.entry(smol_fmt!("{prefix}-{word}")).or_insert(0) += 3 * weight;
                    }
                }
                prev_word = Some(word);
            }

            if let Some(doc) = item.doc {
                texts.push((if item.is_test { 1 } else { 2 } * weight, doc));
            }
        }
        *names.entry(m.file_stem).or_insert(0) += 1;
        if names.len() > 10000 {
            break;
        }
    }

    let mut names = names.into_iter().map(|(k, v)| (v, k)).collect::<Vec<_>>();
    let limit = names.len()/2 + 150;
    pick_top_n_unstable_by(&mut names, limit, |a, b| b.0.cmp(&a.0));

    TextFromSourceCode {
        idents: names,
        doc: texts,
    }
}

#[must_use] pub fn auto_keywords(ranked_phrases: &[(f32, String)]) -> Vec<(f32, SmolStr)> {
    // if we have very little input text, then keywords will suck, so make them all less certain
    let input_data_amount = ranked_phrases.iter().map(|(w, text)| *w * text.len() as f32).sum::<f32>();
    let overall_certainity = 0.5 + ((input_data_amount+1.).sqrt() / 15.).min(0.5);

    debug!("guessing auto_keywords from phrases: dat={input_data_amount:0.2} maxq={overall_certainity:0.2} {ranked_phrases:?}");

    let mut sw = rake::StopWords::new();
    sw.reserve(STOPWORDS.len());
    sw.extend(STOPWORDS.iter().map(|s| (*s).to_string())); // TODO: use real stopwords, THEN filter via STOPWORDS again, because multiple Rust-y words are fine
    // normalize space and _ to -
    let r = rake::Rake::new(sw);
    let rake_keywords = r.run_fragments(ranked_phrases.iter().map(|(_, s)| s.as_str()));
    let rake_keywords = rake_keywords.iter()
        .map(|k| (
            k.score.min(1.1), //
            normalize_keyword(chop3words(k.keyword.as_str())) // rake generates very long setences sometimes
        ));
    // split on / and punctuation too

    let mut keywords_by_pop = HashMap::new();
    ranked_phrases.iter()
        .flat_map(|&(weight, ref line)| line.split(|c: char| c.is_ascii_whitespace() || matches!(c, '.' | '(' | ')' | ',' | '"'))
            .map(|word| word.trim_matches(|c: char| c.is_ascii_punctuation()).trim_end_matches("'s"))
            .filter(|word| word.len() >= 2 && !STOPWORDS.contains(word))
            .map(move |word| (weight, word)))
        .for_each(|(w2, s)| { *keywords_by_pop.entry(s).or_insert(0.) += w2; });
    let mut keywords_by_pop: Vec<_> = keywords_by_pop.into_iter().collect();
    keywords_by_pop.sort_by(|a,b| b.1.total_cmp(&a.1));
    let max = keywords_by_pop.first().map(|&(_, w)| w).unwrap_or(0.).max(1.);
    let keywords = keywords_by_pop.into_iter().map(|(k, w)| (w / max, normalize_keyword(k)));

    let auto_keywords = rake_keywords.map(move |(k,v)| (overall_certainity * k as f32, v)).take(15).chain(keywords).take(30).collect();
    debug!("auto_keywords from phrases = {auto_keywords:?}");
    auto_keywords
}

fn chop3words(s: &str) -> &str {
    let mut words = 0;
    for (pos, ch) in s.char_indices() {
        if ch == ' ' || ch == '-' {
            words += 1;
            if words >= 2 {
                return &s[0..pos];
            }
        }
    }
    s
}

/// Returns reason
#[must_use]
pub fn is_deprecated(k: &RichCrateVersion) -> Option<&'static str> {
    if k.version().contains("deprecated") || k.version() == "0.0.0" || k.version() == "0.0.1" {
        return Some("version");
    }
    if k.maintenance() == MaintenanceStatus::Deprecated {
        return Some("Cargo.toml badge");
    }
    if k.repository().and_then(|r| r.url_owner_name()).map_or(false, |r| r == "rust-lang-deprecated") {
        return Some("deprecated-repo");
    }

    if let Some(orig_desc) = k.description() {
        let orig_desc = orig_desc.trim_matches(|c: char| !c.is_ascii_alphabetic());
        let desc = orig_desc.as_ascii_lowercase();
        let desc = &*desc;
        let is_described_as =
            orig_desc.starts_with("Please use `gix-") ||
            desc.starts_with("deprecated") ||
            desc.starts_with("this crate was renamed") ||
            desc.starts_with("this crate is deprecated") ||
            desc.starts_with("this package was renamed") ||
            desc.starts_with("obsolete") ||
            desc.starts_with("unsafe and deprecated") ||
            desc.starts_with("crate is abandoned") ||
            desc.starts_with("abandoned") ||
            desc.contains("this crate is abandoned") ||
            desc.contains("this crate has been abandoned") ||
            desc.starts_with("do not use") ||
            desc.starts_with("temp crate do not use") ||
            desc.contains("this is a dummy package") ||
            desc.starts_with("an empty crate") ||
            desc.starts_with("discontinued") ||
            desc.starts_with("very early wip") ||
            desc.starts_with("renamed to ") ||
            desc.starts_with("crate renamed to ") ||
            desc.contains("this tool is abandoned") ||
            desc.ends_with("please dont use") ||
            desc.ends_with("deprecated") ||
            desc.contains("deprecated in favor") ||
            desc.contains("project is deprecated");
        if is_described_as {
            return Some("description-deprecated");
        }
        let is_unmaintained =
            desc.starts_with("unmaintained ") ||
            desc.contains("no longer maintained") ||
            desc.starts_with("temporary fork") ||
            desc.starts_with("unfinished") ||
            desc.starts_with("an unfinished") ||
            desc.starts_with("unmaintained! ");
        if is_unmaintained {
            return Some("description-unmaintained");
        }
    }
    if let ok @ Some(_) = is_squatspam(k) {
        return ok;
    }
    if let Ok(req) = k.version().parse() {
        if is_deprecated_requirement(k.short_name(), &req) {
            return Some("known-bad-crate");
        }
    }
    None
}

#[must_use]
pub fn is_bad_dependency(name: &str, _requirement: &VersionReq) -> bool {
    match name {
        // fundamentally unsound
        "str-concat" => true,
        "extprim" => true, // does not compile due to const_fn unstable feature
        "traitobject" | "unsafe-any" => true, // vulnerable
        "typemap" => true, // 2015
        "bitcoin" => true, // this will lower ranking of crates using it (please stop using PoW)
        "rustfmt" | "clippy" => true, // rustup
        _ => false,
    }
}

#[must_use] pub fn is_deprecated_requirement(name: &str, requirement: &VersionReq) -> bool {
    let v02 = "0.2.99".parse().unwrap();
    let v01 = "0.1.99".parse().unwrap();
    let v4 = "4.99.99".parse().unwrap();
    match name {
        "time" | "tokio" | "futures" | "opengl32-sys" | "uuid-sys" if requirement.matches(&v01) => true,
        "winapi" | "winmm-sys" if requirement.matches(&v01) || requirement.matches(&v02) => true,
        "tokio" | "secur32-sys" if requirement.matches(&v02) => true,
        "nom" if requirement.matches(&v4) => true, // future-incompat problem
        "rustc-serialize" | "rmp-serialize" | "gcc" | "rustc-benchmarks" | "rust-crypto" |
        "flate2-crc" | "complex" | "simple_stats" | "concurrent" | "feed" |
        "isatty" | "thread-scoped" | "target_build_utils" | "chan" | "chan-signal" |
        "glsl-to-spirv" => true,
        // futures 0.1
        "futures-preview" | "futures-tls" | "futures-mio" | "futures-channel-preview" | "futures-core-preview" |
        "tokio-io" | "tokio-timer" | "tokio-codec" | "futures-stable" | "futures-async-runtime" | "futures01" |
        "tokio-executor" | "tokio-reactor" | "tokio-signal" | "tokio-core" | "futures-cpupool" |
        "tokio-compat" | "tokio-async-await" | "tokio-serde-json" | "tokio-udp" | "tokio-service" |
        "tokio-threadpool" | "tokio-tcp" | "tokio-current-thread" => true,
        // uses old winapi
        "user32-sys" | "shell32-sys" | "advapi32-sys" | "gdi32-sys" | "ole32-sys" | "ws2_32-sys" | "kernel32-sys" | "userenv-sys" => true,
        // uses the newest windows api, still deprecated :)
        "winrt" => true,
        // renamed
        "hdrsample" => true,
        // in stdlib
        "insideout" | "file" | "ref_slice" => true,
        "serde_derive_internals" | "serde_codegen_internals" | "serde_macros" | "serde_codegen" => true,
        "twoway" => true,
        // says so in the readme
        "jujutsu-lib" | "jujutsu" | "public_items" => true,
        _ => false,
    }
}

#[must_use] pub fn is_autopublished(k: &RichCrateVersion) -> bool {
    k.description().map_or(false, |d| d.starts_with("Automatically published ")) || k.short_name().starts_with("rustc-ap-")
}

#[must_use] pub fn is_squatspam_partial(version: &str, description: Option<&str>) -> Option<&'static str> {
    if version.contains("reserved") || version.contains("placeholder") {
        return Some("version");
    }
    if let Some(desc) = description {
        if is_reserved_boilerplate_text(desc) {
            return Some("reserved-description");
        }
    }
    None
}

/// Returns reason why
#[must_use]
pub fn is_squatspam(k: &RichCrateVersion) -> Option<&'static str> {
    if k.is_spam() {
        return Some("marked as spam");
    }
    if k.authors().len() == 1 && k.authors()[0].name.as_deref() == Some("...") {
        return Some("author"); // spam by mahkoh
    }
    if let Some(reason) = is_squatspam_partial(k.version(), k.description()) {
        return Some(reason);
    }
    if let Some(readme) = k.readme() {
        match &readme.markup {
            Markup::Html(s) | Markup::Markdown(s) | Markup::AsciiDoc(s) | Markup::Rst(s) => if is_reserved_boilerplate_text(s) {
                return Some("readme text");
            }
        }
    }

    if let Some(l) = k.lib_file() {
        let hash = blake3::hash(l.as_bytes());
        if [
            [0x07,0xbc,0x44,0x9b,0xb2,0x62,0x3d,0xd1,0x8f,0x80,0xf4,0x1a,0xb0,0xa1,0xf8,0xd5,0x50,0x87,0x4a,0xe5,0xd0,0x2b,0xa2,0x9d,0x69,0xc2,0x40,0x27,0x11,0x3d,0x1e,0x4b],
            [0x25,0x80,0x23,0x73,0x7f,0xc5,0x1a,0xfb,0x32,0xfa,0x96,0xe7,0x53,0xbc,0x5d,0x7d,0xd6,0xcf,0x53,0x58,0x5a,0xec,0x10,0x0b,0x63,0x16,0xad,0x07,0x7e,0x91,0x30,0x9e],
            [0x5b,0x49,0xda,0x51,0x4c,0x3f,0x8f,0x34,0x1a,0x82,0xbd,0x94,0x45,0xd3,0x3d,0x5b,0x23,0x77,0x54,0x9b,0xb3,0x5f,0x63,0x58,0x77,0x6f,0x94,0x5f,0x3a,0xa0,0x7f,0xfc],
            [0x94,0x0a,0x22,0x83,0x8e,0x3b,0x0a,0xb0,0x5a,0xdd,0xf1,0xa6,0x3e,0xc6,0x24,0xfe,0x52,0x5e,0x25,0xf9,0xa7,0x74,0xc0,0x78,0x0e,0xd2,0x57,0x26,0x7c,0x1b,0x94,0x4d],
            [0xad,0x7d,0x56,0x3f,0x13,0x8f,0x96,0x04,0xbe,0xce,0x74,0xd7,0xf3,0x6d,0xc0,0x9a,0x03,0x2d,0xed,0x6d,0x31,0x96,0xbf,0xb1,0xfa,0x3f,0x29,0x37,0x2b,0x0a,0xcf,0x4b],
            [0xb8,0x68,0xa0,0x68,0x43,0xcd,0x43,0x7a,0x58,0xe0,0xf6,0x6f,0x75,0x1b,0xcb,0x3b,0xc9,0x34,0x36,0xc1,0xb4,0xc2,0xe7,0x76,0x2d,0x56,0x26,0xfc,0xe3,0x73,0x42,0x90],
            [0xbe,0xc2,0xe5,0xb4,0x19,0xef,0x46,0x60,0x6c,0xea,0x0a,0x74,0x57,0x7f,0x36,0x0c,0xaa,0xb5,0xb9,0x2d,0x23,0x59,0x71,0x60,0x41,0xe2,0x96,0x85,0xc4,0x92,0x20,0x23],
            [0xd5,0x94,0xd3,0x46,0x5d,0x20,0x3f,0x1d,0xc6,0x96,0x03,0x22,0xc9,0xf2,0xe9,0xa3,0xcc,0xdc,0x91,0xdf,0xc7,0x58,0xb2,0xc7,0x7d,0xa8,0xb7,0xa9,0xd6,0x16,0xb4,0xab],
        ].iter().any(move |h| hash.as_bytes() == h) {
            return Some("lib.rs junk");
        }
    }

    if let Some(l) = k.bin_file() {
        let hash = blake3::hash(l.as_bytes());
        if hash == [0xfb,0x5c,0xe9,0xdc,0xd7,0x94,0x90,0xba,0x25,0xa7,0x00,0x04,0x07,0x0b,0x11,0xe6,0x2e,0x7f,0xca,0x85,0xc1,0xf6,0x50,0xcb,0x44,0x3e,0x4d,0xd0,0x45,0xbc,0x9f,0xd9] {
            return Some("main.rs junk");
        }
    }

    None
}

fn is_reserved_boilerplate_text(desc: &str) -> bool {
    let desc = desc.trim_matches(|c: char| !c.is_ascii_alphabetic()).as_ascii_lowercase();
    let desc = &*desc;
    let desc2 = desc.trim_start_matches("this crate ")
        .trim_start_matches("is being ")
        .trim_start_matches("is ")
        .trim_start_matches("has been ")
        .trim_start_matches("has ")
        .trim_start_matches("a ").trim_start();
    desc.contains("this crate is a placeholder") ||
        desc.contains("reserving this crate") ||
        desc.contains("reserving this crate") ||
        desc.contains("crate is a placeholder for the future") ||
        desc.contains("only to reserve the name") ||
        desc.contains("this crate has been retired") ||
        desc.contains(" if you want this crate name") ||
        desc.contains("want to use this name") ||
        desc.contains("this is a dummy package") ||
        desc.contains("if you would like to use this crate name, please contact") ||
        desc.starts_with("reserving this crate name for") ||
        desc.starts_with("contact me if you want this name") ||
        desc.starts_with("unused crate name") ||
        desc.starts_with("unused. contact me") ||
        desc.starts_with("crate name not in use") ||
        desc.starts_with("if you want to use this crate name, please contact ") ||
        desc.contains("if you want this name, please contact me") ||
        desc2.starts_with("reserved crate ") ||
        desc.contains("this crate is reserved ") ||
        desc2 == "reserved" ||
        desc2 == "crate name reserved" ||
        desc2.starts_with("reserved for future use") ||
        desc2.starts_with("placeholder") ||
        desc.ends_with(" placeholder") ||
        desc.ends_with(" reserved for use") ||
        desc2.starts_with("dummy crate") ||
        desc2.starts_with("available for ownership transfer") ||
        desc2.starts_with("reserved, for") ||
        desc2.starts_with("crate name reserved for") ||
        desc2.starts_with("wip: reserved") ||
        desc2.starts_with("placeholder") ||
        desc2.starts_with("reserved coming") ||
        desc2.starts_with("empty crate") ||
        desc2.starts_with("an empty crate") ||
        desc2.starts_with("reserved for ") ||
        desc2.starts_with("stub to squat") ||
        desc2.starts_with("claiming it before someone") ||
        desc2.starts_with("reserved name") ||
        desc2.starts_with("reserved package") ||
        desc2.starts_with("reserve the name")
}


#[derive(serde::Deserialize)]
struct CompletionResponseChoice {
    text: String,
}

#[derive(serde::Deserialize)]
struct CompletionResponse {
    choices: Vec<CompletionResponseChoice>,
}

pub fn additional_keywords_for_crate(crate_name: &str, data_dir: &Path) -> io::Result<String> {
    static FILES: OnceCell<HashMap<String, Vec<PathBuf>>> = OnceCell::new();

    let files = FILES.get_or_try_init(|| {
        let dir = data_dir.join("hallucinations");
        let mut out = HashMap::default();
        for e in std::fs::read_dir(dir)? {
            let e = e?;
            let path = e.path();
            let Some(name) = path.file_name().and_then(|s| s.to_str()).filter(|n| n.ends_with(".res.txt")) else { continue };
            let Some((crate_name, _)) = name.split_once('+') else { continue };
            out.entry(crate_name.to_string()).or_insert_with(Vec::new).push(path);
        }
        io::Result::Ok(out)
    })?;

    let Some(paths) = files.get(crate_name) else { return Ok(String::new()) };

    let mut out = Vec::new();
    for p in paths {
        let data: CompletionResponse = serde_json::from_slice(&std::fs::read(p)?)?;
        for c in data.choices {
            out.extend(c.text.lines().filter_map(|line| {
                if let Some((word, rest)) = line.split_once(' ') {
                    if word == "keywords" || word == "categories" || word == "alternative-keywords" {
                        return Some(rest.split(|c:char| c == ',' || c == ';' || c == '/' || c == '\\').map(|w| categories::normalize_keyword(w.trim().trim_end_matches("-rs"))).join("\n"));
                    }
                }
                // trim kv headers or http-like headers
                let line = line.split_once('=').or_else(|| line.split_once(':')).map_or(line, |(_, text)| text)
                    .trim_start()
                    .trim_matches('"');

                let line_lower = line.as_ascii_lowercase();
                let line_lower = &*line_lower;
                let first_ok_word = line_lower.split(|c:char| !c.is_ascii_alphanumeric()).find(|word| {
                    if STOPWORDS.contains(word) { return false; }
                    // guessed categories are too generic
                    !matches!(word.trim_end_matches('s'), "" | "make" | "let" | "robust" | "better" | "easier" | "help" | "package" | "super" | "development" |
                        "tool" | "library" | "librarie" | "rust" | "software" | "going" | "functionality" | "take" | "powerful" |
                        "programming" | "utilitie" | "utility" | "tbd" | "language" | "system" | "misc" | "application" | "lib" | "provide")
                });
                if let Some(first_ok_word) = first_ok_word {
                    let skip_len = (first_ok_word.as_ptr() as usize) - (line_lower.as_ptr() as usize);
                    Some(line.get(skip_len..).unwrap_or(line).replace("\\n", "\n"))
                } else {
                    None
                }
            }))
        }
    }
    Ok(out.join("\n"))
}
