pub use git2::Oid;
pub use git2::Repository;
use cargo_toml::{Manifest, Package};
use crate::iter::HistoryIter;
use git2::build::RepoBuilder;
use git2::Commit;
use git2::{Blob, ObjectType, Reference, Tree};
use lazy_static::lazy_static;
use log::{debug, warn, info, error};
use render_readme::Markup;
use repo_url::Repo;
use std::collections::BTreeMap;
use std::collections::hash_map::Entry::Vacant;
use std::collections::{HashMap, HashSet};
use std::fs;
use std::io;
use std::path::Path;
use std::path::PathBuf;
use std::process::Command;
use std::sync::Arc;
use std::sync::atomic::AtomicU32;
use std::sync::Mutex;
use util::PushString;

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("Git")]
    Git(#[from] #[source] git2::Error),
    #[error("Cargo.toml")]
    Toml(#[from] #[source] cargo_toml::Error),
    #[error("Aborted")]
    Aborted,
    #[error("No disk space")]
    NoDiskSpace,
}

const HECK_NO: &[&str] = &[
    "https://github.com/facebook/hhvm.git",
    "https://github.com/github.com/clbexchange/certified-token-list.git",
    "https://github.com/github.com/supercolony-net/openbrush-contracts.git",
    "https://github.com/paomianzuis/rust-numerical-library.git",
];
const ALWAYS_SHALLOW: &[&str] = &[
    "https://github.com/rust-lang/rust.git",
    "https://github.com/atsam-rs/atsam-pac.git",
    "https://github.com/singularity-data/risingwave.git",
    "https://github.com/azyobuzin/rust-oauthcli.git",
    "https://github.com/andrewdavidmackenzie/flow.git",
    "https://github.com/bytecodealliance/wasmtime.git",
    "https://github.com/cloudpeers/xbuild.git",
    "https://github.com/embali/enimda-rs.git",
    "https://github.com/gkkachi/firestore-grpc.git",
    "https://github.com/hkalbasi/rust.git",
    "https://github.com/kanidm/webauthn-rs.git",
    "https://github.com/khonsulabs/bonsaidb.git",
    "https://github.com/lumen/lumen.git",
    "https://github.com/facebook/hhvm.git",
    "https://github.com/materializeinc/materialize.git",
    "https://github.com/narigo/keepass-diff.git",
    "https://github.com/nathan7/libfringe.git",
    "https://github.com/open-flash/swf-tree.git",
    "https://github.com/nokia/not-perf.git",
    "https://github.com/paomianzuis/rust-numerical-library.git",
    "https://github.com/pylon-protocol/pylon-core-contracts.git",
    "https://github.com/kuleuven-cosic/scale-mamba.git",
    "https://github.com/ruffle-rs/ruffle.git",
    "https://github.com/ramirezmike/not_snake_game.git",
    "https://github.com/rusoto/rusoto.git",
    "https://github.com/changecaps/ike.git",
    "https://github.com/sebcrozet/nphysics.git",
    "https://github.com/servo/servo.git",
    "https://github.com/singularity-data/risingwave.git",
    "https://github.com/swc-project/swc.git",
    "https://github.com/tailhook/vagga.git",
    "https://github.com/tock/tock.git",
];

mod iter;

lazy_static! {
    static ref GLOBAL_LOCK: Mutex<HashMap<String, Arc<Mutex<()>>>> = Mutex::new(HashMap::new());
}

#[derive(Debug, Clone)]
pub struct ParseError(pub String);

fn commit_history_iter<'a>(repo: &Repository, commit: &Reference<'a>) -> Result<HistoryIter<'a>, git2::Error> {
    if repo.is_shallow() {
        let mut origin = repo.find_remote("origin")?;
        if !ALWAYS_SHALLOW.contains(&origin.url().unwrap_or_default()) {
            origin.fetch(&["master"], None, None)?;
        }
    }
    Ok(HistoryIter::new(commit.peel_to_commit()?))
}

pub fn checkout(repo: &Repo, base_path: &Path, shallow: bool) -> Result<Repository, Error> {
    if let Ok(size) = fs4::available_space(base_path) {
        let free_mib = size / (1024 * 1024);
        if free_mib < 3000 {
            error!("Refusing to git clone due to lack of free disk space {free_mib}MB on {}", base_path.display());
            return Err(Error::NoDiskSpace)
        }
    }
    let repo = get_repo(repo, base_path, shallow)?;
    Ok(repo)
}

/// at: tree, commit
#[inline]
pub fn iter_blobs<E, F>(repo: &Repository, at: Option<Oid>, cb: F) -> Result<(), E>
    where F: FnMut(&str, &Tree<'_>, &str, Blob<'_>) -> Result<(), E>, E: From<Error>
{
    let tree = if let Some(oid) = at {
        repo.find_tree(oid).map_err(Error::from)?
    } else {
        let head = repo.head().map_err(Error::from)?;
        head.peel_to_tree().map_err(Error::from)?
    };
    iter_blobs_in_tree(repo, &tree, cb)
}

#[inline]
fn iter_blobs_in_tree<E, F>(repo: &Repository, tree: &Tree<'_>, mut cb: F) -> Result<(), E>
    where F: FnMut(&str, &Tree<'_>, &str, Blob<'_>) -> Result<(), E>, E: From<Error>
{
    iter_blobs_recurse(repo, tree, &mut String::with_capacity(500), &mut cb)?;
    Ok(())
}

fn iter_blobs_recurse<E, F>(repo: &Repository, tree: &Tree<'_>, path: &mut String, cb: &mut F) -> Result<(), E>
    where F: FnMut(&str, &Tree<'_>, &str, Blob<'_>) -> Result<(), E>, E: From<Error>
{
    for i in tree {
        let Some(name) = i.name() else { continue };
        match i.kind() {
            Some(ObjectType::Tree) => {
                let sub = repo.find_tree(i.id()).map_err(Error::from)?;
                let pre_len = path.len();
                path.reserve(name.len()+1);
                if !path.is_empty() {
                    path.push_ascii_in_cap(b'/');
                }
                path.push_str_in_cap(name);
                iter_blobs_recurse(repo, &sub, path, cb)?;
                path.truncate(pre_len);
            },
            Some(ObjectType::Blob) => {
                // symlink?
                if (i.filemode() & 0o0120000) == 0o0120000 {
                    continue;
                }
                cb(path, tree, name, repo.find_blob(i.id()).map_err(Error::from)?)?;
            },
            _ => {},
        }
    }
    Ok(())
}

fn get_repo(repo: &Repo, base_path: &Path, mut shallow: bool) -> Result<Repository, git2::Error> {
    // ensure one clone per dir at a time
    let url = repo.canonical_git_url();
    let lock = GLOBAL_LOCK.lock().unwrap().entry(url.clone()).or_insert_with(|| Arc::new(Mutex::new(()))).clone();
    let _lock = match lock.try_lock() {
        Ok(l) => l,
        Err(_) => {
            info!("Alredy busy with checkout of {}", url);
            lock.lock().unwrap()
        },
    };

    let url = url.as_str();
    if HECK_NO.contains(&url) {
        return Err(git2::Error::new(git2::ErrorCode::GenericError, git2::ErrorClass::Invalid, "Repository is too bloated to be cloned (you need git BFG)"));
    }
    if ALWAYS_SHALLOW.contains(&url) {
        shallow = true;
    }

    let repo_path = base_path.join(&*urlencoding::encode(url));

    match Repository::open(&repo_path) {
        Ok(repo) => Ok(repo),
        Err(err) => {
            if !url.starts_with("http://") && !url.starts_with("https://") && !url.starts_with("git@github.com:") {
                warn!("Rejecting non-HTTP git URL: {url}");
                return Err(err);
            }
            if err.code() == git2::ErrorCode::Exists {
                if let Ok(repo) = Repository::open(&repo_path) {
                    return Ok(repo);
                }
                let _ = fs::remove_dir_all(&repo_path);
            }
            if shallow {
                let ok = Command::new("git")
                    .arg("clone")
                    .arg("--depth=64")
                    .arg("--bare")
                    .arg("--config").arg("core.askPass=true")
                    .arg("--")
                    .arg(url)
                    .arg(&repo_path)
                    .output()
                    .map(|output| output.status.success())
                    .unwrap_or(false);
                if ok {
                    return Repository::open(repo_path);
                }
            }
            let mut ch = RepoBuilder::new();
            ch.bare(true);
            // no support for depth=1!
            ch.clone(url, &repo_path)
        },
    }
}

/// Returns (path, Tree Oid, Cargo.toml)
pub fn find_manifests(repo: &Repository, stop: &AtomicU32) -> Result<(Vec<FoundManifest>, Vec<ParseError>), Error> {
    let head = repo.head()?;
    let tree = head.peel_to_tree()?;
    find_manifests_in_tree(repo, &tree, head.peel_to_commit()?.id(), stop)
}

struct GitFS<'a, 'repo> {
    current_dir: &'a str,
    repo: &'a Repository,
    tree: &'a Tree<'repo>,
    workspaces: &'a BTreeMap<PathBuf, Manifest<cargo_toml::Value>>,
}

impl cargo_toml::AbstractFilesystem for GitFS<'_, '_> {
    fn file_names_in(&self, dir_path: &str) -> Result<HashSet<Box<str>>, io::Error> {
        self.file_names_in_tree(self.tree, Some(dir_path))
    }

    fn parse_root_workspace(&self, rel_path_hint: Option<&str>) -> Result<(Manifest<cargo_toml::Value>, PathBuf), cargo_toml::Error> {
        let path = if let Some(rel) = rel_path_hint {
            let wanted_path = Path::new(self.current_dir).join(rel);
            if let Some((w_dir, w_manifest)) = self.workspaces.get_key_value(&wanted_path) {
                return Ok((w_manifest.clone(), w_dir.into()));
            }
            debug!("Needed {}/{rel} for workspace, not found in {} workspaces", self.current_dir, self.workspaces.len());
            wanted_path
        } else {
            self.current_dir.into()
        };
        // btreemap helps iterate over parent dirs, rev() from longest
        if let Some((w_dir, w_manifest)) = self.workspaces.range::<PathBuf, _>(..=&path).rev().find(|(w_dir, _)| path.starts_with(w_dir)) {
            Ok((w_manifest.clone(), w_dir.into()))
        } else {
            Err(cargo_toml::Error::Io(io::ErrorKind::NotFound.into()))
        }
    }
}

impl GitFS<'_, '_> {
    fn file_names_in_tree(&self, curr_dir: &Tree<'_>, dir_path: Option<&str>) -> Result<HashSet<Box<str>>, io::Error> {
        if let Some(dir_path) = dir_path {
            let mut parts = dir_path.splitn(2, '/');
            let subdir_name = parts.next().unwrap();
            let rest = parts.next();
            for item in curr_dir {
                if item.name() == Some(subdir_name) {
                    if let Ok(tree) = self.repo.find_tree(item.id()) {
                        return self.file_names_in_tree(&tree, rest);
                    }
                }
            }
            Ok(HashSet::new()) // dir not found
        } else {
            let mut res = HashSet::new();
            for item in curr_dir {
                if let Some(n) = item.name() {
                    res.insert(n.into());
                }
            }
            Ok(res)
        }
    }
}

pub struct FoundManifest {
    pub inner_path: String,
    pub tree: Oid,
    pub commit: Oid,
    pub manifest: Manifest,
}

/// Path, tree Oid, parsed TOML
fn find_manifests_in_tree(repo: &Repository, start_tree: &Tree<'_>, commit_id: Oid, stop: &AtomicU32) -> Result<(Vec<FoundManifest>, Vec<ParseError>), Error> {
    let mut tomls = Vec::with_capacity(8);
    let mut workspaces = BTreeMap::new();
    let mut warnings = Vec::new();
    iter_blobs_in_tree::<Error, _>(repo, start_tree, |inner_dir_path, inner_tree, name, blob| {
        if stop.load(std::sync::atomic::Ordering::Relaxed) > 0 {
            return Err(Error::Aborted);
        }

        if name == "Cargo.toml" {
            let blob_content = blob.content();
            match Manifest::from_slice(blob_content) {
                Ok(mut toml) => {
                    // A package may inherit from itself!
                    if toml.workspace.is_some() {
                        workspaces.insert(inner_dir_path.into(), toml.clone());
                    }
                    toml.complete_from_abstract_filesystem::<(), _>(GitFS { current_dir: inner_dir_path, repo, tree: inner_tree, workspaces: &workspaces }, None)?;
                    if toml.package.is_some() {
                        tomls.push(FoundManifest {
                            inner_path: inner_dir_path.to_owned(),
                            tree: inner_tree.id(),
                            commit: commit_id,
                            manifest: toml
                        });
                    }
                },
                Err(err) => {
                    warnings.push(ParseError(format!("Can't parse {}/{inner_dir_path}/{name}: {err}", repo.path().display())));
                },
            }
        }
        Ok(())
    })?;
    Ok((tomls, warnings))
}

pub fn path_in_repo(repo: &Repository, crate_name: &str, stop: &AtomicU32) -> Result<Option<FoundManifest>, Error> {
    let head = repo.head()?;
    let tree = head.peel_to_tree()?;
    path_in_repo_in_tree(repo, &tree, head.peel_to_commit()?.id(), crate_name, stop)
}

fn path_in_repo_in_tree(repo: &Repository, tree: &Tree<'_>, commit_id: Oid, crate_name: &str, stop: &AtomicU32) -> Result<Option<FoundManifest>, Error> {
    Ok(find_manifests_in_tree(repo, tree, commit_id, stop)?.0
        .into_iter()
        .find(|f| f.manifest.package.as_ref().map_or(false, |p| p.name == crate_name)))
}

#[derive(Debug, Copy, Clone, Default)]
struct State {
    since: Option<usize>,
}

pub type PackageVersionTimestamps = HashMap<String, HashMap<String, i64>>;

pub fn find_versions(repo: &Repository, stop: &AtomicU32) -> Result<PackageVersionTimestamps, Error> {
    let mut package_versions: PackageVersionTimestamps = HashMap::with_capacity(4);
    for commit in repo.tag_names(None)?.iter()
        .flatten()
        .filter_map(|tag| repo.find_reference(&format!("refs/tags/{tag}")).map_err(|e| error!("bad tag {tag}: {e}")).ok())
        .filter_map(|r| r.peel_to_commit().map_err(|e| error!("bad ref/tag: {e}")).ok())
    {
        for f in find_manifests_in_tree(repo, &commit.tree()?, commit.id(), stop)?.0 {
            if let Some(pkg) = f.manifest.package {
                add_package(&mut package_versions, pkg, &commit);
            }
        }
    }

    info!("no tags, falling back to slow versions");
    if package_versions.is_empty() {
        return find_dependency_changes(repo, |_, _, _| {}, stop);
    }

    Ok(package_versions)
}

fn is_alnum(q: &str) -> bool {
    q.as_bytes().iter().copied().all(|c| c.is_ascii_alphanumeric() || c == b'_' || c == b'-')
}

fn add_package(package_versions: &mut PackageVersionTimestamps, pkg: Package, commit: &Commit) {
    if pkg.name.is_empty() || !is_alnum(&pkg.name) {
        info!("bad crate name {}", pkg.name);
        return;
    }

    // Find oldest occurence of each version, assuming it's a release date
    let time_epoch = commit.time().seconds();
    #[allow(deprecated)]
    let ver_time = package_versions.entry(pkg.name).or_default()
        .entry(pkg.version.unwrap()).or_insert(time_epoch);
    *ver_time = (*ver_time).min(time_epoch);
}

/// Callback gets added, removed, number of commits ago.
pub fn find_dependency_changes(repo: &Repository, mut cb: impl FnMut(HashSet<String>, HashSet<String>, usize), stop: &AtomicU32) -> Result<PackageVersionTimestamps, Error> {
    let head = repo.head()?;

    let mut newer_deps: HashMap<String, State> = HashMap::with_capacity(100);
    let mut package_versions: PackageVersionTimestamps = HashMap::with_capacity(4);

    // iterates from the latest!
    // The generation number here is not quite accurate (due to diamond-shaped histories),
    // but I need the fiction of it being linerar for this implementation.
    // A recursive implementation could do it better, maybe.
    let commits = commit_history_iter(repo, &head)?.filter(|c| !c.is_merge).map(|c| c.commit);
    for (age, commit) in commits.enumerate().take(1000) {
        // All deps in a repo, because we traverse history once per repo, not once per crate,
        // and because moving of deps between internal crates doesn't count.
        let mut older_deps = HashSet::with_capacity(100);
        for f in find_manifests_in_tree(repo, &commit.tree()?, commit.id(), stop)?.0 {
            // Find oldest occurence of each version, assuming it's a release date
            if let Some(pkg) = f.manifest.package {
                add_package(&mut package_versions, pkg, &commit);
            }

            older_deps.extend(f.manifest.dependencies.into_keys());
            older_deps.extend(f.manifest.dev_dependencies.into_keys());
            older_deps.extend(f.manifest.build_dependencies.into_keys());
        }

        let mut added = HashSet::with_capacity(10);
        let mut removed = HashSet::with_capacity(10);

        for (dep, state) in &mut newer_deps {
            // if it's Some(), it's going to be added in the future! so it's not there now
            // (as a side effect if dependency is added, removed, then re-added, it tracks only the most recent add/remove)
            if state.since.is_none() && !older_deps.contains(dep) {
                added.insert(dep.clone());
                state.since = Some(age);
            }
        }

        for dep in older_deps {
            if let Vacant(e) = newer_deps.entry(dep) {
                if age > 0 {
                    removed.insert(e.key().clone());
                    e.insert(State { since: None }); // until: Some(age)
                } else {
                    e.insert(State::default());
                }
            }
        }

        cb(added, removed, age);
    }
    Ok(package_versions)
}

// FIXME: buggy, barely works
pub fn find_readme(repo: &Repository, package: &Package, stop: &AtomicU32) -> Result<Option<(PathBuf, Markup, bool, Option<[u8; 20]>)>, Error> {
    let head = repo.head()?;
    let commit_id = head.peel_to_commit()?.id();
    let tree = head.peel_to_tree()?;
    let mut readme = None;
    let mut found_best = false; // it'll find many readmes, including fallbacks

    let f = path_in_repo_in_tree(repo, &tree, commit_id, &package.name, stop)?;
    let prefix = Path::new(f.as_ref().map_or("", |f| f.inner_path.as_str()));
    debug!("repo prefix of {} is {} [{commit_id}]", package.name, prefix.display());

    iter_blobs_in_tree::<Error, _>(repo, &tree, |base, _inner_tree, name, blob| {
        if stop.load(std::sync::atomic::Ordering::Relaxed) > 0 {
            return Err(Error::Aborted);
        }
        if found_best {
            return Ok(()); // done
        }

        let base = Path::new(base);
        let stripped = base.strip_prefix(prefix).ok();
        let is_correct_dir = stripped.is_some();
        let rel_path = if let Some(stripped) = stripped {
            stripped
        } else if readme.is_none() {
            base
        } else {
            return Ok(()); // don't search bad dirs if there's some readme already
        };
        let rel_path_name = rel_path.join(name);
        if is_readme_filename(&rel_path_name, Some(package), is_correct_dir) {
            let text = String::from_utf8_lossy(blob.content()).into_owned();
            if text.trim_start().is_empty() {
                debug!("found readme, but it's an empty file? {}", rel_path_name.display());
                return Ok(()); // skip
            }
            let ext = rel_path_name.extension().unwrap_or("".as_ref());
            let markup = if ext == "rst" {
                Markup::Rst(text)
            } else if ext == "adoc" || ext == "asciidoc" {
                Markup::AsciiDoc(text)
            } else {
                Markup::Markdown(text)
            };
            debug!("found a readme in repo at {} (best={is_correct_dir})", rel_path_name.display());
            readme = Some((base.to_owned(), markup, is_correct_dir, commit_id.as_bytes().try_into().ok()));
            found_best = is_correct_dir;
        }
        Ok(())
    })?;
    Ok(readme)
}

/// Check if given filename is a README. If `package` is missing, guess.
fn is_readme_filename(path: &Path, package: Option<&Package>, is_correct_dir: bool) -> bool {
    let Some(s) = path.to_str() else { return false };
    let package_specified_name = package.map(|p| p.readme()).and_then(|p| p.as_path()).and_then(|p| p.to_str());

    if let Some(r) = package_specified_name {
        let r = r.trim_start_matches('.').trim_start_matches('/'); // hacky hack for ../readme
        if r.eq_ignore_ascii_case(s) { // crates published on Mac have this
            return true;
        }
        // if the package specified a file name, and it isn't there, then don't fall back to something else
        if is_correct_dir {
            return false;
        }
    }
    render_readme::is_readme_filename(path)
}

#[test]
fn git_fs() {
    let _ = env_logger::try_init();

    let repo = Repository::open("../.git").expect("own git repo");
    let (m, w) = find_manifests(&repo, &AtomicU32::new(0)).expect("has manifests");
    assert_eq!(0, w.len());
    assert_eq!(32, m.len());
}
