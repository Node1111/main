use ahash::HashMap;
use ahash::HashMapExt;
use ahash::HashSet;
use ahash::HashSetExt;
use categories::Synonyms;
use itertools::Itertools;
use levenshtein_automata::Distance;
use levenshtein_automata::LevenshteinAutomatonBuilder;
use rich_crate::Origin;
use util::pick_top_n_unstable_by;
use util::smol_fmt;
use std::{fs, path::Path};
use tantivy::collector::TopDocs;
use tantivy::query::FuzzyTermQuery;
use tantivy::query::MoreLikeThisQuery;
use tantivy::query::Query;
use tantivy::query::QueryParser;
use tantivy::query::QueryParserError;
use tantivy::query::TermQuery;
use tantivy::schema::*;
use tantivy::SegmentReader;
use tantivy::TantivyError;
use tantivy::{Index, IndexWriter};
use util::crate_name_fuzzy_eq;
use util::CowAscii;
use util::SmolStr;
use log::{warn, error};

#[derive(Debug, Clone, Default)]
pub struct SearchResults {
    pub crates: Vec<CrateFound>,
    /// Did you mean? queries.
    pub dym: Vec<SmolStr>,
    pub nothing_found_without_dym: bool,
    pub keywords: Vec<SmolStr>,
}

pub struct CrateSearchIndex {
    /// Origin.to_str
    origin_pkey: Field,
    /// as-is
    crate_name_field: Field,
    /// space-separated
    keywords_field: Field,
    description_field: Field,
    readme_field: Field,
    /// invisible keywords
    extra_field: Field,
    /// raw number
    monthly_downloads: Field,
    /// semver string
    crate_version: Field,
    /// number in range 0..=SCORE_MAX denoting desirability of the crate
    crate_score: Field,

    tantivy_index: Index,
    synonyms: Synonyms,
}

#[derive(Debug, Clone)]
pub struct CrateFound {
    pub origin: Origin,
    pub crate_name: SmolStr,
    pub description: String,
    /// space-separated
    pub keywords_s: String,
    /// space-separated
    pub keywords_normalized_s: String,
    pub score: f32,
    pub relevance_score: f32,
    pub crate_base_score: f32,
    pub version: SmolStr,
    pub monthly_downloads: u64,
}

impl CrateFound {
    pub fn keywords_normalized(&self) -> impl Iterator<Item=&str> {
        self.keywords_normalized_s.split(' ')
    }

    pub fn keywords(&self) -> impl Iterator<Item=&str> {
        self.keywords_s.split(' ')
    }
}

pub struct Indexer {
    index: CrateSearchIndex,
    writer: IndexWriter,
}

impl CrateSearchIndex {
    pub fn new(data_dir: impl AsRef<Path>) -> tantivy::Result<Self> {
        let data_dir = data_dir.as_ref();
        let synonyms = Synonyms::new(data_dir)?;

        let index_dir = data_dir.join("tantivy21");
        if !index_dir.exists() {
            return Self::reset_db_to_empty(&index_dir, synonyms);
        }
        let tantivy_index = Index::open_in_dir(index_dir)?;
        let schema = tantivy_index.schema();


        Ok(Self {
            tantivy_index,
            origin_pkey: schema.get_field("origin").expect("schema"),
            crate_name_field: schema.get_field("crate_name").expect("schema"),
            keywords_field: schema.get_field("keywords").expect("schema"),
            description_field: schema.get_field("description").expect("schema"),
            readme_field: schema.get_field("readme").expect("schema"),
            extra_field: schema.get_field("extra").expect("schema"),
            monthly_downloads: schema.get_field("monthly_downloads").expect("schema"),
            crate_version: schema.get_field("crate_version").expect("schema"),
            crate_score: schema.get_field("crate_score").expect("schema"),
            synonyms,
        })
    }

    fn reset_db_to_empty(index_dir: &Path, synonyms: Synonyms) -> tantivy::Result<Self> {
        let _ = fs::create_dir_all(index_dir);

        let mut schema_builder = SchemaBuilder::default();

        let origin_pkey = schema_builder.add_text_field("origin", STRING | STORED);
        let crate_name_field = schema_builder.add_text_field("crate_name", STRING | STORED);
        let keywords_field = schema_builder.add_text_field("keywords", TEXT | STORED);
        let description_field = schema_builder.add_text_field("description", TEXT | STORED);
        let text_field_indexing = TextFieldIndexing::default().set_tokenizer("en_stem").set_index_option(IndexRecordOption::WithFreqs);
        let text_options = TextOptions::default().set_indexing_options(text_field_indexing);
        let readme_field = schema_builder.add_text_field("readme", text_options.clone());
        let extra_field = schema_builder.add_text_field("extra", TEXT);
        let crate_version = schema_builder.add_text_field("crate_version", STORED);
        let monthly_downloads = schema_builder.add_u64_field("monthly_downloads", STORED);
        let crate_score = schema_builder.add_f64_field("crate_score", STORED | FAST);

        let schema = schema_builder.build();
        let tantivy_index = Index::create_in_dir(index_dir, schema)?;

        Ok(Self {
            origin_pkey, crate_name_field, keywords_field, description_field,
            readme_field,
            extra_field,
            monthly_downloads, crate_version, crate_score, tantivy_index, synonyms,
        })
    }

    #[must_use] pub fn normalize_keyword<'a>(&'a self, kw: &'a str) -> &'a str {
        self.synonyms.normalize(kw)
    }

    fn parse_query(&self, query_text: &str) -> Result<Box<dyn Query>, QueryParserError> {
        let mut query_parser = QueryParser::for_index(&self.tantivy_index, vec![
            self.crate_name_field, self.keywords_field, self.description_field, self.readme_field,
            self.extra_field,
        ]);
        query_parser.set_conjunction_by_default();
        query_parser.set_field_boost(self.crate_name_field, 2.0);
        query_parser.set_field_boost(self.keywords_field, 1.5);
        query_parser.set_field_boost(self.readme_field, 0.5);
        query_parser.set_field_boost(self.extra_field, 0.1);
        query_parser.parse_query(query_text)
            .or_else(|_| {
                let mangled_query: String = query_text.chars().map(|ch| {
                    if ch.is_alphanumeric() {ch.to_ascii_lowercase()} else {' '}
                }).collect();
                query_parser.parse_query(mangled_query.trim())
            })
    }

    fn fetch_docs(&self, searcher: &tantivy::Searcher, query: &dyn Query, limit: usize) -> Result<Vec<CrateFound>, TantivyError> {
        let top_docs = searcher.search(query, &TopDocs::with_limit(limit).tweak_score(move |segment_reader: &SegmentReader| {
            let crate_score_reader = segment_reader.fast_fields().f64("crate_score").unwrap();
                move |doc, score| {
                    let mut crate_score = crate_score_reader.values.get_val(doc);
                    if crate_score > 0.4 { crate_score += 1. }; // if it's decent, relevancy starts to matter more than crate rank
                    (f64::from(score) * crate_score) as f32
                }
        }))?;

        let docs = top_docs.into_iter().map(|(relevance_score, doc_address)| {
            let doc = searcher.doc(doc_address)?;
            let crate_base_score = take_f64(doc.get_first(self.crate_score)) as f32;
            let crate_name = get_as_str(doc.get_first(self.crate_name_field)).into();
            let origin = Origin::from_str(get_as_str(doc.get_first(self.origin_pkey)));

            let keywords_s = get_as_str(doc.get_first(self.keywords_field)).to_owned();

            Ok(CrateFound {
                crate_base_score,
                relevance_score,
                score: 0.,
                crate_name,
                description: get_as_str(doc.get_first(self.description_field)).into(),
                keywords_s,
                keywords_normalized_s: String::new(),
                version: get_as_str(doc.get_first(self.crate_version)).into(),
                monthly_downloads: if origin.is_crates_io() { take_int(doc.get_first(self.monthly_downloads)) } else { 0 },
                origin,
            })
        })
        .collect::<Result<Vec<CrateFound>, TantivyError>>()?;

        Ok(docs)
    }

    pub fn similar_crates(&self, origin: &Origin) -> tantivy::Result<Vec<(Origin, f32)>> {
        let reader = self.tantivy_index.reader()?;
        let searcher = reader.searcher();
        let crate_name = origin.package_name_icase();
        let pkey = Term::from_field_text(self.crate_name_field, crate_name);
        let query = TermQuery::new(
            pkey,
            IndexRecordOption::Basic,
        );
        let mut the_doc = searcher.search(&query, &TopDocs::with_limit(1))?;
        let Some((_, the_doc_addr)) = the_doc.pop() else { error!("not found {crate_name}"); return Ok(vec![]) };
        let the_doc = searcher.doc(the_doc_addr)?;

        let query = MoreLikeThisQuery::builder()
            .with_min_term_frequency(1)
             .with_min_doc_frequency(2)
             .with_max_doc_frequency(5000) // async is important and popular…
             .with_stop_words(["proc-macro", "implementation", "api", "tool", "parser", "utility", "crate", "subcommand", "rust", "for",
                "macro", "client", "test", "wrapper", "bindings", "no-std"].into_iter().map(String::from).collect())
             .with_document_fields(vec![
                (self.keywords_field, the_doc.get_all(self.crate_name_field).chain(the_doc.get_all(self.keywords_field)).cloned().collect()),
                (self.readme_field, the_doc.get_all(self.readme_field).cloned().collect())
            ]);
             // .with_document(*doc_addr);
        let similar_docs = searcher.search(&query, &TopDocs::with_limit(10).tweak_score(move |segment_reader: &SegmentReader| {
            let crate_score_reader = segment_reader.fast_fields().f64("crate_score").unwrap();
                move |doc, match_score| {
                    let mut crate_score = crate_score_reader.values.get_val(doc);
                    if crate_score > 0.5 { crate_score += 2. };
                    (f64::from(match_score) * crate_score) as f32
                }
        }))?;

        similar_docs.into_iter()
            .filter(move |&(_, addr)| addr != the_doc_addr)
            .map(move |(similarity, addr)| {
                let doc = searcher.doc(addr)?;
                let origin = Origin::from_str(get_as_str(doc.get_first(self.origin_pkey)));
                Ok((origin, similarity))
            }).collect()
    }

    /// if `sort_by_query_relevance` is false, sorts by internal crate score (relevance is still used to select the crates)
    /// Second arg is distinctive keywords
    pub fn search(&self, query_text: &str, limit: usize, sort_by_query_relevance: bool) -> tantivy::Result<SearchResults> {
        let query_text = query_text.trim();
        let query = self.parse_query(query_text)?;

        let reader = self.tantivy_index.reader()?;
        let searcher = reader.searcher();
        let expanded_limit = (limit + 50 + limit / 2).max(200); // 200 is a hack for https://github.com/tantivy-search/tantivy/issues/700
        let mut docs = self.fetch_docs(&searcher, &query, expanded_limit)?;

        // the query could be a crate name, and tantivy keeps missing exact matches!
        self.fix_missing_exact_match(&mut docs, query_text, &searcher)?;

        let nothing_found_without_dym = docs.is_empty();
        let mut dym = self.did_you_mean_fuzzy(&mut docs, query_text, &searcher)?;

        assign_doc_score(&mut docs, sort_by_query_relevance, query_text);
        sort_results_and_discard_irrelevant(&mut docs);

        self.normalize_keywords(&mut docs);

        let dividing_keywords = self.get_dividing_keywords(&docs, query_text, limit);
        let mut keywords: Vec<_> = dividing_keywords.dividing.iter().copied().map(SmolStr::from).collect();

        // If the query is dominated by one keyword, it looks spammy and boring. Repeat with diverse results.
        if let Some(too_common) = dividing_keywords.too_common {
            warn!("Mixing query {query_text}, because keyword {too_common} is too common");
            // TODO: use ast
            if let Ok(query2) = self.parse_query(&format!("{query_text} -\"{too_common}\"")) {
                let mut docs2 = self.fetch_docs(&searcher, &query2, limit/2)?;
                assign_doc_score(&mut docs2, sort_by_query_relevance, query_text);

                // -"keyword" makes scoring different? needs rescale
                let old_max_score = docs.iter().map(|d| d.score).max_by(|a, b| a.total_cmp(b)).unwrap_or(1.);
                let new_max_score = docs2.iter().map(|d| d.score).max_by(|a, b| a.total_cmp(b)).unwrap_or(1.);
                for d in &mut docs2 {
                    d.score = d.score / new_max_score * old_max_score;
                }

                let mut unique_keyword_sets = HashSet::new();
                let too_common = &*SmolStr::from(too_common);
                let mut i = 0;
                for d in docs.iter_mut().skip(3) {
                    if d.keywords_normalized().any(|k| k == too_common) {
                        let factor = (0.9 - (i as f32 / 10_f32)).max(0.33);
                        i += 1;
                        d.score *= factor;
                        if !unique_keyword_sets.insert(d.keywords_normalized_s.as_str()) {
                            d.score *= 0.5;
                        }
                    }
                }
                self.normalize_keywords(&mut docs2);
                docs.append(&mut docs2);
                sort_results_and_discard_irrelevant(&mut docs);
                let dividing_keywords = self.get_dividing_keywords(&docs, query_text, limit);
                keywords = dividing_keywords.dividing.iter().copied().map(SmolStr::from).collect();
            }
        }

        // Dedupe
        let mut unique_crates = HashSet::with_capacity(docs.len());
        docs.retain(|d| unique_crates.insert(d.crate_name.clone()));

        // Make sure that there's at least one crate ranked high for each of the interesting keywords
        // This truncates to the limit
        let crates = move_representative_crates_to_top(&keywords, docs, limit);

        let normalized_query = query_text.split(' ').map(|w| self.normalize_keyword(w)).join(" ");
        if normalized_query != query_text && !dym.iter().any(|d| d == &*normalized_query) { dym.push(normalized_query.into()) };

        Ok(SearchResults {
            crates,
            dym,
            keywords,
            nothing_found_without_dym,
        })
    }

    fn did_you_mean_fuzzy(&self, docs: &mut Vec<CrateFound>, query_text: &str, searcher: &tantivy::Searcher) -> Result<Vec<SmolStr>, TantivyError> {
        if query_text == "tokyo" {
            return Ok(vec!["tokio".into()]);
        }

        // docs are still sorted by tantivy here, which is pure textual relevance, without quality ranking
        // so sometimes the first result is a pretty bad crate
        let top_results_score = docs.iter().take(3).fold(0f32, |a, b| a.max(b.crate_base_score));
        if top_results_score > 0.5 || (top_results_score > 0.2 && docs.len() > 20) {
            // results seem good enough
            return Ok(Vec::new());
        }

        let max_dist = if query_text.len() > 8 && query_text.len() < 30 { 2 } else { 1 };
        let crate_like_query = query_text.chars().map(|c| if c.is_alphanumeric() || c == '_' { c } else { '-' }).collect::<SmolStr>();
        let fuzzy_crate_name = FuzzyTermQuery::new(Term::from_field_text(self.crate_name_field, &crate_like_query), max_dist, true);
        let fuzzy_keyword = FuzzyTermQuery::new(Term::from_field_text(self.keywords_field, query_text), max_dist, true);
        let mut added = 0;
        for q in [fuzzy_crate_name, fuzzy_keyword] {
            let mut tmp = self.fetch_docs(searcher, &q, 5)?;
            self.normalize_keywords(&mut tmp);
            // d.score is not set at this point
            tmp.iter_mut().for_each(|d| { d.relevance_score *= 0.75; }); // reduce relevance, since they're fuzzy
            added += Self::add_extra_docs(docs, tmp);
        }

        let mut words = HashMap::new();
        for doc in &docs[docs.len() - added ..] {
            if doc.crate_base_score < 0.1 {
                continue;
            }
            for word in doc.keywords().chain([doc.crate_name.as_str()]) {
                *words.entry(unicase::Ascii::new(word)).or_insert(0.) += doc.relevance_score * doc.crate_base_score;
            }
        }

        fn normalize_char(c: u8) -> u8 {
            if c == b'-' || c == b'_' { b' ' } else { c.to_ascii_lowercase() }
        }

        let mut dym_scores = Vec::new();
        let dfa = LevenshteinAutomatonBuilder::new(max_dist, true).build_dfa(&String::from_utf8(query_text.bytes().map(normalize_char).collect::<Vec<_>>()).unwrap());
        for (&word, &score) in &words {
            let mut state = dfa.initial_state();
            for b in word.bytes() {
                state = dfa.transition(state, normalize_char(b));
            }
            let dist = match dfa.distance(state) {
                Distance::Exact(dist) => dist,
                Distance::AtLeast(_) => continue,
            };

            if dist > 0 && dist < 4 && score > 0.1 {
                dym_scores.push((score / f32::from(dist), word));
            }
        }
        pick_top_n_unstable_by(&mut dym_scores, 3, |a,b| b.0.total_cmp(&a.0));

        Ok(dym_scores.into_iter().map(|(_, w)| w.into_inner().into()).collect())
    }

    /// Pick a few representative keywords that are for different meanings of the query (e.g. oracle db vs padding oracle)
    fn get_dividing_keywords<'a>(&self, docs: &'a [CrateFound], query_text: &str, limit: usize) -> DividingKeywordSet<'a> {
        let query_lower = query_text.as_ascii_lowercase();
        let query_as_keyword = query_lower.replace(|c:char| { c == '_' || c == ' ' }, "-");
        // "foo bar" == "foo-bar"
        let query_rev = query_as_keyword.split('-').rev().join("-");
        let query_keywords: Vec<_> = query_text.split(|c: char| !c.is_alphanumeric()).filter(|k| !k.is_empty())
            .chain([query_text, &query_lower, &query_as_keyword, &query_rev])
            .map(|k| self.normalize_keyword(k))
            .collect();
        
        Self::dividing_keywords(docs, limit/2, &query_keywords, &query_as_keyword, &["blockchain", "solana", "ethereum", "bitcoin", "cryptocurrency"]).unwrap_or_default()
    }

    fn fix_missing_exact_match(&self, docs: &mut Vec<CrateFound>, query_text: &str, searcher: &tantivy::Searcher) -> Result<(), TantivyError> {
        let query_is_crate_name = query_text.bytes().all(|c| c.is_ascii_alphanumeric() || c == b'-' || c == b'_');
        if !query_is_crate_name {
            return Ok(());
        }

        let has_exact_match = docs.iter().any(|doc| crate_name_fuzzy_eq(&doc.crate_name, query_text));
        if has_exact_match {
            return Ok(());
        }

        let exact_crate_query = self.parse_query(&format!("crate_name:\"{query_text}\""))?;
        let mut extra_docs = self.fetch_docs(searcher, &exact_crate_query, 3)?;
        self.normalize_keywords(&mut extra_docs);
        Self::add_extra_docs(docs, extra_docs);
        Ok(())
    }

    fn add_extra_docs(docs: &mut Vec<CrateFound>, mut extra_docs: Vec<CrateFound>) -> usize {
        // it's probably quicker to loop once than make a hashtable
        for doc in &*docs {
            extra_docs.retain(|d| d.crate_name != doc.crate_name);
            if extra_docs.is_empty() {
                return 0;
            }
        }
        let added = extra_docs.len();
        docs.append(&mut extra_docs);
        added
    }

    fn normalize_keywords(&self, docs: &mut [CrateFound]) {
        let mut kw_dedupe = HashSet::new();
        for doc in docs {
            kw_dedupe.clear();
            doc.keywords_normalized_s = doc.keywords_s.split(' ').filter_map(|k| {
                let normalized = self.synonyms.normalize(k);
                kw_dedupe.insert(normalized).then_some(normalized)
            }).join(" ");
        }
    }

    fn dividing_keywords<'a>(results: &'a [CrateFound], limit: usize, query_keywords: &[&str], query_as_keyword: &str, skip_entire_results: &[&str]) -> Option<DividingKeywordSet<'a>> {
        // divide keyword popularity by its global popularity tf-idf, because everything gets api, linux, cargo, parser
        // bonus if keyword pair exists
        let mut dupes = HashSet::with_capacity(results.len());
        let mut keyword_sets = results.iter().enumerate().filter_map(|(i, found)| {
                if !dupes.insert(found.keywords_s.as_str()) { // some crate families have all the same keywords, which is spammy and biases the results
                    return None;
                }
                let mut k_set: HashSet<_> = found.keywords_normalized().collect();
                for &q in query_keywords {
                    k_set.remove(q);
                }

                if skip_entire_results.iter().any(|&dealbreaker| k_set.contains(dealbreaker)) {
                    return None;
                }
                Some((k_set, if i < limit { 2048 } else { 1024 } + (128. * found.score) as i32)) // integer for ease of sorting, unique for sort stability
            }).collect::<Vec<_>>();
        drop(dupes);

        // api/cli are boring and generic
        let d1 = Self::popular_dividing_keyword(&keyword_sets, query_as_keyword, &["api", "cli"]);
        let too_common = d1.too_common;
        let most_common = d1.dividing?;
        // The most common co-occurrence may be a synonym, so skip it for now
        let second_most_common = Self::popular_dividing_keyword(&keyword_sets, query_as_keyword, &[most_common]).dividing?;

        let mut dividing_keywords = Vec::<&'a str>::with_capacity(10);
        let mut next_keyword = second_most_common;
        for _ in 0..10 {
            if dividing_keywords.iter().any(|&k| k == next_keyword) {
                break; // looped, so nothing interesting left?
            }
            dividing_keywords.push(next_keyword);
            for (k_set, w) in &mut keyword_sets {
                if k_set.remove(&next_keyword) && *w > 0 { *w = -*w/2; }
            }
            if keyword_sets.iter().filter(|&(_, w)| *w > 0).count() < 25 {
                break;
            }
            next_keyword = match Self::popular_dividing_keyword(&keyword_sets, query_as_keyword, &["reserved", next_keyword]).dividing {
                None => break,
                Some(another) => another,
            };
        }
        Some(DividingKeywordSet {
            dividing: dividing_keywords,
            too_common,
        })
    }

    /// Find a keyword that splits the set into two distinctive groups
    fn popular_dividing_keyword<'a>(keyword_sets: &[(HashSet<&'a str>, i32)], query_keyword: &str, ignore_keywords: &[&str]) -> DividingKeywords<'a> {
        if keyword_sets.len() < 25 {
            return Default::default(); // too few results will give odd niche keywords
        }
        let prefix = &*smol_fmt!("{query_keyword}-");
        let suffix = &*smol_fmt!("-{query_keyword}");
        let mut counts: HashMap<&str, (u32, i32)> = HashMap::with_capacity(keyword_sets.len());
        for &(ref k_set, w) in keyword_sets {
            for &k in k_set {
                let n = counts.entry(k).or_default();
                n.0 += 1;
                n.1 += w;
                if w > 0 {
                    // for a query like "http" make "http-client" count as "client" too
                    if let Some(rest) = k.strip_prefix(prefix) {
                        if !k_set.contains(rest) {
                            let n = counts.entry(rest).or_default();
                            n.0 += 1;
                            n.1 += w * 2; // having prefix/suffix keyword is a good indicator that it's a meaningful distinction rather than a synonym
                        }
                    } else if let Some(rest) = k.strip_suffix(&suffix) {
                        if !k_set.contains(rest) {
                            let n = counts.entry(rest).or_default();
                            n.0 += 1;
                            n.1 += w * 2;
                        }
                    }
                }
            }
        }
        for stopword in ["api", "cli", "rust"] {
            counts.entry(stopword).and_modify(|e| { e.1 = e.1 * 3/4; });
        }

        let good_pop = (keyword_sets.len() / 3) as u32;
        let too_common_pop = (keyword_sets.len() * 5 / 8) as u32;
        let mut too_common_word = None;
        let mut too_common_word_score = 0;

        let dividing = counts.into_iter()
            .filter(|&(k, (pop, _))| {
                if pop > too_common_pop && pop > too_common_word_score {
                    too_common_word_score = pop;
                    too_common_word = Some(k);
                }
                pop > 2 && pop <= too_common_pop
            })
            .filter(|&(k, _)| !ignore_keywords.iter().any(|&ignore| ignore == k))
            .max_by_key(|&(_, (pop, weight))| if pop >= 10 && pop <= good_pop { weight * 2 } else { weight })
            .map(|(k, _)| k);

        DividingKeywords {
            dividing,
            too_common: too_common_word,
        }
    }
}

#[derive(Default)]
struct DividingKeywords<'a> {
    dividing: Option<&'a str>,
    too_common: Option<&'a str>,
}

#[derive(Default)]
struct DividingKeywordSet<'a> {
    dividing: Vec<&'a str>,
    too_common: Option<&'a str>,
}

fn move_representative_crates_to_top(dividing_keywords: &[SmolStr], mut docs: Vec<CrateFound>, limit: usize) -> Vec<CrateFound> {
    if docs.len() < 10 || dividing_keywords.is_empty() {
        docs.truncate(limit);
        return docs;
    }
    let mut interesting_crate_indices = dividing_keywords.iter().take(6).filter_map(|dk| {
        docs.iter().position(|k| k.keywords_normalized().any(|k| k == dk))
    }).collect::<Vec<_>>();
    // but keep top 3 results as they are
    interesting_crate_indices.extend(0..3);
    interesting_crate_indices.sort_unstable();
    interesting_crate_indices.dedup();
    let mut top_crates = Vec::with_capacity(docs.len());
    for idx in interesting_crate_indices.into_iter().rev() {
        top_crates.push(docs.remove(idx));
    }
    top_crates.sort_unstable_by(|a, b| b.score.total_cmp(&a.score));

    docs.truncate(limit.saturating_sub(top_crates.len()));
    // search picked a few more results to cut out chaff using crate_score
    if let Some(min_score) = docs.first().map(|f| f.score) {
        // keep score monotonic
        top_crates.iter_mut().for_each(|f| if f.score < min_score { f.score = min_score; });
    }
    top_crates.append(&mut docs);
    top_crates
}

fn contains_query(a: &str, b: &str) -> bool {
    let a = a.trim_start_matches("cargo").trim_start_matches("rust").trim_end_matches("rs").trim_matches(|c| c == '-' || c == '_');
    if a.is_empty() {
        return false;
    }
    let (a, b) = if a.len() >= b.len() { (a, b) } else { (b, a) };
    a.contains(b)
}

fn assign_doc_score(docs: &mut [CrateFound], sort_by_query_relevance: bool, query_text: &str) {
    let max_relevance = docs.iter().map(|v| v.relevance_score).max_by(|a,b| a.total_cmp(b)).unwrap_or(0.);
    for doc in docs.iter_mut() {
        doc.score = if sort_by_query_relevance {
            // bonus for exact match
            doc.crate_base_score * if crate_name_fuzzy_eq(&doc.crate_name, query_text) {
                max_relevance *
                    // if the query is specific and contains _-,
                    // it's more likely a crate name search than a keyword search
                    if query_text.contains('_') || query_text.len() > 15 { 2.0 }
                    else if query_text.contains('-') { 1.5 } else { 1.2 }
            } else {
                doc.relevance_score * if contains_query(&doc.crate_name, query_text) { 1.1 } else { 1. }
            }
        } else {
            doc.crate_base_score
        };
    }
}

fn sort_results_and_discard_irrelevant(docs: &mut Vec<CrateFound>) {
    // re-sort using our base score
    docs.sort_unstable_by(|a, b| b.score.total_cmp(&a.score));

    // throw away yanked and spammy crates, unless there nothing else to show
    if docs.len() > 3 {
        let keep_at_least = (docs.len() / 4).max(10);
        let mut n = 0;
        docs.retain(|doc| {
            n += 1;
            n <= keep_at_least || doc.crate_base_score > 0.01
        });
    }
}

#[track_caller]
fn get_as_str(val: Option<&Value>) -> &str {
    match val {
        Some(val) => match val {
            Value::Str(s) => s,
            _ => panic!("invalid value type"),
        },
        _ => "",
    }
}

#[track_caller]
fn take_int(val: Option<&Value>) -> u64 {
    match val {
        Some(val) => match val {
            Value::U64(s) => *s,
            Value::I64(s) => *s as u64,
            _ => panic!("invalid value type"),
        },
        _ => 0,
    }
}

#[track_caller]
fn take_f64(val: Option<&Value>) -> f64 {
    match val {
        Some(val) => match val {
            Value::F64(s) => *s,
            _ => panic!("invalid value type"),
        },
        _ => 0.0,
    }
}

pub struct IndexerData<'a> {
    pub origin: &'a Origin,
    pub crate_name: &'a str,
    pub version: &'a str,
    pub description: &'a str,
    pub keywords: &'a [&'a str],
    pub readme: Option<&'a str>,
    pub monthly_downloads: u64,
    pub extra_keywords: Option<String>,
    pub score: f64,
}

impl Indexer {
    pub fn new(index: CrateSearchIndex) -> tantivy::Result<Self> {
        Ok(Self { writer: index.tantivy_index.writer(250_000_000)?, index })
    }

    /// score is float 0..=1 range
    pub fn add(&mut self, IndexerData {origin, crate_name, version, description, keywords, readme, monthly_downloads, extra_keywords, score}: IndexerData) -> Result<(), TantivyError> {
        let origin = origin.to_str();
        // delete old doc if any
        let pkey = Term::from_field_text(self.index.origin_pkey, &origin);
        self.writer.delete_term(pkey);

        // bug workaround
        let pkey2 = Term::from_field_text(self.index.crate_name_field, crate_name);
        self.writer.delete_term(pkey2);

        if score > 0.001 {
            // index new one
            let mut doc = Document::default();
            doc.add_text(self.index.origin_pkey, &origin);
            doc.add_text(self.index.crate_name_field, crate_name);
            doc.add_text(self.index.keywords_field, &keywords.join(" "));
            doc.add_text(self.index.description_field, description);
            if let Some(readme) = readme {
                doc.add_text(self.index.readme_field, readme);
            }
            if let Some(e) = extra_keywords {
                doc.add_text(self.index.extra_field, e);
            }
            doc.add_text(self.index.crate_version, version);
            doc.add_u64(self.index.monthly_downloads, monthly_downloads);
            doc.add_f64(self.index.crate_score, score);
            self.writer.add_document(doc)?;
        }
        Ok(())
    }

    pub fn commit(&mut self) -> tantivy::Result<()> {
        self.writer.commit()?;
        Ok(())
    }

    pub fn bye(self) -> tantivy::Result<CrateSearchIndex> {
        self.writer.wait_merging_threads()?;
        Ok(self.index)
    }

    pub fn delete(&self, origin: &Origin) {
        let origin = origin.to_str();
        let pkey = Term::from_field_text(self.index.origin_pkey, &origin);
        self.writer.delete_term(pkey);
    }
}
