#![allow(dead_code)]

use ahash::HashMap;
use ahash::HashMapExt;
use ahash::HashSet;
use categories::Synonyms;
use itertools::Itertools;
use kitchen_sink::KitchenSink;
use search_index::CrateSearchIndex;
use std::cmp::Reverse;
use util::SmolStr;

#[tokio::main]
async fn main() -> Result<(), anyhow::Error> {
    let crates = kitchen_sink::KitchenSink::new_default().await?;
    let all_kw = crates.crate_db.all_explicit_keywords().await?;
    let synon = Synonyms::new(&KitchenSink::data_path()?)?;

    if false {
        let index = CrateSearchIndex::new(KitchenSink::data_path()?)?;
        // other(all_kw, &synon, &index);
        for k1 in all_kw.keys() {
            let res = index.search(k1, 100, true)?;
            if res.keywords.is_empty() {
                continue;
            }
            let mut printed = false;
            for k in &res.keywords {
                if all_kw.contains_key(format!("{k1}-{k}").as_str()) {
                    if !printed { print!("{k1}:"); printed = true; }
                    print!(" +{k}");
                }
                if all_kw.contains_key(format!("{k}-{k1}").as_str()) {
                    if !printed { print!("{k1}:"); printed = true; }
                    print!(" {k}+");
                }
            }
            if printed { println!(); }
        }
        return Ok(());
    }

    normalize_dashes(&synon, all_kw.keys().map(|k| k.as_str()));
    normalize_plural(&synon, all_kw);
    Ok(())
}

fn other(all_kw: &HashMap<SmolStr, u32>, synon: &Synonyms, index: &CrateSearchIndex) {
    for k in all_kw.keys() {
        let mut groups = HashSet::default();
        let mut check = |prefixed: String| {
            if all_kw.contains_key(prefixed.as_str()) {
                groups.insert(prefixed);
            } else if let Some((alias, _)) = synon.get(&prefixed) {
                if alias.contains('-') && all_kw.contains_key(alias) {
                   groups.insert(alias.into());
               }
            }
        };

        if let Ok(serp) = index.search(k, 150, false) {
            for other in &serp.keywords {
                check(format!("{other}-{k}"));
                check(format!("{k}-{other}"));
                if let Some((alias, _)) = synon.get(k) {
                    check(format!("{other}-{alias}"));
                    check(format!("{alias}-{other}"));
                }
            }
        }
        if !groups.is_empty() {
            println!("{k} => {}", groups.iter().join(", "));
        }
    }

    let mut rev_match: HashMap<SmolStr, SmolStr> = HashMap::new();
    for k in all_kw.keys() {
        if let Some((other, _)) = top_other_keyword(index, k, &[k]) {
            if let Some(reverse) = rev_match.get(&other) {
                if reverse == k {
                    println!("{other},{k},2");
                }
            } else {
                rev_match.insert(k.clone(), other);
            }
        }
    }
}

fn normalize_dashes<'a>(synon: &Synonyms, all_kw: impl IntoIterator<Item = &'a str> + Clone) {
    let mut most_popular: HashMap<String, &str> = HashMap::new();
    for k in all_kw.clone() {
        let nodashed = k.replace('-', "");
        most_popular.entry(nodashed)
            .or_insert(k); // relies on sort order to pick most common

    }
    for k in all_kw {
        if let Some(undashed) = most_popular.get(k).copied() {
            if k != undashed && synon.get(k).is_none() && synon.get(undashed).is_none() {
                println!("{k},{undashed},4");
            }
        }
    }
}

fn normalize_plural(synon: &Synonyms, all_kw: &HashMap<SmolStr, u32>) {
    let mut all_kw = all_kw.iter()
        .filter(|&(_, v)| *v > 2)
        .map(|(k, v)| (k.as_str(), *v)).collect::<Vec<(&str, u32)>>();
    all_kw.sort_by_key(|(_, v)| Reverse(*v));

    let mut most_popular: HashMap<&str, &str> = HashMap::new();
    for (k, _) in all_kw {
        let singular = k.strip_suffix('s').unwrap_or(k);
        let print = |k, prev, n| {
            let prev = synon.normalize(prev);
            if k != prev && synon.get(k).is_none() { println!("{k},{prev},{n}"); }
        };
        if singular.len() > 2 {
            most_popular.entry(singular)
                .and_modify(|prev| print(k, prev, 5))
                .or_insert_with(|| k); // relies on sort order to pick most common
        }

        if let Some(singular) = k.strip_suffix("es") {
            if singular.len() > 2 {
                most_popular.entry(singular)
                    .and_modify(|prev| print(k, prev, 4))
                    .or_insert_with(|| k);
            }
        }
    }
}

/// Most common + alterantive unrelated to most common
fn top_other_keyword(index: &CrateSearchIndex, query:&str, skip_words: &[&str]) -> Option<(SmolStr, Vec<SmolStr>)> {
    let res = index.search(query, 300, true).ok()?.crates;
    if res.len() < 25 {
        return None; // noisy data?
    }
    let mut keyword_sets = res.iter().map(|k| {
        let mut k: Vec<_> = k.keywords_normalized()
            .filter(|k| !skip_words.contains(k)).collect();
        k.sort_unstable();
        k
    }).collect::<HashSet<_>>();

    let most_common = most_common_in_results(&keyword_sets)?;
    let mut alt = Vec::new();
    let mut last = most_common.as_str();
    for _ in 0..5 {
        keyword_sets.retain(|s| !s.contains(&last));
        if keyword_sets.len() < 50 {
            break;
        }
        if let Some(another) = most_common_in_results(&keyword_sets) {
            alt.push(another);
            last = alt.last().unwrap();
        } else {
            break;
        }
    }
    Some((most_common, alt))
}

fn most_common_in_results(keyword_sets: &HashSet<Vec<&str>>) -> Option<SmolStr> {
    let mut counts: HashMap<&str, u32> = HashMap::new();
    for k in keyword_sets {
        for k in k {
            *counts.entry(k).or_default() += 1;
        }
    }
    let most_common = counts.into_iter().max_by_key(|&(_, v)| v).map(|(k, _)| k.into())?;
    Some(most_common)
}
