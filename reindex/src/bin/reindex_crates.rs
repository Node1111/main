use std::time::Instant;
use ahash::HashSet;
use ahash::HashSetExt;
use anyhow::anyhow;
use debcargo_list::DebcargoList;
use feat_extractor::{is_autopublished, is_deprecated, is_squatspam, wlita};
use futures::future::join_all;
use futures::Future;
use futures::stream::StreamExt;
use itertools::Itertools;
use kitchen_sink::ArcRichCrateVersion;
use kitchen_sink::CrateOwners;
use kitchen_sink::RichCrate;
use kitchen_sink::SemVer;
use kitchen_sink::SpawnAbortOnDrop;
use kitchen_sink::VersionPopularity;
use kitchen_sink::{self, stop, stopped, KitchenSink, Origin, RichCrateVersion};
use log::debug;
use log::info;
use log::warn;
use parking_lot::Mutex;
use rand::{seq::SliceRandom, thread_rng};
use ranking::CrateTemporalInputs;
use ranking::CrateVersionInputs;
use ranking::OverallScoreInputs;
use render_readme::Links;
use render_readme::LinksContext;
use render_readme::Renderer;
use repo_url::Repo;
use search_index::IndexerData;
use search_index::{CrateSearchIndex, Indexer};
use simple_cache::TempCache;
use std::borrow::Cow;
use std::cmp::Reverse;
use std::convert::TryInto;
use std::pin::Pin;
use std::process::ExitCode;
use std::time::Duration;
use tokio::sync::mpsc;
use triomphe::Arc;
use udedokei::LanguageExt;
use udedokei::synscrape::TextFromSourceCode;

struct Reindexer {
    crates: KitchenSink,
    deblist: DebcargoList,
}

/// (ver, downloads_per_month, source_code_texts, score)
type SearchIndexDataTmp = (ArcRichCrateVersion, usize, Option<TextFromSourceCode>, f64);

fn main() -> ExitCode {
    if let Err(e) = run() {
        eprintln!("Failed: {e}");
        for c in e.chain() {
            eprintln!("  {c}");
        }
        ExitCode::FAILURE
    } else {
        ExitCode::SUCCESS
    }
}

fn run() -> anyhow::Result<()> {
    let (options, crate_filters) = std::env::args().skip(1).partition::<Vec<_>, _>(|a| a.starts_with('-'));

    let everything = options.iter().any(|a| a == "--all");
    let search_only = options.iter().any(|a| a == "--search");
    let specific: Vec<_> = crate_filters.into_iter().map(|name| {
        Origin::try_from_crates_io_name(&name).unwrap_or_else(|| Origin::from_str(name))
    }).collect();

    let repos = !everything && !search_only && specific.len() < 20;
    let rt = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .thread_name("reindex")
        .build()?;


    let crates = rt.block_on(kitchen_sink::KitchenSink::new_default())?;
    let deblist = DebcargoList::new(crates.main_data_dir()).expect("deblist");
    if everything {
        deblist.update().expect("debcargo"); // it needs to be updated sometime, but not frequently
    }
    let r = Arc::new(Reindexer { crates, deblist });
    let mut indexer = Indexer::new(CrateSearchIndex::new(r.crates.main_data_dir()).expect("init search")).expect("init search indexer");
    let lines = TempCache::new(r.crates.main_data_dir().join("search-uniq-lines.dat"), Duration::ZERO).expect("init lines cache");
    let (tx, mut rx) = mpsc::channel::<SearchIndexDataTmp>(64);

    let r2 = r.clone();
    let async_cleanup_thread = std::thread::spawn(move || loop {
        std::thread::sleep(Duration::from_millis(1000));
        if stopped() {
            eprintln!("Flushing caches");
            r2.crates.cleanup();
            return;
        }
    });

    let index_thread = SpawnAbortOnDrop(rt.spawn({
        async move {
            let renderer = Arc::new(Renderer::new(None));
            let mut n = 0usize;
            let mut next_n = 100usize;
            while let Some((ver, downloads_per_month, source_code_texts, score)) = rx.recv().await {
                if stopped() {break;}
                blocking::block_in_place("indxsear", || {
                    index_search(&mut indexer, &lines, &renderer, &ver, downloads_per_month, source_code_texts, score)?;
                    n += 1;
                    if n == next_n {
                        next_n *= 2;
                        println!("savepoint…");
                        indexer.commit()?;
                    }
                    Ok::<_, anyhow::Error>(())
                })?;
            }
            blocking::block_in_place("indx-cmmit", || indexer.commit())?;
            let _ = indexer.bye()?;
            Ok::<_, anyhow::Error>(())
        }
    }));

    rt.spawn_blocking({
        let r = r.clone();
        move || r.crates.prewarm()
    });

    let c: Box<dyn Iterator<Item = Origin> + Send> = if !specific.is_empty() {
        Box::new(specific.into_iter())
    } else if everything || search_only {
        let mut urgent = rt.block_on(r.crates.crates_to_reindex(2000))?
            .into_iter().collect::<Vec<_>>();
        urgent.shuffle(&mut thread_rng());
        let mut all = r.crates.index()?.all_crates()?;
        all.shuffle(&mut thread_rng());
        Box::new(urgent.into_iter().chain(all))
    } else {
        Box::new(rt.block_on(r.crates.crates_to_reindex(500))?.into_iter())
    };

    rt.block_on(SpawnAbortOnDrop(rt.spawn(async move {
        is_send(main_indexing_loop(r, c, tx, repos, search_only, everything)).await;
        if stopped() {return;}
        index_thread.await.unwrap().unwrap();
    })))?;

    stop();
    let _ = async_cleanup_thread.join();
    Ok(())
}

async fn main_indexing_loop(ref r: Arc<Reindexer>, crate_origins: Box<dyn Iterator<Item=Origin> + Send>, tx: mpsc::Sender<SearchIndexDataTmp>, repos: bool, search_only: bool, skip_heavy_indexing: bool) {
    let renderer = &Renderer::new(None);
    let seen_repos = &Mutex::new(HashSet::new());
    let repo_concurrency = &tokio::sync::Semaphore::new(4);
    futures::stream::iter(crate_origins.enumerate()).for_each_concurrent(16, move |(i, origin)| {
        let mut tx = tx.clone();
        async move {
        if stopped() {return;}
        println!("{i}…");
        if !search_only {
            match run_timeout(&format!("hv-{origin:?}"), 62, Box::pin(r.crates.index_crate_highest_version(&origin, skip_heavy_indexing))).await {
                Ok(()) => {},
                err => {
                    print_res(err);
                    if skip_heavy_indexing { return; }
                    // on hourly updates, try to continue and update score, search, etc.
                },
            }
        }
        if stopped() {return;}
        if i%3000 == 0 {
            info!("flushing caches");
            r.crates.cleanup(); // reduce mem usage, write stuff to disk
        }
        match run_timeout(&format!("rc-{origin:?}"), 59, Box::pin(is_send(r.index_crate(&origin, renderer, &mut tx)))).await {
            Ok(v) => {
                if repos && !v.is_spam() {
                    for repo in r.repositories_for_crate(&v).await {
                        {
                            let mut s = seen_repos.lock();
                            let url = repo.canonical_git_url();
                            if s.contains(&*url) {
                                continue;
                            }
                            println!("Indexing repo of {}: {url}", v.short_name());
                            s.insert(url);
                        }
                        let _finished = repo_concurrency.acquire().await;
                        if stopped() {return;}
                        print_res(run_timeout(&format!("rep-{repo:?}"), 500, Box::pin(r.crates.index_repo(&repo, v.version()))).await);
                    }
                }
            },
            err => print_res(err),
        }
    }}).await;
}

impl Reindexer {
    async fn repositories_for_crate<'a>(&self, k: &'a RichCrateVersion) -> Vec<Cow<'a, Repo>> {
        if let Some(repo) = k.repository() {
            return vec![Cow::Borrowed(repo)];
        }

        let crate_name = k.short_name();
        let owners = self.crates.crate_owners(k.origin(), CrateOwners::Strict).await.unwrap_or_default();
        debug!("{crate_name} has no repo property, checking by {} owners", owners.len());

        let repos = join_all(owners.iter().filter_map(|o| o.github_login()).map(|login| async move {
            self.crates.gh.all_repos(login).await.map_err(|e| warn!("{login} repos fail: {e}")).unwrap_or_default().unwrap_or_default()
        })).await;
        repos.into_iter().flatten().filter(|repo| {
            debug!("{crate_name} is it in {:?}", repo);
            repo.name.contains(crate_name) || repo.name.eq_ignore_ascii_case(crate_name)
        })
        .filter_map(|repo| {
            let url = format!("https://github.com/{}/{}", repo.owner.as_ref()?.login, repo.name);
            info!("will try to find crate {crate_name} in repo {url}");
            Repo::new(&url).ok().map(Cow::Owned)
        }).take(3).collect()
    }

async fn index_crate(&self, origin: &Origin, renderer: &Renderer, search_sender: &mut mpsc::Sender<SearchIndexDataTmp>) -> Result<ArcRichCrateVersion, anyhow::Error> {
    let crates = &self.crates;
    let (k, v) = futures::try_join!(
        run_timeout("rca", 31, Box::pin(is_send(crates.rich_crate_async(origin)))),
        run_timeout("rcv", 45, Box::pin(is_send(crates.rich_crate_version_async(origin)))),
    )?;

    let source_code_texts = crates.extra_derived_storage.get_deserialized::<TextFromSourceCode>((origin.to_str().as_str(), "1"))?;

    let (downloads_per_month, score) = run_timeout("score", 41, Box::pin(is_send(self.crate_overall_score(&k, &v, renderer)))).await?;
    debug!("{origin:?} has score {score} and {downloads_per_month}dl/mo");
    let (_, index_res) = futures::join!(
        run_timeout("ssend", 10, Box::pin(async {
            search_sender.send((v.clone(), downloads_per_month, source_code_texts, score)).await
                .map_err(|e| {stop();e})
        })),
        run_timeout("ic", 50, Box::pin(crates.index_crate(&k, score)))
    );
    index_res?;
    Ok(v)
}
}

fn index_search(indexer: &mut Indexer, lines: &TempCache<(String, f64), [u8; 16]>, renderer: &Renderer, k: &RichCrateVersion, downloads_per_month: usize, source_code_texts: Option<TextFromSourceCode>, score: f64) -> Result<(), anyhow::Error> {
    let to_delete = k.is_spam() || k.is_hidden();

    if to_delete {
        indexer.delete(k.origin());
        return Ok(());
    }

    if stopped() {
        anyhow::bail!("aborted");
    }

    let keywords: Vec<_> = if !k.is_spam() { k.keywords().iter().map(|s| s.as_str()).collect() } else { Vec::new() };
    let version = k.version();
    let mut unique_text = String::with_capacity(4000);

        let mut variables = Vec::with_capacity(15);
        variables.push(k.short_name());
        let repo;
        if let Some(r) = k.repository() {
            if let Some(name) = r.repo_name() {
                variables.push(name);
            }
            repo = r.canonical_http_url("", None);
            variables.push(&repo);
        }
        for name in k.authors().iter().filter_map(|a| a.name.as_deref()) {
            variables.push(name);
        }
        variables.sort_unstable_by_key(|a| Reverse(a.len())); // longest first

        let mut dupe_sentences = HashSet::new();
        let mut cb = |key: &str, orig: &str| {
            let key: [u8; 16] = blake3::hash(key.as_bytes()).as_bytes()[..16].try_into().unwrap();
            if dupe_sentences.insert(key) {
                let (matches, wins) = if let Ok(Some((their_crate_name, their_score))) = lines.get(&key) {
                    (k.short_name() == their_crate_name, score > their_score)
                } else {
                    (true, true)
                };
                if wins {
                    lines.set(key, (k.short_name().to_string(), score)).expect("upd lines");
                }
                if matches || wins {
                    unique_text.push_str(orig);
                    unique_text.push('\n');
                }
            }
        };

        let mut dedup = wlita::WLITA::new(variables.iter(), &mut cb);
        if let Some(markup) = k.readme().map(|readme| &readme.markup) {
            for (s, par) in &renderer.visible_text_by_section(markup) {
                dedup.add_text(s);
                dedup.add_text(par);
            }
        }
        if let Some(markup) = k.lib_file_markdown() {
            for (s, par) in &renderer.visible_text_by_section(&markup) {
                dedup.add_text(s);
                dedup.add_text(par);
            }
        }

    let mut extra_keywords = source_code_texts.map(|t| {
        Itertools::intersperse(t.doc.iter().map(|(_, s)| s.as_str())
            .chain(t.idents.iter().map(|(_, s)| s.as_str())), " ")
        .collect::<String>()
    });

    let crate_name = k.short_name();
    if crate_name.as_bytes().iter().any(|&c| c == b'_' || c == b'-') {
        let extra_keywords = extra_keywords.get_or_insert_with(String::new);
        if !extra_keywords.is_empty() {
            extra_keywords.push(' ');
        }
        // Make serde-json searchable by serdejson
        extra_keywords.extend(crate_name.chars().filter(|&c| c != '_' && c != '-'));
        extra_keywords.push(' ');
        // Make serde-json searchable by 'json serde'
        extra_keywords.extend(crate_name.chars().map(|c| if c.is_ascii_alphabetic() {c} else {' '}));
    }

    indexer.add(IndexerData {
        origin: k.origin(),
        crate_name,
        version,
        description: k.description().unwrap_or(""),
        keywords: &keywords,
        readme: Some(unique_text.as_str()).filter(|s| !s.trim_start().is_empty()),
        monthly_downloads: downloads_per_month as u64,
        extra_keywords,
        score,
    })?;
    Ok(())
}

impl Reindexer {
    async fn crate_overall_score(&self, all: &RichCrate, k: &RichCrateVersion, renderer: &Renderer) -> Result<(usize, f64), anyhow::Error> {
        let crates = &self.crates;
        let origin = all.origin();
        let image_deadline = Instant::now() + Duration::from_secs(2);
        let stats = crates.index()?.deps_stats()?;

        let (traction_stats, downloads_per_month, has_docs_rs, has_verified_repository_link, (do_block, blocklist_actions), is_sub_component, vet_reviews) = futures::join!(
            run_timeout("ts", 10, Box::pin(crates.traction_stats(origin, stats))),
            run_timeout("dl", 10, Box::pin(crates.downloads_per_month_or_equivalent(origin))),
            crates.has_docs_rs(origin, k.short_name(), k.version()),
            crates.has_verified_repository_link(k),
            crates.crate_blocklist_actions(all),
            crates.is_sub_component(k),
            crates.vets_for_crate(origin),
        );
        let has_verified_repository_link = has_verified_repository_link?;
        let crev_reviews = crates.reviews_for_crate(origin);
        let ablocklist_banned = do_block && blocklist_actions.iter().any(|b| b.is_ban());
        let ablocklist_hidden = do_block && blocklist_actions.iter().any(|b| b.is_block());
        let ablocklist_redirect = do_block && blocklist_actions.iter().any(|b| b.is_redirect());
        let traction_stats = traction_stats?;
        let vet_reviews = vet_reviews?;
        let has_vet_reviews = vet_reviews.iter().any(|r| r.version().is_some() && !r.is_wildcard_trust()); // version is on full code reviews
        let has_vet_trust = vet_reviews.iter().any(|r| r.is_wildcard_trust());
        let downloads_per_month = downloads_per_month?.unwrap_or(0) as u32;
        let advisories = crates.advisories_for_crate(origin)?;
        let semver: SemVer = k.version().parse()?;
        let is_unmaintained = crates.is_unmaintained_in_advisories(k.origin(), &advisories, &crev_reviews, &semver)?;
        let has_crev_reviews = !crev_reviews.is_empty();

        let (contrib_info, owners, is_repo_archived) = futures::try_join!(
            run_timeout("contr", 20, Box::pin(crates.all_contributors(k))),
            run_timeout("own", 20, Box::pin(crates.crate_owners(k.origin(), CrateOwners::All))),
            async {
                if let Some(repo) = k.repository() {
                    if let Some(github_repo) = Box::pin(crates.github_repo(repo)).await? {
                        return Ok(github_repo.archived);
                    }
                }
                Ok(false)
            }
        )?;

        let contributors_count = contrib_info.0.len() as u32;

        let langs = k.language_stats();
        let (rust_code_lines, rust_comment_lines) = langs.langs.get(&udedokei::Language::Rust).map(|rs| (rs.code, rs.comments)).unwrap_or_default();
        let total_code_lines = langs.langs.iter().filter(|(k, _)| k.is_code()).map(|(_, l)| l.code).sum::<u32>();
        let base_score = ranking::crate_score_version(&CrateVersionInputs {
            versions: all.versions(),
            description: k.description().unwrap_or(""),
            readme: k.readme().map(|readme| {
                renderer.page_node(&readme.markup, &LinksContext {
                    base_url: None,
                    nofollow: Links::Ugc,
                    own_crate_name: Some(k.short_name()),
                    link_own_crate_to_crates_io: false,
                    link_fixer: None,
                }, image_deadline)
            }).as_ref(),
            owners: &owners,
            authors: k.authors(),
            contributors: Some(contributors_count),
            edition: k.edition(),
            total_code_lines,
            rust_code_lines,
            rust_comment_lines,
            is_app: k.is_app(),
            has_build_rs: k.has_buildrs(),
            has_links: k.links().is_some(),
            has_documentation_link: k.documentation().is_some(),
            has_homepage_link: k.homepage().is_some(),
            has_repository_link: k.repository().is_some(),
            has_verified_repository_link,
            has_keywords: k.has_own_keywords(),
            has_own_categories: k.has_own_categories(),
            has_features: !k.features().is_empty(),
            has_examples: k.has_examples(),
            has_code_of_conduct: k.has_code_of_conduct(),
            has_benches: k.has_benches(),
            has_tests: k.has_tests(),
            // has_lockfile: k.has_lockfile(),
            // has_changelog: k.has_changelog(),
            license: k.license().unwrap_or(""),
            has_badges: k.has_badges(),
            maintenance: k.maintenance(),
            is_nightly: k.is_nightly(),
        });

        let (runtime, _, build) = k.direct_dependencies();

        // outdated dev deps don't matter
        let dependency_freshness = join_all(runtime.iter().chain(&build).filter_map(move |richdep| {
                if let rich_crate::Dependency::Inherited(_) = &richdep.dep {
                    return None;
                }
                if !richdep.dep.is_crates_io() {
                    return None;
                }
                let req = richdep.dep.req().parse().ok()?;
                let origin = Origin::try_from_crates_io_name(&richdep.package)?;
                Some(async move {
                    let pop = run_timeout("depf", 15, Box::pin(is_send(crates.version_popularity(&origin, &req, stats)))).await
                        .map_err(|e| log::error!("ver1pop {}", e)).unwrap_or(None);
                    (richdep.is_optional(), pop.unwrap_or(VersionPopularity { matches_latest: true, pop: 0., lost_popularity: false, deprecated: false}))
                })
            })).await
            .into_iter().map(|(is_optional, pop)| {
                if pop.matches_latest {1.0} // don't penalize pioneers
                else if is_optional {0.8 + pop.pop * 0.2} // not a big deal when it's off
                else {pop.pop}
            })
            .collect();

        let is_in_debian = self.deblist.has(k.short_name()).map_err(|e| log::error!("debcargo check: {}", e)).unwrap_or(false);

        let owner_trust = crates.crate_owners_trust(&owners)?;
        let mut temp_inp = CrateTemporalInputs {
            owners: &owners,
            owner_trust: &owner_trust,
            traction_stats,
            versions: all.versions(),
            is_app: k.is_app(),
            is_proc_macro: k.is_proc_macro(),
            has_docs_rs,
            is_nightly: k.is_nightly(),
            downloads_per_month,
            downloads_per_month_minus_most_downloaded_user: downloads_per_month, // fixed below
            number_of_direct_reverse_deps: 0,
            number_of_indirect_reverse_deps: 0,
            number_of_indirect_reverse_optional_deps: 0,
            dependency_freshness,
            is_in_debian,
            has_vet_reviews,
            has_crev_reviews,
            has_vet_trust,
        };

        let mut has_many_direct_rev_deps = false;
        if let Some(deps) = stats.crates_io_full_dependents_stats_of(origin) {
            let direct_rev_deps = deps.counters.direct.all();
            if direct_rev_deps > 50 {
                has_many_direct_rev_deps = true;
            }
            let indirect_reverse_optional_deps = (deps.counters.runtime.def + deps.counters.runtime.opt)
                .max(u32::from(deps.counters.dev))
                .max(deps.counters.build.def + deps.counters.build.opt);

            temp_inp.number_of_direct_reverse_deps = direct_rev_deps;
            temp_inp.number_of_indirect_reverse_deps = deps.counters.runtime.def.max(deps.counters.build.def);
            temp_inp.number_of_indirect_reverse_optional_deps = indirect_reverse_optional_deps;
            let tmp = futures::future::join_all(
                deps.rev_dep_names_default.iter().map(|n| (n, false)).chain(
                deps.rev_dep_names_optional.iter().map(|n| (n, true)))
                .filter_map(|(name, opt)| Some((Origin::try_from_crates_io_name(name)?, opt)))
                .map(|(o, opt)| async move {
                    let dl = run_timeout("dlpm", 5, Box::pin(crates.downloads_per_month(&o))).await.unwrap_or_default().unwrap_or_default();
                    if opt { dl / 2 } else { dl } // hyper-tls vs reqwest
                })).await;
            let biggest = tmp.into_iter().max().unwrap_or(0);
            temp_inp.downloads_per_month_minus_most_downloaded_user = downloads_per_month.saturating_sub(biggest as u32);
        }

        let is_deprecated = is_deprecated(k);
        crates.update_deprecation_status(k.origin(), is_deprecated);

        let temp_score = ranking::crate_score_temporal(&temp_inp);
        let overall = OverallScoreInputs {
            former_glory: traction_stats.map_or(1., |t| t.former_glory),
            is_proc_macro: k.is_proc_macro(),
            is_sys: k.is_sys(),
            is_sub_component,
            is_internal: crates.is_internal_crate(k) && !has_many_direct_rev_deps,
            is_autopublished: is_autopublished(k),
            is_deprecated: is_deprecated.is_some(),
            is_crates_io_published: k.origin().is_crates_io(),
            is_yanked: k.is_yanked(),
            is_squatspam: is_squatspam(k).is_some() || ablocklist_banned,
            // Check out https://web3isgoinggreat.com
            is_vaporware_or_ponzi_scheme: k.category_slugs().iter().any(|c| &**c == "cryptography::cryptocurrencies"),
            ablocklist_deadend: ablocklist_hidden || ablocklist_banned,
            ablocklist_redirect,
            traction_stats,
            is_unmaintained: is_unmaintained || is_repo_archived,
        };

        debug!("score {base_score:?} {temp_score:?} {overall:?}");
        let score = ranking::combined_score(base_score, temp_score, &overall);

        Ok((downloads_per_month as usize, score))
    }
}

fn print_res<T>(res: Result<T, anyhow::Error>) {
    if let Err(e) = res {
        let s = e.to_string();
        if s.starts_with("Too many open files") {
            stop();
            panic!("{}", s);
        }
        log::error!("••• Error: {}", s);
        for c in e.chain().skip(1) {
            let s = c.to_string();
            log::error!("•   error: -- {}", s);
            if s.starts_with("Too many open files") {
                stop();
                panic!("{}", s);
            }
        }
    }
}

async fn run_timeout<'a, 'b, T, E>(label: &'b str, secs: u64, fut: Pin<Box<dyn Future<Output = Result<T, E>> + Send + 'a>>) -> Result<T, anyhow::Error>
    where anyhow::Error: From<E> {
    let res = tokio::time::timeout(Duration::from_secs(secs), blocking::awatch(label, fut)).await.map_err(move |_| anyhow!("{label} timed out {secs}"))?;
    res.map_err(From::from)
}

#[inline(always)]
fn is_send<T: Send>(t: T) -> T {t}
