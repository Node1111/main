use futures::try_join;
use util::SmolStr;
use kitchen_sink::running;
use rand::seq::SliceRandom;
use tokio::task::JoinSet;
use anyhow::bail;
use kitchen_sink::{stopped, KitchenSink, OwnerKind, CrateOwner};
use std::{
    collections::HashSet,
    sync::{Arc, Mutex}, process::ExitCode,
};

#[tokio::main]
async fn main() -> ExitCode {
    let crates = Arc::new(KitchenSink::new_default().await.unwrap());

    if let Err(e) = run(crates).await {
        dump_error(&e);
        ExitCode::FAILURE
    } else {
        eprintln!("finished");
        ExitCode::SUCCESS
    }
}

fn dump_error(e: &anyhow::Error) {
    eprintln!("••• error: {e}");
    for c in e.chain() {
        eprintln!("•   error: -- {c}");
    }
}

async fn run(crates: Arc<KitchenSink>) -> Result<(), anyhow::Error> {
    let mut all_crates = crates.index()?.all_crates()?;
    all_crates.shuffle(&mut rand::thread_rng());

    let mut waiting = JoinSet::new();
    let concurrency = Arc::new(tokio::sync::Semaphore::new(16));
    let seen = Arc::new(Mutex::new(HashSet::new()));
    let seen_owners = Arc::new(Mutex::new(HashSet::new()));
    for o in all_crates {
        let _f = concurrency.acquire().await?;
        if stopped() {
            eprintln!("STOPPING");
            break;
        }

        let crates = Arc::clone(&crates);
        let concurrency = Arc::clone(&concurrency);
        let seen = Arc::clone(&seen);
        let seen_owners = Arc::clone(&seen_owners);
        waiting.spawn(async move {
            if stopped() { bail!("stop"); }
            let _f = concurrency.acquire().await?;
            if stopped() { bail!("stop"); }

            print!("{}… ", o.package_name_icase());
            let c = match crates.rich_crate_version_async(&o).await {
                Ok(c) => c,
                Err(e) => {
                    dump_error(&e);
                    return Ok(()); // don't stop entire process due to one failed crate
                }
            };
            let owners = crates.crate_owners(&o, kitchen_sink::CrateOwners::All).await.unwrap_or_default();
            for owner in owners.iter().filter(|o| o.kind == OwnerKind::User && o.github_id.is_some()) {
                let github_id = owner.github_id.unwrap_or(0);
                if seen_owners.lock().unwrap().insert(github_id) {
                    print!("@{}… ", owner.crates_io_login);
                    if let Err(err) = index_owner(&crates, owner).await {
                        eprint!("•• {}:", owner.crates_io_login);
                        dump_error(&err);
                    }
                }
            }

            running()?;

            let emails_to_index: Vec<_> = c.authors().iter().filter_map(|a| {
                Some((a.email.as_deref()?, a.name.as_deref()))
            }).filter(|&(email, _)| {
                seen.lock().unwrap().insert(SmolStr::from(email))
            }).collect();
            if !emails_to_index.is_empty() {
                let Ok(all) = crates.rich_crate_async(&o).await else { return Ok(()); };
                let data_source_date = all.most_recent_release().into();
                for (email, name) in emails_to_index {
                    if let Err(err) = crates.index_email(email, name, data_source_date).await {
                        eprint!("•• {email}");
                        dump_error(&err);
                    }
                }
            }
            Ok::<_, anyhow::Error>(())
        });
    }
    running()?;
    while let Some(res) = waiting.join_next().await {
        res??;
    }
    waiting.shutdown().await;
    Ok(())
}

async fn index_owner(crates: &KitchenSink, owner: &CrateOwner) -> Result<(), anyhow::Error> {
    let Some(github_id) = owner.github_id else { bail!("owner without github ID!? {owner:?}") };
    let Some(login) = owner.github_login() else { bail!("bad owner {owner:?}") };

    let (mut owner_crates, orgs) = try_join!(crates.all_crates_of_github_id(github_id), crates.user_github_orgs(login))?;

    let oldest_crate = owner_crates.iter().filter_map(|c| c.invited_at).min();
    owner_crates.retain(|c| c.deleted_at.is_none());

    let owner_ranking_score = ranking::owner_ranking(&ranking::OwnerRankingInputs {
        oldest_crate,
        github_id,
        login,
        lowercase_github_org_names: orgs.unwrap_or_default()
        .into_iter().map(|mut org| {
            org.login.make_ascii_lowercase();
            org.login
        }).collect(),
    });

    crates.index_owner(owner, &owner_crates, owner_ranking_score).await
}
