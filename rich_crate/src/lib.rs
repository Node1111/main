use util::CowAscii;
pub use cargo_author::*;
use std::fmt;
mod rich_crate;
pub use crate::rich_crate::*;
mod rich_crate_version;
pub use crate::rich_crate_version::*;
use util::SmolStr;

pub use cargo_toml::Manifest;
pub use render_readme::Markup;
pub use render_readme::Readme;
pub use repo_url::Repo;
pub use repo_url::SimpleRepo;
pub use repo_url::RepoPackage;

/// URL-like identifier of location where crate has been published + normalized crate name
#[derive(Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub enum Origin {
    CratesIo(SmolStr),
    GitHub(Box<RepoPackage>),
    GitLab(Box<RepoPackage>),
}

impl fmt::Debug for Origin {
    #[cold]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Origin::CratesIo(name) => write!(f, "Origin( lib.rs/{name} )"),
            Origin::GitHub(r) => write!(f, "Origin( github.com/{}/{} {} )", r.repo.owner, r.repo.repo, r.package),
            Origin::GitLab(r) => write!(f, "Origin( gitlab.com/{}/{} {} )", r.repo.owner, r.repo.repo, r.package),
        }
    }
}

impl Origin {
    #[inline]
    #[track_caller]
    #[must_use] pub fn from_crates_io_name(name: &str) -> Self {
        match Self::try_from_crates_io_name(name) {
            Some(n) => n,
            None => panic!("bad crate name: '{name}'"),
        }
    }

    #[inline]
    #[must_use] pub fn try_from_crates_io_name(name: &str) -> Option<Self> {
        if Self::is_valid_crate_name(name) {
            Some(Origin::CratesIo(name.as_ascii_lowercase().into()))
        } else {
            None
        }
    }

    #[inline(always)]
    #[must_use] pub fn is_valid_crate_name(name: &str) -> bool {
        !name.is_empty() && is_alnum(name)
    }

    #[inline]
    pub fn from_github(repo: SimpleRepo, package: impl Into<SmolStr>) -> Self {
        Origin::GitHub(Box::new(RepoPackage { repo, package: package.into() }))
    }

    #[inline]
    pub fn from_gitlab(repo: SimpleRepo, package: impl Into<SmolStr>) -> Self {
        Origin::GitLab(Box::new(RepoPackage { repo, package: package.into() }))
    }

    #[inline]
    #[must_use] pub fn from_repo(r: &Repo, package: &str) -> Option<Self> {
        match r.host() {
            Repo::GitHub(r) => Some(Self::from_github(r.clone(), package)),
            Repo::GitLab(r) => Some(Self::from_gitlab(r.clone(), package)),
            _ => None,
        }
    }

    #[track_caller]
    pub fn from_str(s: impl AsRef<str>) -> Self {
        let s = s.as_ref();
        let mut n = s.splitn(2, ':');
        let host = n.next().unwrap();
        match host {
            "crates.io" => Self::from_crates_io_name(n.next().expect("parse")),
            "github" | "gitlab" => {
                let mut n = n.next().expect("parse").splitn(3, '/');
                let owner = n.next().expect("parse").into();
                let repo = n.next().expect("parse").into();
                let package = n.next().expect("parse");
                if host == "github" {
                    Self::from_github(SimpleRepo { owner, repo }, package)
                } else {
                    Self::from_gitlab(SimpleRepo { owner, repo }, package)
                }
            },
            _ => panic!("bad str {s}"),
        }
    }

    #[must_use] pub fn to_str(&self) -> String {
        match *self {
            Origin::CratesIo(ref s) => format!("crates.io:{s}"),
            Origin::GitHub(ref r) => format!("github:{}/{}/{}", r.repo.owner, r.repo.repo, r.package),
            Origin::GitLab(ref r) => format!("gitlab:{}/{}/{}", r.repo.owner, r.repo.repo, r.package),
        }
    }

    #[inline]
    #[must_use] pub fn package_name_icase(&self) -> &str {
        match self {
            Origin::CratesIo(s) => s,
            Origin::GitHub(r) |
            Origin::GitLab(r) => &r.package,
        }
    }

    #[inline]
    #[must_use] pub fn is_crates_io(&self) -> bool {
        matches!(self, Origin::CratesIo(_))
    }

    #[inline]
    #[must_use] pub fn repo(&self) -> Option<(Repo, SmolStr)> {
        match self {
            Origin::CratesIo(_) => None,
            Origin::GitHub(r) => Some((Repo::GitHub(r.repo.clone()), SmolStr::from(&*r.package))),
            Origin::GitLab(r) => Some((Repo::GitLab(r.repo.clone()), SmolStr::from(&*r.package))),
        }
    }

    #[inline]
    #[must_use] pub fn simple_repo(&self) -> Option<(&SimpleRepo, &str)> {
        match self {
            Origin::CratesIo(_) => None,
            Origin::GitHub(r) |
            Origin::GitLab(r) => Some((&r.repo, &r.package)),
        }
    }

    #[inline]
    #[must_use] pub fn into_repo(self) -> Option<(Repo, SmolStr)> {
        match self {
            Origin::CratesIo(_) => None,
            Origin::GitHub(r) => Some((Repo::GitHub(r.repo), r.package)),
            Origin::GitLab(r) => Some((Repo::GitLab(r.repo), r.package)),
        }
    }
}

#[test]
fn roundtrip() {
    let o1 = Origin::from_crates_io_name("hello");
    let o2 = Origin::from_str(o1.to_str());
    assert_eq!(o1, o2);
    assert_eq!("hello", o2.package_name_icase());
}

#[test]
fn roundtrip_gh() {
    assert!(32 <= std::mem::size_of::<Origin>());

    let o1 = Origin::from_github(SimpleRepo { owner: "foo".into(), repo: "bar".into() }, "baz");
    let o3 = Origin::from_github(SimpleRepo { owner: "foo".into(), repo: "bar".into() }, "other_package");
    let o2 = Origin::from_str(o1.to_str());
    assert_eq!(o1, o2);
    assert_ne!(o1, o3);
    assert_eq!("baz", o2.package_name_icase());
}

fn is_alnum(q: &str) -> bool {
    q.as_bytes().iter().copied().all(|c| c.is_ascii_alphanumeric() || c == b'_' || c == b'-')
}
