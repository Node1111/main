use util::CowAscii;
use parking_lot::RwLockReadGuard;
use dashmap::DashMap;
use old_semver::SemVerError;
use rayon::prelude::*;
pub use crates_index::DependencyKind;
pub use crates_index::Version;
use util::PushInCapacity;
use crate::deps_stats::DepsStats;
use crate::DepsErr;
use crate::git_crates_index::GitIndex;
pub use crates_index::Crate;
use crates_index::Dependency;
use log::{debug, warn, info, error};
use parking_lot::RwLock;
use rich_crate::Origin;
use rich_crate::RichCrateVersion;
use rich_crate::RichDep;
use semver::Version as SemVer;
use semver::VersionReq;
use serde::{Deserialize, Serialize};
use util::SmolStr;
use std::panic::RefUnwindSafe;
use std::panic::UnwindSafe;
use std::path::{Path, PathBuf};
use std::time::Duration;
use std::time::Instant;
use string_interner::StringInterner;
use string_interner::symbol::SymbolU32 as Sym;
use ahash::{HashMap, HashSet};
use triomphe::Arc;

#[derive(Debug, Clone, Eq, PartialEq, Hash, Serialize, Deserialize, Default, PartialOrd)]
pub struct MiniVer {
    pub major: u16,
    pub minor: u16,
    pub patch: u16,
    pub build: u16,
    pub pre: Box<[old_semver::Identifier]>,
}

impl MiniVer {
    #[must_use] pub fn to_semver(&self) -> SemVer {
        if self.pre.is_empty() && self.build == 0 {
            return SemVer {
                major: self.major.into(),
                minor: self.minor.into(),
                patch: self.patch.into(),
                pre: semver::Prerelease::EMPTY,
                build: semver::BuildMetadata::EMPTY
            };
        }
        old_semver::Version {
            major: self.major.into(),
            minor: self.minor.into(),
            patch: self.patch.into(),
            pre: self.pre.clone().into(),
            build: if self.build > 0 { vec![old_semver::Identifier::Numeric(self.build.into())] } else { Vec::new() },
        }.to_string().parse().unwrap_or(SemVer { major: 0, minor: 0, patch: 0, pre: semver::Prerelease::EMPTY, build: semver::BuildMetadata::EMPTY })
    }
}

pub trait FeatureGetter {
    fn get_actions_for_feature_key(&self, key: &str) -> Option<&[String]>;
}
impl FeatureGetter for std::collections::HashMap<String, Vec<String>> {
    fn get_actions_for_feature_key(&self, key: &str) -> Option<&[String]> {
        self.get(key).map(|v| v.as_slice())
    }
}
impl FeatureGetter for std::collections::BTreeMap<String, Vec<String>> {
    fn get_actions_for_feature_key(&self, key: &str) -> Option<&[String]> {
        self.get(key).map(|v| v.as_slice())
    }
}

pub trait IVersion {
    type Features: FeatureGetter;
    fn name(&self) -> &str;
    fn version(&self) -> &str;
    fn dependencies(&self) -> Fudge;
    fn features(&self) -> &Self::Features;
    fn is_yanked(&self) -> bool;
}

impl IVersion for Version {
    type Features = std::collections::HashMap<String, Vec<String>>;
    fn name(&self) -> &str {self.name()}
    fn version(&self) -> &str {self.version()}
    fn dependencies(&self) -> Fudge {Fudge::CratesIo(self.dependencies())}
    fn features(&self) -> &Self::Features  {self.features()}
    fn is_yanked(&self) -> bool {self.is_yanked()}
}

pub trait ICrate {
    type Ver: IVersion;
    /// Returns feature keys, not actions. Don't include default.
    fn latest_version_with_all_features_for_deps(&self) -> (&Self::Ver, Option<Box<[SmolStr]>>);
}

impl ICrate for Crate {
    type Ver = Version;
    fn latest_version_with_all_features_for_deps(&self) -> (&Self::Ver, Option<Box<[SmolStr]>>) {
        let latest = CratesIoIndexLookup::find_highest_crates_io_version(self, true);
        let dependencies = latest.dependencies();
        let features = latest.features();

        let deps_iter = dependencies.iter().filter(|d| d.is_optional());
        let feat_iter = features.iter().filter(|&(k, a)| k != "default" && a.iter().any(|a| a.bytes().any(|b| b == b'/')));
        let num = deps_iter.clone().count() +
            // features without / are either redundant or irrelevant for selecting optional deps
            feat_iter.clone().count();
        if num == 0 {
            return (latest, None);
        }

        let mut enable_features = Vec::with_capacity(num);
        // implicit features (this ignores the dep: nuance)
        for d in deps_iter {
            enable_features.push_in_cap(d.name().into());
        }
        for (k, _) in feat_iter {
            enable_features.push_in_cap(k.as_str().into());
        }
        debug_assert_eq!(enable_features.capacity(), enable_features.len());
        (latest, Some(enable_features.into_boxed_slice()))
    }
}

pub enum Fudge<'a> {
    CratesIo(&'a [Dependency]),
    /// Run dev build
    Manifest((Vec<RichDep>, Vec<RichDep>, Vec<RichDep>)),
}

impl<'a> Fudge<'a> {
    pub(crate) fn has_any_dev_deps(&self) -> bool {
        match self {
            Fudge::CratesIo(d) => d.iter().any(|d| d.kind() == DependencyKind::Dev),
            Fudge::Manifest((_, dev, _)) => !dev.is_empty(),
        }
    }
}

impl IVersion for RichCrateVersion {
    type Features = std::collections::BTreeMap<String, Vec<String>>;
    fn name(&self) -> &str {self.short_name()}
    fn version(&self) -> &str {self.version()}
    fn dependencies(&self) -> Fudge {Fudge::Manifest(self.direct_dependencies())}
    fn features(&self) -> &Self::Features {self.features()}
    fn is_yanked(&self) -> bool {self.is_yanked()}
}

impl ICrate for RichCrateVersion {
    type Ver = RichCrateVersion;
    fn latest_version_with_all_features_for_deps(&self) -> (&Self::Ver, Option<Box<[SmolStr]>>) {
        let enable_deps = self.direct_dependencies_normal().iter()
            .chain(self.direct_dependencies_build())
            .chain(self.direct_dependencies_dev())
            .chain(self.direct_dependencies_targets().values().flat_map(|t| {
                t.dependencies.iter()
                    .chain(t.dev_dependencies.iter())
                    .chain(t.build_dependencies.iter())
            }))
            .filter(|(_, d)| d.optional())
            .map(|(k, _)| k.as_str());

        // features without / either enable other features (no change), or optional deps (already included)
        let enable_features = self.features().iter().filter(|&(k, a)| k != "default" && a.iter().any(|a| a.bytes().any(|b| b == b'/')))
            .map(|(k, _)| k.as_str());

        let to_enable = enable_deps.chain(enable_features).map(SmolStr::from).collect::<Vec<_>>();
        (self, if to_enable.is_empty() { None } else { Some(to_enable.into_boxed_slice()) })
    }
}

impl RefUnwindSafe for Index {}
impl UnwindSafe for Index {}

#[derive(Default)]
pub struct CratesIoIndexLookup {
    pub(crate) crates_by_lc: HashMap<SmolStr, Crate>,
}

impl CratesIoIndexLookup {
    fn is_empty(&self) -> bool {
        self.crates_by_lc.is_empty()
    }

    #[inline]
    pub fn len(&self) -> usize {
        self.crates_by_lc.len()
    }

    #[inline]
    pub fn keys(&self) -> impl ExactSizeIterator<Item = &str> {
        self.crates_by_lc.keys().map(|k| k.as_str())
    }

    #[inline]
    pub fn values(&self) -> impl ExactSizeIterator<Item = &Crate> {
        self.crates_by_lc.values()
    }

    #[inline]
    pub fn iter(&self) -> impl ExactSizeIterator<Item = (&str, &Crate)> {
        self.crates_by_lc.iter().map(|(k, c)| (k.as_str(), c))
    }

    #[inline]
    pub fn get(&self, lowercase_name: &str) -> Option<&Crate> {
        debug_assert_eq!(lowercase_name, lowercase_name.as_ascii_lowercase());
        self.crates_by_lc.get(lowercase_name)
    }

    #[inline]
    pub fn get_res(&self, lowercase_name: &str) -> Result<&Crate, DepsErr> {
        debug_assert_eq!(lowercase_name, lowercase_name.as_ascii_lowercase());
        self.crates_by_lc.get(lowercase_name).ok_or_else(move || DepsErr::CrateNotFound(Origin::from_crates_io_name(lowercase_name)))
    }

    /// Does not need fallback to unstable
    pub fn highest_version_by_lowercase_name(&self, lc_name: &str, prefer_stable: bool) -> Option<&Version> {
        let krate = self.get(lc_name)?;

        Some(Self::find_highest_crates_io_version(krate, prefer_stable))
    }

    pub(crate) fn find_highest_crates_io_version(krate: &Crate, prefer_stable: bool) -> &Version {
        krate.versions()
            .iter()
            .rev()
            .max_by_key(move |a| {
                let ver = SemVer::parse(a.version())
                    .map_err(|e| info!("{} has invalid version {}: {}", krate.name(), a.version(), e))
                    .ok();
                let matches_stability = ver.as_ref().map_or(false, move |v| !prefer_stable || v.pre.is_empty());
                let bad = a.is_yanked() || !matches_stability;
                (!bad, ver)
            })
            .unwrap_or_else(move || krate.most_recent_version()) // latest_version = most recently published version
    }

    pub fn highest_version_matching_requirement_by_lowercase_name(&self, crate_name: &str, req: &str) -> Result<SemVer, DepsErr> {
        let krate = self.get_res(crate_name)?;
        let max = self.iter_crates_versions_matching_requirement(krate, req)?
            .rev().max();
        max.ok_or(DepsErr::NoVersionsMatched)
    }

    pub fn lowest_version_matching_requirement_by_lowercase_name(&self, crate_name: &str, req: &str) -> Result<SemVer, DepsErr> {
        let krate = self.get_res(crate_name)?;
        let min = self.iter_crates_versions_matching_requirement(krate, req)?
            .min();
        min.ok_or(DepsErr::NoVersionsMatched)
    }

    #[inline]
    fn iter_crates_versions_matching_requirement<'a>(&self, krate: &'a Crate, req: &str) -> Result<impl DoubleEndedIterator<Item=SemVer> + 'a, DepsErr> {
        let req = VersionReq::parse(req).map_err(|e| DepsErr::SemverParsingError(e.to_string()))?;
        Ok(krate
            .versions()
            .iter()
            .filter_map(move |v| {
                let semver = v.version().parse().ok()?;
                if req.matches(&semver) {
                    Some(semver)
                } else {None}
            }))
    }
}

pub struct Index {
    indexed_crates_lazy: RwLock<CratesIoIndexLookup>,
    crates_index_path: PathBuf,
    git_index: GitIndex,

    pub(crate) inter: RwLock<StringInterner<Sym, string_interner::backend::StringBackend<Sym>>>,
    pub(crate) deps_set_cache: DashMap<DepName, HashMap<Features, DepSetResult>, util::FxHasherBuilder>,
    deps_stats: std::sync::OnceLock<DepsStats>,
}

impl Index {
    pub fn new(data_dir: &Path) -> Result<Self, DepsErr> {
        let crates_index_path = data_dir.join("index");
        Ok(Self {
            git_index: GitIndex::new(data_dir)?,
            deps_set_cache: DashMap::with_capacity_and_hasher(10000, Default::default()),
            inter: RwLock::new(StringInterner::new()),
            deps_stats: std::sync::OnceLock::new(),
            indexed_crates_lazy: RwLock::default(),
            crates_index_path,
        })
    }

    fn scan_crates_index(crates_index_path: &Path, update: bool) -> Result<CratesIoIndexLookup, DepsErr> {
        debug!("Scanning crates index at {}…", crates_index_path.display());
        let start = Instant::now();
        let mut crates_io_index = crates_index::GitIndex::with_path(crates_index_path, "https://github.com/rust-lang/crates.io-index")
            .map_err(|e| DepsErr::CratesIndex(Arc::new(e)))?;
        if update {
            crates_io_index.update().map_err(|e| DepsErr::CratesIndex(Arc::new(e)))?;
        }
        let indexed_crates: HashMap<_,_> = crates_io_index.crates_parallel()
            .filter_map(|c| {
                debug_assert!(c.is_ok());
                let c = c.ok()?;
                if c.name() == "test+package" {
                    return None; // crates-io bug
                }
                let mut name = SmolStr::from(c.name());
                name.make_ascii_lowercase();
                debug_assert!(Origin::is_valid_crate_name(&name), "{name}");
                Some((name, c))
            })
            .collect();
        info!("Scanned crates index in {}…", start.elapsed().as_millis() as u32);
        if indexed_crates.len() < 120_000 {
            return Err(DepsErr::IndexBroken);
        }
        Ok(CratesIoIndexLookup { crates_by_lc: indexed_crates })
    }

    pub fn update_crate_index(&self) -> Result<(), DepsErr> {
        let new_index = blocking::watch("idx", || Self::scan_crates_index(&self.crates_index_path, true))?;
        let mut locked_index = blocking::watch("idxlock", || self.indexed_crates_lazy.try_write_for(Duration::from_secs(25)))
            .expect("can't replace crates index copy due to a weirdly long-lived lock");
        *locked_index = new_index;
        Ok(())
    }

    /// Crates available in the crates.io index
    ///
    /// It returns only a thin and mostly useless data from the index itself,
    /// so `rich_crate`/`rich_crate_version` is needed to do more.
    pub fn crates_io_crates(&self) -> Result<RwLockReadGuard<CratesIoIndexLookup>, DepsErr> {
        let mut retry = 3;
        while retry > 0 {
            retry -= 1;
            let r = self.indexed_crates_lazy.try_read_for(Duration::from_secs(15)).expect("r-deadlock in crates_io_crates");
            if !r.is_empty() {
                return Ok(r);
            } else {
                drop(r);
                let mut w = blocking::watch("cl2", || {
                    self.indexed_crates_lazy.try_write_for(Duration::from_secs(30))
                }).expect("w-deadlock in crates_io_crates");
                if w.is_empty() {
                    let new_index = blocking::watch("idxs2", || Self::scan_crates_index(&self.crates_index_path, false))
                        .map_err(|e| { error!("scan_crates_index failure! {e}"); e })?;
                    assert!(!new_index.is_empty());
                    *w = new_index;
                }
                drop(w);
            }
        }
        panic!("CratesIoIndexLookup lock is broken");
    }

    pub fn crate_exists(&self, origin: &Origin) -> bool {
        match origin {
            Origin::CratesIo(lowercase_name) => {
                self.crates_io_crates().expect("Loading index").get(lowercase_name).is_some()
            },
            Origin::GitHub { .. } | Origin::GitLab { .. } => self.git_index.has(origin),
        }
    }

    /// All crates available in the crates.io index and our index
    ///
    pub fn all_crates(&self) -> Result<Vec<Origin>, DepsErr> {
        Ok(self.git_index.crates().cloned().chain(self.crates_io_crates()?.keys().map(Origin::from_crates_io_name)).collect::<Vec<_>>())
    }

    pub fn number_of_all_crates(&self) -> Result<usize, DepsErr> {
        Ok(self.git_index.len() + self.crates_io_crates()?.len())
    }

    pub fn deps_stats(&self) -> Result<&DepsStats, DepsErr> {
        Ok(blocking::block_in_place("deps_stats", || {
            self.deps_stats.get_or_init(|| self.get_deps_stats())
        }))
    }

    /// Changes when crates-io metadata changes (something is published or yanked)
    pub fn cache_key_for_crates_io_index_state(&self, name: &str) -> Result<u64, DepsErr> {
        use std::hash::Hash;
        use std::hash::Hasher;
        let mut hasher = util::FxHasher::default();

        let l = self.crates_io_crates()?;
        for v in l.get_res(name)?.versions() {
            v.checksum().hash(&mut hasher);
            v.is_yanked().hash(&mut hasher);
        }
        Ok(hasher.finish())
    }

    /// It temporarily borrows non_default_feature_keys and returns it back
    pub(crate) fn deps_of_crate_version(&self, indexed_crates: &CratesIoIndexLookup, key: DepName, latest: &impl IVersion, non_default_feature_keys: &mut Option<Box<[SmolStr]>>, DepQuery { enable_all_targets: enable_all_features_and_targets, dev }: DepQuery) -> Result<Dep, DepsErr> {
        let semver = latest.version().try_into()
                .map_err(|e| DepsErr::SemverParsingError(format!("{}@{}: {e}", latest.name(), latest.version())))?;
        let mut features = Features {
            enable_all_targets: enable_all_features_and_targets,
            default_features: true,
            build: false,
            dev,
            non_default_feature_keys: non_default_feature_keys.take(),
        };
        let runtime = self.deps_of_ver(indexed_crates, key, latest, &features);
        let build = if let Ok(DepSetResult { has_any_relevant_direct_build_deps: true, ..}) = &runtime {
            features.build = true;
            Some(self.deps_of_ver(indexed_crates, key, latest, &features))
        } else {
            None
        };
        *non_default_feature_keys = features.non_default_feature_keys.take();
        let runtime = runtime?;
        let build = build.transpose()?;
        Ok(Dep {
            semver,
            runtime: runtime.set,
            build: build.map(|res| res.set),
        })
    }

    pub(crate) fn deps_of_ver(&self, indexed_crates: &CratesIoIndexLookup, key_id_part: DepName, ver: &impl IVersion, wants: &Features) -> Result<DepSetResult, DepsErr> {
        // dev deps are not recursive, so there's zero chance of cache hit
        let cache_lock_on_the_dep = if wants.dev { None } else { self.deps_set_cache.get_mut(&key_id_part) };
        if let Some(feature_variants) = cache_lock_on_the_dep.as_deref() {
            if let Some(res) = feature_variants.get(wants) {
                return Ok(res.clone());
            }
        }


        let ver_features = ver.features(); // available features
        let wants_feature_keys = wants.non_default_feature_keys.as_deref().unwrap_or_default();
        let mut feature_keys_to_enable = HashMap::with_capacity_and_hasher(wants_feature_keys.len() + if wants.default_features { 1 } else { 0 }, Default::default());
        let all_wanted_features = wants_feature_keys.iter().map(|s| s.as_str())
                        .chain(if wants.default_features { Some("default") } else { None });
        for feat_key in all_wanted_features {
            if let Some(actions) = ver_features.get_actions_for_feature_key(feat_key) {
                for action in actions {
                    let mut part = action.splitn(2, '/');
                    let mut dep_key = part.next().unwrap_or_default();
                    let with_feature = part.next();
                    dep_key = dep_key.strip_prefix("dep:").unwrap_or(dep_key);
                    dep_key = dep_key.strip_suffix('?').unwrap_or(dep_key);
                    let e = feature_keys_to_enable.entry(dep_key)
                        .or_insert_with(HashSet::default);
                    if let Some(with_feature) = with_feature {
                        e.insert(with_feature);
                    }
                }
            } else if feat_key != "default" {
                feature_keys_to_enable.entry(feat_key).or_insert_with(HashSet::default);
            }
        }

        let mut has_any_relevant_direct_build_deps = false;

        let deps = ver.dependencies();
        let mut all_enabled_dependencies: HashMap<&str, (bool, _, _, HashSet<&str>)> = HashMap::with_capacity_and_hasher(feature_keys_to_enable.len(), Default::default());
        let mut iter1;
        let mut iter2;
        let deps: &mut dyn Iterator<Item = _> = match deps {
            Fudge::CratesIo(dep) => {
                iter1 = dep.iter()
                .filter(|d| wants.enable_all_targets || d.target().is_none())
                .filter(|d| match d.kind() {
                    DependencyKind::Normal => true,
                    DependencyKind::Build => {
                        has_any_relevant_direct_build_deps = true;
                        wants.build
                    },
                    DependencyKind::Dev => wants.dev,
                })
                .map(|d| {
                    (d.name(), d.crate_name(), d.is_optional(), d.requirement(), d.has_default_features(), d.features())
                });
                &mut iter1
            },
            Fudge::Manifest((ref run, ref dev, ref build)) => {
                if !build.is_empty() {
                    has_any_relevant_direct_build_deps = true;
                }
                iter2 = run.iter()
                .chain(dev.iter().filter(|_| wants.dev))
                .chain(build.iter().filter(|_| wants.build))
                .filter(|r| wants.enable_all_targets || r.only_for_targets.is_empty())
                .map(|r| {
                    (&*r.user_alias, &*r.package, r.dep.optional(), r.dep.req(), r.dep.detail().map_or(true, |d| d.default_features), r.dep.req_features())
                });
                &mut iter2
            },
        };
        for (dep_key, crate_name, is_optional, requirement, with_default_features, dep_req_own_features) in deps {
            // people forget to include winapi conditionally
            if !wants.enable_all_targets && crate_name == "winapi" {
                continue; // FIXME: allow common targets?
            }

            let features_for_dep = feature_keys_to_enable.get_mut(dep_key);
            if is_optional && features_for_dep.is_none() {
                continue;
            }

            let (_, _, _, all_dep_req_features) = all_enabled_dependencies.entry(dep_key)
                .and_modify(|(default_features, ..) | {
                    *default_features |= with_default_features;
                })
                .or_insert_with(|| (with_default_features, crate_name, requirement, HashSet::default()));
            all_dep_req_features.extend(dep_req_own_features.iter().map(|s| s.as_str()));
            if let Some(enable_dep_features) = features_for_dep {
                all_dep_req_features.extend(enable_dep_features.iter().copied());
            }
        }
        drop(feature_keys_to_enable);

        if !wants.dev {
            // break infinite recursion. Must be inserted first, since depth-first search
            // may end up requesting it.
            let mut by_feature_set = match cache_lock_on_the_dep {
                Some(already_locked) => already_locked,
                None => self.deps_set_cache.entry(key_id_part).or_insert_with(Default::default),
            };
            by_feature_set.entry(wants.clone()).or_insert_with(|| DepSetResult {
                has_any_relevant_direct_build_deps,
                set: Arc::default(),
            });
            drop(by_feature_set); // unlock before recursing
        } else {
            drop(cache_lock_on_the_dep);
        }

        let set: Result<_,_> = all_enabled_dependencies.into_values().map(|(mut default_features, crate_name, requirement, mut all_features)| {
            let crate_name_lc = &*crate_name.as_ascii_lowercase();
            let krate = indexed_crates.get(crate_name_lc).ok_or_else(|| {
                DepsErr::Crates(format!("{}@{} depends on missing crate {crate_name_lc} (@{requirement})", ver.name(), ver.version()))
            })?;
            let req = VersionReq::parse(requirement)
                .unwrap_or_else(|e| { warn!("{crate_name} = {requirement}: {e}"); VersionReq::STAR });

            let matched_version = krate.versions().iter().rev()
                .filter(|v| !v.is_yanked())
                .find_map(|v| {
                    let semver = SemVer::parse(v.version()).ok()?;
                    if req.matches(&semver) { Some(v) } else { None }
                })
                .unwrap_or_else(|| {
                    // bad version, but it shouldn't happen anyway
                    krate.most_recent_version()
                });

            // Minimize the set of necessary non_default_feature_keys
            if !all_features.is_empty() {
                if all_features.remove("default") {
                    default_features = true;
                }
                if default_features {
                    if let Some(crates_default_features) = matched_version.features().get("default") {
                        crates_default_features.iter().for_each(|f| {
                            let k = f.split('/').next().unwrap_or_default();
                            all_features.remove(k);
                        })
                    }
                }
            }

            let non_default_feature_keys = if all_features.is_empty() { drop(all_features); None } else {
                Some(all_features.into_iter().map(Into::into).collect::<Vec<_>>().into_boxed_slice())
            };

            let semver = MiniVer::try_from(matched_version.version()).map_err(|e| DepsErr::SemverParsingError(e.to_string()))?;
            let deps_external_cache_key = {
                let mut inter = self.inter.write();
                (inter.get_or_intern(crate_name_lc), inter.get_or_intern(matched_version.version()))
            };

            let mut wants_features = Features {
                enable_all_targets: wants.enable_all_targets,
                build: false,
                dev: false, // dev is only for top-level
                default_features,
                non_default_feature_keys,
            };
            let runtime = self.deps_of_ver(indexed_crates, deps_external_cache_key, matched_version, &wants_features)?;
            let build = if runtime.has_any_relevant_direct_build_deps {
                wants_features.build = true;
                let build = self.deps_of_ver(indexed_crates, deps_external_cache_key, matched_version, &wants_features)?;
                Some(build.set)
            } else {
                None
            };
            Ok((deps_external_cache_key, Dep {
                semver,
                runtime: runtime.set,
                build,
            }))
        }).collect();

        let result = DepSetResult {
            set: Arc::new(set?),
            has_any_relevant_direct_build_deps,
        };
        if !wants.dev {
            *self.deps_set_cache.get_mut(&key_id_part).ok_or(DepsErr::IndexBroken)?
                .get_mut(wants).ok_or(DepsErr::IndexBroken)? = result.clone();
        }

        Ok(result)
    }

    pub fn clear_cache(&self) {
        let mut interner = self.inter.try_write_for(Duration::from_secs(30)).expect("interner deadlock");
        self.deps_set_cache.clear();
        self.deps_set_cache.shrink_to_fit();
        *interner = StringInterner::new();
    }
}

use std::fmt;
impl fmt::Debug for Dep {
    #[cold]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Dep {{ {}, runtime: x{}, build: x{} }}", self.semver, self.runtime.len(), self.build.as_ref().map_or(0, |b| b.len()))
    }
}

impl From<SemVer> for MiniVer {
    fn from(s: SemVer) -> Self {
        Self::try_from(s.to_string().as_str()).unwrap_or_default()
    }
}

impl TryFrom<&str> for MiniVer {
    type Error = SemVerError;
    fn try_from(s: &str) -> Result<Self, Self::Error> {
        let s: old_semver::Version = s.parse()?;
        Ok(Self {
            major: s.major as u16,
            minor: s.minor as u16,
            patch: s.patch as u16,
            pre: s.pre.into_boxed_slice(),
            build: if let Some(old_semver::Identifier::Numeric(m)) = s.build.get(0) { *m as u16 } else { 0 },
        })
    }
}

impl fmt::Display for MiniVer {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}.{}.{}-{}", self.major, self.minor, self.patch, self.build)
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Features {
    pub enable_all_targets: bool,
    pub default_features: bool,
    pub build: bool,
    pub dev: bool,
    pub non_default_feature_keys: Option<Box<[SmolStr]>>,
}

pub type DepName = (Sym, Sym);
pub type DepSet = util::FxHashMap<DepName, Dep>;
pub type ArcDepSet = Arc<DepSet>;

#[derive(Clone)]
pub(crate) struct DepSetResult {
    has_any_relevant_direct_build_deps: bool,
    set: Arc<DepSet>,
}

pub struct Dep {
    pub semver: MiniVer,
    pub runtime: ArcDepSet,
    pub build: Option<ArcDepSet>,
}

#[derive(Debug, Copy, Clone)]
pub struct DepQuery {
    // default features always included
    pub enable_all_targets: bool,
    pub dev: bool,
}
