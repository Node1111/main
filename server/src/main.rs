use blocking::block_in_place;
use actix_web::body::BoxBody;
use actix_web::dev::Url;
use actix_web::guard::fn_guard;
use actix_web::http::header::HeaderValue;
use actix_web::http::header;
use actix_web::http::StatusCode;
use actix_web::HttpResponse;
use actix_web::middleware;
use actix_web::{web, App, HttpRequest, HttpServer};
use ahash::HashMap;
use ahash::HashMapExt;
use anyhow::{anyhow, Context};
use arc_swap::ArcSwap;
use cap::Cap;
use categories::CATEGORIES;
use categories::Category;
use chrono::prelude::*;
use tracing::Instrument;
use crate::writer::writer;
use futures::future::Future;
use futures::future::FutureExt;
use futures::TryFutureExt;
use kitchen_sink::ArcRichCrateVersion;
use kitchen_sink::filter::ImageOptimAPIFilter;
use kitchen_sink::KitchenSink;
use kitchen_sink::Origin;
use kitchen_sink::RendCtx;
use kitchen_sink::RichCrate;
use kitchen_sink::running;
use kitchen_sink::SpawnAbortOnDrop;
use kitchen_sink::stop;
use kitchen_sink::stopped;
use locale::Numeric;
use log::{debug, info, warn, error};
use once_cell::sync::Lazy;
use render_readme::{Highlighter, Markup, Renderer};
use repo_url::SimpleRepo;
use search_index::CrateSearchIndex;
use std::convert::TryInto;
use std::env;
use std::path::Path;
use std::path::PathBuf;
use std::pin::Pin;
use std::process::ExitCode;
use std::sync::Arc;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::{AtomicU32, Ordering};
use std::time::{Duration, Instant, SystemTime};
use tokio::runtime::Handle;
use tokio::time::timeout;
use urlencoding::decode;
use urlencoding::Encoded;
use util::CowAscii;

mod writer;

const MAX_MEM: usize = 7 * 1024 * 1024 * 1024;

#[cfg(not(feature = "dhat-heap"))]
use std::alloc::System as GlobalAlloc;

#[cfg(feature = "dhat-heap")]
use dhat::Alloc as GlobalAlloc;

#[global_allocator]
static ALLOCATOR: Cap<GlobalAlloc> = Cap::new(GlobalAlloc, MAX_MEM);

static HUP_SIGNAL: AtomicU32 = AtomicU32::new(0);

/// Makes sure that the same page isn't rendered multiple times in parallel
static PAGES_RENDER: Lazy<std::sync::Mutex<HashMap<Box<str>, Arc<tokio::sync::Mutex<()>>>>> = Lazy::new(|| std::sync::Mutex::new(HashMap::new()));

struct ServerState {
    markup: Renderer,
    index: CrateSearchIndex,
    crates: ArcSwap<KitchenSink>,
    page_cache_dir: PathBuf,
    data_dir: PathBuf,
    rt: Handle,
    background_job: tokio::sync::Semaphore,
    foreground_job: tokio::sync::Semaphore,
    start_time: Instant,
    last_ok_response: AtomicU32,
}

type AServerState = web::Data<ServerState>;

fn main() -> ExitCode {
    #[cfg(feature = "dhat-heap")]
    let _profiler = dhat::Profiler::builder().trim_backtraces(Some(15)).build();

    // let mut b = env_logger::Builder::from_default_env();
    // b.filter_module("html5ever", log::LevelFilter::Error);
    // b.filter_module("tokei", log::LevelFilter::Error);
    // b.filter_module("hyper", log::LevelFilter::Warn);
    // b.filter_module("tantivy", log::LevelFilter::Error);
    // if cfg!(debug_assertions) {
    //     b.filter_level(log::LevelFilter::Debug);
    // }
    // let _ = b.try_init().map_err(|e| eprintln!("log: {e}"));

    // console_subscriber::init();

    let sys = actix_web::rt::System::new();

    let rt = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .thread_name("server")
        .build()
        .unwrap();

    let img_rt = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .thread_name("images")
        .build()
        .unwrap();

    let res = sys.block_on(run_server(rt.handle().clone(), img_rt.handle().clone()));

    kitchen_sink::stop();
    rt.shutdown_timeout(Duration::from_secs(2));

    if let Err(e) = res {
        for c in e.chain() {
            error!("Sever Error: {}", c);
        }
        return ExitCode::FAILURE;
    }
    ExitCode::SUCCESS
}

const FOREGROUND_JOB_MAX_PERMITS: usize = 32;

async fn run_server(rt: Handle, img_rt: Handle) -> Result<(), anyhow::Error> {
    unsafe { signal_hook::low_level::register(signal_hook::consts::SIGHUP, || HUP_SIGNAL.store(1, Ordering::SeqCst)) }?;
    unsafe { signal_hook::low_level::register(signal_hook::consts::SIGUSR1, || HUP_SIGNAL.store(1, Ordering::SeqCst)) }?;

    kitchen_sink::dont_hijack_ctrlc();

    let dev_root_path = Path::new(if Path::new("data").exists() { "" } else { ".." });

    let data_dir = env::var_os("CRATE_DATA_DIR").map(PathBuf::from).unwrap_or_else(move || dev_root_path.join("data"));
    let public_document_root = env::var_os("DOCUMENT_ROOT").map(PathBuf::from).unwrap_or_else(move || dev_root_path.join("style/public"));
    let page_cache_dir: PathBuf = "/var/tmp/crates-server".into();
    let github_token = env::var("GITHUB_TOKEN").context("GITHUB_TOKEN missing")?;

    let _ = std::fs::create_dir_all(&page_cache_dir);
    assert!(page_cache_dir.exists(), "{} does not exist", page_cache_dir.display());
    assert!(public_document_root.exists(), "DOCUMENT_ROOT {} does not exist", public_document_root.display());
    assert!(data_dir.exists(), "CRATE_DATA_DIR {} does not exist", data_dir.display());


    let crates = SpawnAbortOnDrop(rt.spawn({
        let data_dir = data_dir.clone();
        let github_token = github_token.clone();
        async move {
            KitchenSink::new(&data_dir, &github_token).await
        }
    })).await??;
    let image_filter = Arc::new(ImageOptimAPIFilter::new("czjpqfbdkz", crates.main_data_dir().join("images.db"), img_rt).await?);
    let markup = Renderer::new_filter(Some(Highlighter::new()), image_filter);

    let index = CrateSearchIndex::new(&data_dir)?;

    let state = web::Data::new(ServerState {
        markup,
        index,
        crates: ArcSwap::from_pointee(crates),
        page_cache_dir,
        data_dir: data_dir.clone(),
        rt,
        background_job: tokio::sync::Semaphore::new(5),
        foreground_job: tokio::sync::Semaphore::new(FOREGROUND_JOB_MAX_PERMITS),
        start_time: Instant::now(),
        last_ok_response: AtomicU32::new(0),
    });

    let timestamp = Arc::new(AtomicU32::new(0));
    let enabled = Arc::new(AtomicBool::new(true));

    // event observer
    state.rt.spawn({
        let state = state.clone(); // not Arc
        async move {
            loop {
                // must reload subscriber to avoid holding a reference to the old db after crates reload
                let mut subscriber = state.crates.load().event_log().subscribe("server observer").unwrap();
                for _ in 0..300 {
                    use kitchen_sink::SharedEvent::*;
                    let Ok(batch) = subscriber.next_batch().await else { break; };
                    debug!("Got events from the log {:?}", batch);
                    for ev in batch.filter_map(|e| e.ok()) {
                        match ev {
                            CrateUpdated(origin_str) => {
                                info!("New crate updated {}", origin_str);
                                let o = Origin::from_str(&origin_str);
                                let cache_path = state.page_cache_dir.join(cache_file_name_for_origin(&o));
                                let _ = std::fs::remove_file(cache_path);
                            },
                            CrateIndexed(origin_str) => {
                                info!("Purging local cache {}", origin_str);
                                let o = Origin::from_str(&origin_str);
                                state.crates.load().reload_indexed_crate(&o);
                                let cache_path = state.page_cache_dir.join(cache_file_name_for_origin(&o));
                                let _ = std::fs::remove_file(&cache_path);
                                background_refresh(state.clone(), cache_path, Box::pin(render_crate_page(state.clone(), o, 600)));
                            },
                            CrateNeedsReindexing(origin_str) => {
                                info!("Heard about outdated crate {}", origin_str);
                                let o = Origin::from_str(&origin_str);
                                let s2 = state.clone();
                                let _runs_in_bg = rt_run_timeout_bg(&state.rt, "bg reload", 300, async move {
                                    let _b = s2.background_job.acquire().await;
                                    // this kicks off indexing if not cached
                                    let _ = s2.crates.load().rich_crate_version_async(&o).await;
                                    Ok(())
                                });
                            },
                            DailyStatsUpdated => {
                                let _ = std::fs::remove_file(state.page_cache_dir.join("_stats_.html"));
                            },
                        }
                    }
                    tokio::time::sleep(Duration::from_secs(1)).await;
                }
            }
        }.instrument(tracing::info_span!("events"))
    });

    // refresher thread
    let watchdog = state.rt.spawn({
        let state = Arc::clone(&state);
        let timestamp = timestamp.clone();
        let enabled = enabled.clone();
        async move {
            let mut last_full_reload = Instant::now();
            let mut last_quick_update = Instant::now();
            state.rt.spawn_blocking({
                let state = state.clone();
                blocking::closure("prewarm", move || {
                    state.crates.load().prewarm();
                })
            }).await.expect("prewarm");
            info!("prewarmed initial data, starting async watchdog");
            let mut latency_failures = 0;
            loop {
                if !enabled.load(Ordering::SeqCst) {
                    info!("watchdog task end");
                    return;
                }
                tokio::time::sleep(Duration::from_secs(2)).await;

                // test that runtime hasn't deadlocked
                let ls_start = Instant::now();
                tokio::task::spawn(async {}).await.unwrap();
                tokio::task::spawn_blocking(|| {}).await.unwrap();
                let latency = ls_start.elapsed();
                if latency > Duration::from_millis(500) {
                    latency_failures += 1;
                    warn!("NOP latency is {}ms; sem free: {}bg {}fg; {}MB mem used; {latency_failures} failed in a row",
                        latency.as_millis(),
                        state.background_job.available_permits(),
                        state.foreground_job.available_permits(),
                        ALLOCATOR.allocated()/1_000_000);
                    if latency_failures == 2 {
                        eprintln!("Flushing due to async latency");
                        blocking::dump_all();
                        blocking::block_in_place("cleanupf", || state.crates.load().cleanup());
                    } else if latency_failures > 4 {
                        eprintln!("Aborting due to async latency");
                        blocking::dump_all();
                        std::process::abort();
                    }
                } else {
                    latency_failures = 0;
                }

                timestamp.store(state.start_time.elapsed().as_secs() as u32, Ordering::SeqCst);
                let should_do_full_reload = if 1 == HUP_SIGNAL.swap(0, Ordering::SeqCst) {
                    info!("HUP!");
                    true
                } else if last_full_reload.elapsed() > Duration::from_secs(3 * 3600) {
                    info!("Reloading state on a timer (3h)");
                    true
                } else {
                    false
                };
                if should_do_full_reload {
                    // reload will take some time, this pauses watchdog for a bit
                    timestamp.store(state.start_time.elapsed().as_secs() as u32 + 60, Ordering::SeqCst);
                    match blocking::awatch("loading newksink", KitchenSink::new(&data_dir, &github_token)).await {
                        Ok(k) => {
                            let mem_before = ALLOCATOR.allocated() as isize;
                            info!("Reloading state");
                            timestamp.store(state.start_time.elapsed().as_secs() as u32 + 60, Ordering::SeqCst);

                            blocking::block_in_place("upd_repos", || {
                                let old_crates = state.crates.load();
                                old_crates.update_repositories(); // runs git update now, so that next instance will load fresh repos
                                old_crates.cleanup();
                            });
                            let k = Arc::new(k);
                            let preload_result = state.rt.spawn_blocking({
                                let k = k.clone();
                                blocking::closure("prewarm2", move || {
                                    k.prewarm();
                                    info!("prewarmed");
                                })
                            }).await;
                            if let Err(e) = preload_result {
                                error!("Reload failed: {e}");
                                std::process::exit(5);
                            }
                            last_full_reload = Instant::now();
                            let old = state.crates.swap(k);
                            info!("Swapped state; old has {} ref", Arc::strong_count(&old));
                            drop(old);
                            let mem_after = ALLOCATOR.allocated() as isize;
                            info!("Reloaded state; added {}KB mem", mem_after - mem_before);
                        },
                        Err(e) => {
                            error!("Refresh failed: {e}");
                            std::process::exit(1);
                        },
                    }
                } else if last_quick_update.elapsed() > Duration::from_secs(15 * 60) {
                    info!("Periodic index update");
                    timestamp.store(state.start_time.elapsed().as_secs() as u32 + 15, Ordering::SeqCst);
                    blocking::watch("updindx", || state.crates.load().update_crate_index());
                    last_quick_update = Instant::now();
                }

                if ALLOCATOR.allocated() > MAX_MEM * 8 / 10 {
                    warn!("High memory usage {}MB; flushing caches", ALLOCATOR.allocated()/1_000_000);
                    timestamp.store(state.start_time.elapsed().as_secs() as u32 + 15, Ordering::SeqCst);
                    blocking::block_in_place("mem-clean", || state.crates.load().cleanup());
                    if ALLOCATOR.allocated() > MAX_MEM * 8 / 10 {
                        blocking::dump_all();
                        error!("Flushing caches didn't help: {}MB used", ALLOCATOR.allocated()/1_000_000);
                        std::process::exit(8);
                    }
                }
            }
        }.instrument(tracing::info_span!("watchdog"))
    });

    // watchdog
    std::thread::spawn({
        let state = Arc::clone(&state);
        let enabled = enabled.clone();
        move || {
        std::thread::sleep(Duration::from_secs(60)); // give startup some time
        loop {
            if !enabled.load(Ordering::SeqCst) {
                info!("watchdog thread end");
                return;
            }
            std::thread::sleep(Duration::from_secs(2));
            let expected = state.start_time.elapsed().as_secs() as u32;
            let rt_timestamp = timestamp.load(Ordering::SeqCst);
            if rt_timestamp + 8 < expected {

                if rt_timestamp + 16 == expected {
                    blocking::dump_all();
                }

                warn!("Update loop is {}s behind; sem free: {}bg {}fg; {}MB mem used", expected - rt_timestamp,
                    state.background_job.available_permits(),
                    state.foreground_job.available_permits(),
                    ALLOCATOR.allocated()/1_000_000);
                if rt_timestamp + 45 < expected {
                    error!("tokio is dead");
                    blocking::dump_all();
                    kitchen_sink::stop();
                    std::thread::sleep(Duration::from_secs(2));
                    std::process::exit(4);
                }
            }
            let response_timestamp = state.last_ok_response.load(Ordering::SeqCst);
            if response_timestamp + 60*2 < expected {
                warn!("no requests for 2 minutes? probably a deadlock");
                // don't exit in debug mode, because it's legitimately idling
                if !cfg!(debug_assertions) {
                    blocking::dump_all();
                    kitchen_sink::stop();
                    std::thread::sleep(Duration::from_secs(1));
                    std::process::exit(2);
                }
            }
        }
    }});

    let server = HttpServer::new({
        let state = state.clone(); // not arc!
        move || {
        App::new()
            .app_data(state.clone())
            .wrap(middleware::Compress::default())
            .wrap(middleware::DefaultHeaders::new().add(("x-powered-by", HeaderValue::from_static(concat!("actix-web lib.rs/", env!("CARGO_PKG_VERSION"))))))
            .wrap(middleware::Logger::default())
            .route("/{any:.*}", web::route()
                .guard(actix_web::guard::Header("host", "libs.rs"))
                .guard(fn_guard(|ctx| !ctx.head().uri.path().contains('.'))) // css and png
                .to(handle_libs_redir))
            .route("/{any:.*}", web::route()
                .guard(actix_web::guard::Header("host", "crates.rs"))
                .guard(fn_guard(|ctx| !ctx.head().uri.path().contains('.')))
                .to(handle_crates_rs_redir))
            .route("/", web::get().to(handle_home))
            .route("/search", web::get().to(handle_search))
            .route("/game-engines", web::get().to(handle_game_redirect))
            .route("//", web::get().to(handle_search)) // odd dir listing?
            .route("/index", web::get().to(handle_search)) // old crates.rs/index url
            .route("/categories/{rest:.*}", web::get().to(handle_redirect))
            .route("/new", web::get().to(handle_new_trending))
            .route("/stats", web::get().to(handle_global_stats))
            .route("/keywords/{keyword}", web::get().to(handle_keyword))
            .route("/crates/{crate}", web::get().to(handle_crate))
            .route("/crates/{crate}/features", web::get().to(handle_crate_features))
            .route("/crates/{crate}/versions", web::get().to(handle_crate_all_versions))
            .route("/crates/{crate}/source", web::get().to(handle_crate_source_redirect))
            .route("/crates/{crate}/rev", web::get().to(handle_crate_reverse_dependencies))
            .route("/api/crates/{crate}/downloads.json", web::get().to(handle_crate_api_downloads))
            .route("/crates/{crate}/reverse_dependencies", web::get().to(handle_crate_reverse_dependencies_redir))
            .route("/crates/{crate}/crev", web::get().to(handle_crate_reviews_redirect))
            .route("/crates/{crate}/audit", web::get().to(handle_crate_reviews))
            .route("/~{author}", web::get().to(handle_author))
            .route("/~{author}/dash", web::get().to(handle_maintainer_dashboard_html))
            .route("/~{author}/dash.xml", web::get().to(handle_maintainer_dashboard_xml))
            .route("/~", web::get().to(handle_maintainer_form))
            .route("/dash", web::get().to(handle_maintainer_form))
            .route("/users/{author}", web::get().to(handle_author_redirect))
            .route("/install/{crate:.*}", web::get().to(handle_install))
            .route("/compat/{crate:.*}", web::get().to(handle_compat))
            .route("/debug/{crate:.*}", web::get().to(handle_debug))
            .route("/_status", web::get().to(handle_status))
            .route("/{host}/{owner}/{repo}/{crate}", web::get().to(handle_repo_crate))
            .route("/{host}/{owner}/{repo}/{crate}/versions", web::get().to(handle_repo_crate_all_versions))
            .route("/atom.xml", web::get().to(handle_feed))
            .route("/sitemap.xml", web::get().to(handle_sitemap))
            .route("/{crate}/info/refs", web::get().to(handle_git_clone))
            .route("/crates/{crate}/info/refs", web::get().to(handle_git_clone))
            .service(actix_files::Files::new("/", &public_document_root))
            .default_service(web::route().to(default_handler))
    }})
    .bind("127.0.0.1:32531")
    .expect("Can not bind to 127.0.0.1:32531")
    .shutdown_timeout(1);

    // handler installed late after actix hopefully is ready for it
    let _stopme = SpawnAbortOnDrop(state.rt.spawn(async {
        let _ = actix_web::rt::signal::ctrl_c().await;
        info!("Actix got ctrl-c");
        tokio::time::sleep(Duration::from_millis(100)).await;
        stop();
    }));

    info!("Starting HTTP server {} on http://127.0.0.1:32531", env!("CARGO_PKG_VERSION"));
    server.run().await?;

    info!("bye!");

    enabled.store(false, Ordering::SeqCst);
    watchdog.abort();

    Ok(())
}

fn mark_server_still_alive(state: &ServerState) {
    let elapsed = state.start_time.elapsed().as_secs() as u32;
    state.last_ok_response.store(elapsed, Ordering::SeqCst);
}

fn find_category<'a>(slugs: impl Iterator<Item = &'a str>) -> Option<&'static Category> {
    let mut found = None;
    let mut current_sub = &CATEGORIES.root;
    for slug in slugs {
        if let Some(cat) = current_sub.get(slug) {
            found = Some(cat);
            current_sub = &cat.sub;
        } else {
            return None;
        }
    }
    found
}

#[tracing::instrument(level = "debug", skip(state))]
fn handle_static_page(state: &ServerState, path: &str) -> Result<Option<HttpResponse>, ServerError> {
    if !is_alnum(path) {
        return Ok(None);
    }

    let md_path = state.data_dir.as_path().join(format!("page/{path}.md"));
    if !md_path.exists() {
        return Ok(None);
    }

    let mut chars = path.chars();
    let path_capitalized = chars.next().into_iter().flat_map(char::to_uppercase).chain(chars)
        .map(|c| if c == '-' {' '} else {c}).collect();
    let crates = state.crates.load();
    let index = crates.index().map_err(anyhow::Error::from)?;
    let crate_num = index.crates_io_crates().map_err(|e| anyhow!("Ooops: {e}"))?.len();
    let total_crate_num = index.number_of_all_crates().map_err(|e| anyhow!("Ooops: {e}"))?;

    let md = std::fs::read_to_string(md_path).context("reading static page")?
        .replace("$CRATE_NUM", &Numeric::english().format_int(crate_num))
        .replace("$TOTAL_CRATE_NUM", &Numeric::english().format_int(total_crate_num));
    let mut page = Vec::with_capacity(md.len() * 2);
    front_end::render_static_page(&mut page, path_capitalized, &Markup::Markdown(md), &state.markup)?;
    minify_html(&mut page);

    mark_server_still_alive(state);
    Ok(Some(HttpResponse::Ok()
        .content_type("text/html;charset=UTF-8")
        .insert_header((header::CACHE_CONTROL, "public, max-age=7200, stale-while-revalidate=604800, stale-if-error=86400"))
        .no_chunking(page.len() as u64)
        .body(page)))
}

async fn handle_libs_redir(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let path = req.path();
    if path.as_bytes().iter().any(|&b| b == b'<' || b == b'"' || b == b':' || b == b'>' || b == b'\'' || b.is_ascii_whitespace()) {
        return Ok(HttpResponse::BadRequest().body(vec![]));
    }

    info!("libs.rs from {}", req.headers().get(header::REFERER).and_then(|h| h.to_str().ok()).unwrap_or("no-referrer"));
    let mut page = Vec::with_capacity(5000);
    front_end::render_static_trusted_html(&mut page, "Wrong domain".into(), format!(r##"
        <h1>The domain is <b>lib.rs</b>, singular</h1>
        <p>You've navigated to lib<mark>s</mark>.rs, but the site uses <b>lib.rs</b> domain (3-letter name, like src/lib.rs).</p>
        <p>Please update your links/bookmarks/muscle memory, because the longer alias domain may stop working.</p>
        <p><a rel="canonical" href="https://lib.rs{path}">&rarr; lib.rs</a><p>
    "##))?;

    Ok(HttpResponse::Ok()
        .content_type("text/html;charset=UTF-8")
        .insert_header((header::CACHE_CONTROL, "public, max-age=604800"))
        .insert_header((header::LINK, format!("<https://lib.rs{path}>; rel=\"canonical\"")))
        .insert_header((header::CONTENT_LOCATION, format!("https://lib.rs{path}")))
        .insert_header((header::REFRESH, format!("60;url=https://lib.rs{path}")))
        .no_chunking(page.len() as u64)
        .body(page))
}

async fn handle_crates_rs_redir(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let path = req.path();
    if path.as_bytes().iter().any(|&b| b == b'<' || b == b'"' || b == b':' || b == b'>' || b == b'\'' || b.is_ascii_whitespace()) {
        return Ok(HttpResponse::BadRequest().body(vec![]));
    }

    info!("crates.rs from {}", req.headers().get(header::REFERER).and_then(|h| h.to_str().ok()).unwrap_or("no-referrer"));
    let mut page = Vec::with_capacity(5000);
    front_end::render_static_trusted_html(&mut page, "Unofficial".into(), format!(r##"
        <h1>crates.<em>rs</em> is unofficial</h1>
        <p>The official crate repository is <a rel="alternate" href="https://crates.io{path}">crates.<b>io</b></a>.</p>
        <p>crates.<b>rs</b> is a former domain name of <a rel="canonical" href="https://lib.rs{path}">the lib.rs website</a>.</p>
        <p>Please update your links/bookmarks/habits, because the crates.rs domain will be phased out.</p>
    "##))?;

    Ok(HttpResponse::Ok()
        .content_type("text/html;charset=UTF-8")
        .insert_header((header::CACHE_CONTROL, "public, max-age=60"))
        .insert_header((header::LINK, format!("<https://lib.rs{path}>; rel=\"canonical\"")))
        .insert_header((header::CONTENT_LOCATION, format!("https://lib.rs{path}")))
        .insert_header((header::REFRESH, format!("10;url=https://lib.rs{path}")))
        .no_chunking(page.len() as u64)
        .body(page))
}

#[tracing::instrument(level = "debug", skip_all)]
async fn handle_maintainer_form(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let query = req.query_string().trim_start_matches('?');
    if let Some(u) = query.strip_prefix("username=") {
        return Ok(HttpResponse::PermanentRedirect().insert_header((header::LOCATION, format!("/~{u}/dash"))).finish());
    }

    let mut page = Vec::with_capacity(5000);
    front_end::render_static_trusted_html(&mut page, "Maintainer dashboard".into(), r#"
        <h1>Hello Rustaceans</h1>
        <p>Have you published any Rust crates? Find your profile page and dashboard on lib.rs. The dashboard lets you validate your crates' metadata and dependencies.</p>
        <form>
        <p><label>Your <strong>GitHub username</strong>: <input type="text" name="username"></label><p>
        <p><button type="submit">Go to the Dashboard</button></p>
        </form>
    "#.into())?;

    Ok(HttpResponse::Ok()
        .content_type("text/html;charset=UTF-8")
        .insert_header((header::CACHE_CONTROL, "public, max-age=604800"))
        .no_chunking(page.len() as u64)
        .body(page))
}

#[tracing::instrument]
async fn default_handler(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    if stopped() {
        return Ok(HttpResponse::ServiceUnavailable()
            .insert_header((header::REFRESH, "2"))
            .insert_header((header::CACHE_CONTROL, "must-revalidate"))
            .content_type("text/plain;charset=UTF-8")
            .body("ERROR: lib.rs server is restarting, please try again in a second"));
    }

    let state: &AServerState = req.app_data().expect("appdata");
    let path = req.uri().path();
    if let Some(path_trimmed) = path.strip_suffix('/').or_else(|| if path.starts_with("//") { path.strip_prefix('/') } else { None }) {
        return Ok(HttpResponse::PermanentRedirect().insert_header((header::LOCATION, path_trimmed.trim_end_matches('/'))).finish());
    }

    let mut path = path.strip_prefix('/').unwrap_or(path);

    if let Some(cat) = find_category(path.split('/')) {
        return Box::pin(handle_category(req, cat)).await;
    }

    if let Some(page) = handle_static_page(state, path)? {
        return Ok(page);
    }

    let subpage_suffix = path.rsplit_once('/').and_then(|(rest, suffix)| {
        if matches!(suffix, "features" | "versions" | "source" | "rev" | "reverse_dependencies" | "crev" | "audit") {
            path = rest;
            Some(suffix)
        } else {
            None
        }
    });

    let name = path.trim_matches('/').to_owned();
    let crates = state.crates.load();

    let (found_crate, found_keyword, found_author) = rt_run_timeout(&state.rt, "findcrate", 10, async move {
        let mut crate_maybe = None;
        for name in name.split(['/', ':', '?', '.']).filter(|n| !n.is_empty()) {
            if let Some(o) = Origin::try_from_crates_io_name(name) {
                crate_maybe = Box::pin(crates.rich_crate_async(&o)).await.ok();
                if crate_maybe.is_some() {
                    break;
                }
            }
        }
        let inverted_hyphens = invert_hyphens(&name);
        if crate_maybe.is_none() {
            if let Some(o) = Origin::try_from_crates_io_name(&inverted_hyphens) {
                crate_maybe = Box::pin(crates.rich_crate_async(&o)).await.ok();
            }
        }
        if let Some(k) = crate_maybe {
            // lib.rs/git isn't a real crate, so it's better to search for git instead of showing the crate
            let is_good = crates.is_crate_good_enough_for_short_url(k.origin()).await.unwrap_or(true); // new not-indexed-yet won't be found
            return Ok((Some((k, is_good)), None, None))
        }

        if crates.is_it_a_keyword(&inverted_hyphens).await {
            return Ok((None, Some(inverted_hyphens), None))
        }

        if let Ok(author) = crates.author_by_login(&name).await {
            if let Ok(rows) = crates.all_crates_of_author(&author).await {
                return Ok((None, None, Some(author.github.login).filter(|_| !rows.is_empty())))
            }
        }
        Ok((None, None, None))
    }).await?;

    if let Some((k, is_good)) = found_crate {
        let show_crate = if is_good { true } else {
            let name = k.name().to_owned();
            let state = state.clone();
            tokio::task::spawn_blocking(blocking::closure("defsearch", move || {
                // if search just find this crate (+ its -derive or -core or such), there's no point showing the search
                state.index.search(&name, 4, false).unwrap_or_default().crates.len() <= 2
            })).await.unwrap()
        };
        return Ok(if show_crate {
            let url = format!("/crates/{}{}{}", Encoded(k.name()), if subpage_suffix.is_some() {"/"} else {""}, subpage_suffix.unwrap_or_default());
            HttpResponse::PermanentRedirect().insert_header((header::LOCATION, url)).finish()
        } else {
            HttpResponse::TemporaryRedirect().insert_header((header::LOCATION, format!("/search?q={}", Encoded(k.name())))).finish()
        })
    }
    if let Some(keyword) = found_keyword {
        return Ok(HttpResponse::TemporaryRedirect().insert_header((header::LOCATION, format!("/keywords/{}", Encoded(&keyword)))).finish());
    }
    if let Some(author) = found_author {
        return Ok(HttpResponse::TemporaryRedirect().insert_header((header::LOCATION, format!("/~{}", Encoded(&author)))).finish());
    }

    render_404_page(state, path, "crate or category").await
}

fn invert_hyphens(name: &str) -> String {
    name.chars().map(|c| if c == '-' {'_'} else if c == '_' {'-'} else {c.to_ascii_lowercase()}).collect()
}

fn render_404_page(state: &AServerState, path: &str, item_name: &str) -> Pin<Box<dyn Future<Output = Result<HttpResponse, ServerError>>>> {
    let item_name = item_name.to_owned();
    let decoded = decode(path).ok();
    let rawtext = decoded.as_deref().unwrap_or(path);

    let query = rawtext.chars().map(|c| if c.is_alphanumeric() { c } else { ' ' }).take(100).collect::<String>();
    let query = query.trim().to_owned();
    let state = state.clone();

    Box::pin(tokio::task::spawn_blocking(blocking::closure("404", move || {
        let results = state.index.search(&query, 7, true).unwrap_or_default();
        let mut page: Vec<u8> = Vec::with_capacity(32000);
        front_end::render_404_page(&mut page, &query, &item_name, &results.crates, &state.markup)?;
        Ok(page)
    })).map(|res| {
        res?.map(|page| {
            HttpResponse::NotFound()
                .content_type("text/html;charset=UTF-8")
                .no_chunking(page.len() as u64)
                .insert_header((header::CACHE_CONTROL, "public, max-age=60, stale-while-revalidate=3600, stale-if-error=3600"))
                .body(page)
        })
    }))
}

fn is_bot(req: &HttpRequest) -> bool {
    req.headers().get(header::USER_AGENT)
    .and_then(|ua| ua.to_str().ok())
    .map_or(true, |ua| ua.contains("+http") || ua.bytes().any(|c| c == b'@') || ua.contains("https://") || ua.contains("Headless") || ua.contains("feed/"))
}

#[tracing::instrument(level = "debug", skip_all)]
async fn handle_category(req: HttpRequest, cat: &'static Category) -> Result<HttpResponse, ServerError> {
    let state: &AServerState = req.app_data().expect("appdata");
    let crates = state.crates.load();
    let stale = is_bot(&req);
    let mut cache_time = 1800;
    Ok(serve_page(
        with_file_cache(state, &format!("_{}.html", cat.slug), cache_time, stale, {
            let state = state.clone();
            rt_run_timeout(&state.clone().rt, "catrender", 30, async move {
                let mut page: Vec<u8> = Vec::with_capacity(150_000);
                let rend = RendCtx::new();
                front_end::render_category(&mut page, cat, &crates, &state.markup, &rend).await?;
                minify_html(&mut page);
                mark_server_still_alive(&state);
                if rend.is_incomplete() {
                    cache_time /= 8;
                }
                Ok::<_, anyhow::Error>(Rendered {page: Page::Html(page), cache_time, refresh: false, last_modified: None})
            })
        })
        .await?,
    ))
}

#[tracing::instrument(level = "debug", skip_all)]
async fn handle_home(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let query = req.query_string().trim_start_matches('?');
    if !query.is_empty() && query.find('=').is_none() {
        return Ok(HttpResponse::TemporaryRedirect().insert_header((header::LOCATION, format!("/search?q={query}"))).finish());
    }

    let state: &AServerState = req.app_data().expect("appdata");
    let mut cache_time = 3600;
    Ok(serve_page(
        with_file_cache(state, "_.html", cache_time, false, {
            let state = state.clone();
            run_timeout("homepage", 300, async move {
                let crates = state.crates.load();
                let mut page: Vec<u8> = Vec::with_capacity(32000);
                let rend = RendCtx::new();
                front_end::render_homepage(&mut page, &crates, &rend).await?;
                minify_html(&mut page);
                mark_server_still_alive(&state);
                if rend.is_incomplete() {
                    cache_time /= 8;
                }
                Ok::<_, anyhow::Error>(Rendered {page: Page::Html(page), cache_time, refresh: false, last_modified: Some(Utc::now())})
            })
        })
        .await?,
    ))
}


#[tracing::instrument(level = "debug", skip_all)]
async fn handle_redirect(req: HttpRequest) -> HttpResponse {
    let inf = req.match_info();
    let rest = inf.query("rest");
    HttpResponse::PermanentRedirect().insert_header((header::LOCATION, format!("/{rest}"))).finish()
}


#[tracing::instrument(level = "debug", skip_all)]
async fn handle_git_clone(req: HttpRequest) -> HttpResponse {
    let inf = req.match_info();
    let crate_name = inf.query("crate");
    if let Some(o) = Origin::try_from_crates_io_name(crate_name) {
        let state2: &AServerState = req.app_data().expect("appdata");
        let state = state2.clone();
        if let Ok(url) = rt_run_timeout(&state2.rt, "gc", 60, async move {
            let crates = state.crates.load();
            let k = match crates.rich_crate_version_async(&o).await {
                Ok(k) => k,
                Err(_) => {
                    let o = Origin::from_crates_io_name(&invert_hyphens(o.package_name_icase()));
                    crates.rich_crate_version_async(&o).await?
                },
            };
            let r = k.repository().ok_or(anyhow!("no repo"))?;

            let mut url = r.canonical_git_url();
            url.truncate(url.trim_end_matches('/').len());
            if !url.ends_with(".git") {
                url.push_str(".git");
            }
            url.push_str("/info/refs?service=git-upload-pack");

            Ok(url)
        }).await {
            return HttpResponse::PermanentRedirect()
                .insert_header(("X-Robots-Tag", "noindex, nofollow"))
                .insert_header((header::LOCATION, url))
                .body("");
        }
    }
    HttpResponse::NotFound().body("Crate not found")
}

#[tracing::instrument(level = "debug", skip_all)]
async fn handle_crate_reverse_dependencies_redir(req: HttpRequest) -> HttpResponse {
    let inf = req.match_info();
    let rest = inf.query("crate");
    HttpResponse::PermanentRedirect().insert_header((header::LOCATION, format!("/crates/{rest}/rev"))).finish()
}

#[tracing::instrument(level = "debug", skip_all)]
async fn handle_author_redirect(req: HttpRequest) -> HttpResponse {
    let inf = req.match_info();
    let rest = inf.query("author");
    HttpResponse::PermanentRedirect().insert_header((header::LOCATION, format!("/~{rest}"))).finish()
}

#[tracing::instrument(level = "debug", skip_all)]
async fn handle_game_redirect(_: HttpRequest) -> HttpResponse {
    HttpResponse::PermanentRedirect().insert_header((header::LOCATION, "/game-development")).finish()
}

#[tracing::instrument(level = "debug", skip_all)]
async fn handle_repo_crate(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let state: &AServerState = req.app_data().expect("appdata");
    let origin = match get_origin_from_req_match(&req) {
        Ok(res) => res,
        Err(crate_name) => return render_404_page(state, crate_name, "git crate").await,
    };

    if !state.crates.load().crate_exists(&origin) {
        let (repo, _) = origin.into_repo().expect("repohost");
        let url = repo.canonical_http_url("", None);
        return Ok(HttpResponse::TemporaryRedirect().insert_header((header::LOCATION, url)).finish());
    }

    let stale = is_bot(&req);
    let cache_time = 86400;
    Ok(serve_page(with_file_cache(state, &cache_file_name_for_origin(&origin), cache_time, stale, {
        Box::pin(render_crate_page(state.clone(), origin, cache_time))
    }).await?))
}

fn get_origin_from_req_match(req: &HttpRequest) -> Result<Origin, &str> {
    let inf = req.match_info();
    let slug = inf.query("host");
    let owner = inf.query("owner");
    let repo = inf.query("repo");
    let crate_name = inf.query("crate");
    debug!("{} crate {}/{}/{}", slug, owner, repo, crate_name);
    if !is_alnum_dot(owner) || !is_alnum_dot(repo) || !is_alnum(crate_name) {
        return Err(crate_name);
    }

    let origin = match slug {
        "gh" => Origin::from_github(SimpleRepo::new(owner, repo), crate_name),
        "lab" => Origin::from_gitlab(SimpleRepo::new(owner, repo), crate_name),
        _ => return Err(crate_name),
    };
    Ok(origin)
}

fn get_origin_from_subpath(q: &actix_web::dev::Path<Url>) -> Option<Origin> {
    let parts = q.query("crate");
    let mut parts = parts.splitn(4, '/');
    let first = parts.next()?;
    match parts.next() {
        None => Origin::try_from_crates_io_name(first),
        Some(owner) => {
            let repo = parts.next()?;
            let package = parts.next()?;
            match first {
                "github" | "gh" => Some(Origin::from_github(SimpleRepo::new(owner, repo), package)),
                "gitlab" | "lab" => Some(Origin::from_gitlab(SimpleRepo::new(owner, repo), package)),
                _ => None,
            }
        },
    }
}

#[tracing::instrument(level = "debug", skip_all)]
async fn handle_compat(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let origin = get_origin_from_subpath(req.match_info()).ok_or_else(|| anyhow!("boo"))?;
    let state: &AServerState = req.app_data().expect("appdata");
    let crates = state.crates.load();
    crates.reload_indexed_crate(&origin);
    let page = rt_run_timeout(&state.rt, "dbgcrate", 60, async move {
        let all = crates.rich_crate_async(&origin).await?;
        let mut page: Vec<u8> = Vec::with_capacity(32000);
        front_end::render_compat_page(&mut page, all, &crates).await?;
        Ok(page)
    }).await?;
    Ok(HttpResponse::Ok()
        .content_type("text/html;charset=UTF-8")
        .insert_header((header::CACHE_CONTROL, "no-cache"))
        .no_chunking(page.len() as u64)
        .body(page))
}

#[tracing::instrument(level = "debug", skip_all)]
async fn handle_debug(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let origin = get_origin_from_subpath(req.match_info()).ok_or_else(|| anyhow!("boo"))?;
    let state: &AServerState = req.app_data().expect("appdata");
    let crates = state.crates.load();
    let page = rt_run_timeout(&state.rt, "dbgcrate", 60, async move {
        let mut page: Vec<u8> = Vec::with_capacity(32000);
        front_end::render_debug_page(&mut page, &crates, &origin).await?;
        Ok(page)
    }).await?;
    Ok(HttpResponse::Ok()
        .content_type("text/html;charset=UTF-8")
        .insert_header((header::CACHE_CONTROL, "no-cache"))
        .no_chunking(page.len() as u64)
        .body(page))
}

#[tracing::instrument(level = "debug", skip_all)]
async fn handle_status(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let state: &AServerState = req.app_data().expect("appdata");
    let page = format!("
up: {} min
bg: {}
fg: {}
mem: {}MB",
    state.start_time.elapsed().as_secs()/60,
    state.background_job.available_permits(),
    state.foreground_job.available_permits(),
    ALLOCATOR.allocated()/1_000_000);

    let page: Vec<_> = page.into();
    Ok(HttpResponse::Ok()
        .content_type("text/plain;charset=UTF-8")
        .insert_header((header::CACHE_CONTROL, "no-cache"))
        .no_chunking(page.len() as u64)
        .body(page))
}

#[tracing::instrument(level = "debug", skip_all)]
async fn handle_install(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let state2: &AServerState = req.app_data().expect("appdata");
    let origin = if let Some(o) = get_origin_from_subpath(req.match_info()) {
        o
    } else {
        return render_404_page(state2, req.path().trim_start_matches("/install"), "crate").await;
    };

    let state = state2.clone();
    let rendered = rt_run_timeout(&state2.rt, "instpage", 30, async move {
        let crates = state.crates.load();
        let ver = blocking::awatch(format!("get {origin:?}"), crates.rich_crate_version_async(&origin)).await?;
        let mut page: Vec<u8> = Vec::with_capacity(32000);
        blocking::awatch(format!("inst for {origin:?}"), front_end::render_install_page(&mut page, &ver, &crates, &state.markup)).await?;
        minify_html(&mut page);
        mark_server_still_alive(&state);
        Ok::<_, anyhow::Error>(Rendered {page: Page::Html(page), cache_time: 24 * 3600, refresh: false, last_modified: None})
    }).await?;
    Ok(serve_page(rendered))
}

#[tracing::instrument(level = "debug", skip_all)]
async fn handle_author(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let login = req.match_info().query("author");
    let state: &AServerState = req.app_data().expect("appdata");

    let aut = match rt_run_timeout(&state.rt, "aut1", 5, {
        let login = login.to_owned();
        let crates = state.crates.load();
        async move { crates.author_by_login(&login).await }
    }).await {
        Ok(aut) => aut,
        Err(e) => {
            debug!("user fetch {} failed: {}", login, e);
            return render_404_page(state, login, "user").await;
        }
    };
    debug!("author page for {login:?}");
    if !aut.github.login.eq_ignore_ascii_case(login) {
        return Ok(HttpResponse::PermanentRedirect().insert_header((header::LOCATION, format!("/~{}", Encoded(&aut.github.login)))).finish());
    }

    let crates = state.crates.load();
    let aut2 = aut.clone();
    let mut rows = rt_run_timeout(&state.rt, "authorpage1", 60, async move { crates.all_crates_of_author(&aut2).await }).await?;
    rows.retain(|c| c.deleted_at.is_none());

    if rows.is_empty() {
        debug!("author {:?} has 0 crates", aut.github.login);
        return Ok(HttpResponse::TemporaryRedirect()
            .insert_header((header::CACHE_CONTROL, "no-cache"))
            .insert_header(("X-Robots-Tag", "noindex, nofollow"))
            .insert_header((header::LOCATION, format!("https://github.com/{}", Encoded(&aut.github.login)))).finish());
    }

    let stale = is_bot(&req);
    let cache_time = 3600;
    Ok(serve_page(
        with_file_cache(state, &format!("@{}.html", aut.github.login.as_ascii_lowercase()), cache_time, stale, {
            let state = state.clone();
            run_timeout(format!("authorpage {}", aut.github.login), 60, async move {
                let crates = state.crates.load();
                let mut page: Vec<u8> = Vec::with_capacity(32000);
                front_end::render_author_page(&mut page, rows, &aut, &crates).await?;
                minify_html(&mut page);
                mark_server_still_alive(&state);
                Ok::<_, anyhow::Error>(Rendered {page: Page::Html(page), refresh: false, cache_time, last_modified: None})
            })
        })
        .await?,
    ))
}

#[tracing::instrument(level = "debug", skip_all)]
async fn handle_maintainer_dashboard_html(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    handle_maintainer_dashboard(req, false).await
}

#[tracing::instrument(level = "debug", skip_all)]
async fn handle_maintainer_dashboard_xml(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    handle_maintainer_dashboard(req, true).await
}

/// did you put crate name by accident?
#[tracing::instrument(level = "debug", skip(state))]
async fn author_by_crate_name(crate_name: &str, state: &ServerState) -> Option<String> {
    let origin = Origin::try_from_crates_io_name(crate_name)?;
    let crate_name = crate_name.to_owned();
    let crates = state.crates.load();
    rt_run_timeout(&state.rt, "chk", 5, async move {
        let owners = crates.crate_owners(&origin, kitchen_sink::CrateOwners::Strict).await?;
        if let Some(gh) = owners.iter().find_map(|o| o.github_login()) {
            if !gh.eq_ignore_ascii_case(&crate_name) {
                return Ok(gh.to_owned());
            }
        }
        anyhow::bail!("none")
    }).await.ok()
}

async fn handle_maintainer_dashboard(req: HttpRequest, atom_feed: bool) -> Result<HttpResponse, ServerError> {
    let login = req.match_info().query("author").trim();
    debug!("maintainer_dashboard for {:?}", login);
    let state: &AServerState = req.app_data().expect("appdata");

    let crates = state.crates.load();
    if crates.crates_io_login_on_blocklist(login).is_some() {
        return Ok(HttpResponse::TemporaryRedirect().insert_header((header::LOCATION, format!("/~{}", Encoded(login)))).finish());
    }

    let aut = match rt_run_timeout(&state.rt, "aut1", 5, {
        let login = login.to_owned();
        let crates = state.crates.load();
        async move { crates.author_by_login(&login).await }
    }).await {
        Ok(aut) => aut,
        Err(e) => {
            if let Some(gh) = author_by_crate_name(login, state).await {
                return Ok(HttpResponse::TemporaryRedirect().insert_header((header::LOCATION, format!("/~{}/dash", Encoded(gh)))).finish());
            }
            debug!("user fetch {} failed: {}", login, e);
            return render_404_page(state, login, "user").await;
        }
    };
    if !atom_feed && !aut.github.login.eq_ignore_ascii_case(login) {
        return Ok(HttpResponse::PermanentRedirect().insert_header((header::LOCATION, format!("/~{}/dash", Encoded(&aut.github.login)))).finish());
    }

    let aut2 = aut.clone();
    let mut rows = rt_run_timeout(&state.rt, "maintainer_dashboard1", 60, async move { crates.all_crates_of_author(&aut2).await }).await?;
    rows.retain(|c| c.deleted_at.is_none());

    if rows.is_empty() {
        if let Some(gh) = author_by_crate_name(login, state).await {
            return Ok(HttpResponse::TemporaryRedirect().insert_header((header::LOCATION, format!("/~{}/dash", Encoded(gh)))).finish());
        }
        return Ok(HttpResponse::TemporaryRedirect().insert_header((header::LOCATION, format!("/~{}", Encoded(&aut.github.login)))).finish());
    }

    let cache_file_name = format!("@{login}.dash{}.html", if atom_feed { "xml" } else { "" });
    let mut cache_time = if atom_feed { 3 * 3600 } else { 600 };
    let rendered = with_file_cache(state, &cache_file_name, cache_time, false, {
        let state = state.clone();
        run_timeout(format!("maintainer_dashboard2 {} {atom_feed}", aut.github.login), 60, async move {
            let crates = state.crates.load();
            let mut page: Vec<u8> = Vec::with_capacity(32000);
            let rend = RendCtx::new();
            front_end::render_maintainer_dashboard(&mut page, atom_feed, rows, &aut, &crates, &rend).await?;
            if !atom_feed { minify_html(&mut page); }
            mark_server_still_alive(&state);
            if rend.is_incomplete() {
                cache_time = 1;
            }
            Ok::<_, anyhow::Error>(Rendered {page: Page::Html(page), refresh: false, cache_time, last_modified: None})
        })
    })
    .await?;
    if !atom_feed {
        Ok(serve_page(rendered))
    } else {
        Ok(serve_feed(rendered))
    }
}

fn cache_file_name_for_origin(origin: &Origin) -> String {
    match origin {
        Origin::CratesIo(crate_name) => {
            assert!(!crate_name.as_bytes().iter().any(|&b| b == b'/' || b == b'.'));
            format!("{crate_name}.html")
        },
        Origin::GitHub(r) | Origin::GitLab(r) => {
            assert!(!r.package.as_bytes().iter().any(|&b| b == b'/' || b == b'.'));
            let slug = if let Origin::GitHub {..} = origin { "gh" } else { "lab" };
            format!("{slug},{},{},{}.html", r.repo.owner, r.repo.repo, r.package)
        }
    }
}

#[tracing::instrument(level = "debug", skip_all)]
async fn handle_crate(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let crate_name = req.match_info().query("crate");
    debug!("crate page for {:?}", crate_name);
    let state: &AServerState = req.app_data().expect("appdata");
    let crates = state.crates.load();
    let origin = match Origin::try_from_crates_io_name(crate_name).filter(|o| crates.crate_exists(o)) {
        Some(o) => o,
        None => return render_404_page(state, crate_name, "crate").await,
    };
    let stale = is_bot(&req);
    let cache_time = 600;
    Ok(serve_page(with_file_cache(state, &cache_file_name_for_origin(&origin), cache_time, stale, {
        Box::pin(render_crate_page(state.clone(), origin, cache_time))
    }).await?))
}


#[tracing::instrument(level = "debug", skip_all)]
async fn handle_repo_crate_all_versions(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let state: &AServerState = req.app_data().expect("appdata");
    let origin = match get_origin_from_req_match(&req) {
        Ok(res) => res,
        Err(crate_name) => return render_404_page(state, crate_name, "git crate").await,
    };

    Ok(serve_page(Box::pin(render_crate_all_versions(state.clone(), origin)).await?))
}

#[tracing::instrument(level = "debug", skip_all)]
async fn handle_crate_all_versions(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let crate_name = req.match_info().query("crate");
    debug!("allver for {:?}", crate_name);
    let state: &AServerState = req.app_data().expect("appdata");
    let crates = state.crates.load();
    let origin = match Origin::try_from_crates_io_name(crate_name).filter(|o| crates.crate_exists(o)) {
        Some(o) => o,
        None => return render_404_page(state, crate_name, "crate").await,
    };
    Ok(serve_page(Box::pin(render_crate_all_versions(state.clone(), origin)).await?))
}

#[tracing::instrument(level = "debug", skip_all)]
async fn handle_crate_features(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let crate_name = req.match_info().query("crate");
    let state: &AServerState = req.app_data().expect("appdata");
    let crates = state.crates.load();
    let origin = match Origin::try_from_crates_io_name(crate_name).filter(|o| crates.crate_exists(o)) {
        Some(o) => o,
        None => return render_404_page(state, crate_name, "crate").await,
    };
    Ok(serve_page(Box::pin(render_crate_features(state.clone(), origin)).await?))
}

#[tracing::instrument(level = "debug", skip_all)]
async fn handle_crate_source_redirect(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let crate_name = req.match_info().query("crate");
    let qs = qstring::QString::from(req.query_string());
    let at_ver = qs.get("at").unwrap_or_default();
    debug!("git redirect for {:?} {:?}", crate_name, at_ver);

    let state: &AServerState = req.app_data().expect("appdata");
    let crates = state.crates.load();
    let origin = match Origin::try_from_crates_io_name(crate_name).filter(|o| crates.crate_exists(o)) {
        Some(o) => o,
        None => return render_404_page(state, crate_name, "git crate").await,
    };

    let at_ver = at_ver.to_owned();
    let state = Arc::clone(state);
    let git_url = rt_run_timeout(&state.clone().rt, "git revision lookup", 15, async move {
        let crates = state.crates.load();
        crates.canonical_http_of_crate_at_version(&origin, &at_ver).await
    })
    .await?;
    return Ok(HttpResponse::TemporaryRedirect().insert_header((header::LOCATION, git_url)).finish());
}

#[tracing::instrument(level = "debug", skip_all)]
async fn handle_crate_api_downloads(crate_name: web::Path<String>, state: web::Data<ServerState>) -> Result<web::Json<HashMap<String, u32>>, ServerError> {
    let origin = Origin::try_from_crates_io_name(&crate_name).ok_or_else(|| anyhow!("bad crate name"))?;
    let dl = rt_run_timeout(&state.clone().rt, "api dl", 30, async move {
        let kitchen_sink = state.crates.load();
        kitchen_sink.recent_downloads_by_version(&origin).await.map_err(anyhow::Error::from)
    }).await?;
    let dl = dl.into_iter().map(|(k, v)| (k.to_semver().to_string(), v)).collect();
    Ok(web::Json(dl))
}

async fn handle_crate_reverse_dependencies(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let crate_name = req.match_info().query("crate");
    debug!("rev deps for {:?}", crate_name);
    let state: &AServerState = req.app_data().expect("appdata");
    let crates = state.crates.load();
    let origin = match Origin::try_from_crates_io_name(crate_name).filter(|o| crates.crate_exists(o)) {
        Some(o) => o,
        None => return render_404_page(state, crate_name, "crate").await,
    };
    Ok(serve_page(Box::pin(render_crate_reverse_dependencies(state.clone(), origin)).await?))
}

async fn handle_crate_reviews_redirect(req: HttpRequest) -> HttpResponse {
    let c = req.match_info().query("crate");
    HttpResponse::PermanentRedirect().insert_header((header::LOCATION, format!("/crates/{}/audit", Encoded(c)))).body(())
}

#[tracing::instrument(level = "debug", skip_all)]
async fn handle_crate_reviews(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let crate_name = req.match_info().query("crate");
    debug!("crev for {:?}", crate_name);
    let state: &AServerState = req.app_data().expect("appdata");
    let crates = state.crates.load();
    let origin = match Origin::try_from_crates_io_name(crate_name).filter(|o| crates.crate_exists(o)) {
        Some(o) => o,
        None => return render_404_page(state, crate_name, "crate").await,
    };
    let state = state.clone();
    Ok(serve_page(
        rt_run_timeout(&state.clone().rt, "revpage", 30, async move {
            let crates = state.crates.load();
            let ver = crates.rich_crate_version_async(&origin).await?;
            let mut page: Vec<u8> = Vec::with_capacity(32000);
            let reviews = crates.reviews_for_crate(ver.origin());
            let vreviews = crates.vets_for_crate(ver.origin()).await.map_err(|e| {
                error!("cargo vet fail: {e} {:?}", e.source())
            }).unwrap_or_default();
            front_end::render_crate_reviews(&mut page, &reviews, &vreviews, &ver, &crates, &state.markup).await?;
            minify_html(&mut page);
            mark_server_still_alive(&state);
            Ok::<_, anyhow::Error>(Rendered {page: Page::Html(page), cache_time: 24 * 3600, refresh: false, last_modified: None})
        })
        .await?,
    ))
}

#[tracing::instrument(level = "debug", skip_all)]
async fn handle_new_trending(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let state: &AServerState = req.app_data().expect("appdata");
    let mut cache_time = 600;
    Ok(serve_page(
        with_file_cache(state, "_new_.html", cache_time, false, {
            let state = state.clone();
            run_timeout("trendpage", 60, async move {
                let crates = state.crates.load();
                let mut page: Vec<u8> = Vec::with_capacity(32000);
                let rend = RendCtx::new();
                front_end::render_trending_crates(&mut page, Arc::clone(&crates), &state.markup, &rend).await?;
                minify_html(&mut page);
                if rend.is_incomplete() {
                    cache_time /= 8;
                }
                Ok::<_, anyhow::Error>(Rendered {page: Page::Html(page), refresh: false, cache_time, last_modified: None})
            })
        })
        .await?,
    ))
}

#[tracing::instrument(level = "debug", skip_all)]
async fn handle_global_stats(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let state: &AServerState = req.app_data().expect("appdata");
    let mut cache_time = 6 * 3600;
    Ok(serve_page(
        with_file_cache(state, "_stats_.html", cache_time, false, {
            let state = state.clone();
            run_timeout("globstats", 90, async move {
                let crates = state.crates.load();
                let mut page: Vec<u8> = Vec::with_capacity(32000);
                let rend = RendCtx::new();
                front_end::render_global_stats(&mut page, &crates, &state.markup, &rend).await?;
                minify_html(&mut page);
                if rend.is_incomplete() {
                    cache_time /= 8;
                }
                Ok::<_, anyhow::Error>(Rendered {page: Page::Html(page), refresh: false, cache_time, last_modified: None})
            })
        })
        .await?,
    ))
}

const CACHE_MAGIC_TAG: &[u8; 4] = b"  <c";


struct RenderLock<'k> {
    key: &'k str,
    lock: Arc<tokio::sync::Mutex<()>>,
}

struct RenderLockGuard<'k, 'l> {
    key: &'k str,
    _guard: tokio::sync::MutexGuard<'l, ()>,
}

impl Drop for RenderLockGuard<'_, '_> {
    fn drop(&mut self) {
        // this is a bit crap, it should check refcount?
        PAGES_RENDER.lock().unwrap().remove(self.key);
    }
}

impl<'k> RenderLock<'k> {
    pub fn new(key: &'k str) -> Self {
        let lock = PAGES_RENDER.lock().unwrap().entry(key.into()).or_default().clone();
        Self { key, lock }
    }

    async fn lock(&self) -> Result<RenderLockGuard<'k, '_>, ServerError> {
        let guard = match self.lock.try_lock() {
            Ok(g) => g,
            Err(_) => {
                debug!("throttling rendering of {}, because one is already running", self.key);
                tokio::time::timeout(Duration::from_secs(125), self.lock.lock()).await
                    .map_err(|_| {
                        PAGES_RENDER.lock().unwrap().remove(self.key);
                        anyhow!("Timed out waiting for cache lock of {}", self.key)
                    })?
            },
        };
        Ok(RenderLockGuard {
            key: self.key,
            _guard: guard,
        })
    }
}

fn get_cached_page(cache_file: &Path, is_acceptable: bool) -> Result<(u32, Vec<u8>), anyhow::Error> {
    let mut page_cached = std::fs::read(cache_file)?;

    if !is_acceptable || page_cached.len() <= 8 {
        let _ = std::fs::remove_file(cache_file); // next req will block instead of an endless refresh loop
    }
    if page_cached.len() <= 8 {
        anyhow::bail!("bad cache");
    }

    let trailer_pos = page_cached.len() - 8; // The worst data format :)
    if page_cached[trailer_pos .. trailer_pos + 4] != CACHE_MAGIC_TAG[..] {
        let _ = std::fs::remove_file(cache_file);
    }
    let timestamp = u32::from_le_bytes(page_cached.get(trailer_pos + 4..).unwrap().try_into().unwrap());
    page_cached.truncate(trailer_pos);
    Ok((timestamp, page_cached))
}

/// takes path to storage, freshness in seconds, and a function to call on cache miss
/// returns (page, fresh in seconds)
#[inline(never)]
async fn with_file_cache(state: &AServerState, cache_file_name: &str, cache_time: u32, allow_stale: bool, generate: Pin<Box<dyn Future<Output=Result<Rendered, anyhow::Error>> + Send + 'static>>) -> Result<Rendered, anyhow::Error> {
    let l = RenderLock::new(cache_file_name);
    let _g = blocking::awatch(format!("render lock {cache_file_name}"), l.lock()).await;

    let cache_file = state.page_cache_dir.join(cache_file_name);
    if let Ok(modified) = std::fs::metadata(&cache_file).and_then(|m| m.modified()) {
        let now = SystemTime::now();
        let cache_time = u64::from(cache_time); // TODO: read it from the cache file
        // rebuild in debug always
        let is_fresh = !cfg!(debug_assertions) && modified > (now - Duration::from_secs(cache_time / 20 + 5));
        let is_acceptable = allow_stale || (modified > (now - Duration::from_secs(3600 * 24 * 2 + cache_time * 8)));

        let age_secs = now.duration_since(modified).ok().map_or(0, |age| age.as_secs());

        if let Ok((timestamp, page_cached)) = get_cached_page(&cache_file, is_acceptable) {
            let last_modified = if timestamp > 0 { Utc.timestamp_opt(i64::from(timestamp), 0).single() } else { None };
            let cache_time_remaining = cache_time.saturating_sub(age_secs);

            debug!("Using cached page {} {}s fresh={:?} acc={:?} bot={allow_stale}", cache_file.display(), cache_time_remaining, is_fresh, is_acceptable);

            if !is_fresh && !allow_stale && !stopped() {
                background_refresh(state.clone(), cache_file, generate);
            }
            return Ok(Rendered {
                page: Page::Html(page_cached),
                cache_time: if !is_fresh { cache_time_remaining / 4 } else { cache_time_remaining }.max(2) as u32,
                refresh: !is_acceptable,
                last_modified,
            });
        }

        debug!("Cache miss {} {}", cache_file.display(), age_secs);
    } else {
        debug!("Cache miss {} no file", cache_file.display());
    }

    let generate_spawned = state.rt.spawn({
        let state = state.clone();
        let cache_file_name2 = cache_file_name.to_string();
        async move {
            let _s = tokio::time::timeout(Duration::from_secs(10), state.foreground_job.acquire()).await
                .context("The server is too busy to generate this page")
                .map_err(|e| { warn!("too busy for {cache_file_name2}"); e })?;
            running()?;
            blocking::awatch(format!("page miss {cache_file_name2}"), generate).await
        }.instrument(tracing::debug_span!("g", cache_file_name))});

    let mut rendered = match timeout(Duration::from_secs(if allow_stale {29} else {14}), generate_spawned).await {
        Ok(ran) => ran??,
        Err(_) => {
            warn!("{cache_file_name} is slow to generate");
            return Err(WorkInProgressError(FOREGROUND_JOB_MAX_PERMITS - state.foreground_job.available_permits()).into())
        },
    };

    let timestamp = rendered.last_modified.map_or(0, |a| a.timestamp() as u32);
    if let Page::Html(ref mut page) = rendered.page {
        running()?;

        page.extend_from_slice(&CACHE_MAGIC_TAG[..]); // The worst data format :)
        page.extend_from_slice(&timestamp.to_le_bytes()); // The worst data format :)
        if let Err(e) = std::fs::write(&cache_file, &page) {
            error!("warning: Failed writing to {}: {}", cache_file.display(), e);
        }
        page.truncate(page.len() - 8);
    }

    Ok(rendered)
}

#[tracing::instrument(level = "debug", skip(state, generate))]
#[inline(never)]
fn background_refresh(state: AServerState, cache_file: PathBuf, generate: Pin<Box<dyn Future<Output=Result<Rendered, anyhow::Error>> + Send + 'static>>) {
    let _runs_in_bg = rt_run_timeout_bg(&state.clone().rt, "refreshbg", 300, {
        async move {
            running()?;
            let start = Instant::now();
            debug!("Bg refresh of {}", cache_file.display());
            if let Ok(_s) = state.background_job.try_acquire() {
                match blocking::awatch(format!("bg {}", cache_file.display()), generate).await {
                    Ok(Rendered { page: Page::Html(mut page), last_modified, .. }) => {
                        running()?;
                        info!("Done refresh of {} in {}ms", cache_file.display(), start.elapsed().as_millis() as u32);

                        let timestamp = last_modified.map_or(0, |a| a.timestamp() as u32);
                        page.extend_from_slice(&CACHE_MAGIC_TAG[..]); // The worst data format :)
                        page.extend_from_slice(&timestamp.to_le_bytes());

                        if let Err(e) = std::fs::write(&cache_file, &page) {
                            error!("warning: Failed writing to {}: {}", cache_file.display(), e);
                        }
                    }
                    Ok(Rendered { page: _, .. }) => {
                        info!("Refresh of {} bailed out after {}ms", cache_file.display(), start.elapsed().as_millis() as u32);
                        let _ = std::fs::remove_file(&cache_file);
                    },
                    Err(e) => {
                        error!("Refresh err: {} {}", e.chain().map(|e| e.to_string()).collect::<Vec<_>>().join("; "), cache_file.display());
                    }
                }
            } else {
                info!("Too busy to refresh {}", cache_file.display());
            }
            Ok(())
        }
    });
}

#[inline(never)]
async fn load_crate(crates: &KitchenSink, origin: &Origin) -> Result<(bool, bool, RichCrate, ArcRichCrateVersion), ServerError> {
    let ((bans, all), ver) = futures::try_join!(async move {
        let all = crates.rich_crate_async(origin).await.context("Loading crate versions")?;
        Ok((crates.crate_blocklist_actions(&all).await, all))
    }, crates.rich_crate_version_async(origin).map_err(|e| e.context("Can't load crate's details")))?;

    let (matches, actions) = bans;
    let redir = matches && actions.iter().all(|a| a.is_redirect());
    Ok((matches, redir, all, ver))
}

#[tracing::instrument(level = "debug", skip(state))]
#[inline(never)]
fn render_crate_page(state: AServerState, origin: Origin, mut cache_time: u32) -> Pin<Box<dyn Future<Output = Result<Rendered, anyhow::Error>> + Send + 'static>> {
    run_timeout(format!("cratepage {origin:?}"), 60, async move {
        let crates = state.crates.load();
        let (_, redir, all, ver) = load_crate(&crates, &origin).await?;

        if redir {
            // if there's no message to print, do not collect $200, go to crates.io
            return Ok(Rendered { page: Page::Redirect(format!("https://crates.io/crates/{}", Encoded(all.name()))), cache_time: 0, refresh: false, last_modified: None });
        }

        if ver.is_hidden() || ver.is_spam() {
            return Ok(Rendered { page: Page::Redirect(format!("/search?q={}", Encoded(all.name().replace(['-','_'], " OR ")))), cache_time: 0, refresh: false, last_modified: None });
        }

        let mut page: Vec<u8> = Vec::with_capacity(32000);
        let rend = RendCtx::new();
        let similar = block_in_place("similarc", || state.index.similar_crates(&origin))
            .map_err(|e| error!("similar: {e}")).unwrap_or_default();
        let last_modified = Box::pin(blocking::awatch("fcp", front_end::render_crate_page(&mut page, &all, &ver, similar, &crates, &state.markup, &rend))).await?;
        minify_html(&mut page);
        mark_server_still_alive(&state);
        if rend.is_incomplete() {
            cache_time /= 8;
        }
        if let Some(l) = last_modified {
            let age_days = Utc::now().signed_duration_since(l).num_days().max(0) as u32;
            if age_days < 7 {
                cache_time /= 2;
            } else if age_days > 360 {
                cache_time *= 2;
            }
        }
        Ok::<_, anyhow::Error>(Rendered {page: Page::Html(page), refresh: false, last_modified, cache_time})
    })
}

#[tracing::instrument(level = "debug", skip(state))]
async fn render_crate_all_versions(state: AServerState, origin: Origin) -> Result<Rendered, anyhow::Error> {
    let s = state.clone();
    rt_run_timeout(&s.rt, "allver", 60, async move {
        let crates = state.crates.load();
        let (redir, _, all, ver) = load_crate(&crates, &origin).await?;
        if redir {
            return Ok(Rendered { page: Page::Redirect(format!("https://crates.io/crates/{}/versions", all.name())), cache_time: 0, refresh: false, last_modified: None });
        }

        let last_modified = Some(all.most_recent_release());
        let mut page: Vec<u8> = Vec::with_capacity(60000);
        front_end::render_all_versions_page(&mut page, &all, &ver, &crates).await?;
        minify_html(&mut page);
        mark_server_still_alive(&state);
        Ok::<_, anyhow::Error>(Rendered {page: Page::Html(page), cache_time: 24*3600, refresh: false, last_modified})
    }).await
}

#[tracing::instrument(level = "debug", skip(state))]
async fn render_crate_features(state: AServerState, origin: Origin) -> Result<Rendered, anyhow::Error> {
    let s = state.clone();
    rt_run_timeout(&s.rt, "deps", 60, async move {
        let crates = state.crates.load();
        let (redir, _, all, ver) = load_crate(&crates, &origin).await?;
        if redir {
            return Ok(Rendered { page: Page::Redirect(format!("https://docs.rs/crate/{}/{}/features", all.name(), all.most_recent_version())), cache_time: 0, refresh: false, last_modified: None });
        }

        let last_modified = Some(all.most_recent_release()); drop(all);
        let mut page: Vec<u8> = Vec::with_capacity(60000);
        front_end::render_features_page(&mut page, &state.markup, &ver, &crates).await?;
        minify_html(&mut page);
        mark_server_still_alive(&state);
        Ok::<_, anyhow::Error>(Rendered {page: Page::Html(page), cache_time: 3*24*3600, refresh: false, last_modified})
    }).await
}

async fn render_crate_reverse_dependencies(state: AServerState, origin: Origin) -> Result<Rendered, anyhow::Error> {
    let s = state.clone();
    rt_run_timeout(&s.rt, "revpage2", 30, async move {
        let crates = state.crates.load();
        let ver = crates.rich_crate_version_async(&origin).await?;
        let mut page: Vec<u8> = Vec::with_capacity(32000);
        front_end::render_crate_reverse_dependencies(&mut page, &ver, &crates, &state.markup).await?;
        minify_html(&mut page);
        mark_server_still_alive(&state);
        Ok::<_, anyhow::Error>(Rendered {page: Page::Html(page), cache_time: 24*3600, refresh: false, last_modified: None})
    }).await
}

#[tracing::instrument(level = "debug", skip_all)]
async fn handle_keyword(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let query = match decode(req.match_info().query("keyword")) {
        Ok(q) if !q.is_empty() => q,
        _ => return Ok(HttpResponse::TemporaryRedirect().insert_header((header::LOCATION, "/")).finish()),
    };

    let state: &AServerState = req.app_data().expect("appdata");
    let norm = state.index.normalize_keyword(&query);
    if norm != query {
        return Ok(HttpResponse::TemporaryRedirect().insert_header((header::LOCATION, format!("/keywords/{norm}"))).finish());
    }

    let query = query.into_owned();
    let state2 = state.clone();
    let (query, page) = tokio::task::spawn_blocking(blocking::closure("hkw", move || {
        if !is_alnum(&query) {
            return Ok::<_, anyhow::Error>((query, None));
        }
        let keyword_query = format!("keywords:\"{query}\"");
        let mut results = state2.index.search(&keyword_query, 250, false).context("Search index failure")?;

        let crates = state2.crates.load();
        results.crates.retain(|res| crates.crate_exists(&res.origin)); // search index can contain stale items

        if !results.crates.is_empty() {
            let mut page: Vec<u8> = Vec::with_capacity(60_000);
            front_end::render_keyword_page(&mut page, &query, &results, &state2.markup)?;
            minify_html(&mut page);
            Ok((query, Some(page)))
        } else {
            Ok((query, None))
        }
    })).await??;

    Ok(if let Some(page) = page {
        HttpResponse::Ok()
            .content_type("text/html;charset=UTF-8")
            .insert_header((header::CACHE_CONTROL, "public, max-age=172800, stale-while-revalidate=604800, stale-if-error=86400"))
            .no_chunking(page.len() as u64)
            .body(page)
    } else {
        HttpResponse::TemporaryRedirect().insert_header((header::LOCATION, format!("/search?q={}", Encoded(&query)))).finish()
    })
}

#[derive(Debug)]
enum Page {
    Html(Vec<u8>),
    Redirect(String),
}

impl Page {
    pub fn into_response(self) -> HttpResponse {
        let Self::Redirect(url) = self else { panic!() };
        HttpResponse::TemporaryRedirect().insert_header((header::LOCATION, url)).finish()
    }
}

#[derive(Debug)]
struct Rendered {
    page: Page,
    // s
    cache_time: u32,
    refresh: bool,
    last_modified: Option<DateTime<Utc>>,
}

#[inline(never)]
fn serve_page(Rendered {page, cache_time, refresh, last_modified}: Rendered) -> HttpResponse {
    let Page::Html(page) = page else {
        return page.into_response();
    };

    let err_max = (cache_time * 10).max(3600 * 24 * 2);

    let last_modified_secs = last_modified.map_or(0, |l| Utc::now().signed_duration_since(l).num_seconds().max(0) as u32);
    // if no updates for a year, don't expect more, and keep old page cached for longer
    let extra_time = if last_modified_secs < 3600*24*365 { last_modified_secs / 300 } else { last_modified_secs / 40 };
    let cache_time = cache_time.max(extra_time);

    // last-modified is ambiguous, because it's modification of the content, not the whole state
    let mut hasher = blake3::Hasher::new();
    hasher.update(if refresh { b"1" } else { b"0" });
    hasher.update(&page);
    let etag = format!("\"{:.16}\"", base64::encode(hasher.finalize().as_bytes()));

    let mut h = HttpResponse::Ok();
    h.content_type("text/html;charset=UTF-8");
    h.insert_header((header::ETAG, etag));
    if !refresh {
        h.insert_header((header::CACHE_CONTROL, format!("public, max-age={cache_time}, stale-while-revalidate={}, stale-if-error={err_max}", cache_time * 3)));
    }
    if refresh {
        h.insert_header((header::REFRESH, "5"));
        h.insert_header((header::CACHE_CONTROL, "no-cache, s-maxage=4, must-revalidate"));
    }
    if let Some(l) = last_modified {
        // can't give validator, because then 304 leaves refresh
        if !refresh {
            h.insert_header((header::LAST_MODIFIED, l.to_rfc2822()));
        }
    }
    h.no_chunking(page.len() as u64).body(page)
}

#[inline(never)]
fn serve_feed(Rendered {page, cache_time, refresh, last_modified}: Rendered) -> HttpResponse {
    let Page::Html(page) = page else {
        return page.into_response();
    };

    // last-modified is ambiguous, because it's modification of the content, not the whole state
    let mut hasher = blake3::Hasher::new();
    hasher.update(if refresh { b"1" } else { b"0" });
    hasher.update(&page);
    let etag = format!("\"{:.16}\"", base64::encode(hasher.finalize().as_bytes()));

    let mut h = HttpResponse::Ok();
    h.content_type("application/atom+xml;charset=UTF-8");
    h.insert_header((header::ETAG, etag));
    if !refresh {
        h.insert_header((header::CACHE_CONTROL, format!("public, max-age={cache_time}")));
        if let Some(l) = last_modified {
            h.insert_header((header::LAST_MODIFIED, l.to_rfc2822()));
        }
    }
    h.no_chunking(page.len() as u64).body(page)
}

fn is_alnum(q: &str) -> bool {
    q.as_bytes().iter().copied().all(|c| c.is_ascii_alphanumeric() || c == b'_' || c == b'-')
}

fn is_alnum_dot(q: &str) -> bool {
    let mut chars = q.as_bytes().iter().copied();
    if !chars.next().map_or(false, |first| first.is_ascii_alphanumeric() || first == b'_') {
        return false;
    }
    chars.all(|c| c.is_ascii_alphanumeric() || c == b'_' || c == b'-' || c == b'.')
}

#[tracing::instrument(level = "debug", skip_all)]
#[inline(never)]
async fn handle_search(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let qs = req.query_string().replace('+', "%20");
    let qs = qstring::QString::from(qs.as_str());
    let q = qs.get("q").unwrap_or("").trim_start();
    if q.is_empty() {
        return Ok(HttpResponse::PermanentRedirect().insert_header((header::LOCATION, "/")).finish());
    }

    let state: &AServerState = req.app_data().expect("appdata");
    let query = q.to_owned();
    Ok(serve_page(tokio::task::spawn_blocking({
        let state = state.clone();
        blocking::closure(format!("search {q}"), move || {
            let mut results = state.index.search(&query, 150, true).context("Search index failure")?;
            let crates = state.crates.load();
            results.crates.retain(|res| crates.crate_exists(&res.origin)); // search index can contain stale items

            if results.crates.len() == 1 && results.crates[0].crate_base_score > 0.5 {
                return Ok(Rendered {
                    page: Page::Redirect(format!("/crates/{}", Encoded(results.crates.remove(0).crate_name))),
                    cache_time: 0, refresh: false, last_modified: None
                });
            }

            let mut page = Vec::with_capacity(32000);
            front_end::render_serp_page(&mut page, &query, &results, &state.markup)?;
            minify_html(&mut page);
            Ok::<_, anyhow::Error>(Rendered {page: Page::Html(page), cache_time: 600, refresh: false, last_modified: None})
        })
    }).await??))
}

#[tracing::instrument(level = "debug", skip_all)]
#[inline(never)]
async fn handle_sitemap(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let (w, page) = writer::<_, ServerError>().await;
    let state: &AServerState = req.app_data().expect("appdata");
    let _runs_in_bg = state.rt.spawn({
        let state = state.clone();
        async move {
            let mut w = std::io::BufWriter::with_capacity(16000, w);
            let crates = state.crates.load();
            if let Err(e) = front_end::render_sitemap(&mut w, &crates).await {
                if let Ok(mut w) = w.into_inner() {
                    w.fail(e.into());
                }
            }
        }
    });
    Ok(HttpResponse::Ok()
        .content_type("application/xml;charset=UTF-8")
        .insert_header((header::CACHE_CONTROL, "public, max-age=259200, stale-while-revalidate=72000, stale-if-error=72000"))
        .streaming::<_, ServerError>(page))
}

#[tracing::instrument(level = "debug", skip_all)]
#[inline(never)]
async fn handle_feed(req: HttpRequest) -> Result<HttpResponse, ServerError> {
    let state: &AServerState = req.app_data().expect("appdata");
    let state2 = state.clone();
    let page = rt_run_timeout(&state.rt, "feed", 60, async move {
        let crates = state2.crates.load();
        let mut page: Vec<u8> = Vec::with_capacity(32000);
        let rend = RendCtx::new();
        front_end::render_feed(&mut page, &crates, &rend).await?;
        Ok::<_, anyhow::Error>(page)
    }).await?;
    Ok(HttpResponse::Ok()
        .content_type("application/atom+xml;charset=UTF-8")
        .insert_header((header::CACHE_CONTROL, "public, max-age=10800, stale-while-revalidate=259200, stale-if-error=72000"))
        .no_chunking(page.len() as u64)
        .body(page))
}

#[tracing::instrument(level = "debug", skip(secs, fut))]
#[inline]
fn run_timeout<'a, S, R, T: 'static + Send>(label: S, secs: u32, fut: R) -> Pin<Box<dyn Future<Output = Result<T, anyhow::Error>> + Send + 'a>>
where
    S: 'a + fmt::Debug + AsRef<str> + Send + fmt::Display + tracing::Value + Clone,
    R: 'static + Send + Future<Output = Result<T, anyhow::Error>>,
{
    let fut = tokio::time::timeout(Duration::from_secs(secs.into()), fut).instrument(tracing::debug_span!("t", "{}", &label));
    let fut = kitchen_sink::NonBlock::new(label.clone(), fut);
    let timeout = fut.map(move |res| {
        res.map_err(|_| anyhow!("{} timed out after >{}s", label, secs))?
    });
    Box::pin(timeout)
}

#[inline]
#[track_caller]
fn rt_run_timeout<R, T: 'static + Send>(rt: &Handle, label: &'static str, secs: u32, fut: R) -> Pin<Box<dyn Future<Output=Result<T, anyhow::Error>> + 'static + Send>>
where
    R: 'static + Send + Future<Output = Result<T, anyhow::Error>>,
{
    Box::pin(SpawnAbortOnDrop(tokio::task::Builder::new()
            .name(label)
            .spawn_on(kitchen_sink::NonBlock::new(label, tokio::time::timeout(Duration::from_secs(secs.into()), fut)), rt).unwrap())
    .map(move |res| -> Result<T, anyhow::Error> {
        res?.map_err(|_| anyhow!("{} timed out after >{}s", label, secs))?
    }).instrument(tracing::debug_span!("a", label)))
}

#[tracing::instrument(level = "debug", skip(rt, secs, fut))]
#[track_caller]
#[inline]
fn rt_run_timeout_bg<R, T: 'static + Send>(rt: &Handle, label: &'static str, secs: u32, fut: R) -> impl Future<Output=Result<T, anyhow::Error>> + 'static + Send
where
    R: 'static + Send + Future<Output = Result<T, anyhow::Error>>,
{
    tokio::task::Builder::new()
            .name(label)
            .spawn_on(kitchen_sink::NonBlock::new(label, tokio::time::timeout(Duration::from_secs(secs.into()), fut)), rt)
            .unwrap()
    .map(move |res| -> Result<T, anyhow::Error> {
        res?.map_err(|_| anyhow!("{label} timed out after >{secs}s"))?
    }).instrument(tracing::debug_span!("b", label))
}

/// jobs in queue
#[derive(Debug)]
struct WorkInProgressError(usize);

struct ServerError {
    pub(crate) err: anyhow::Error,
}

impl ServerError {
    #[cold]
    pub fn new(err: anyhow::Error) -> Self {
        for cause in err.chain() {
            let mut s = cause.to_string();
            error!("• {}", s);
            // The server is stuck and useless
            s.make_ascii_lowercase();
            if s.contains("too many open files") || s.contains("instance has previously been poisoned") ||
               s.contains("cannot allocate memory") ||
               s.contains("inconsistent park state") || s.contains("failed to allocate an alternative stack") {
                error!("Fatal error: {}", s);
                kitchen_sink::stop();
                std::thread::sleep(Duration::from_secs(1));
                std::process::exit(2);
            }
        }
        Self { err }
    }
}

impl From<anyhow::Error> for ServerError {
    #[cold]
    fn from(err: anyhow::Error) -> Self {
        Self::new(err)
    }
}

impl From<tokio::task::JoinError> for ServerError {
    #[cold]
    fn from(err: tokio::task::JoinError) -> Self {
        Self::new(err.into())
    }
}

use std::fmt;
impl fmt::Display for ServerError {
    #[cold]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.err.fmt(f)
    }
}
impl fmt::Debug for ServerError {
    #[cold]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.err.fmt(f)
    }
}
impl std::error::Error for ServerError {
}

impl fmt::Display for WorkInProgressError {
    #[cold]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "The page is being generated, and should be ready in a few seconds. Lib.rs must fetch and process new data.\nThere {} {} job{} in the queue. Please refresh the page.",
            if self.0 == 1 { "is" } else { "are" },
            self.0,
            if self.0 == 1 { "" } else { "s" },
        )
    }
}

impl std::error::Error for WorkInProgressError {
}

impl actix_web::ResponseError for ServerError {
    fn status_code(&self) -> StatusCode {
        StatusCode::INTERNAL_SERVER_ERROR
    }

    #[cold]
    fn error_response(&self) -> HttpResponse<BoxBody> {
        let (mut res, title, headline) = if self.err.downcast_ref::<WorkInProgressError>().is_some() {
            let mut r = HttpResponse::Accepted(); // CDN shows error page for gateway timeout
            r.insert_header((header::REFRESH, "4"));
            r.insert_header((header::CACHE_CONTROL, "no-cache, no-store, must-revalidate"));
            r.insert_header(("X-Robots-Tag", "noindex, nofollow"));
            (r, "Please wait…", "Loading…")
        } else {
            (HttpResponse::InternalServerError(), "error[E0500]: lib.rs:1", "Sorry, something went wrong!")
        };

        let mut page = Vec::with_capacity(20000);
        front_end::render_error(&mut page, title.into(), headline, &self.err);

        res.content_type("text/html;charset=UTF-8")
            .no_chunking(page.len() as u64)
            .body(page)
    }
}

#[inline(never)]
fn minify_html(page: &mut Vec<u8>) {
    blocking::watch("minify", || {
    let mut m = html_minifier::HTMLMinifier::new();
    // digest wants bytes anyway
    if let Ok(()) = m.digest(&page) {
        let out = m.get_html();
        page.clear();
        page.extend_from_slice(out);
    }
    })
}
