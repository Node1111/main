use std::borrow::Cow;
use smartstring::alias::String as SmolStr;
use url::Url;

pub type GResult<T> = Result<T, GitError>;


#[derive(Debug, Clone, Hash, Eq, PartialEq)]
pub enum Repo {
    GitHub(SimpleRepo),
    GitLab(SimpleRepo),
    BitBucket(SimpleRepo),
    /// as set by the create author
    Other(Url),
}

#[derive(Debug, Clone, Hash, Eq, PartialEq, Ord, PartialOrd)]
pub struct SimpleRepo {
    pub owner: SmolStr,
    pub repo: SmolStr,
}

#[derive(Debug, Clone, Hash, Eq, PartialEq, Ord, PartialOrd)]
pub struct RepoPackage {
    pub repo: SimpleRepo,
    pub package: SmolStr,
}

impl SimpleRepo {
    pub fn new(owner: impl Into<SmolStr>, repo: impl Into<SmolStr>) -> Self {
        Self {
            owner: owner.into(),
            repo: repo.into(),
        }
    }
}

#[derive(Debug, Clone)]
pub enum GitError {
    IncompleteUrl,
    InvalidUrl(url::ParseError),
}

impl std::error::Error for GitError {}

impl std::fmt::Display for GitError {
    #[cold]
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match *self {
            GitError::IncompleteUrl => f.write_str("Incomplete URL"),
            GitError::InvalidUrl(e) => e.fmt(f),
        }
    }
}

impl Repo {
    /// Parse the given URL
    pub fn new(url: &str) -> GResult<Self> {
        let url = Url::parse(url.trim_end_matches('/')).map_err(GitError::InvalidUrl)?;
        Ok(match (&url.host_str(), url.path_segments()) {
            (Some("www.github.com" | "github.com"), Some(path)) => {
                Self::GitHub(Self::repo_from_path(path)?)
            },
            (Some("www.gitlab.com" | "gitlab.com"), Some(path)) => {
                Self::GitLab(Self::gitlab_repo_from_path(path)?)
            },
            (Some("bitbucket.org"), Some(path)) => {
                Self::BitBucket(Self::repo_from_path(path)?)
            },
            _ => Self::Other(url),
        })
    }

    fn repo_from_path<'a>(mut path: impl Iterator<Item = &'a str>) -> GResult<SimpleRepo> {
        let mut owner = SmolStr::from(path.next().ok_or(GitError::IncompleteUrl)?);
        owner.make_ascii_lowercase();
        let mut repo = SmolStr::from(path.next().ok_or(GitError::IncompleteUrl)?
            .trim_end_matches('/')
            .trim_end_matches(".git")
        );
        repo.make_ascii_lowercase();
        Ok(SimpleRepo {
            owner,
            repo,
        })
    }

    fn gitlab_repo_from_path<'a>(mut path: impl Iterator<Item = &'a str>) -> GResult<SimpleRepo> {
        let mut segments = vec![
            path.next().ok_or(GitError::IncompleteUrl)?,
        ];
        let mut seen_end = false;
        segments.extend(path
            .filter(|&item| {
                if seen_end { return false; }
                if item.ends_with(".git") { seen_end = true; }
                true
            })
            .take_while(|&item| item != "-" && item != "tree" && item != "blob" && item != "graphs")
            .map(|s| s.trim_end_matches('/').strip_suffix(".git").unwrap_or(s)));

        let (&owner, repo) = segments.split_first().unwrap();
        let mut owner = SmolStr::from(owner);
        owner.make_ascii_lowercase();
        let mut repo = SmolStr::from(repo.join("/"));
        repo.make_ascii_lowercase();
        Ok(SimpleRepo {
            owner,
            repo,
        })
    }

    /// True if the URL may be a well-known git repository URL
    #[must_use]
    pub fn looks_like_repo_url(url: &str) -> bool {
        Url::parse(url).ok().map_or(false, |url| matches!(url.host_str(),
            Some("github.com" | "www.github.com") | Some("gitlab.com" | "www.gitlab.com") | Some("bitbucket.org")))
    }

    /// Enum with details of git hosting service
    #[must_use] pub fn host(&self) -> &Repo {
        self
    }

    /// Enum with details of git hosting service
    #[must_use] pub fn github_host(&self) -> Option<&SimpleRepo> {
        match self {
            Self::GitHub(s) => Some(s),
            _ => None,
        }
    }

    /// URL to view who contributed to the repository
    #[must_use] pub fn contributors_http_url(&self) -> Cow<'_, str> {
        match self {
            Self::GitHub(SimpleRepo {ref owner, ref repo}) => {
                format!("https://github.com/{owner}/{repo}/graphs/contributors").into()
            },
            Self::GitLab(SimpleRepo {ref owner, ref repo}) => {
                format!("https://gitlab.com/{owner}/{repo}{}/graphs/master", if repo.contains('/') {"/-"} else {""}).into()
            },
            Self::BitBucket(SimpleRepo {ref owner, ref repo}) => {
                // not really…
                format!("https://bitbucket.org/{owner}/{repo}/commits/all").into()
            },
            Self::Other(url) => url.as_str().into(),
        }
    }

    /// Name of the hosting service
    #[must_use] pub fn site_link_label(&self, add_repo_suffix: bool) -> &'static str {
        match self {
            Self::GitHub(..) => if add_repo_suffix {"GitHub repo"} else {"GitHub"},
            Self::GitLab(..) => if add_repo_suffix {"GitLab repo"} else {"GitLab"},
            Self::BitBucket(..) => if add_repo_suffix {"BitBucket repo"} else {"BitBucket"},
            Self::Other(_) => "Repository",
        }
    }

    /// URL for links in readmes hosted on the git website
    ///
    /// Base dir is without leading or trailing `/`, i.e. `""` for root, `"foo/bar"`, etc.
    #[must_use] pub fn readme_base_url(&self, base_dir_in_repo: &str, treeish_revision: Option<&str>) -> String {
        assert!(!base_dir_in_repo.starts_with('/'));
        let treeish_revision = treeish_revision.unwrap_or("HEAD");
        let slash = if !base_dir_in_repo.is_empty() && !base_dir_in_repo.ends_with('/') { "/" } else { "" };
        match self {
            Self::GitHub(SimpleRepo {ref owner, ref repo}) => {
                format!("https://github.com/{owner}/{repo}/blob/{treeish_revision}/{base_dir_in_repo}{slash}")
            },
            Self::GitLab(SimpleRepo {ref owner, ref repo}) => {
                format!("https://gitlab.com/{owner}/{repo}{}/blob/{treeish_revision}/{base_dir_in_repo}{slash}", if repo.contains('/') {"/-"} else {""})
            },
            Self::BitBucket(SimpleRepo {ref owner, ref repo}) => {
                format!("https://bitbucket.org/{owner}/{repo}/src/{treeish_revision}/{base_dir_in_repo}{slash}")
            },
            Self::Other(url) => url.to_string() // FIXME: how to add base dir?
        }
    }

    /// URL for image embeds in readmes hosted on the git website
    ///
    /// Base dir is without leading or trailing `/`, i.e. `""` for root, `"foo/bar"`, etc.
    #[must_use] pub fn readme_base_image_url(&self, base_dir_in_repo: &str, treeish_revision: Option<&str>) -> String {
        let treeish_revision = treeish_revision.unwrap_or("HEAD");
        assert!(!base_dir_in_repo.starts_with('/'));
        let slash = if !base_dir_in_repo.is_empty() && !base_dir_in_repo.ends_with('/') { "/" } else { "" };
        match self {
            Self::GitHub(SimpleRepo {ref owner, ref repo}) => {
                format!("https://raw.githubusercontent.com/{owner}/{repo}/{treeish_revision}/{base_dir_in_repo}{slash}")
            },
            Self::GitLab(SimpleRepo {ref owner, ref repo}) => {
                format!("https://gitlab.com/{owner}/{repo}{}/raw/{treeish_revision}/{base_dir_in_repo}{slash}", if repo.contains('/') {"/-"} else {""})
            },
            Self::BitBucket(SimpleRepo {ref owner, ref repo}) => {
                format!("https://bitbucket.org/{owner}/{repo}/raw/{treeish_revision}/{base_dir_in_repo}{slash}")
            },
            Self::Other(url) => url.to_string() // FIXME: how to add base dir?
        }
    }

    /// URL for cloning the repository via git
    #[must_use] pub fn canonical_git_url(&self) -> String {
        match self {
            Self::GitHub(SimpleRepo {ref owner, ref repo}) => {
                format!("https://github.com/{owner}/{repo}.git")
            },
            Self::GitLab(SimpleRepo {ref owner, ref repo}) => {
                format!("https://gitlab.com/{owner}/{repo}.git")
            },
            Self::BitBucket(SimpleRepo {ref owner, ref repo}) => {
                format!("https://bitbucket.org/{owner}/{repo}")
            },
            Self::Other(url) => url.to_string()
        }
    }

    #[must_use]
    pub fn canonical_http_url_sha1(&self, base_dir_in_repo: &str, commit_hash: Option<[u8; 20]>) -> String {
        self.canonical_http_url(base_dir_in_repo, commit_hash.map(hex::encode).as_deref())
    }

    /// URL for browsing the repository via web browser, exact source unless basedir == "" and rev == None
    #[must_use]
    pub fn canonical_http_url(&self, base_dir_in_repo: &str, treeish_revision: Option<&str>) -> String {
        assert!(!base_dir_in_repo.starts_with('/'));
        let treeish_revision = treeish_revision.unwrap_or("HEAD");
        let path_part = if !base_dir_in_repo.is_empty() || treeish_revision != "HEAD" {
            let dir_name = match self {
                Self::BitBucket(_) => "src",
                _ => "tree",
            };
            format!("/{dir_name}/{treeish_revision}/{base_dir_in_repo}")
        } else {
            String::new()
        };
        match self {
            Self::GitHub(SimpleRepo {ref owner, ref repo}) => {
                format!("https://github.com/{owner}/{repo}{path_part}")
            },
            Self::GitLab(SimpleRepo {ref owner, ref repo}) => {
                format!("https://gitlab.com/{owner}/{repo}{}{path_part}", if repo.contains('/') {"/-"} else {""})
            },
            Self::BitBucket(SimpleRepo {ref owner, ref repo}) => {
                format!("https://bitbucket.org/{owner}/{repo}{path_part}")
            },
            Self::Other(url) => url.to_string(),
        }
    }

    /// This info may be out of date, because usernames can change, and URLs get redirected
    #[must_use] pub fn url_owner_name(&self) -> Option<&str> {
        match self {
            Self::GitHub(SimpleRepo { ref owner, .. }) |
            Self::BitBucket(SimpleRepo { ref owner, .. }) |
            Self::GitLab(SimpleRepo { ref owner, .. }) => Some(owner),
            Self::Other(_) => None,
        }
    }

    #[must_use] pub fn repo_name(&self) -> Option<&str> {
        self.repo().map(|r| &*r.repo)
    }

    #[must_use] pub fn repo(&self) -> Option<&SimpleRepo> {
        match self {
            Self::GitHub(repo) |
            Self::BitBucket(repo) |
            Self::GitLab(repo) => Some(repo),
            Self::Other(_) => None,
        }
    }
}

/// `https://github.com/user/repo/blob/main/assets/img.png`
/// to
/// `https://raw.githubusercontent.com/user/repo/main/assets/img.png`
#[must_use] pub fn github_blob_url_to_raw_url(url: &str) -> Option<String> {
    let gh = url.strip_prefix("https://github.com/")?;
    let after_user = gh.splitn(3, '/').nth(2)?;
    let user_repo = gh.get(..gh.len() - after_user.len())?;
    let path_suffix = after_user.strip_prefix("blob/")?;
    Some(format!("https://raw.githubusercontent.com/{user_repo}{path_suffix}"))
}

#[test]
fn image_fix() {
    assert_eq!(github_blob_url_to_raw_url("https://github.com/uname/reponame/blob/main/assets/img.png").unwrap(),
        "https://raw.githubusercontent.com/uname/reponame/main/assets/img.png");
}

#[test]
fn repo_parse() {
    let repo = Repo::new("HTTPS://GITHUB.COM/FOO/BAR").unwrap();
    assert_eq!("https://github.com/foo/bar.git", repo.canonical_git_url());
    assert_eq!("https://github.com/foo/bar", repo.canonical_http_url("", None));
    assert_eq!("https://github.com/foo/bar/tree/HEAD/subdir", repo.canonical_http_url("subdir", None));
    assert_eq!("https://github.com/foo/bar/tree/HEAD/sub/dir", repo.canonical_http_url("sub/dir", None));
    assert_eq!("https://raw.githubusercontent.com/foo/bar/HEAD/sub/dir/", repo.readme_base_image_url("sub/dir", None));

    let repo = Repo::new("https://gitlab.com/username/endsinslash/").unwrap();
    assert_eq!("https://gitlab.com/username/endsinslash.git", repo.canonical_git_url());

    let repo = Repo::new("HTTPS://GITlaB.COM/FOO/BAR").unwrap();
    assert_eq!("https://gitlab.com/foo/bar.git", repo.canonical_git_url());
    assert_eq!("https://gitlab.com/foo/bar/blob/HEAD/", repo.readme_base_url("", None));
    assert_eq!("https://gitlab.com/foo/bar/blob/main/foo/", repo.readme_base_url("foo", Some("main")));
    assert_eq!("https://gitlab.com/foo/bar/blob/HEAD/foo/bar/", repo.readme_base_url("foo/bar", None));
    assert_eq!("https://gitlab.com/foo/bar/raw/HEAD/baz/", repo.readme_base_image_url("baz/", None));
    assert_eq!("https://gitlab.com/foo/bar/raw/main/baz/", repo.readme_base_image_url("baz/", Some("main")));
    assert_eq!("https://gitlab.com/foo/bar/tree/HEAD/sub/dir", repo.canonical_http_url("sub/dir", None));

    let repo = Repo::new("HTTPS://GITlaB.COM/user/sub/project.git").unwrap();
    assert_eq!("https://gitlab.com/user/sub/project.git", repo.canonical_git_url());
    assert_eq!("https://gitlab.com/user/sub/project/-/blob/HEAD/", repo.readme_base_url("", None));
    assert_eq!("https://gitlab.com/user/sub/project/-/blob/main/foo/", repo.readme_base_url("foo", Some("main")));
    assert_eq!("https://gitlab.com/user/sub/project/-/blob/HEAD/foo/bar/", repo.readme_base_url("foo/bar", None));
    assert_eq!("https://gitlab.com/user/sub/project/-/raw/HEAD/baz/", repo.readme_base_image_url("baz/", None));
    assert_eq!("https://gitlab.com/user/sub/project/-/raw/main/baz/", repo.readme_base_image_url("baz/", Some("main")));
    assert_eq!("https://gitlab.com/user/sub/project/-/tree/HEAD/sub/dir", repo.canonical_http_url("sub/dir", None));
    assert_eq!("user", repo.url_owner_name().unwrap());
    assert_eq!("sub/project", repo.repo_name().unwrap());

    let repo = Repo::new("https://gitlab.com/kornelski/exclude_from_backups.git").unwrap();
    assert_eq!("https://gitlab.com/kornelski/exclude_from_backups.git", repo.canonical_git_url());

    let repo = Repo::new("http://priv@example.com/#111").unwrap();
    assert_eq!("http://priv@example.com/#111", repo.canonical_git_url());
    assert_eq!("http://priv@example.com/#111", repo.canonical_http_url("", None));

    let bad = Repo::new("N/A");
    assert!(bad.is_err());
}
