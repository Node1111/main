use cargo_toml::Manifest;
use cargo_toml::Package;
use libflate::gzip;
use libflate::deflate;
use log::{debug, warn, error};
use manifest::IdentifiedBinFile;
use render_readme::Markup;
use std::collections::HashSet;
use std::io::Read;
use std::io;
use std::path::Path;
use std::path::PathBuf;
use std::sync::atomic::AtomicU32;
use std::sync::atomic::Ordering;
use std::time::Duration;
use std::time::Instant;
use tar::{Archive, Entry, EntryType};
use udedokei::LanguageExt;
use serde::Deserialize;

pub use manifest::DetailedFeatures;

pub use udedokei::synscrape::ModuleScrape;

#[derive(Debug, thiserror::Error)]
pub enum UnarchiverError {
    #[error("Cargo.toml not found.\nGot files: {0}")]
    TomlNotFound(String),
    #[error("I/O error during unarchiving")]
    Io(#[from] #[source] io::Error),
    #[error("Cargo.toml parsing error")]
    Toml(#[from] #[source] cargo_toml::Error),
    #[error("Git checkout failure")]
    Checkout(#[from] #[source] crate_git_checkout::Error),
    #[error("JSON parse")]
    Parse(#[from] #[source] serde_json::Error),
    #[error("Untar timed out")]
    Timeout,
}

fn read_archive_files<R: Read>(archive: R, mut cb: impl FnMut(Entry<'_, gzip::Decoder<R>>) -> Result<(), UnarchiverError>) -> Result<(), UnarchiverError> {
    let mut archive = Archive::new(gzip::Decoder::new(archive)?);
    let entries = archive.entries()?;
    for entry in entries {
        cb(entry?)?
    }
    Ok(())
}

#[derive(Debug, Eq, PartialEq)]
enum ReadAs {
    Toml,
    TomlOrig,
    ReadmeMarkdown(String),
    ReadmeAsciiDoc(String),
    ReadmeRst(String),
    Lib,
    Bin,
    GetStatsOfFile(udedokei::Language),
    CargoVcsInfo,
    Irrelevant,
    OtherFile,
}

const MAX_FILE_SIZE: u64 = 50_000_000;

pub fn read_repo(repo: &crate_git_checkout::Repository, path_in_tree: crate_git_checkout::Oid, crate_name: &str, stop: &AtomicU32) -> Result<CrateFilesSummary, UnarchiverError> {
    let mut collect = Collector::new(crate_name, 0, false);
    crate_git_checkout::iter_blobs::<UnarchiverError, _>(repo, Some(path_in_tree), |path, _, name, blob| {
        if stop.load(Ordering::Relaxed) > 0 {
            panic!("stop");
        }

        // FIXME: skip subdirectories that contain other crates
        let mut blob_content = blob.content();
        collect.add(Path::new(path).join(name), blob_content.len() as u64, &mut blob_content)?;
        Ok(())
    })?;
    collect.finish()
}

pub fn read_archive(archive: &[u8], crate_name: &str, ver: &str) -> Result<CrateFilesSummary, UnarchiverError> {
    let start = Instant::now();
    debug!("untar {crate_name}-{ver} {}B", archive.len());

    let prefix = PathBuf::from(format!("{crate_name}-{ver}"));
    let mut collect = Collector::new(crate_name, archive.len() as _, true);
    let mut n = 0;
    read_archive_files(archive, |mut file| {
        if start.elapsed() > Duration::from_secs(60) || n > 1_000_000 {
            error!("untar of {crate_name}-{ver} is taking too long {}B/{n}", archive.len());
            return Err(UnarchiverError::Timeout);
        }
        n += 1;
        let header = file.header();
        match header.entry_type() {
            EntryType::Regular | EntryType::Char => {
                let path = header.path()?;
                if let Ok(relpath) = path.strip_prefix(&prefix) {
                    return collect.add(relpath.to_path_buf(), header.size()?, &mut file);
                }
            },
            _ => {},
        }
        Ok(())
    })?;
    collect.finish()
}

#[derive(Debug, Clone)]
pub struct Readme {
    pub rel_path: String,
    pub markup: Markup,
    pub found_relevant: bool,
    pub git_sha1: Option<[u8; 20]>,
}

#[derive(Debug, Clone)]
pub struct CrateFilesSummary {
    pub manifest: Manifest,
    /// Rust source
    pub lib_file: Option<String>,
    pub bin_file: Option<String>,
    pub files: Vec<PathBuf>,
    /// relative path, markdown, and true if it's a good match, false if a fallback
    pub readme: Option<Readme>,
    pub compressed_size: u64,
    pub decompressed_size: u64,
    pub is_nightly: bool,

    /// From .cargo_vcs_info.json, or polyfilled from checkout
    pub path_in_repo: Option<String>,
    pub vcs_info_git_sha1: Option<[u8; 20]>,

    /// From all code-like files + extra parsing of Rust files
    pub language_stats: udedokei::Stats,
    pub modules: Vec<ModuleScrape>,

    pub features_comments: Option<DetailedFeatures>,
    pub suspicious_binary_files: Vec<IdentifiedBinFile>,
}

struct Collector {
    crate_name: String,
    expect_cargo_toml_orig: bool,

    manifest: Option<Manifest>,
    features_comments: Option<DetailedFeatures>,
    lib_file: Option<String>,
    bin_file: Option<String>,
    files: Vec<PathBuf>,
    suspicious_binary_files: Vec<(PathBuf, &'static str, u64)>,
    markup: Option<(String, Markup)>,
    compressed_size: u64,
    decompressed_size: u64,
    is_nightly: bool,

    // From .cargo_vcs_info.json
    vcs_info_path: Option<String>,
    vcs_info_git_sha1: Option<[u8; 20]>,

    readme_from_workspace: bool,

    // From all code-like files
    stats: udedokei::Collect,
}

impl Collector {
    pub fn new(crate_name: &str, compressed_size: u64, expect_cargo_toml_orig: bool) -> Self {
        Self {
            expect_cargo_toml_orig,
            crate_name: crate_name.into(),
            manifest: None,
            markup: None,
            features_comments: None,
            files: Vec::new(),
            lib_file: None,
            bin_file: None,
            stats: udedokei::Collect::new(),
            decompressed_size: 0,
            compressed_size,
            is_nightly: false,
            vcs_info_path: None,
            vcs_info_git_sha1: None,
            suspicious_binary_files: Vec::new(),
            readme_from_workspace: false,
        }
    }

    pub fn add(&mut self, relpath: PathBuf, size: u64, file_data: &mut dyn Read) -> Result<(), UnarchiverError> {
        let path_match = {
            match &relpath {
                p if p == Path::new("Cargo.toml") || p == Path::new("cargo.toml") => ReadAs::Toml,
                p if self.expect_cargo_toml_orig && (p == Path::new("Cargo.toml.orig") || p == Path::new("cargo.toml.orig")) => ReadAs::TomlOrig,
                p if p == Path::new(".cargo_vcs_info.json") => ReadAs::CargoVcsInfo,
                p if is_lib_filename(p, self.manifest.as_ref()) => {
                    if self.manifest.is_none() {
                        warn!("tarball had {} before seeing Cargo.toml", p.display());
                    }
                    ReadAs::Lib
                },
                p if is_bin_filename(p, self.manifest.as_ref()) => {
                    if self.manifest.is_none() {
                        warn!("tarball had {} before seeing Cargo.toml", p.display());
                    }
                    ReadAs::Bin
                },
                p if is_readme_filename(p, self.manifest.as_ref().and_then(|m| m.package.as_ref())) => {
                    if self.manifest.is_none() {
                        warn!("tarball had {} before seeing Cargo.toml", p.display());
                    }
                    let path_prefix = p.parent().unwrap().display().to_string();
                    let ext = p.extension().unwrap_or("".as_ref());
                    if ext == "rst" {
                        ReadAs::ReadmeRst(path_prefix)
                    } else if ext == "adoc" || ext == "asciidoc" {
                        ReadAs::ReadmeAsciiDoc(path_prefix)
                    } else {
                        ReadAs::ReadmeMarkdown(path_prefix)
                    }
                },
                // don't include test files in crate code size stats
                p if p.starts_with("tests") || p.starts_with("testing") || p.starts_with("test_assets") || p.starts_with("fixtures") || p.starts_with("benches") || p.starts_with("examples") => {
                    ReadAs::Irrelevant
                },
                p if p.starts_with("assets") || may_be_executable_path(p) => {
                    ReadAs::OtherFile
                },
                p => if let Some(lang) = is_source_code_file(p) {
                    if lang.is_code() {
                        ReadAs::GetStatsOfFile(lang)
                    } else {
                        ReadAs::Irrelevant
                    }
                } else {
                    ReadAs::Irrelevant
                },
            }
        };

        self.files.push(relpath);

        let last_file_path = self.files.last().map(|p| p.as_path()).unwrap_or("".as_ref());
        let module_file_stem = if last_file_path.file_name().unwrap_or_default() == "mod.rs" { last_file_path.parent().and_then(|s| s.file_name()) } else { last_file_path.file_stem() };
        let module_file_stem = module_file_stem.and_then(|s| s.to_str()).unwrap_or("");

        let content_is_irrelevant = matches!(path_match, ReadAs::Irrelevant | ReadAs::OtherFile);

        let size_limit = size.min(if content_is_irrelevant { 10_000 } else { MAX_FILE_SIZE });

        // Only count seemingly relevant files towards decompressed size
        if path_match != ReadAs::Irrelevant {
            self.decompressed_size += size;
        }

        let mut data = Vec::new();
        data.try_reserve(size_limit as usize).unwrap();
        file_data.take(size_limit).read_to_end(&mut data)?;

        let str_data = match String::from_utf8(data) {
            Ok(s) => s,
            Err(e) => {
                let bytes = &*e.into_bytes();
                if let Some(desc) = check_for_binaries(bytes, last_file_path) {
                    self.suspicious_binary_files.push((last_file_path.to_owned(), desc, size));
                    warn!("Found suspicious binary {desc} ({size}B) in {path_match:?} {}", last_file_path.display());
                    return Ok(());
                }
                debug!("{} ({size}B {path_match:?}) is not UTF-8 encoded", last_file_path.display());
                if content_is_irrelevant {
                    return Ok(());
                }

                if let Some(s) = string_from_utf16(bytes) {
                    s
                } else {
                    String::from_utf8_lossy(bytes).into_owned()
                }
            },
        };

        if str_data.trim_start().is_empty() {
            return Ok(());
        }

        match path_match {
            ReadAs::Lib => {
                self.stats.add_to_stats(udedokei::from_path("lib.rs").unwrap(), &self.crate_name, &str_data);
                if check_if_uses_nightly_features(&str_data) {
                    self.is_nightly = true;
                }
                self.lib_file = Some(str_data);
            },
            ReadAs::Bin => {
                self.stats.add_to_stats(udedokei::from_path("main.rs").unwrap(), &self.crate_name, &str_data);
                if check_if_uses_nightly_features(&str_data) {
                    self.is_nightly = true;
                }
                self.bin_file = Some(str_data);
            },
            ReadAs::CargoVcsInfo => {
                let vcs: CargoVcsInfo = serde_json::from_str(&str_data)?;
                self.vcs_info_path = vcs.path_in_vcs;
                self.vcs_info_git_sha1 = vcs.git.map(|g| g.sha1);
            },
            ReadAs::Toml => {
                self.manifest = Some(Manifest::from_str(&str_data)?);
                if !self.expect_cargo_toml_orig {
                    self.read_features_comments(&str_data);
                }
            },
            ReadAs::TomlOrig => {
                if let Ok(manifest) = Manifest::from_str(&str_data) {
                    // there's always an implied default set when it's not inherited
                    if manifest.package.as_ref().map_or(false, |p| !p.readme.is_set()) {
                        self.readme_from_workspace = true;
                    }
                }
                self.read_features_comments(&str_data);
            },
            ReadAs::ReadmeMarkdown(path_prefix) => {
                self.markup = Some((path_prefix, Markup::Markdown(str_data)));
            },
            ReadAs::ReadmeAsciiDoc(path_prefix) => {
                self.markup = Some((path_prefix, Markup::AsciiDoc(str_data)));
            },
            ReadAs::ReadmeRst(path_prefix) => {
                self.markup = Some((path_prefix, Markup::Rst(str_data)));
            },
            ReadAs::GetStatsOfFile(lang) => {
                self.stats.add_to_stats(lang, module_file_stem, &str_data);
            },
            ReadAs::Irrelevant | ReadAs::OtherFile => {}
        }
        Ok(())
    }

    #[inline(never)]
    fn read_features_comments(&mut self, data: &str) {
        let Ok(feature_comments) = DetailedFeatures::from_toml(data)
            .map_err(|e| error!("{}'s toml.orig: {e}", self.crate_name)) else { return };
        self.features_comments = Some(feature_comments);
    }

    fn finish(mut self) -> Result<CrateFilesSummary, UnarchiverError> {
        let mut manifest = match self.manifest {
            Some(m) => m,
            None => return Err(UnarchiverError::TomlNotFound(self.files.iter().map(|p| p.display().to_string()).collect::<Vec<_>>().join(", "))),
        };

        manifest.complete_from_abstract_filesystem::<(), _>(FilesFs(&self.files), None)?;

        // Something ends up reading symlinks as "../README.md" text!
        if self.markup.as_ref().map_or(false, |(_, markup)| match markup { Markup::Markdown(m) | Markup::Rst(m) | Markup::AsciiDoc(m) | Markup::Html(m) => { m.starts_with("../")} }) {
            debug!("Found broken symlink readme in {}", manifest.package().name());
            self.markup = None;
        }

        let readme = self.markup.map(|(rel_path, markup)| Readme {
            markup,
            rel_path: if self.readme_from_workspace { "".into() } else { rel_path }, // We don't know workspace path
            found_relevant: true,
            git_sha1: self.vcs_info_git_sha1,
        });

        let (language_stats, modules) = self.stats.finish();
        Ok(CrateFilesSummary {
            language_stats, modules,
            readme,
            manifest,
            decompressed_size: self.decompressed_size,
            compressed_size: self.compressed_size,
            files: self.files,
            lib_file: self.lib_file,
            bin_file: self.bin_file,
            is_nightly: self.is_nightly,
            path_in_repo: self.vcs_info_path,
            vcs_info_git_sha1: self.vcs_info_git_sha1,
            features_comments: self.features_comments,
            suspicious_binary_files: self.suspicious_binary_files.into_iter().map(|(path, kind, size)| {
                IdentifiedBinFile {
                    path,
                    size,
                    kind: kind.into(),
                }
            }).collect(),
        })
    }
}

fn string_from_utf16(bytes: &[u8]) -> Option<String> {
    if bytes.len() < 6 {
        return None;
    }
    let is_le = match bytes {
        [0xff, 0xfe, 9..=255, ..] | [9..=127, 00, 9..=127, 00, 9..=127, 00, ..] => true,
        [0xfe, 0xff, _, 9..=255, ..] | [00, 9..=127, 00, 9..=127, 00, 9..=127, ..] => false,
        _ => return None,
    };
    let mut out = String::new();
    out.try_reserve(bytes.len() / 2 + bytes.len() / 16).ok()?;
    out.extend(std::char::decode_utf16(bytes.chunks_exact(2).map(|ch| {
        let arr = ch.try_into().unwrap_or([0xff, 0xff]);
        if is_le { u16::from_le_bytes(arr) } else { u16::from_be_bytes(arr) }
    }))
    .map(|r| r.unwrap_or(char::REPLACEMENT_CHARACTER)));
    Some(out)
}

#[derive(Deserialize)]
struct CargoVcsInfo {
    git: Option<CargoVcsInfoGit>,
    path_in_vcs: Option<String>,
}

#[derive(Deserialize)]
struct CargoVcsInfoGit {
    #[serde(with = "hex")]
    sha1: [u8; 20],
}

struct FilesFs<'a>(&'a [PathBuf]);

impl cargo_toml::AbstractFilesystem for FilesFs<'_> {
    fn file_names_in(&self, dir: &str) -> io::Result<HashSet<Box<str>>> {
        Ok(self.0.iter().filter_map(|p| {
            p.strip_prefix(dir).ok()
        })
        .filter_map(|p| p.to_str())
        .map(From::from)
        .collect())
    }
}

fn check_if_uses_nightly_features(lib_source: &str) -> bool {
    lib_source.lines()
        .take(1000)
        .map(|line| line.find('"').map_or(line, |pos| &line[0..pos])) // half-assed effort to ignore feature in strings
        .map(|line| line.find("//").map_or(line, |pos| &line[0..pos])) // half-assed effort to remove comments
        .any(|line| line.contains("#![feature("))
}

fn is_source_code_file(path: &Path) -> Option<udedokei::Language> {
    if path.starts_with("tests") || path.starts_with("benches") || path.starts_with("docs") || path.starts_with("examples") {
        return None;
    }
    udedokei::from_path(path)
}

impl CrateFilesSummary {
    /// Checks whether tarball contained given file path,
    /// relative to project root.
    pub fn has(&self, path: impl AsRef<Path>) -> bool {
        let path = path.as_ref();
        self.files.iter().any(|p| p == path)
    }
}

fn is_lib_filename(path: &Path, manifest: Option<&Manifest>) -> bool {
    let expected_path = if let Some(lib_path) = manifest.and_then(|m| m.lib.as_ref()).and_then(|l| l.path.as_ref()) {
        Path::new(lib_path)
    } else {
        Path::new("src/lib.rs")
    };
    path == expected_path

}

fn is_bin_filename(path: &Path, manifest: Option<&Manifest>) -> bool {
    if let Some(bins) = manifest.map(|m| &m.bin) {
        if bins.iter().filter_map(|p| p.path.as_ref()).any(|bin_path| Path::new(bin_path) == path) {
            return true;
        }
    }
    path == Path::new("src/main.rs")
}

/// Yes, this is flimsy. Malicious crate authors can run arbitrary executable code anyway
fn may_be_executable_path(path: &Path) -> bool {
    let Some(mut file_name) = path.file_name().and_then(|f| f.to_str()) else { return true };
    if let Some((rest, ext)) = file_name.rsplit_once('.') {
        if matches!(ext, "gz" | "bz2" | "xz" | "1" | "0") {
            file_name = rest;
        }
    }
    if let Some((_, ext)) = file_name.rsplit_once('.') {
        matches!(ext, "so" | "dll" | "exe" | "lib" | "rlib" | "a" | "com" | "dylib" | "zip" | "cab" | "msi" | "deb")
    } else {
        // fragments of rust's triples
        file_name.contains("x86_64") || file_name.contains("i686") || file_name.contains("aarch64")
    }
}

/// Check if given filename is a README. If `package` is missing, guess.
fn is_readme_filename(path: &Path, package: Option<&Package>) -> bool {
    path.to_str().map_or(false, |pathstr| {
        if let Some(p) = package {
            if p.readme.is_set() { // broken inheritance
                if let Some(r) = p.readme().as_path().and_then(|r| r.to_str()) {
                    // packages put ./README which doesn't match README
                    return r.trim_start_matches('.').trim_start_matches('/') == pathstr
                }
            }
        }
        render_readme::is_readme_filename(path)
    })
}

fn ungzip_fragment(data: &[u8]) -> Option<Vec<u8>> {
    let decoder = gzip::Decoder::new(data).ok()?;
    let mut decoded_data = Vec::new();
    decoder.take(1000).read_to_end(&mut decoded_data).ok()?;
    Some(decoded_data)
}

fn inflate_fragment(data: &[u8]) -> Option<Vec<u8>> {
    let decoder = deflate::Decoder::new(data);
    let mut decoded_data = Vec::new();
    decoder.take(1000).read_to_end(&mut decoded_data).ok()?;
    Some(decoded_data)
}

fn check_for_binaries(data: &[u8], path: &Path) -> Option<&'static str> {
    if data.get(..2) == Some(b"\x1F\x8B") {
        if let Some(d) = ungzip_fragment(data) {
            if let ok @ Some(_) = check_for_binaries_inner(&d, path) {
                return ok;
            }
        }
    }
    if let ok @ Some(_) = check_for_binaries_inner(data, path) {
        return ok;
    }
    if let Some(d) = inflate_fragment(data) {
        if let ok @ Some(_) = check_for_binaries_inner(&d, path) {
            return ok;
        }
    }
    None
}

fn check_for_binaries_inner(data: &[u8], path: &Path) -> Option<&'static str> {
    let ext = path.extension().and_then(|e| e.to_str()).unwrap_or_default();
    Some(match data {
        [0xCE,0xFA,0xED,0xFE,..] |
        [0xCF,0xFA,0xED,0xFE,..] |
        [0xfe,0xca,0xbe,0xba,..] |
        [0xfa,0xcf,0xfe,0xed,..] => match ext {
            "dylib" => "Mach-o library",
            _ => "Mach-o exe",
        },
        [0x7F,0x45,0x4C,0x46, ..] => match ext {
            "so" | "0" | "1" | "solib" | "dylib" | "cdylib" => "ELF lib",
            _ if path.starts_with("bin") || path.starts_with("bins") || path.starts_with("sbin") => "ELF exe",
            _ => "ELF exe/lib",
        },
        [0x50,0x4B,3|5|7,4|6|8, ..] => match ext {
            "xlsx" | "xlsm" | "pptx" | "docx" | "odt" | "ods" | "odg" | "odp" | "odf" | "kmz" |
            "epub" | "cbz" | "sketch" | "xmind" | "tdesktop-theme" | "npz" | "key" | "numbers" | "pages" |
            "procreate" | "cdr" |"zip" => return None,
            "jar" => "JAR file",
            "apk" => "APK file",
            "whl" => "Python package",
            _ => "Zip file",
        },
        [0x4D,0x5A, ..] => {
            if ext == "dll" || ext == "lib" {
                "Windows DLL"
            } else if data.windows(3).take(100).any(|w| w == [0x50,0x45,0]) {
                "Windows exe"
            } else {"DOS exe"}
        },
        [0xD0,0xCF,0x11,0xE0, ..] => match ext {
            "db" | "doc" | "ppt" | "xls" => return None,
            "msm" | "msi" => "MSI installer",
            _ => "Cab file"
        },
        [b'!',b'<',b'a',b'r',b'c',b'h',b'>', ..] => match ext {
            "a" | "lib" => "static library",
            "rlib" => "rust library",
            "deb" => "debian package",
            _ => "ar library",
        },
        _ => return None,
    })
}

#[test]
fn unpack_crate() {
    let k = include_bytes!("../test.crate");
    let d = read_archive(&k[..], "testing", "1.0.0").unwrap();
    assert_eq!(d.manifest.package.as_ref().unwrap().name, "crates-server");
    assert_eq!(d.manifest.package.as_ref().unwrap().version(), "0.5.1");
    assert!(d.lib_file.unwrap().contains("fn nothing"));
    assert_eq!(d.files.len(), 5);
    assert!(match d.readme.unwrap().markup {
        Markup::Rst(a) => a == "o hi\n",
        _ => false,
    });
    assert_eq!(d.language_stats.langs.get(&udedokei::Language::Rust).unwrap().code, 1);
    assert_eq!(d.language_stats.langs.get(&udedokei::Language::C).unwrap().code, 1);
    assert_eq!(d.language_stats.langs.get(&udedokei::Language::JavaScript).unwrap().code, 0);
    assert!(d.language_stats.langs.get(&udedokei::Language::Bash).is_none());
    assert_eq!(d.decompressed_size, 161);
}

#[test]
fn unpack_repo() {
    use repo_url::Repo;
    let test_repo_path = Path::new(env!("CARGO_MANIFEST_DIR")).join("test.repo");
    let repo = Repo::new("http://example.invalid/foo.git").unwrap();
    let checkout = crate_git_checkout::checkout(&repo, &test_repo_path, false).unwrap();
    let f = crate_git_checkout::path_in_repo(&checkout, "crates-server", &AtomicU32::new(0)).unwrap().unwrap();
    let tree_id = f.tree;
    let manifest = f.manifest;

    let d = read_repo(&checkout, tree_id, "test", &AtomicU32::new(0)).unwrap();
    assert_eq!(d.manifest.package, manifest.package);

    assert_eq!(d.manifest.package.as_ref().unwrap().name, "crates-server");
    assert_eq!(d.manifest.package.as_ref().unwrap().version(), "0.5.1");
    assert!(d.lib_file.unwrap().contains("fn nothing"));
    assert_eq!(d.files.len(), 5);
    assert!(match d.readme.unwrap().markup {
        Markup::Rst(a) => a == "o hi\n",
        _ => false,
    });
    assert_eq!(d.language_stats.langs.get(&udedokei::Language::Rust).unwrap().code, 1);
    assert_eq!(d.language_stats.langs.get(&udedokei::Language::C).unwrap().code, 1);
    assert_eq!(d.language_stats.langs.get(&udedokei::Language::JavaScript).unwrap().code, 0);
    assert!(d.language_stats.langs.get(&udedokei::Language::Bash).is_none());
    assert_eq!(d.decompressed_size, 161);
}
