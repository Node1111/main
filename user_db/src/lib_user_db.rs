use util::CowAscii;
use ahash::HashMap;
use github_info::User;
use github_info::UserType;
use log::info;
use parking_lot::Mutex;
use rich_crate::CrateOwner;
use rich_crate::Origin;
use rusqlite::OptionalExtension;
use rusqlite::types::ToSql;
use rusqlite::{Connection, Error, Row, Transaction};
use std::path::Path;
use std::time::SystemTime;
use util::SmolStr;

type Result<T, E = rusqlite::Error> = std::result::Result<T, E>;

mod schema;

pub struct UserDb {
    pub(crate) conn: Mutex<Connection>,
}

impl UserDb {
    pub fn new<P: AsRef<Path>>(path: P) -> Result<Self> {
        let db = Self::db(path.as_ref())?;
        Ok(Self {
            conn: Mutex::new(db),
        })
    }

    pub fn email_has_github(&self, email: &str) -> Result<bool> {
        let conn = self.conn.lock();
        let mut get_email = conn.prepare_cached("SELECT 1 FROM github_emails WHERE email = ?1 LIMIT 1")?;
        get_email.exists([&&*email.as_ascii_lowercase()])
    }

    pub fn user_by_github_login(&self, login: &str) -> Result<Option<User>> {
        self.user_by_github_login_opt(login, false)
    }

    pub fn user_by_github_login_opt(&self, login: &str, allow_stale: bool) -> Result<Option<User>> {
        let res = self.user_by_query(r"SELECT
                u.id,
                u.login,
                u.name,
                u.avatar_url,
                u.gravatar_id,
                u.html_url,
                u.type,
                u.two_factor_authentication,
                u.created_at,
                u.blog,
                COALESCE(u.login_case, u.login),
                u.fetched_timestamp
            FROM github_users u
            WHERE u.id = (SELECT i.id FROM github_users i WHERE login = ?1 ORDER BY i.fetched_timestamp DESC, i.created_at DESC LIMIT 1)
            ORDER BY u.fetched_timestamp DESC, u.created_at DESC
            LIMIT 1", &login.as_ascii_lowercase());
        Ok(if let Some((fetched_at, user)) = res? {
            if allow_stale || fetched_at + 3600*24*12 > SystemTime::UNIX_EPOCH.elapsed().unwrap().as_secs() {
                Some(user)
            } else {
                info!("ignoring stale gh user data for {}", user.login);
                None
            }
        } else { None })
    }

    fn user_by_query(&self, query: &str, arg: &dyn ToSql) -> Result<Option<(u64, User)>> {
        let conn = self.conn.lock();
        let mut get_user = conn.prepare_cached(query)?;
        let mut res = get_user.query_map([arg], Self::read_user_row)?;
        res.next().transpose()
    }

    /// Timestamp + user
    fn read_user_row(row: &Row) -> Result<(u64, User), Error> {
        let fetched_at = row.get_ref(11)?
            .as_i64_or_null()?.unwrap_or(0) as u64;
        let mut login = row.get_ref(10)?.as_str_or_null()?.map(Ok::<_, Error>)
            .unwrap_or_else(|| Ok(row.get_ref(1)?.as_str()?))?;
        let name = row.get_ref(2)?.as_str_or_null()?
            .map(|n| n.trim())
            .filter(|n| !n.is_empty());
        let html_url: Box<str> = row.get(5)?;
        if login.bytes().all(|b| b.is_ascii_lowercase()) {
            let url_login = html_url.strip_prefix("https://github.com/").and_then(|l| l.split('/').next())
                .filter(|url_login| url_login.eq_ignore_ascii_case(login));
            if let Some(url_login) = url_login {
                login = url_login;
            } else if let Some(name) = name {
                if name.eq_ignore_ascii_case(login) {
                    login = name;
                }
            }
        }
        Ok((fetched_at, User {
            id: row.get(0)?,
            login: login.into(),
            name: name.map(From::from),
            avatar_url: row.get_unwrap::<_, Option<Box<str>>>(3).filter(|n| !n.is_empty()),
            gravatar_id: row.get_unwrap::<_, Option<Box<str>>>(4).filter(|n| !n.is_empty()),
            html_url,
            user_type: match row.get_ref_unwrap(6).as_str().unwrap() {
                "org" => UserType::Org,
                "bot" => UserType::Bot,
                _ => UserType::User,
            },
            two_factor_authentication: row.get_unwrap(7),
            created_at: row.get_unwrap::<_, Option<Box<str>>>(8).filter(|n| !n.is_empty()),
            blog: row.get_unwrap::<_, Option<Box<str>>>(9).filter(|n| !n.is_empty()),
        }))
    }

    pub fn index_outdated_login(&self, old_login: &str, current_id: u64) -> Result<(), Error> {
        let conn = self.conn.lock();
        let mut insert_bad = conn.prepare_cached("INSERT OR IGNORE INTO github_users(id, login, fetched_timestamp) VALUES(?1, ?2, 1)")?;
        insert_bad.execute((current_id, &*old_login.as_ascii_lowercase()))?;
        Ok(())
    }

    /// Not possible via GitHub API any more
    pub fn login_by_github_id(&self, id: u64) -> Result<SmolStr> {
        let conn = self.conn.lock();
        let mut get_user = conn.prepare_cached(r"SELECT COALESCE(login_case, login) FROM github_users WHERE id = ?1
            ORDER BY fetched_timestamp DESC, created_at DESC
            LIMIT 1")?;
        get_user.query_row([&id], |row| Ok(SmolStr::from(row.get_ref(0)?.as_str()?)))
    }

    pub fn user_by_email(&self, email: &str) -> Result<Option<User>> {
        let std_suffix = "@users.noreply.github.com";
        if let Some(rest) = email.strip_suffix(std_suffix) {
            // it's id+login@users
            let mut parts = rest.split('+');
            if let Some(id) = parts.next().and_then(|id| id.parse().ok()) {
                if let Ok(login) = self.login_by_github_id(id) {
                    // don't lose reliable association only because other metadata is stale
                    if let ok @ Ok(Some(_)) = self.user_by_github_login_opt(&login, true) {
                        return ok;
                    }
                }
                if let Some(login) = parts.next() {
                    if let ok @ Ok(Some(_)) = self.user_by_github_login_opt(login, true) {
                        return ok;
                    }
                }
            }
        }
        if let Some(u) = self.user_by_email_inner(email)? {
            return Ok(Some(u));
        }
        if let Some(fallback) = unplussed(email) {
            return self.user_by_email_inner(&fallback);
        }
        Ok(None)
    }

    fn user_by_email_inner(&self, email: &str) -> Result<Option<User>> {
        let res = self.user_by_query(r"SELECT
                u.id,
                u.login,
                COALESCE(u.name, e.name) as name,
                u.avatar_url,
                u.gravatar_id,
                u.html_url,
                u.type,
                u.two_factor_authentication,
                u.created_at,
                u.blog,
                COALESCE(u.login_case, u.login),
                u.fetched_timestamp
            FROM github_emails e
            JOIN github_users u ON e.github_id = u.id
            WHERE email = ?1
            ORDER BY u.fetched_timestamp DESC, u.created_at DESC
            LIMIT 1", &email.as_ascii_lowercase());
        // must allow stale data, beacuse the caller won't know which user to refresh!
        Ok(res?.map(|(_, u)| u))
    }

    pub fn index_users(&self, users: &[User], fetched_at: SystemTime, is_db_dump: bool) -> Result<()> {
        let mut conn = self.conn.lock();
        let tx = conn.transaction()?;
        Self::insert_users_inner(&tx, users, fetched_at, is_db_dump)?;
        tx.commit()?;
        Ok(())
    }

    fn insert_users_inner(tx: &Transaction, users: &[User], fetched_at: SystemTime, is_db_dump: bool) -> Result<(), Error> {
        // is_db_dump is for crates-io datadump, which may be stale, as well as other guessed data sources
        let mut insert_user = tx.prepare_cached(if !is_db_dump {
            "INSERT INTO github_users (
                id, login, name, avatar_url, gravatar_id, html_url, type, two_factor_authentication, created_at, blog, login_case, fetched_timestamp)
                VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12)
                ON CONFLICT(id, login) DO UPDATE SET
                login = excluded.login,
                login_case = excluded.login_case,
                name = COALESCE(excluded.name, name),
                avatar_url = COALESCE(excluded.avatar_url, avatar_url),
                gravatar_id = COALESCE(excluded.gravatar_id, gravatar_id),
                html_url = excluded.html_url,
                type = excluded.type,
                two_factor_authentication = COALESCE(excluded.two_factor_authentication, two_factor_authentication),
                created_at = COALESCE(excluded.created_at, created_at),
                fetched_timestamp = excluded.fetched_timestamp,
                blog = COALESCE(excluded.blog, blog)
                WHERE excluded.fetched_timestamp > fetched_timestamp
                "}
            else {
                "INSERT OR IGNORE INTO github_users (
                id, login, name, avatar_url, gravatar_id, html_url, type, two_factor_authentication, created_at, blog, login_case, fetched_timestamp)
                VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12)
            "})?;
        let timestamp = fetched_at.duration_since(SystemTime::UNIX_EPOCH).ok().unwrap_or_default().as_secs();
        for user in users {
            let login_lowercase = &*user.login.as_ascii_lowercase();
            let args: &[&dyn ToSql] = &[
                &user.id,
                &login_lowercase,
                &user.name.as_deref().map(|n| n.trim()).filter(|n| !n.eq_ignore_ascii_case(login_lowercase)),
                &user.avatar_url.as_deref().filter(|n| !n.is_empty()),
                &user.gravatar_id.as_deref().filter(|n| !n.is_empty()),
                &user.html_url,
                &match user.user_type {
                    UserType::User => "user",
                    UserType::Org => "org",
                    UserType::Bot => "bot",
                },
                &user.two_factor_authentication,
                &user.created_at.as_deref().filter(|n| !n.is_empty()),
                &user.blog.as_deref().filter(|n| !n.is_empty()),
                &(user.login != login_lowercase).then_some(user.login.as_str()),
                &timestamp,
            ];
            insert_user.execute(args)?;
        }
        Ok(())
    }

    pub fn index_user(&self, user: &User, email: Option<&str>, name: Option<&str>, fetched_at: SystemTime, low_quality_data: bool) -> Result<()> {
        let mut conn = self.conn.lock();
        let tx = conn.transaction()?;
        {
            Self::insert_users_inner(&tx, std::slice::from_ref(user), fetched_at, low_quality_data)?;
            if let Some(e) = email {
                let mut insert_email = tx.prepare_cached("INSERT OR REPLACE INTO github_emails (
                    github_id, email, name) VALUES (?1, ?2, ?3)")?;
                let name = name.map(|n| n.trim()).filter(|n| !n.is_empty() && !n.eq_ignore_ascii_case(&user.login));
                let email = e.as_ascii_lowercase();
                let email = &*email;
                let args: &[&dyn ToSql] = &[&user.id, &email, &name];
                insert_email.execute(args)?;

                if let Some(plain) = unplussed(email) {
                    let args: &[&dyn ToSql] = &[&user.id, &plain.as_str(), &name];
                    insert_email.execute(args)?;
                }
            }
        }
        tx.commit()?;
        Ok(())
    }

    pub fn set_user_crate_relevance(&self, github_id: u32, crates: &HashMap<Origin, f64>) -> Result<()> {
        let mut conn = self.conn.lock();
        let tx = conn.transaction()?;
        {
            let mut insert = tx.prepare_cached("INSERT OR REPLACE INTO owner_crates_relevance(github_id, origin, relevance) VALUES(?1,?2,?3)")?;
            for (origin, &relevance) in crates {
                assert!(relevance.is_finite());
                if relevance < 0.00001 { continue; }
                insert.execute((github_id, origin.to_str(), relevance))?;
            }
        }
        tx.commit()?;
        Ok(())
    }

    pub fn set_user_dependency_trust(&self, github_id: u32, deps: &HashMap<SmolStr, f64>) -> Result<()> {
        let mut conn = self.conn.lock();
        let tx = conn.transaction()?;
        {
            let mut delete = tx.prepare_cached("DELETE FROM owner_dependency_trust WHERE github_id = ?1")?;
            let mut insert = tx.prepare_cached("INSERT OR REPLACE INTO owner_dependency_trust(github_id, origin, trust) VALUES(?1,?2,?3)")?;
            delete.execute([github_id])?;
            for (name, &trust) in deps {
                assert!(trust.is_finite());
                if trust < 0.00001 { continue; }
                insert.execute((github_id, Origin::from_crates_io_name(name).to_str(), trust))?;
            }
        }
        tx.commit()?;
        Ok(())
    }

    pub fn trust_for_owners(&self, owners: &[CrateOwner]) -> Result<Vec<f64>> {
        let conn = self.conn.lock();
        let mut query = conn.prepare_cached("SELECT rank3 FROM owner_rank WHERE github_id = ?1")?;

        owners.iter().map(|o| {
            let Some(id) = o.github_id else { return Ok(0.); };
            Ok(query.query_row([id], |row| row.get(0)).optional()?.unwrap_or(0.))
        })
        .collect()
    }

    pub fn set_coauthor_trust(&self, github_id: u32, users: &HashMap<u32, f64>) -> Result<()> {
        let mut conn = self.conn.lock();
        let tx = conn.transaction()?;
        {
            let mut delete = tx.prepare_cached("DELETE FROM owner_coauthor_trust WHERE github_id = ?1")?;
            let mut insert = tx.prepare_cached("INSERT OR REPLACE INTO owner_coauthor_trust(github_id, other_github_id, trust) VALUES(?1,?2,?3)")?;
            delete.execute([github_id])?;
            for (&other_github_id, &trust) in users {
                assert!(trust.is_finite());
                if trust < 0.00001 { continue; }
                insert.execute((github_id, other_github_id, trust))?;
            }
        }
        tx.commit()?;
        Ok(())
    }

    pub fn set_owner_ranking_score(&self, github_id: u32, rank1: f64) -> Result<()> {
        assert!(rank1.is_finite());

        let mut conn = self.conn.lock();
        let tx = conn.transaction()?;
        {
            let mut insert = tx.prepare_cached("INSERT OR REPLACE INTO owner_rank(github_id, rank1, rank2, rank3) VALUES(?1, ?2, ?3, ?4)")?;
            let mut rank23 = tx.prepare_cached("SELECT
                count(*) as cnt,
                COALESCE(sum(o.rank1 * trust), 0) as rank1_given,
                COALESCE(sum(COALESCE(o.rank2, 0) * trust), 0) as rank2_given
                FROM owner_coauthor_trust ct
                JOIN owner_rank o on o.github_id = ct.github_id
                WHERE ct.other_github_id = ?1")?;
            let (user_count, rank1_given, rank2_given): (i32, f64, f64) =
                rank23.query_row([github_id], |row| Ok((row.get_unwrap(0), row.get_unwrap(1), row.get_unwrap(2))))
                    .optional()?.unwrap_or_default();

            // rank1 is from users' own properties (like orgs, account age)
            // rank2 is rank1 spread through co-author trust relationships,
            // rank3 is rank2 spread through co-author trust relationships.
            //
            // rank2 is also multiplied by the numer of users to bias towards popular accounts (assuming it's easier to trick 1 person into trusting, than a whole crowd)
            // sqrt to dampen the winner-takes-all power curve.
            // the value is unbounded, scaled by a fudge factor to roughly similar range as original rank.
            let popularity_factor = (f64::from(user_count.min(10)) / 10.) + f64::from(user_count).sqrt() / 20.;
            let rank2 = rank1 + rank1_given * popularity_factor;
            // rank3 is the trust relationship again, two levels removed. full page-rank doesn't make sense here, as trust isn't perfectly transitive.
            // the max is because not all users collaborate, but may still be trusted themselves.
            // At this point it can loop (user_a -> user_b -> user_a) and amplify itself, so it's capped.
            let rank3 = rank2 + rank2_given.min(rank2 + 100.) / 2.;
            insert.execute((github_id, rank1, rank2, rank3))?;
        }
        tx.commit()?;
        Ok(())
    }
}

fn unplussed(email: &str) -> Option<String> {
    let (u, rest) = email.split_once('+')?;
    let host = rest.split_once('@')?.1;
    Some(format!("{u}@{host}"))
}

#[test]
fn gh_userdb() {
    let _ = std::fs::remove_file("/tmp/userdbtest6.db");
    let u = UserDb::new("/tmp/userdbtest6.db").unwrap();
    u.index_users(&[User {
        id: 1,
        login: "HELLO".into(),
        name: None,
        avatar_url: None,
        gravatar_id: None,
        html_url: "bla".into(),
        blog: None,
        two_factor_authentication: Some(false),
        user_type: UserType::Org,
        created_at: None,
    }], SystemTime::now() - std::time::Duration::from_secs(3600), true).unwrap();
    u.index_users(&[User {
        id: 2345,
        login: "second_user".into(),
        name: None,
        avatar_url: None,
        gravatar_id: None,
        html_url: "bla".into(),
        blog: None,
        two_factor_authentication: Some(false),
        user_type: UserType::Org,
        created_at: None,
    }], SystemTime::now(), true).unwrap();

    assert_eq!("HELLO", u.login_by_github_id(1).unwrap());
    let res = u.user_by_github_login("HellO").unwrap().unwrap();
    assert_eq!("second_user", u.login_by_github_id(2345).unwrap());
    assert_eq!("second_user", u.user_by_github_login("second_user").unwrap().unwrap().login);

    assert_eq!(1, res.id);
    assert_eq!("bla", &*res.html_url);
    assert_eq!(UserType::Org, res.user_type);
    assert_eq!(Some(false), res.two_factor_authentication);

    u.index_users(&[User {
        id: 1,
        login: "HELlO".into(),
        name: None,
        avatar_url: None,
        gravatar_id: None,
        html_url: "bla2".into(),
        blog: None,
        two_factor_authentication: None,
        user_type: UserType::User,
        created_at: Some("2020-02-20".into()),
    }], SystemTime::now() - std::time::Duration::from_secs(60), false).unwrap();
    let res = u.user_by_github_login("HellO").unwrap().unwrap();

    assert_eq!(1, res.id);
    assert_eq!("bla2", &*res.html_url);
    assert_eq!(UserType::User, res.user_type);
    assert_eq!(Some(false), res.two_factor_authentication);
    assert_eq!("2020-02-20", res.created_at.as_deref().unwrap());

    assert_eq!("HELlO", u.login_by_github_id(1).unwrap());

    u.index_users(&[User {
        id: 1,
        login: "ruined login".into(),
        name: None,
        avatar_url: None,
        gravatar_id: None,
        html_url: "wrong".into(),
        blog: None,
        two_factor_authentication: None,
        user_type: UserType::User,
        created_at: Some("2030-02-20".into()),
    }], SystemTime::now() - std::time::Duration::from_secs(3600 * 2), false).unwrap();
    assert_eq!("HELlO", u.login_by_github_id(1).unwrap());

    u.index_users(&[User {
        id: 1,
        login: "changed_login".into(),
        name: None,
        avatar_url: None,
        gravatar_id: None,
        html_url: "bla2".into(),
        blog: None,
        two_factor_authentication: None,
        user_type: UserType::User,
        created_at: Some("2020-02-20".into()),
    }], SystemTime::now(), false).unwrap();
    assert_eq!("changed_login", u.login_by_github_id(1).unwrap());

    assert_eq!("second_user", u.login_by_github_id(2345).unwrap());
    assert_eq!(2345, u.user_by_github_login("second_user").unwrap().unwrap().id);
}
