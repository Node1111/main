use crate::UserDb;
use rusqlite::*;
use std::path::Path;


impl UserDb {
    pub(crate) fn db(path: &Path) -> Result<Connection> {
        let conn = Connection::open(path)?;
        conn.execute_batch(r#"
            BEGIN;
            CREATE TABLE IF NOT EXISTS github_users (
                id            INTEGER NOT NULL,
                login         TEXT NOT NULL, -- lowercase
                login_case    TEXT, -- case-preserving
                name          TEXT,
                avatar_url    TEXT,
                gravatar_id   TEXT,
                html_url      TEXT,
                type          TEXT NOT NULL DEFAULT 'user',
                two_factor_authentication INTEGER,
                blog       TEXT,
                created_at TEXT,
                fetched_timestamp INTEGER NOT NULL DEFAULT 0,
                PRIMARY KEY(id, login)
            );
            CREATE INDEX IF NOT EXISTS github_login_idx on github_users(login);

            CREATE TABLE IF NOT EXISTS github_emails (
                github_id     INTEGER NOT NULL,
                email         TEXT NOT NULL,
                name          TEXT
            );
            CREATE UNIQUE INDEX IF NOT EXISTS github_emails_idx on github_emails(github_id, email);
            CREATE INDEX IF NOT EXISTS github_emails_email_idx on github_emails(email);

            -- how important are crates to their owner
            CREATE TABLE IF NOT EXISTS owner_crates_relevance (
                github_id INTEGER NOT NULL,
                origin TEXT NOT NULL,
                relevance FLOAT NOT NULL,
                PRIMARY KEY (github_id, origin)
            );

            -- how relevant and potentially trusted are other crates used by this user
            CREATE TABLE IF NOT EXISTS owner_dependency_trust (
                github_id INTEGER NOT NULL,
                origin TEXT NOT NULL,
                trust FLOAT NOT NULL,
                PRIMARY KEY (origin, github_id)
            );
            CREATE INDEX IF NOT EXISTS owner_dependency_trust_r_idx on owner_dependency_trust(github_id);

            -- how trusted are other crate owners, by this user
            CREATE TABLE IF NOT EXISTS owner_coauthor_trust (
                github_id INTEGER NOT NULL,
                other_github_id INTEGER NOT NULL,
                trust FLOAT NOT NULL,
                PRIMARY KEY (github_id, other_github_id)
            );
            CREATE INDEX IF NOT EXISTS owner_coauthor_trust_r_idx on owner_coauthor_trust(other_github_id);

            CREATE TABLE IF NOT EXISTS owner_rank (
                github_id INTEGER NOT NULL PRIMARY KEY,
                rank1 INTEGER NOT NULL,
                rank2 INTEGER,
                rank3 INTEGER
            );

            COMMIT;"#)?;
        Ok(conn)
    }
}

