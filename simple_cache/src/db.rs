use blocking::block_in_place;
use brotli::BrotliCompress;
use brotli::BrotliDecompress;
use crate::error::Error;
use fetcher::Fetcher;
use rusqlite::Connection;
use rusqlite::Error::SqliteFailure;
use rusqlite::ErrorCode::DatabaseLocked;
use rusqlite::types::ToSql;
use std::path::Path;
use std::sync::Arc;
use std::thread;
use std::time::Duration;
use thread_local::ThreadLocal;

#[derive(Debug)]
pub struct SimpleCache {
    url: String,
    conn: ThreadLocal<Result<Connection, Arc<rusqlite::Error>>>,
    pub cache_only: bool,
    compress: bool,
}

#[derive(Debug)]
pub struct SimpleFetchCache {
    cache: SimpleCache,
    fetcher: Arc<Fetcher>,
}

impl SimpleFetchCache {
    pub fn new(db_path: impl AsRef<Path>, fetcher: Arc<Fetcher>, compress: bool) -> Result<Self, Error> {
        Ok(Self {
            cache: SimpleCache::new(db_path, compress)?,
            fetcher,
        })
    }

    pub async fn get_json<B>(&self, key: (&str, &str), url: impl AsRef<str>) -> Result<Option<B>, Error>
    where B: for<'a> serde::Deserialize<'a> {
        if let Some(data) = self.get_cached(key, url).await? {
            match serde_json::from_slice(&data) {
                Ok(res) => Ok(Some(res)),
                Err(parse) => Err(Error::Parse(parse, data)),
            }
        } else {
            Ok(None)
        }
    }

    pub async fn get_cached(&self, key: (&str, &str), url: impl AsRef<str>) -> Result<Option<Vec<u8>>, Error> {
        Ok(if let Some(data) = self.cache.get(key)? {
            Some(data)
        } else if self.cache.cache_only {
            None
        } else {
            let data = Box::pin(self.fetcher.fetch(url.as_ref())).await?;
            self.cache.set(key, &data)?;
            Some(data)
        })
    }


    pub fn set_serialize<B: serde::Serialize + serde::de::DeserializeOwned>(&self, key: (&str, &str), value: &B) -> Result<(), Error> {
        self.cache.set_serialize(key, value)
    }

    #[inline]
    pub fn get_deserialized<B: serde::de::DeserializeOwned>(&self, key: (&str, &str)) -> Result<Option<B>, Error> {
        self.cache.get_deserialized(key)
    }

    pub async fn fetch_cached_deserialized<B: serde::de::DeserializeOwned + serde::Serialize>(&self, key: (&str, &str), url: impl AsRef<str>) -> Result<Option<B>, Error> {
        if let Some(data) = self.get_deserialized(key)? {
            Ok(Some(data))
        } else if self.cache.cache_only {
            Ok(None)
        } else {
            let data = Box::pin(self.fetcher.fetch(url.as_ref())).await?;
            let res = serde_json::from_slice(&data).map_err(|e| Error::Parse(e, data))?;
            self.cache.set_serialize(key, &res)?;
            Ok(Some(res))
        }
    }

    pub fn set_cache_only(&mut self, val: bool) {
        self.cache.cache_only = val;
    }
}

impl SimpleCache {
    #[inline(never)]
    pub fn new(db_path: impl AsRef<Path>, compress: bool) -> Result<Self, Error> {
        Ok(Self {
            url: db_path.as_ref().to_str().unwrap().to_string(),
            conn: ThreadLocal::new(),
            cache_only: false,
            compress,
        })
    }

    #[inline(never)]
    fn connect(&self) -> Result<Connection, rusqlite::Error> {
        let conn = Connection::open(&self.url)?;
        conn.execute_batch("
            CREATE TABLE IF NOT EXISTS cache2 (key TEXT NOT NULL PRIMARY KEY, ver TEXT NOT NULL, data BLOB NOT NULL);
            PRAGMA read_uncommitted;",
        )?;
        Ok(conn)
    }

    #[inline]
    fn with_connection<F, T>(&self, cb: F) -> Result<T, Error> where F: FnOnce(&Connection) -> Result<T, Error> {
        block_in_place("dbkvconn", || {
            let conn = self.conn.get_or(|| self.connect().map_err(Arc::new));
            match conn {
                Ok(conn) => cb(conn),
                Err(err) => Err(Error::Db(self.url.clone(), Arc::clone(err))),
            }
        })
    }

    #[inline(never)]
    pub fn get(&self, key: (&str, &str)) -> Result<Option<Vec<u8>>, Error> {
        let span = tracing::debug_span!("get", k=key.0, v=key.1);
        let _s = span.enter();
        Self::with_retries(&format!("get {key:?}"), || self.get_inner(key))
    }

    fn with_retries<T>(label: &str, mut cb: impl FnMut() -> Result<T, Error>) -> Result<T, Error> {
        let mut retries = 5;
        loop {
            match block_in_place(label, &mut cb) {
                Err(Error::Db(_, ref e)) if retries > 0 && matches!(**e, SqliteFailure(ref e, _) if e.code == DatabaseLocked) => {
                    eprintln!("Retrying: {e}");
                    retries -= 1;
                    block_in_place("sleep", || thread::sleep(Duration::from_secs(1)));
                },
                err => return err,
            }
        }
    }

    fn get_inner(&self, key: (&str, &str)) -> Result<Option<Vec<u8>>, Error> {
        self.with_connection(|conn| {
            let mut q = conn.prepare_cached("SELECT data FROM cache2 WHERE key = ?1 AND ver = ?2")
                .map_err(|e| Error::Db(self.url.clone(), Arc::new(e)))?;
            let row: Result<Vec<u8>, _> = q.query_row([key.0, key.1], |r| r.get(0));
            match row {
                Ok(row) => Ok(Some(row)),
                Err(rusqlite::Error::QueryReturnedNoRows) => Ok(None),
                Err(err) => Err(Error::Db(self.url.clone(), Arc::new(err))),
            }
        })
    }

    #[inline(never)]
    pub fn delete(&self, key: (&str, &str)) -> Result<(), Error> {
        self.with_connection(|conn| {
            let mut q = conn.prepare_cached("DELETE FROM cache2 WHERE key = ?1")
                .map_err(|e| Error::Db(self.url.clone(), Arc::new(e)))?;
            q.execute([key.0])
                .map_err(|e| Error::Db(self.url.clone(), Arc::new(e)))?;
            Ok(())
        })
    }

    #[inline(never)]
    pub fn set_serialize<B: serde::Serialize + serde::de::DeserializeOwned>(&self, key: (&str, &str), value: &B) -> Result<(), Error> {
        let serialized = rmp_serde::encode::to_vec_named(value)?;
        debug_assert!(rmp_serde::decode::from_slice::<B>(&serialized).is_ok());
        self.set(key, &serialized)
    }

    pub fn get_deserialized<B: serde::de::DeserializeOwned>(&self, key: (&str, &str)) -> Result<Option<B>, Error> {
        Ok(match self.get(key)? {
            None => None,
            Some(data) => {
                match Self::deserialize_compressed(&data) {
                    Ok(res) => Some(res),
                    Err(e) => {
                        log::error!("Deserialization error: {} for key {:?}", e, key);
                        self.delete(key)?;
                        None
                    },
                }
            },
        })
    }

    pub(crate) fn deserialize_compressed<B: serde::de::DeserializeOwned>(mut data: &[u8]) -> Result<B, Error> {
        let mut decomp = Vec::with_capacity(data.len() * 2);
        BrotliDecompress(&mut data, &mut decomp)?;
        Self::deserialize(&decomp)
    }

    pub(crate) fn deserialize<B: serde::de::DeserializeOwned>(data: &[u8]) -> Result<B, Error> {
        Ok(rmp_serde::decode::from_slice(data)?)
    }

    #[inline(never)]
    pub fn set(&self, key: (&str, &str), mut data_in: &[u8]) -> Result<(), Error> {
        let mut out;
        let data = if self.compress {
            out = Vec::with_capacity(data_in.len() / 2);
            BrotliCompress(&mut data_in, &mut out, &Default::default())?;
            &out
        } else {
            data_in
        };
        Self::with_retries("set", || self.set_inner(key, data))
    }

    fn set_inner(&self, key: (&str, &str), data: &[u8]) -> Result<(), Error> {
        self.with_connection(|conn| {
            let mut q = conn.prepare_cached("INSERT OR REPLACE INTO cache2(key, ver, data) VALUES(?1, ?2, ?3)")
                .map_err(|e| Error::Db(self.url.clone(), Arc::new(e)))?;
            let arr: &[&dyn ToSql] = &[&key.0, &key.1, &data];
            q.execute(arr)
                .map_err(|e| Error::Db(self.url.clone(), Arc::new(e)))?;
            Ok(())
        })
    }
}
