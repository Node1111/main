
/// Trait to help insert commas, etc. in printed lists of iterated things
pub trait IdentifyLast: Iterator + Sized {
    fn identify_last(self) -> LastIter<Self>;
    fn peek_prev(self) -> PrevIter<Self>;
    /// Returns PREFIX
    fn comma_list(self, oxford: bool) -> CommaIter<Self>;
}

impl<It> IdentifyLast for It
where It: Iterator
{
    fn identify_last(mut self) -> LastIter<Self> {
        let e = self.next();
        LastIter { iter: self, buffer: e }
    }
    fn peek_prev(self) -> PrevIter<Self> {
        PrevIter { iter: self, prev: None }
    }
    fn comma_list(self, oxford: bool) -> CommaIter<Self> {
        CommaIter { nth: 0, oxford, iter: self.identify_last() }
    }
}

pub struct CommaIter<It> where It: Iterator {
    iter: LastIter<It>,
    nth: usize,
    oxford: bool,
}

pub struct PrevIter<It>
where It: Iterator
{
    iter: It,
    prev: Option<It::Item>,
}

pub struct LastIter<It>
where It: Iterator
{
    iter: It,
    buffer: Option<It::Item>,
}

impl<It> Iterator for LastIter<It>
where It: Iterator
{
    type Item = (bool, It::Item);

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        match self.buffer.take() {
            None => None,
            Some(e) => match self.iter.next() {
                None => Some((true, e)),
                Some(f) => {
                    self.buffer = Some(f);
                    Some((false, e))
                },
            },
        }
    }
}

impl<It> Iterator for PrevIter<It>
where It: Iterator, It::Item: Clone
{
    type Item = (Option<It::Item>, It::Item);

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        let this = self.iter.next()?;
        let prev = self.prev.replace(this.clone());
        Some((prev, this))
    }
}

impl<It> Iterator for CommaIter<It> where It: Iterator {
    type Item = (&'static str, It::Item);

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        let (last, item) = self.iter.next()?;
        let prefix = if self.nth == 0 { "" }
            else if last {
                if self.oxford && self.nth > 1 {", and "} else { " and "}
            } else { ", " };
        self.nth += 1;
        Some((prefix, item))
    }
}

#[test] fn iter() {
    let s: String = (1..=5).comma_list(true).map(|(sep, i)| format!("{sep}{i}")).collect();
    assert_eq!(s, "1, 2, 3, 4, and 5");
    let s: String = (1..=3).comma_list(false).map(|(sep, i)| format!("{sep}{i}")).collect();
    assert_eq!(s, "1, 2 and 3");
    let s: String = (1..=2).comma_list(true).map(|(sep, i)| format!("{sep}{i}")).collect();
    assert_eq!(s, "1 and 2");
    let s: String = (1..=1).comma_list(true).map(|(sep, i)| format!("{sep}{i}")).collect();
    assert_eq!(s, "1");
}
