use ahash::HashMapExt;
use ahash::HashMap;
use util::CowAscii;
use util::pick_top_n_unstable_by;
use util::smol_fmt;
use crate::SmolStr;
use crate::Page;
use kitchen_sink::CResult;
use kitchen_sink::DependencyKind;
use kitchen_sink::DependerChangesMonthly;
use kitchen_sink::Index;
use kitchen_sink::KitchenSink;
use kitchen_sink::Origin;
use kitchen_sink::RevDependencies;
use kitchen_sink::SemVer;
use locale::Numeric;
use render_readme::Renderer;
use rich_crate::RichCrateVersion;
use semver::VersionReq;
use std::cmp::Reverse;
use std::fmt::Display;
use std::time::Duration;
use tokio::time::Instant;
use tokio::time::timeout_at;

pub struct CratePageRevDeps<'a> {
    pub ver: &'a RichCrateVersion,
    pub deps: Vec<RevDepInf>,
    pub stats: Option<&'a RevDependencies>,
    pub has_download_columns: bool,
    downloads_by_ver: Vec<(SemVer, u32)>,
    changes: Vec<DependerChangesMonthly>,
}

#[derive(Debug, Default)]
pub(crate) struct DownloadsBar {
    pub num: u32,
    pub str: (String, &'static str),
    pub perc: f32,
    pub num_width: f32,
}

pub struct RevDepInf {
    pub origin: Origin,
    pub downloads: u32,
    pub depender_name: SmolStr,
    pub is_optional: bool,
    pub matches_latest: bool,
    pub kind: DependencyKind,
    pub req: VersionReq,
    pub rev_dep_count: u32,
    pub features: String,
}

impl RevDepInf {
    fn sort_value(&self) -> u64 {
        let mut dl = u64::from(1 + self.downloads);
        // round download numbers to sort them similarly to values shown, also rounded
        if dl > 10_000 {
            if dl > 1_000_000 {
                dl |= (1<<20)-1;
            } else {
                dl |= 1023;
            }
        }

        let mut d = dl + 256 * u64::from(1 + self.rev_dep_count);
        if self.is_optional {
            d /= 3;
        }
        // dev is handled separately anyway
        d
    }
}

pub(crate) struct DlRow {
    pub ver: SemVer,
    pub patch_merged: Option<(SmolStr, SmolStr, u16)>,
    pub num: u32,
    pub num_str: String,
    pub perc: f32,
    pub num_width: f32,

    pub dl: DownloadsBar,
}

pub struct ChangesEntry {
    pub running_total: u32,
    pub users_total: u16,
    pub added: u32,
    pub removed: u32,

    pub width: u16,
    pub running_totals_height: u16,
    pub users_totals_height: u16,
    pub added_height: u16,
    pub removed_height: u16,
    pub year: u16,

    pub label_inside: bool,
    pub users_label_inside: bool,
}

impl<'a> CratePageRevDeps<'a> {
    pub(crate) async fn new(ver: &'a RichCrateVersion, kitchen_sink: &'a KitchenSink, index: &'a Index, _markup: &'a Renderer) -> CResult<CratePageRevDeps<'a>> {
        let crate_name = ver.short_name();
        let own_name_lowercase = &*crate_name.as_ascii_lowercase();

        let all_deps_stats = index.deps_stats()?;
        let stats = all_deps_stats.counts.get(own_name_lowercase);

        let (ref latest_stable_semver, ref latest_unstable_semver) = {
            let c = index.crates_io_crates()?;
            (c.highest_version_by_lowercase_name(own_name_lowercase, true).expect("index").version().parse()?, c.highest_version_by_lowercase_name(own_name_lowercase, false).expect("index").version().parse()?)
        };

        let mut downloads_by_ver: Vec<_> = kitchen_sink.recent_downloads_by_version(ver.origin()).await?.into_iter().map(|(v, d)| (v.to_semver(), d)).collect();
        downloads_by_ver.sort_unstable_by(|a, b| b.0.cmp(&a.0));

        let deadline = Instant::now() + Duration::from_secs(8);
        let mut deps: Vec<_> = match stats {
            Some(s) => {
                let deps = s.rev_dep_names_default.iter().chain(s.rev_dep_names_optional.iter()).chain(s.rev_dep_names_dev.iter());
                futures::future::join_all(deps.map(|rev_dep| async move {
                    let origin = Origin::from_crates_io_name(rev_dep);
                    let downloads = timeout_at(deadline, kitchen_sink.downloads_per_month(&origin)).await
                        .unwrap_or(Ok(Some(100))); // if it times out, don't assume zero
                    let downloads = downloads.ok().and_then(|x| x).unwrap_or(0) as u32;
                    let crates_io_crates = &*index.crates_io_crates().unwrap();
                    let depender = crates_io_crates.highest_version_by_lowercase_name(&rev_dep.as_ascii_lowercase(), true).expect("rev dep integrity");

                    let (is_optional, req, kind, features) = depender.dependencies().iter().find(move |d| {
                        own_name_lowercase.eq_ignore_ascii_case(d.crate_name())
                    })
                    .map(|d| {
                        let mut features = d.features().iter().map(|f| f.as_str())
                            .filter(|&f| f != "default")
                            .take(5)
                            .flat_map(|f| ["+", f])
                            .collect::<String>();
                        if d.features().len() > 5 { features.push('…') }
                        (d.is_optional(), d.requirement(), d.kind(), features)
                    })
                    .unwrap_or_default();
                    let depender_name = depender.name().into();

                    let req = req.parse().unwrap_or(VersionReq::STAR);
                    let matches_latest = req.matches(latest_stable_semver) || req.matches(latest_unstable_semver);

                    RevDepInf {
                        origin,
                        depender_name,
                        downloads, is_optional, req, kind,
                        matches_latest,
                        rev_dep_count: 0,
                        features,
                    }
                })).await
            },
            None => Vec::new(),
        };

        // sort by downloads if > 100, then by name
        pick_top_n_unstable_by(&mut deps, 1000, |a, b| {
            b.sort_value().max(100).cmp(&a.sort_value().max(100))
            .then_with(|| {
                a.depender_name.cmp(&b.depender_name)
            })
        });
        for d in &mut deps {
            d.rev_dep_count = all_deps_stats.counts.get(&d.depender_name).map_or(0, |s| s.counters.direct.all());
        }
        let has_download_columns = deps.iter().any(|d| d.rev_dep_count > 0 || d.downloads > 100);

        let changes = kitchen_sink.depender_changes(ver.origin())?;
        Ok(Self { ver, deps, stats, has_download_columns, downloads_by_ver, changes })
    }

    pub(crate) fn kind(&self, k: DependencyKind) -> &'static str {
        match k {
            DependencyKind::Normal => "normal",
            DependencyKind::Dev => "dev",
            DependencyKind::Build => "build",
        }
    }

    /// Nicely rounded number of downloads
    ///
    /// To show that these numbers are just approximate.
    pub(crate) fn downloads(&self, num: u32) -> (String, &'static str) {
        crate::format_downloads(num as _)
    }

    pub(crate) fn format_number(&self, num: impl Display) -> String {
        Numeric::english().format_int(num)
    }

    // version, deps, normalized popularity 0..100
    pub(crate) fn version_breakdown(&self) -> Vec<DlRow> {
        let stats = match self.stats {
            None => return Vec::new(),
            Some(s) => s,
        };

        let mut ver: Vec<_> = stats.versions.iter().map(|(k, v)| {
            DlRow {
                ver: k.to_semver(),
                patch_merged: None,
                num: *v,
                perc: 0.,
                num_width: 0.,
                num_str: String::new(),
                dl: DownloadsBar::default(),
            }
        }).collect();

        // Ensure the (latest) version is always included
        let own_ver_semver: SemVer = self.ver.version().parse().expect("semver2");
        if !ver.iter().any(|v| v.ver == own_ver_semver) {
            ver.push(DlRow {
                ver: own_ver_semver,
                patch_merged: None,
                num: 0,
                perc: 0.,
                num_width: 0.,
                num_str: String::new(),
                dl: DownloadsBar::default(),
            });
        }

        // Download data may be older and not match exactly, so at least avoid
        // accidentally omitting the most popular version
        if let Some(biggest) = self.downloads_by_ver.iter().max_by_key(|v| v.1) {
            if !ver.iter().any(|v| v.ver == biggest.0) {
                ver.push(DlRow {
                    ver: biggest.0.clone(),
                    patch_merged: None,
                    num: 0,
                    perc: 0.,
                    num_width: 0.,
                    num_str: String::new(),
                    dl: DownloadsBar::default(),
                });
            }
        }

        // align selected versions and their (or older) downloads
        let mut dl_vers = self.downloads_by_ver.iter().rev().peekable();
        ver.sort_unstable_by(|a, b| b.ver.cmp(&a.ver));
        for curr in ver.iter_mut().rev() {
            let mut sum = 0;
            while let Some((next_ver, dl)) = dl_vers.peek() {
                if next_ver > &curr.ver {
                    break;
                }
                if next_ver.major == curr.ver.major &&
                (next_ver.major != 0 || next_ver.minor == curr.ver.minor) {
                    sum += dl;
                }
                dl_vers.next();
            }
            curr.dl.num = sum;
        }

        let rev_max = ver.iter().map(|v| v.num).max().unwrap_or(1);
        let dl_max = ver.iter().map(|v| v.dl.num).max().unwrap_or(1);

        if ver.len() >= 10 {
            Self::group_less_used_versions(&mut ver, dl_max, rev_max);
        }

        let dl_max = f64::from(dl_max);
        let rev_max = f64::from(rev_max);
        for i in &mut ver {
            i.perc = (f64::from(i.num) / rev_max * 100.0) as f32;
            i.num_str = self.format_number(i.num);
            i.num_width = 4. + 7. * i.num_str.len() as f32; // approx visual width of the number

            i.dl.perc = (i.dl.num as f64 / dl_max * 100.0) as f32;
            i.dl.str = self.downloads(i.dl.num);
            i.dl.num_width = 4. + 7. * (i.dl.str.0.len() + i.dl.str.1.len()) as f32; // approx visual width of the number
        }
        ver
    }

    /// entries + year and colspan
    pub(crate) fn changes_graph(&self) -> Option<(Vec<ChangesEntry>, Vec<(u16, u16)>)> {
        const MAX_ENTRIES: usize = 88;

        let total_max = self.changes.iter().map(|c| c.running_total()).max().unwrap_or(0).max(1);
        let users_total_max = self.changes.iter().map(|c| c.users_total).max().unwrap_or(0).max(1);

        let good_data = (self.changes.len() >= 12 && total_max >= 15) || (self.changes.len() >= 6 && total_max >= 100);
        if !good_data {
            return None;
        }

        let added_removed_max = self.changes.iter().map(|c| c.added.max(c.removed + c.expired)).max().unwrap_or(0).max(18);

        let adds_chart_height = 20;
        let totals_chart_height = 80;
        let width = (800 / self.changes.len().min(MAX_ENTRIES)).max(1).min(30) as _;

        let mut entries: Vec<_> = self.changes.iter().map(|ch| {
            let removed = ch.removed + ch.expired;
            let running_totals_height = f64::from(ch.running_total() * totals_chart_height) / f64::from(total_max.max(50));
            let users_totals_height = f64::from(u32::from(ch.users_total) * totals_chart_height) / f64::from(users_total_max.max(50));
            ChangesEntry {
                running_total: ch.running_total(),
                users_total: ch.users_total,
                added: ch.added,
                removed,
                width,
                running_totals_height: running_totals_height.round() as _,
                users_totals_height: users_totals_height.round() as _,
                added_height: (ch.added * adds_chart_height / added_removed_max) as _,
                removed_height: (removed * adds_chart_height / added_removed_max) as _,
                year: ch.year,

                label_inside: (f64::from(ch.running_total()).log10().ceil() * 5.8 + 7.) < running_totals_height,
                users_label_inside: (f64::from(ch.users_total).log10().ceil() * 5.8 + 7.) < users_totals_height,
            }
        }).collect();

        // wide chart doesn't fit on the page
        if entries.len() > MAX_ENTRIES {
            entries.drain(.. entries.len()-MAX_ENTRIES);
        }

        let mut years = Vec::with_capacity(entries.len() / 12 + 1);
        let mut curr_year = entries[0].year;
        let mut curr_year_months = 0;
        for e in &entries {
            if curr_year != e.year {
                years.push((curr_year, curr_year_months));
                curr_year = e.year;
                curr_year_months = 0;
            }
            curr_year_months += 1;
        }
        years.push((curr_year, curr_year_months));
        Some((entries, years))
    }

    pub(crate) fn page(&self) -> Page {
        Page {
            title: format!("Reverse dependencies of {}", self.ver.short_name()),
            item_name: Some(self.ver.short_name().to_string()),
            item_description: self.ver.description().map(|d| d.to_string()),
            noindex: true,
            search_meta: false,
            critical_css_data: Some(include_str!("../../style/public/revdeps.css")),
            critical_css_dev_url: Some("/revdeps.css"),
            ..Default::default()
        }
    }

    /// List of versions is sometimes way too long
    fn group_less_used_versions(ver: &mut Vec<DlRow>, dl_max: u32, rev_max: u32) {
        let dl_limit = dl_max.min(200).max(dl_max / 4) as u64;
        let rev_limit = rev_max.min(5).max(rev_max / 4);

        let mut count_of_patch = HashMap::new();
        let mut count_of_minor = HashMap::new();
        ver.iter().for_each(|r| {
            *count_of_patch.entry((r.ver.major as u32, r.ver.minor as u32)).or_insert(0) += 1u16;
            *count_of_minor.entry(r.ver.major as u32 /10).or_insert(0) += 1u16;
        });

        let version_group = move |ver: &SemVer| {
            let major = ver.major as u32;
            let minor = ver.minor as u32;
            let patch = ver.patch as u32;

            // count is for serde 1.0.9999
            let patch_cnt = count_of_patch.get(&(major, minor)).copied().unwrap_or(0);
            let minor_cnt = count_of_minor.get(&(major/10)).copied().unwrap_or(0);

            let (granularity, val) = if patch_cnt > 20 {
                (2u8, (minor, patch/10))
            } else if patch_cnt <= 1 && minor_cnt > 20 {
                (3, (minor/10, 0))
            } else {
                (1, (minor, patch))
            };
            (ver.major as u32, ver.pre.is_empty(), granularity, val)
        };

        let mut by_rough_version = HashMap::with_capacity(ver.len()/2);
        ver.iter_mut().enumerate().for_each(|(pos, r)| {
            let key = version_group(&r.ver);
            let nums = by_rough_version.entry(key).or_insert((0, 0, 0, pos as u16, r.ver.pre.as_str().as_bytes()));
            nums.0 += r.dl.num as u64;
            nums.1 += r.num;
            nums.2 += 1u16;
            let common_prefix = nums.4.iter().zip(r.ver.pre.as_str().as_bytes()).take_while(|(a,b)| a == b).count();
            nums.4 = &nums.4[..common_prefix];
        });

        let mut by_rough_version = by_rough_version.into_iter()
            .filter(move |&(_, (dl, rev, cnt, pos, _))| cnt > 1 && pos > 3 && dl < dl_limit && rev < rev_limit)
            .map(|(v, (dl, rev, cnt, pos, prefix))| {
                let common_prefix_len = prefix.len() as u16 - if prefix.last().map_or(false, |p| p.is_ascii_punctuation()) { 1 } else { 0 };
                (v, (dl, rev, cnt, pos, common_prefix_len))
            })
            .collect::<Vec<_>>();
        by_rough_version.sort_unstable_by_key(|&((major, is_pre, granularity, _), (dl, rev, cnt, _, _))| Reverse(
            // false is better, smaller is better
            (cnt > 10, !is_pre, granularity, cnt < 3, (dl / cnt as u64), rev, major == 0, Reverse(cnt))
        ));

        let reduce_down_to = 10.max(ver.len()/4);
        while let Some((to_change_ver, (dl_num, rev_num, cnt, _, common_prefix))) = by_rough_version.pop() {
            if ver.len() < 16 && cnt > 6 {
                continue;
            }
            let mut changed = false;
            ver.retain_mut(|r| {
                let group = version_group(&r.ver);
                let matches = to_change_ver == group && r.patch_merged.is_none();
                if matches {
                    if changed { return false; }
                    changed = true;
                    let (_, _, granularity, _) = group;

                    let (version, mut suffix) = match granularity {
                        1 => (smol_fmt!("{}.{}.", r.ver.major, r.ver.minor), SmolStr::from("x")),
                        2 => (smol_fmt!("{}.{}.", r.ver.major, r.ver.minor), if r.ver.patch>=10 { smol_fmt!("{}0", r.ver.patch/10) } else { SmolStr::from("x")}),
                        _ => (smol_fmt!("{}.", r.ver.major), if r.ver.minor>=10 { smol_fmt!("{}0", r.ver.minor/10) } else { SmolStr::from("x") }),
                    };

                    if !r.ver.pre.is_empty() {
                        use std::fmt::Write;
                        let _ = write!(&mut suffix, "-{}", r.ver.pre.as_str().get(..common_prefix as usize).unwrap_or("pre"));
                    }
                    r.patch_merged = Some((version, suffix, cnt));
                    r.num = rev_num;
                    r.dl.num = dl_num as u32; // str fixed later
                }
                true
            });

            if ver.len() <= reduce_down_to {
                break;
            }
        }
    }
}
