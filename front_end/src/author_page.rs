use ahash::HashMap;
use ahash::HashMapExt;
use ahash::HashSet;
use chrono::prelude::*;
use util::pick_top_n_unstable_by;
use util::sort_top_n_unstable_by;
use crate::Page;
use crate::url_domain;
use futures::stream::StreamExt;
use kitchen_sink::ABlockReason;
use kitchen_sink::ArcRichCrateVersion;
use kitchen_sink::CrateOwner;
use kitchen_sink::CrateOwnerRow;
use kitchen_sink::CrateOwners;
use kitchen_sink::CResult;
use kitchen_sink::KitchenSink;
use kitchen_sink::Org;
use kitchen_sink::OwnerKind;
use kitchen_sink::RichAuthor;
use kitchen_sink::Rustacean;
use kitchen_sink::SocialAccount;
use kitchen_sink::UserType;
use log::error;
use rich_crate::MaintenanceStatus;
use util::SmolStr;
use std::borrow::Cow;
use std::cmp::Ordering;
use std::cmp::Reverse;
use url::Url;

pub struct OwnedCrate {
    pub(crate) krate: ArcRichCrateVersion,
    pub(crate) dl: u32,
    pub(crate) ownership: CrateOwnerRow,
    pub(crate) all_human_owners: Vec<CrateOwner>,
    pub(crate) updated_long_time_ago: bool,
    pub(crate) updated_recently: bool,
    pub(crate) total_owners_count: u32,
}

/// Data sources used in `author.rs.html`
pub struct AuthorPage<'a> {
    pub(crate) aut: &'a RichAuthor,
    pub(crate) founder_crates: Vec<OwnedCrate>,
    pub(crate) member_crates: Vec<OwnedCrate>,
    pub(crate) orgs: Vec<Org>,
    pub(crate) social: Vec<SocialAccount>,
    pub(crate) joined: Option<DateTime<Utc>>,
    pub(crate) founder_total: usize,
    pub(crate) member_total: usize,
    pub(crate) keywords: Vec<SmolStr>,
    pub(crate) collab: Vec<(SmolStr, SmolStr)>,
    pub(crate) rustacean: Option<Rustacean>,
    // If Some, it's banned
    pub(crate) blocklist_reason: Option<&'a ABlockReason>,
    pub(crate) banned: bool,
    pub(crate) must_hide_data: bool,
}

impl<'a> AuthorPage<'a> {
    pub(crate) async fn new(aut: &'a RichAuthor, rows: Vec<CrateOwnerRow>, kitchen_sink: &'a KitchenSink) -> CResult<AuthorPage<'a>> {
        let rustacean = kitchen_sink.rustacean_for_github_login(&aut.github.login);
        let synonyms = kitchen_sink.synonyms();
        let orgs = kitchen_sink.user_github_orgs(&aut.github.login).await?;
        let orgs = futures::stream::iter(orgs.unwrap_or_default()).map(|org| async move {
            kitchen_sink.github_org(&org.login).await
                .map_err(|e| error!("org: {} {e}", &org.login))
                .ok().and_then(|x| x)
        })
        .buffered(8)
        .filter_map(|c| async move {c})
        .collect().await;
        let joined = rows.iter().filter_map(|row| row.invited_at).min();

        let social = kitchen_sink.github_social(&aut.github.login).await.unwrap_or_default();

        let (mut founder, mut member): (Vec<_>, Vec<_>) = rows.into_iter().partition(|c| c.invited_by_github_id.is_none());
        let founder_total = founder.len();
        let member_total = member.len();
        pick_top_n_unstable_by(&mut founder, 300, |a, b| b.latest_release.cmp(&a.latest_release));
        pick_top_n_unstable_by(&mut member, 300, |a, b| b.crate_ranking.partial_cmp(&a.crate_ranking).unwrap_or(Ordering::Equal));

        let (mut founder_crates, member_crates) = futures::join!(
            Self::look_up(kitchen_sink, founder),
            Self::look_up(kitchen_sink, member),
        );

        founder_crates.sort_unstable_by_key(move |a| {
            let dead = a.updated_long_time_ago || a.krate.is_yanked() || a.krate.is_spam() ||
                a.krate.maintenance() == MaintenanceStatus::Deprecated || a.krate.maintenance() == MaintenanceStatus::AsIs;
            let is_used = a.dl > 10_000;
            let major = if a.dl > 150_000 { a.dl } else { 0 };
            let with_others = a.total_owners_count > 1;
            Reverse((major, !dead, is_used, with_others, a.ownership.latest_release))
        });

        // Most common keywords
        let mut keywords = HashMap::new();
        let now = Utc::now();
        // Most collaborated with
        let mut collab = HashMap::new();
        for OwnedCrate { krate, ownership, all_human_owners, updated_long_time_ago, .. } in founder_crates.iter().chain(member_crates.iter()) {
            if krate.is_spam() || krate.is_yanked() {
                continue;
            }
            let kw_base_relevance = if *updated_long_time_ago || krate.maintenance() == MaintenanceStatus::Deprecated { 0.5 } else { 2. };
            for (i, k) in krate.keywords().iter().enumerate() {
                let k = synonyms.normalize(k);
                // take first(-ish) keyword from each crate to avoid one crate taking most
                // ranking should not be important here, it counts what the owner chose to work on
                *keywords.entry(k).or_insert(0.) += (ownership.crate_ranking + kw_base_relevance) / (i + 2) as f32;
            }

            // crates have naming patterns
            let n = krate.short_name().trim_matches(|c: char| c.is_ascii_punctuation());
            if n.chars().any(|c| c.is_ascii_punctuation()) {
                for (i, k) in n.split(|c: char| c.is_ascii_punctuation()).filter(|w| !w.is_empty()).enumerate() {
                    *keywords.entry(synonyms.normalize(k)).or_insert(0.) += (ownership.crate_ranking + kw_base_relevance) / (i + 3) as f32;
                }
            }

            if let Some(the_owner) = all_human_owners.iter().find(|o| o.github_id == Some(aut.github.id)) {
                let owner_since = the_owner.invited_at.unwrap_or(ownership.oldest_release);
                let oldest = all_human_owners.iter().filter_map(|o| o.invited_at).min()
                    .unwrap_or(ownership.oldest_release).min(ownership.oldest_release);
                // max discounts young crates
                let crates_age = now.signed_duration_since(oldest).num_days().max(30 * 6) as f32;
                let own_days = now.signed_duration_since(owner_since).num_days();
                for other in all_human_owners {
                    debug_assert!(!other.not_a_person);
                    debug_assert!(other.kind == OwnerKind::User);
                    let Some(login) = other.github_login() else { continue };
                    if other.github_id == Some(aut.github.id) {
                        continue;
                    }
                    let Some(other_invited_at) = other.invited_at else { continue };
                    // How long co-owned together, relative to crate's age
                    let overlap = now.signed_duration_since(other_invited_at).num_days().min(own_days) as f32 / crates_age;
                    let relationship = if the_owner.invited_by_github_id == other.github_id {4.}
                        else if other.invited_by_github_id == the_owner.github_id {3.} else {1.};

                    let value = (ownership.crate_ranking + 0.5) * overlap * relationship * if *updated_long_time_ago { 0.1 } else { 1. };
                    let entry = collab.entry(other.github_id).or_insert_with(move || (0., login, value, krate.short_name()));
                    entry.0 += value;
                    if value > entry.2 {
                        entry.2 = value;
                        entry.3 = krate.short_name();
                    }
                }
            }
        }

        let mut keywords: Vec<_> = keywords.into_iter().collect();
        if !keywords.is_empty() {
            let num_keywords = (1 + founder_total / 2 + member_total / 3).clamp(2, 7);
            let cmp = |a: &(&str, f32), b: &(&str, f32)| b.1.total_cmp(&a.1).then_with(move || a.0.cmp(b.0));
            sort_top_n_unstable_by(&mut keywords, num_keywords*2, cmp);

            if keywords.len() > num_keywords {
                // avoid ["foo-bar", "foo", "bar"]
                let compound_words = keywords.iter().take(num_keywords).filter(|&(k, _)| k.contains('-'))
                    .flat_map(|&(k, _)| k.split('-'))
                    .collect::<HashSet<_>>();
                if !compound_words.is_empty() {
                    let mut changed = false;
                    keywords.iter_mut().take(num_keywords*2)
                        .filter(|(k, _)| compound_words.contains(*k))
                        .for_each(|(_, v)| { *v *= 0.5; changed = true });
                    if changed {
                        sort_top_n_unstable_by(&mut keywords, num_keywords, cmp);
                    }
                }
            }
            keywords.truncate(num_keywords);
        }
        let keywords: Vec<_> = keywords.into_iter().map(|(k, _)| k.into()).collect();

        let mut collab: Vec<_> = collab.into_values().take(200).collect();
        collab.sort_unstable_by(|a, b| b.0.total_cmp(&a.0));
        let collab: Vec<_> = collab.into_iter().take(100).map(|(_, login, _, krate)| (login.into(), krate.into())).collect();

        let blocklist_reason = kitchen_sink.crates_io_login_on_blocklist(&aut.github.login);

        Ok(Self {
            founder_crates, member_crates,
            founder_total, member_total,
            aut,
            orgs,
            social,
            joined,
            keywords,
            collab,
            rustacean,
            banned: blocklist_reason.map_or(false, |b| matches!(b, ABlockReason::Banned(..))),
            must_hide_data: blocklist_reason.map_or(false, |b| matches!(b, ABlockReason::Hidden(..))),
            blocklist_reason,
        })
    }

    async fn look_up(kitchen_sink: &KitchenSink, rows: Vec<CrateOwnerRow>) -> Vec<OwnedCrate> {
        let now = Utc::now();
        futures::stream::iter(rows.into_iter())
            .map(|ownership| async move {
                let krate = kitchen_sink.rich_crate_version_async(&ownership.origin).await.map_err(|e| error!("{e}")).ok()?;
                let dl = kitchen_sink.downloads_per_month(&ownership.origin).await.map_err(|e| error!("{e}")).ok()?.unwrap_or(0) as u32;
                let mut all_owners = kitchen_sink.crate_owners(&ownership.origin, CrateOwners::All).await
                    .map_err(|e| error!("author collab: {:?} {e}", ownership.origin)).unwrap_or_default();
                let total_owners_count = all_owners.len() as u32;
                all_owners.retain(|o| !o.not_a_person && o.kind == OwnerKind::User);
                let updated_days = now.signed_duration_since(ownership.latest_release).num_days();
                Some(OwnedCrate {
                    updated_long_time_ago: updated_days > 3*365,
                    updated_recently: updated_days < 32,
                    krate, dl, ownership, all_human_owners: all_owners, total_owners_count,
                })
            })
            .buffered(8)
            .filter_map(|f| async move {f})
            .collect().await
    }

    pub(crate) fn name(&self) -> Option<&str> {
        let gh_name = self.aut.name();
        if !gh_name.eq_ignore_ascii_case(&self.aut.github.login) {
            return Some(gh_name);
        }
        self.rustacean.as_ref().and_then(|r| r.name.as_deref())
    }

    fn github_mastodon_link(&self) -> Option<(Cow<str>, Cow<str>)> {
        let s = self.social.iter().find(|i| i.provider == "mastodon")?;
        let url = Url::parse(&s.url).ok()?;
        let host = url.host_str()?;
        let user = url.path().trim_matches('/').trim_start_matches('@');

        Some((s.url.as_str().into(), format!("@{user}@{host}").into()))
    }

    /// URL, label
    pub(crate) fn mastodon_link(&self) -> Option<(Cow<str>, Cow<str>)> {
        if let Some(l) = self.github_mastodon_link() {
            return Some(l);
        }
        self.rustacean.as_ref()?.mastodon().map(|(url, label)| (url.into(), label.into()))
    }

    pub(crate) fn forum_link(&self) -> Option<(String, &str)> {
        self.rustacean
            .as_ref()
            .and_then(|r| r.discourse.as_deref())
            .map(|t| t.trim_start_matches('@'))
            .filter(|t| !t.is_empty())
            .map(|t| (format!("https://users.rust-lang.org/u/{t}"), t))
    }

    /// `(url, label)`
    pub(crate) fn homepage_link(&self) -> Option<(&str, Cow<'_, str>)> {
        let url = self.aut.github.blog.as_deref()
            .or_else(|| self.rustacean.as_ref().and_then(|r| r.website.as_deref()))
            .or_else(|| self.rustacean.as_ref().and_then(|r| r.blog.as_deref()))
            .or_else(|| self.social.iter().find(|r| r.provider == "generic").map(|r| r.url.as_str()));
        if let Some(url) = url {
            if url.starts_with("https://") || url.starts_with("http://") {
                let label = url_domain(url)
                    .map(|host| {
                        format!("Home ({host})").into()
                    })
                    .unwrap_or_else(|| "Homepage".into());
                return Some((url, label));
            }
        }
        None
    }

    pub(crate) fn joined_github(&self) -> Option<DateTime<FixedOffset>> {
        if let Some(d) = &self.aut.github.created_at {
            DateTime::parse_from_rfc3339(d).ok()
        } else {
            None
        }
    }

    pub(crate) fn org_name(org: &Org) -> &str {
        if let Some(name) = &org.name {
            if name.eq_ignore_ascii_case(&org.login) {
                return name;
            }
        }
        &org.login
    }

    pub(crate) fn format_month(date: &DateTime<Utc>) -> String {
        date.format("%b %Y").to_string()
    }

    pub(crate) fn is_org(&self) -> bool {
        self.aut.github.user_type == UserType::Org
    }

    pub(crate) fn github_url(&self) -> String {
        format!("https://github.com/{}", self.aut.github.login)
    }

    pub(crate) fn page(&self) -> Page {
        Page {
            title: format!("@{}'s Rust crates", self.aut.github.login),
            canonical: Some(format!("https://lib.rs/~{}", self.aut.github.login)),
            critical_css_data: Some(include_str!("../../style/public/author.css")),
            critical_css_dev_url: Some("/author.css"),
            noindex: self.joined.is_none() || self.blocklist_reason.is_some(),
            ..Default::default()
        }
    }
}
