use ahash::HashSet;
use ahash::HashSetExt;
use anyhow::Error;
use categories::CATEGORIES;
use categories::Category;
use crate::Page;
use crate::templates;
use futures::stream::StreamExt;
use kitchen_sink::ArcRichCrateVersion;
use kitchen_sink::KitchenSink;
use kitchen_sink::MaintenanceStatus;
use kitchen_sink::RendCtx;
use log::error;
use render_readme::Renderer;
use rich_crate::RichCrateVersion;
use std::time::Duration;
use tokio::time::Instant;
use tokio::time::timeout_at;
use util::SmolStr;

/// Data for category page template
pub struct CatPage<'a> {
    pub cat: &'a Category,
    pub keywords: Vec<SmolStr>,
    pub crates: Vec<(ArcRichCrateVersion, u32)>,
    pub related_category_slugs: Vec<String>,
    pub markup: &'a Renderer,
    pub count: usize,
    pub main_page_nav: bool,
}

impl<'a> CatPage<'a> {
    pub(crate) async fn new(cat: &'a Category, crates: &'a KitchenSink, markup: &'a Renderer, rend: &RendCtx) -> Result<CatPage<'a>, Error> {
        let deadline = Instant::now() + Duration::from_secs(20);

        let (count, keywords, related) = futures::join!(
            crates.category_crate_count(&cat.slug),
            crates.top_keywords_in_category(cat),
            crates.related_categories(&cat.slug),
        );
        let mut crates = futures::stream::iter(crates
                .top_crates_in_category(&cat.slug, rend).await?.iter().cloned())
                .map(|o| async move {
                    let crate_timeout = Instant::now() + Duration::from_secs(5);
                    let c = match timeout_at(deadline.min(crate_timeout), crates.rich_crate_version_stale_is_ok(&o)).await {
                        Ok(Ok(c)) => {
                            if c.is_hidden() || c.is_spam() {
                                return None;
                            }
                            c
                        },
                        Err(e) => {
                            error!("Skipping in cat {o:?} because timed out {e}");
                            return None;
                        },
                        Ok(Err(e)) => {
                            error!("Skipping in cat {o:?} because {e}");
                            return None;
                        },
                    };
                    if c.is_yanked() || c.maintenance() == MaintenanceStatus::Deprecated {
                        return None;
                    }
                    let d = match crates.downloads_per_month_or_equivalent(&o).await {
                        Ok(d) => d.unwrap_or(0) as u32,
                        Err(e) => {
                            error!("Skipping {o:?} because dl {e}");
                            return None;
                        },
                    };
                    Some((c, d))
                })
                .buffered(8)
                .filter_map(|c| async move {c})
                .collect::<Vec<_>>().await;

        let round_by = if crates.len() > 1000 { 100 } else if crates.len() > 150 { 50 } else { 10 };
        let round_len = crates.len() / round_by * round_by;
        crates.truncate(round_len);

        Ok(Self {
            main_page_nav: cat.slug == "std",
            crates,
            count: count?.0 as usize,
            keywords: keywords?,
            related_category_slugs: related?,
            cat,
            markup,
        })
    }

    pub(crate) fn has_subcategories_and_siblings(&self) -> bool {
        if self.cat.slug == "cryptography" {
            return false; // hides cryptocrrencies
        }
        !self.cat.sub.is_empty() || !self.cat.siblings.is_empty()
    }

    pub(crate) fn subcategories_and_siblings(&self) -> impl Iterator<Item = &Category> {
        self.cat.sub.values().chain(self.cat.siblings.iter().flat_map(|slug| CATEGORIES.from_slug(slug).0.into_iter()))
    }

    /// Used to render descriptions
    pub(crate) fn render_maybe_markdown_str(&self, s: &str) -> templates::Html<String> {
        crate::render_maybe_markdown_str(s, self.markup, None, None)
    }

    /// For color of the version
    ///
    /// It tries to guess which versions seem "unstable".
    ///
    /// TODO: Merge with the better version history analysis from the individual crate page.
    pub(crate) fn version_class(&self, c: &RichCrateVersion) -> &str {
        let v = c.version_semver().unwrap();
        match (v.major, v.minor, v.patch, !v.pre.is_empty()) {
            (1..=15, _, _, false) => "stable",
            (0, m, p, false) if m >= 2 && p >= 3 => "stable",
            (m, ..) if m >= 1 => "okay",
            (0, 1, p, _) if p >= 10 => "okay",
            (0, 3..=10, p, _) if p > 0 => "okay",
            _ => "unstable",
        }
    }

    pub(crate) fn description(&self) -> &str {
        self.cat.description.trim_end_matches('.')
    }

    /// "See also" feature
    pub(crate) fn related_categories(&self) -> Vec<Vec<&Category>> {
        let mut seen = HashSet::with_capacity(self.related_category_slugs.len());
        self.related_category_slugs.iter().map(|slug| CATEGORIES.from_slug(slug).0.into_iter().filter(|c| seen.insert(&c.slug)).collect()).filter(|v: &Vec<_>| !v.is_empty()).collect()
    }

    /// Metadata about the category
    pub(crate) fn page(&self) -> Page {
        Page {
            title: format!("{} — list of Rust libraries/crates", self.cat.standalone_name()),
            description: Some(self.cat.description.clone()),
            item_name: Some(self.cat.name.to_string()),
            item_description: Some(self.cat.short_description.clone()),
            keywords: Some(self.keywords.join(", ")),
            noindex: false,
            search_meta: true,
            ..Default::default()
        }
    }

    /// For breadcrumbs
    pub(crate) fn parent_categories(&self) -> Vec<&Category> {
        let mut c: Vec<_> = CATEGORIES.from_slug(&self.cat.slug).0;
        c.pop();
        c
    }
}
