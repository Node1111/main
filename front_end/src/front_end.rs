//! This crate generates HTML templates for https://lib.rs
//!
//! Most template require their own type of struct that does
//! some lightweight conversion from data model/APIs,
//! because the template engine Ructe doesn't support
//! complex expressions in the templates.
#![allow(clippy::match_overlapping_arm)]

mod all_versions;
mod author_page;
mod cat_page;
mod crate_page;
mod crev;
mod features_page;
mod download_graph;
mod global_stats;
mod home_page;
mod install_page;
mod iter;
mod maintainer_dashboard;
mod not_found_page;
mod reverse_dependencies;
mod search_page;
mod urler;

use log::warn;
use futures::future::join_all;
use std::sync::Arc;
use blocking::block_in_place;
use kitchen_sink::running;
pub use crate::global_stats::*;
pub use crate::not_found_page::*;
pub use crate::search_page::*;
use tokio::time::timeout_at;
use ahash::HashSet;
use ahash::HashSetExt;
use anyhow::Context;
use categories::Category;
use chrono::prelude::*;
use crate::author_page::AuthorPage;
use crate::crate_page::{Contributors, CratePage};
use crate::urler::Urler;
use kitchen_sink::CrateOwnerRow;
use kitchen_sink::CrateOwners;
use kitchen_sink::KitchenSink;
use kitchen_sink::KitchenSinkErr;
use kitchen_sink::RendCtx;
use kitchen_sink::Review;
use kitchen_sink::RichAuthor;
use kitchen_sink::vet::Review as VetReview;
use log::error;
use maintainer_dashboard::MaintainerDashboard;
use render_readme::Links;
use render_readme::LinksContext;
use render_readme::Markup;
use render_readme::Renderer;
use rich_crate::Origin;
use rich_crate::RichCrate;
use rich_crate::RichCrateVersion;
use util::SmolStr;
use std::borrow::Cow;
use std::io::BufWriter;
use std::io::Write;
use std::time::Duration;
use std::time::Instant;
use url::Url;
use urlencoding::Encoded;
use util::CowAscii;
use util::PushInCapacity;
use util::PushString;
use blocking::awatch;

include!(concat!(env!("OUT_DIR"), "/templates.rs"));

/// Metadata used in the base template, mostly for `<meta>`
#[derive(Default)]
pub struct Page {
    title: String,
    description: Option<String>,
    author: Option<String>,
    item_name: Option<String>,
    item_description: Option<String>,
    keywords: Option<String>,
    created: Option<String>,
    alternate: Option<String>,
    alternate_type: Option<&'static str>,
    canonical: Option<String>,
    noindex: bool,
    search_meta: bool,
    critical_css_data: Option<&'static str>,
    #[allow(dead_code)]
    critical_css_dev_url: Option<&'static str>,
    local_css_data: Option<&'static str>,

    /// for libs.rs redir
    absolute_urls: bool,
}

impl Page {
    #[must_use] pub(crate) fn critical_css(&self) -> templates::Html<&'static str> {
        #[cfg(debug_assertions)]
        {
            if let Some(url) = self.critical_css_dev_url {
                // it's super ugly hack, but just for dev
                return templates::Html(Box::leak(format!("</style><link rel=stylesheet href='{url}'><style>").into_boxed_str()));
            }
        }
        let data = self.critical_css_data.unwrap_or(include_str!("../../style/public/critical.css"));
        templates::Html(data)
    }

    pub(crate) fn local_css_data(&self) -> Option<templates::Html<&'static str>> {
        self.local_css_data.map(templates::Html)
    }

    pub(crate) fn url_prefix(&self) -> &'static str {
        if self.absolute_urls { "https://lib.rs" } else { "" }
    }
}

/// See `cat_page.rs.html`
pub async fn render_category(out: &mut Vec<u8>, cat: &Category, crates: &KitchenSink, renderer: &Renderer, rend: &RendCtx) -> Result<(), anyhow::Error> {
    let urler = Urler::new(None);
    let page = awatch(format!("cat {}", cat.slug), cat_page::CatPage::new(cat, crates, renderer, rend)).await.context("Category page data loading")?;
    templates::cat_page_html(out, &page, &urler)?;
    Ok(())
}

/// See `homepage.rs.html`
pub async fn render_homepage(out: &mut Vec<u8>, crates: &KitchenSink, rend: &RendCtx) -> Result<(), anyhow::Error> {
    let urler = Urler::new(None);
    let home = home_page::HomePage::new(crates).await?;
    let all = blocking::awatch("all_cat", home.all_categories(rend)).await;
    templates::homepage_html(out, &home, &all, &urler)?;
    Ok(())
}

/// See `atom.rs.html`
pub async fn render_feed(out: &mut Vec<u8>, crates: &KitchenSink, rend: &RendCtx) -> Result<(), anyhow::Error> {
    let urler = Urler::new(None);
    let home = home_page::HomePage::new(crates).await?;
    let recents = home.recently_updated_crates(rend).await?;
    templates::atom_html(out, &home, &urler, &recents)?;
    Ok(())
}

pub async fn render_sitemap<W: Write>(sitemap: &mut BufWriter<W>, crates: &KitchenSink) -> Result<(), anyhow::Error> {
    let all_crates = crates.sitemap_crates().await?;
    let urler = Urler::new(None);

    sitemap.write_all(br#"<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">"#)?;

    for (origin, _rank, lastmod) in all_crates {
        write!(
            sitemap,
            r#"
<url><lastmod>{date}</lastmod><loc>https://lib.rs{url}</loc></url>"#,
            url = urler.crate_by_origin(&origin),
            date = Utc.timestamp_opt(lastmod, 0).unwrap().to_rfc3339(),
        )?;
    }

    sitemap.write_all(b"\n</urlset>\n")?;
    Ok(())
}

/// See `author.rs.html`
pub async fn render_author_page(out: &mut Vec<u8>, rows: Vec<CrateOwnerRow>, aut: &RichAuthor, kitchen_sink: &KitchenSink) -> Result<(), anyhow::Error> {
    running()?;

    let urler = Urler::new(None);
    let c = AuthorPage::new(aut, rows, kitchen_sink).await.context("Can't load data for author page")?;
    templates::author_html(out, &urler, &c).context("author page io")?;
    Ok(())
}

/// See `maintainer_dashboard.rs.html`
/// See `maintainer_dashboard_atom.rs.html`
pub async fn render_maintainer_dashboard(out: &mut Vec<u8>, atom_feed: bool, rows: Vec<CrateOwnerRow>, aut: &RichAuthor, kitchen_sink: &KitchenSink, rend: &RendCtx) -> Result<(), anyhow::Error> {
    running()?;

    let urler = Urler::new(None);
    let c = MaintainerDashboard::new(aut, rows, kitchen_sink, &urler, !atom_feed, rend).await.context("Can't load data for the dashboard")?;
    if !atom_feed {
        templates::maintainer_dashboard_html(out, &urler, &c)
    } else {
        templates::maintainer_dashboard_atom_html(out, &urler, &c)
    }.context("maintainer dashboard io")?;
    Ok(())
}

/// See `crate_page.rs.html`
pub async fn render_crate_page(out: &mut Vec<u8>, all: &RichCrate, ver: &RichCrateVersion, similar: Vec<(Origin, f32)>, kitchen_sink: &KitchenSink, renderer: &Renderer, rend: &RendCtx) -> Result<Option<DateTime<Utc>>, anyhow::Error> {
    running()?;

    let urler = Urler::new(Some(ver.origin().clone()));
    let c = awatch(format!("crpage {}", ver.short_name()), CratePage::new(all, ver, kitchen_sink, similar, renderer, rend)).await.context("Can't load data for crate page")?;
    templates::crate_page_html(out, &urler, &c).context("crate page io")?;
    Ok(c.date_created())
}

/// See `reverse_dependencies.rs.html`
pub async fn render_crate_reverse_dependencies(out: &mut Vec<u8>, ver: &RichCrateVersion, kitchen_sink: &KitchenSink, renderer: &Renderer) -> Result<(), anyhow::Error> {
    running()?;
    let urler = Urler::new(None);
    let index = kitchen_sink.index()?;
    let c = awatch(format!("rev {}", ver.short_name()), reverse_dependencies::CratePageRevDeps::new(ver, kitchen_sink, index, renderer)).await.context("Rev deps data loading")?;
    templates::reverse_dependencies_html(out, &urler, &c)?;
    Ok(())
}

/// See `install.rs.html`
pub async fn render_install_page(out: &mut Vec<u8>, ver: &RichCrateVersion, kitchen_sink: &KitchenSink, renderer: &Renderer) -> Result<(), anyhow::Error> {
    running()?;
    let urler = Urler::new(None); // Don't set self-crate, because we want to link back to crate page
    let c = crate::install_page::InstallPage::new(ver, kitchen_sink, renderer).await;
    templates::install_html(out, &urler, &c).context("install page io")?;
    Ok(())
}

/// See `all_versions.rs.html`
pub async fn render_all_versions_page(out: &mut Vec<u8>, all: &RichCrate, ver: &RichCrateVersion, kitchen_sink: &KitchenSink) -> Result<(), anyhow::Error> {
    running()?;
    let urler = Urler::new(None); // Don't set self-crate, because we want to link back to crate page
    let versions = match kitchen_sink.all_crates_io_versions(all.origin()) {
        Ok(v) => v,
        Err(KitchenSinkErr::NoVersions) => Vec::new(),
        Err(e) => return Err(e.into()),
    };
    let c = is_send(crate::all_versions::AllVersions::new(all, ver, &versions, kitchen_sink, &urler)).await.context("Versions data loading")?;
    templates::all_versions_html(out, &urler, &c).context("all_versions page io")?;
    Ok(())
}

/// See `deps.rs.html`
pub async fn render_features_page(out: &mut Vec<u8>, renderer: &Renderer, ver: &RichCrateVersion, kitchen_sink: &KitchenSink) -> Result<(), anyhow::Error> {
    running()?;
    let urler = Urler::new(None); // Don't set self-crate, because we want to link back to crate page

    let mut stuff = crate::features_page::FeaturesPageCollector::new(ver, kitchen_sink.index()?)?;
    let more_crates = stuff.needs_additional_crates();
    let deadline = Instant::now() + Duration::from_secs(2);
    let more_crates = join_all(more_crates.into_iter().map(|origin| async {
        timeout_at(deadline.into(), kitchen_sink.rich_crate_version_stale_is_ok(origin)).await.ok().and_then(|res| res.ok())
    })).await;
    let more_crates = more_crates.iter().filter_map(|res| res.as_deref())
        .map(|k| (k.origin(), k)).collect();
    stuff.add_additional_crates(more_crates);

    let c = crate::features_page::FeaturesPage::new(stuff, renderer, ver, kitchen_sink, &urler).context("Features data gathering")?;
    templates::features_html(out, &urler, &c)?;
    Ok(())
}

/// See `crev.rs.html`
pub async fn render_crate_reviews(out: &mut Vec<u8>, crev_reviews: &[Review], vet_reviews: &[VetReview<'_>], ver: &RichCrateVersion, kitchen_sink: &KitchenSink, renderer: &Renderer) -> Result<(), anyhow::Error> {
    running()?;
    let urler = Urler::new(None); // Don't set self-crate, because we want to link back to crate page
    let c = crate::crev::ReviewsPage::new(crev_reviews, vet_reviews, ver, kitchen_sink, renderer, &urler).await;
    templates::crev_html(out, &urler, &c).context("crev page io")?;
    Ok(())
}

pub async fn render_trending_crates(out: &mut Vec<u8>, kitchen_sink: Arc<KitchenSink>, renderer: &Renderer, rend: &RendCtx) -> Result<(), anyhow::Error> {
    let (top, upd) = futures::join!(kitchen_sink.clone().trending_crates(55, rend), Box::pin(kitchen_sink.notable_recently_updated_crates(100, rend)));
    let kitchen_sink = &*kitchen_sink;
    let upd = upd?;
    let top = top?;

    let deadline = tokio::time::Instant::now() + Duration::from_secs(10);
    let mut seen = HashSet::new();
    let mut tmp1 = Vec::with_capacity(upd.len().min(45));
    for (k, _) in &upd {
        if seen.insert(k) {
            let f1 = async move { timeout_at(deadline, kitchen_sink.rich_crate_version_async(k)).await? };
            let f2 = async move { timeout_at(deadline, kitchen_sink.rich_crate_async(k)).await? };
            tmp1.push(async move { futures::try_join!(f1, f2) });
            if tmp1.len() >= 45 {
                break;
            }
        }
    }
    let mut updated = blocking::awatch("tren-1", join_all(tmp1)).await.into_iter().filter_map(|res| {
        match res {
            Ok(res) => Some(res),
            Err(e) => {
                rend.set_incomplete();
                warn!("new skips1: {e}");
                None
            },
        }
    }).collect::<Vec<_>>();
    if updated.is_empty() {
        anyhow::bail!("failed to load recently updated crates");
    }

    let mut tmp2 = Vec::with_capacity(top.len().min(45));
    for (k, _) in &top {
        if seen.insert(k) {
            let f1 = async move { timeout_at(deadline, kitchen_sink.rich_crate_version_async(k)).await? };
            let f2 = async move { timeout_at(deadline, kitchen_sink.rich_crate_async(k)).await? };
            tmp2.push(async move { futures::try_join!(f1, f2) });
            if tmp2.len() >= 45 {
                break;
            }
        }
    }

    let mut trending = blocking::awatch("tren-2", join_all(tmp2)).await.into_iter().filter_map(|res| {
        match res {
            Ok(res) => Some(res),
            Err(e) => {
                rend.set_incomplete();
                warn!("new skips2: {e}");
                None
            },
        }
    }).collect::<Vec<_>>();
    if trending.is_empty() {
        anyhow::bail!("failed to load trending crates");
    }

    drop(seen);
    drop(upd);
    drop(top);

    updated.retain(|(k, _)| {
        !k.category_slugs().iter().any(|c| c == "cryptography::cryptocurrencies")
    });
    trending.retain(|(k, _)| {
        !k.category_slugs().iter().any(|c| c == "cryptography::cryptocurrencies")
    });

    // updated were sorted by rank…
    updated.sort_by_cached_key(|(_, all)| {
        std::cmp::Reverse(all.versions().iter().map(|v| v.created_at).max())
    });

    block_in_place("trendhtml", move || {
        let urler = Urler::new(None);
        templates::trending_html(out, &Page {
            title: "New and trending crates".to_owned(),
            description: Some("Rust packages that have been recently published or gained popularity. See what's new.".to_owned()),
            noindex: false,
            search_meta: true,
            critical_css_data: Some(include_str!("../../style/public/home.css")),
            critical_css_dev_url: Some("/home.css"),
            ..Default::default()
        }, &trending, &updated, &urler, renderer)
    })?;
    Ok(())
}

pub async fn render_debug_page(out: &mut Vec<u8>, kitchen_sink: &KitchenSink, origin: &Origin) -> Result<(), anyhow::Error> {
    let stats = kitchen_sink.index()?.deps_stats()?;
    let t = kitchen_sink.traction_stats(origin, stats).await?;
    let r = kitchen_sink.crate_ranking_for_builder(origin).await?;
    let dl = kitchen_sink.downloads_per_month_or_equivalent(origin).await?.unwrap_or(0);
    let dep = stats.crates_io_dependents_stats_of(origin);
    let pop = kitchen_sink.version_popularity(origin, &"*".parse().unwrap(), stats).await?;
    let owners = kitchen_sink.crate_owners(origin, CrateOwners::All).await?;
    let trusts = kitchen_sink.crate_owners_trust(&owners)?;
    let deprecated = kitchen_sink.is_marked_deprecated_reason(origin).map(|d| format!("is pre-marked as DEPRECATED because {d}")).unwrap_or_default();

    writeln!(out, "<pre>{t:#?}
rank: {r}
dl: {dl}
pop: {pop:#?}
{dep:#?}
{deprecated}
")?;
    for (i, o) in owners.iter().enumerate() {
        writeln!(out, "tr={:0.2} {o:?} ", trusts.get(i).copied().unwrap_or(-1.))?;
    }
    write!(out, "</pre>")?;
    Ok(())
}

pub async fn render_compat_page(out: &mut impl Write, all: RichCrate, kitchen_sink: &KitchenSink) -> Result<(), anyhow::Error> {
    let mut rustc_versions = HashSet::new();
    rustc_versions.insert(60);
    rustc_versions.insert(55);
    rustc_versions.insert(50);
    rustc_versions.insert(45);
    rustc_versions.insert(40);
    rustc_versions.insert(35);
    rustc_versions.insert(30);

    let by_crate_ver = kitchen_sink.rustc_compatibility(&all).await?;

    for c in by_crate_ver.values() {
        for v in c.all_rustc_versions() { rustc_versions.insert(v); }
    }

    let mut rustc_versions = rustc_versions.into_iter().collect::<Vec<u16>>();
    rustc_versions.sort_unstable();
    templates::compat_html(out, (rustc_versions, by_crate_ver))?;
    Ok(())
}

pub fn render_error(out: &mut Vec<u8>, title: String, headline: &str, err: &anyhow::Error) {
    templates::error_page_html(out, err, headline, &Page {
        title,
        noindex: true,
        ..Default::default()
    }).expect("error rendering error page");
}

/// See `crate_page.rs.html`
pub fn render_static_page(out: &mut Vec<u8>, title: String, page: &Markup, renderer: &Renderer) -> Result<(), anyhow::Error> {
    running()?;

    let (html, warnings) = renderer.page(page, &LinksContext {
        base_url: Some(("https://lib.rs", "https://lib.rs")), nofollow: Links::Trusted,
        own_crate_name: None, link_own_crate_to_crates_io: false,
        link_fixer: None,
    }, Instant::now() + Duration::from_secs(4));
    if !warnings.is_empty() {
        error!("static: {warnings:?}");
    }

    templates::static_page_html(out, &Page {
        title,
        local_css_data: Some("main li {margin-top:0.25em;margin-bottom:0.25em}"),
        noindex: false,
        search_meta: true,
        ..Default::default()
    }, templates::Html(html))?;
    Ok(())
}

/// See `crate_page.rs.html`
pub fn render_static_trusted_html(out: &mut Vec<u8>, title: String, html: String) -> Result<(), anyhow::Error> {
    running()?;

    templates::static_page_html(out, &Page {
        title,
        local_css_data: Some("main li {margin-top:0.25em;margin-bottom:0.25em}"),
        noindex: false,
        search_meta: true,
        absolute_urls: true,
        ..Default::default()
    }, templates::Html(html))?;
    Ok(())
}

#[must_use]
pub(crate) fn limit_description_len<'c>(text: &'c str, len_min: usize, len_max: usize, skip_word: &str) -> Cow<'c, str> {
    // skip over intros like "this is a yet another easy-to-use rust library that helps make…"
    let first_ok_word = text.split(|c: char| c.is_ascii_whitespace() || c == ',').find(|&word| {
        let word = word.as_ascii_lowercase();
        let word = &*word;
        word != skip_word && !word.is_empty() && !matches!(word, "a" | "an" | "the" | "it" | "this" | "is" | "simple" | "use" | "library" | "crate" | "rusty" | "which" |
        "provides" | "implementation" | "rust" | "to" | "of" | "for" | "helps" | "helpful" | "and" | "easy-to-use" | "yet" | "another" | "make" | "you" | "with" | "utility" | "utilities" | "tool")
    });
    let text = if let Some(first_ok_word) = first_ok_word {
        let skip_len = (first_ok_word.as_ptr() as usize) - (text.as_ptr() as usize);
        text.get(skip_len..).unwrap_or(text)
    } else {
        text
    };
    limit_text_len(text, len_min, len_max)
}

#[must_use]
#[inline]
pub(crate) fn limit_text_len_mid(text: &str, len_max: u32) -> Cow<'_, str> {
    if text.len() <= len_max as usize {
        return text.into();
    }

    #[inline(never)]
    #[cold]
    fn cut_text(text: &str, len_max: u32) -> Cow<'_, str> {
        let break_positions = text.split_inclusive(|c: char| c.is_ascii_punctuation() || c.is_ascii_whitespace())
            .map(|s| s.len() as u32)
            // .flat_map(|s| if s > 10 {[s-(s/2), s/2]} else {[0, s]})
            // .filter(|&s| s > 0)
            .collect::<Vec<_>>();

        let mut breaks = break_positions.iter().copied();
        let mut from_start = breaks.next().unwrap_or(0);
        let mut from_end = breaks.next_back().unwrap_or(0);
        let mut start_done = false;
        let mut end_done = false;
        'fin: while !start_done && !end_done {
            while end_done || from_start <= from_end {
                let Some(next) = breaks.next() else { break 'fin };
                if from_start + from_end + next >= len_max {
                    start_done = true;
                    break;
                }
                from_start += next;
            }
            while start_done || from_start > from_end {
                let Some(next) = breaks.next_back() else { break 'fin };
                if from_start + from_end + next >= len_max {
                    end_done = true;
                    break;
                }
                from_end += next;
            }
        }
        let mut start_pos = from_start.min(len_max) as usize;
        while !text.is_char_boundary(start_pos) && start_pos > 0 {
            start_pos -= 1;
        }
        let start_text = text.get(..start_pos).unwrap_or_default().trim_end_matches(|c: char| c.is_ascii_punctuation());

        let mut end_pos = text.len() - (from_end as usize).min(text.len() - start_text.len());
        while !text.is_char_boundary(end_pos) && end_pos < text.len()-1 {
            end_pos += 1;
        }
        let end_text = text.get(end_pos..).unwrap_or_default().trim_start_matches(|c: char| c.is_ascii_punctuation());

        if start_text.len() + end_text.len() + 3 >= text.len() {
            return text.into();
        }

        let mut out = String::with_capacity(start_text.len() + end_text.len() + '…'.len_utf8());
        out.push_str_in_cap(start_text);
        out.push_in_cap('…');
        out.push_str_in_cap(end_text);
        out.into()
    }
    cut_text(text, len_max)
}

pub(crate) fn short_target_label(text: &str) -> Cow<'_, str> {
    Cow::Borrowed(match text {
        "macos" => "mac",
        "windows" => "win",
        "aarch64" => "arm64",
        "little" => "le",
        "big" => "be",
        "target_thread_local" => "tls",
        "target_has_atomic" => "atomic",
        "debug_assertions" => "debug",
        _ => return limit_text_len(text.trim_end_matches("-unknown"), 10, 20),
    })
}

#[inline]
#[must_use]
pub(crate) fn limit_text_len(text: &str, len_min: usize, len_max: usize) -> Cow<'_, str> {
    debug_assert!(len_min <= len_max);
    if text.len() <= len_max {
        return text.into();
    }

    #[inline(never)]
    #[cold]
    fn cut_text_start(text: &str, mut len_min: usize, mut len_max: usize) -> &str {
        while !text.is_char_boundary(len_max) {
            len_max += 1;
        }
        while !text.is_char_boundary(len_min) {
            len_min += 1;
        }
        let mut cut = text.get(..len_max).unwrap_or_default();
        let optional = cut.get(len_min..).unwrap_or_default();
        if let Some(pos) = optional.find(&['.', ',', '!', '\n', '?', ')', ']'][..]).or_else(|| optional.find(' ')) {
            cut = cut[..=len_min + pos].trim_end_matches(&['.', '_', '-', ',', '!', '\n', '?', ' '][..]);
        }
        cut
    }

    let cut = cut_text_start(text, len_min, len_max);
    let mut out = String::with_capacity(cut.len() + '…'.len_utf8());
    out.push_str_in_cap(cut);
    out.push_in_cap('…');
    out.into()
}

#[test]
fn limit_text_len_test() {
    assert_eq!("hello world", limit_text_len("hello world", 100, 200));
    assert_eq!("hel…", limit_text_len("hello world", 1, 3));
    assert_eq!("こん…", limit_text_len("こんにちは、世界", 3, 5));
    assert_eq!("hello…", limit_text_len("hello world", 1, 10));
    assert_eq!("hello world…", limit_text_len("hello world! long", 1, 15));
    assert_eq!("hello (world)…", limit_text_len("hello (world) long! lorem ipsum", 1, 15));
}

/// Ructe doesn't like complex expressions…
trait MyAsStr {
    fn as_str(&self) -> Option<&str>;
}

impl<S: AsRef<str>> MyAsStr for Option<S> {
    fn as_str(&self) -> Option<&str> {
        self.as_ref().map(|s| s.as_ref())
    }
}

pub(crate) fn date_now() -> impl std::fmt::Display {
    Utc::now().format("%Y-%m-%d")
}

fn sniff_markdown(s: &str, autolink: bool) -> Option<Cow<'_, str>> {
    let looks_like_markdown = s.bytes().any(|b| b == b'`') || // code
        s.contains("](") || // url
        s.contains(" **"); // bold
    let looks_like_html = s.contains("&amp;") || s.contains("&lt;") ||
        s.split('<').filter_map(|s| s.split_once('>')).any(|(s, _)| s.starts_with('/') || s.starts_with("a ") || s.starts_with("img ") || matches!(s, "b" | "em" | "strong" | "i"));

    let has_links = s.contains("https://");

     // markdown autolinks
    if !looks_like_markdown && !looks_like_html && (!autolink || !has_links) {
        None
    } else if looks_like_html && (!autolink || !has_links) {
        Some(Cow::Borrowed(s))
    } else {
        // it might be markdown, but also may be just plaintext using things like `Vec<T>`, which shouldn't be a `<T>` HTML.
        let mut uses_code_blocks = false;
        let lines = s.lines().map(|line| {
            if line.starts_with("```") { // code blocks show &lt; literally ;(
                uses_code_blocks = true;
            }
            if uses_code_blocks || !line.contains('<') || line.starts_with("    ") || line.contains('`') || line.contains("<http") {
                Cow::Borrowed(line)
            } else {
                Cow::Owned(line.replace('<', "&lt;"))
            }
        });
        Some(Cow::Owned(lines.flat_map(|c| [c, Cow::Borrowed("\n")]).collect()))
    }
}

/// Used to render descriptions
pub(crate) fn render_maybe_markdown_str(s: &str, markup: &Renderer, base_url: Option<(&str, &str)>, own_crate_name: Option<&str>) -> templates::Html<String> {
    templates::Html(if let Some(markdown) = sniff_markdown(s, false) {
        let links = LinksContext { own_crate_name, base_url, nofollow: Links::Ugc, link_own_crate_to_crates_io: false, link_fixer: None };
        markup.markdown_str(&markdown, base_url.is_some(), &links, Instant::now() + Duration::from_secs(4))
    } else {
        use templates::ToHtml;
        let mut buf = Vec::with_capacity(s.len() + 16);
        let _ = s.to_html(&mut buf);
        String::from_utf8(buf).unwrap_or_default()
    })
}

/// Used to render comments from various weird sources that are markdown-ish or plaintext
pub(crate) fn render_maybe_markdown_paragraph(s: &str, markup: &Renderer, links_context: &LinksContext<'_>) -> templates::Html<String> {
    templates::Html(if let Some(markdown) = sniff_markdown(s, true) {
        markup.page(&Markup::Markdown(markdown.into_owned()), links_context, Instant::now() + Duration::from_secs(4)).0
    } else {
        use templates::ToHtml;
        let mut buf = Vec::with_capacity(s.len() + 32);
        buf.extend_in_cap(b"<p>");
        s.trim().lines().for_each(|line| {
            let _ = line.to_html(&mut buf);
            // people wrap for terminal and that looks bad, so keep empty lines and short lines, unless it looks more like markup/data/code than regular text
            if line.len() < 50 ||
                line.trim_start().bytes().nth(0).map_or(false, |b| !b.is_ascii_alphabetic()) ||
                line.trim_end().bytes().last().map_or(false, |b| !b.is_ascii_alphabetic()) {
                buf.extend(b"<br>");
            } else {
                buf.push(b' ');
            }
        });
        buf.extend(b"</p>");
        String::from_utf8(buf).unwrap_or_default()
    })
}

/// Nicely rounded number of downloads
///
/// To show that these numbers are just approximate.
pub(crate) fn format_downloads(num: u32) -> (String, &'static str) {
    match num {
        a @ 0..=99 => (format!("{a}"), ""),
        a @ 0..=500 => (format!("{}", a / 10 * 10), ""),
        a @ 0..=999 => (format!("{}", a / 50 * 50), ""),
        a @ 0..=9999 => (format!("{}.{}", a / 1000, a % 1000 / 100), "K"),
        a @ 0..=999_999 => (format!("{}", a / 1000), "K"),
        a => (format!("{}.{}", a / 1_000_000, a % 1_000_000 / 100_000), "M"),
    }
}

pub(crate) fn format_downloads_verbose(num: u32) -> (String, &'static str) {
    match num {
        a @ 0..=99 => (format!("{a}"), ""),
        a @ 0..=999_999 => (format!("{}", a / 1000), "thousand"),
        a => (format!("{}.{}", a / 1_000_000, a % 1_000_000 / 100_000), "million"),
    }
}


pub(crate) fn format_kbytes(bytes: u64) -> String {
    let (num, unit) = match bytes {
        0..=100_000 => ((bytes + 999) / 1000, "KB"),
        0..=800_000 => ((bytes + 3999) / 5000 * 5, "KB"),
        0..=9_999_999 => return format!("{}MB", ((bytes + 250_000) / 500_000) as f64 * 0.5),
        _ => ((bytes + 500_000) / 1_000_000, "MB"),
    };
    format!("{}{unit}", locale::Numeric::english().format_int(num))
}

pub(crate) fn url_domain(url: &str) -> Option<SmolStr> {
    let url = Url::parse(url).ok()?;
    parsed_url_domain(&url).map(SmolStr::from)
}


pub(crate) fn parsed_url_domain(url: &Url) -> Option<&str> {
    let host = url.host_str()?;
    if host.ends_with(".github.io") {
        Some("github.io")
    } else if host.ends_with(".githubusercontent.com") {
        None
    } else {
        Some(host.trim_start_matches("www."))
    }
}

pub(crate) fn is_it_arm() -> bool {
    cfg!(target_arch = "aarch64")
}


pub(crate) fn base_urls_fallback<'c, 'tmp: 'c>(ver: &'c RichCrateVersion, temp: &'tmp mut Option<String>) -> Option<(&'c str, &'c str)> {
    ver.readme().and_then(|r| r.base_urls()).or_else(move || {
        *temp = ver.repository_http_url().map(|r| r.exact_url.unwrap_or(r.project_url));
        let fallback = temp.as_deref().or_else(move || ver.homepage())?;
        Some((fallback, fallback))
    })
}

pub(crate) fn make_link_fixer<'a>(own_crate_name: Option<&'a str>, base_url: Option<(&'a str, &'a str)>, crate_exists: Option<&'a dyn for<'x> Fn(&'x str) -> bool>) -> impl Fn(&str) -> Option<(String, String)> + 'a {
    move |link_ref| {
        let mut link_trimmed = link_ref.trim().trim_matches('`').trim_start_matches('<');

        if link_trimmed.starts_with("http://") || link_trimmed.starts_with("https://") {
            link_trimmed = link_trimmed.trim_end_matches('>');
            return Some((link_trimmed.replace(' ', ""), link_trimmed.to_string()));
        }

        // probably text or array syntax
        let non_url_char = |b| b > 127 || matches!(b, b' ' | b',' | b'=' | b'+' | b'^' | b'@' | b';' | b'{' | b'"' | b'$' | b'(' | b'*');
        let is_ext = link_trimmed.starts_with('.');
        if link_trimmed.len() <= 3 || is_ext || link_trimmed.bytes().any(non_url_char) || link_trimmed.contains("..") || link_trimmed.bytes().all(|c| !c.is_ascii_alphabetic()) {
            return None;
        }

        // rustdoc type link
        if (link_ref.starts_with('`') || link_trimmed.contains("::")) && !link_trimmed.contains('/') {
            link_trimmed = link_trimmed.trim_start_matches("::");
            let tmp2;
            let doc_base = if let Some((prefix, rest)) = link_trimmed.split_once("::") {
                if prefix == "std" || prefix == "core" {
                    // search libstd
                    link_trimmed = rest;
                    "https://doc.rust-lang.org/stable/std/?search="
                } else if prefix != "crate" && crate_exists.map_or(false, |exists| (exists)(prefix)) {
                    // if it may start with crate name, search all of docs.rs
                    "https://docs.rs/releases/search?query="
                } else if let Some(name) = own_crate_name {
                    if prefix == "crate" || prefix == name {
                        link_trimmed = rest;
                    }
                    // if it's not crate-like, search own docs
                    tmp2 = format!("https://docs.rs/{name}/latest/{}/?search=", name.replace('-', "_"));
                    &tmp2
                } else {
                    // can't search other symbols without crate name
                    return None;
                }
            } else if crate_exists.map_or(false, |exists| (exists)(link_trimmed)) {
                // no :: in the name here
                "https://lib.rs/"
            } else if let Some(name) = own_crate_name {
                // search own crate for single-word query
                tmp2 = format!("https://docs.rs/{name}/latest/{}/?search=", name.replace('-', "_"));
                &tmp2
            } else {
                // lots of keywords or crate names, docs.rs can't find them all
                return None;
            };
            let url = format!("{doc_base}{}", Encoded(link_trimmed));
            return Some((url, String::from(link_ref)));
        }

        if let Some((mut base, image_base)) = base_url {
            // #123 issue github url
            if let Some(rest) = link_trimmed.strip_prefix('#') {
                if rest.bytes().all(|c| c.is_ascii_digit()) {
                    if let Some(path) = base.strip_prefix("https://github.com/") {
                        let take: usize = path.split_inclusive('/').take(2).map(|s| s.len()).sum();
                        let base = &base[.."https://github.com/".len() + take];
                        return Some((format!("{base}issues/{rest}"), String::new()));
                    }
                }
            }
            let ext = link_trimmed.rsplit_once('.').and_then(|(_, l)| l.split('?').next());
            if ext.map_or(false, |ext| ["png","svg","gif","jpg","jpeg","webp","avif","jxl"].into_iter().any(|e| e.eq_ignore_ascii_case(ext))) {
                base = image_base;
            }
            if ext.is_none() && !link_trimmed.contains('/') {
                return None;
            }
            base = base.trim_end_matches('/');
            link_trimmed = link_trimmed.trim_start_matches('/');

            return Some((format!("{base}/{link_trimmed}"), String::new()));
        }
        None
    }
}

#[inline(always)]
fn is_send<T: Send>(t: T) -> T {t}
