## What is lib.rs?

Lib.rs is a catalog of programs and libraries written in the [Rust programming language](https://www.rust-lang.org). It has $TOTAL_CRATE_NUM packages, including almost all of $CRATE_NUM crates from the [crates.io](https://crates.io) registry (with exceptions like spam, name squatting, etc.), plus a few notable projects published only on GitHub or GitLab. The site is unofficial, and not affiliated with the Rust Foundation.

## Why use lib.rs?

 * lib.rs is _fast_. There's no JavaScript anywhere.
 * It has search with an advanced ranking algorithm which promotes stable, regularly updated, popular crates, and hides spam and abandoned crates.
 * It [combines multiple data sources](/data-processing) to fix missing or low-quality metadata:
   * Finds missing READMEs and pulls in documentation from `src/lib.rs`
   * Automatically categorizes crates and adds missing keywords, to improve browsing by categories and keywords.
   * Accurately shows which dependencies are out of date or deprecated.
   * Highlights which crates require nightly compiler or use non-Rust code.
   * Has a detailed reverse dependencies page, including version fragmentation and popularity over time.
   * Shows size of each crate with its dependencies.
   * Integrates with RustSec advisory database, cargo-vet, and cargo-crev reviews.
   * Automatically finds and credits co-authors based on git history.
 * It has short URLs to open a crate page `lib.rs/crate-name` and search `lib.rs?keyword`.
 * Shows similar/related crates on each crate page, which helps discovering better alternatives.
 * Has a dark theme (it's automatic, needs to be selected by your browser or OS).

## Ranking algorithm

Sorting crates just by their download numbers has some downsides:

 * it tends to favor old crates and incumbents, and makes it more difficult for new, high-quality crates to gain users.
 * crates.io download counter has no throttling nor anti-spam measures, so the metric can be easily inflated (not only maliciously, but by uncached CI builds too).
 * internal and obscure crates that happen to be a dependency of a popular crate ride on its popularity, despite not being interesting themselves.

To fix these issues, lib.rs uses algorithm that uses a combination of many quality signals. The algorithm has been designed based on research for [RFC 1824](https://github.com/rust-lang/rfcs/blob/master/text/1824-crates.io-default-ranking.md), feedback from Rust users, as well as inspired by Open Hub analysis, SourceRank, CocoaPods' QualityIndex, and npm search.

Crates are sorted by their overall quality score, which is a weighed combination of:

 * The crate's popularity measured by number of downloads, but only as a small fraction of the score, and with some filtering of noise and obviously spammed crates,
 * Number of direct and indirect reverse dependencies. The numbers are corrected for biases that affect application crates and dependencies of dependencies.
 * The crate's usage trend — is it gaining or losing users.
 * Availability of the crate's documentation, examples, and length and quality of the README.
 * Stability estimated from release history, number of breaking versions, patch versions, and use of nightly features.
 * Presence of tests, CI, code comments.
 * Accuracy and completeness of the crate's metadata.
 * Number of authors and contributors.
 * Weight of the crate's unique dependencies (taking into account that some crates are very common and shared between projects).
 * Whether the crate is actively maintained or at least stable and done, based on release frequency, age, maintenance status, use of deprecated/outdated dependencies, non-0.x releases, etc.
 * Reputation of maintainers of the crate, calculated page-rank-like based on co-owning relationships.
 * Plus special-case factors and blocklists for undesirable or junk crates.

The score is combined with relevance of crate's keywords to a given category or a search query. Overall, this algorithm has proven to be very good at [discovering quality crates](https://lib.rs/std). However, there are always edge cases. If you find poor results, [please report them](https://forms.gle/SFntxLhGJB7xzFy19).

## Dependency freshness

Versions are considered out of date not merely when they're less than the latest, but when *more than half of users of that crate uses a newer version*. This way lib.rs only warns about crates that are really lagging behind, and doesn't complain about minor / irrelevant / experimental / unupgradable versions of dependencies.

Crate pages highlight out-of-date dependency versions:

* If there's no version number, the crate uses the latest, most popular version of the dependency.
* If there's a version number in black, the crate uses the latest version of the dependency, which is newer than the most popular version of the dependency.
* If there's a version number in orange, the crate uses a slightly out-of-date version of the dependency.
* If there's a version number in bold dark red, the crate uses an outdated or deprecated version of the dependency.


## Policies

lib.rs is not a package registry itself, only a view into crates published elsewhere. Majority of the crates come from crates-io, and therefore are subject to their policies. All crates are subject to Rust's [Code of Conduct](https://www.rust-lang.org/policies/code-of-conduct).

While lib.rs shows almost all crates from crates-io, it reserves the right to omit/hide crates it deems inappropriate or were subject of a takedown request. The site disapproves of cryptocurrencies, and such crates may be downranked or hidden. Presence of crates on lib.rs is not an endorsement (they're allowed by default). [Policies on data processing](/data-processing).

### Name squatting

lib.rs disapproves of name squatting (registering crate names for purpose of holding/blocking a name, rather than to publish an actual crate). This site will downrank or hide squatted crates, and ban accounts that primarily exist to squat names. Squatting is hard to define, but here are some exceptions:

* It's okay to reserve crate names with a prefix (`foo-something`) if you own the crate that is the prefix (`foo`).
* It's okay to reserve crate names of projects or trademarks that you have already established elsewhere (e.g. register `glados` if you're the Aperture Labs).
* It's okay to reserve a few crates that you genuinely plan to release soon. When you do, please use `0.0.0-reserved` version and include a way to contact you that will work long-term.
