use util::PushString;
use ahash::HashSet;
use std::borrow::Cow;
use std::collections::BTreeMap;
use toml::value::{Table, Value};
use once_cell::sync::Lazy;
use quick_error::quick_error;
use serde::Deserialize;
use util::SmolStr;
use util::CowAscii;

mod tuning;
pub use crate::tuning::*;
mod synonyms;
pub use crate::synonyms::*;

const CATEGORIES_TOML: &str = include_str!("categories.toml");

#[derive(Debug, Clone)]
pub struct Categories {
    pub root: CategoryMap,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct Category {
    pub name: SmolStr,
    pub description: String,
    pub short_description: String,
    pub standalone_name: Option<SmolStr>,
    pub title: String,
    pub slug: String,
    // fudge factor for selecting primary category (how useful and specific it is)
    pub preference: f32,
    pub sub: CategoryMap,
    pub siblings: Vec<SmolStr>,
    pub obvious_keywords: Vec<SmolStr>,
}

pub type CategoryMap = BTreeMap<String, Category>;
pub type CResult<T> = Result<T, CatError>;

quick_error! {
    #[derive(Debug, Clone)]
    pub enum CatError {
        MissingField(f: &'static str) {
            display("missing {}", f)
            from()
        }
        Parse(err: toml::de::Error) {
            display("Categories parse error: {}", err)
            from()
            source(err)
        }
    }
}

pub static CATEGORIES: Lazy<Categories> = Lazy::new(|| Categories::new().expect("built-in categories"));

impl Categories {
    fn new() -> CResult<Self> {
        Ok(Self {
            root: Self::categories_from_table("", toml::from_str(CATEGORIES_TOML)?)?
        })
    }

    /// true if full match
    pub fn from_slug<S: AsRef<str>>(&self, slug: S) -> (Vec<&Category>, bool) {
        let mut out = Vec::new();
        let mut cats = &self.root;
        for name in slug.as_ref().split("::") {
            match cats.get(name) {
                Some(cat) => {
                    cats = &cat.sub;
                    out.push(cat);
                },
                None => return (out, false),
            }
        }
        (out, true)
    }

    fn categories_from_table(full_slug_start: &str, toml: Table) -> CResult<CategoryMap> {
        toml.into_iter().map(|(slug, details)| {
            let mut details: Table = details.try_into()?;
            let name = details.remove("name").ok_or("name")?.try_into()?;
            let description = details.remove("description").ok_or("description")?.try_into()?;
            let short_description = details.remove("short-description").ok_or("description")?.try_into()?;
            let title = details.remove("title").ok_or("title")?.try_into()?;
            let standalone_name = details.remove("standalone-name").and_then(|v| v.try_into().ok());
            let obvious_keywords = details.remove("obvious-keywords").and_then(|v| v.try_into().ok()).unwrap_or_default();
            let preference = details.remove("preference").ok_or("preference")?.try_into()?;
            let siblings = details.remove("siblings").and_then(|v| v.try_into().ok()).unwrap_or_default();

            let mut full_slug = String::with_capacity(full_slug_start.len() + 2 + slug.len());
            if !full_slug_start.is_empty() {
                full_slug.push_str_in_cap(full_slug_start);
                full_slug.push_str_in_cap("::");
            }
            full_slug.push_str_in_cap(&slug);

            let sub = if let Some(Value::Table(table)) = details.remove("categories") {
                Self::categories_from_table(&full_slug, table)?
            } else {
                CategoryMap::new()
            };
            Ok((slug, Category {
                name,
                title,
                short_description,
                standalone_name,
                description,
                preference,
                slug: full_slug,
                sub,
                siblings,
                obvious_keywords,
            }))
        }).collect()
    }


    pub fn fixed_category_slugs<'a>(cats: &'a [String], invalid_lib_rs_categories: &mut Vec<SmolStr>) -> Vec<Cow<'a, str>> {
        let mut cats = cats.iter().enumerate()
        .filter_map(|(idx, orig_name)| {
            let orig_name = orig_name.as_str();
            let s = orig_name.trim().trim_matches(':');
            let mut chars = s.chars().peekable();
            while let Some(cur) = chars.next() {
                // look for a:b instead of a::b
                if cur == ':' && chars.peek().map_or(false, |&c| c == ':') {
                    chars.next(); // OK, skip second ':'
                    continue;
                }
                if cur == '-' || cur.is_ascii_lowercase() || cur.is_ascii_digit() {
                    continue;
                }

                // bad syntax! Fix!
                let slug = s.as_ascii_lowercase().split(':').filter(|s| !s.is_empty()).collect::<Vec<_>>().join("::");
                if slug.is_empty() {
                    return None;
                }
                let depth = slug.split("::").count();
                return Some((depth, idx, slug.into(), orig_name));
            }
            let depth = s.split("::").count();
            Some((depth, idx, Cow::Borrowed(s), orig_name))
        })
        .filter(|(_, _, s, orig_name)| {
            if s.len() < 2 {
                return false;
            }
            // mapping of crates.io -> lib.rs categories is done elsewhere
            if s == "external-ffi-bindings" { // We pretend it doesn't exist
                return false;
            }
            if !CATEGORIES.from_slug(s).1 {
                // this is invalid for lib.rs, but may contain slus valid for crates.io
                invalid_lib_rs_categories.push((*orig_name).into());
                return false;
            }
            true
        })
        .map(|(a,b,c,_)| (a,b,c))
        .collect::<Vec<_>>();

        // depth, then original order
        cats.sort_by(|a, b| b.0.cmp(&a.0).then(a.1.cmp(&b.1)));

        cats.into_iter().map(|(_, _, c)| c).collect()
    }
}

impl Category {
    #[must_use] pub fn standalone_name(&self) -> &str {
        self.standalone_name.as_ref().unwrap_or(&self.name).as_str()
    }
}


// deps that are closely related to crates in some category
#[inline(never)]
#[must_use] pub fn dep_interesting_for_index(name: &str) -> Option<bool> {
    match name {
        "futures" | "async-trait" | "tokio" | "actix-web" | "warp" | "rocket_codegen" | "iron" | "rusoto_core" | "rocket" | "router" | "async-std" |
        "constant_time_eq" | "digest" | "subtle" |
        "quoted_printable" | "mime" | "rustls" | "websocket" | "hyper" |
        "piston2d-graphics" | "amethyst_core" | "amethyst" | "specs" | "piston" | "allegro" | "minifb" | "bevy" |
        "rgb" | "imgref" | "gstreamer" | "gtk" | "gtk4" |
        "bare-metal" | "usb-device" |
        "core-foundation" |
        "proc-macro2" | "proc-macro-hack" | "darling" | "quote" |
        "cargo" | "cargo_metadata" | "git2" | "dbus" |
        "hound" | "lopdf" |
        "nom" | "lalrpop" | "combine" | "pest" | "unicode-xid" |
        "clap" | "structopt" | "ansi_term" |
        "alga" | "bio" | "nalgebra" |
        "syntect" | "stdweb" | "parity-wasm" | "wasm-bindgen" |
        "solana-program" | "ethabi" | "bitcoin" | "ink_primitives" | "parity-scale-codec" | "ethnum" | "borsh" | "solana-sdk" | "anchor-lang" | "mpl-token-metadata" | "spl-token" => Some(true),
        /////////
        "threadpool" | "rayon" | "md5" | "arrayref" | "memmmap" | "xml" | "crossbeam" | "pyo3" |
        "rustc_version" | "crossbeam-channel" | "cmake" | "errno" | "zip" | "enum_primitive" | "pretty_env_logger" |
        "skeptic" | "crc" | "hmac" | "sha1" | "serde_macros" | "serde_codegen" | "derive_builder" |
        "derive_more" | "ron" | "fxhash" | "simple-logger" | "chan" | "stderrlog" => Some(false),
        _ => {
            static DEP_KEYS: Lazy<HashSet<&str>> = Lazy::new(|| tuning::KEYWORD_CATEGORIES.iter().flat_map(|(c, _)| match c {
                    Cond::Any(c) | Cond::All(c) | Cond::NotAny(c) => c.iter().filter_map(|c| c.strip_prefix("dep:")),
                }).collect());
            if DEP_KEYS.contains(name) { Some(true) } else { None }
        },
    }
}


#[test]
fn cat() {
    assert!(CATEGORIES.from_slug("database").1);

    Categories::new().expect("categories").root.get("parsing").expect("parsing");

    CATEGORIES.root.get("development-tools").expect("development-tools").sub.get("build-utils").expect("build-utils");
}
