use heck::AsKebabCase;
use ahash::HashMapExt;
use ahash::HashMap;
use util::smol_fmt;
use std::collections::hash_map::Entry;
use std::path::Path;
use std::io;
use std::fs;
use util::SmolStr;
use log::debug;

pub struct Synonyms {
    mapping: HashMap<SmolStr, (SmolStr, u8)>,
}

impl Synonyms {
    pub fn new(data_dir: &Path) -> io::Result<Self> {
        let lines = fs::read_to_string(data_dir.join("tag-synonyms.csv"))?;
        let mut mapping = HashMap::<SmolStr, (SmolStr, u8)>::with_capacity(2500);
        for l in lines.lines().filter(|l| !l.starts_with('#')) {
            let fail = move || io::Error::new(io::ErrorKind::InvalidData, l);
            let mut cols = l.splitn(3, ',');
            let find = SmolStr::from(cols.next().ok_or_else(fail)?);
            let replace = SmolStr::from(cols.next().ok_or_else(fail)?);
            let score: u8 = cols.next().and_then(|p| p.parse().ok()).ok_or_else(fail)?;
            match mapping.entry(find) {
                Entry::Occupied(mut e) => if e.get().1 < score {
                    debug!("duplicate synonym {l} and {}", e.get().0);
                    e.insert((replace, score));
                },
                Entry::Vacant(e) => { e.insert((replace, score)); }
            }
        }
        Ok(Self { mapping })
    }

    #[inline]
    #[must_use] pub fn get(&self, keyword: &str) -> Option<(&str, f32)> {
        let (tag, votes) = self.mapping.get(keyword)?;
        let relevance = (f32::from(*votes) / 5. + 0.1).min(1.);
        Some((tag, relevance))
    }

    fn get_matching(&self, keyword: &str) -> Option<&str> {
        let (tag, votes) = self.mapping.get(keyword)?;
        if *votes > 0 {
            return Some(tag)
        }
        None
    }

    #[must_use] pub fn normalize<'a>(&'a self, keyword: &'a str) -> &'a str {
        if let Some(alt) = self.get_matching(keyword) {
            if let Some(alt2) = self.get_matching(alt) {
                return alt2;
            }
            return alt;
        }
        keyword
    }
}

#[must_use]
pub fn normalize_keyword(k: &str) -> SmolStr {
    let mut k: SmolStr = k.trim_end_matches("'s").chars().filter(|&c| c != '\'' && c != '`').collect();

    // heck messes up CJK
    if !k.is_ascii() {
        return k.chars().flat_map(|c| if c.is_ascii_punctuation() || c.is_ascii_whitespace() { '-' } else { c }.to_lowercase()).collect();
    }

    // SSDs, APIs, etc.
    let mut chars = k.chars();
    if let Some((last, before_last)) = chars.next_back().zip(chars.next_back()) {
        if k.len() > 2 && last == 's' && before_last.is_ascii_uppercase() {
            k = k.trim_end_matches('s').into();
        }
    }

    let mut chars = k.chars();
    if let Some((first, second)) = chars.next().zip(chars.next()) {
        if first.is_ascii_lowercase() && second.is_ascii_uppercase() {
            k.make_ascii_lowercase();
        }
    }

    // i-os and java-script look bad
    if k.starts_with("IoT") || k.starts_with("LaTeX") || k.ends_with("Script") {
        k.make_ascii_lowercase();
    }
    smol_fmt!("{}", AsKebabCase(k.as_str()))
}

#[test]
fn loads_data_file() {
    Synonyms::new("../data/".as_ref()).unwrap();
}
