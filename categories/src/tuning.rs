#![allow(clippy::type_complexity)]

use log::{trace, debug};
use util::sort_top_n_unstable_by;
use crate::CATEGORIES;
use ahash::HashMap;
use ahash::HashSet;
use ahash::HashSetExt;
use std::collections::BTreeMap;
use std::collections::hash_map::Entry;
use util::SmolStr;

/// If one is present, adjust score of a category
///
/// `keyword: [(slug, multiply, add)]`
pub(crate) static KEYWORD_CATEGORIES: &[(Cond, &[(&str, f64, f64)])] = &[
        (Cond::Any(&["no-std", "no_std"]), &[("no-std", 1.4, 0.15), ("command-line-utilities", 0.5, 0.), ("cryptography::cryptocurrencies", 0.9, 0.)]),
        // derived from features
        (Cond::Any(&["feature:no_std", "feature:no-std", "heapless", "no-alloc"]), &[("no-std", 1.2, 0.05)]),
        (Cond::Any(&["feature:std", "feature:no-stdlib", "feature:alloc"]), &[("no-std", 1.5, 0.1)]),
        (Cond::Any(&["print", "font", "parsing", "hashmap", "money", "flags", "data-structure", "cache", "macros", "wasm", "emulator", "hash"]), &[("no-std", 0.6, 0.)]),

        (Cond::Any(&["winsdk", "winrt", "directx", "dll", "win32", "ntfs", "winutil", "msdos", "winapi", "gdiplus", "ntapi", "mscorlib"]),
            &[("os::windows-apis", 1.5, 0.1), ("os::unix-apis", 0.9, 0.), ("os::macos-apis", 0.9, 0.), ("parser-implementations", 0.9, 0.),
            ("text-processing", 0.9, 0.), ("text-editors", 0.8, 0.), ("no-std", 0.9, 0.), ("config", 0.9, 0.)]),
        (Cond::All(&["windows", "ffi"]), &[("os::windows-apis", 1.1, 0.1), ("parsing", 0.1, 0.), ("memory-management", 0.9, 0.)]),
        (Cond::Any(&["windows", "msvcrt"]), &[("os::windows-apis", 1.1, 0.1), ("parsing", 0.1, 0.), ("text-processing", 0.8, 0.)]),
        (Cond::All(&["ffi", "winsdk"]), &[("os::windows-apis", 1.9, 0.5), ("no-std", 0.5, 0.), ("parsing", 0.8, 0.), ("science::math", 0.9, 0.)]),
        (Cond::All(&["ffi", "windows"]), &[("os::windows-apis", 1.2, 0.2), ("parsing", 0.1, 0.)]),
        (Cond::All(&["ffi", "com"]), &[("os::windows-apis", 1.2, 0.1), ("parsing", 0.1, 0.), ("development-tools::ffi", 1.2, 0.1)]),
        (Cond::All(&["abi", "com"]), &[("os::windows-apis", 1.1, 0.), ("development-tools::ffi", 1.1, 0.05)]),
        (Cond::All(&["winrt", "com"]), &[("os::windows-apis", 1.2, 0.2)]),
        (Cond::All(&["windows", "uwp"]), &[("os::windows-apis", 1.2, 0.2)]),
        (Cond::All(&["microsoft", "api"]), &[("os::windows-apis", 1.2, 0.2), ("parsing", 0.1, 0.)]),
        (Cond::Any(&["winauth", "ntlm"]), &[("os::windows-apis", 1.25, 0.2), ("authentication", 1.3, 0.2)]),
        (Cond::Any(&["winauth", "ntlm", "windows-runtime"]), &[("os::windows-apis", 1.25, 0.2), ("authentication", 1.3, 0.2)]),
        (Cond::Any(&["dotnet", "csharp"]), &[("os::windows-apis", 1.2, 0.1), ("development-tools", 1.1, 0.), ("development-tools::ffi", 1.1, 0.)]),
        (Cond::Any(&["dep:core-foundation-sys", "dep:core-graphics", "dep:coreaudio-sys", "dep:apple-sys", "dep:cocoa-foundation", "dep:core-video-sys"]), &[("os::macos-apis", 1.1, 0.05)]),

        (Cond::Any(&["windows", "winsdk", "win32", "activex"]), &[("os::macos-apis", 0., 0.), ("os::unix-apis", 0., 0.), ("science::math", 0.8, 0.),
            ("memory-management", 0.9, 0.), ("parsing", 0.5, 0.), ("config", 0.9, 0.)]),
        (Cond::Any(&["macos", "osx", "ios", "cocoa", "tvos", "watchos", "erlang"]), &[("os::windows-apis", 0.1, 0.), ("no-std", 0.01, 0.), ("parsing", 0.8, 0.)]),
        (Cond::Any(&["macos", "osx", "cocoa", "mach-o", "uikit", "appkit"]), &[("os::macos-apis", 1.4, 0.2), ("os::unix-apis", 0.9, 0.), ("science::math", 0.75, 0.)]),
        (Cond::All(&["os", "x"]), &[("os::macos-apis", 1.2, 0.)]),
        (Cond::All(&["mac", "bindings"]), &[("os::macos-apis", 1.2, 0.), ("parsing", 0.8, 0.)]),
        (Cond::Any(&["dmg", "fsevents", "fseventsd", "tvos", "watchos"]), &[("os::macos-apis", 1.2, 0.1), ("parsing", 0.1, 0.), ("os::unix-apis", 0.9, 0.)]),
        (Cond::All(&["macos", "apis"]), &[("os::macos-apis", 1.15, 0.05)]),
        (Cond::All(&["core", "foundation"]), &[("os::macos-apis", 1.2, 0.1), ("os", 0.8, 0.), ("concurrency", 0.3, 0.)]),
        (Cond::Any(&["core-module"]), &[("os::macos-apis", 0.5, 0.)]),
        (Cond::Any(&["core-library"]), &[("os::macos-apis", 0.5, 0.), ("games", 0.5, 0.)]),
        (Cond::Any(&["corefoundation"]), &[("os::macos-apis", 1.2, 0.1), ("os", 0.8, 0.), ("concurrency", 0.3, 0.), ("os::unix-apis", 0.9, 0.), ("os::windows-apis", 0.9, 0.)]),
        (Cond::All(&["mac"]), &[("os::macos-apis", 1.1, 0.)]),
        (Cond::Any(&["keycode", "platforms", "platform", "processor", "child", "system", "executable", "processes"]),
            &[("os", 1.2, 0.1), ("network-programming", 0.7, 0.), ("cryptography", 0.5, 0.), ("date-and-time", 0.8, 0.),
            ("games", 0.7, 0.), ("authentication", 0.6, 0.), ("internationalization", 0.7, 0.)]),
        (Cond::Any(&["mount", "package", "uname", "boot", "kernel"]),
            &[("os", 1.2, 0.1), ("network-programming", 0.7, 0.), ("cryptography", 0.5, 0.), ("date-and-time", 0.8, 0.),
            ("games", 0.7, 0.), ("multimedia", 0.8, 0.), ("multimedia::video", 0.7, 0.),
            ("rendering::engine", 0.8, 0.), ("rendering", 0.8, 0.), ("authentication", 0.6, 0.), ("internationalization", 0.7, 0.)]),
        (Cond::Any(&["dependency-manager", "package-manager", "debian-packaging", "clipboard", "process", "bootloader", "taskbar", "microkernel", "multiboot"]),
            &[("os", 1.2, 0.1), ("network-programming", 0.8, 0.), ("multimedia::video", 0.7, 0.),  ("multimedia", 0.7, 0.), ("caching", 0.9, 0.), ("cryptography", 0.7, 0.), ("filesystem", 0.8, 0.), ("games", 0.2, 0.), ("authentication", 0.6, 0.),
            ("internationalization", 0.7, 0.)]),
        (Cond::All(&["package", "manager"]), &[("os", 1.2, 0.1), ("development-tools", 1.2, 0.1), ("parsing", 0.1, 0.)]),
        (Cond::All(&["packaging", "podman"]), &[("os", 1.2, 0.), ("development-tools", 1.2, 0.1)]),
        (Cond::All(&["device", "configuration"]), &[("os", 1.2, 0.2), ("config", 0.9, 0.)]),
        (Cond::Any(&["os", "hostname"]), &[("os", 1.2, 0.1), ("data-structures", 0.6, 0.), ("no-std", 0.6, 0.)]),
        (Cond::All(&["shared", "library"]), &[("os", 1.2, 0.2), ("games", 0.8, 0.), ("no-std", 0.3, 0.), ("config", 0.9, 0.)]),
        (Cond::Any(&["dlopen"]), &[("os", 1.2, 0.2), ("no-std", 0.3, 0.), ("config", 0.8, 0.)]),
        (Cond::Any(&["library"]), &[("games", 0.8, 0.), ("development-tools::cargo-plugins", 0.8, 0.)]),
        (Cond::Any(&["ios", "objective-c", "core-foundation"]), &[("os::macos-apis", 1.2, 0.1), ("no-std", 0.1, 0.)]),
        (Cond::Any(&["linux", "freebsd", "openbsd", "netbsd", "dragonflybsd", "arch-linux", "pacman", "deb", "appimage", "rpm", "freebsd-apis", "linux-apis", "os-freebsd-apis", "os-linux-apis"]),
            &[("os", 1.1, 0.), ("os::unix-apis", 1.4, 0.1), ("os::macos-apis", 0.3, 0.), ("os::windows-apis", 0.1, 0.)]),
        (Cond::Any(&["sudo", "sudoers"]),
            &[("os", 1.1, 0.), ("os::unix-apis", 1.4, 0.1)]),
        (Cond::Any(&["bpf"]), &[("os", 1.1, 0.), ("os::unix-apis", 1.3, 0.1)]),
        (Cond::Any(&["ebpf", "libbpf"]), &[("os", 1.1, 0.), ("os::unix-apis", 1.3, 0.1), ("os::macos-apis", 0.7, 0.), ("parsing", 0.1, 0.)]),
        (Cond::Any(&["glib", "gobject", "gdk"]), &[("os", 1.15, 0.), ("gui", 1.1, 0.), ("parsing", 0.8, 0.),  ("hardware-support", 0.8, 0.), ("os::unix-apis", 1.4, 0.15)]),
        (Cond::Any(&["fedora", "centos", "redhat", "debian"]),
            &[("os::unix-apis", 1.3, 0.1), ("os::macos-apis", 0.1, 0.), ("os::windows-apis", 0.1, 0.)]),
        (Cond::All(&["linux", "kernel"]), &[("os::unix-apis", 1.3, 0.2), ("multimedia", 0.7, 0.)]),
        (Cond::All(&["api", "kernel"]), &[("os::unix-apis", 1.1, 0.), ("os", 1.1, 0.)]),
        (Cond::Any(&["dylib"]), &[("os", 1.1, 0.), ("os::windows-apis", 0.6, 0.), ("parsing", 0.4, 0.)]),
        (Cond::Any(&["so"]), &[("os", 1.1, 0.), ("os::windows-apis", 0.6, 0.), ("os::macos-apis", 0.6, 0.)]),
        (Cond::Any(&["os", "cpuinfo"]), &[("os", 1.1, 0.), ("data-structures", 0.9, 0.)]),
        (Cond::Any(&["dll"]), &[("os", 1.1, 0.), ("os::unix-apis", 0.6, 0.), ("os::macos-apis", 0.6, 0.)]),
        (Cond::Any(&["redox", "rtos", "embedded", "hard-real-time"]), &[("os", 1.2, 0.1), ("gui", 0.8, 0.), ("rust-patterns", 0.8, 0.), ("encoding", 0.9, 0.), ("os::macos-apis", 0., 0.), ("os::windows-apis", 0., 0.)]),
        (Cond::Any(&["rtos", "embedded", "microkernel", "hard-real-time", "tockos", "embedded-linux", "embedded-operating-system"]), &[("embedded", 1.3, 0.1), ("science::math", 0.7, 0.)]),
        (Cond::All(&["operating", "system"]), &[("os", 1.2, 0.2)]),
        (Cond::All(&["unix", "signal"]), &[("os::unix-apis", 1.2, 0.), ("os", 1.2, 0.2), ("multimedia::video", 0.5, 0.)]),
        (Cond::All(&["system", "signal"]), &[("os", 1.2, 0.2), ("multimedia::video", 0.5, 0.)]),
        (Cond::All(&["signal", "handler"]), &[("os", 1.2, 0.2), ("multimedia::video", 0.5, 0.), ("multimedia::audio", 0.5, 0.)]),
        (Cond::Any(&["signal-handling"]),
            &[("os::unix-apis", 1.2, 0.), ("date-and-time", 0.4, 0.), ("memory-management", 0.8, 0.), ("games", 0.6, 0.),
            ("gui", 0.9, 0.), ("game-development", 0.8, 0.), ("multimedia::images", 0.5, 0.), ("multimedia::video", 0.5, 0.),
            ("command-line-utilities", 0.7, 0.), ("development-tools", 0.8, 0.), ("science::math", 0.7, 0.)]),
        (Cond::Any(&["autotools", "ld_preload", "libnotify", "syslog", "rfc5424", "systemd", "seccomp", "kill", "ebpf"]),
            &[("os::unix-apis", 1.2, 0.05), ("date-and-time", 0.1, 0.), ("memory-management", 0.6, 0.), ("embedded", 0.8, 0.), ("hardware-support", 0.9, 0.), ("template-engine", 0.8, 0.),
            ("parsing", 0.5, 0.), ("games", 0.2, 0.), ("multimedia::audio", 0.5, 0.), ("gui", 0.9, 0.), ("game-development", 0.6, 0.), ("multimedia::audio", 0.5, 0.),
            ("multimedia::images", 0.2, 0.), ("no-std", 0.8, 0.), ("accessibility", 0.8, 0.), ("command-line-utilities", 0.8, 0.), ("science::math", 0.6, 0.)]),
        (Cond::Any(&["epoll", "affinity", "sigint", "syscall", "ioctl", "unix-socket", "unix-sockets"]),
            &[("os::unix-apis", 1.3, 0.1), ("date-and-time", 0.1, 0.), ("memory-management", 0.6, 0.), ("games", 0.2, 0.), ("multimedia::audio", 0.5, 0.),
            ("gui", 0.8, 0.), ("game-development", 0.6, 0.), ("multimedia::images", 0.2, 0.),
            ("command-line-utilities", 0.6, 0.), ("development-tools", 0.9, 0.), ("science::math", 0.6, 0.)]),
        (Cond::Any(&["arch-linux", "unix", "coreutils", "archlinux", "docker", "pacman", "systemd", "posix", "x11", "epoll"]),
            &[("os::unix-apis", 1.2, 0.2), ("no-std", 0.5, 0.), ("multimedia::audio", 0.8, 0.), ("os::windows-apis", 0.7, 0.), ("cryptography", 0.8, 0.), ("cryptography::cryptocurrencies", 0.6, 0.)]),
        (Cond::Any(&["users"]), &[("os::unix-apis", 1.1, 0.), ("caching", 0.8, 0.)]),
        (Cond::Any(&["nix", "nix-flake", "nixos", "nixpkgs"]), &[("os::unix-apis", 1.2, 0.1), ("os::macos-apis", 0.9, 0.), ("os::windows-apis", 0.7, 0.), ("development-tools::build-utils", 1.1, 0.)]),
        (Cond::Any(&["hypervisor", "hyper-v", "efi"]), &[("os", 1.1, 0.1), ("hardware-support", 1.1, 0.1)]),
        (Cond::Any(&["docker", "docker-compose", "containerize", "containerized"]),
            &[("development-tools", 1.2, 0.1), ("web-programming", 1.1, 0.02), ("os::macos-apis", 0.9, 0.),
            ("config", 0.8, 0.), ("os::windows-apis", 0.1, 0.), ("command-line-utilities", 1.1, 0.02)]),
        (Cond::Any(&["kubernetes", "containerized", "k8s", "devops", "hypervisor"]),
            &[("development-tools", 1.2, 0.1), ("web-programming", 1.1, 0.02), ("web-programming::http-client", 0.9, 0.), ("no-std", 0.9, 0.), ("embedded", 0.75, 0.),
            ("os::macos-apis", 0.8, 0.), ("config", 0.9, 0.), ("algorithms", 0.8, 0.), ("parsing", 0.4, 0.), ("os::windows-apis", 0.1, 0.), ("command-line-utilities", 1.1, 0.02)]),
        (Cond::All(&["container", "tool"]), &[("development-tools", 1.2, 0.1)]),
        (Cond::All(&["deploy", "deployment"]), &[("development-tools", 1.2, 0.1), ("web-programming", 1.1, 0.02)]),
        (Cond::All(&["containers", "build"]), &[("development-tools", 1.2, 0.1), ("embedded", 0.9, 0.)]),
        (Cond::Any(&["ios"]), &[("development-tools::profiling", 0.8, 0.), ("os::windows-apis", 0.1, 0.), ("no-std", 0.9, 0.), ("development-tools::cargo-plugins", 0.8, 0.)]),
        (Cond::Any(&["android"]), &[("os::macos-apis", 0.5, 0.), ("os::windows-apis", 0.7, 0.), ("os::unix-apis", 0.9, 0.), ("development-tools::profiling", 0.9, 0.)]),
        (Cond::Any(&["cross-platform", "portable"]), &[("os::macos-apis", 0.25, 0.), ("os::windows-apis", 0.25, 0.), ("os::unix-apis", 0.25, 0.)]),
        (Cond::All(&["cross", "platform"]), &[("os::macos-apis", 0.25, 0.), ("os::windows-apis", 0.25, 0.), ("os::unix-apis", 0.25, 0.)]),
        (Cond::All(&["freebsd", "windows"]), &[("os::macos-apis", 0.6, 0.), ("os::windows-apis", 0.8, 0.), ("os::unix-apis", 0.8, 0.)]),
        (Cond::All(&["linux", "windows"]), &[("os::macos-apis", 0.5, 0.), ("os::windows-apis", 0.8, 0.), ("os::unix-apis", 0.9, 0.), ("parsing", 0.6, 0.)]),
        (Cond::All(&["macos", "windows"]), &[("os::macos-apis", 0.9, 0.), ("os::windows-apis", 0.5, 0.), ("os::unix-apis", 0.5, 0.), ("no-std", 0.9, 0.)]),
        (Cond::All(&["ios", "bindings"]), &[("os::macos-apis", 1.2, 0.1)]),
        (Cond::NotAny(&["ios", "objective-c", "objc", "obj-c", "objrs", "hfs", "osx", "os-x", "dylib", "mach", "xcode", "uikit", "appkit", "metal", "foundation", "macos", "mac", "apple", "cocoa"]), &[("os::macos-apis", 0.7, 0.)]),
        (Cond::Any(&["dep:core-graphics"]), &[("os::windows-apis", 0.4, 0.), ("os::unix-apis", 0.6, 0.)]),

        (Cond::NotAny(&["has:is_sys", "ffi", "sys", "bindings", "c", "libc", "libffi", "cstr", "python", "ruby", "lua", "jvm", "erlang", "unsafe"]),
            &[("development-tools::ffi", 0.7, 0.)]),
        (Cond::Any(&["ffi", "n-api"]), &[("development-tools::ffi", 1.2, 0.1), ("games", 0.1, 0.), ("filesystem", 0.9, 0.), ("compilers", 0.9, 0.)]),
        (Cond::Any(&["abi"]), &[("development-tools::ffi", 1.2, 0.2), ("compilers", 1.1, 0.), ("rust-patterns", 1.1, 0.)]),
        (Cond::Any(&["swig", "autocxx"]), &[("development-tools::ffi", 1.2, 0.2)]),
        (Cond::Any(&["sys"]), &[("development-tools::ffi", 0.9, 0.), ("games", 0.4, 0.), ("asynchronous", 0.9, 0.), ("rendering::engine", 0.8, 0.), ("rendering", 0.8, 0.), ("multimedia", 0.9, 0.)]),
        (Cond::Any(&["has:is_sys"]), &[("development-tools::ffi", 0.1, 0.), ("no-std", 0.7, 0.), ("compilers", 0.7, 0.), ("algorithms", 0.8, 0.),
            ("data-structures", 0.7, 0.), ("development-tools::testing", 0.4, 0.), ("development-tools::build-utils", 0.4, 0.),
            ("development-tools::debugging", 0.8, 0.), ("web-programming::websocket", 0.6, 0.), ("database-implementations", 0.6, 0.),
            ("games", 0.2, 0.), ("filesystem", 0.9, 0.), ("multimedia", 0.8, 0.), ("cryptography", 0.8, 0.), ("command-line-utilities", 0.2, 0.)]),
        (Cond::Any(&["bindgen"]), &[("development-tools::ffi", 1.5, 0.2)]),
        (Cond::All(&["napi", "has:is_sys"]), &[("development-tools::ffi", 1.2, 0.1), ("parsing", 0.7, 0.)]),
        (Cond::All(&["ffi", "has:is_dev"]), &[("development-tools::ffi", 1.2, 0.1), ("parsing", 0.7, 0.)]),
        (Cond::All(&["ffi", "has:is_build"]), &[("development-tools::ffi", 1.2, 0.1)]),
        (Cond::All(&["interface", "api"]), &[("games", 0.2, 0.), ("algorithms", 0.8, 0.)]),
        (Cond::Any(&["encoder", "gamedev", "pdf"]), &[("development-tools::ffi", 0.7, 0.), ("rust-patterns", 0.7, 0.)]),
        (Cond::Any(&["bindings", "api-bindings", "binding", "ffi-bindings", "wrapper", "api-wrapper"]),
            &[("development-tools::ffi", 0.8, 0.), ("database-implementations", 0.8, 0.), ("games", 0.2, 0.), ("command-line-utilities", 0.2, 0.),
            ("no-std", 0.9, 0.), ("development-tools::cargo-plugins", 0.7, 0.), ("rust-patterns", 0.8, 0.), ("config", 0.8, 0.), ("rendering::data-formats", 0.2, 0.),
            ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.6, 0.)]),
        (Cond::Any(&["rgb", "palette", "bgra"]), &[("command-line-utilities", 0.8, 0.), ("config", 0.8, 0.), ("compilers", 0.7, 0.), ("date-and-time", 0.7, 0.), ("development-tools::build-utils", 0.8, 0.)]),

        (Cond::Any(&["cargo", "rustup"]), &[("development-tools", 1.1, 0.), ("development-tools::build-utils", 1.1, 0.),
            ("algorithms", 0.6, 0.), ("os", 0.7, 0.), ("os::macos-apis", 0.7, 0.), ("os::windows-apis", 0.7, 0.), ("cryptography::cryptocurrencies", 0.6, 0.)]),
        (Cond::Any(&["scripts", "scripting"]), &[("development-tools", 1.1, 0.), ("compilers", 1.1, 0.), ("development-tools::build-utils", 1.1, 0.),
            ("algorithms", 0.9, 0.), ("cryptography::cryptocurrencies", 0.9, 0.)]),
        (Cond::All(&["compilation", "target"]), &[("development-tools", 1.1, 0.), ("compilers", 1.1, 0.), ("development-tools::build-utils", 1.1, 0.)]),
        (Cond::Any(&["build-tool", "build-time", "build-script", "build-system", "build-dependencies"]), &[("development-tools::build-utils", 1.2, 0.2),]),
        (Cond::All(&["build", "tool"]), &[("development-tools::build-utils", 1.1, 0.),]),
        (Cond::All(&["build", "script"]), &[("development-tools::build-utils", 1.1, 0.),]),
        (Cond::Any(&["pkg-config", "apt-get"]), &[("os::windows-apis", 0.5, 0.), ("config", 0.8, 0.), ("algorithms", 0.8, 0.), ("compilers", 0.7, 0.)]),
        (Cond::Any(&["autotools"]), &[("os::windows-apis", 0.7, 0.)]),

        (Cond::Any(&["finance", "financial", "quantitative-finance", "financial-market", "personal-finance", "backtesting", "finhub", "fintech", "market-data",
            "quantaxis", "arbitrage", "accounting", "high-frequency-trading", "income-tax", "vanguard", "passive-investing", "taxservice", "taxes", "tax-rate",
            "iso-4217", "currency-converter", "accounting-software", "plain-text-accounting", "plaintext-accounting", "billing", "card-processing",
            "payment-gateway", "bookkeeping", "robinhood", "banking", "invoice", "invoicing", "payroll", "wages", "mortgage", "mortgages",
            "credit-card", "credit-cards", "ledger-cli", "stripe-api", "portfolio-optimization", "option-pricing",
            "blackscholes", "stocks", "financial", "financial-analysis", "financial-data", "money", "equity", "stock-exchange", "budgeting", "beancount",
            "investment", "investments", "stock-market", "algo-trading", "algorithmic-trading", "cemtex"]),
            &[("finance", 1.3, 0.2), ("science::math", 0.8, 0.), ("multimedia", 0.5, 0.), ("asynchronous", 0.8, 0.), ("memory-management", 0.5, 0.),
            ("text-processing", 0.75, 0.), ("caching", 0.5, 0.), ("emulators", 0.5, 0.), ("development-tools::build-utils", 0.8, 0.), ("multimedia::audio", 0.5, 0.),
            ("parsing", 0.1, 0.), ("database-implementations", 0.9, 0.), ("multimedia", 0.9, 0.), ("multimedia::video", 0.9, 0.)]),

        (Cond::Any(&["bonds", "stocks", "monetary", "financing", "tax-service", "payee", "orderbook", "order-book", "hft", "investing", "pricing", "portfolio", "stripe"]),
            &[("finance", 1.1, 0.), ("science::math", 0.9, 0.), ("date-and-time", 0.7, 0.), ("asynchronous", 0.8, 0.), ("wasm", 0.9, 0.), ("command-line-utilities", 0.8, 0.),
            ("parsing", 0.7, 0.), ("command-line-interface", 0.7, 0.)]),


        (Cond::All(&["bills", "split"]), &[("finance", 1.2, 0.1)]),
        (Cond::All(&["payment", "stripe"]), &[("finance", 1.2, 0.1), ("web-programming", 1.1, 0.)]),
        (Cond::All(&["payment", "api"]), &[("finance", 1.2, 0.1), ("web-programming", 1.1, 0.)]),
        (Cond::All(&["payment", "gateway"]), &[("finance", 1.2, 0.1), ("web-programming", 1.1, 0.)]),
        (Cond::All(&["pay", "stripe"]), &[("finance", 1.2, 0.1), ("web-programming", 1.1, 0.)]),
        (Cond::All(&["finance", "option"]), &[("finance", 1.1, 0.1), ("rust-patterns", 0.7, 0.)]),
        (Cond::All(&["bond", "period"]), &[("date-and-time", 0.7, 0.), ("finance", 1.2, 0.1)]),
        (Cond::All(&["currency", "bank"]), &[("finance", 1.2, 0.1)]),
        (Cond::All(&["currency", "convert"]), &[("finance", 1.2, 0.1)]),
        (Cond::All(&["double", "accounting"]), &[("finance", 1.2, 0.1)]),
        (Cond::All(&["market", "quotes"]), &[("date-and-time", 0.7, 0.), ("finance", 1.2, 0.1)]),
        (Cond::All(&["stock", "exchange"]), &[("finance", 1.2, 0.1), ("asynchronous", 0.8, 0.), ("science::math", 0.7, 0.)]),
        (Cond::All(&["stock", "market"]), &[("date-and-time", 0.7, 0.), ("accessibility", 0.8, 0.), ("finance", 1.2, 0.1)]),
        (Cond::All(&["market", "ticker"]), &[("date-and-time", 0.7, 0.), ("accessibility", 0.8, 0.), ("caching", 0.8, 0.), ("finance", 1.2, 0.1)]),
        (Cond::All(&["stock", "quote"]), &[("finance", 1.2, 0.1)]),
        (Cond::All(&["stock", "ticker"]), &[("finance", 1.2, 0.1), ("asynchronous", 0.8, 0.)]),
        (Cond::All(&["stocks", "finance"]), &[("finance", 1.2, 0.1), ("asynchronous", 0.8, 0.)]),
        (Cond::All(&["trading", "indicators"]), &[("finance", 1.2, 0.1), ("multimedia", 0.7, 0.), ("multimedia::video", 0.7, 0.)]),
        (Cond::Any(&["trading"]), &[("finance", 1.1, 0.1), ("asynchronous", 0.5, 0.), ("multimedia", 0.7, 0.), ("multimedia::video", 0.7, 0.)]),
        (Cond::All(&["quantitative", "analysis"]), &[("finance", 1.1, 0.), ("date-and-time", 0.7, 0.)]),
        (Cond::All(&["credit", "card"]), &[("games", 0.3, 0.), ("filesystem", 0.4, 0.), ("finance", 1.1, 0.1)]),
        (Cond::All(&["visa", "card"]), &[("games", 0.5, 0.), ("parsing", 0.6, 0.), ("science::math", 0.7, 0.), ("filesystem", 0.5, 0.)]),
        (Cond::Any(&["crypto", "cryptocurrency", "blockchain", "chain", "bitcoin", "lightning", "staking", "substrate", "polkadot", "web3", "defi", "monero",
            "bioinformatics", "taxonomy", "3d", "raylib", "genomics"]), &[("finance", 0.00001, 0.), ("web-programming::websocket", 0.2, 0.)]),
        (Cond::Any(&["buffer", "container", "file-system", "microservice", "hardware", "emulation"]), &[("finance", 0.5, 0.)]),

        (Cond::NotAny(&["wasm", "yew", "wat", "wast", "webasm", "webgpu", "wasmtime", "asmjs", "webassembly", "assemblyscript", "web-assembly", "wasmcloud", "wasmer", "rustwasm",
            "webidl", "wasi", "dep:wasi", "wasm-bindgen", "pwasm", "wasm32", "emscripten", "web-sys", "javascript"]), &[
            ("wasm", 0.5, 0.),
        ]),
        (Cond::Any(&["web", "chrome", "electron"]), &[("os::macos-apis", 0.5, 0.), ("filesystem", 0.8, 0.), ("os::unix-apis", 0.5, 0.), ("os::windows-apis", 0.5, 0.)]),
        (Cond::Any(&["wasm", "webasm", "webassembly", "web-assembly"]),
            &[("wasm", 3., 0.7), ("embedded", 0.4, 0.), ("hardware-support", 0.9, 0.), ("gui", 0.4, 0.), ("no-std", 0.9, 0.),
            ("development-tools", 0.95, 0.), ("development-tools::ffi", 0.6, 0.), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.8, 0.),
            ("os::macos-apis", 0.5, 0.), ("os::unix-apis", 0.5, 0.), ("rust-patterns", 0.7, 0.), ("compilers", 0.7, 0.), ("os::windows-apis", 0.5, 0.), ("filesystem", 0.7, 0.),
            ("command-line-interface", 0.6, 0.), ("command-line-utilities", 0.75, 0.)]),
        (Cond::Any(&["emscripten", "wasi"]), &[("wasm", 1.1, 0.2), ("embedded", 0.3, 0.), ("no-std", 0.8, 0.)]),
        (Cond::Any(&["wasi"]), &[("os", 1.1, 0.2)]),
        (Cond::Any(&["parity", "mach-o", "intrusive", "cli"]), &[("wasm", 0.5, 0.), ("embedded", 0.8, 0.), ("development-tools::debugging", 0.8, 0.)]),
        (Cond::Any(&["native"]), &[("wasm", 0.5, 0.), ("web-programming", 0.5, 0.), ("multimedia::video", 0.8, 0.), ("multimedia", 0.8, 0.)]),
        (Cond::Any(&["dep:wasm-bindgen", "wasm-bindgen", "dep:wasi"]), &[("wasm", 1.2, 0.), ("parsing", 0.75, 0.), ("os::windows-apis", 0.4, 0.)]),
        (Cond::All(&["web", "assembly"]), &[("wasm", 1.1, 0.)]),

        (Cond::NotAny(&["embedded", "dsp", "eeprom", "i2c", "bootloader", "arm", "sensor", "nrf", "mems", "peripheral", "nordic", "riscv", "risc-v", "dep:bare-metal", "dep:cortex-m-rt", "dep:cortex-m", "svd2rust", "interrupt", "interrupts",
            "controller", "microcontroller", "microcontrollers", "analog", "lcd", "hw", "sdp", "bluez", "dep:btleplug", "hci", "xhci", "bluetooth",
            "ble", "cortex-m", "avr", "nickel", "device-drivers", "hal", "hardware-abstraction-layer", "driver", "register", "bare-metal",
            "crt0", "no-std", "stm32", "framebuffer", "no_std", "feature:no_std", "feature:no-std", "feature:std"]),
            &[("embedded", 0.7, 0.)]),
        (Cond::Any(&["svd2rust", "interrupt", "interrupts", "microcontroller", "microcontrollers", "analog", "sdp",
            "bluez", "dep:btleplug", "ble", "cortex-m", "avr", "nickel", "device-drivers", "hal", "hardware-abstraction-layer",
            "bare-metal", "crt0", "stm32"]),
            &[("embedded", 1.2, 0.1), ("hardware-support", 1.1, 0.), ("science::geo", 0.9, 0.), ("parsing", 0.75, 0.)]),
        (Cond::Any(&["api"]), &[("embedded", 0.9, 0.), ("web-programming::websocket", 0.9, 0.)]),
        (Cond::All(&["embedded", "no-std"]), &[("embedded", 1.2, 0.2), ("no-std", 0.8, 0.)]),
        (Cond::All(&["embedded", "no_std"]), &[("embedded", 1.2, 0.2), ("no-std", 0.8, 0.)]),
        (Cond::Any(&["sdk"]), &[("os", 1.05, 0.), ("algorithms", 0.9, 0.), ("rust-patterns", 0.8, 0.), ("compilers", 0.8, 0.),]),
        (Cond::Any(&["compile-time"]), &[("compilers", 0.9, 0.), ("games", 0.5, 0.)]),
        (Cond::Any(&["compile-time", "codegen", "asm"]),
                &[("development-tools", 1.2, 0.2), ("compilers", 1.2, 0.1), ("rust-patterns", 1.1, 0.), ("game-development", 0.5, 0.), ("multimedia::audio", 0.8, 0.), ("concurrency", 0.9, 0.), ("games", 0.15, 0.)]),
        (Cond::Any(&["toolchain", "tooling", "sdk", "toolchain-manager"]),
                &[("development-tools", 1.2, 0.2), ("game-development", 0.8, 0.), ("no-std", 0.8, 0.), ("caching", 0.9, 0.), ("multimedia::audio", 0.7, 0.), ("concurrency", 0.7, 0.), ("games", 0.5, 0.)]),
        (Cond::Any(&["llvm", "clang", "cretonne", "gcc"]),
                &[("development-tools", 1.2, 0.2), ("compilers", 1.2, 0.2), ("game-development", 0.8, 0.), ("no-std", 0.8, 0.), ("rendering::graphics-api", 0.8, 0.), ("multimedia::audio", 0.7, 0.), ("concurrency", 0.7, 0.), ("games", 0.5, 0.)]),
        (Cond::Any(&[ "rustc", "cargo", "compiler"]),
                &[("development-tools", 1.2, 0.2), ("compilers", 1.2, 0.2), ("game-development", 0.5, 0.), ("network-programming", 0.8, 0.), ("development-tools::ffi", 0.7, 0.), ("multimedia::audio", 0.8, 0.), ("concurrency", 0.9, 0.), ("games", 0.15, 0.)]),
        (Cond::All(&["code", "completion"]), &[("development-tools", 1.2, 0.2)]),
        (Cond::Any(&["compilator", "dsl", "type-checking", "programming-language", "gluon", "compiler", "cranelift"]), &[
            ("development-tools", 1.1, 0.1), ("compilers", 1.2, 0.1), ("no-std", 0.8, 0.), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::Any(&["tree-sitter", "treesitter"]), &[("development-tools", 1.2, 0.1), ("config", 0.7, 0.), ("compilers", 1.2, 0.1), ("parsing", 0.8, 0.), ("parser-implementations", 1.2, 0.)]),
        (Cond::Any(&["abstract-syntax-tree"]), &[("development-tools", 1.1, 0.), ("compilers", 1.2, 0.1), ("parser-implementations", 1.1, 0.)]),
        (Cond::All(&["language", "parser"]), &[("development-tools", 1.1, 0.), ("compilers", 1.2, 0.05), ("parser-implementations", 1.05, 0.)]),
        (Cond::Any(&["hir", "static-analysis"]), &[("development-tools", 1.1, 0.), ("compilers", 1.2, 0.05)]),
        (Cond::Any(&["code", "generation", "codebase"]), &[("visualization", 0.7, 0.), ("date-and-time", 0.7, 0.)]),
        (Cond::Any(&["framework", "generate", "generator", "precompiled", "precompile", "tools", "assets"]),
            &[("development-tools", 1.2, 0.15), ("development-tools::ffi", 1.3, 0.05), ("date-and-time", 0.6, 0.), ("database-implementations", 0.8, 0.), ("caching", 0.9, 0.)]),
        (Cond::Any(&["interface"]), &[("rust-patterns", 1.1, 0.), ("gui", 1.1, 0.), ("command-line-interface", 1.1, 0.)]),
        (Cond::Any(&["abstraction", "crud"]), &[("database-implementations", 0.8, 0.)]),

        (Cond::Any(&["lang", "esoteric-language", "esolang", "esoteric-interpreter"]),
            &[("compilers", 1.1, 0.05), ("parsing", 0.8, 0.), ("filesystem", 0.8, 0.), ("internationalization", 0.5, 0.)]),
        (Cond::Any(&["programming-language", "proof-assistant", "abstract-interpretation"]),
            &[("compilers", 1.3, 0.2), ("parsing", 0.8, 0.), ("wasm", 0.8, 0.), ("filesystem", 0.8, 0.), ("internationalization", 0.5, 0.), ("rust-patterns", 0.9, 0.)]),
        (Cond::Any(&["scripting-language", "scripting-engine"]), &[("compilers", 1.1, 0.1), ("filesystem", 0.3, 0.)]),
        (Cond::Any(&["functional-programming", "functional-language", "algebraic-effects"]),
            &[("compilers", 1.2, 0.05), ("rust-patterns", 1.2, 0.05), ("data-structures", 1.1, 0.), ("algorithms", 1.1, 0.), ("filesystem", 0.3, 0.)]),
        (Cond::All(&["functional", "language"]), &[("compilers", 1.2, 0.05), ("rust-patterns", 1.1, 0.05)]),
        (Cond::Any(&["bindings"]), &[("compilers", 0.9, 0.), ("parsing", 0.8, 0.)]),
        (Cond::Any(&["typescript-compiler", "ecmascript-parser", "javascript-parser"]), &[("compilers", 1.4, 0.3), ("web-programming", 1.4, 0.3), ("filesystem", 0.4, 0.), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.9, 0.)]),
        (Cond::All(&["javascript", "compiler"]), &[("compilers", 1.2, 0.1), ("parsing", 0.8, 0.), ("web-programming", 1.2, 0.1)]),
        (Cond::Any(&["sourcemap", "source-maps", "sourcemaps"]), &[("compilers", 1.2, 0.2), ("web-programming", 1.2, 0.2)]),

        (Cond::Any(&["git"]), &[("no-std", 0.85, 0.), ("command-line-interface", 0.5, 0.), ("algorithms", 0.9, 0.), ("gui", 0.9, 0.), ("date-and-time", 0.4, 0.), ("cryptography::cryptocurrencies", 0.6, 0.)]),
        (Cond::Any(&["teaching"]), &[("gui", 0.1, 0.), ("rendering::engine", 0.1, 0.)]),

        (Cond::NotAny(&["gis", "map", "geography", "geoip", "dep:geo", "dep:geojson", "nmea", "h3", "dep:geo-types", "geo", "osm", "cartography", "earth", "geocoding", "geodesy", "geospatial", "postgis", "latitude", "geojson",
            "geotiff", "openstreetmap", "gps", "nmea", "wgs-84", "wgs84", "triangulation", "seismology", "lidar"]),
            &[("science::geo", 0.8, 0.)]),
        (Cond::Any(&["gis", "latitude", "geospatial", "postgis", "cartography", "triangulation", "seismology", "lidar"]),
            &[("science::geo", 1.25, 0.2), ("science", 0.9, 0.), ("science::math", 0.6, 0.), ("algorithms", 0.9, 0.), ("data-structures", 0.9, 0.), ("no-std", 0.9, 0.), ("rust-patterns", 0.5, 0.),
            ("command-line-utilities", 0.75, 0.), ("config", 0.8, 0.), ("multimedia", 0.8, 0.), ("rendering::graphics-api", 0.6, 0.), ("parsing", 0.75, 0.),
            ("multimedia::images", 0.8, 0.), ("cryptography::cryptocurrencies", 0.6, 0.), ("development-tools::build-utils", 0.6, 0.)]), // geo
        (Cond::Any(&["openstreetmap", "cartography", "geojson", "libgeos", "osm", "geography", "geo", "geos", "wgs-84", "ephemeris"]),
            &[("science::geo", 1.2, 0.2), ("science::math", 0.6, 0.), ("template-engine", 0.7, 0.), ("filesystem", 0.7, 0.), ("algorithms", 0.9, 0.), ("multimedia", 0.7, 0.), ("multimedia::images", 0.8, 0.),
             ("command-line-utilities", 0.75, 0.), ("parsing", 0.75, 0.), ("rendering::graphics-api", 0.5, 0.), ("cryptography::cryptocurrencies", 0.6, 0.)]), // geo
        (Cond::Any(&["dep:geo", "dep:geozero", "gnss", "mercator", "dep:gdal", "gdal", "geotiff", "dep:geo-types", "geo-data", "geolocation"]),
            &[("science::geo", 1.2, 0.2), ("rendering::graphics-api", 0.8, 0.), ("multimedia::images", 0.8, 0.), ("development-tools::build-utils", 0.6, 0.),  ("development-tools::ffi", 0.6, 0.), ("compilers", 0.7, 0.), ("games", 0.8, 0.)]),
        (Cond::Any(&["gps", "glonass", "dep:osmpbfreader", "pmtiles", "gpx", "opengeo", "galileo", "rinex", "nmea", "earth", "satellite", "stac"]),
            &[("science::geo", 1.1, 0.1), ("parsing", 0.5, 0.)]),
        (Cond::Any(&["astronomy", "high-energy-physics", "radio-astronomy", "cosmology", "hydrodynamics", "planet", "chemistry", "isotope", "chem-eq", "nuclear-physics", "particle-physics", "cheminformatics", "astro",  "electromagnetism"]),
            &[("science", 1.2, 0.2), ("rust-patterns", 0.5, 0.), ("parsing", 0.8, 0.), ("template-engine", 0.7, 0.), ("rendering::graphics-api", 0.8, 0.), ("concurrency", 0.7, 0.), ("compilers", 0.7, 0.), ("no-std", 0.9, 0.),
            ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.), ("development-tools::cargo-plugins", 0.5, 0.), ("command-line-utilities", 0.75, 0.)]),
        (Cond::Any(&["geodesic", "georeferencing", "google-maps", "geodesy", "osgeo", "geocoding", "geocoder", "wgs84"]),
            &[("science::geo", 1.2, 0.2), ("rust-patterns", 0.5, 0.), ("concurrency", 0.7, 0.), ("template-engine", 0.7, 0.), ("caching", 0.7, 0.), ("games", 0.8, 0.), ("game-development", 0.8, 0.), ("compilers", 0.7, 0.), ("no-std", 0.9, 0.),
            ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.), ("rendering::graphics-api", 0.8, 0.), ("parsing", 0.5, 0.),
            ("development-tools::cargo-plugins", 0.5, 0.), ("multimedia::audio", 0.5, 0.), ("command-line-utilities", 0.75, 0.)]),
        (Cond::Any(&["weather", "weather-api"]), &[("science::geo", 1.1, 0.1), ("web-programming", 1.1, 0.05)]),

        (Cond::Any(&["bioinformatics", "dep:bio", "transcriptomics", "proteomics", "phylogenomics", "transcriptomics", "phenotype", "protein", "protein-structure", "protein-sequences", "genomic"]),
            &[("science::bio", 1.3, 0.3), ("science::math", 0.6, 0.), ("rendering::graphics-api", 0.8, 0.), ("visualization", 1.1, 0.), ("no-std", 0.7, 0.), ("algorithms", 0.9, 0.),
            ("parsing", 0.4, 0.), ("config", 0.9, 0.), ("encoding", 0.9, 0.), ("embedded", 0.7, 0.), ("asynchronous", 0.7, 0.), ("filesystem", 0.8, 0.),
            ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.), ("development-tools::cargo-plugins", 0.5, 0.), ("command-line-utilities", 0.8, 0.)]),
        (Cond::Any(&["rna-seq", "fastq", "bio", "htslib", "genomics", "crispr", "interactomics", "chemical", "biology", "systems-biology", "biomedical", "biological", "rna", "genome", "genotype", "genbank"]),
            &[("science", 1.1, 0.), ("science::bio", 1.2, 0.2), ("science::math", 0.6, 0.), ("template-engine", 0.7, 0.), ("no-std", 0.7, 0.),
            ("parsing", 0.5, 0.), ("encoding", 0.9, 0.), ("parser-implementations", 0.9, 0.), ("embedded", 0.7, 0.), ("asynchronous", 0.7, 0.),
            ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.), ("development-tools::cargo-plugins", 0.5, 0.)]),
        (Cond::Any(&["chemistry", "chemical", "sensory", "benzene", "molecules"]),
            &[("science", 1.2, 0.3), ("science::math", 0.6, 0.), ("rendering::graphics-api", 0.8, 0.), ("visualization", 1.1, 0.), ("compilers", 0.7, 0.), ("no-std", 0.9, 0.),
            ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.), ("development-tools::cargo-plugins", 0.5, 0.), ("rust-patterns", 0.5, 0.),
            ("algorithms", 0.9, 0.), ("config", 0.8, 0.), ("command-line-utilities", 0.7, 0.)]),
        (Cond::All(&["dna", "rna"]), &[("science::bio", 1.3, 0.2), ("parsing", 0.5, 0.)]),
        (Cond::Any(&["fasta", "nucleotide"]), &[("science::bio", 1.1, 0.1), ("parsing", 0.5, 0.)]),
        (Cond::Any(&["netcdf", "hdf5"]), &[("science", 1.1, 0.1), ("parsing", 0.5, 0.)]),

        (Cond::All(&["validation", "api"]), &[("email", 0.7, 0.), ("multimedia", 0.7, 0.)]),

        (Cond::Any(&["parser", "syntex"]),
            &[("parser-implementations", 1.1, 0.), ("parsing", 1.08, 0.), ("no-std", 0.7, 0.), ("embedded", 0.9, 0.), ("science::robotics", 0.7, 0.),
            ("science", 0.9, 0.), ("development-tools", 0.8, 0.), ("development-tools::build-utils", 0.7, 0.), ("development-tools::debugging", 0.8, 0.), ("rendering::graphics-api", 0.3, 0.),
            ("rendering", 0.6, 0.), ("gui", 0.8, 0.), ("web-programming::http-client", 0.75, 0.), ("command-line-utilities", 0.75, 0.),
            ("command-line-interface", 0.5, 0.), ("games", 0.7, 0.), ("config", 0.9, 0.), ("visualization", 0.7, 0.), ("wasm", 0.8, 0.)]),
        (Cond::All(&["elf", "parser"]), &[("parser-implementations", 1.2, 0.2), ("development-tools::debugging", 1.1, 0.), ("parsing", 0.8, 0.), ("wasm", 0.5, 0.), ("os::unix-apis", 1.1, 0.)]),
        (Cond::All(&["packet", "parsing"]), &[("parser-implementations", 1.2, 0.2), ("parsing", 0.7, 0.), ("config", 0.9, 0.), ("text-processing", 0.5, 0.)]),
        (Cond::Any(&["packet-parsing", "file-format"]), &[("parser-implementations", 1.2, 0.2), ("parsing", 0.5, 0.)]),
        (Cond::Any(&["document", "file-format", "file", "reader", "format"]), &[("parsing", 0.8, 0.)]),
        (Cond::Any(&["microsoft", "windows", "apple", "xml", "logs", "security", "opencv", "macos", "ole", "network", "messages", "command-line", "dotenv", "hashicorp",
            "zettelkasten", "llvm", "mctp", "blizzard", "gamedev", "mpeg", "midi", "bmp", "promql", "riff", "gitcommit", "webrtc", "emacs", "url-parser", "dsn", "tcp",
            "packets", "pdf", "literate-programming", "solidity", "http-header", "imap", "sql", "checksum", "commandline", "d-bus", "matrix", "golang", "siem", "usb", "verilog",
            "parser-implementations", "elf", "ical", "tiff", "gps", "mysql", "whatsapp", "utc", "riscv", "amf", "doom", "rsa", "gpg", "ansi", "thumbnail", "ssl", "certificate",
            "ldap", "linguistics", "nlp", "natural-language", "mach-o", "markdown", "lisp", "thrift", "rpm", "sse", "eventsource", "tor"]),
            &[("parsing", 0.4, 0.)]),
        (Cond::Any(&["s-expr", "expression-parser", "sexp"]), &[("parser-implementations", 1.2, 0.1), ("parsing", 1.1, 0.), ("data-structures", 1.1, 0.)]),
        (Cond::All(&["format", "parser"]), &[("parser-implementations", 1.3, 0.2), ("no-std", 0.8, 0.), ("parsing", 0.95, 0.), ("games", 0.8, 0.), ("development-tools::ffi", 0.8, 0.)]),
        (Cond::All(&["file", "format"]), &[("parser-implementations", 1.2, 0.1), ("parsing", 0.7, 0.), ("encoding", 1.1, 0.), ("wasm", 0.9, 0.), ("algorithms", 0.8, 0.), ("web-programming::http-server", 0.8, 0.)]),
        (Cond::All(&["file", "format", "parser"]), &[("parser-implementations", 1.2, 0.1), ("algorithms", 0.8, 0.), ("parsing", 0.4, 0.), ("development-tools::ffi", 0.8, 0.)]),
        (Cond::Any(&["tokenizer", "sanitizer", "parse", "lexer", "lexer-parser", "parser", "parsing"]),
            &[("science::math", 0.6, 0.), ("data-structures", 0.7, 0.), ("os", 0.7, 0.), ("config", 0.9, 0.), ("games", 0.5, 0.),
            ("os::macos-apis", 0.9, 0.), ("command-line-utilities", 0.8, 0.), ("simulation", 0.8, 0.), ("wasm", 0.9, 0.), ("no-std", 0.9, 0.), ("encoding", 0.8, 0.),
            ("command-line-interface", 0.5, 0.), ("text-editors", 0.5, 0.), ("development-tools::build-utils", 0.8, 0.), ("development-tools::testing", 0.6, 0.)]),
        (Cond::Any(&["sanitizer", "nom"]), &[("parser-implementations", 1.3, 0.2), ("algorithms", 0.8, 0.), ("encoding", 0.8, 0.)]),
        (Cond::Any(&["tokenizer", "lexer", "parser", "jwt", "macro", "rpc"]), &[
            ("encoding", 0.8, 0.), ("no-std", 0.7, 0.), ("emulators", 0.6, 0.), ("wasm", 0.9, 0.), ("development-tools::build-utils", 0.8, 0.), ("rendering::graphics-api", 0.9, 0.)]),

        (Cond::Any(&["tokenizer", "tokenize", "parser-combinators", "packrat", "peg", "lalr", "yacc"]),
            &[("parsing", 1.2, 0.1), ("parser-implementations", 0.8, 0.), ("wasm", 0.9, 0.), ("os", 0.7, 0.), ("emulators", 0.7, 0.),
            ("compilers", 0.8, 0.), ("internationalization", 0.8, 0.), ("games", 0.8, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::Any(&["tokenizers", "tokenizer"]), &[("text-processing", 1.1, 0.1)]),
        (Cond::Any(&["combinator", "ll1", "ll-1", "recursive-descent", "lexer", "ll-k", "lex", "context-free", "grammars", "grammar"]),
            &[("parsing", 1.2, 0.1), ("parser-implementations", 0.8, 0.), ("os", 0.7, 0.), ("emulators", 0.7, 0.), ("config", 0.9, 0.), ("internationalization", 0.8, 0.), ("games", 0.8, 0.)]),
        (Cond::All(&["parser", "generator"]), &[("parsing", 1.4, 0.3), ("parser-implementations", 0.9, 0.), ("compilers", 0.8, 0.), ("caching", 0.9, 0.), ("gui", 0.5, 0.)]),
        (Cond::Any(&["parser-generator", "parser-combinator"]), &[("parsing", 1.4, 0.3), ("parser-implementations", 0.5, 0.), ("gui", 0.5, 0.)]),
        (Cond::Any(&["backus–naur", "bnf"]), &[("parsing", 1.2, 0.), ("parser-implementations", 1.2, 0.)]),
        (Cond::All(&["parser", "combinators"]), &[("parsing", 1.3, 0.2), ("parser-implementations", 0.7, 0.), ("no-std", 0.9, 0.), ("multimedia", 0.5, 0.)]),
        (Cond::All(&["parser", "combinator"]), &[("parsing", 1.3, 0.2), ("parser-implementations", 0.7, 0.), ("no-std", 0.9, 0.), ("config", 0.9, 0.), ("compilers", 0.7, 0.), ("multimedia", 0.5, 0.)]),
        (Cond::All(&["parser", "combinatoric"]), &[("parsing", 1.3, 0.2), ("parser-implementations", 0.8, 0.), ("no-std", 0.9, 0.), ("compilers", 0.8, 0.), ("multimedia", 0.5, 0.)]),
        (Cond::Any(&["dep:nom", "dep:lalrpop-util", "dep:lalrpop", "dep:pest_derive"]), &[("parser-implementations", 1.2, 0.1), ("parsing", 0.7, 0.)]),
        (Cond::Any(&["dep:serde", "dep:serde_json", "dep:toml", "dep:csv", "dep:quick-xml", "dep:colored", "dep:owo-colors", "dep:chrono"]), &[("parsing", 0.4, 0.)]),
        (Cond::Any(&["dep:regex"]), &[("parsing", 0.9, 0.)]),
        (Cond::Any(&["dep:tree-sitter"]), &[("parsing", 0.1, 0.), ("parser-implementations", 1.3, 0.1), ("compilers", 1.2, 0.)]),
        (Cond::Any(&["dep:prost"]), &[("parsing", 0.9, 0.), ("parser-implementations", 1.2, 0.1)]),
        (Cond::Any(&["validator", "rfc", "emulator", "gcode", "elasticsearch", "tree-sitter", "command-line", "cli",
            "movie", "glsl", "2d", "3d", "lcov", "opengl", "pdl", "vcard", "science", "uris", "ocaml", "d-bus", "scpi", "ftp",
            "cron", "date-and-time", "pcap", "voip", "linux", "bbcode", "deb", "cbor", "git", "gcov", "markdown-it",
            "kubernetes", "docker", "systemd", "keybindings", "savegame", "mp4", "iso-bmff", "protobuf", "graphics",
            "arxiv", "specification", "kanji", "format-args", "vmf", "pdf", "html",
            "game", "json", "database", "protocol", "microsoft", "email", "sgf", "bgp", "x86", "qml", "settings"]),
            &[("parsing", 0.6, 0.)]),
        (Cond::Any(&["ll", "lr", "incremental"]),
            &[("parsing", 1.2, 0.), ("parser-implementations", 0.8, 0.)]),
        (Cond::Any(&["syntex", "decoder", "mime", "html", "dep:peg", "dep:pest"]),
            &[("parser-implementations", 1.2, 0.01), ("parsing", 0.6, 0.), ("caching", 0.8, 0.), ("no-std", 0.8, 0.)]),
        (Cond::Any(&["json", "yaml", "bencode", "asn1", "ue4", "javascript", "scraper", "blockchain", "irc", "twitch"]),
            &[("parsing", 0.1, 0.), ("date-and-time", 0.7, 0.), ("database", 0.7, 0.), ("multimedia::video", 0.8, 0.), ("development-tools::procedural-macro-helpers", 0.9, 0.),]),
        (Cond::Any(&["xml", "yaml", "bencode", "bencode-parser", "csv", "csv-parser", "rss", "tex"]),
            &[("parsing", 0.3, 0.), ("parser-implementations", 1.2, 0.01), ("rust-patterns", 0.7, 0.), ("no-std", 0.9, 0.), ("compilers", 0.8, 0.),
            ("data-structures", 0.8, 0.), ("os::macos-apis", 0.7, 0.), ("development-tools::ffi", 0.9, 0.),
            ("os::windows-apis", 0.7, 0.), ("os", 0.9, 0.), ("multimedia", 0.7, 0.), ("multimedia", 0.7, 0.)]),
        (Cond::All(&["xml", "parser"]), &[("parsing", 0.3, 0.), ("parser-implementations", 1.2, 0.01)]),
        (Cond::Any(&["dep:html5ever", "dep:ammonia"]), &[("parsing", 0.4, 0.), ("network-programming", 0.8, 0.), ("parser-implementations", 0.2, 0.)]),
        (Cond::Any(&["font", "dom", "files", "language", "formats", "lua", "format", "asn", "cue", "loader", "dep:serde"]), &[("parsing", 0.4, 0.), ("development-tools::build-utils", 0.9, 0.)]),
        (Cond::Any(&["semver", "atoi", "ast", "syntax", "format", "iban"]),
            &[("parsing", 0.8, 0.), ("parser-implementations", 1.2, 0.01), ("os::macos-apis", 0.7, 0.), ("config", 0.9, 0.), ("development-tools::ffi", 0.9, 0.),
            ("os::windows-apis", 0.7, 0.), ("os", 0.9, 0.), ("network-programming", 0.9, 0.), ("web-programming", 0.9, 0.), ("web-programming::http-server", 0.7, 0.)]),

        (Cond::All(&["parser", "nom"]), &[("parser-implementations", 1.3, 0.1), ("algorithms", 0.8, 0.), ("multimedia", 0.4, 0.)]),

        (Cond::Any(&["extraction", "deserializer", "deserialize", "deserialization"]),
            &[("parser-implementations", 1.25, 0.2), ("parsing", 1.1, 0.), ("authentication", 0.8, 0.), ("network-programming", 0.8, 0.),
            ("value-formatting", 1.1, 0.), ("encoding", 1.2, 0.01), ("database", 0.8, 0.)]),
        (Cond::Any(&["serialization", "serialize"]),
            &[("parser-implementations", 1.25, 0.2), ("parsing", 0.5, 0.), ("encoding", 1.2, 0.1), ("database", 0.8, 0.)]),
        (Cond::Any(&["deserialize", "serializer", "serializes", "decoder", "decoding"]),
            &[("parser-implementations", 1.2, 0.15), ("parsing", 1.1, 0.), ("authentication", 0.8, 0.), ("compilers", 0.9, 0.), ("database", 0.9, 0.),
            ("network-programming", 0.8, 0.), ("value-formatting", 1.1, 0.), ("encoding", 1.2, 0.1)]),

        (Cond::All(&["machine", "learning"]), &[("science::ml", 1.5, 0.3), ("network-programming", 0.8, 0.), ("science::math", 0.8, 0.), ("science", 0.8, 0.),
            ("emulators", 0.15, 0.), ("command-line-utilities", 0.5, 0.), ("games", 0.5, 0.), ("config", 0.9, 0.), ("parsing", 0.8, 0.), ("database", 0.8, 0.), ("development-tools::build-utils", 0.7, 0.), ("no-std", 0.9, 0.), ("gui", 0.8, 0.)]),
        (Cond::All(&["reinforcement", "learning"]), &[("science::ml", 1.5, 0.3), ("network-programming", 0.8, 0.), ("science::math", 0.8, 0.), ("science", 0.8, 0.),
            ("emulators", 0.5, 0.), ("games", 0.5, 0.), ("config", 0.9, 0.), ("parsing", 0.8, 0.), ("development-tools::build-utils", 0.7, 0.), ("no-std", 0.9, 0.), ("gui", 0.8, 0.)]),
        (Cond::All(&["decision", "tree"]), &[("science::ml", 1.2, 0.1), ("algorithms", 1.1, 0.), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::All(&["neural", "classifier"]), &[("science::ml", 1.5, 0.3), ("science::math", 0.8, 0.), ("network-programming", 0.5, 0.)]),
        (Cond::All(&["neural", "network"]), &[("science::ml", 1.5, 0.3), ("science::math", 0.8, 0.), ("network-programming", 0.2, 0.),
            ("emulators", 0.2, 0.), ("command-line-utilities", 0.6, 0.), ("development-tools::build-utils", 0.2, 0.)]),
        (Cond::All(&["deep", "learning"]), &[("science::ml", 1.5, 0.3), ("science::math", 0.8, 0.), ("network-programming", 0.5, 0.), ("config", 0.9, 0.),
            ("emulators", 0.2, 0.), ("command-line-utilities", 0.6, 0.), ("development-tools::build-utils", 0.2, 0.)]),
        (Cond::Any(&["learning", "testing", "publishing"]), &[("development-tools::build-utils", 0.8, 0.)]),
        (Cond::Any(&["fuzzy-logic"]),
            &[("science", 1.25, 0.2), ("science::ml", 1.3, 0.1), ("games", 0.5, 0.), ("os", 0.8, 0.), ("algorithms", 1.2, 0.1), ("command-line-utilities", 0.8, 0.), ("command-line-interface", 0.4, 0.)]),
        (Cond::Any(&["natural-language-processing", "nlp", "linguistics", "language-processing", "language-recognition", "fasttext", "embeddings", "word2vec", "layered-nlp"]),
            &[("science", 1.2, 0.15), ("text-processing", 1.2, 0.2), ("science::ml", 1.3, 0.1), ("games", 0.5, 0.), ("embedded", 0.5, 0.), ("wasm", 0.8, 0.),
            ("os", 0.8, 0.), ("game-development", 0.75, 0.), ("development-tools::cargo-plugins", 0.5, 0.), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.),
            ("algorithms", 1.2, 0.1), ("command-line-utilities", 0.8, 0.), ("command-line-interface", 0.4, 0.)]),
        (Cond::Any(&["blas", "tensorflow", "tensor-flow", "word2vec", "word-embeddings", "deep-learning-framework", "torch", "decision-tree", "genetic-algorithm",
            "mnist", "deep-learning", "stable-diffusion", "neuralnetworks", "neuralnetwork", "machine-learning"]),
            &[("science::ml", 1.25, 0.3), ("science::math", 0.8, 0.), ("parsing", 0.8, 0.), ("caching", 0.6, 0.), ("embedded", 0.75, 0.),  ("rust-patterns", 0.75, 0.), ("development-tools", 0.8, 0.), ("science", 0.7, 0.), ("web-programming::http-client", 0.8, 0.), ("parsing", 0.8, 0.), ("games", 0.5, 0.), ("os", 0.8, 0.), ("game-development", 0.75, 0.), ("algorithms", 1.2, 0.), ("command-line-utilities", 0.8, 0.), ("command-line-interface", 0.4, 0.)]),
        (Cond::Any(&["neural-network", "machinelearning", "hyperparameter", "hyperparameter-optimization", "pytorch", "backpropagation", "cudnn", "randomforest", "neural-networks", "deep-neural-networks", "reinforcement", "perceptron"]),
            &[("science::ml", 1.25, 0.3), ("science::math", 0.8, 0.), ("science", 0.7, 0.), ("web-programming::http-client", 0.8, 0.), ("games", 0.5, 0.), ("os", 0.8, 0.),
            ("game-development", 0.75, 0.), ("compilers", 0.5, 0.), ("algorithms", 1.2, 0.), ("command-line-utilities", 0.8, 0.), ("command-line-interface", 0.4, 0.), ("development-tools::build-utils", 0.6, 0.)]),
        (Cond::Any(&["bayesian", "llama-cpp", "generative-model", "ngram", "openai", "bayes", "classifier", "lstm", "classify", "markov", "ai", "cuda", "svm", "artificial-intelligence", "llama", "llm", "nn", "rnn", "tensor", "learning", "statistics"]),
            &[("science::ml", 1.2, 0.15), ("science::math", 0.9, 0.), ("no-std", 0.9, 0.), ("algorithms", 1.1, 0.), ("web-programming::http-client", 0.8, 0.),
            ("development-tools", 0.9, 0.), ("development-tools::build-utils", 0.6, 0.)]),
        (Cond::All(&["llm", "ai"]), &[("science::ml", 1.25, 0.3)]),
        (Cond::Any(&["math", "maths", "mathematical-library", "math-library", "math-physics", "calculus", "geometry", "calculator", "logic", "satisfiability", "haar", "eigenvalues", "combinatorics", "fft", "discrete"]),
            &[("science::math", 1.25, 0.3), ("algorithms", 1.2, 0.1), ("science::bio", 0.7, 0.), ("encoding", 0.9, 0.), ("science::geo", 0.8, 0.), ("database", 0.8, 0.), ("web-programming::http-client", 0.9, 0.),
            ("config", 0.8, 0.), ("rendering::graphics-api", 0.75, 0.), ("games", 0.5, 0.), ("os", 0.8, 0.), ("game-development", 0.75, 0.),
            ("command-line-utilities", 0.8, 0.), ("command-line-interface", 0.4, 0.), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.6, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::Any(&["polynomial", "sigmoid", "numerics", "graph-theory", "gaussian", "root-finding", "complex-numbers", "geometric-algebra", "mathematics", "matlab", "mathematical",
            "voronoi", "gmp", "bignum", "prime", "primes", "linear-algebra", "numpy", "lexicographic", "algebra", "euler", "bijective"]),
            &[("science::math", 1.25, 0.3), ("algorithms", 1.2, 0.1), ("web-programming::http-client", 0.9, 0.), ("rendering::graphics-api", 0.7, 0.), ("games", 0.5, 0.), ("development-tools", 0.5, 0.),
            ("os", 0.8, 0.), ("game-development", 0.75, 0.), ("development-tools::testing", 0.75, 0.), ("command-line-utilities", 0.7, 0.), ("development-tools::build-utils", 0.8, 0.),
            ("development-tools::testing", 0.7, 0.), ("command-line-interface", 0.4, 0.)]),
        (Cond::Any(&["arithmetic", "dihedral", "arithmetics", "tanh", "histogram", "quaternions", "math-functions", "arbitrary-precision", "algebraic", "logarithm", "topology"]),
            &[("science::math", 1.25, 0.15), ("algorithms", 1.2, 0.), ("text-processing", 0.8, 0.), ("compression", 0.8, 0.), ("development-tools::build-utils", 0.8, 0.)]),
        (Cond::Any(&["precision", "computational", "polygon", "dep:num-complex", "calculations", "calculation"]), &[("science::math", 1.1, 0.05)]),
        (Cond::Any(&["worley", "perlin", "simplex-noise", "perlin-noise", "fractal"]), &[("science::math", 1.2, 0.1), ("multimedia::images", 1.1, 0.1)]),
        (Cond::Any(&["voronoi", "delaunay", "simplex", "delaunay-triangulation", "graph-equality", "transduction"]), &[("science::math", 1.2, 0.1), ("algorithms", 1.1, 0.05), ("caching", 0.75, 0.), ("config", 0.9, 0.)]),
        (Cond::All(&["arithmetic", "coding"]),
             &[("science::math", 0.7, 0.), ("compression", 1.2, 0.1), ("algorithms", 1.1, 0.05)]),
        (Cond::All(&["discrete", "transforms"]),
            &[("science::math", 1.25, 0.1), ("algorithms", 1.1, 0.), ("simulation", 0.9, 0.)]),
        (Cond::Any(&["dep:nalgebra", "dep:num-complex", "dep:alga", "dep:openblas-src"]), &[("science::math", 1.2, 0.), ("science", 1.1, 0.), ("compilers", 0.7, 0.), ("algorithms", 1.1, 0.)]),
        (Cond::Any(&["optimization", "floating-point"]),
            &[("science::math", 0.9, 0.), ("science::ml", 0.9, 0.), ("science", 0.9, 0.), ("algorithms", 1.2, 0.1)]),
        (Cond::Any(&["huggingface"]), &[("science::ml", 1.2, 0.1)]),
        (Cond::Any(&["training"]), &[("science::ml", 1.1, 0.05)]),
        (Cond::All(&["computer", "vision"]),
            &[("science::ml", 1.2, 0.1), ("science", 1.1, 0.), ("no-std", 0.9, 0.), ("algorithms", 1.1, 0.), ("multimedia::images", 1.1, 0.), ("development-tools::build-utils", 0.8, 0.)]),
        (Cond::Any(&["computer-vision"]),
            &[("science::ml", 1.3, 0.3), ("multimedia::images", 1.3, 0.3), ("science", 1.1, 0.), ("no-std", 0.8, 0.),
            ("compilers", 0.5, 0.), ("algorithms", 1.1, 0.), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::Any(&["physics", "ncollide", "dynamics", "pressure", "molecular-dynamics"]),
            &[("science", 1.1, 0.1), ("simulation", 1.25, 0.1), ("multimedia::video", 0.8, 0.), ("no-std", 0.9, 0.), ("game-development", 1.1, 0.), ("science::math", 0.8, 0.), ("parsing", 0.8, 0.), ("parser-implementations", 0.8, 0.), ("science::ml", 0.7, 0.), ]),
        (Cond::All(&["physics", "simulation"]),
            &[("simulation", 1.2, 0.1), ("game-development", 1.2, 0.), ("development-tools::build-utils", 0.8, 0.)]),
        (Cond::Any(&["collision", "aabb", "bbox"]),
            &[("simulation", 1.2, 0.), ("game-development", 1.1, 0.), ("network-programming", 0.8, 0.), ("data-structures", 0.9, 0.),
            ("multimedia::audio", 0.7, 0.), ("parsing", 0.8, 0.), ("rendering", 0.8, 0.), ("rendering::graphics-api", 0.9, 0.), ("parser-implementations", 0.8, 0.), ("science::ml", 0.7, 0.)]),
        (Cond::All(&["rigid", "body"]),
            &[("simulation", 1.2, 0.), ("game-development", 1.1, 0.)]),
        (Cond::All(&["rigid", "joints"]),
            &[("simulation", 1.2, 0.), ("game-development", 1.1, 0.), ("development-tools::build-utils", 0.8, 0.)]),
        (Cond::Any(&["read", "byte",  "ffi", "debuginfo", "debug", "api", "sys", "algorithms", "ieee754", "cast", "macro", "ascii", "parser"]),
            &[("science::math", 0.6, 0.), ("science::ml", 0.8, 0.), ("science", 0.9, 0.), ("games", 0.8, 0.)]),
        (Cond::Any(&["simd", "jit", "cipher", "sql", "service", "data-structures", "plugin", "system"]),
            &[("science::math", 0.6, 0.), ("encoding", 0.6, 0.), ("science::ml", 0.8, 0.), ("science", 0.9, 0.), ("wasm", 0.8, 0.), ("algorithms", 0.9, 0.)]),
        (Cond::Any(&["cargo", "openssl", "terminal", "game", "collision", "piston"]),
            &[("science::math", 0.6, 0.), ("encoding", 0.6, 0.), ("memory-management", 0.6, 0.), ("internationalization", 0.9, 0.),
            ("database", 0.6, 0.), ("science::ml", 0.8, 0.), ("science", 0.9, 0.), ("wasm", 0.8, 0.)]),
        (Cond::Any(&["algorithms", "algorithm", "copy-on-write"]),
            &[("algorithms", 1.1, 0.1), ("science::math", 0.8, 0.), ("science::ml", 0.8, 0.), ("science", 0.8, 0.), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::All(&["gaussian", "blur"]),
            &[("science::math", 0.2, 0.), ("multimedia::images", 1.3, 0.2), ("compilers", 0.7, 0.), ("development-tools::build-utils", 0.6, 0.)]),
        (Cond::Any(&["hamming", "levenshtein", "damerau-levenshtein"]),
            &[("algorithms", 1.2, 0.1), ("text-processing", 1.1, 0.), ("internationalization", 0.8, 0.), ("science::math", 0.5, 0.),
            ("rust-patterns", 0.5, 0.), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::All(&["count", "lines"]), &[("text-processing", 1.2, 0.1)]),
        (Cond::Any(&["segmentation"]), &[("text-processing", 1.1, 0.), ("algorithms", 1.1, 0.)]),
        (Cond::Any(&["word-count", "word-counter"]), &[("text-processing", 1.1, 0.1), ("algorithms", 1.1, 0.)]),
        (Cond::Any(&["text"]), &[("text-processing", 1.1, 0.)]),
        (Cond::Any(&["word"]), &[("text-processing", 1.1, 0.)]),
        (Cond::All(&["count", "lines", "code"]), &[("text-processing", 1.2, 0.1)]),
        (Cond::All(&["pattern", "matches"]), &[("algorithms", 1.2, 0.), ("text-processing", 1.2, 0.)]),
        (Cond::All(&["pattern", "expression"]), &[("algorithms", 1.2, 0.), ("text-processing", 1.2, 0.), ("rust-patterns", 1.2, 0.)]),
        (Cond::All(&["evaluate", "expression"]), &[("algorithms", 1.2, 0.), ("rust-patterns", 1.2, 0.)]),

        (Cond::Any(&["openssl", "boringssl", "libressl"]),
            &[("network-programming", 1.2, 0.1), ("cryptography", 1.2, 0.05), ("algorithms", 0.8, 0.), ("config", 0.9, 0.), ("science::math", 0.2, 0.), ("science", 0.7, 0.), ("memory-management", 0.4, 0.),
            ("cryptography::cryptocurrencies", 0.7, 0.), ("command-line-utilities", 0.9, 0.), ("no-std", 0.9, 0.), ("compilers", 0.8, 0.), ("development-tools::testing", 0.7, 0.)]),
        (Cond::Any(&["dep:openssl-sys"]), &[("network-programming", 1.1, 0.), ("os::macos-apis", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::Any(&["dep:autotools"]), &[("os::macos-apis", 0.7, 0.)]),
        (Cond::Any(&["subtle", "ber", "der"]), &[("cryptography", 1.1, 0.01), ("config", 0.9, 0.)]),
        (Cond::Any(&["asn1", "x509", "asn1-der"]), &[("cryptography", 1.2, 0.2), ("encoding", 1.1, 0.05), ("parser-implementations", 1.1, 0.05), ("config", 0.9, 0.)]),
        (Cond::Any(&["tls", "ssl", "pkcs", "tor", "tor-arti"]),
            &[("network-programming", 1.2, 0.1), ("cryptography", 1.2, 0.05), ("multimedia", 0.7, 0.0), ("caching", 0.5, 0.), ("compression", 0.7, 0.0), ("multimedia::video", 0.6, 0.0), ("science::math", 0.2, 0.), ("science", 0.7, 0.), ("memory-management", 0.6, 0.),
            ("cryptography::cryptocurrencies", 0.6, 0.), ("no-std", 0.9, 0.), ("command-line-utilities", 0.7, 0.), ("compilers", 0.8, 0.), ("development-tools::testing", 0.9, 0.)]),
        (Cond::Any(&["packet", "firewall", "ethernet", "packet-filtering"]), &[("network-programming", 1.1, 0.1), ("no-std", 0.9, 0.), ("encoding", 0.9, 0.), ("text-processing", 0.7, 0.)]),
        (Cond::Any(&["hmac-sha256", "sha3", "homomorphic-encryption", "elgamal", "openssl", "public-key", "libsodium", "public-key-cryptography", "sha1", "sha-1",
            "blake3", "sha256", "sha2", "shamir", "cipher", "aes", "md5", "pkcs7", "key-encapsulation", "password-hash", "pkcs12", "k12sum", "keccak", "scrypt", "bcrypt", "merkle", "digest", "chacha", "chacha20"]),
            &[("cryptography", 1.4, 0.3), ("algorithms", 0.8, 0.), ("no-std", 0.95, 0.), ("config", 0.8, 0.), ("date-and-time", 0.5, 0.), ("rendering::engine", 0.7, 0.),
            ("command-line-utilities", 0.75, 0.), ("development-tools::testing", 0.8, 0.), ("development-tools::profiling", 0.8, 0.), ("development-tools", 0.8, 0.)]),
        (Cond::Any(&["cryptography", "cryptographic", "cryptographic-primitives", "sponge", "ecdsa", "galois", "ed25519","argon2", "pbkdf2"]),
            &[("cryptography", 1.4, 0.3), ("algorithms", 0.9, 0.), ("no-std", 0.95, 0.), ("command-line-utilities", 0.75, 0.), ("development-tools", 0.8, 0.), ("development-tools::testing", 0.8, 0.)]),
        (Cond::Any(&["lets-encrypt", "letsencrypt", "csr", "acme"]), &[("cryptography", 1.2, 0.1), ("web-programming", 1.1, 0.01)]),
        (Cond::Any(&["dep:constant_time_eq"]), &[("cryptography", 1.2, 0.05), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::Any(&["dep:digest", "dep:subtle"]), &[("cryptography", 1.2, 0.), ("algorithms", 1.2, 0.)]),
        (Cond::Any(&["finite-field", "polynomial"]), &[("cryptography", 1.2, 0.1), ("science::math", 1.2, 0.)]),

        (Cond::NotAny(&["ethereum", "tetcoin", "erc20", "eth2", "dep:sp-runtime", "bitcoincash", "faucet", "substrate", "eth", "xrp", "xrpl", "corelightning", "lightning", "aptos", "nimiq", "soroban", "fuel-vm", "sui", "algonaut", "eth2", "token", "dep:frame-support", "dep:gemachain-sdk", "dep:web3", "cryptocurrency", "vapory","arweave", "randomx", "dep:ic-cdk", "dfinity", "mining", "tari", "pow", "proof-of-work", "binance", "tetsy", "diem", "marine", "fluence", "snarkvm", "snarkos", "zcash", "tetcore",
            "dep:ckb-types", "ethcore", "xynthe", "mimblewimble", "crypto", "contract", "safecoin", "contracts", "smart-contracts", "smart-contract", "cosmos", "eth", "stake", "staking", "near",
            "zk-snark", "dep:near-sdk", "dep:tet-core", "dep:solana-sdk", "exonum", "dep:solana-program", "bech32", "fungible", "iota", "tezos", "dep:ethnum", "dep:ethabi", "dep:sp-core", "dep:frame-support", "dep:safecoin-sdk", "dep:ethereum-types", "dep:ethereumvm",
            "parity", "solidity", "dep:holochain_core_types", "dep:solid","kraken", "holochain", "lnp-bp", "dep:miniscript", "metaplex", "web3", "tendermint", "dep:bitcoin-cfg", "bitcoin", "daml", "dep:spl-token", "ink", "owasm", "solana", "monero", "xmr", "coinbase", "sovereign",
            "litecoin", "bitfinex", "ledger", "bitcoin-wallet", "serai", "pallet", "nakamoto", "miraland", "hyperledger", "btc", "sway", "fuel", "interledger", "dep:anchor-lang", "agsol", "dep:mv-core-types", "dep:move-deps", "apdu", "abci", "dep:wedpr_l_macros", "consensus", "contract", "dep:parity-scale-codec", "self-sovereign", "dep:libp2p", "dep:bitcoin", "dep:blockchain",
            "nanocurrency", "currency", "stellar", "coin", "lightning", "lightning-network", "fedimint", "wallet", "eosio", "cardano", "polkadot", "supercolony", "pallas", "dep:cw2", "dep:cw20","dep:cw20-base", "starknet", "fadroma", "cosmwasm", "dep:candid", "dep:cosmwasm-std", "bitstamp"]),
            &[("cryptography::cryptocurrencies", 0.8, 0.)]),
        (Cond::Any(&["ethereum", "erc20", "multiversx", "dep:sp-runtime", "wallet-generation", "bitcoin-blockchain", "nakamoto", "rapid-blockchain-prototypes", "ethcore", "lnp-bp", "corelightning", "owasm", "starknet", "aptos", "fuel-vm", "xrpl", "algonaut", "tendermint", "randomx", "dep:wedpr_l_macros", "dep:solana-program", "tari", "safecoin",  "chainlink", "vapory", "arweave","nft", "web3", "dfinity", "snarkvm", "tetsy", "fluence", "dep:diem-types", "binance", "dep:ckb-types", "snarkos",
            "zcash", "bitcoin", "dep:bitcoin-cfg", "dep:ex3-canister-types", "bitcoin-wallet", "dep:tet-core", "dep:miraland-sdk", "tetcore", "kraken", "tezos", "solana", "supercolony", "xynthe", "dep:anchor-lang", "soroban", "dep:miniscript", "dep:ic-cdk", "dep:ethereumvm", "solidity", "zk-snark", "smart-contracts", "holochain", "dep:solid", "mimblewimble",
            "dep:holochain_core_types", "gemachain-sdkreumvm", "dep:hdk", "dep:tofuri-core", "dep:candid", "cairo-lang", "starknet", "eosio", "pallas", "nimiq", "dep:nimiq-primitives", "dep:near-primitives-core", "dep:cw2", "dep:cw20","dep:cw20-base", "cardano", "dep:cosmwasm-std", "dep:gemachain-sdk", "dep:ethers-core", "dep:mv-core-types", "dep:move-deps", "metaplex", "dep:spl-token", "dep:near-sdk","dep:gemachain-frozen-abi", "dep:safecoin-sdk", "dep:solana-sdk", "dep:ethabi", "dep:cxmr-currency",
            "dep:sp-core", "dep:frame-support", "monero", "cartallum", "kaspa", "zeropoolnetwork",  "dep:miraland-logger", "dep:miraland-transaction-status", "hyperledger"]),
            &[("cryptography::cryptocurrencies", 1.7, 0.8), ("finance", 0.1, 0.), ("development-tools::procedural-macro-helpers", 0.3, 0.), ("science::math", 0.7, 0.), ("science::bio", 0.2, 0.), ("parsing", 0.6, 0.), ("network-programming", 0.6, 0.), ("data-structures", 0.6, 0.), ("cryptography", 0.4, 0.),
            ("database-implementations", 0.3, 0.), ("compilers", 0.5, 0.), ("database", 0.1, 0.), ("email", 0.5, 0.), ("value-formatting", 0.7, 0.), ("rust-patterns", 0.7, 0.), ("algorithms", 0.9, 0.),
            ("encoding", 0.7, 0.), ("embedded", 0.7, 0.), ("no-std", 0.4, 0.), ("development-tools::cargo-plugins", 0.6, 0.), ("command-line-utilities", 0.4, 0.), ("multimedia::video", 0.5, 0.),
            ("multimedia", 0.6, 0.), ("accessibility", 0.2, 0.),  ("development-tools::testing", 0.5, 0.), ("development-tools", 0.6, 0.), ("date-and-time", 0.4, 0.), ("wasm", 0.3, 0.)]),
        (Cond::Any(&["coinbase", "fedimint", "litecoin", "chiapos", "chia-blockchain", "gemachain", "dep:miraland-program", "dep:miraland-account-decoder", "bitfinex",
            "dep:finality-grandpa", "nft", "dep:parity-scale-codec", "dep:solana-vote-program", "dep:grin_util", "dep:ethereum-block", "dep:ethereum-types",
            "dep:oasis-types", "nanocurrency", "dep:ethereum-types", "near-protocol", "dep:ethbloom", "dep:sp-io", "self-sovereign"]),
            &[("cryptography::cryptocurrencies", 1.7, 0.8), ("finance", 0.1, 0.), ("wasm", 0.5, 0.), ("science::math", 0.8, 0.), ("data-structures", 0.7, 0.), ("cryptography", 0.4, 0.), ("database-implementations", 0.7, 0.),
            ("database", 0.2, 0.), ("email", 0.6, 0.), ("value-formatting", 0.7, 0.), ("algorithms", 0.9, 0.), ("embedded", 0.8, 0.), ("no-std", 0.4, 0.), ("development-tools::cargo-plugins", 0.7, 0.),
            ("command-line-utilities", 0.8, 0.), ("accessibility", 0.5, 0.), ("development-tools::testing", 0.6, 0.), ("development-tools", 0.6, 0.), ("date-and-time", 0.4, 0.)]),
        (Cond::Any(&["cryptocurrency", "tetcoin", "lightning-network", "dep:sp-blockchain", "nearprotocol", "dep:blockchain", "dep:ink_env", "dep:ink_primitives", "altcoin", "dep:web3",
            "dep:near-primitives", "dep:cw-utils", "dep:bitcoin", "dep:holochain_core_types", "dep:grin_api", "dep:wedpr_l_utils", "dep:grin_core",
            "bitcoincash", "cryptocurrencies", "blockchain", "dep:solana-frozen-abi", "blockchains", "exonum", "solana"]),
            &[("cryptography::cryptocurrencies", 1.7, 0.8), ("finance", 0.1, 0.), ("science::math", 0.7, 0.), ("development-tools::procedural-macro-helpers", 0.3, 0.), ("multimedia", 0.8, 0.), ("data-structures", 0.7, 0.), ("cryptography", 0.4, 0.),
            ("database-implementations", 0.4, 0.), ("database", 0.2, 0.), ("email", 0.5, 0.), ("value-formatting", 0.7, 0.), ("rust-patterns", 0.7, 0.),
            ("algorithms", 0.8, 0.), ("os::unix-apis", 0.3, 0.), ("caching", 0.6, 0.), ("os", 0.5, 0.), ("science", 0.5, 0.), ("embedded", 0.6, 0.), ("no-std", 0.4, 0.), ("development-tools::cargo-plugins", 0.6, 0.),
            ("command-line-utilities", 0.8, 0.), ("web-programming::websocket", 0.4, 0.), ("multimedia::video", 0.5, 0.), ("multimedia", 0.6, 0.),
            ("development-tools::testing", 0.6, 0.), ("wasm", 0.5, 0.), ("development-tools", 0.6, 0.), ("date-and-time", 0.4, 0.)]),
        (Cond::Any(&["parity", "stellar", "coin", "wallet", "dep:solana-transaction-status", "serai", "eth2", "wallets", "bitstamp", "dep:parity-codec", "dep:libp2p"]),
            &[("cryptography::cryptocurrencies", 1.3, 0.1), ("finance", 0.1, 0.), ("web-programming::websocket", 0.8, 0.), ("development-tools::testing", 0.3, 0.), ("cryptography", 0.4, 0.), ("command-line-utilities", 0.8, 0.), ("rust-patterns", 0.7, 0.),
            ("multimedia", 0.6, 0.), ("compilers", 0.5, 0.), ("science::math", 0.8, 0.)]),
        (Cond::All(&["bitcoin", "cash"]), &[("cryptography::cryptocurrencies", 1.5, 0.2), ("finance", 0.1, 0.), ("cryptography", 0.4, 0.)]),
        (Cond::All(&["kraken", "exchange"]), &[("cryptography::cryptocurrencies", 1.5, 0.3), ("finance", 0.1, 0.), ("wasm", 0.5, 0.), ("cryptography", 0.4, 0.)]),
        (Cond::All(&["fuel", "sway"]), &[("cryptography::cryptocurrencies", 1.5, 0.3), ("finance", 0.1, 0.), ("gui", 0.1, 0.), ("cryptography", 0.4, 0.), ("wasm", 0.4, 0.)]),
        (Cond::All(&["smart", "contract"]), &[("cryptography::cryptocurrencies", 1.3, 0.1), ("finance", 0.1, 0.), ("cryptography", 0.6, 0.), ("no-std", 0.9, 0.), ("wasm", 0.5, 0.)]),
        (Cond::All(&["proof", "work"]), &[("cryptography::cryptocurrencies", 1.3, 0.1), ("finance", 0.1, 0.), ("no-std", 0.9, 0.), ("wasm", 0.5, 0.)]),
        (Cond::All(&["smart", "contracts"]), &[("cryptography::cryptocurrencies", 1.3, 0.1), ("finance", 0.1, 0.), ("no-std", 0.9, 0.), ("wasm", 0.25, 0.)]),
        (Cond::Any(&["smart-contracts", "dep:parity-scale-codec"]), &[("cryptography::cryptocurrencies", 1.3, 0.1), ("finance", 0.1, 0.), ("no-std", 0.5, 0.), ("wasm", 0.2, 0.), ("development-tools::testing", 0.3, 0.)]),
        (Cond::All(&["fungible", "tokens"]), &[("cryptography::cryptocurrencies", 1.3, 0.1), ("finance", 0.1, 0.), ("cryptography", 0.4, 0.)]),
        (Cond::All(&["crypto", "asset"]), &[("cryptography::cryptocurrencies", 1.2, 0.05), ("development-tools::testing", 0.3, 0.)]),
        (Cond::Any(&["cosmwasm"]), &[("cryptography::cryptocurrencies", 1.3, 0.2), ("finance", 0.1, 0.), ("wasm", 0.3, 0.), ("cryptography", 0.4, 0.)]),
        (Cond::All(&["ledger", "public"]), &[("cryptography::cryptocurrencies", 1.1, 0.1), ("finance", 0.8, 0.), ("wasm", 0.5, 0.), ("embedded", 0.5, 0.)]),
        (Cond::All(&["ledger", "finance"]), &[("finance", 1.2, 0.)]),
        (Cond::Any(&["polkadot", "parachain", "solarti", "diem", "faucet", "dep:borsh", "staking", "proof-of-work", "eth", "substrate", "dep:frame-support"]),
            &[("cryptography::cryptocurrencies", 1.2, 0.05), ("development-tools::procedural-macro-helpers", 0.3, 0.), ("finance", 0.1, 0.), ("cryptography", 0.8, 0.),
            ("development-tools::testing", 0.3, 0.)]),
        (Cond::Any(&["uint"]), &[("rust-patterns", 1.2, 0.), ("cryptography::cryptocurrencies", 0.8, 0.)]),
        (Cond::All(&["integer", "types"]), &[("rust-patterns", 1.1, 0.), ("cryptography::cryptocurrencies", 0.6, 0.)]),
        (Cond::All(&["primitive", "integer"]), &[("rust-patterns", 1.1, 0.), ("cryptography::cryptocurrencies", 0.6, 0.)]),
        (Cond::All(&["unsigned", "integers"]), &[("data-structures", 1.1, 0.), ("cryptography::cryptocurrencies", 0.9, 0.)]),
        (Cond::Any(&["fixed-size", "fixed-size-integers", "stack-based"]), &[("data-structures", 1.1, 0.), ("algorithms", 1.1, 0.), ("rust-patterns", 1.1, 0.), ("cryptography::cryptocurrencies", 0.5, 0.)]),
        (Cond::Any(&["fixed-size-integers"]), &[("data-structures", 1.2, 0.), ("cryptography::cryptocurrencies", 0.5, 0.)]),

        (Cond::Any(&["bitcoin", "nervos", "exonum"]), &[("database", 0.1, 0.), ("database-implementations", 0.1, 0.)]),
        (Cond::Any(&["network"]), &[("database", 0.8, 0.), ("database-implementations", 0.8, 0.), ("rendering::data-formats", 0.8, 0.)]),
        (Cond::Any(&["libtwitch", "libtwitch-rs"]), &[("cryptography::cryptocurrencies", 0.5, 0.), ("web-programming", 1.1, 0.1)]),

        (Cond::NotAny(&["tokio", "async-runtime", "future", "futures", "dep:tokio", "dep:futures-core", "promise", "promises", "executor", "reactor", "pin", "eventloop", "event-loop", "event", "callback",
            "callbacks", "non-blocking", "async", "waker", "async-await", "message", "queue", "dep:futures", "message-queue", "msg-queue", "stream", "await", "asynchronous", "aio", "mio", "timer", "dep:mio", "dep:async-task"]),
            &[("asynchronous", 0.8, 0.)]),
        (Cond::Any(&["tokio", "async-runtime", "future", "futures", "async-await", "promise", "stream", "non-blocking", "async"]),
            &[("asynchronous", 1.1, 0.1), ("concurrency", 0.9, 0.), ("os", 0.9, 0.), ("command-line-utilities", 0.75, 0.), ("cryptography::cryptocurrencies", 0.6, 0.),
            ("caching", 0.9, 0.), ("value-formatting", 0.5, 0.), ("text-processing", 0.6, 0.), ("config", 0.8, 0.), ("science::math", 0.8, 0.), ("memory-management", 0.9, 0.),
            ("games", 0.15, 0.), ("multimedia::video", 0.9, 0.), ("development-tools::ffi", 0.8, 0.), ("development-tools::procedural-macro-helpers", 0.8, 0.)]),
        (Cond::Any(&["async-std", "await", "job-queue", "background-jobs", "task-queue", "runtime", "asynchronous", "waker", "dep:async-task", "dep:mio", "pin", "dep:pin-utils"]),
            &[("asynchronous", 1.2, 0.1), ("value-formatting", 0.8, 0.0), ("compression", 0.8, 0.0)]),
        (Cond::Any(&["io-uring", "iouring", "liburing", "asyncio", "async-io"]),
            &[("asynchronous", 1.2, 0.1), ("concurrency", 1.1, 0.), ("os::unix-apis", 1.1, 0.1), ("web-programming", 1.1, 0.1), ("network-programming", 1.1, 0.1)]),
        (Cond::Any(&["dep:reqwest", "dep:curl", "dep:hyper"]),
            &[("date-and-time", 0.7, 0.), ("parsing", 0.5, 0.), ("rust-patterns", 0.7, 0.), ("config", 0.9, 0.), ("embedded", 0.7, 0.),  ("hardware-support", 0.7, 0.), ("algorithms", 0.8, 0.),
            ("data-structures", 0.7, 0.), ("rendering", 0.7, 0.),  ("memory-management", 0.7, 0.), ("value-formatting", 0.8, 0.0), ("development-tools::ffi", 0.8, 0.),
            ("development-tools::build-utils", 0.6, 0.), ("development-tools::procedural-macro-helpers", 0.5, 0.)]),
        (Cond::Any(&["dep:tokio", "dep:futures-core", "dep:actix", "dep:mio", "dep:async-std", "dep:tracing", "dep:axum"]),
            &[("value-formatting", 0.7, 0.), ("parsing", 0.6, 0.), ("algorithms", 0.8, 0.), ("date-and-time", 0.8, 0.), ("config", 0.8, 0.), ("compression", 0.9, 0.),
            ("data-structures", 0.6, 0.), ("rust-patterns", 0.6, 0.), ("no-std", 0.6, 0.)]),
        (Cond::Any(&["dep:tokio", "dep:mio", "dep:clap"]), &[("wasm", 0.8, 0.)]),

        (Cond::NotAny(&["settings", "configuration", "config", "dotenv", "envvar", "configurator", "dotfile", "dotfiles", "dotproperties", "env", "customization", "environment"]),
            &[("config", 0.75, 0.)]),
        (Cond::Any(&["settings", "configuration", "configurator", "config"]),
            &[("config", 1.15, 0.2), ("development-tools::debugging", 0.8, 0.), ("os::macos-apis", 0.95, 0.), ("caching", 0.9, 0.), ("os::unix-apis", 0.95, 0.), ("algorithms", 0.9, 0.), ("wasm", 0.8, 0.),
            ("parsing", 0.7, 0.), ("command-line-utilities", 0.9, 0.), ("internationalization", 0.9, 0.), ("command-line-interface", 0.9, 0.)]),
        (Cond::Any(&["configure", "configuration-language", "dotenv", "envvar", "dotfile", "dotfiles", "environment"]),
            &[("config", 1.2, 0.1), ("command-line-interface", 0.9, 0.), ("multimedia::video", 0.8, 0.), ("parsing", 0.8, 0.), ("parser-implementations", 0.9, 0.)]),
        (Cond::All(&["configuration", "management"]),
            &[("config", 1.2, 0.1), ("parsing", 0.7, 0.), ("caching", 0.9, 0.)]),
        (Cond::Any(&["log", "logger", "logging", "logs", "diagnostics", "logging-library", "slog"]),
            &[("development-tools::debugging", 1.2, 0.1), ("rust-patterns", 0.9, 0.), ("wasm", 0.7, 0.), ("no-std", 0.7, 0.),
            ("concurrency", 0.8, 0.), ("algorithms", 0.6, 0.), ("asynchronous", 0.9, 0.),  ("parser-implementations", 0.9, 0.), ("date-and-time", 0.9, 0.), ("multimedia::video", 0.8, 0.),
            ("config", 0.9, 0.), ("emulators", 0.8, 0.), ("data-structures", 0.8, 0.), ("parsing", 0.9, 0.), ("encoding", 0.8, 0.), ("games", 0.01, 0.), ("development-tools::profiling", 0.9, 0.),
            ("command-line-interface", 0.9, 0.), ("command-line-utilities", 0.8, 0.)]),
        (Cond::Any(&["dmesg", "kmsg", "libproc"]),
            &[("development-tools::debugging", 1.1, 0.1), ("multimedia::audio", 0.5, 0.)]),
        (Cond::Any(&["newrelic", "open-telemetry", "telemetry", "jaeger"]),
            &[("development-tools::debugging", 1.1, 0.1), ("parsing", 0.3, 0.), ("config", 0.9, 0.), ("value-formatting", 0.8, 0.), ("development-tools::profiling", 1.1, 0.1)]),
        (Cond::Any(&["tracing", "minidump", "crash", "metrics", "prometheus", "breakpad", "crash-context", "crash-reporting", "crash-reports", "statsd", "datadog"]),
            &[("development-tools::debugging", 1.15, 0.1), ("web-programming", 0.9, 0.), ("filesystem", 0.9, 0.), ("database-implementations", 0.9, 0.),
            ("value-formatting", 0.8, 0.), ("web-programming::http-server", 0.8, 0.)]),
        (Cond::Any(&["dep:tracing-core"]), &[("development-tools::debugging", 1.1, 0.), ("development-tools::ffi", 0.8, 0.), ("parsing", 0.6, 0.)]),
        (Cond::Any(&["dlsym", "debug", "debugging", "debugger", "debugging-tools", "debugging-tool", "disassemlber", "demangle", "dwarf", "stacktrace", "sentry"]),
            &[("development-tools::debugging", 1.2, 0.1), ("concurrency", 0.9, 0.), ("no-std", 0.9, 0.), ("database", 0.7, 0.), ("database-implementations", 0.7, 0.), ("algorithms", 0.7, 0.), ("wasm", 0.9, 0.), ("emulators", 0.9, 0.), ("games", 0.01, 0.),
            ("development-tools::profiling", 0.7, 0.), ("command-line-utilities", 0.8, 0.)]),
        (Cond::Any(&["backtrace", "disassembly", "disassembler", "symbolic", "symbolicate", "coredump", "valgrind", "lldb"]),
            &[("development-tools::debugging", 1.2, 0.1), ("concurrency", 0.9, 0.), ("data-structures", 0.9, 0.), ("algorithms", 0.7, 0.), ("wasm", 0.9, 0.), ("emulators", 0.9, 0.),
            ("games", 0.01, 0.), ("multimedia", 0.8, 0.), ("development-tools::profiling", 0.7, 0.), ("caching", 0.8, 0.), ("command-line-utilities", 0.8, 0.)]),
        (Cond::Any(&["elf", "archive"]), &[("development-tools::debugging", 0.8, 0.), ("games", 0.4, 0.), ("parsing", 0.9, 0.)]),
        (Cond::Any(&["monitor", "monitoring"]), &[("development-tools::debugging", 1.1, 0.), ("development-tools::profiling", 1.1, 0.), ("development-tools", 1.1, 0.)]),
        (Cond::Any(&["elf"]), &[("encoding", 1.1, 0.), ("os::unix-apis", 1.1, 0.)]),
        (Cond::Any(&["eh-frame", "demangle", "readelf", "elf-parser"]), &[("development-tools::debugging", 1.1, 0.), ("encoding", 1.1, 0.), ("os::unix-apis", 1.1, 0.), ("parsing", 0.7, 0.)]),
        (Cond::Any(&["travis", "jenkins", "ci", "testing", "quickcheck", "test-driven", "tdd", "unittest"]),
            &[("development-tools::testing", 1.2, 0.2), ("development-tools::cargo-plugins", 0.9, 0.), ("rust-patterns", 0.9, 0.),
            ("development-tools", 0.8, 0.), ("os::macos-apis", 0.8, 0.), ("development-tools::build-utils", 0.8, 0.), ("parsing", 0.8, 0.),
            ("games", 0.4, 0.), ("rendering::data-formats", 0.5, 0.), ("text-processing", 0.5, 0.)]),
        (Cond::Any(&["unittests", "junit", "continuous-integration", "ci-cd", "unit-testing", "pentest", "code-coverage", "testbed", "mock", "mocks"]),
            &[("development-tools::testing", 1.2, 0.2), ("development-tools::cargo-plugins", 0.9, 0.), ("rust-patterns", 0.9, 0.),
            ("development-tools", 0.8, 0.), ("os::macos-apis", 0.8, 0.), ("development-tools::build-utils", 0.8, 0.), ("parsing", 0.8, 0.),
            ("games", 0.4, 0.), ("rendering::data-formats", 0.5, 0.), ("text-processing", 0.5, 0.)]),
        (Cond::All(&["gui", "automation"]), &[("development-tools::testing", 1.3, 0.3), ("gui", 0.25, 0.), ("no-std", 0.9, 0.), ("algorithms", 0.8, 0.), ("parsing", 0.5, 0.), ("os::macos-apis", 0.8, 0.)]),
        (Cond::All(&["continuous", "integration"]), &[("development-tools::testing", 1.3, 0.3)]),
        (Cond::All(&["test", "framework"]), &[("development-tools::testing", 1.3, 0.2), ("config", 0.9, 0.), ("os::unix-apis", 0.5, 0.), ("database-implementations", 0.6, 0.)]),
        (Cond::All(&["github", "actions"]), &[("development-tools::testing", 1.2, 0.1)]),
        (Cond::Any(&["fuzzing", "fuzzer"]), &[("development-tools::testing", 1.3, 0.2), ("config", 0.9, 0.)]),
        (Cond::Any(&["tests", "black-box"]), &[("development-tools::testing", 1.2, 0.)]),
        (Cond::Any(&["black-box-benchmarking", "black-box-optimization"]), &[("development-tools::testing", 0.7, 0.)]),
        (Cond::Any(&["automation"]), &[("compression", 0.75, 0.)]),
        (Cond::Any(&["testing-framework", "test-runner", "unittesting", "unit-testing", "integration-testing", "american-fuzzy-lop", "afl"]), &[
            ("development-tools::testing", 1.2, 0.2), ("development-tools::profiling", 0.5, 0.), ("development-tools::debugging", 0.7, 0.),
            ("development-tools", 0.9, 0.), ("filesystem", 0.9, 0.), ("development-tools::cargo-plugins", 0.9, 0.)]),
        (Cond::All(&["integration", "test"]), &[("development-tools::testing", 1.2, 0.1), ("rust-patterns", 0.9, 0.), ("date-and-time", 0.6, 0.)]),
        (Cond::All(&["integration", "tests"]), &[("development-tools::testing", 1.2, 0.1), ("date-and-time", 0.6, 0.)]),
        (Cond::All(&["unit", "tests"]), &[("development-tools::testing", 1.2, 0.1), ("date-and-time", 0.6, 0.)]),
        (Cond::Any(&["diff", "writer", "table", "gcd", "sh", "unwrap", "build", "relative", "path", "fail"]),
            &[("development-tools::testing", 0.5, 0.), ("internationalization", 0.7, 0.), ("gui", 0.7, 0.)]),
        (Cond::Any(&["string", "strings"]), &[("command-line-utilities", 0.5, 0.), ("multimedia::images", 0.5, 0.)]),
        (Cond::Any(&["rope"]), &[("command-line-utilities", 0.5, 0.), ("multimedia::images", 0.5, 0.)]),
        (Cond::Any(&["binary", "streaming", "version", "buffer", "recursive", "escape"]),
            &[("development-tools::testing", 0.75, 0.), ("internationalization", 0.75, 0.), ("development-tools::build-utils", 0.8, 0.)]),
        (Cond::Any(&["escape", "text-processing", "rot13"]), &[("text-processing", 1.2, 0.05), ("compression", 0.8, 0.)]),
        (Cond::Any(&["string", "unescape", "opengl", "opengl-es", "memchr", "ios",  "cuda"]),
            &[("development-tools::testing", 0.75, 0.), ("internationalization", 0.8, 0.), ("development-tools::build-utils", 0.8, 0.)]),
        (Cond::Any(&["dex", "android", "android-ndk", "ndk"]), &[("development-tools", 1.2, 0.05), ("parsing", 0.7, 0.), ("config", 0.9, 0.), ("caching", 0.9, 0.)]),
        (Cond::All(&["build", "system"]), &[("development-tools", 1.1, 0.1), ("development-tools::build-utils", 1.15, 0.1)]),
        (Cond::All(&["android", "apk"]), &[("development-tools", 1.1, 0.1), ("development-tools::build-utils", 1.15, 0.1), ("embedded", 0.6, 0.)]),
        (Cond::Any(&["streams", "streaming"]), &[("algorithms", 1.1, 0.03), ("network-programming", 1.1, 0.), ("development-tools::cargo-plugins", 0.5, 0.), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.), ("development-tools::cargo-plugins", 0.7, 0.)]),
        (Cond::Any(&["boolean", "search"]), &[("development-tools::testing", 0.9, 0.), ("multimedia::images", 0.8, 0.), ("multimedia::audio", 0.8, 0.),
            ("internationalization", 0.8, 0.), ("rendering::data-formats", 0.8, 0.)]),
        (Cond::Any(&["fuzzy"]), &[("algorithms", 1.1, 0.), ("multimedia::audio", 0.8, 0.), ("internationalization", 0.8, 0.), ("rendering::data-formats", 0.8, 0.)]),
        (Cond::Any(&["text"]), &[("development-tools::testing", 0.9, 0.), ("caching", 0.8, 0.), ("multimedia::images", 0.8, 0.), ("multimedia::audio", 0.8, 0.),
            ("rendering::data-formats", 0.9, 0.)]),

        (Cond::Any(&["ai", "piston", "logic", "2d", "graphic"]), &[("web-programming::http-client", 0.5, 0.), ("web-programming::websocket", 0.5, 0.), ("parsing", 0.9, 0.)]),

        (Cond::Any(&["dep:cookie"]), &[("web-programming", 1.25, 0.1), ("web-programming::http-client", 1.25, 0.1), ("web-programming::http-server", 1.25, 0.1), ("parsing", 0.4, 0.)]),
        (Cond::Any(&["activitypub", "activitystreams", "pubsub"]),
            &[("web-programming", 1.25, 0.2), ("network-programming", 1.25, 0.2), ("rust-patterns", 0.9, 0.), ("algorithms", 0.8, 0.), ("database-implementations", 0.8, 0.),
            ("web-programming::websocket", 1.1, 0.), ("command-line-utilities", 0.75, 0.)]),
        (Cond::Any(&["websocket", "websockets"]), &[("web-programming::websocket", 1.85, 0.4), ("no-std", 0.9, 0.), ("wasm", 0.8, 0.),
            ("development-tools::cargo-plugins", 0.5, 0.), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.), ("command-line-utilities", 0.5, 0.)]),
        (Cond::NotAny(&["sse", "tungstenite", "websocket", "websockets", "ws", "pubsub", "broadcast", "server-sent-events", "rfc6455"]), &[("web-programming::websocket", 0.5, 0.)]),
        (Cond::Any(&["servo"]), &[("web-programming::websocket", 0.5, 0.), ("no-std", 0.3, 0.), ("command-line-interface", 0.5, 0.)]),
        (Cond::Any(&["dep:httparse", "dep:serde_qs"]), &[("no-std", 0.9, 0.), ("database-implementations", 0.8, 0.), ("parsing", 0.1, 0.)]),
        (Cond::Any(&["router"]), &[("web-programming::websocket", 0.6, 0.), ("web-programming::http-client", 0.7, 0.), ("command-line-interface", 0.8, 0.), ("parsing", 0.7, 0.)]),
        (Cond::Any(&["dep:tokio-tungstenite", "dep:tungstenite", "dep:async-tungstenite"]), &[
            ("web-programming::websocket", 1.2, 0.1), ("web-programming", 1.2, 0.), ("web-programming::http-server", 1.1, 0.), ("network-programming", 1.1, 0.), ("wasm", 0.7, 0.), ("algorithms", 0.7, 0.), ("data-structures", 0.7, 0.),
            ("command-line-interface", 0.8, 0.), ("command-line-utilities", 0.8, 0.), ("multimedia::images", 0.8, 0.), ("caching", 0.9, 0.), ("multimedia::audio", 0.8, 0.)]),

        (Cond::Any(&["generic"]), &[("development-tools::debugging", 0.5, 0.), ("web-programming::websocket", 0.5, 0.)]),
        (Cond::Any(&["quaternion"]), &[("science::math", 1.1, 0.1), ("game-development", 1.1, 0.), ("parsing", 0.25, 0.), ("algorithms", 0.9, 0.), ("command-line-utilities", 0.75, 0.)]),
        (Cond::Any(&["bitmap", "raster"]), &[("rendering::data-formats", 1.1, 0.0), ("multimedia::images", 1.1, 0.0), ("internationalization", 0.5, 0.), ("parsing", 0.8, 0.)]),
        (Cond::Any(&["plural", "pluralize", "iso3166-1", "iso3166", "iso-3166-1", "bcp47", "translate"]),
            &[("internationalization", 1.2, 0.1), ("parsing", 0.7, 0.), ("caching", 0.9, 0.)]),
        (Cond::Any(&["internationalisation", "i18n", "iso-639", "internationalization"]),
            &[("internationalization", 1.5, 0.3), ("value-formatting", 0.9, 0.), ("parsing", 0.8, 0.), ("os", 0.9, 0.), ("algorithms", 0.8, 0.),
            ("network-programming", 0.9, 0.), ("web-programming", 0.8, 0.), ("web-programming::http-server", 0.7, 0.)]),
        (Cond::Any(&["gettext"]), &[("internationalization", 1.3, 0.2), ("parsing", 0.5, 0.), ("caching", 0.9, 0.)]),
        (Cond::Any(&["math"]), &[("rendering", 0.75, 0.), ("command-line-utilities", 0.75, 0.)]),
        (Cond::Any(&["rendering", "mssql", "sql", "sqlx"]), &[("rendering::data-formats", 0.2, 0.), ("caching", 0.8, 0.), ("value-formatting", 0.8, 0.), ("hardware-support", 0.6, 0.), ("parsing", 0.7, 0.)]),

        (Cond::Any(&["speech-recognition"]), &[("science", 1.3, 0.1), ("multimedia::audio", 1.3, 0.1), ("config", 0.9, 0.), ("caching", 0.8, 0.)]),
        (Cond::Any(&["tts", "speech"]), &[("multimedia::audio", 1.1, 0.), ("internationalization", 0.6, 0.), ("development-tools::ffi", 0.8, 0.), ("config", 0.9, 0.), ("caching", 0.9, 0.)]),
        (Cond::Any(&["downsample", "dsp", "samplerate"]), &[("multimedia::audio", 1.2, 0.1), ("filesystem", 0.7, 0.), ("parsing", 0.7, 0.)]),
        (Cond::Any(&["music", "music-player", "audio-player", "chiptune", "synth", "chords", "audio", "digital-audio", "sound", "sound-convolution", "sounds", "speech", "microphone"]),
            &[("multimedia::audio", 1.3, 0.3), ("command-line-utilities", 0.8, 0.), ("config", 0.9, 0.), ("caching", 0.9, 0.), ("database-implementations", 0.8, 0.), ("multimedia::images", 0.6, 0.),
            ("multimedia", 0.9, 0.), ("rendering::graphics-api", 0.6, 0.), ("rendering", 0.6, 0.), ("cryptography::cryptocurrencies", 0.6, 0.), ("rust-patterns", 0.75, 0.),
            ("command-line-interface", 0.5, 0.), ("caching", 0.8, 0.), ("network-programming", 0.8, 0.), ("no-std", 0.8, 0.)]),
        (Cond::Any(&["flac", "spotify", "audio-effect", "libopus", "vst", "vorbis", "midi", "soundfont", "pulseaudio", "mp3", "aac", "wav", "synth", "synthesizer"]),
            &[("multimedia::audio", 1.3, 0.3), ("multimedia::video", 0.9, 0.), ("command-line-utilities", 0.8, 0.), ("multimedia::images", 0.6, 0.), ("rendering::graphics-api", 0.75, 0.),
            ("cryptography::cryptocurrencies", 0.6, 0.), ("command-line-interface", 0.5, 0.), ("caching", 0.8, 0.), ("parsing", 0.4, 0.)]),
        (Cond::Any(&["nyquist", "osc"]), &[("multimedia::audio", 1.1, 0.1), ("game-development", 0.8, 0.), ("network-programming", 0.8, 0.)]),
        (Cond::Any(&["audio-plugin", "audio-processing", "vst-plugin", "spotify", "lv2-plugin"]), &[("multimedia::audio", 1.3, 0.2), ("development-tools::testing", 0.3, 0.), ("network-programming", 0.8, 0.)]),
        (Cond::Any(&["dep:cpal", "dep:coreaudio-rs", "dep:surge-filter", "dep:surge-constants"]),
            &[("multimedia::audio", 1.1, 0.1), ("development-tools::ffi", 0.8, 0.), ("os::windows-apis", 0.8, 0.), ("caching", 0.7, 0.)]),
        (Cond::All(&["gain", "level"]), &[("multimedia::audio", 1.2, 0.1)]),
        (Cond::All(&["opus", "db"]), &[("multimedia::audio", 0.6, 0.)]),
        (Cond::All(&["opus", "codec"]), &[("multimedia::audio", 1.2, 0.2), ("caching", 0.9, 0.)]),
        (Cond::All(&["opus", "database"]), &[("multimedia::audio", 0.7, 0.)]),
        (Cond::All(&["pcm", "stereo"]), &[("multimedia::audio", 1.2, 0.1), ("caching", 0.9, 0.)]),
        (Cond::All(&["dsp", "audio"]), &[("multimedia::audio", 1.2, 0.1), ("science::math", 0.6, 0.)]),
        (Cond::Any(&["signal-processing"]), &[("multimedia::audio", 1.1, 0.), ("science::math", 1.2, 0.05), ("caching", 0.9, 0.)]),
        (Cond::Any(&["ogg"]), &[("multimedia::audio", 1.1, 0.), ("accessibility", 0.7, 0.)]),
        (Cond::All(&["signal-processing", "audio"]), &[("multimedia::audio", 1.2, 0.1), ("science::math", 0.6, 0.)]),
        (Cond::All(&["fft", "audio"]), &[("multimedia::audio", 1.2, 0.1), ("science::math", 0.6, 0.), ("caching", 0.9, 0.)]),
        (Cond::All(&["gain", "microphone"]), &[("multimedia::audio", 1.2, 0.1), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::All(&["mod", "tracker"]), &[("multimedia::audio", 1.1, 0.)]),
        (Cond::Any(&["pulseaudio", "alsa", "openal", "opensles"]), &[("os::unix-apis", 1.1, 0.1), ("multimedia::audio", 1.1, 0.1), ("science::math", 0.6, 0.), ("command-line-utilities", 0.75, 0.)]),
        (Cond::Any(&["waveform", "wave"]), &[("multimedia::audio", 1.1, 0.), ("science::math", 1.1, 0.)]),
        (Cond::Any(&["perspective", "graphics", "cam", "signal-handling"]), &[("multimedia::audio", 0.4, 0.)]),
        (Cond::Any(&["ffi", "sys", "daemon"]), &[("development-tools::build-utils", 0.8, 0.), ("science::math", 0.6, 0.), ("multimedia::audio", 0.9, 0.)]),
        (Cond::Any(&["sigabrt", "sigint"]), &[("multimedia::audio", 0.1, 0.), ("algorithms", 0.8, 0.), ("multimedia", 0.3, 0.)]),
        (Cond::Any(&["sigterm", "sigquit"]), &[("multimedia::audio", 0.1, 0.), ("multimedia", 0.3, 0.)]),

        (Cond::Any(&["multimedia", "chromecast", "media", "dvd", "mpeg"]), &[
            ("multimedia", 1.3, 0.3), ("algorithms", 0.8, 0.), ("science::math", 0.9, 0.), ("rust-patterns", 0.8, 0.), ("data-structures", 0.9, 0.), ("encoding", 0.5, 0.), ("development-tools::testing", 0.7, 0.),
            ("development-tools::build-utils", 0.7, 0.), ("parsing", 0.8, 0.)]),
        (Cond::Any(&["dep:gstreamer"]), &[("multimedia", 1.2, 0.1), ("science::math", 0.6, 0.), ("development-tools::testing", 0.7, 0.), ("caching", 0.8, 0.), ("development-tools::build-utils", 0.7, 0.), ("parsing", 0.6, 0.)]),
        (Cond::Any(&["dep:gstreamer-audio"]), &[("multimedia", 1.2, 0.1), ("multimedia::audio", 1.2, 0.1), ("caching", 0.9, 0.)]),
        (Cond::Any(&["dep:gstreamer-video"]), &[("multimedia", 1.2, 0.1), ("multimedia::video", 1.2, 0.2), ("caching", 0.9, 0.)]),
        (Cond::Any(&["dep:allegro", "nannou", "art"]), &[("multimedia", 1.1, 0.), ("science::geo", 0.8, 0.)]),
        (Cond::Any(&["image", "images", "viewer", "photos", "picture", "create-art", "image-rendering", "image-resizing", "resize-image", "image-optimization"]), &[
            ("multimedia::images", 1.2, 0.1), ("parser-implementations", 0.9, 0.), ("parsing", 0.6, 0.), ("data-structures", 0.8, 0.)]),
        (Cond::Any(&["icons", "icns", "ico", "favicon", "screenshots"]), &[("multimedia::images", 1.2, 0.1), ("no-std", 0.5, 0.), ("gui", 1.1, 0.), ("caching", 0.9, 0.), ("accessibility", 0.8, 0.)]),
        (Cond::All(&["kernel", "image"]), &[("multimedia::images", 0.5, 0.)]),
        (Cond::All(&["screen", "capture"]), &[("multimedia::images", 1.2, 0.1), ("gui", 1.2, 0.1), ("config", 0.9, 0.)]),
        (Cond::Any(&["dep:dssim", "dep:imgref"]), &[("multimedia::images", 1.2, 0.05), ("no-std", 0.2, 0.)]),
        (Cond::Any(&["dep:mozjpeg", "dep:lodepng", "dep:image", "image-converter", "image-viewer", "dep:gdk-pixbuf"]),
            &[("multimedia::images", 1.2, 0.05), ("no-std", 0.5, 0.), ("parsing", 0.5, 0.), ("config", 0.9, 0.)]),
        (Cond::Any(&["dep:rgb", "dep:gif", "dep:png", "gif", "gif-file", "giphy"]), &[("multimedia::images", 1.2, 0.05), ("caching", 0.8, 0.), ("no-std", 0.5, 0.), ("parsing", 0.5, 0.)]),
        (Cond::Any(&["dep:weezl"]), &[("multimedia::images", 1.2, 0.05), ("caching", 0.9, 0.), ("compression", 1.2, 0.)]),
        (Cond::All(&["binary", "image"]), &[("multimedia::images", 0.8, 0.)]),
        (Cond::All(&["bootable", "image"]), &[("multimedia::images", 0.1, 0.)]),
        (Cond::All(&["image", "generation"]), &[("multimedia::images", 1.1, 0.), ("no-std", 0.7, 0.)]),
        (Cond::All(&["image", "processing"]), &[("multimedia::images", 1.2, 0.), ("no-std", 0.7, 0.)]),
        (Cond::All(&["qr", "code"]), &[("multimedia::images", 1.15, 0.), ("caching", 0.9, 0.), ("parsing", 0.8, 0.)]),
        (Cond::Any(&["qr-code", "qrcode"]), &[("multimedia::images", 1.2, 0.05), ("science::math", 0.6, 0.), ("parsing", 0.5, 0.)]),
        (Cond::Any(&["dicom"]), &[("multimedia::images", 1.2, 0.1), ("parser-implementations", 1.1, 0.05), ("science", 1.05, 0.)]),
        (Cond::Any(&["fourcc"]), &[("multimedia::video", 1.1, 0.05), ("multimedia", 1.1, 0.05), ("rust-patterns", 1.1, 0.05)]),
        (Cond::Any(&["hdr"]), &[("multimedia::images", 1.1, 0.1), ("multimedia::video", 1.1, 0.1)]),
        (Cond::Any(&["flif", "png", "libpng", "jpeg2000", "webp", "libwebp", "j2k", "jpeg", "libjpeg", "heif", "heic", "avif", "avic", "exif", "ocr", "svg", "svg-parser", "pixel", "manga"]), &[
            ("multimedia::images", 1.3, 0.15), ("multimedia::audio", 0.8, 0.), ("multimedia::video", 0.9, 0.), ("parsing", 0.5, 0.), ("caching", 0.9, 0.), ("rendering::graphics-api", 0.6, 0.),
            ("encoding", 0.8, 0.), ("parsing", 0.8, 0.), ("rust-patterns", 0.6, 0.), ("data-structures", 0.9, 0.)]),
        (Cond::Any(&["imagemagick", "gamma", "photo", "openexr", "pixiv"]), &[
            ("multimedia::images", 1.3, 0.15), ("encoding", 0.5, 0.), ("parsing", 0.6, 0.), ("config", 0.6, 0.), ("caching", 0.9, 0.), ("data-structures", 0.8, 0.)]),
        (Cond::Any(&["color", "colors", "colour", "srgb", "colours", "opencv", "color-management", "colorspace", "ciede2000", "color-space", "hsl", "image-scramble", "manga-downloader", "manga-reader", "colorimetry"]),
            &[("multimedia::images", 1.2, 0.1), ("multimedia", 1.1, 0.), ("multimedia::audio", 0.6, 0.), ("wasm", 0.6, 0.), ("development-tools::build-utils", 0.8, 0.)]),
        (Cond::Any(&["quantization"]), &[("multimedia::images", 1.2, 0.1), ("multimedia", 1.1, 0.), ("command-line-interface", 0.2, 0.)]),
        (Cond::Any(&["webm", "av1", "dvd", "vpx"]), &[("multimedia::video", 1.4, 0.3),
            ("encoding", 0.15, 0.), ("parsing", 0.8, 0.), ("science::math", 0.6, 0.), ("data-structures", 0.7, 0.), ("parsing", 0.5, 0.), ("development-tools::build-utils", 0.8, 0.)]),
        (Cond::Any(&["h265", "h264", "streaming-video", "mpeg-dash", "video-encoder", "ffmpeg", "libavformat", "h263", "movie"]), &[
            ("multimedia::video", 1.5, 0.3), ("encoding", 0.15, 0.), ("science::math", 0.6, 0.), ("science", 0.3, 0.), ("config", 0.9, 0.), ("parsing", 0.1, 0.),
            ("embedded", 0.8, 0.), ("data-structures", 0.8, 0.)]),
        (Cond::Any(&["x265", "x264", "mp4", "h263", "vp9", "libvpx", "video", "movies"]), &[
            ("multimedia::video", 1.5, 0.3), ("encoding", 0.15, 0.), ("science::math", 0.6, 0.), ("multimedia::audio", 0.9, 0.), ("parsing", 0.15, 0.), ("embedded", 0.9, 0.), ("data-structures", 0.8, 0.)]),
        (Cond::Any(&["webcam", "videocamera"]), &[("multimedia::video", 1.5, 0.3), ("multimedia", 1.1, 0.), ("parsing", 0.1, 0.), ("no-std", 0.9, 0.), ("development-tools::build-utils", 0.8, 0.)]),
        (Cond::Any(&["opengl", "opengl-es", "esolang", "interpreter", "ascii", "mesh", "vulkan", "line"]), &[("multimedia::video", 0.5, 0.)]),
        (Cond::Any(&["reader"]), &[("multimedia::video", 0.85, 0.), ("parser-implementations", 1.1, 0.), ("data-structures", 0.8, 0.)]),
        (Cond::Any(&["audio-video"]), &[("multimedia::video", 1.2, 0.05), ("science::math", 0.6, 0.), ("multimedia::audio", 1.2, 0.05), ("multimedia", 1.2, 0.1), ("caching", 0.9, 0.)]),
        (Cond::Any(&["timer"]), &[("multimedia::video", 0.8, 0.), ("multimedia", 0.8, 0.)]),
        (Cond::Any(&["codec"]), &[("multimedia::video", 1.2, 0.), ("science::math", 0.9, 0.), ("encoding", 1.1, 0.), ("caching", 0.9, 0.)]),
        (Cond::All(&["timer", "rpm"]), &[("date-and-time", 1.1, 0.), ("os", 0.7, 0.), ("os::unix-apis", 0.7, 0.)]),
        (Cond::All(&["rate", "rpm"]), &[("date-and-time", 1.1, 0.), ("os", 0.7, 0.), ("os::unix-apis", 0.7, 0.)]),
        (Cond::All(&["video", "driver"]), &[("multimedia::video", 0.7, 0.), ("parsing", 0.5, 0.), ("science::math", 0.9, 0.), ("caching", 0.9, 0.), ("multimedia::audio", 0.8, 0.)]),
        (Cond::Any(&["sound"]), &[("multimedia::video", 0.9, 0.)]),
        (Cond::Any(&["video-games", "video-game"]), &[("multimedia::video", 0.6, 0.), ("database-implementations", 0.15, 0.), ("science::math", 0.6, 0.), ("parsing", 0.5, 0.), ("config", 0.9, 0.)]),

        (Cond::Any(&["plotting", "codeviz", "viz"]),
            &[("visualization", 1.3, 0.3), ("science::math", 0.5, 0.), ("science", 0.85, 0.), ("science::bio", 0.85, 0.),
            ("command-line-interface", 0.75, 0.), ("command-line-utilities", 0.6, 0.), ("games", 0.01, 0.),
            ("parsing", 0.6, 0.), ("caching", 0.5, 0.), ("development-tools::testing", 0.8, 0.), ("development-tools::build-utils", 0.8, 0.)]),
        (Cond::Any(&["visualizer", "javascript-d3", "graphviz", "visualization", "visualisation", "visualizing", "data-plotting", "graphing"]),
            &[("visualization", 1.3, 0.3), ("parsing", 0.8, 0.), ("rendering::data-formats", 0.8, 0.), ("caching", 0.5, 0.), ("finance", 0.7, 0.), ("wasm", 0.7, 0.)]),
        (Cond::Any(&["dot", "graph", "grid-lines", "d3", "axis-ticks"]), &[("visualization", 1.3, 0.), ("multimedia::images", 0.8, 0.)]),
        (Cond::Any(&["gnuplot", "codeviz", "chart", "plot", "barchart", "sparklines"]),
            &[("visualization", 1.3, 0.3), ("science::math", 0.75, 0.), ("multimedia::images", 0.9, 0.), ("parsing", 0.5, 0.), ("wasm", 0.8, 0.), ("science", 0.8, 0.), ("development-tools::cargo-plugins", 0.5, 0.),
            ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.), ("command-line-interface", 0.5, 0.),
            ("command-line-utilities", 0.75, 0.), ("caching", 0.5, 0.)]),
        (Cond::Any(&["aws", "s3", "gcp", "google-cloud", "cpython", "interpreter", "pdf", "derive"]), &[("visualization", 0.8, 0.), ("cryptography", 0.8, 0.), ("filesystem", 0.8, 0.), ("encoding", 0.9, 0.)]),

        (Cond::Any(&["cve", "vulnerability", "security"]), &[("development-tools", 1.2, 0.1)]),

        (Cond::Any(&["security", "disassemlber", "iota"]), &[("emulators", 0.6, 0.), ("multimedia", 0.8, 0.), ("game-development", 0.8, 0.), ("rust-patterns", 0.9, 0.), ("os::macos-apis", 0.7, 0.)]),
        (Cond::Any(&["compilers"]), &[("development-tools", 1.3, 0.2), ("emulators", 1.2, 0.)]),
        (Cond::Any(&["zx", "gameboy", "super-nintendo", "emulator", "emulation", "chip8-emulator"]),
            &[("emulators", 1.25, 0.15), ("games", 0.7, 0.), ("parsing", 0.3, 0.), ("no-std", 0.7, 0.), ("email", 0.8, 0.), ("concurrency", 0.7, 0.),
            ("text-processing", 0.5, 0.), ("parser-implementations", 0.9, 0.), ("data-structures", 0.8, 0.), ("algorithms", 0.9, 0.),
            ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.), ("development-tools::cargo-plugins", 0.5, 0.), ("multimedia::images", 0.5, 0.),
            ("multimedia::audio", 0.6, 0.), ("no-std", 0.8, 0.), ("gui", 0.8, 0.), ("command-line-interface", 0.5, 0.), ("development-tools::build-utils", 0.8, 0.),
            ("multimedia::video", 0.5, 0.), ("command-line-utilities", 0.75, 0.)]),
        (Cond::Any(&["qemu", "vm", "codegen", "cranelift"]), &[("emulators", 1.4, 0.1), ("parser-implementations", 0.9, 0.), ("parsing", 0.5, 0.), ("development-tools", 1.1, 0.),
            ("multimedia::video", 0.5, 0.), ("multimedia", 0.5, 0.), ("wasm", 0.8, 0.)]),
        (Cond::Any(&["z80", "mos6502", "6502", "intel-8080"]), &[("emulators", 1.3, 0.1), ("hardware-support", 1.3, 0.1), ("embedded", 1.1, 0.), ("wasm", 0.5, 0.), ("multimedia", 0.7, 0.)]),
        (Cond::Any(&["rom", "sega"]), &[("emulators", 1.1, 0.), ("hardware-support", 1.1, 0.)]),
        (Cond::Any(&["c64", "ms-dos", "chip-8", "chip8", "spc700", "snes", "gameboy", "game-boy", "game-boy-advance", "gba", "nintendo", "playstation", "commodore", "nes", "atari", "atari-st"]),
            &[("emulators", 1.3, 0.1), ("game-development", 1.1, 0.05), ("games", 1.1, 0.05), ("multimedia", 0.9, 0.), ("development-tools::build-utils", 0.7, 0.),
            ("development-tools::cargo-plugins", 0.7, 0.),
            ("rendering::graphics-api", 0.7, 0.), ("wasm", 0.5, 0.), ("multimedia::video", 0.5, 0.), ("no-std", 0.8, 0.), ("parsing", 0.7, 0.)]),
        (Cond::All(&["virtual", "machine"]), &[("emulators", 1.4, 0.1), ("simulation", 1.1, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::All(&["virtual", "machines"]), &[("emulators", 1.1, 0.), ("simulation", 1.1, 0.), ("parser-implementations", 0.8, 0.)]),
        (Cond::All(&["game", "gb"]), &[("emulators", 1.2, 0.05), ("wasm", 0.8, 0.)]),
        (Cond::All(&["game", "rom"]), &[("emulators", 1.2, 0.05), ("parsing", 0.5, 0.)]),
        (Cond::All(&["commodore", "64"]), &[("emulators", 1.3, 0.1), ("wasm", 0.8, 0.), ("no-std", 0.9, 0.), ("caching", 0.9, 0.)]),
        (Cond::All(&["emulator", "gb"]), &[("emulators", 1.2, 0.1)]),
        (Cond::All(&["accurate", "cpu"]), &[("emulators", 1.1, 0.)]),
        (Cond::All(&["super", "nintendo"]), &[("emulators", 1.3, 0.1), ("wasm", 0.8, 0.)]),
        (Cond::Any(&["tick-accurate"]), &[("emulators", 1.1, 0.), ("parsing", 0.5, 0.), ("parser-implementations", 0.5, 0.)]),
        (Cond::Any(&["esolang", "interpreter", "jit", "just-in-time", "libjit", "libgccjit", "c2rust", "brainfuck", "malbolge"]),
            &[("compilers", 1.2, 0.2), ("emulators", 0.7, 0.), ("text-processing", 0.7, 0.), ("parsing", 0.5, 0.), ("caching", 0.8, 0.), ("no-std", 0.8, 0.), ("parser-implementations", 0.7, 0.)]),
        (Cond::Any(&["vte", "virtual-terminal", "terminal-emulator"]), &[("emulators", 0.5, 0.), ("parsing", 0.8, 0.), ("command-line-interface", 1.1, 0.05), ("rendering::graphics-api", 0.7, 0.)]),
        (Cond::All(&["terminal", "emulator"]), &[("emulators", 0.8, 0.), ("parsing", 0.7, 0.), ("rendering::engine", 0.9, 0.), ("command-line-interface", 1.2, 0.1)]),

        (Cond::Any(&["radix", "genetic"]), &[("science", 1.4, 0.), ("command-line-utilities", 0.75, 0.)]),

        (Cond::Any(&["protocol-specification"]), &[("gui", 0.5, 0.), ("algorithms", 0.8, 0.), ("command-line-utilities", 0.75, 0.), ("parsing", 0.6, 0.)]),
        (Cond::Any(&["dsl", "embedded", "rtos"]), &[("gui", 0.75, 0.), ("command-line-utilities", 0.75, 0.)]),
        (Cond::Any(&["idl", "asmjs", "webasm"]), &[("gui", 0.5, 0.), ("command-line-utilities", 0.75, 0.)]),
        (Cond::Any(&["javascript", "typescript"]), &[("gui", 0.9, 0.), ("caching", 0.9, 0.), ("command-line-utilities", 0.8, 0.), ("multimedia", 0.7, 0.), ("visualization", 0.8, 0.),
            ("cryptography::cryptocurrencies", 0.6, 0.), ("parsing", 0.7, 0.)]),

        (Cond::Any(&["concurrency", "parallelism", "parallelizm", "structured-concurrency"]),
            &[("concurrency", 1.35, 0.1), ("command-line-utilities", 0.8, 0.), ("games", 0.5, 0.), ("memory-management", 0.8, 0.), ("os", 0.8, 0.), ("parsing", 0.9, 0.), ("simulation", 0.8, 0.)]),
        (Cond::Any(&["spinlock", "semaphore", "parallel", "multithreaded", "barrier", "thread-local"]),
            &[("concurrency", 1.2, 0.1), ("command-line-utilities", 0.8, 0.), ("games", 0.5, 0.), ("memory-management", 0.8, 0.), ("caching", 0.9, 0.), ("os", 0.8, 0.), ("parsing", 0.9, 0.), ("simulation", 0.8, 0.)]),
        (Cond::Any(&["coroutines", "threads", "threadpool", "fork-join", "parallelization", "actor", "openmp"]),
            &[("concurrency", 1.35, 0.1), ("command-line-utilities", 0.8, 0.), ("games", 0.5, 0.), ("memory-management", 0.8, 0.), ("caching", 0.8, 0.), ("os", 0.8, 0.), ("parsing", 0.9, 0.), ("simulation", 0.8, 0.)]),
        (Cond::Any(&["atomic"]), &[("concurrency", 1.15, 0.15), ("data-structures", 0.9, 0.), ("command-line-utilities", 0.75, 0.)]),
        (Cond::Any(&["thread-safe"]), &[("concurrency", 1.15, 0.15), ("data-structures", 1.1, 0.), ("asynchronous", 1.1, 0.), ("algorithms", 0.75, 0.)]),
        (Cond::Any(&["queue", "zookeeper"]), &[("concurrency", 1.2, 0.), ("visualization", 0.6, 0.)]),
        (Cond::Any(&["cuda"]), &[("concurrency", 1.2, 0.), ("development-tools::testing", 0.7, 0.), ("config", 0.8, 0.), ("development-tools::build-utils", 0.7, 0.), ("parsing", 0.8, 0.)]),
        (Cond::Any(&["task-scheduler", "work-stealing"]), &[("concurrency", 1.2, 0.1), ("multimedia", 0.7, 0.), ("multimedia::video", 0.7, 0.)]),
        (Cond::All(&["cuda", "compute"]), &[("concurrency", 1.2, 0.1), ("parsing", 0.5, 0.), ("no-std", 0.9, 0.), ("multimedia", 0.75, 0.)]),
        (Cond::All(&["opencl", "compute"]), &[("concurrency", 1.2, 0.1), ("parsing", 0.5, 0.), ("multimedia", 0.75, 0.)]),
        (Cond::All(&["parallel", "compute"]), &[("concurrency", 1.2, 0.1), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::All(&["parallel", "computing"]), &[("concurrency", 1.2, 0.1)]),

        (Cond::Any(&["futures", "actor"]), &[("concurrency", 1.25, 0.1), ("parsing", 0.5, 0.), ("asynchronous", 1.35, 0.3)]),
        (Cond::Any(&["events", "event"]), &[("asynchronous", 1.2, 0.), ("concurrency", 0.9, 0.), ("command-line-utilities", 0.8, 0.)]),
        (Cond::All(&["loop", "event"]), &[("game-development", 1.2, 0.1), ("parsing", 0.5, 0.), ("asynchronous", 0.8, 0.), ("games", 0.4, 0.)]),
        (Cond::All(&["tile"]), &[("game-development", 1.1, 0.), ("network-programming", 0.8, 0.), ("asynchronous", 0.8, 0.), ("internationalization", 0.8, 0.)]),
        (Cond::All(&["map", "grid"]), &[("game-development", 1.1, 0.)]),
        (Cond::All(&["game", "map"]), &[("game-development", 1.1, 0.1), ("games", 1.1, 0.1), ("data-structures", 0.8, 0.), ("science::geo", 0.7, 0.)]),
        (Cond::All(&["game", "graphics"]), &[("game-development", 1.1, 0.1), ("games", 1.1, 0.1), ("rendering::graphics-api", 1.2, 0.1)]),
        (Cond::Any(&["consensus", "erlang", "gossip"]), &[("concurrency", 1.2, 0.1), ("network-programming", 1.2, 0.1), ("asynchronous", 1.2, 0.1), ("gui", 0.8, 0.)]),

        (Cond::Any(&["gui", "desktop-applications", "desktop-app"]), &[
            ("gui", 1.35, 0.1), ("command-line-interface", 0.15, 0.), ("algorithms", 0.8, 0.), ("caching", 0.9, 0.), ("rendering::graphics-api", 0.9, 0.), ("hardware-support", 0.9, 0.), ("wasm", 0.8, 0.), ("multimedia::video", 0.5, 0.)]),
        (Cond::Any(&["qt", "x11", "wayland", "linux-desktop", "gtk", "gtk4", "gtk3", "gtk-rs", "window-manager", "window-management", "swaywm", "swaybar", "i3wm"]),
            &[("gui", 1.35, 0.1), ("os::unix-apis", 1.2, 0.05), ("rendering::graphics-api", 1.1, 0.), ("os::windows-apis", 0.9, 0.), ("algorithms", 0.8, 0.), ("no-std", 0.7, 0.), ("cryptography::cryptocurrencies", 0.9, 0.),
            ("os::macos-apis", 0.25, 0.), ("caching", 0.5, 0.), ("email", 0.8, 0.), ("parsing", 0.5, 0.), ("development-tools::build-utils", 0.9, 0.), ("wasm", 0.6, 0.), ("command-line-interface", 0.15, 0.)]),
        (Cond::Any(&["sixtyfps", "flutter", "dep:i-slint-common", "declarative-ui", "window-events", "dep:sixtyfps-corelib", "dep:sixtyfps", "slint", "dep:slint", "dep:i-slint-core", "dep:sixtyfps-build", "gui-application"]),
            &[("gui", 1.2, 0.2), ("parsing", 0.5, 0.), ("wasm", 0.5, 0.), ("rendering::graphics-api", 0.8, 0.)]),
        (Cond::All(&["window", "manager"]), &[("gui", 1.4, 0.2), ("parsing", 0.5, 0.), ("caching", 0.9, 0.), ("concurrency", 0.5, 0.), ("hardware-support", 0.5, 0.)]),
        (Cond::Any(&["xaml"]), &[("gui", 1.2, 0.2), ("os::windows-apis", 1.2, 0.1), ("parsing", 0.8, 0.)]),
        (Cond::All(&["gui", "toolkit"]), &[("gui", 1.4, 0.2), ("development-tools::testing", 0.7, 0.), ("rendering::graphics-api", 0.8, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::All(&["status", "bar"]), &[("gui", 1.2, 0.1), ("rendering::graphics-api", 0.8, 0.)]),
        (Cond::All(&["copy", "pasting"]), &[("gui", 1.1, 0.), ("parser-implementations", 0.9, 0.)]),
        (Cond::Any(&["material-design", "user-interface"]), &[("gui", 1.1, 0.05), ("parser-implementations", 0.9, 0.), ("caching", 0.9, 0.)]),
        (Cond::All(&["command-line", "user-interface"]), &[("gui", 0.5, 0.), ("command-line-interface", 1.2, 0.05)]),
        (Cond::All(&["cli", "user-interface"]), &[("gui", 0.5, 0.), ("command-line-interface", 1.2, 0.05)]),
        (Cond::All(&["ui", "interface"]), &[("gui", 1.1, 0.05), ("wasm", 0.9, 0.)]),
        (Cond::All(&["ui", "framework"]), &[("gui", 1.2, 0.), ("compression", 0.9, 0.), ("caching", 0.9, 0.), ("wasm", 0.9, 0.), ("database", 0.6, 0.), ("database-implementations", 0.6, 0.), ("rendering::graphics-api", 1.1, 0.)]),
        (Cond::All(&["ui", "layout"]), &[("gui", 1.1, 0.1), ("rendering::graphics-api", 0.8, 0.)]),
        (Cond::Any(&["window", "ui", "dashboard", "notification", "layout-engine"]),
            &[("gui", 1.2, 0.1), ("command-line-utilities", 0.9, 0.), ("hardware-support", 0.8, 0.), ("rendering::engine", 0.9, 0.),
            ("wasm", 0.9, 0.), ("asynchronous", 0.8, 0.), ("concurrency", 0.8, 0.), ("compression", 0.9, 0.), ("internationalization", 0.9, 0.)]),
        (Cond::Any(&["displaying", "desktop", "compositor"]),
            &[("gui", 1.2, 0.1), ("command-line-utilities", 0.9, 0.), ("hardware-support", 0.9, 0.), ("wasm", 0.8, 0.), ("asynchronous", 0.8, 0.), ("internationalization", 0.9, 0.)]),
        (Cond::Any(&["dashboard", "displaying", "inspector", "instrumentation"]), &[("visualization", 1.2, 0.1), ("games", 0.5, 0.)]),
        (Cond::All(&["user", "interface"]), &[("gui", 1.1, 0.), ("rendering::graphics-api", 0.8, 0.)]),
        (Cond::Any(&["toolkit", "imgui", "egui", "clickable"]), &[("gui", 1.1, 0.), ("config", 0.9, 0.), ("caching", 0.9, 0.)]),
        (Cond::Any(&["webview"]), &[("gui", 1.1, 0.1), ("hardware-support", 0.8, 0.)]),
        (Cond::Any(&["dep:miniquad", "dep:imgui", "dep:egui"]), &[("gui", 1.1, 0.), ("games", 1.1, 0.05), ("rendering::graphics-api", 0.8, 0.), ("compression", 0.9, 0.), ("wasm", 0.7, 0.),
            ("game-development", 1.1, 0.05), ("rust-patterns", 0.9, 0.), ("config", 0.9, 0.), ("caching", 0.9, 0.), ("hardware-support", 0.9, 0.)]),
        (Cond::Any(&["dep:winit", "dep:fltk", "dep:taffy", "dep:wayland-protocols", "dep:relm4", "dep:wry", "dep:gdk-pixbuf", "dep:druid", "dep:azul", "dep:conrod_winit",
            "dep:tauri", "dep:tauri-runtime", "dep:gtk4", "dep:gtk", "dep:iced", "dep:slint"]),
            &[("gui", 1.2, 0.1), ("hardware-support", 0.8, 0.), ("wasm", 0.8, 0.), ("os::windows-apis", 0.9, 0.), ("development-tools::cargo-plugins", 0.6, 0.), ("caching", 0.9, 0.), ("rendering::graphics-api", 0.7, 0.), ("development-tools::testing", 0.8, 0.)]),

        (Cond::Any(&["scotland", "scottish", "japan", "thai", "american", "uk", "country",  "country-code", "country-codes", "language-code"]),
            &[("internationalization", 1.2, 0.2), ("os::macos-apis", 0.8, 0.), ("command-line-utilities", 0.75, 0.), ("caching", 0.9, 0.),
            ("development-tools::build-utils", 0.8, 0.), ("rendering::engine", 0.1, 0.), ("parsing", 0.8, 0.), ("rendering::data-formats", 0.2, 0.), ("filesystem", 0.2, 0.)]),
        (Cond::Any(&["japanese", "icelandic", "arabic", "swedish", "korean", "hangul", "pinyin", "hanzi", "locale", "chinese", "chinese-numbers"]),
            &[("internationalization", 1.2, 0.2), ("os::macos-apis", 0.7, 0.), ("command-line-utilities", 0.75, 0.), ("parsing", 0.9, 0.),
            ("rendering::engine", 0.1, 0.), ("rendering::data-formats", 0.2, 0.), ("multimedia", 0.8, 0.), ("filesystem", 0.2, 0.)]),
        (Cond::Any(&["l10n", "localization", "localisation"]), &[("internationalization", 1.3, 0.2), ("parsing", 0.5, 0.)]),
        (Cond::Any(&["make", "cmd"]), &[("internationalization", 0.4, 0.)]),

        (Cond::Any(&["time", "date", "datetime", "gregorian-calendar", "datepicker", "julian-day", "date-time", "chrono", "timezone", "calendar", "tz", "time-handling", "dow", "sunrise", "time-ago", "hour-ago"]),
            &[("date-and-time", 1.35, 0.2), ("value-formatting", 1.1, 0.), ("finance", 0.9, 0.), ("no-std", 0.4, 0.), ("os", 0.8, 0.), ("command-line-interface", 0.7, 0.),
            ("parsing", 0.7, 0.), ("science", 0.7, 0.), ("science::math", 0.8, 0.), ("science::bio", 0.7, 0.), ("no-std", 0.95, 0.), ("games", 0.1, 0.), ("cryptography::cryptocurrencies", 0.8, 0.)]),
        (Cond::Any(&["week", "solar", "time-zone", "sunset", "moon", "tzdata", "year", "timeago", "stopwatch", "chrono"]),
            &[("date-and-time", 1.3, 0.18), ("value-formatting", 1.1, 0.), ("no-std", 0.6, 0.), ("os", 0.9, 0.), ("os::windows-apis", 0.9, 0.), ("command-line-interface", 0.7, 0.), ("parsing", 0.7, 0.), ("science", 0.7, 0.), ("no-std", 0.95, 0.), ("games", 0.1, 0.), ("cryptography::cryptocurrencies", 0.8, 0.)]),
        (Cond::All(&["constant","time"]), &[("date-and-time", 0.5, 0.), ("value-formatting", 0.9, 0.)]),
        (Cond::Any(&["constant-time"]), &[("date-and-time", 0.5, 0.), ("value-formatting", 0.9, 0.)]),
        (Cond::All(&["linear","time"]), &[("date-and-time", 0.7, 0.)]),
        (Cond::All(&["compile","time"]), &[("date-and-time", 0.4, 0.)]),
        (Cond::Any(&["menu", "tui", "subtitles", "srt", "nonce", "cortex-m", "qemu", "navigation", "timecode", "time-code",
            "finder", "search", "fedora", "vim", "cursor", "color"]), &[("date-and-time", 0.7, 0.), ("encoding", 0.9, 0.)]),
        (Cond::Any(&["uuid", "simulation", "failure", "fail", "iter", "domain", "engine", "kernel"]),
            &[("date-and-time", 0.4, 0.), ("value-formatting", 0.7, 0.), ("development-tools::build-utils", 0.8, 0.), ("rendering::graphics-api", 0.9, 0.)]),
        (Cond::Any(&["nan", "profile", "float", "timecode", "tsc", "fps", "arrow", "compiler"]),
            &[("date-and-time", 0.4, 0.), ("network-programming", 0.8, 0.), ("os::windows-apis", 0.9, 0.), ("development-tools::debugging", 0.8, 0.)]),

        (Cond::Any(&["nntp"]), &[("date-and-time", 1.1, 0.06), ("network-programming", 1.05, 0.), ("database", 0.7, 0.), ("caching", 0.9, 0.)]),
        (Cond::Any(&["layout"]), &[("gui", 1.1, 0.06), ("rendering::graphics-api", 1.05, 0.), ("database", 0.7, 0.)]),

        (Cond::NotAny(&["has:cargo-bin", "subcommand", "cargo-subcommand", "sub-command", "cargo-plugin", "cargo", "crate"]),
            &[("development-tools::cargo-plugins", 0.6, 0.)]),
        (Cond::Any(&["cargo-subcommand"]), &[
            ("development-tools::cargo-plugins", 1.8, 0.4), ("development-tools", 0.3, 0.), ("algorithms", 0.8, 0.),
            ("cryptography::cryptocurrencies", 0.6, 0.), ("parsing", 0.5, 0.), ("development-tools::build-utils", 0.8, 0.)]),
        (Cond::Any(&["has:cargo-bin"]), &[
            ("development-tools::cargo-plugins", 1.2, 0.2), ("parsing", 0.5, 0.), ("development-tools", 0.8, 0.), ("development-tools::build-utils", 0.7, 0.),
            ("development-tools::procedural-macro-helpers", 0.8, 0.), ("memory-management", 0.6, 0.)]),
        (Cond::All(&["has:cargo-bin", "has:is_dev"]), &[("development-tools::cargo-plugins", 1.2, 0.1)]),
        (Cond::All(&["cargo", "subcommand"]), &[("development-tools::cargo-plugins", 1.8, 0.4), ("development-tools", 0.7, 0.), ("development-tools::build-utils", 0.8, 0.),
            ("development-tools::testing", 0.8, 0.), ("parsing", 0.9, 0.)]),
        (Cond::All(&["cargo", "debian"]), &[("development-tools::cargo-plugins", 1.3, 0.2), ("os::unix-apis", 1.3, 0.1)]),

        (Cond::NotAny(&["build", "build-script", "build-scripts", "vcs", "task", "manifest", "embed", "ccache", "linking", "linkage",
            "metabuild", "generating", "runner", "ninja", "cargo-metadata", "changelog", "package", "resource", "compile-time", "build-tool", "generator", "packaging",
            "codegen", "code-generation", "build-system", "build-dependencies", "cmake", "make", "makefile"]),
            &[("development-tools::build-utils", 0.66, 0.)]),
        (Cond::Any(&["build-script", "ccache", "metabuild", "ninja", "build-tool", "build-system", "build-dependencies", "cmake", "makefile", "task-runner"]),
            &[("development-tools::build-utils", 1.2, 0.1)]),
        (Cond::Any(&["code-generation", "code-generator", "codegen"]),
            &[("development-tools::build-utils", 1.2, 0.), ("development-tools::ffi", 1.2, 0.), ("development-tools", 1.1, 0.1), ("caching", 0.9, 0.), ("database", 0.9, 0.), ("compilers", 1.2, 0.1)]),
        (Cond::Any(&["interact", "interactive"]), &[("development-tools::build-utils", 0.8, 0.), ("caching", 0.9, 0.)]),

        (Cond::All(&["cargo", "sub-command"]), &[("development-tools::cargo-plugins", 1.8, 0.4), ("development-tools", 0.7, 0.), ("development-tools::build-utils", 0.8, 0.)]),
        (Cond::Any(&["cargo"]), &[("development-tools::cargo-plugins", 1.2, 0.1), ("multimedia", 0.9, 0.), ("development-tools::build-utils", 1.1, 0.1)]),
        (Cond::Any(&["build-dependencies"]), &[("config", 0.5, 0.), ("development-tools::build-utils", 1.3, 0.15)]),
        (Cond::All(&["development", "helper"]), &[("development-tools", 1.1, 0.), ("development-tools::build-utils", 1.1, 0.)]),
        (Cond::All(&["build", "helper"]), &[("development-tools::build-utils", 1.1, 0.1)]),
        (Cond::Any(&["build-time", "libtool"]), &[("development-tools::build-utils", 1.2, 0.2), ("config", 0.9, 0.),("development-tools::cargo-plugins", 1.1, 0.)]),
        (Cond::All(&["build", "scripts"]), &[("development-tools::build-utils", 1.2, 0.2)]),
        (Cond::All(&["build", "script"]), &[("development-tools::build-utils", 1.2, 0.2)]),
        (Cond::Any(&["api", "upload", "nlp", "zeroed", "issue", "ttf"]), &[("development-tools::build-utils", 0.8, 0.)]),

        (Cond::Any(&["oauth", "auth", "authentication", "authorization", "authorisation", "credentials"]),
            &[("authentication", 1.4, 0.2), ("command-line-utilities", 0.6, 0.), ("hardware-support", 0.7, 0.), ("caching", 0.9, 0.), ("no-std", 0.9, 0.),
            ("accessibility", 0.7, 0.), ("config", 0.9, 0.), ("compilers", 0.8, 0.), ("web-programming::http-client", 0.8, 0.), ("parsing", 0.7, 0.),
            ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::Any(&["diceware", "totp", "credentials", "authenticator"]),
            &[("authentication", 1.4, 0.2), ("hardware-support", 0.8, 0.), ("parsing", 0.7, 0.), ("config", 0.9, 0.), ("algorithms", 0.7, 0.)]),
        (Cond::Any(&["gpg", "pgp", "openssh", "webauthn", "hotp"]),
            &[("authentication", 1.2, 0.1), ("cryptography", 1.2, 0.1), ("compilers", 0.8, 0.), ("parsing", 0.5, 0.), ("algorithms", 0.8, 0.)]),
        (Cond::Any(&["fido", "fido2", "u2f", "opensk"]),
            &[("authentication", 1.4, 0.2), ("hardware-support", 1.2, 0.1), ("compilers", 0.8, 0.), ("parsing", 0.5, 0.), ("command-line-utilities", 0.9, 0.),
            ("config", 0.8, 0.), ("web-programming::http-client", 0.8, 0.), ("algorithms", 0.8, 0.), ("parsing", 0.7, 0.)]),
        (Cond::Any(&["authorize", "authenticate", "oauth2", "credential", "ssk-keys", "passphrase"]),
            &[("authentication", 1.4, 0.2), ("command-line-utilities", 0.9, 0.), ("compilers", 0.8, 0.), ("hardware-support", 0.8, 0.), ("config", 0.8, 0.), ("web-programming::http-client", 0.8, 0.),
            ("parsing", 0.7, 0.), ("algorithms", 0.9, 0.)]),
        (Cond::Any(&["secret", "secrets", "vaults", "secret-handshake", "secrets-manager"]),
            &[("authentication", 1.2, 0.), ("cryptography", 1.2, 0.), ("algorithms", 0.9, 0.)]),
        (Cond::Any(&["openid", "keychain", "libsecret", "credential-vault", "openid-connect", "hashicorp", "hashicorp-vault", "kerberos", "ldap-client", "krb5", "gssapi", "vault", "bitwarden", "linux-pam", "libpam"]),
            &[("authentication", 1.2, 0.2), ("cryptography", 1.1, 0.), ("algorithms", 0.9, 0.), ("parsing", 0.5, 0.), ("compilers", 0.8, 0.), ("compression", 0.5, 0.)]),
        (Cond::Any(&["2fa", "session", "askpass", "fido2", "saml", "okta", "ldap", "ldap3", "active-directory", "ldap3", "sspi", "authorized-keys", "identity-management", "acl", "rbac", "login", "pam", "yubico", "yubikey",
            "access-control", "authorization-framework", "handshake", "multifactor", "basic-authentication"]),
            &[("authentication", 1.1, 0.05), ("parsing", 0.5, 0.), ("compression", 0.9, 0.), ("parser-implementations", 0.9, 0.)]),

        (Cond::NotAny(&["database", "db", "databases", "datastore", "persistence", "wal", "diesel", "clickhouse", "sqlx", "queryable", "indexed", "columnar", "persistent", "relational", "search",
            "dbms", "migrations", "dataframe", "key-value", "kv", "kvs", "sql", "sqlx", "nosql", "geoip", "key-value", "orm", "schema", "lmdb", "odbc", "transactions", "transactional",
            "sqlite3", "database-wrapper", "leveldb", "postgres", "embedded-database", "wal", "acid", "relational-database", "postgresql", "dynamodb", "mysql", "mssql", "sqlx", "oracle", "hadoop", "sqlite", "mongo",
             "mongodb", "mongo-db", "memcached", "lucene", "elasticsearch", "tkiv", "cassandra", "rocksdb", "query"]),
            &[("database-implementations", 0.8, 0.), ("database", 0.8, 0.)]),
        (Cond::Any(&["database", "dataframe", "databases", "datastore", "write-ahead-log"]), &[("database-implementations", 1.3, 0.3), ("no-std", 0.9, 0.),
            ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.), ("config", 0.9, 0.), ("development-tools::cargo-plugins", 0.5, 0.), ("cryptography::cryptocurrencies", 0.9, 0.),
            ("multimedia", 0.8, 0.),("database", 1.3, 0.1), ("caching", 0.8, 0.), ("multimedia::audio", 0.8, 0.), ("development-tools", 0.9, 0.)]),
        (Cond::All(&["personal", "information", "management"]), &[("database-implementations", 1.5, 0.3)]),
        (Cond::Any(&["sql", "query-language"]), &[("database", 1.3, 0.1), ("database-implementations", 1.1, 0.1), ("accessibility", 0.8, 0.), ("caching", 0.9, 0.)]),
        (Cond::Any(&["pdb", "symbolication", "microsoft-pdb"]), &[("database", 0.5, 0.), ("database-implementations", 0.5, 0.), ("caching", 0.8, 0.), ("development-tools::debugging", 1.2, 0.1), ("parser-implementations", 1.1, 0.)]),
        (Cond::All(&["microsoft", "pdb"]), &[("database", 0.5, 0.), ("database-implementations", 0.5, 0.), ("development-tools::debugging", 1.2, 0.1),
            ("development-tools::profiling", 1.1, 0.), ("parsing", 0.5, 0.), ("parser-implementations", 1.1, 0.)]),
        (Cond::All(&["bioinformatics", "pdb"]), &[("development-tools::debugging", 0.2, 0.), ("development-tools::profiling", 0.5, 0.)]),
        (Cond::All(&["rpm", "redis"]), &[("databases", 1.25, 0.1), ("os", 0.5, 0.), ("database-implementations", 0.8, 0.), ("caching", 0.9, 0.), ("parsing", 0.5, 0.), ("os::unix-apis", 0.7, 0.)]),
        (Cond::Any(&["redis", "elasticsearch", "elastic-search", "clickhouse", "sqlx", "sparql", "graph-database"]),
            &[("database", 1.25, 0.15), ("os", 0.5, 0.), ("web-programming", 0.9, 0.), ("parsing", 0.5, 0.), ("os::unix-apis", 0.7, 0.)]),
        (Cond::Any(&["nosql", "geoip", "wal", "schema"]), &[
            ("database", 1.5, 0.3), ("database-implementations", 1.2, 0.1), ("data-structures", 1.2, 0.1),
            ("command-line-utilities", 0.5, 0.), ("rendering::engine", 0.6, 0.), ("development-tools::build-utils", 0.8, 0.), ("science::geo", 0.8, 0.), ("embedded", 0.9, 0.)]),
        (Cond::Any(&["tkiv", "transactions", "transactional"]), &[("database", 1.5, 0.3),("database-implementations", 1.2, 0.1), ("data-structures", 1.2, 0.1), ("command-line-utilities", 0.5, 0.)]),
        (Cond::Any(&["kv", "kv-store", "key-value", "key-value-store", "upsert", "document-database", "graph-database"]), &[("database", 1.1, 0.1),("database-implementations", 1.1, 0.05)]),
        (Cond::Any(&["oracle", "database-wrapper", "database-client", "lmdb"]), &[("database", 1.2, 0.1), ("database-implementations", 0.8, 0.)]),
        (Cond::Any(&["write-ahead-log", "embedded-database"]), &[("database", 1.1, 0.), ("database-implementations", 1.3, 0.2)]),
        (Cond::Any(&["acid-compliant", "acid-transactional", "sanakirja"]), &[("database", 1.1, 0.), ("database-implementations", 1.25, 0.1)]),
        (Cond::All(&["oracle", "odpi"]), &[("database", 1.3, 0.2), ("database-implementations", 0.5, 0.)]),
        (Cond::Any(&["firebase", "mvcc", "savepoints", "rollbacks"]), &[("database", 1.1, 0.05), ("web-programming", 1.1, 0.05), ("gui", 0.7, 0.)]),
        (Cond::Any(&["sqlite", "hadoop", "rusqlite", "dep:rusqlite"]), &[("web-programming", 0.7, 0.), ("database-implementations", 0.8, 0.), ("parsing", 0.5, 0.), ("web-programming::http-client", 0.8, 0.)]),
        (Cond::Any(&["database", "db", "sqlite", "sqlite3", "influxdb", "leveldb", "diesel", "postgres", "postgresql", "mysql", "dynamodb", "hadoop"]),
                &[("database", 1.4, 0.2), ("cryptography::cryptocurrencies", 0.5, 0.), ("cryptography", 0.7, 0.), ("email", 0.7, 0.), ("text-processing", 0.7, 0.), ("rust-patterns", 0.7, 0.),
                ("database-implementations", 1.1, 0.), ("value-formatting", 0.7, 0.), ("visualization", 0.8, 0.), ("os::macos-apis", 0.5, 0.), ("internationalization", 0.7, 0.), ("hardware-support", 0.6, 0.),
                ("web-programming", 0.9, 0.), ("network-programming", 0.9, 0.), ("algorithms", 0.9, 0.), ("data-structures", 0.9, 0.), ("web-programming::http-server", 0.8, 0.), ("command-line-interface", 0.5, 0.),
                ("multimedia::video", 0.5, 0.), ("multimedia::audio", 0.5, 0.), ("command-line-utilities", 0.9, 0.), ("memory-management", 0.7, 0.), ("development-tools", 0.9, 0.)]),
        (Cond::Any(&["orm", "mongo", "mongodb", "dynamo-db", "mongo-db", "lucene", "elasticsearch", "memcached", "mariadb", "cassandra", "rocksdb", "redis", "couchdb"]),
                &[("database", 1.4, 0.2), ("database-implementations", 0.85, 0.), ("cryptography::cryptocurrencies", 0.5, 0.), ("cryptography", 0.7, 0.),
                ("text-processing", 0.7, 0.), ("rust-patterns", 0.7, 0.), ("value-formatting", 0.7, 0.), ("os::macos-apis", 0.5, 0.),
                ("internationalization", 0.7, 0.), ("hardware-support", 0.6, 0.), ("web-programming", 1.1, 0.), ("algorithms", 0.9, 0.),
                ("data-structures", 0.9, 0.), ("command-line-interface", 0.5, 0.), ("multimedia::video", 0.5, 0.), ("multimedia::audio", 0.6, 0.), ("command-line-utilities", 0.9, 0.),
                ("memory-management", 0.7, 0.), ("development-tools::testing", 0.7, 0.), ("visualization", 0.9, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::Any(&["distributed", "zookeeper"]), &[("text-processing", 0.4, 0.), ("network-programming", 1.1, 0.), ("encoding", 0.8, 0.), ("command-line-interface", 0.4, 0.)]),
        (Cond::All(&["kv", "distributed"]), &[("database", 1.3, 0.2), ("network-programming", 0.8, 0.), ("email", 0.7, 0.), ("database-implementations", 1.2, 0.1)]),
        (Cond::Any(&["csv", "driver"]), &[("database-implementations", 0.8, 0.)]),
        (Cond::Any(&["structured-query", "query-based", "query-language", "query-engine"]),
            &[("database-implementations", 1.1, 0.05), ("database", 1.1, 0.05), ("email", 0.9, 0.), ("algorithms", 1.1, 0.05)]),
        (Cond::Any(&["validator"]), &[("database", 0.9, 0.)]),
        (Cond::Any(&["distributed"]), &[("asynchronous", 1.1, 0.), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::Any(&["persistence", "persistent", "lsm-tree"]), &[("database", 1.1, 0.)]),
        (Cond::Any(&["search", "lsm-tree", "fuzzy-search", "information-retrieval", "dataframe", "query-engine", "polars"]),
            &[("database", 1.2, 0.), ("algorithms", 1.18, 0.), ("config", 0.8, 0.), ("email", 0.9, 0.),  ("parsing", 0.8, 0.), ("parser-implementations", 0.9, 0.),
            ("database-implementations", 1.2, 0.), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::All(&["database", "embedded"]), &[("database", 1.2, 0.1), ("embedded", 0.2, 0.), ("parsing", 0.2, 0.), ("database-implementations", 1.2, 0.1), ("multimedia::audio", 0.8, 0.)]),
        (Cond::All(&["database", "has:is_sys"]), &[("database", 1.3, 0.1), ("database-implementations", 0.5, 0.), ("parsing", 0.2, 0.)]),
        (Cond::All(&["elastic", "search"]), &[("database", 1.2, 0.), ("algorithms", 0.2, 0.), ("parsing", 0.2, 0.), ("email", 0.9, 0.), ("database-implementations", 0.2, 0.)]),
        (Cond::All(&["search", "engine"]), &[("database-implementations", 1.2, 0.2), ("algorithms", 0.8, 0.), ("caching", 0.9, 0.), ("data-structures", 0.6, 0.)]),
        (Cond::All(&["information", "retrieval"]), &[("database-implementations", 1.1, 0.1), ("algorithms", 0.9, 0.), ("data-structures", 0.9, 0.)]),
        (Cond::Any(&["search-engine"]), &[("database-implementations", 1.2, 0.2), ("algorithms", 0.8, 0.), ("data-structures", 0.6, 0.)]),
        (Cond::All(&["key", "value", "store"]), &[("database", 1.2, 0.2), ("database-implementations", 1.2, 0.2), ("multimedia::video", 0.5, 0.)]),
        (Cond::Any(&["rabbitmq", "zeromq", "amqp", "mqtt"]),
            &[("network-programming", 1.2, 0.1), ("os::windows-apis", 0.7, 0.), ("algorithms", 0.5, 0.), ("email", 0.7, 0.), ("rust-patterns", 0.75, 0.),
            ("web-programming", 1.2, 0.1), ("multimedia::video", 0.5, 0.), ("multimedia", 0.5, 0.), ("parsing", 0.2, 0.), ("compression", 0.75, 0.), ("asynchronous", 1.2, 0.05)]),
        (Cond::Any(&["messaging", "client-server"]), &[("network-programming", 1.2, 0.), ("web-programming", 1.1, 0.), ("asynchronous", 1.1, 0.)]),

        (Cond::All(&["aws", "rusoto"]), &[("network-programming", 1.2, 0.1), ("finance", 0.5, 0.), ("gui", 0.9, 0.), ("multimedia::video", 0.8, 0.), ("data-structures", 0.6, 0.),
            ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.), ("development-tools::cargo-plugins", 0.5, 0.), ("algorithms", 0.2, 0.), ("parsing", 0.5, 0.),
            ("filesystem", 0.6, 0.), ("web-programming", 1.2, 0.1)]),
        (Cond::Any(&["cloudformation", "google-cloud", "cloudwatch", "nextcloud"]),
            &[("network-programming", 1.2, 0.1), ("multimedia::video", 0.8, 0.), ("data-structures", 0.6, 0.), ("algorithms", 0.2, 0.), ("parsing", 0.5, 0.), ("filesystem", 0.6, 0.),
            ("web-programming", 1.2, 0.1), ("development-tools::testing", 0.7, 0.)]),
        (Cond::All(&["aws", "sdk"]), &[("network-programming", 1.2, 0.2), ("finance", 0.5, 0.), ("web-programming", 1.2, 0.1), ("data-structures", 0.6, 0.),
            ("development-tools::build-utils", 0.8, 0.), ("algorithms", 0.8, 0.), ("filesystem", 0.5, 0.)]),
        (Cond::All(&["aws", "lambda"]), &[("network-programming", 1.2, 0.2), ("web-programming", 1.2, 0.1), ("gui", 0.9, 0.), ("data-structures", 0.6, 0.), ("algorithms", 0.8, 0.), ("filesystem", 0.5, 0.)]),
        (Cond::All(&["cloud", "google"]), &[("network-programming", 1.1, 0.), ("web-programming", 1.25, 0.25), ("parsing", 0.2, 0.),
            ("multimedia::audio", 0.75, 0.), ("gui", 0.9, 0.), ("algorithms", 0.8, 0.), ("data-structures", 0.6, 0.), ("no-std", 0.7, 0.), ("development-tools::build-utils", 0.6, 0.)]),
        (Cond::All(&["api", "client"]), &[("network-programming", 1.1, 0.05), ("web-programming", 1.1, 0.05), ("algorithms", 0.8, 0.), ("science::math", 0.8, 0.), ("config", 0.8, 0.), ("value-formatting", 0.8, 0.), ("data-structures", 0.6, 0.), ("development-tools::cargo-plugins", 0.7, 0.)]),
        (Cond::All(&["api", "dep:reqwest"]), &[("web-programming", 1.1, 0.1), ("algorithms", 0.8, 0.), ("command-line-interface", 0.8, 0.), ("development-tools::procedural-macro-helpers", 0.5, 0.), ("data-structures", 0.8, 0.)]),
        (Cond::All(&["api-bindings", "dep:reqwest"]), &[("web-programming", 1.1, 0.1), ("algorithms", 0.8, 0.), ("email", 0.9, 0.), ("parsing", 0.5, 0.), ("config", 0.9, 0.), ("command-line-interface", 0.8, 0.), ("data-structures", 0.6, 0.)]),
        (Cond::All(&["api", "dep:url"]), &[("web-programming", 1.1, 0.1)]),
        (Cond::All(&["client", "library"]), &[("network-programming", 1.1, 0.05), ("web-programming", 1.1, 0.05), ("algorithms", 0.8, 0.), ("value-formatting", 0.8, 0.)]),
        (Cond::Any(&["dep:reqwest"]), &[("network-programming", 1.1, 0.0), ("web-programming", 1.1, 0.0), ("algorithms", 0.8, 0.),
            ("data-structures", 0.9, 0.), ("parsing", 0.2, 0.), ("memory-management", 0.6, 0.)]),
        (Cond::All(&["service", "google"]), &[("network-programming", 1.1, 0.), ("algorithms", 0.7, 0.), ("web-programming", 1.22, 0.2), ("data-structures", 0.6, 0.)]),
        (Cond::Any(&["service", "daemon", "server"]), &[("algorithms", 0.7, 0.), ("data-structures", 0.7, 0.), ("date-and-time", 0.9, 0.), ("science::math", 0.7, 0.)]),
        (Cond::Any(&["dep:rusoto_core", "amazon-web-services"]), &[("network-programming", 1.1, 0.1), ("database-implementations", 0.5, 0.), ("web-programming", 1.1, 0.1)]),
        (Cond::Any(&["rusoto", "azure", "cloudflare", "amazon", "google-apis", "aws-lambda", "aws-lambda-functions"]),
            &[("network-programming", 1.3, 0.3), ("database-implementations", 0.9, 0.), ("web-programming", 1.2, 0.1), ("algorithms", 0.8, 0.), ("embedded", 0.5, 0.), ("hardware-support", 0.75, 0.),
            ("no-std", 0.7, 0.), ("multimedia::video", 0.9, 0.), ("cryptography::cryptocurrencies", 0.6, 0.)]),

        (Cond::Any(&["compress", "compression", "rar", "archive", "archives", "zip", "gzip", "arithmetic-coding", "unrar", "lzfse", "compression-algorithm"]),
            &[("compression", 1.3, 0.3), ("cryptography", 0.7, 0.), ("encoding", 0.7, 0.), ("finance", 0.8, 0.), ("config", 0.9, 0.), ("games", 0.4, 0.), ("parsing", 0.4, 0.), ("no-std", 0.7, 0.),
            ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.), ("development-tools::cargo-plugins", 0.8, 0.), ("asynchronous", 0.9, 0.),
            ("command-line-interface", 0.4, 0.), ("command-line-utilities", 0.8, 0.), ("development-tools::testing", 0.6, 0.),
            ("development-tools::profiling", 0.2, 0.)]),
        (Cond::Any(&["zlib", "libz", "7z", "lz4", "brotli", "huffman", "hpack", "xz", "r-ans", "ans", "lzma", "decompress", "deflate"]),
            &[("compression", 1.3, 0.3), ("cryptography", 0.6, 0.), ("encoding", 0.9, 0.), ("caching", 0.9, 0.), ("parsing", 0.6, 0.), ("games", 0.4, 0.), ("command-line-interface", 0.4, 0.),
            ("command-line-utilities", 0.8, 0.),  ("development-tools::testing", 0.6, 0.), ("development-tools::profiling", 0.2, 0.)]),

        (Cond::Any(&["dep:flate2"]), &[("rust-patterns", 0.7, 0.)]),
        (Cond::Any(&["dep:sdl2", "graphics"]),
            &[("compression", 0.8, 0.), ("games", 1.1, 0.), ("parsing", 0.5, 0.), ("config", 0.8, 0.), ("email", 0.7, 0.), ("no-std", 0.8, 0.), ("development-tools::build-utils", 0.8, 0.)]),

        (Cond::NotAny(&["simulation", "simulator", "vm", "virtual", "dynamics", "nets", "sim", "particle", "city", "fluid", "systems", "real-time", "physics", "automata", "quantum", "traffic-simulation"]),
            &[("simulation", 0.8, 0.)]),

        (Cond::Any(&["simulation", "simulator"]), &[("simulation", 1.3, 0.3), ("emulators", 1.15, 0.1), ("parser-implementations", 0.8, 0.), ("caching", 0.9, 0.), ("development-tools::build-utils", 0.8, 0.), ("multimedia", 0.8, 0.)]),
        (Cond::Any(&["traffic-simulation"]), &[("simulation", 1.3, 0.2)]),
        (Cond::Any(&["real-time", "realtime"]), &[("simulation", 1.1, 0.0), ("parser-implementations", 0.6, 0.), ("development-tools::build-utils", 0.8, 0.)]),
        (Cond::Any(&["sim", "systems-biology"]), &[("simulation", 1.1, 0.1)]),
        (Cond::All(&["software", "implementation"]), &[("simulation", 1.3, 0.), ("emulators", 1.2, 0.), ("development-tools", 0.8, 0.)]),
        (Cond::All(&["skeletal", "animation"]), &[("game-development", 1.3, 0.1), ("email", 0.4, 0.)]),
        (Cond::All(&["character", "animation"]), &[("game-development", 1.1, 0.05)]),
        (Cond::Any(&["animation", "animated", "anim"]), &[("multimedia", 1.2, 0.), ("finance", 0.5, 0.), ("game-development", 1.1, 0.), ("multimedia::video", 1.2, 0.1), ("asynchronous", 0.9, 0.), ("rendering", 1.1, 0.), ("simulation", 0.7, 0.), ("text-processing", 0.5, 0.)]),
        (Cond::Any(&["gstreamer"]), &[("multimedia", 1.2, 0.), ("multimedia::video", 1.3, 0.15), ("caching", 0.9, 0.), ("finance", 0.5, 0.)]),

        (Cond::Any(&["rsync", "dns-server", "scp", "xmpp", "port-scanner", "nmap", "ldap", "openssh", "ssh", "socks5", "elb", "iptables", "kademlia", "bittorrent", "sctp", "docker", "wireguard", "tun-tap", "vpn", "openvpn"]),
            &[("network-programming", 1.2, 0.2), ("web-programming", 0.6, 0.), ("parsing", 0.6, 0.), ("caching", 0.9, 0.), ("hardware-support", 0.6, 0.), ("config", 0.9, 0.), ("development-tools::testing", 0.5, 0.),
            ("algorithms", 0.9, 0.), ("asynchronous", 0.9, 0.), ("os::windows-apis", 0.6, 0.)]),
        (Cond::Any(&["bot", "netsec", "waf", "curl", "net", "notification", "chat"]),
            &[("network-programming", 1.1, 0.1), ("web-programming", 1.1, 0.1), ("parsing", 0.8, 0.), ("data-structures", 0.8, 0.),
            ("development-tools::procedural-macro-helpers", 0.7, 0.), ("rendering::graphics-api", 0.9, 0.), ("development-tools::testing", 0.7, 0.),
            ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::Any(&["rpki", "bgp", "lorawan", "lpwan", "chirpstack"]), &[("network-programming", 1.2, 0.15), ("gui", 0.9, 0.), ("email", 0.8, 0.), ("config", 0.9, 0.)]),
        (Cond::Any(&["rpki", "noise-protocol", "matrix-org"]), &[("network-programming", 1.1, 0.1), ("cryptography", 1.1, 0.1), ("science::math", 0.8, 0.)]),
        (Cond::Any(&["ruma", "messaging", "chat", "matrix-bot", "matrix-org", "zettelkasten"]), &[("science::math", 0.5, 0.)]),
        (Cond::Any(&["ip", "ipv6", "ipv4", "network", "internet"]), &[("network-programming", 1.2, 0.1), ("web-programming", 1.1, 0.), ("parsing", 0.8, 0.)]),
        (Cond::Any(&["nat", "stun", "portmapping"]), &[("network-programming", 1.1, 0.05)]),
        (Cond::Any(&["proxy", "networking", "cidr"]), &[("network-programming", 1.2, 0.1), ("web-programming", 1.1, 0.), ("parser-implementations", 0.9, 0.), ("parsing", 0.5, 0.), ("algorithms", 0.8, 0.), ("data-structures", 0.9, 0.)]),
        (Cond::Any(&["http2", "http-2", "http", "https", "httpd", "tcp", "icmp", "irc", "zigbee2mqtt", "tcp-client", "multicast", "anycast", "bgp", "amazon", "aws", "amazon-s3", "cloud", "service"]),
            &[("network-programming", 1.1, 0.1), ("filesystem", 0.7, 0.), ("science", 0.9, 0.), ("database", 0.9, 0.), ("compression", 0.9, 0.), ("memory-management", 0.5, 0.), ("asynchronous", 0.8, 0.), ("algorithms", 0.8, 0.),
            ("data-structures", 0.8, 0.), ("text-processing", 0.7, 0.), ("encoding", 0.9, 0.), ("command-line-interface", 0.5, 0.), ("development-tools::procedural-macro-helpers", 0.8, 0.), ("development-tools::build-utils", 0.6, 0.)]),
        (Cond::Any(&["ipfs", "ceph"]), &[("network-programming", 1.2, 0.1), ("filesystem", 1.3, 0.1), ("cryptography", 0.8, 0.), ("database-implementations", 0.8, 0.), ("database", 0.8, 0.), ("text-processing", 0.7, 0.), ("command-line-interface", 0.5, 0.)]),
        (Cond::Any(&["irc", "dht", "bot", "kademlia", "icmp"]), &[("network-programming", 1.2, 0.1), ("database", 0.9, 0.), ("caching", 0.9, 0.), ("parsing", 0.9, 0.), ("asynchronous", 0.8, 0.)]),
        (Cond::Any(&["dht", "kademlia"]), &[("data-structures", 1.2, 0.1)]),
        (Cond::Any(&["pipe", "read", "write", "mtime", "atime"]), &[("filesystem", 1.1, 0.), ("development-tools::profiling", 0.6, 0.), ("science", 0.8, 0.)]),

        (Cond::Any(&["pointers", "pointer", "slices", "primitive", "primitives", "clone-on-write", "dependency-injection"]),
                &[("rust-patterns", 1.2, 0.1), ("command-line-utilities", 0.7, 0.), ("command-line-interface", 0.8, 0.), ("finance", 0.5, 0.), ("no-std", 0.95, 0.), ("asynchronous", 0.8, 0.),
                ("development-tools::testing", 0.7, 0.), ("internationalization", 0.7, 0.), ("template-engine", 0.8, 0.)]),
        (Cond::Any(&["references", "methods", "own", "function", "variables", "inference", "assert"]),
                &[("rust-patterns", 1.2, 0.1), ("no-std", 0.9, 0.), ("command-line-utilities", 0.7, 0.), ("command-line-interface", 0.8, 0.), ("no-std", 0.95, 0.), ("asynchronous", 0.8, 0.),
                ("development-tools::testing", 0.9, 0.), ("internationalization", 0.7, 0.), ("template-engine", 0.8, 0.)]),
        (Cond::Any(&["endianness", "derive", "float", "delegation", "floats", "floating-point", "downcasting", "initialized", "primitives", "tuple", "type-level", "panic", "literal"]),
                &[("rust-patterns", 1.2, 0.1), ("no-std", 0.8, 0.), ("science", 0.8, 0.), ("science", 0.8, 0.), ("science::ml", 0.7, 0.), ("science::math", 0.88, 0.), ("os", 0.9, 0.),
                ("command-line-utilities", 0.7, 0.), ("command-line-interface", 0.8, 0.), ("memory-management", 0.8, 0.), ("rendering", 0.8, 0.), ("rendering::graphics-api", 0.7, 0.), ("template-engine", 0.8, 0.),
                ("hardware-support", 0.5, 0.), ("development-tools::cargo-plugins", 0.3, 0.), ("development-tools::testing", 0.7, 0.)]),
        (Cond::Any(&["trait", "cow", "range", "annotation", "abstractions", "abstraction", "generics", "interning", "tailcall", "traits", "pin-api", "contravariant", "metaprogramming", "type-level", "unreachable", "oop", "type", "types", "scoped", "scope", "functions", "clone"]),
                &[("rust-patterns", 1.2, 0.1), ("no-std", 0.7, 0.), ("science", 0.8, 0.), ("authentication", 0.8, 0.), ("games", 0.8, 0.), ("science", 0.8, 0.), ("science::ml", 0.7, 0.), ("science::math", 0.88, 0.), ("os", 0.9, 0.),
                ("command-line-utilities", 0.7, 0.), ("command-line-interface", 0.8, 0.), ("memory-management", 0.8, 0.), ("rendering", 0.8, 0.), ("rendering::graphics-api", 0.7, 0.), ("template-engine", 0.8, 0.),
                ("hardware-support", 0.5, 0.), ("algorithms", 0.9, 0.), ("config", 0.7, 0.), ("development-tools", 0.6, 0.), ("development-tools::cargo-plugins", 0.3, 0.), ("development-tools::testing", 0.7, 0.)]),
        (Cond::Any(&["u128", "closure", "trait-object", "trait-objects", "visitor-pattern", "transmute", "unwrap", "fnonce", "cell", "object-safe", "byteorder", "printf", "nightly", "std",  "macro", "null", "standard-library"]),
                &[("rust-patterns", 1.2, 0.1), ("algorithms", 0.8, 0.), ("no-std", 0.8, 0.), ("science", 0.8, 0.), ("science::bio", 0.8, 0.), ("science::ml", 0.7, 0.), ("science::math", 0.88, 0.), ("os", 0.9, 0.),
                ("rendering::graphics-api", 0.9, 0.), ("command-line-utilities", 0.7, 0.), ("command-line-interface", 0.8, 0.), ("memory-management", 0.8, 0.),
                ("rendering", 0.8, 0.), ("hardware-support", 0.6, 0.), ("development-tools::cargo-plugins", 0.4, 0.), ("development-tools::testing", 0.8, 0.)]),
        (Cond::Any(&["enum", "enums", "boilerplate", "prelude", "boxing", "error", "error-handling", "error-logging", "println", "dsl"]),
                &[("rust-patterns", 1.2, 0.1), ("algorithms", 0.7, 0.), ("science", 0.7, 0.), ("science::ml", 0.7, 0.), ("science::math", 0.7, 0.), ("os", 0.8, 0.), ("command-line-utilities", 0.7, 0.), ("command-line-interface", 0.8, 0.), ("memory-management", 0.8, 0.), ("rendering", 0.8, 0.), ("hardware-support", 0.5, 0.), ("development-tools::cargo-plugins", 0.4, 0.), ("development-tools::ffi", 0.4, 0.), ("development-tools::testing", 0.7, 0.)]),
        (Cond::All(&["error", "handling"]), &[("rust-patterns", 1.2, 0.), ("no-std", 0.8, 0.), ("data-structures", 0.9, 0.), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::All(&["error", "type"]), &[("rust-patterns", 1.2, 0.)]),
        (Cond::Any(&["observable"]), &[("rust-patterns", 1.2, 0.)]),
        (Cond::Any(&["gpt", "chatgpt", "openai", "web-api"]), &[("rust-patterns", 0.7, 0.)]),
        (Cond::Any(&["dep:anyhow", "dep:dirs-next", "iota", "xz2", "flate2"]), &[("data-structures", 0.8, 0.), ("date-and-time", 0.8, 0.),]),
        (Cond::Any(&["error-handling", "higher-order", "syntax-sugar"]), &[("rust-patterns", 1.2, 0.1), ("data-structures", 0.8, 0.)]),
        (Cond::All(&["proc", "macro"]), &[("rust-patterns", 1.1, 0.1), ("no-std", 0.6, 0.), ("development-tools::procedural-macro-helpers", 1.2, 0.2), ("rendering::graphics-api", 0.8, 0.),]),
        (Cond::All(&["proc", "linux"]), &[("os::unix-apis", 1.2, 0.1), ("hardware-support", 0.8, 0.)]),
        (Cond::All(&["proc", "info", "linux"]), &[("os::unix-apis", 1.2, 0.1), ("hardware-support", 0.8, 0.)]),
        (Cond::All(&["rust", "macro"]), &[("rust-patterns", 1.1, 0.1), ("development-tools::procedural-macro-helpers", 1.1, 0.1)]),
        (Cond::Any(&["dep:pkg-config"]), &[("rust-patterns", 0.5, 0.), ("development-tools::procedural-macro-helpers", 0.5, 0.)]),
        (Cond::Any(&["dep:quote"]), &[("development-tools::procedural-macro-helpers", 1.3, 0.15), ("command-line-utilities", 0.3, 0.)]),
        (Cond::Any(&["dep:darling"]), &[("development-tools::procedural-macro-helpers", 1.2, 0.1)]),
        (Cond::Any(&["dep:proc-macro-hack"]), &[("development-tools::procedural-macro-helpers", 1.2, 0.1), ("development-tools::build-utils", 0.8, 0.)]),
        (Cond::Any(&["dep:syn"]), &[("development-tools::procedural-macro-helpers", 1.2, 0.15), ("data-structures", 0.7, 0.), ("gui", 0.9, 0.), ("command-line-utilities", 0.3, 0.)]),
        (Cond::Any(&["dep:proc-macro2"]), &[("development-tools::procedural-macro-helpers", 1.2, 0.1), ("finance", 0.5, 0.), ("command-line-utilities", 0.3, 0.), ("caching", 0.9, 0.), ("data-structures", 0.3, 0.), ("compilers", 0.3, 0.), ("algorithms", 0.5, 0.)]),
        (Cond::All(&["implementations"]), &[("games", 0.8, 0.), ("development-tools", 0.8, 0.)]),
        (Cond::Any(&["singleton", "iterators", "streaming-iterator", "hkt", "gat", "higher-kinded-types", "scoped-tls", "thread-local", "thread-local-storage",
            "newtype", "dictionary", "functor", "monad", "haskell", "mutation"]),
            &[("rust-patterns", 1.1, 0.1), ("command-line-utilities", 0.7, 0.), ("development-tools", 0.8, 0.), ("date-and-time", 0.9, 0.), ("filesystem", 0.9, 0.), ("science::geo", 0.9, 0.), ("memory-management", 0.8, 0.),
            ("development-tools::build-utils", 0.9, 0.), ("internationalization", 0.8, 0.), ("command-line-interface", 0.8, 0.), ("games", 0.5, 0.)]),
        (Cond::Any(&["rustc", "string", "strings", "num", "struct", "coproduct", "slice", "assert",]),
            &[("rust-patterns", 1.1, 0.1), ("command-line-utilities", 0.7, 0.), ("development-tools", 0.8, 0.),
            ("memory-management", 0.8, 0.), ("command-line-interface", 0.8, 0.), ("games", 0.5, 0.), ("parser-implementations", 0.7, 0.)]),
        (Cond::Any(&["monoidal", "monoid", "type-level", "bijective", "impl", "semigroup"]),
            &[("rust-patterns", 1.1, 0.1), ("command-line-utilities", 0.7, 0.), ("no-std", 0.8, 0.), ("internationalization", 0.7, 0.), ("development-tools", 0.8, 0.), ("os::macos-apis", 0.8, 0.),
            ("memory-management", 0.8, 0.), ("command-line-interface", 0.8, 0.), ("games", 0.5, 0.), ("parser-implementations", 0.7, 0.)]),
        (Cond::Any(&["iterator", "owned-string", "type-inference"]),
            &[("rust-patterns", 1.1, 0.1), ("algorithms", 1.1, 0.1), ("gui", 0.9, 0.), ("no-std", 0.8, 0.), ("filesystem", 0.8, 0.), ("development-tools::testing", 0.6, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::Any(&["stack", "builder", "nan", "zero-cost"]),
            &[("rust-patterns", 1.1, 0.1), ("algorithms", 1.1, 0.1), ("gui", 0.9, 0.)]),
        (Cond::Any(&["structure", "endian", "big-endian", "binary", "binaries", "storing-values"]),
            &[("data-structures", 1.2, 0.1), ("algorithms", 1.1, 0.), ("science", 0.8, 0.), ("science::geo", 0.8, 0.), ("science::bio", 0.8, 0.), ("multimedia::audio", 0.9, 0.),
            ("command-line-utilities", 0.9, 0.), ("text-editors", 0.7, 0.), ("internationalization", 0.7, 0.), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::Any(&["structures", "trie", "linked-list", "incremental", "tree", "trees", "intersection", "tree-structure"]),
            &[("data-structures", 1.22, 0.1), ("algorithms", 1.1, 0.), ("science", 0.8, 0.), ("no-std", 0.8, 0.), ("multimedia::audio", 0.9, 0.),
            ("command-line-utilities", 0.9, 0.), ("text-editors", 0.7, 0.), ("internationalization", 0.7, 0.), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::Any(&["string-interner", "interner", "internment"]),
            &[("data-structures", 1.22, 0.1), ("algorithms", 1.1, 0.), ("concurrency", 0.8, 0.), ("no-std", 0.8, 0.), ("multimedia::audio", 0.6, 0.),
            ("command-line-utilities", 0.9, 0.), ("text-editors", 0.9, 0.), ("internationalization", 0.8, 0.), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::Any(&["data-structure", "vec2d",]),
            &[("data-structures", 1.2, 0.1), ("algorithms", 0.8, 0.), ("compilers", 0.7, 0.), ("science", 0.8, 0.), ("no-std", 0.8, 0.),
            ("development-tools::build-utils", 0.8, 0.), ("development-tools::cargo-plugins", 0.6, 0.), ("multimedia::audio", 0.9, 0.), ("command-line-utilities", 0.9, 0.),
            ("text-editors", 0.7, 0.), ("internationalization", 0.7, 0.)]),
        (Cond::All(&["structures", "data"]), &[("data-structures", 1.2, 0.2), ("algorithms", 0.9, 0.)]),
        (Cond::All(&["structure", "data"]), &[("data-structures", 1.2, 0.3), ("algorithms", 0.9, 0.)]),
        (Cond::All(&["tree", "binary"]), &[("data-structures", 1.2, 0.1)]),
        (Cond::All(&["functional", "programming"]), &[("rust-patterns", 1.1, 0.05)]),
        (Cond::Any(&["chunking", "solver", "satisfiability"]), &[("algorithms", 1.1, 0.05)]),
        (Cond::Any(&["algorithm", "sat-solver", "proof-checker"]), &[("algorithms", 1.1, 0.05)]),
        (Cond::Any(&["crdt", "crdts"]), &[("data-structures", 1.2, 0.1), ("algorithms", 1.1, 0.05), ("science::geo", 0.9, 0.)]),
        (Cond::Any(&["hnsw", "ann", "distance"]), &[("algorithms", 1.2, 0.1)]),
        (Cond::All(&["deduplication"]), &[("algorithms", 1.1, 0.), ("filesystem", 1.1, 0.), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::Any(&["convolution", "movie"]), &[("games", 0.5, 0.), ("data-structures", 0.9, 0.), ("finance", 0.8, 0.)]),
        (Cond::Any(&["dsp", "movies"]), &[("games", 0.5, 0.), ("parsing", 0.5, 0.), ("data-structures", 0.9, 0.)]),
        (Cond::Any(&["convolution", "dsp"]), &[("algorithms", 1.4, 0.1), ("data-structures", 0.8, 0.)]),
        (Cond::Any(&["collection", "collections", "ringbuffer"]), &[("data-structures", 1.2, 0.1), ("algorithms", 0.9, 0.)]),
        (Cond::Any(&["safe", "unsafe", "specialized", "convenience", "helper", "helpers"]),
            &[("rust-patterns", 1.1, 0.), ("science", 0.8, 0.), ("cryptography::cryptocurrencies", 0.8, 0.)]),
        (Cond::Any(&["safe", "unsafe"]), &[("multimedia::video", 0.8, 0.), ("development-tools::cargo-plugins", 0.5, 0.), ("command-line-utilities", 0.9, 0.), ("rendering::engine", 0.8, 0.)]),

        (Cond::Any(&["algorithms", "algorithm", "algorithmic", "algos", "sliding"]),
            &[("algorithms", 1.2, 0.2), ("cryptography", 0.8, 0.), ("text-editors", 0.9, 0.),("web-programming::http-client", 0.8, 0.), ("development-tools::testing", 0.5, 0.),
            ("development-tools", 0.5, 0.), ("data-structures", 0.9, 0.), ("gui", 0.9, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::Any(&["convert", "converter", "guid", "convex", "hamming-distance"]),
            &[("algorithms", 1.2, 0.2), ("cryptography", 0.8, 0.), ("text-editors", 0.8, 0.), ("web-programming::http-client", 0.8, 0.), ("development-tools::testing", 0.5, 0.),
            ("development-tools", 0.5, 0.), ("data-structures", 0.9, 0.), ("gui", 0.9, 0.), ("visualization", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::Any(&["integer", "floating-point","partition", "sequences", "quadtree", "lookup",  "kernels", "sieve", "values"]),
            &[("algorithms", 1.1, 0.1), ("data-structures", 1.1, 0.1), ("no-std", 0.9, 0.), ("science::math", 0.8, 0.), ("os", 0.8, 0.), ("games", 0.5, 0.), ("memory-management", 0.75, 0.), ("multimedia::video", 0.8, 0.)]),
        (Cond::Any(&["implementation", "generator", "normalize", "random", "random-numbers", "ordered", "set", "hierarchical", "multimap", "bitvector", "integers"]),
            &[("algorithms", 1.1, 0.1), ("data-structures", 1.1, 0.1), ("science::math", 0.8, 0.), ("os", 0.8, 0.), ("games", 0.5, 0.), ("memory-management", 0.75, 0.), ("multimedia::video", 0.8, 0.)]),
        (Cond::Any(&["bloom", "arrays", "bloom-filter", "list", "vec", "container", "octree", "binary-tree", "hash-set", "hashmap", "hashtable", "map"]),
            &[("data-structures", 1.2, 0.1), ("algorithms", 1.1, 0.1), ("science::math", 0.8, 0.), ("science::geo", 0.9, 0.), ("os", 0.9, 0.), ("embedded", 0.9, 0.),
            ("games", 0.8, 0.), ("date-and-time", 0.8, 0.), ("memory-management", 0.75, 0.), ("multimedia::video", 0.8, 0.)]),
        (Cond::Any(&["concurrent", "mpsc", "mpmc", "spsc", "producer", "condition",  "mutex", "rwlock", "futex"]), &[
            ("concurrency", 1.3, 0.15), ("algorithms", 1.1, 0.1), ("data-structures", 0.9, 0.), ("config", 0.9, 0.)]),
        (Cond::All(&["concurrent", "queue"]), &[("concurrency", 1.3, 0.15), ("no-std", 0.7, 0.), ("caching", 0.8, 0.), ("data-structures", 0.8, 0.)]),
        (Cond::All(&["message", "queue"]), &[("asynchronous", 1.2, 0.1)]),
        (Cond::All(&["bloom", "filter"]), &[("data-structures", 1.3, 0.2), ("accessibility", 0.5, 0.), ("config", 0.9, 0.), ("parsing", 0.5, 0.)]),
        (Cond::Any(&["scheduler", "publisher", "lock", "deque", "channel"]), &[("concurrency", 1.3, 0.15), ("algorithms", 0.9, 0.), ("os", 1.1, 0.), ("data-structures", 0.8, 0.)]),
        (Cond::Any(&["thread", "parallel-processing", "parallel-execution"]), &[("concurrency", 1.2, 0.1), ("algorithms", 0.9, 0.), ("data-structures", 0.9, 0.)]),
        (Cond::Any(&["persistent", "immutable", "persistent-datastructures"]), &[("algorithms", 1.15, 0.1), ("data-structures", 1.3, 0.2), ("database-implementations", 1.1, 0.05), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.)]),

        (Cond::Any(&["statistics", "statistic", "order-statistics", "svd", "markov", "cognitive", "monte-carlo"]),
            &[("science", 1.2, 0.1), ("science::ml", 1.25, 0.), ("algorithms", 1.2, 0.1), ("command-line-interface", 0.3, 0.), ("caching", 0.9, 0.), ("simulation", 0.75, 0.),("command-line-utilities", 0.75, 0.),
            ("games", 0.3, 0.), ("no-std", 0.95, 0.), ("caching", 0.8, 0.), ("rendering::graphics-api", 0.9, 0.), ("development-tools::profiling", 0.6, 0.), ("internationalization", 0.7, 0.)]),
        (Cond::Any(&["variance", "units", "subsequences", "lazy", "linear", "distribution"]),
            &[("science", 1.25, 0.2), ("algorithms", 1.2, 0.1), ("data-structures", 1.2, 0.1), ("command-line-interface", 0.3, 0.), ("simulation", 0.75, 0.),("command-line-utilities", 0.75, 0.), ("games", 0.3, 0.), ("no-std", 0.95, 0.), ("caching", 0.8, 0.), ("development-tools::profiling", 0.6, 0.), ("internationalization", 0.7, 0.)]),
        (Cond::Any(&["computational", "hpc", "tries", "collection", "rational", "newtonian", "scientific", "science"]),
            &[("science", 1.25, 0.2), ("algorithms", 1.2, 0.1), ("data-structures", 1.2, 0.1), ("command-line-interface", 0.3, 0.), ("simulation", 0.75, 0.),("command-line-utilities", 0.75, 0.),
            ("games", 0.3, 0.), ("no-std", 0.95, 0.), ("caching", 0.8, 0.), ("rendering::graphics-api", 0.8, 0.), ("development-tools::profiling", 0.6, 0.), ("internationalization", 0.7, 0.)]),
        (Cond::Any(&["median", "alpha", "equations", "bigdecimal", "matrix", "proving", "matrices", "sat", "multi-dimensional", "convex", "unification"]),
            &[("science", 1.2, 0.1), ("science::math", 1.2, 0.1), ("science::bio", 0.7, 0.), ("algorithms", 1.2, 0.1), ("text-processing", 0.8, 0.), ("data-structures", 1.15, 0.01),
            ("command-line-interface", 0.3, 0.), ("simulation", 0.75, 0.), ("memory-management", 0.8, 0.), ("caching", 0.8, 0.),
            ("command-line-utilities", 0.75, 0.), ("games", 0.3, 0.), ("no-std", 0.95, 0.), ("caching", 0.8, 0.),
            ("development-tools::profiling", 0.6, 0.), ("internationalization", 0.7, 0.), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::Any(&["fibonacci", "interpolate", "interpolation", "coordinates", "permutation", "combinatorial", "dfa", "cellular-automata", "automata", "solvers", "solver", "integral"]),
            &[("science", 1.2, 0.1), ("science::math", 1.2, 0.1), ("science::bio", 0.7, 0.), ("algorithms", 1.3, 0.1), ("text-processing", 0.8, 0.), ("cryptography", 0.9, 0.),
            ("data-structures", 1.1, 0.1), ("command-line-interface", 0.3, 0.), ("memory-management", 0.8, 0.), ("caching", 0.8, 0.), ("simulation", 0.75, 0.), ("text-editors", 0.8, 0.),
            ("command-line-utilities", 0.75, 0.), ("games", 0.3, 0.), ("no-std", 0.95, 0.), ("caching", 0.8, 0.), ("development-tools::profiling", 0.6, 0.), ("internationalization", 0.7, 0.)]),
        (Cond::Any(&["approximate-knn", "knn", "nearest-neighbor", "kd-tree"]), &[("algorithms", 1.2, 0.2), ("data-structures", 1.1, 0.), ("rendering::graphics-api", 0.8, 0.), ("email", 0.7, 0.), ("database-implementations", 0.5, 0.)]),
        (Cond::Any(&["graph", "sparse", "summed", "kdtree", "kd-tree", "space-partitioning"]),
            &[("data-structures", 1.5, 0.2), ("algorithms", 1.3, 0.1), ("science", 1.2, 0.2), ("database", 0.9, 0.), ("config", 0.7, 0.), ("concurrency", 0.9, 0.), ("command-line-interface", 0.3, 0.), ("command-line-utilities", 0.75, 0.)]),

        (Cond::Any(&["procedural", "procgen"]), &[("algorithms", 1.25, 0.2), ("game-development", 1.25, 0.), ("games", 0.8, 0.), ("multimedia::images", 1.05, 0.)]),
        (Cond::All(&["finite", "state"]), &[("algorithms", 1.1, 0.), ("science::math", 0.8, 0.)]),
        (Cond::All(&["finite", "automata"]), &[("algorithms", 1.25, 0.2), ("science::math", 0.8, 0.)]),
        (Cond::All(&["machine", "state"]), &[("algorithms", 1.25, 0.2), ("science::math", 0.8, 0.)]),
        (Cond::All(&["fsm", "state"]), &[("algorithms", 1.25, 0.2), ("science::math", 0.8, 0.)]),
        (Cond::All(&["machine", "state", "logic", "fuzzy"]), &[("algorithms", 1.25, 0.2), ("science::math", 0.8, 0.)]),
        (Cond::Any(&["state-machine", "statemachine", "stateful"]), &[("algorithms", 1.25, 0.2), ("science", 1.1, 0.), ("science::math", 0.7, 0.)]),
        (Cond::Any(&["compute", "computation"]), &[("algorithms", 1.2, 0.), ("science::math", 1.1, 0.)]),
        (Cond::Any(&["worker", "taskqueue", "a-star", "pathfinding", "easing", "sorter", "sorting", "quicksort", "sorting-algorithms", "prng", "random", "mersenne"]),
                &[("algorithms", 1.25, 0.1), ("science::math", 0.8, 0.), ("caching", 0.8, 0.), ("no-std", 0.8, 0.), ("command-line-interface", 0.4, 0.),
                ("database", 0.8, 0.), ("os", 0.9, 0.), ("command-line-utilities", 0.4, 0.), ("development-tools::build-utils", 0.9, 0.), ("development-tools::testing", 0.6, 0.), ("development-tools::profiling", 0.8, 0.)]),
        (Cond::Any(&["prolog"]), &[("algorithms", 1.25, 0.1), ("cryptography", 0.5, 0.)]),
        (Cond::Any(&["lock-free"]),
                &[("data-structures", 1.25, 0.1), ("concurrency", 1.1, 0.), ("algorithms", 1.1, 0.), ("science::math", 0.8, 0.), ("caching", 0.8, 0.), ("command-line-interface", 0.4, 0.), ("os", 0.9, 0.), ("command-line-utilities", 0.4, 0.)]),
        (Cond::Any(&["queue", "collection", "sort"]),
                &[("data-structures", 1.25, 0.1), ("algorithms", 1.1, 0.), ("caching", 0.9, 0.), ("science::math", 0.8, 0.), ("caching", 0.8, 0.), ("command-line-interface", 0.4, 0.), ("os", 0.9, 0.), ("command-line-utilities", 0.4, 0.)]),
        (Cond::Any(&["hyperloglog", "hll", "index-scan"]),
                &[("data-structures", 1.1, 0.1), ("algorithms", 1.3, 0.2), ("science::math", 1.1, 0.), ("database-implementations", 1.1, 0.), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.5, 0.)]),

        (Cond::Any(&["macro", "macros", "dsl", "procedural-macros", "proc-macro", "proc-macros", "derive", "proc_macro", "custom-derive"]), &[
            ("development-tools::procedural-macro-helpers", 1.4, 0.2), ("no-std", 0.7, 0.), ("parser-implementations", 0.8, 0.), ("multimedia::video", 0.6, 0.), ("multimedia", 0.8, 0.), ("rust-patterns", 1.2, 0.1), ("cryptography", 0.7, 0.),
            ("memory-management", 0.7, 0.), ("internationalization", 0.7, 0.), ("development-tools", 0.7, 0.), ("algorithms", 0.8, 0.), ("science::math", 0.7, 0.),
            ("web-programming::websocket", 0.6, 0.), ("no-std", 0.8, 0.), ("compression", 0.8, 0.), ("command-line-interface", 0.5, 0.),
            ("development-tools::testing", 0.8, 0.), ("development-tools::debugging", 0.8, 0.), ("development-tools::build-utils", 0.6, 0.)]),
        (Cond::Any(&["has:proc_macro"]), &[("development-tools::procedural-macro-helpers", 1.5, 0.3), ("command-line-utilities", 0.3, 0.), ("rust-patterns", 0.7, 0.)]),
        (Cond::NotAny(&["has:proc_macro", "derive", "proc-macro", "proc-macros", "dep:syn", "dep:paste", "dep:darling_core", "dep:quote", "proc", "procmacro", "macros", "syntax"]),
            &[("development-tools::procedural-macro-helpers", 0.6, 0.)]),
        (Cond::NotAny(&["has:proc_macro", "dep:proc-macro-error", "dep:proc-quote", "dep:proc-macro2", "proc-macro", "proc-macros", "dep:syn", "dep:quote"]),
            &[("development-tools::procedural-macro-helpers", 0.7, 0.)]),

        (Cond::Any(&["similarity", "string"]), &[("development-tools::procedural-macro-helpers", 0.9, 0.), ("rust-patterns", 0.9, 0.)]),

        (Cond::Any(&["emoji", "stemming", "highlighting", "whitespace", "uppercase", "indentation", "spellcheck", "hunspell"]), &[("text-processing", 1.4, 0.2), ("finance", 0.9, 0.), ("science::math", 0.8, 0.), ("algorithms", 0.8, 0.)]),
        (Cond::Any(&["regex", "matching"]), &[("text-processing", 1.2, 0.1), ("science::math", 0.8, 0.), ("science::math", 0.8, 0.), ("filesystem", 0.7, 0.), ("science::math", 0.8, 0.)]),
        (Cond::Any(&["memchr"]), &[("text-processing", 1.1, 0.1), ("algorithms", 1.1, 0.1), ("internationalization", 0.5, 0.)]),
        (Cond::Any(&["string"]), &[("text-processing", 1.1, 0.), ("internationalization", 0.9, 0.)]),
        (Cond::Any(&["strchr"]), &[("text-processing", 1.1, 0.), ("internationalization", 0.7, 0.)]),
        (Cond::Any(&["dep:csv"]), &[("text-processing", 1.1, 0.1)]),
        (Cond::Any(&["pulldown-cmark", "dep:pulldown-cmark", "bbcode", "ascii", "ngrams"]),
            &[("text-processing", 1.2, 0.2), ("parser-implementations", 1.2, 0.), ("no-std", 0.8, 0.), ("caching", 0.9, 0.), ("multimedia::video", 0.8, 0.), ("parsing", 0.9, 0.),
            ("development-tools::build-utils", 0.8, 0.), ("development-tools::testing", 0.2, 0.), ("filesystem", 0.7, 0.), ("development-tools", 0.7, 0.),
            ("multimedia::images", 0.5, 0.), ("command-line-utilities", 0.8, 0.)]),
        (Cond::Any(&["markdown", "restructuredtext", "commonmark", "pulldown-cmark", "common-mark", "unicode-aware", "latex", "mdbook", "dep:mdbook", "stack-graphs", "kindle"]),
            &[("text-processing", 1.2, 0.2), ("parser-implementations", 1.2, 0.),
            ("multimedia::video", 0.8, 0.), ("compression", 0.6, 0.), ("config", 0.7, 0.), ("web-programming", 0.9, 0.), ("cryptography", 0.6, 0.), ("parsing", 0.9, 0.),
            ("development-tools::build-utils", 0.8, 0.), ("development-tools::testing", 0.2, 0.),
            ("filesystem", 0.7, 0.), ("development-tools", 0.8, 0.), ("multimedia::images", 0.5, 0.), ("command-line-interface", 0.9, 0.)]),
        (Cond::Any(&["case-folding", "text", "syllable", "text-replacement", "rot13", "character-property", "character"]),
            &[("text-processing", 1.1, 0.2), ("rendering", 0.9, 0.), ("embedded", 0.8, 0.), ("web-programming", 0.9, 0.), ("rendering::data-formats", 0.5, 0.), ("development-tools::testing", 0.6, 0.)]),
        (Cond::Any(&["unicode",  "characters", "grapheme", "crlf", "codepage", "whitespace", "unicode-characters", "utf", "utf-8", "utf-16", "utf-32", "utf8", "case"]),
            &[("text-processing", 1.1, 0.2), ("rendering", 0.9, 0.), ("embedded", 0.8, 0.), ("no-std", 0.9, 0.), ("caching", 0.9, 0.), ("games", 0.7, 0.),
            ("web-programming", 0.9, 0.), ("rendering::data-formats", 0.6, 0.), ("development-tools::testing", 0.6, 0.)]),
        (Cond::Any(&["markdown", "commonmark", "common-mark", "latex", "dep:mdbook"]),
            &[("compilers", 0.6, 0.), ("caching", 0.5, 0.)]),

        (Cond::Any(&["ascii", "grep", "string-replace"]), &[("text-processing", 1.2, 0.)]),
        (Cond::Any(&["dep:hangul", "dep:sejong", "dep:hanja"]), &[("text-processing", 1.2, 0.1), ("internationalization", 1.2, 0.1), ("config", 0.9, 0.)]),
        (Cond::All(&["ascii", "convert"]), &[("text-processing", 1.2, 0.1), ("visualization", 0.6, 0.)]),
        (Cond::Any(&["pdf", "epub", "ebook", "book", "typesetting", "xetex"]),
            &[("text-processing", 1.3, 0.2), ("science", 0.9, 0.), ("encoding", 0.9, 0.), ("compression", 0.9, 0.), ("science::math", 0.8, 0.), ("no-std", 0.7, 0.),
            ("games", 0.8, 0.), ("wasm", 0.8, 0.), ("database-implementations", 0.8, 0.), ("rendering::data-formats", 1.2, 0.), ("rendering::graphics-api", 0.8, 0.),
            ("rendering", 1.05, 0.), ("web-programming::http-client", 0.5, 0.), ("parsing", 0.8, 0.), ("command-line-interface", 0.5, 0.), ("visualization", 0.7, 0.)]),
        (Cond::Any(&["dep:lopdf", "dep:pdfpdf"]), &[("text-processing", 1.2, 0.1), ("parsing", 0.5, 0.), ("date-and-time", 0.5, 0.)]),
        (Cond::All(&["auto", "correct"]), &[("text-processing", 1.2, 0.1), ("multimedia::images", 0.5, 0.)]),

        (Cond::Any(&["templating", "template", "template-engine", "handlebars", "mustache", "jinja", "jinja2"]),
            &[("template-engine", 1.4, 0.3), ("embedded", 0.2, 0.), ("games", 0.5, 0.), ("no-std", 0.8, 0.), ("internationalization", 0.6, 0.), ("development-tools::cargo-plugins", 0.7, 0.), ("command-line-interface", 0.4, 0.)]),

        (Cond::Any(&["benchmark","benchmarking", "profiling"]),
            &[("development-tools::profiling", 1.2, 0.2), ("rust-patterns", 0.94, 0.), ("finance", 0.8, 0.), ("algorithms", 0.8, 0.),
            ("development-tools::build-utils", 0.8, 0.), ("cryptography::cryptocurrencies", 0.7, 0.),
            ("simulation", 0.75, 0.), ("science::robotics", 0.7, 0.), ("parsing", 0.8, 0.), ("os::macos-apis", 0.9, 0.), ("authentication", 0.5, 0.)]),
        (Cond::Any(&["bench", "profiler", "flamegraph"]),
            &[("development-tools::profiling", 1.2, 0.2), ("development-tools::debugging", 0.7, 0.), ("rust-patterns", 0.94, 0.), ("algorithms", 0.8, 0.), ("cryptography::cryptocurrencies", 0.7, 0.),
            ("simulation", 0.75, 0.), ("parsing", 0.8, 0.), ("os::macos-apis", 0.9, 0.), ("web-programming::http-client", 0.8, 0.), ("authentication", 0.5, 0.)]),
        (Cond::Any(&["perf", "performance", "optimizer"]), &[("development-tools::profiling", 1.2, 0.1), ("development-tools", 1.1, 0.), ("caching", 0.9, 0.)]),
        (Cond::Any(&["spdx"]), &[("development-tools", 1.1, 0.1), ("no-std", 0.8, 0.), ("parsing", 0.5, 0.), ("caching", 0.9, 0.)]),
        (Cond::All(&["sampling", "profiler"]), &[("development-tools::profiling", 1.2, 0.1), ("development-tools", 1.1, 0.), ("email", 0.7, 0.), ("web-programming::http-client", 0.6, 0.)]),
        (Cond::All(&["tracing", "profiler"]), &[("development-tools::profiling", 1.2, 0.1), ("development-tools", 1.1, 0.), ("web-programming::http-client", 0.8, 0.)]),
        (Cond::Any(&["continuous-deployment"]), &[("development-tools", 1.1, 0.1)]),
        (Cond::Any(&["bump", "changelog"]), &[("development-tools", 1.2, 0.), ("development-tools::profiling", 0.6, 0.)]),
        (Cond::Any(&["git", "git-subtree", "github", "github-api", "dep:git-object", "dep:git2", "development-workflow", "dep:gix"]),
            &[("development-tools", 1.1, 0.), ("parsing", 0.8, 0.), ("gui", 0.9, 0.), ("database", 0.8, 0.), ("caching", 0.9, 0.), ("email", 0.8, 0.), ("date-and-time", 0.8, 0.)]),
        (Cond::Any(&["version-control", "vcs", "mercurial", "mercurial-source-control", "devtools", "dev-tools"]),
            &[("development-tools", 1.15, 0.15), ("date-and-time", 0.8, 0.), ("parsing", 0.6, 0.), ("accessibility", 0.8, 0.)]),
        (Cond::Any(&["sourcecode", "commit", "generates", "code-beautify"]), &[("development-tools", 1.1, 0.)]),
        (Cond::Any(&["installer", "packages"]), &[("development-tools", 1.1, 0.), ("os", 1.1, 0.)]),
        (Cond::Any(&["dep:unicode-xid"]), &[("development-tools", 1.1, 0.)]),

        (Cond::Any(&["filter", "download", "downloader"]), &[("command-line-utilities", 0.75, 0.), ("command-line-interface", 0.5, 0.), ("data-structures", 0.8, 0.)]),
        (Cond::Any(&["error"]), &[("command-line-utilities", 0.5, 0.), ("command-line-interface", 0.7, 0.), ("games", 0.7, 0.)]),
        (Cond::Any(&["serde", "avro", "apache-avro"]), &[("encoding", 1.3, 0.1), ("no-std", 0.8, 0.), ("command-line-utilities", 0.5, 0.), ("command-line-interface", 0.7, 0.), ("development-tools::cargo-plugins", 0.8, 0.)]),
        (Cond::Any(&["encoding", "encode", "encodes"]), &[("encoding", 1.3, 0.1), ("command-line-utilities", 0.5, 0.), ("command-line-interface", 0.7, 0.), ("development-tools::cargo-plugins", 0.8, 0.)]),
        (Cond::Any(&["binary", "byte"]), &[("encoding", 1.2, 0.05), ("parser-implementations", 1.1, 0.05)]),
        (Cond::Any(&["json", "base64", "toml", "semver", "punycode"]), &[
            ("encoding", 1.2, 0.1), ("parser-implementations", 1.2, 0.1), ("parsing", 0.2, 0.), ("date-and-time", 0.8, 0.), ("web-programming::websocket", 0.5, 0.),
            ("rust-patterns", 0.8, 0.), ("multimedia", 0.5, 0.), ("multimedia::video", 0.7, 0.), ("multimedia::audio", 0.7, 0.)]),
        (Cond::Any(&["sodium"]), &[("algorithms", 1.2, 0.1), ("cryptography", 1.1, 0.1),
            ("cryptography::cryptocurrencies", 0.8, 0.), ("os::macos-apis", 0.8, 0.), ("no-std", 0.9, 0.), ("date-and-time", 0.5, 0.),
            ("memory-management", 0.7, 0.), ("development-tools", 0.7, 0.), ("command-line-utilities", 0.5, 0.), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::All(&["hash", "function"]), &[("algorithms", 1.2, 0.1), ("cryptography", 1.1, 0.)]),
        (Cond::Any(&["hashing"]), &[("algorithms", 1.2, 0.1), ("cryptography", 1.1, 0.)]),
        (Cond::All(&["cryptographic", "hash", "function"]), &[("algorithms", 1.2, 0.1), ("cryptography", 1.2, 0.1)]),
        (Cond::Any(&["crc32", "fnv", "phf", "adler32", "consistent-hashing", "murmur", "fasthash", "xxhash"]),
            &[("algorithms", 1.2, 0.1), ("cryptography", 0.5, 0.), ("os::macos-apis", 0.8, 0.), ("config", 0.9, 0.), ("data-structures", 0.9, 0.)]),
        (Cond::Any(&["fetching"]), &[("encoding", 0.7, 0.), ("cryptography", 0.9, 0.)]),
        (Cond::Any(&["insecure", "unverified", "non-secure", "non-cryptographic", "not-cryptographically", "pcg", "vulnerable",
            "congruential", "xxhash", "rolling", "checksum", "crc"]), &[("cryptography", 0.8, 0.), ("compression", 0.8, 0.)]),
        (Cond::Any(&["pcg", "xxhash", "checksum", "crc"]), &[("algorithms", 1.2, 0.2)]),

        (Cond::Any(&["pickle", "serde"]), &[("encoding", 1.3, 0.1), ("embedded", 0.9, 0.), ("development-tools", 0.8, 0.),
            ("parsing", 0.9, 0.), ("parser-implementations", 1.2, 0.1), ("algorithms", 0.8, 0.)]),
        (Cond::Any(&["manager"]), &[("encoding", 0.6, 0.), ("parser-implementations", 0.4, 0.), ("data-structures", 0.8, 0.)]),

        (Cond::NotAny(&["crypto", "cryptography", "cryptographic", "schnorr", "pgp", "signature", "digital-signature", "hash", "hashes", "hashing", "dep:digest", "digest", "nacl", "blake2", "libsodium", "zeroize", "merkle", "hmac",
            "constant-time", "constant_time", "ecc", "elliptic", "rsa", "ecdsa", "secp256k1", "ed25519", "curve25519", "argon2", "pbkdf2","block-cipher","cipher","signature",
             "random", "pki", "webpki", "bcrypt", "sha2", "aes", "symmetric-encryption", "nonce", "zero-knowledge", "entropy",
            "pem", "cert", "certificate", "certificates", "pki", "tls", "ssl", "cryptohash"]),
            &[("cryptography", 0.75, 0.)]),
        (Cond::Any(&["zero-knowledge", "digital-signature", "symmetric-encryption", "elliptic-curve", "zero-knowledge-proof", "zk-snarks", "diffie-hellman", "curve25519", "zk-snark", "cryptohash"]),
            &[("cryptography", 1.3, 0.2), ("algorithms", 0.8, 0.), ("authentication", 1.1, 0.), ("caching", 0.9, 0.), ("no-std", 0.8, 0.), ("development-tools::cargo-plugins", 0.9, 0.),
            ("compilers", 0.8, 0.), ("science", 0.8, 0.), ("accessibility", 0.5, 0.), ("filesystem", 0.8, 0.), ("command-line-utilities", 0.6, 0.)]),
        (Cond::Any(&["crypto", "nonce", "dep:digest", "post-quantum", "post-quantum-cryptography", "kyber", "schnorr", "ecdsa","signature", "key-exchange", "entropy", "pem", "cert", "certificate", "certificates", "pki"]),
            &[("cryptography", 1.2, 0.2), ("cryptography::cryptocurrencies", 1.1, 0.), ("algorithms", 0.9, 0.), ("asynchronous", 0.9, 0.), ("no-std", 0.9, 0.), ("development-tools::cargo-plugins", 0.9, 0.),
            ("filesystem", 0.8, 0.), ("accessibility", 0.8, 0.), ("date-and-time", 0.9, 0.), ("command-line-utilities", 0.8, 0.)]),
        (Cond::Any(&["keyfile", "key", "encrypt", "encryption-key", "homomorphic"]), &[("cryptography", 1.2, 0.05), ("cryptography::cryptocurrencies", 0.9, 0.), ("development-tools::ffi", 0.6, 0.)]),
        (Cond::Any(&["secure", "encrypted", "signing", "nacl", "na-cl"]), &[("cryptography", 1.2, 0.01)]),
        (Cond::All(&["elliptic", "curve"]), &[("cryptography", 1.4, 0.2), ("asynchronous", 0.9, 0.)]),
        (Cond::Any(&["dep:subtle", "paillier"]), &[("cryptography", 1.2, 0.2), ("asynchronous", 0.9, 0.)]),

        (Cond::Any(&["command-line-tool", "coreutil", "cli-utilities", "uutils", "coreutils"]), &[
            ("command-line-utilities", 1.2, 0.4), ("algorithms", 0.8, 0.), ("parsing", 0.5, 0.), ("data-structures", 0.8, 0.), ("caching", 0.9, 0.), ("compilers", 0.8, 0.), ("asynchronous", 0.9, 0.), ("no-std", 0.7, 0.)
        ]),
        (Cond::Any(&["command-line-utility", "command-line-application"]), &[("command-line-utilities", 1.2, 0.4)]),
        (Cond::All(&["command", "line"]), &[("command-line-utilities", 1.15, 0.1), ("command-line-interface", 1.15, 0.)]),
        (Cond::Any(&["prompt"]), &[("command-line-utilities", 1.1, 0.), ("command-line-interface", 1.1, 0.)]),
        (Cond::Any(&["dep:exitcode"]), &[("command-line-utilities", 1.2, 0.1), ("command-line-interface", 1.1, 0.), ("parsing", 0.1, 0.)]),
        (Cond::All(&["commandline", "interface"]), &[("command-line-utilities", 1.15, 0.1), ("command-line-interface", 1.15, 0.)]),
        (Cond::Any(&["commandline", "command-line", "cmdline"]),
            &[("command-line-utilities", 1.1, 0.1), ("command-line-interface", 1.1, 0.), ("rust-patterns", 0.8, 0.), ("database", 0.8, 0.), ("development-tools::ffi", 0.7, 0.),
            ("web-programming::http-client", 0.8, 0.), ("web-programming::http-server", 0.8, 0.), ("web-programming", 0.8, 0.)]),

        (Cond::Any(&["has:is_build", "has:is_dev"]), &[("os::windows-apis", 0.9, 0.), ("finance", 0.9, 0.), ("development-tools", 1.1, 0.),
            ("science", 0.8, 0.), ("science::math", 0.8, 0.), ("games", 0.8, 0.), ("value-formatting", 0.9, 0.)]),
        (Cond::Any(&["has:is_dev"]), &[("development-tools::profiling", 1.2, 0.), ("multimedia::audio", 0.7, 0.), ("email", 0.9, 0.), ("caching", 0.9, 0.), ("rendering", 0.9, 0.), ("text-editors", 0.9, 0.), ("email", 0.9, 0.), ("rendering::graphics-api", 0.9, 0.), ("concurrency", 0.9, 0.)]),
        (Cond::Any(&["has:is_build"]), &[("concurrency", 0.9, 0.), ("rendering", 0.9, 0.), ("text-editors", 0.8, 0.), ("visualization", 0.9, 0.), ("simulation", 0.8, 0.), ("science::robotics", 0.8, 0.), ("multimedia::audio", 0.7, 0.), ("memory-management", 0.8, 0.)]),

        (Cond::Any(&["numeral", "numerals", "human-readable", "formatter", "notation", "pretty", "metric"]),
            &[("value-formatting", 1.2, 0.2), ("simulation", 0.5, 0.), ("science::robotics", 0.7, 0.), ("wasm", 0.7, 0.), ("caching", 0.9, 0.), ("no-std", 0.8, 0.)]),
        (Cond::Any(&["pretty-print", "value-formatting", "string-formatting", "pretty-printing", "punycode", "timeago", "time-ago", "units", "weights", "uom"]),
            &[("value-formatting", 1.2, 0.2), ("simulation", 0.5, 0.), ("data-structures", 0.8, 0.), ("wasm", 0.7, 0.), ("no-std", 0.8, 0.), ("parsing", 0.8, 0.)]),
        (Cond::All(&["human", "readable"]), &[("value-formatting", 1.2, 0.2), ("data-structures", 0.8, 0.)]),
        (Cond::All(&["human", "friendly"]), &[("value-formatting", 1.1, 0.)]),
        (Cond::Any(&["fpu", "simd", "comparison"]), &[("value-formatting", 0.5, 0.)]),
        (Cond::Any(&["math", "lint"]), &[("value-formatting", 0.9, 0.)]),
        (Cond::Any(&["morphological"]), &[("value-formatting", 1.1, 0.), ("text-processing", 1.1, 0.), ("internationalization", 1.1, 0.)]),

        (Cond::Any(&["roman", "phonenumber", "currency"]), &[("value-formatting", 1.2, 0.2), ("internationalization", 1.1, 0.), ("caching", 0.9, 0.), ("parsing", 0.5, 0.), ("multimedia", 0.8, 0.), ("compilers", 0.5, 0.)]),
        (Cond::Any(&["numbers", "numeric", "value"]), &[("value-formatting", 1.2, 0.), ("science", 1.2, 0.), ("encoding", 1.1, 0.), ("parsing", 1.1, 0.), ("parser-implementations", 1.1, 0.)]),
        (Cond::Any(&["bytes", "byte", "metadata"]), &[("value-formatting", 0.8, 0.), ("development-tools::ffi", 0.9, 0.)]),
        (Cond::Any(&["log", "logging", "serde",  "nlp", "3d", "sdl2"]),
            &[("value-formatting", 0.7, 0.), ("database-implementations", 0.8, 0.), ("text-processing", 0.8, 0.), ("config", 0.9, 0.),  ("caching", 0.5, 0.),  ("no-std", 0.8, 0.), ("multimedia::images", 0.7, 0.),
            ("multimedia::audio", 0.7, 0.), ("multimedia", 0.8, 0.),  ("date-and-time", 0.5, 0.), ("cryptography::cryptocurrencies", 0.8, 0.)]),
        (Cond::Any(&["utils", "parser", "linear"]),
            &[("value-formatting", 0.7, 0.), ("database-implementations", 0.8, 0.), ("text-processing", 0.8, 0.), ("multimedia::images", 0.7, 0.),
            ("multimedia::audio", 0.7, 0.), ("multimedia", 0.8, 0.), ("cryptography::cryptocurrencies", 0.8, 0.), ("accessibility", 0.9, 0.)]),
        (Cond::Any(&["performance", "bitflags", "storage", "terminal", "rpc"]),
            &[("value-formatting", 0.25, 0.), ("development-tools", 0.8, 0.), ("network-programming", 0.7, 0.), ("science::math", 0.8, 0.)]),

        (Cond::NotAny(&["mailer", "mail", "pgp", "dkim", "spf", "mailbox", "dmarc", "mime", "pop3", "ssmtp", "smtp", "usenet", "imap", "jmap", "email", "e-mail", "sendmail"]), &[("email", 0.7, 0.)]),
        (Cond::Any(&["mailer", "pop3", "email-validation", "ssmtp", "mailbox", "esmtp", "vsmtp", "smtp", "dkim", "dkim-validator", "dmarc", "sendmail", "imap", "email", "jmap", "e-mail"]), &[
            ("email", 1.2, 0.3), ("network-programming", 0.8, 0.), ("finance", 0.8, 0.), ("database", 0.8, 0.), ("caching", 0.8, 0.), ("database-implementations", 0.8, 0.), ("embedded", 0.6, 0.),
            ("no-std", 0.7, 0.), ("algorithms", 0.8, 0.), ("config", 0.9, 0.), ("development-tools::cargo-plugins", 0.6, 0.), ("filesystem", 0.7, 0.), ("parsing", 0.5, 0.), ("accessibility", 0.7, 0.),
            ("development-tools::build-utils", 0.8, 0.), ("data-structures", 0.8, 0.), ("command-line-utilities", 0.75, 0.), ("rust-patterns", 0.75, 0.)]),
        (Cond::Any(&["newsletter", "maildir", "usenet", "rfc5322", "spf", "mua", "mailchimp", "mailgun", "spamassassin"]),
            &[("email", 1.1, 0.1), ("parsing", 0.7, 0.), ("cryptography", 0.7, 0.), ("caching", 0.9, 0.)]),
        (Cond::Any(&["editor", "vim", "text-editor", "hex-editor", "emacs", "vscode", "visual-studio", "visual-studio-code", "wordprocessor", "sublime", "neovim", "editor-plugin"]),
            &[("text-editors", 1.2, 0.2), ("os::windows-apis", 0.7, 0.), ("compilers", 0.7, 0.), ("database", 0.7, 0.), ("database-implementations", 0.7, 0.), ("games", 0.4, 0.),
            ("rendering::engine", 0.7, 0.), ("parsing", 0.75, 0.), ("rendering::graphics-api", 0.7, 0.)]),
        (Cond::Any(&["obj", "loop", "lattice", "api", "bin", "framework", "stopwatch", "sensor", "github", "algorithm", "protocol"]),
            &[("games", 0.5, 0.), ("development-tools::profiling", 0.8, 0.), ("data-structures", 0.9, 0.)]),
        (Cond::Any(&["api", "bin", "framework", "hashmap", "trie", "protocol"]),
            &[("algorithms", 0.9, 0.), ("memory-management", 0.8, 0.)]),
        (Cond::All(&["text", "editor"]), &[("text-editors", 1.4, 0.4), ("caching", 0.8, 0.), ("text-processing", 0.8, 0.), ("parsing", 0.8, 0.), ("internationalization", 0.1, 0.)]),
        (Cond::All(&["dns", "spf"]), &[("email", 1.2, 0.1), ("network-programming", 0.7, 0.), ("config", 0.9, 0.), ("caching", 0.9, 0.), ("embedded", 0.6, 0.)]),
        (Cond::All(&["language", "server"]), &[("text-editors", 1.2, 0.2), ("development-tools", 1.2, 0.1), ("date-and-time", 0.6, 0.), ("parsing", 0.5, 0.),
            ("development-tools::build-utils", 0.8, 0.), ("science::math", 0.8, 0.)]),
        (Cond::Any(&["language-server-protocol", "language-server", "vim-like"]), &[("text-editors", 1.2, 0.2), ("development-tools", 1.2, 0.1), ("parsing", 0.5, 0.),
            ("development-tools::build-utils", 0.8, 0.), ("science::math", 0.8, 0.)]),
        (Cond::Any(&["syntax-highlighting"]), &[("text-editors", 1.2, 0.2), ("compilers", 1.1, 0.), ("parsing", 0.9, 0.)]),
        (Cond::Any(&["repl", "pack"]), &[("parsing", 0.8, 0.)]),

        (Cond::Any(&["cli"]), &[("command-line-utilities", 1.1, 0.), ("command-line-interface", 1.1, 0.), ("rust-patterns", 0.6, 0.),
            ("os", 0.9, 0.), ("os::windows-apis", 0.7, 0.), ("os::macos-apis", 0.8, 0.), ("template-engine", 0.8, 0.), ("text-editors", 0.9, 0.), ("network-programming", 0.9, 0.)]),
        (Cond::Any(&["tui", "dep:tui", "terminal-ui", "command-line-arguments", "terminal-colors", "ansi-term", "lolcat", "terminal-based", "cowsay", "cli-args", "command-line-parser",
            "arguments-parser", "argument-parsing", "argparser", "argparse"]),
            &[("command-line-interface", 1.25, 0.15), ("compilers", 0.9, 0.), ("caching", 0.9, 0.), ("encoding", 0.9, 0.), ("rendering::engine", 0.8, 0.), ("parsing", 0.6, 0.), ("multimedia::audio", 0.8, 0.), ("rendering::graphics-api", 0.8, 0.)]),
        (Cond::Any(&["dep:clap", "dep:docopt", "dep:structopt", "dep:ncurses", "dep:expect-exit", "dep:wild"]),
            &[("command-line-utilities", 1.15, 0.1), ("command-line-interface", 0.9, 0.), ("parsing", 0.6, 0.), ("caching", 0.7, 0.), ("date-and-time", 0.8, 0.)]),
        (Cond::All(&["curses", "interface"]), &[("command-line-interface", 1.1, 0.05), ("parsing", 0.6, 0.), ("hardware-support", 0.7, 0.)]),
        (Cond::All(&["terminal", "ui"]), &[("command-line-interface", 1.1, 0.05), ("multimedia", 0.8, 0.), ("compression", 0.9, 0.), ("caching", 0.9, 0.), ("rendering::engine", 0.9, 0.), ("parsing", 0.5, 0.)]),
        (Cond::Any(&["tui", "dep:tui"]), &[("gui", 1.1, 0.05), ("encoding", 0.9, 0.)]),
        (Cond::Any(&["terminal", "ncurses", "pancurses", "terminfo", "termcap", "tui", "curses", "ansi", "ansi-color", "terminal-graphics", "term-painter", "progressbar", "vt100", "ansi_term"]),
            &[("command-line-interface", 1.2, 0.1), ("multimedia::images", 0.1, 0.), ("multimedia", 0.4, 0.), ("parsing", 0.7, 0.), ("rendering::engine", 0.7, 0.), ("no-std", 0.9, 0.), ("wasm", 0.9, 0.),
            ("science::math", 0.8, 0.), ("command-line-utilities", 0.75, 0.), ("internationalization", 0.9, 0.), ("algorithms", 0.7, 0.), ("rendering::graphics-api", 0.7, 0.), ("rendering", 0.7, 0.),
            ("development-tools::procedural-macro-helpers", 0.7, 0.), ("memory-management", 0.5, 0.), ("hardware-support", 0.9, 0.), ("date-and-time", 0.8, 0.), ("rust-patterns", 0.8, 0.),
            ("development-tools::cargo-plugins", 0.9, 0.), ("development-tools::build-utils", 0.8, 0.), ("emulators", 0.6, 0.), ("text-processing", 0.8, 0.)]),
        (Cond::Any(&["term", "terminal-emulator", "xterm", "console", "isatty", "readline", "repl", "getopts", "readline-implementation"]),
            &[("command-line-interface", 1.2, 0.1), ("multimedia::images", 0.1, 0.), ("multimedia", 0.4, 0.), ("config", 0.9, 0.), ("no-std", 0.9, 0.), ("wasm", 0.9, 0.),
            ("science::math", 0.8, 0.), ("hardware-support", 0.7, 0.), ("command-line-utilities", 0.75, 0.), ("internationalization", 0.9, 0.), ("parsing", 0.5, 0.),
            ("development-tools::procedural-macro-helpers", 0.7, 0.), ("memory-management", 0.5, 0.), ("rendering::engine", 0.8, 0.), ("emulators", 0.6, 0.)]),
        (Cond::Any(&["has:bin"]), &[
            ("command-line-utilities", 1.1, 0.1), ("command-line-interface", 0.8, 0.),
            ("development-tools::cargo-plugins", 0.9, 0.), ("no-std", 0.7, 0.), ("game-development", 0.9, 0.),
            ("development-tools::procedural-macro-helpers", 0.7, 0.), ("memory-management", 0.4, 0.), ("parsing", 0.8, 0.), ("os", 0.9, 0.),
            ("os::windows-apis", 0.8, 0.), ("algorithms", 0.5, 0.), ("rust-patterns", 0.75, 0.)]),

        (Cond::Any(&["hardware", "verilog", "bluetooth", "fpga", "xilinx", "system-verilog", "rs232","enclave", "eeprom", "arduino", "adafruit", "laser", "altimeter", "sensor", "tp-link"]),
                &[("hardware-support", 1.2, 0.3), ("command-line-utilities", 0.7, 0.), ("finance", 0.8, 0.), ("multimedia::video", 0.7, 0.), ("caching", 0.9, 0.), ("multimedia::images", 0.6, 0.),
                ("multimedia", 0.8, 0.), ("parsing", 0.5, 0.), ("os", 0.9, 0.), ("development-tools::testing", 0.8, 0.), ("email", 0.7, 0.), ("development-tools::procedural-macro-helpers", 0.6, 0.),
                ("development-tools", 0.9, 0.), ("development-tools::build-utils", 0.8, 0.)]),
        (Cond::Any(&["cpuid", "tpu", "acpi", "uefi", "bluez", "simd", "sgx", "raspberry", "rp2040"]),
                &[("hardware-support", 1.2, 0.3), ("command-line-utilities", 0.7, 0.), ("multimedia::images", 0.6, 0.), ("email", 0.7, 0.), ("os", 0.9, 0.), ("wasm", 0.8, 0.),
                 ("development-tools", 0.8, 0.), ("development-tools::testing", 0.8, 0.), ("parsing", 0.5, 0.), ("asynchronous", 0.6, 0.),
                 ("development-tools::procedural-macro-helpers", 0.6, 0.), ("development-tools", 0.9, 0.)]),
        (Cond::Any(&["sse3", "ssse3", "avx2", "avx512", "riscv", "sifive", "risc-v", "cpuinfo"]), &[("hardware-support", 1.2, 0.1)]),
        (Cond::Any(&["accelerometer", "magnetometer", "thermometer", "gyroscope"]), &[("hardware-support", 1.2, 0.1), ("parsing", 0.5, 0.)]),
        (Cond::Any(&["mems"]), &[("embedded", 1.2, 0.1)]),
        (Cond::Any(&["firmware", "raspberrypi", "broadcom", "serial-port", "infineon", "hidapi", "rfcomm", "zigbee", "usb", "rfid", "usb-c", "hdmi", "microbit", "xhci", "apdu", "scsi", "hdd", "embedded-hal"]),
                &[("hardware-support", 1.2, 0.3), ("embedded", 1.1, 0.), ("no-std", 0.8, 0.), ("encoding", 0.9, 0.), ("command-line-utilities", 0.7, 0.),
                ("compression", 0.7, 0.), ("multimedia::images", 0.6, 0.), ("os", 0.85, 0.), ("development-tools", 0.8, 0.), ("caching", 0.9, 0.),
                ("development-tools::testing", 0.8, 0.), ("parsing", 0.6, 0.), ("development-tools::procedural-macro-helpers", 0.6, 0.),
                ("development-tools", 0.9, 0.)]),
        (Cond::Any(&["hal", "keyboard", "gamepad", "keypad", "joystick", "mouse", "enclave", "driver", "device", "device-drivers", "hardware-abstraction-layer", "embedded-hal-driver"]),
                &[("hardware-support", 1.2, 0.3), ("command-line-utilities", 0.8, 0.), ("multimedia::images", 0.5, 0.), ("compression", 0.8, 0.), ("filesystem", 0.8, 0.), ("science::robotics", 0.9, 0.), ("development-tools::testing", 0.8, 0.),
                ("development-tools::procedural-macro-helpers", 0.6, 0.), ("development-tools", 0.9, 0.), ("rendering::data-formats", 0.5, 0.)]),
        (Cond::Any(&["adafruit", "ardruino", "raspberry-pi", "gpio", "texas-instruments", "sensors", "svd2rust"]),
            &[("hardware-support", 1.3, 0.1), ("embedded", 1.3, 0.1), ("date-and-time", 0.6, 0.)]),
        (Cond::Any(&["dep:cortex-m-rt", "dep:atsamd-hal", "dep:bare-metal", "dep:ndless", "dep:embedded-ffi", "modbus", "industrial-automation", "atsame70q20",
            "ambient-light-sensor", "esp32", "esp-idf", "nrf52"]),
            &[("hardware-support", 1.2, 0.1), ("embedded", 1.2, 0.1), ("date-and-time", 0.9, 0.)]),
        (Cond::All(&["hue", "light"]), &[("hardware-support", 1.2, 0.3), ("multimedia::audio", 0.5, 0.), ("no-std", 0.9, 0.)]),
        (Cond::Any(&["home-assistant", "homeassistant"]), &[("hardware-support", 1.1, 0.05), ("web-programming::websocket", 0.5, 0.)]),
        (Cond::Any(&["plc", "ferrous-systems", "automotive", "autosar"]), &[("hardware-support", 1.1, 0.05), ("embedded", 1.1, 0.05)]),
        (Cond::Any(&["google"]), &[("hardware-support", 0.8, 0.), ("embedded", 0.7, 0.), ("parsing", 0.7, 0.)]),
        (Cond::Any(&["libusb", "dep:libusb1-sys", "dep:usb-device", "nl80211", "yubikey"]), &[("hardware-support", 1.2, 0.1), ("compilers", 0.5, 0.), ("config", 0.9, 0.), ("caching", 0.9, 0.), ("parsing", 0.7, 0.), ("date-and-time", 0.7, 0.), ("rust-patterns", 0.75, 0.)]),
        (Cond::Any(&["controlling", "serial", "uart", "bootloader", "com-port", "bios", "led-controller"]),
            &[("hardware-support", 1.1, 0.), ("embedded", 1.1, 0.), ("multimedia::video", 0.8, 0.)]),
        (Cond::Any(&["osdev", "acpi", "battery"]), &[("hardware-support", 1.2, 0.1), ("os", 1.2, 0.1)]),
        (Cond::All(&["teledildonics"]), &[("hardware-support", 1.3, 0.2)]),
        (Cond::All(&["hue", "philips"]), &[("hardware-support", 1.2, 0.3), ("no-std", 0.9, 0.)]),
        (Cond::Any(&["camera", "vesa", "ddcci", "ddc"]), &[("hardware-support", 1.1, 0.2), ("multimedia::images", 1.2, 0.1), ("caching", 0.9, 0.), ("no-std", 0.9, 0.),
            ("parsing", 0.5, 0.), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::Any(&["low-level"]), &[("hardware-support", 1.1, 0.), ("multimedia", 0.8, 0.)]),
        (Cond::Any(&["vga"]), &[("hardware-support", 1.1, 0.1), ("embedded", 1.1, 0.)]),

        (Cond::Any(&["robotics", "roborio", "urdf", "rosrust", "dep:arci"]), &[("science::robotics", 1.3, 0.4), ("finance", 0.5, 0.), ("email", 0.7, 0.), ("caching", 0.9, 0.), ("hardware-support", 1.1, 0.), ("parsing", 0.7, 0.), ("embedded", 1.1, 0.)]),
        (Cond::Any(&["self-driving", "drone", "ros", "ros2", "openrr"]), &[("science::robotics", 1.2, 0.2), ("finance", 0.5, 0.), ("hardware-support", 1.1, 0.)]),
        (Cond::Any(&["aerospace"]), &[("science::robotics", 1.2, 0.2), ("science", 1.2, 0.1)]),
        (Cond::Any(&["scientific-computing"]), &[("science", 1.2, 0.1)]),
        (Cond::Any(&["autonomous", "autonomos", "robots", "robot", "robotic", "photogrammetry", "slam"]), &[("science::robotics", 1.1, 0.1), ("parsing", 0.5, 0.)]),
        (Cond::Any(&["kinematics", "kalman"]), &[("science::robotics", 1.1, 0.05), ("simulation", 1.1, 0.05), ("science::math", 1.1, 0.1),  ("algorithms", 1.1, 0.1), ("parsing", 0.7, 0.), ("compilers", 0.5, 0.)]),
        (Cond::Any(&["adafruit", "servomotor", "servo-motor", "motor", "cnc", "stepper", "motor-controller"]), &[("science::robotics", 1.1, 0.), ("hardware-support", 1.1, 0.05), ("parsing", 0.5, 0.)]),

        (Cond::Any(&["robotstxt", "robots-txt", "crawler", "web-bot", "sitemap", "scraper"]),
            &[("web-programming", 1.2, 0.1), ("parsing", 0.5, 0.), ("science::robotics", 0.3, 0.), ("hardware-support", 0.8, 0.), ("accessibility", 0.8, 0.)]),
        (Cond::Any(&["spider", "url", "svg", "pgp", "gamepad", "interpreter", "ssl", "irc", "web", "robot36", "actor-framework"]), &[("science::robotics", 0.7, 0.), ("parsing", 0.8, 0.)]),

        (Cond::Any(&["microcontrollers", "avr", "nickel", "crt0", "bare-metal", "micropython", "6502", "sgx", "embedded", "embedded-hal-driver"]),
            &[("embedded", 1.3, 0.25), ("no-std", 0.9, 0.), ("wasm", 0.7, 0.), ("finance", 0.5, 0.), ("parsing", 0.7, 0.), ("multimedia", 0.8, 0.), ("encoding", 0.9, 0.),
            ("multimedia::video", 0.8, 0.), ("compilers", 0.7, 0.), ("development-tools", 0.8, 0.), ("web-programming", 0.7, 0.)]),
        (Cond::All(&["metal", "bare"]), &[("embedded", 1.3, 0.2), ("os", 0.9, 0.), ("no-std", 0.9, 0.)]),
        (Cond::Any(&["iot"]), &[("embedded", 1.2, 0.1), ("network-programming", 1.1, 0.), ("hardware-support", 1.2, 0.), ("web-programming::websocket", 0.9, 0.)]),
        (Cond::All(&["pid", "control"]), &[("embedded", 1.2, 0.1), ("hardware-support", 1.2, 0.1), ("no-std", 0.9, 0.)]),
        (Cond::All(&["pid", "controler"]), &[("embedded", 1.2, 0.1), ("hardware-support", 1.2, 0.1)]),
        (Cond::All(&["pid", "processes"]), &[("os::unix-apis", 1.1, 0.1), ("multimedia::audio", 0.5, 0.)]),
        (Cond::All(&["pid", "process"]), &[("os::unix-apis", 1.1, 0.1), ("multimedia::audio", 0.5, 0.)]),

        (Cond::Any(&["game", "json", "simulation", "turtle"]), &[("rendering::engine", 0.7, 0.), ("multimedia", 0.8, 0.), ("database", 0.9, 0.), ("internationalization", 0.8, 0.), ("asynchronous", 0.9, 0.)]),
        (Cond::Any(&["game", "games", "videogame", "videogames", "video-game", "video-games"]),
            &[("games", 1.25, 0.2), ("science::math", 0.6, 0.), ("wasm", 0.8, 0.), ("encoding", 0.9, 0.), ("science::ml", 0.7, 0.), ("caching", 0.9, 0.), ("development-tools::cargo-plugins", 0.7, 0.),
            ("rendering::engine", 0.8, 0.), ("embedded", 0.75, 0.), ("filesystem", 0.5, 0.), ("database-implementations", 0.75, 0.), ("web-programming::http-client", 0.5, 0.),
            ("internationalization", 0.7, 0.), ("multimedia::video", 0.8, 0.), ("date-and-time", 0.3, 0.), ("text-editors", 0.6, 0.), ("development-tools::procedural-macro-helpers", 0.6, 0.)]),
        (Cond::Any(&["rocket-league", "league-of-legends", "leagueoflegends", "riot-games", "runeterra", "nintendo", "conway", "pokemon", "starcraft", "quake2", "quake3", "deathmatch", "speedrun",
            "roguelike", "roguelikes", "runescape", "rimworld", "minecraft-server", "kerbal-space-program", "minecraft", "guild-wars-2", "oblivion", "morrowind", "fallout-4", "skyrim", "roblox", "tic-tac-toe", "sudoku"]),
            &[("games", 1.25, 0.3), ("rendering::engine", 0.8, 0.), ("wasm", 0.8, 0.), ("data-structures", 0.6, 0.), ("algorithms", 0.6, 0.), ("os::windows-apis", 0.6, 0.), ("simulation", 0.9, 0.),
            ("science::robotics", 0.9, 0.), ("internationalization", 0.8, 0.), ("caching", 0.5, 0.), ("parser-implementations", 0.8, 0.), ("compilers", 0.8, 0.), ("development-tools::build-utils", 0.6, 0.),
            ("command-line-interface", 0.8, 0.), ("cryptography", 0.5, 0.), ("command-line-utilities", 0.75, 0.)]),
        (Cond::Any(&["fun", "fun-game", "osu", "quake", "doom", "play", "playdate", "steam", "steamworks", "steam-games", "steam-deck", "asteroids", "twitch", "shooter", "game-of-life"]),
            &[("games", 1.2, 0.1), ("parsing", 0.7, 0.), ("date-and-time", 0.7, 0.)]),

        (Cond::Any(&["vector", "openai", "client"]), &[("games", 0.7, 0.), ("emulators", 0.8, 0.), ("config", 0.8, 0.), ("multimedia::video", 0.9, 0.), ("development-tools::cargo-plugins", 0.7, 0.)]),
        (Cond::NotAny(&["gamedev", "game", "bevy", "openvr", "steam", "playdate", "valve", "godot", "unity", "unreal", "canvas", "texture", "player", "sprite", "dep:bevy", "games", "sdl", "ecs", "specs", "game-dev", "dep:allegro", "dice", "bounding", "library", "3d",
            "utils", "format", "polygon", "amethyst", "piston", "poker", "chess", "board", "ai", "editor", "mod", "maps", "camera", "particles", "gamepad", "sdk", "game-server", "gameserver"]),
            &[("game-development", 0.6, 0.)]),
        (Cond::NotAny(&["game", "games", "gamedev", "dep:bevy", "unity", "valve", "poker", "unreal", "canvas", "texture", "rimworld", "minecraft", "roblox", "bevy", "game-dev", "tetris", "conway", "level",
            "kerbal-space-program", "dice", "roguelike", "tic-tac-toe", "3d", "2d", "board-game", "save",
            "fantasy", "rpg", "rts", "play", "fun", "xbox", "gamepad", "voxel", "marching-cubes", "puzzle", "toy", "cards", "mods", "sudoku", "puzzle", "bounding", "chess", "amethyst", "game-server", "piston"]),
            &[("game-development", 0.8, 0.), ("games", 0.8, 0.)]),
        (Cond::All(&["imag"]), &[("game-development", 0.8, 0.), ("development-tools::build-utils", 0.8, 0.)]),
        (Cond::Any(&["event-loop", "eventloop"]), &[("game-development", 1.1, 0.), ("games", 0.6, 0.), ("development-tools::build-utils", 0.8, 0.)]),
        (Cond::Any(&["gamedev", "gamedev-tool", "game-dev", "game-development", "bevy-plugin", "ecs-framework", "vulkan-game-engine", "game-engine-3d", "game-framework"]),
            &[("game-development", 1.3, 0.2), ("games", 0.25, 0.), ("finance", 0.5, 0.), ("science::geo", 0.7, 0.), ("caching", 0.9, 0.), ("science", 0.5, 0.),
            ("concurrency", 0.75, 0.), ("compilers", 0.5, 0.), ("science::ml", 0.8, 0.), ("science::math", 0.9, 0.), ("parsing", 0.6, 0.),
            ("multimedia::video", 0.75, 0.), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.)]),
        (Cond::Any(&["openvr", "steamvr", "gaming", "cli-game", "game-hacking"]), &[("games", 1.2, 0.1), ("email", 0.7, 0.), ("embedded", 0.9, 0.), ("game-development", 1.2, 0.1), ("rust-patterns", 0.75, 0.), ("date-and-time", 0.6, 0.), ("parsing", 0.5, 0.)]),
        (Cond::All(&["game", "2d"]), &[("games", 1.1, 0.1), ("game-development", 1.1, 0.1), ("encoding", 0.9, 0.)]),
        (Cond::All(&["game", "3d"]), &[("games", 1.1, 0.1), ("game-development", 1.1, 0.1)]),
        (Cond::Any(&["2d-game", "3d-game", "strategy-game", "modding"]), &[("games", 1.1, 0.1), ("game-development", 1.1, 0.1)]),
        (Cond::All(&["game", "video"]), &[("multimedia::video", 0.5, 0.), ("multimedia", 0.8, 0.), ("parsing", 0.2, 0.)]),
        (Cond::All(&["game", "2048"]), &[("games", 1.2, 0.)]),
        (Cond::All(&["gamedev", "engine"]), &[("game-development", 1.5, 0.4), ("games", 0.1, 0.), ("multimedia::video", 0.5, 0.), ("concurrency", 0.5, 0.),
            ("development-tools::build-utils", 0.8, 0.), ("development-tools::cargo-plugins", 0.7, 0.), ("rendering::data-formats", 0.8, 0.)]),
        (Cond::All(&["entity-component", "system"]), &[("game-development", 1.5, 0.4), ("games", 0.7, 0.), ("concurrency", 0.7, 0.)]),
        (Cond::All(&["games", "framework"]), &[("game-development", 1.3, 0.2), ("games", 0.6, 0.), ("gui", 0.8, 0.)]),
        (Cond::All(&["gamedev", "ecs"]), &[("game-development", 1.5, 0.4), ("games", 0.1, 0.), ("concurrency", 0.7, 0.), ("caching", 0.9, 0.)]),
        (Cond::All(&["game", "library"]), &[("game-development", 1.3, 0.2), ("games", 0.6, 0.), ("gui", 0.8, 0.)]),
        (Cond::All(&["game", "ecs"]), &[("game-development", 1.2, 0.), ("games", 0.4, 0.)]),
        (Cond::Any(&["roblox"]), &[("game-development", 1.3, 0.2), ("games", 1.2, 0.1)]),
        (Cond::All(&["game", "parser"]), &[("game-development", 1.1, 0.), ("rendering::engine", 0.1, 0.), ("games", 0.2, 0.), ("gui", 0.5, 0.)]),
        (Cond::All(&["chess", "engine"]), &[("game-development", 1.5, 0.3), ("rendering::engine", 0.4, 0.), ("games", 0.4, 0.)]),
        (Cond::All(&["game", "scripting"]), &[("game-development", 1.5, 0.3), ("rendering::engine", 0.4, 0.), ("games", 0.5, 0.), ("rendering::data-formats", 0.2, 0.)]),
        (Cond::All(&["game", "editor"]), &[("game-development", 1.3, 0.1), ("rendering::engine", 0.4, 0.), ("games", 0.8, 0.), ("rendering::engine", 0.2, 0.)]),
        (Cond::All(&["game", "graphics"]), &[("game-development", 1.3, 0.), ("games", 0.8, 0.), ("data-structures", 0.6, 0.)]),
        (Cond::All(&["game", "piston"]), &[("game-development", 1.2, 0.1), ("games", 1.19, 0.1)]),
        (Cond::Any(&["gamepad", "joystick"]), &[("game-development", 1.2, 0.), ("games", 1.2, 0.), ("hardware-support", 1.1, 0.1)]),
        (Cond::All(&["save", "files"]), &[("game-development", 1.1, 0.0), ("games", 1.1, 0.05)]),
        (Cond::Any(&["piston", "nintendo", "rolling-dice"]), &[("game-development", 1.1, 0.1), ("games", 1.1, 0.08)]),
        (Cond::Any(&["engine", "godot", "unity", "unreal", "amethyst", "box2d"]), &[("game-development", 1.3, 0.1), ("games", 0.7, 0.)]),
        (Cond::Any(&["bevy", "bevy-engine", "bevy-ui", "dep:bevy", "fyrox", "ggez", "player-object"]), &[("game-development", 1.3, 0.3), ("games", 1.1, 0.08), ("date-and-time", 0.9, 0.)]),
        (Cond::Any(&["game-engine", "game-server", "game-engines", "2d-game-engine", "ecs", "game-loop", "game-development", "entity-component-system"]),
            &[("game-development", 1.5, 0.2), ("rendering::engine", 0.8, 0.), ("games", 0.95, 0.), ("command-line-utilities", 0.75, 0.), ("caching", 0.9, 0.), ("web-programming::websocket", 0.9, 0.), ("rendering::data-formats", 0.2, 0.)]),
        (Cond::All(&["game", "engine"]), &[("game-development", 1.5, 0.3), ("games", 0.3, 0.), ("rendering::data-formats", 0.2, 0.), ("filesystem", 0.7, 0.), ("no-std", 0.8, 0.), ("filesystem", 0.8, 0.), ("command-line-interface", 0.8, 0.)]),
        (Cond::All(&["game", "development"]), &[("game-development", 1.3, 0.2), ("games", 0.3, 0.)]),
        (Cond::All(&["game", "dev"]), &[("game-development", 1.2, 0.), ("games", 0.9, 0.)]),
        (Cond::Any(&["xbox", "xbox360", "kinect"]), &[("games", 1.1, 0.), ("game-development", 1.1, 0.), ("science::geo", 0.7, 0.), ("development-tools::build-utils", 0.8, 0.), ("parsing", 0.5, 0.)]),
        (Cond::Any(&["wordle", "sudoku"]), &[("games", 1.2, 0.2), ("game-development", 1.1, 0.), ("algorithms", 1.1, 0.)]),
        (Cond::All(&["rpg", "game"]), &[("games", 1.2, 0.2), ("game-development", 1.1, 0.)]),
        (Cond::All(&["rts", "game"]), &[("games", 1.2, 0.2), ("game-development", 1.1, 0.)]),
        (Cond::All(&["voxel", "game"]), &[("games", 1.2, 0.2), ("parsing", 0.6, 0.), ("game-development", 1.1, 0.), ("algorithms", 0.7, 0.)]),
        (Cond::Any(&["unreal-engine"]), &[("games", 1.2, 0.), ("game-development", 1.1, 0.), ("rust-patterns", 0.75, 0.)]),
        (Cond::All(&["unreal", "engine"]), &[("games", 1.2, 0.), ("game-development", 1.2, 0.1)]),
        (Cond::Any(&["boundingbox", "bounding-box", "aabb"]), &[("game-development", 1.1, 0.), ("rendering::engine", 0.8, 0.)]),
        (Cond::Any(&["texture", "fps", "gamepad"]), &[("game-development", 1.2, 0.1), ("parsing", 0.5, 0.), ("algorithms", 0.7, 0.)]),
        (Cond::All(&["rendering", "engine"]), &[("rendering::engine", 1.5, 0.3), ("rendering::data-formats", 0.2, 0.)]),
        (Cond::Any(&["storage", "gluster", "glusterfs"]), &[("filesystem", 1.2, 0.1), ("database", 1.2, 0.), ("game-development", 0.5, 0.), ("visualization", 0.6, 0.), ("rendering::engine", 0.2, 0.), ("rendering::data-formats", 0.2, 0.)]),
        (Cond::NotAny(&["has:bin"]), &[("games", 0.6, 0.), ("development-tools::cargo-plugins", 0.5, 0.), ("command-line-utilities", 0.1, 0.)]),
        (Cond::All(&["game", "has:bin"]), &[("games", 1.2, 0.1), ("command-line-utilities", 0.5, 0.)]),
        (Cond::All(&["game", "play"]), &[("games", 1.2, 0.1)]),
        (Cond::Any(&["piston", "uuid", "scheduler", "countdown", "sleep"]), &[("development-tools::profiling", 0.2, 0.), ("algorithms", 0.8, 0.)]),
        (Cond::Any(&["timer"]), &[("development-tools::profiling", 0.8, 0.)]),
        (Cond::Any(&["mastercard", "master-card"]), &[("games", 0.5, 0.), ("filesystem", 0.5, 0.), ("parsing", 0.5, 0.)]),
        (Cond::All(&["card", "number"]), &[("games", 0.8, 0.), ("filesystem", 0.5, 0.)]),
        (Cond::All(&["validate", "numbers"]), &[("games", 0.3, 0.)]),
        (Cond::Any(&["admob"]), &[("games", 0.5, 0.), ("game-development", 0.5, 0.)]),
        (Cond::Any(&["games", "game"]), &[("gui", 0.7, 0.), ("development-tools", 0.7, 0.)]),

        (Cond::Any(&["specs", "ecs", "http", "spider", "crawler"]),
            &[("command-line-utilities", 0.75, 0.), ("parsing", 0.9, 0.), ("algorithms", 0.8, 0.), ("text-processing", 0.9, 0.), ("multimedia::video", 0.8, 0.), ("compilers", 0.5, 0.)]),
        (Cond::Any(&["spider", "crawler", "downloader", "file-downloader"]),
            &[("web-programming::http-client", 1.2, 0.05), ("web-programming::http-server", 0.75, 0.), ("parsing", 0.5, 0.), ("compression", 0.5, 0.), ("algorithms", 0.7, 0.)]),
        (Cond::Any(&["documentation"]), &[("rendering::data-formats", 0.2, 0.)]),

        (Cond::NotAny(&["file", "path", "file-system", "ntfs", "zfs", "directory-tree", "io", "fs", "ext4", "ext3", "directory", "directories", "dir", "fdisk", "folder", "basedir", "xdg",
            "gluster", "nfs", "samba", "disk", "xattr", "subdirectories",
            "ionotify", "inode", "filesystem", "fuse", "temporary-files", "temp-files", "tempfile"]),
            &[("filesystem", 0.7, 0.)]),
        (Cond::Any(&["basedir", "xdg", "ext4", "ntfs", "ext3", "nfs", "samba", "disk", "directory-tree", "temporary-files", "temp-files", "tempfile", "gluster"]),
             &[("filesystem", 1.25, 0.3), ("command-line-interface", 0.3, 0.), ("no-std", 0.5, 0.), ("parsing", 0.9, 0.), ("memory-management", 0.8, 0.),
             ("os", 0.95, 0.), ("gui", 0.9, 0.), ("science", 0.8, 0.), ("parsing", 0.6, 0.), ("date-and-time", 0.8, 0.), ("science::math", 0.3, 0.), ("development-tools", 0.95, 0.), ("cryptography", 0.6, 0.),
             ("asynchronous", 0.8, 0.), ("algorithms", 0.7, 0.), ("development-tools::testing", 0.9, 0.), ("command-line-utilities", 0.75, 0.)]),
        (Cond::Any(&["backups", "backup", "path-manipulation", "directories", "dir", "filesystem", "fdisk"]),
             &[("filesystem", 1.25, 0.2), ("command-line-interface", 0.4, 0.), ("no-std", 0.5, 0.), ("os", 0.95, 0.), ("gui", 0.8, 0.),
             ("science", 0.8, 0.), ("science::math", 0.5, 0.), ("development-tools", 0.95, 0.), ("cryptography", 0.6, 0.), ("asynchronous", 0.9, 0.),
             ("concurrency", 0.8, 0.), ("algorithms", 0.8, 0.), ("multimedia::video", 0.8, 0.), ("development-tools::testing", 0.9, 0.), ("command-line-utilities", 0.8, 0.)]),
        (Cond::Any(&["xattr", "ionotify", "zfs", "libzfs", "inode", "temp-file", "file-system", "flock", "fuse", "fseventsd"]),
             &[("filesystem", 1.25, 0.3), ("command-line-interface", 0.3, 0.), ("no-std", 0.5, 0.), ("os", 0.95, 0.), ("database-implementations", 0.9, 0.), ("gui", 0.9, 0.),
             ("science", 0.8, 0.), ("science::math", 0.3, 0.), ("development-tools", 0.95, 0.), ("cryptography", 0.6, 0.), ("asynchronous", 0.8, 0.),
             ("concurrency", 0.8, 0.), ("algorithms", 0.8, 0.), ("multimedia::video", 0.6, 0.), ("encoding", 0.7, 0.), ("development-tools::testing", 0.9, 0.), ("command-line-utilities", 0.8, 0.)]),
        (Cond::Any(&["file", "vfs"]), &[("filesystem", 1.1, 0.), ("memory-management", 0.7, 0.), ("no-std", 0.8, 0.)]),
        (Cond::All(&["fuse", "filesystem"]), &[("filesystem", 1.3, 0.3), ("os::unix-apis", 1.1, 0.1), ("encoding", 0.3, 0.)]),
        (Cond::All(&["file", "path"]), &[("filesystem", 1.1, 0.)]),
        (Cond::All(&["file", "system"]), &[("filesystem", 1.2, 0.)]),
        (Cond::Any(&["path", "files", "vfs", "glob"]),
             &[("filesystem", 1.2, 0.2), ("concurrency", 0.8, 0.), ("command-line-interface", 0.7, 0.), ("no-std", 0.6, 0.), ("cryptography", 0.6, 0.), ("development-tools::testing", 0.9, 0.), ("command-line-utilities", 0.8, 0.)]),
        (Cond::All(&["disk", "image"]),
             &[("filesystem", 1.3, 0.1), ("os", 1.3, 0.1), ("multimedia::images", 0.01, 0.), ("compilers", 0.5, 0.)]),
        (Cond::All(&["file", "metadata"]),
             &[("filesystem", 1.2, 0.1), ("os", 1.1, 0.)]),
        (Cond::Any(&["subdirectories", "walk-dir", "rename-files", "filesystem"]),
             &[("filesystem", 1.15, 0.1), ("gui", 0.9, 0.)]),
        (Cond::Any(&["keyboard-layout", "hid"]), &[("filesystem", 0.8, 0.)]),

        (Cond::Any(&["consistent", "passphrase"]), &[("algorithms", 1.15, 0.05), ("cryptography", 1.05, 0.)]),
        (Cond::Any(&["encryption", "e2e", "end-to-end", "e2ee", "keygen", "decryption", "data-decryption", "password", "data-encryption", "encryption-algorithm"]), &[("cryptography", 1.25, 0.2), ("compilers", 0.5, 0.)]),
        (Cond::Any(&["password", "passwords", "password-manager", "password-strength", "password-generator"]), &[("authentication", 1.25, 0.2), ("caching", 0.9, 0.)]),
        (Cond::Any(&["overhead", "byte", "zero-copy"]), &[("algorithms", 1.05, 0.), ("memory-management", 1.02, 0.), ("games", 0.8, 0.)]),
        (Cond::Any(&["buffer", "buffered", "ringbuffer", "circular-queue", "ring-buffer", "clone-on-write"]),
            &[("algorithms", 1.25, 0.2), ("memory-management", 1.25, 0.), ("caching", 1.2, 0.), ("network-programming", 0.25, 0.)]),
        (Cond::NotAny(&["intern", "interning", "write-once", "lru", "proxy", "memoize", "memoization", "memcached", "cache", "caching", "memory-cache", "cached"]),
            &[("caching", 0.6, 0.)]),
        (Cond::Any(&["concurrent-cache", "fast-concurrent-cache", "caching"]),
            &[("caching", 1.3, 0.2), ("memory-management", 1.1, 0.), ("data-structures", 0.7, 0.), ("date-and-time", 0.7, 0.),
             ("embedded", 0.9, 0.), ("cryptography", 0.6, 0.), ("encoding", 0.8, 0.), ("algorithms", 0.7, 0.)]),
        (Cond::Any(&["memcached", "cache", "eviction", "memory-cache", "in-memory-caching"]),
            &[("caching", 1.3, 0.2), ("memory-management", 1.1, 0.), ("visualization", 0.9, 0.), ("data-structures", 0.7, 0.), ("date-and-time", 0.7, 0.),
             ("embedded", 0.9, 0.), ("cryptography", 0.6, 0.), ("database-implementations", 0.8, 0.), ("encoding", 0.8, 0.), ("concurrency", 0.8, 0.), ("algorithms", 0.7, 0.)]),
        (Cond::Any(&["lru"]),
            &[("caching", 1.3, 0.2), ("memory-management", 1.1, 0.), ("date-and-time", 0.7, 0.), ("algorithms", 0.8, 0.), ("embedded", 0.9, 0.)]),
        (Cond::All(&["memory", "cache"]), &[("caching", 1.2, 0.1), ("memory-management", 1.1, 0.), ("os::windows-apis", 0.7, 0.), ("database-implementations", 0.8, 0.)]),
        (Cond::All(&["lookup"]), &[("caching", 1.2, 0.)]),
        (Cond::Any(&["allocate", "allocates", "deallocate", "alloc", "mmap", "garbage-collector"]),
            &[("memory-management", 1.3, 0.2), ("caching", 0.8, 0.), ("encoding", 0.8, 0.), ("algorithms", 0.8, 0.), ("game-development", 0.7, 0.), ("development-tools", 0.8, 0.)]),
        (Cond::Any(&["allocator", "alloc", "slab", "memory-allocator"]),
            &[("memory-management", 1.3, 0.2), ("caching", 0.8, 0.), ("no-std", 0.8, 0.), ("database", 0.8, 0.), ("algorithms", 0.8, 0.), ("game-development", 0.7, 0.), ("development-tools", 0.8, 0.)]),
        (Cond::Any(&["garbage", "reclamation", "gc", "refcell", "garbage-collection"]), &[
            ("memory-management", 1.3, 0.2), ("data-structures", 0.9, 0.), ("authentication", 0.8, 0.),
            ("science::math", 0.7, 0.), ("rendering::graphics-api", 0.8, 0.), ("concurrency", 0.9, 0.),
            ("encoding", 0.7, 0.), ("development-tools::cargo-plugins", 0.8, 0.), ("development-tools::build-utils", 0.8, 0.), ("internationalization", 0.7, 0.)]),
        (Cond::Any(&["rc", "process-memory", "memory", "oom", "malloc", "heap"]), &[
            ("memory-management", 1.25, 0.1), ("os", 1.1, 0.), ("data-structures", 0.8, 0.), ("authentication", 0.8, 0.),
            ("science::math", 0.8, 0.), ("os", 1.1, 0.), ("rendering::graphics-api", 0.8, 0.), ("concurrency", 0.8, 0.),
            ("encoding", 0.8, 0.), ("development-tools::cargo-plugins", 0.8, 0.), ("development-tools::build-utils", 0.8, 0.), ("internationalization", 0.7, 0.)]),
        (Cond::All(&["memory", "allocation"]), &[("memory-management", 1.25, 0.1), ("encoding", 0.8, 0.)]),
        (Cond::All(&["garbage", "collector"]), &[("memory-management", 1.25, 0.1), ("encoding", 0.8, 0.), ("science::math", 0.8, 0.), ("rendering::graphics-api", 0.8, 0.), ("concurrency", 0.9, 0.)]),
        (Cond::All(&["memory", "pool"]), &[("memory-management", 1.25, 0.1), ("encoding", 0.8, 0.), ("rendering::graphics-api", 0.8, 0.), ("os::windows-apis", 0.7, 0.), ("concurrency", 0.9, 0.)]),
        (Cond::Any(&["object-pool", "memory-pool", "mempool"]), &[("memory-management", 1.25, 0.1), ("encoding", 0.8, 0.), ("concurrency", 0.9, 0.), ("algorithms", 0.7, 0.)]),
        (Cond::Any(&["serde-json", "hashing", "tokio", "web-api", "database", "spsc", "queue"]), &[("memory-management", 0.7, 0.)]),
        (Cond::Any(&["reference-counting"]), &[("memory-management", 1.1, 0.), ("data-structures", 1.1, 0.), ("rust-patterns", 1.1, 0.)]),

        (Cond::All(&["vector", "clock"]), &[("games", 0.25, 0.), ("algorithms", 1.25, 0.1), ("date-and-time", 0.3, 0.), ("compilers", 0.5, 0.)]),
        (Cond::All(&["vector", "tree"]), &[("data-structures", 1.2, 0.1)]),
        (Cond::Any(&["vectorclock", "raft-consensus"]), &[("games", 0.25, 0.), ("algorithms", 1.5, 0.2), ("date-and-time", 0.3, 0.)]),
        (Cond::Any(&["phf"]), &[("date-and-time", 0.4, 0.)]),

        (Cond::Any(&["utility", "utilities", "ripgrep", "tools"]),
            &[("command-line-utilities", 1.1, 0.2), ("internationalization", 0.8, 0.), ("algorithms", 0.6, 0.), ("games", 0.01, 0.), ("filesystem", 0.8, 0.),
            ("rendering::engine", 0.6, 0.), ("science", 0.9, 0.), ("simulation", 0.75, 0.), ("os::windows-apis", 0.7, 0.), ("data-structures", 0.6, 0.)]),
        (Cond::All(&["cli", "utility"]),
            &[("command-line-utilities", 1.3, 0.3), ("command-line-interface", 0.3, 0.), ("data-structures", 0.6, 0.), ("rust-patterns", 0.7, 0.), ("algorithms", 0.8, 0.), ("parsing", 0.5, 0.),
            ("games", 0.1, 0.), ("filesystem", 0.6, 0.), ("science", 0.8, 0.)]),
        (Cond::All(&["cli", "tool"]),
            &[("command-line-utilities", 1.3, 0.3), ("command-line-interface", 0.3, 0.), ("data-structures", 0.6, 0.), ("os::windows-apis", 0.7, 0.), ("rust-patterns", 0.7, 0.), ("filesystem", 0.8, 0.), ("science", 0.8, 0.)]),
        (Cond::All(&["bash", "tool"]), &[("command-line-utilities", 1.2, 0.2), ("os::windows-apis", 0.7, 0.)]),
        (Cond::All(&["commandline", "tool"]), &[("command-line-utilities", 1.2, 0.2)]),
        (Cond::All(&["cli", "command"]), &[("command-line-utilities", 1.1, 0.1)]),
        (Cond::Any(&["bash", "shell", "shell-script"]),
            &[("command-line-utilities", 1.05, 0.025), ("asynchronous", 0.9, 0.), ("os", 1.1, 0.), ("rust-patterns", 0.9, 0.), ("os::windows-apis", 0.8, 0.)]),
        (Cond::Any(&["command", "tool", "util"]),
            &[("command-line-utilities", 1.05, 0.025), ("asynchronous", 0.9, 0.), ("rust-patterns", 0.9, 0.), ("os::windows-apis", 0.8, 0.)]),
        (Cond::Any(&["dep:colored", "dep:ansi_term"]), &[("command-line-utilities", 1.1, 0.), ("parsing", 0.7, 0.), ("caching", 0.9, 0.), ("command-line-interface", 1.05, 0.)]),
        (Cond::Any(&["has:bin"]), &[("command-line-interface", 0.6, 0.), ("os::windows-apis", 0.7, 0.), ("data-structures", 0.5, 0.), ("caching", 0.9, 0.), ("encoding", 0.9, 0.), ("algorithms", 0.7, 0.)]),
        (Cond::Any(&["dep:ansi_term", "dep:pager"]), &[("command-line-utilities", 1.1, 0.1)]),

        (Cond::NotAny(&["accessibility", "a11y", "atk", "screen-reader", "automation", "colorblind"]), &[("accessibility", 0.8, 0.)]),
        (Cond::Any(&["accessebility", "accessibility", "colorblind", "color-blind", "colorblindness"]), &[("accessibility", 1.4, 0.3), ("date-and-time", 0.7, 0.), ("finance", 0.5, 0.)]),
        (Cond::Any(&["a11y", "screen-reader", "screenreader", "atspi", "assistive-technology"]),
            &[("accessibility", 1.6, 0.4), ("compilers", 0.5, 0.), ("config", 0.9, 0.), ("encoding", 0.9, 0.), ("date-and-time", 0.6, 0.), ("parsing", 0.6, 0.)]),
        (Cond::All(&["gui", "accessibility"]), &[("accessibility", 1.1, 0.1), ("parsing", 0.5, 0.)]),
        (Cond::All(&["ui", "accessibility"]), &[("accessibility", 1.1, 0.1)]),
        (Cond::Any(&["tts", "text-to-speech"]), &[("accessibility", 1.2, 0.1), ("date-and-time", 0.7, 0.), ("multimedia::audio", 1.2, 0.2)]),
        (Cond::All(&["screen", "reader"]), &[("accessibility", 1.2, 0.2)]),
        (Cond::Any(&["accessibile", "atk"]), &[("accessibility", 1.1, 0.1)]),
        (Cond::Any(&["braille"]), &[("accessibility", 1.2, 0.2), ("config", 0.9, 0.), ("encoding", 0.9, 0.)]),
        (Cond::Any(&["aws", "accesscontextmanager", "accesscontext"]), &[("accessibility", 0.8, 0.)]),
        (Cond::Any(&["iam", "credentials", "rusoto", "kms", "utime", "privacy", "protection", "secrets", "accessapproval"]), &[("accessibility", 0.7, 0.)]),
        (Cond::All(&["google", "drive"]), &[("accessibility", 0.8, 0.), ("multimedia::audio", 0.5, 0.), ("multimedia", 0.5, 0.)]),
        (Cond::All(&["disk", "access"]), &[("accessibility", 0.8, 0.)]),
        (Cond::All(&["file", "access"]), &[("accessibility", 0.8, 0.)]),
        (Cond::All(&["linux", "mounts"]), &[("accessibility", 0.8, 0.)]),
        (Cond::All(&["cargo", "registry"]), &[("accessibility", 0.8, 0.)]),
        (Cond::All(&["windows", "registry"]), &[("os::windows-apis", 1.2, 0.1)]),
        (Cond::Any(&["dep:jemallocator", "dep:x11", "dep:ndk-sys", "dep:signal-hook", "dep:dbus"]), &[("os::windows-apis", 0.8, 0.)]),
        (Cond::Any(&["dep:winapi", "dep:wio", "dep:winreg", "dep:windows-sys", "dep:windows"]), &[("os::unix-apis", 0.7, 0.), ("os::macos-apis", 0.7, 0.)]),

        (Cond::NotAny(&["web", "html", "js", "javascript", "vue", "typescript", "deno", "json", "dep:tide", "tide", "warp", "ua", "jwt", "github", "proxy", "apache", "pubsub", "rpc", "rest", "k8s", "containers", "kubernetes", "thrift", "serverless",
            "graphql", "lambda", "aws", "mime", "wordpress", "rss", "atom", "xml", "css", "xss", "rocket", "webhook", "conduit", "hyper", "nodejs", "asmjs", "browser",
            "front-end", "ipfs", "youtube", "google", "webrtc", "tower", "axum", "rtcp", "dep:jsonwebtoken", "jsonrpc", "streaming", "api-client", "json-rpc", "dep:cookie", "jsonapi", "http-api", "rest-api", "json-api", "webhook", "dep:rocket", "dep:actix-web", "website", "ct-logs"]),
            &[("web-programming", 0.8, 0.)]),
        (Cond::NotAny(&["web", "http", "http2", "webrtc", "api", "fetch", "http-client", "serverless", "graphql", "lambda", "s3", "aws", "api-client", "client", "rest", "thrift", "gotham", "hyper", "request", "json", "jsonrpc", "jsonapi", "rpc", "curl", "tls", "requests", "http-api", "json-api"]),
            &[("web-programming::http-client", 0.8, 0.)]),
        (Cond::NotAny(&["web", "http", "webrtc", "web-server","http2", "server", "router", "hyper", "actix-web", "apache", "conduit", "webserver", "serverless", "graphql", "aws", "lambda", "rpc", "iron",
            "tcp", "sse", "server-sent-events", "service", "edge-computing", "middleware", "microservice", "proxy", "rest", "rest-api", "thrift", "webhook", "restful", "framework",
            "rocket", "dep:actix-web", "dep:tokio", "dep:async-std", "tower", "axum", "dep:warp", "dep:tide", "dep:rocket", "actix", "lucene", "elasticsearch"]),
            &[("web-programming::http-server", 0.8, 0.)]),
        (Cond::Any(&["hyper", "http-api", "json-api"]), &[("web-programming::http-client", 1.2, 0.), ("web-programming::http-server", 1.1, 0.),
            ("development-tools::build-utils", 0.8, 0.), ("parsing", 0.5, 0.), ("multimedia", 0.8, 0.), ("compression", 0.8, 0.)]),
        (Cond::All(&["protocol", "web"]), &[("web-programming", 1.4, 0.1), ("parsing", 0.5, 0.), ("rust-patterns", 0.8, 0.), ("filesystem", 0.7, 0.), ("command-line-utilities", 0.75, 0.)]),
        (Cond::All(&["protocol", "implementation"]), &[("network-programming", 1.3, 0.1), ("web-programming", 1.1, 0.)]),
        (Cond::Any(&["dep:js-sys", "dep:web-sys"]), &[("web-programming", 1.2, 0.1), ("wasm", 0.9, 0.), ("filesystem", 0.9, 0.), ("hardware-support", 0.6, 0.), ("embedded", 0.6, 0.)]),
        (Cond::Any(&["flink"]), &[("network-programming", 1.2, 0.1), ("web-programming", 1.2, 0.1)]),
        (Cond::Any(&["mastodon", "nostr", "atproto", "bluesky", "static-files", "rss", "rss-feed", "atom-feed", "lemmy"]),
            &[("web-programming", 1.2, 0.1), ("development-tools::build-utils", 0.8, 0.), ("config", 0.9, 0.), ("caching", 0.9, 0.), ("cryptography", 0.9, 0.), ("email", 0.9, 0.), ("compression", 0.7, 0.)]),
        (Cond::Any(&["webrtc", "rtmp", "rtcp", "rtsp"]), &[("network-programming", 1.2, 0.1), ("web-programming", 1.3, 0.2), ("date-and-time", 0.6, 0.), ("parsing", 0.5, 0.),
            ("multimedia", 1.25, 0.2), ("rust-patterns", 0.75, 0.), ("multimedia::video", 1.15, 0.15)]),
        (Cond::Any(&["hls", "m3u8"]), &[("web-programming", 1.1, 0.1), ("multimedia", 1.1, 0.1), ("multimedia::video", 1.1, 0.1)]),
        (Cond::All(&["live", "streaming"]), &[("web-programming", 1.2, 0.1), ("multimedia", 1.2, 0.1), ("multimedia::video", 1.2, 0.1)]),
        (Cond::All(&["multimedia", "streaming"]), &[("web-programming", 1.1, 0.1), ("multimedia", 1.3, 0.1), ("multimedia::video", 1.2, 0.1), ("parsing", 0.5, 0.)]),
        (Cond::All(&["static", "web"]), &[("web-programming", 1.2, 0.1)]),
        (Cond::Any(&["rpc", "thrift", "fbthrift"]), &[("network-programming", 1.2, 0.05), ("development-tools::build-utils", 0.9, 0.), ("rust-patterns", 0.7, 0.)]),
        (Cond::Any(&["rdf", "linked-data", "json-ld", "semantic-web", "n-triples"]), &[("web-programming", 1.2, 0.1), ("parser-implementations", 1.1, 0.), ("data-structures", 1.05, 0.), ("science", 0.9, 0.), ("science::geo", 0.9, 0.)]),
        (Cond::Any(&["thrift", "fbthrift"]), &[("encoding", 1.2, 0.1), ("rust-patterns", 0.7, 0.)]),
        (Cond::All(&["streaming", "api"]), &[("network-programming", 1.2, 0.), ("web-programming", 1.2, 0.), ("algorithms", 1.2, 0.)]),
        (Cond::All(&["packet", "sniffing"]), &[("network-programming", 1.3, 0.1), ("text-processing", 0.5, 0.)]),
        (Cond::All(&["packet", "ether"]), &[("network-programming", 1.3, 0.1), ("text-processing", 0.5, 0.)]),
        (Cond::All(&["packet", "dump"]), &[("network-programming", 1.2, 0.1)]),
        (Cond::All(&["packet", "capture"]), &[("network-programming", 1.3, 0.1), ("compression", 0.7, 0.), ("config", 0.9, 0.)]),
        (Cond::All(&["api", "dep:reqwest"]), &[("web-programming", 1.2, 0.05), ("compilers", 0.8, 0.), ("data-structures", 0.8, 0.), ("rust-patterns", 0.8, 0.), ("development-tools::cargo-plugins", 0.9, 0.)]),
        (Cond::All(&["wrapper", "dep:reqwest"]), &[("web-programming", 1.2, 0.05), ("compilers", 0.8, 0.), ("data-structures", 0.8, 0.), ("rust-patterns", 0.8, 0.)]),
        (Cond::All(&["sdk", "dep:reqwest"]), &[("web-programming", 1.2, 0.05), ("compilers", 0.8, 0.), ("data-structures", 0.8, 0.), ("development-tools::procedural-macro-helpers", 0.5, 0.), ("rust-patterns", 0.7, 0.)]),
        (Cond::All(&["sdk", "dep:hyper"]), &[("web-programming", 1.2, 0.05), ("compilers", 0.8, 0.), ("embedded", 0.8, 0.), ("hardware-support", 0.8, 0.), ("data-structures", 0.8, 0.), ("rust-patterns", 0.8, 0.), ("multimedia::video", 0.9, 0.)]),
        (Cond::Any(&["jwt", "jwst", "jwks", "dep:jsonwebtoken", "api-client", "csrf"]),
            &[("web-programming", 1.2, 0.05), ("date-and-time", 0.6, 0.), ("compilers", 0.5, 0.), ("algorithms", 0.5, 0.), ("parsing", 0.5, 0.), ("parser-implementations", 0.7, 0.),
            ("data-structures", 0.5, 0.), ("rust-patterns", 0.7, 0.)]),
        (Cond::Any(&["jwt", "jwst", "jwk", "jwks"]), &[("authentication", 1.1, 0.05), ("cryptography", 1.1, 0.)]),
        (Cond::Any(&["jsonwebtoken", "json-web-token"]), &[("authentication", 1.1, 0.05)]),
        (Cond::Any(&["dep:js-sys"]), &[("web-programming", 1.15, 0.05), ("data-structures", 0.5, 0.), ("algorithms", 0.8, 0.), ("compression", 0.9, 0.), ("rust-patterns", 0.7, 0.)]),
        (Cond::Any(&["dep:wasm-bindgen"]), &[("web-programming", 1.1, 0.), ("rust-patterns", 0.7, 0.)]),
        (Cond::Any(&["vue", "vue3", "query-params"]), &[("web-programming", 1.1, 0.1)]),
        (Cond::Any(&["sveltekit", "svelte", "content-management-system"]), &[("web-programming", 1.1, 0.1), ("template-engine", 1.1, 0.1), ("caching", 0.9, 0.), ("config", 0.9, 0.), ("os::windows-apis", 0.4, 0.)]),
        (Cond::Any(&["minifier"]), &[("web-programming", 1.1, 0.), ("encoding", 1.1, 0.), ("no-std", 0.9, 0.), ("rust-patterns", 0.7, 0.)]),
        (Cond::Any(&["tailwindcss", "front-end-development", "tailwind-css", "css3", "tailwind", "yew"]),
            &[("web-programming", 1.2, 0.1), ("development-tools", 1.1, 0.), ("config", 0.9, 0.), ("wasm", 0.9, 0.), ("development-tools::build-utils", 0.9, 0.), ("rust-patterns", 0.7, 0.), ("compression", 0.7, 0.)]),
        (Cond::All(&["web", "api"]), &[("web-programming", 1.3, 0.1), ("algorithms", 0.7, 0.), ("data-structures", 0.6, 0.)]),
        (Cond::All(&["web", "framework"]), &[("web-programming", 1.3, 0.1), ("algorithms", 0.7, 0.), ("os::unix-apis", 0.5, 0.), ("data-structures", 0.6, 0.)]),
        (Cond::Any(&["web-server", "webserver"]), &[("web-programming::http-server", 1.2, 0.2), ("text-processing", 0.5, 0.)]),
        (Cond::Any(&["web-framework", "leptos", "reverse-proxy", "microservice"]), &[("web-programming", 1.2, 0.2), ("os::unix-apis", 0.5, 0.), ("algorithms", 0.7, 0.), ("data-structures", 0.6, 0.)]),
        (Cond::All(&["cloud", "web"]), &[("web-programming", 1.4, 0.2), ("rust-patterns", 0.5, 0.), ("gui", 0.9, 0.), ("data-structures", 0.6, 0.), ("algorithms", 0.8, 0.), ("compilers", 0.8, 0.), ("parsing", 0.4, 0.), ("config", 0.8, 0.), ("filesystem", 0.7, 0.), ("no-std", 0.7, 0.), ("development-tools::build-utils", 0.8, 0.)]),
        (Cond::All(&["user", "agent"]), &[("web-programming", 1.1, 0.1), ("web-programming::http-client", 1.1, 0.1)]),
        (Cond::Any(&["user-agent", "useragent"]), &[("web-programming", 1.1, 0.1), ("web-programming::http-client", 1.1, 0.1), ("compilers", 0.5, 0.), ("data-structures", 0.8, 0.), ("algorithms", 0.8, 0.)]),
        (Cond::All(&["cloud", "provider"]), &[("web-programming", 1.3, 0.1), ("os::windows-apis", 0.7, 0.), ("gui", 0.9, 0.), ("compilers", 0.5, 0.)]),
        (Cond::All(&["web", "token"]), &[("web-programming", 1.2, 0.)]),
        (Cond::Any(&["webdev", "web-dev", "blogging", "site-generator", "static-site-generator", "blog-engine", "web-development", "web-application", "webhook", "blog", "discord-api", "openai", "matrix-org",
         "webdriver", "web-scraping", "web-interface", "chatbot", "browsers", "browser", "cloud",
            "reqwest", "webhooks", "lucene", "elasticsearch", "web-api", "webapi", "openapi"]),
            &[("web-programming", 1.2, 0.1), ("embedded", 0.9, 0.), ("encoding", 0.9, 0.), ("development-tools::cargo-plugins", 0.5, 0.),
            ("os::windows-apis", 0.7, 0.), ("os::unix-apis", 0.7, 0.), ("emulators", 0.4, 0.), ("compilers", 0.9, 0.), ("parsing", 0.5, 0.)]),
        (Cond::Any(&["edge-computing", "distributed-computing", "scuttlebutt"]),
            &[("web-programming", 1.2, 0.1), ("network-programming", 1.2, 0.1), ("science::geo", 0.8, 0.), ("rust-patterns", 0.7, 0.)]),
        (Cond::All(&["api-bindings", "http"]), &[("web-programming", 1.2, 0.1)]),
        (Cond::All(&["matrix", "chat"]), &[("web-programming", 1.2, 0.1)]),
        (Cond::All(&["matrix", "bot"]), &[("web-programming", 1.2, 0.1)]),
        (Cond::All(&["ssr", "web"]), &[("web-programming::http-server", 1.2, 0.1)]),
        (Cond::All(&["router", "web"]), &[("web-programming::http-server", 1.2, 0.1), ("rust-patterns", 0.75, 0.)]),
        (Cond::All(&["reactive", "dom"]), &[("web-programming", 1.2, 0.1)]),
        (Cond::All(&["virtual", "dom"]), &[("web-programming", 1.2, 0.), ("gui", 1.2, 0.)]),
        (Cond::Any(&["vdom", "dom-manipulation"]), &[("web-programming", 1.1, 0.), ("gui", 1.1, 0.)]),
        (Cond::All(&["isomorphic", "wasm"]), &[("web-programming", 1.2, 0.1)]),
        (Cond::All(&["api-bindings", "matrix"]), &[("web-programming", 1.2, 0.1)]),
        (Cond::All(&["api-bindings", "oauth"]), &[("web-programming", 1.2, 0.1)]),
        (Cond::All(&["api-bindings", "chat"]), &[("web-programming", 1.2, 0.1)]),
        (Cond::All(&["api-bindings", "http-client"]), &[("web-programming", 1.2, 0.1)]),
        (Cond::Any(&["serverless", "aws-lambda", "cloud-native", "cloudflare", "cloudflare-workers", "url-shortener"]),
            &[("web-programming", 1.2, 0.1), ("network-programming", 1.1, 0.1), ("rust-patterns", 0.7, 0.)]),

        (Cond::Any(&["csv", "writer"]), &[("encoding", 1.2, 0.1), ("command-line-interface", 0.3, 0.), ("data-structures", 0.9, 0.), ("database", 0.9, 0.), ("command-line-utilities", 0.75, 0.)]),
        (Cond::Any(&["nodejs"]), &[("web-programming::http-server", 1.1, 0.), ("embedded", 0.5, 0.), ("hardware-support", 0.5, 0.), ("algorithms", 0.5, 0.), ("database-implementations", 0.7, 0.), ("rust-patterns", 0.5, 0.)]),
        (Cond::Any(&["html", "html-parsing"]), &[("web-programming", 1.11, 0.), ("template-engine", 1.12, 0.), ("text-processing", 1.1, 0.), ("development-tools::build-utils", 0.9, 0.)]),
        (Cond::Any(&["rss", "rss-feed", "atom-feed", "template-project", "project-template", "git-commit", "refactoring", "microservice"]),
            &[("template-engine", 0.8, 0.), ("os::unix-apis", 0.75, 0.)]),
        (Cond::Any(&["mime"]), &[("web-programming", 1.2, 0.), ("email", 1.2, 0.), ("encoding", 0.8, 0.)]),
        (Cond::All(&["static", "site"]), &[("web-programming", 1.11, 0.2), ("template-engine", 1.12, 0.2), ("text-processing", 1.1, 0.1), ("development-tools::build-utils", 0.9, 0.)]),
        (Cond::Any(&["ruby", "python", "common-lisp", "lisp-language", "lisp-interpreter", "pyo3", "lua", "malbolge", "gluon", "bytecode", "lisp", "java", "maven", "jvm", "jni"]),
            &[("development-tools", 1.2, 0.12), ("compilers", 1.2, 0.12), ("parsing", 0.7, 0.), ("embedded", 0.6, 0.), ("development-tools::ffi", 1.3, 0.05), ("science", 0.9, 0.), ("os", 0.9, 0.), ("parser-implementations", 0.9, 0.),
            ("command-line-interface", 0.3, 0.), ("command-line-utilities", 0.75, 0.)]),
        (Cond::Any(&["github"]),
            &[("development-tools", 1.2, 0.12), ("science", 0.9, 0.), ("os", 0.9, 0.), ("parser-implementations", 0.9, 0.), ("command-line-interface", 0.3, 0.), ("command-line-utilities", 0.75, 0.)]),
        (Cond::All(&["lexical", "analysis"]), &[("compilers", 1.2, 0.12), ("parsing", 0.6, 0.)]),
        (Cond::All(&["ast", "parser"]), &[("compilers", 1.2, 0.12), ("parsing", 0.6, 0.), ("parser-implementations", 1.2, 0.)]),
        (Cond::Any(&["sitter"]), &[("parsing", 0.6, 0.)]),
        (Cond::All(&["language", "parser"]), &[("compilers", 1.2, 0.)]),
        (Cond::All(&["scripting", "language"]), &[("compilers", 1.2, 0.12), ("parsing", 0.6, 0.), ("parser-implementations", 0.6, 0.)]),
        (Cond::Any(&["scripting-language", "scripting-engine", "c", "cxx"]), &[("compilers", 1.2, 0.12), ("parsing", 0.6, 0.), ("config", 0.9, 0.), ("parser-implementations", 0.6, 0.), ("compression", 0.9, 0.)]),
        (Cond::All(&["programming", "language"]), &[("compilers", 1.2, 0.12), ("development-tools", 1.4, 0.3), ("development-tools::ffi", 1.2, 0.05), ("parsing", 0.8, 0.)]),
        (Cond::Any(&["dep:codespan-reporting", "dep:wast", "dep:clang-sys", "dep:clan", "dep:llvm-sys", "abstract-syntax-tree"]), &[("compilers", 1.1, 0.05)]),
        (Cond::Any(&["dep:gimli", "repl"]), &[("compilers", 1.1, 0.05), ("development-tools::debugging", 1.1, 0.05)]),
        (Cond::Any(&["dep:cranelift-codegen"]), &[("compilers", 1.2, 0.1), ("no-std", 0.8, 0.)]),
        (Cond::Any(&["dep:schemars", "dep:reqwest", "dep:actix-web"]), &[("compilers", 0.7, 0.), ("embedded", 0.6, 0.)]),

        (Cond::Any(&["dep:libgit2-sys", "dep:libgit2", "monorepo"]),
            &[("development-tools", 1.2, 0.1), ("rust-patterns", 0.7, 0.), ("encoding", 0.9, 0.), ("template-engine", 0.7, 0.)]),
        (Cond::Any(&["runtime"]), &[("development-tools", 1.3, 0.1), ("development-tools::testing", 0.7, 0.), ("no-std", 0.8, 0.), ("encoding", 0.8, 0.), ("command-line-utilities", 0.7, 0.)]),
        (Cond::Any(&["pijul", "scripting", "rbenv", "pyenv", "pip", "lint", "linter", "linting"]), &[("development-tools", 1.2, 0.1), ("caching", 0.9, 0.), ("no-std", 0.8, 0.), ("embedded", 0.8, 0.)]),

        (Cond::Any(&["server", "server-sent", "micro-services", "rest", "webrtc", "microservices", "dep:actix-web", "actix-web-middleware", "dep:tower", "dep:axum", "dep:iron", "dep:gotham", "dep:roa", "dep:rocket"]),
            &[("web-programming::http-server", 1.2, 0.11), ("web-programming", 1.1, 0.), ("data-structures", 0.9, 0.), ("date-and-time", 0.9, 0.), ("rust-patterns", 0.9, 0.), ("filesystem", 0.9, 0.),
            ("command-line-interface", 0.3, 0.), ("data-structures", 0.7, 0.),("science::math", 0.7, 0.),("command-line-utilities", 0.75, 0.), ("development-tools::cargo-plugins", 0.4, 0.)]),
        (Cond::Any(&["iron", "kafka", "rdkafka", "actix-web", "wsgi", "openid", "openid-connect", "conduit", "async-graphql", "graphql", "graphql-parser", "restful", "http-server"]),
            &[("web-programming::http-server", 1.2, 0.11), ("web-programming", 1.1, 0.), ("text-processing", 0.7, 0.), ("rust-patterns", 0.8, 0.), ("value-formatting", 0.8, 0.), ("parser-implementations", 0.9, 0.),
            ("algorithms", 0.8, 0.), ("no-std", 0.5, 0.), ("date-and-time", 0.5, 0.), ("command-line-interface", 0.3, 0.), ("data-structures", 0.7, 0.),("command-line-utilities", 0.75, 0.),
            ("multimedia::video", 0.7, 0.), ("multimedia", 0.6, 0.), ("multimedia", 0.5, 0.), ("config", 0.9, 0.), ("development-tools", 0.9, 0.), ("development-tools::cargo-plugins", 0.4, 0.),
            ("development-tools::build-utils", 0.5, 0.), ("compression", 0.9, 0.)]),
        (Cond::All(&["web", "routing"]), &[("web-programming::http-server", 1.2, 0.1), ("command-line-utilities", 0.75, 0.)]),
        (Cond::All(&["rest", "api"]), &[("web-programming::http-server", 1.2, 0.), ("web-programming::http-client", 1.2, 0.), ("web-programming", 1.1, 0.), ("network-programming", 0.9, 0.)]),
        (Cond::All(&["language", "server"]), &[("web-programming::http-server", 0.2, 0.), ("compilers", 0.7, 0.), ("development-tools", 1.2, 0.2), ("parsing", 0.9, 0.), ("text-editors", 1.3, 0.)]),
        (Cond::Any(&["deno"]), &[("web-programming::http-server", 1.2, 0.1), ("embedded", 0.6, 0.), ("development-tools", 1.1, 0.1), ("development-tools::build-utils", 0.9, 0.)]),
        (Cond::All(&["lsp"]), &[("web-programming::http-server", 0.8, 0.), ("development-tools", 1.2, 0.), ("text-editors", 1.2, 0.1)]),
        (Cond::Any(&["self-hosting"]), &[("web-programming::http-server", 1.2, 0.1), ("compilers", 1.2, 0.)]),
        (Cond::All(&["web", "framework"]), &[("web-programming", 1.4, 0.2), ("web-programming::http-server", 1.2, 0.), ("command-line-utilities", 0.75, 0.)]),
        (Cond::Any(&["dep:tide", "dep:warp", "dep:actix-web", "dep:actix-files", "actix", "middleware"]),
            &[("web-programming", 1.1, 0.), ("web-programming::websocket", 0.9, 0.), ("web-programming::http-server", 1.2, 0.1), ("config", 0.9, 0.), ("compression", 0.9, 0.),
            ("command-line-utilities", 0.9, 0.), ("network-programming", 0.9, 0.), ("science::math", 0.9, 0.), ("date-and-time", 0.4, 0.)]),
        (Cond::Any(&["wamp", "nginx", "apache"]), &[("web-programming::http-server", 1.2, 0.1), ("web-programming::websocket", 0.9, 0.), ("filesystem", 0.7, 0.), ("date-and-time", 0.7, 0.), ("command-line-utilities", 0.75, 0.)]),
        (Cond::Any(&["http", "dns", "nameserver", "ip-address", "ip-address-lookup", "network-interfaces", "dnssec", "grpc", "rpc", "json-rpc", "jsonrpc", "jsonapi", "json-api", "jwt", "statsd", "telemetry"]),
            &[("network-programming", 1.2, 0.1), ("web-programming", 1.1, 0.), ("web-programming::websocket", 0.88, 0.), ("parsing", 0.7, 0.), ("encoding", 0.8, 0.),
            ("config", 0.9, 0.), ("rendering::data-formats", 0.5, 0.), ("asynchronous", 0.9, 0.),
            ("value-formatting", 0.8, 0.), ("data-structures", 0.7, 0.), ("rust-patterns", 0.8, 0.),
            ("command-line-utilities", 0.9, 0.), ("development-tools", 0.9, 0.), ("development-tools::testing", 0.9, 0.), ("development-tools::build-utils", 0.9, 0.)]),
        (Cond::Any(&["backend", "server-sent"]), &[("web-programming::http-server", 1.2, 0.1), ("command-line-utilities", 0.8, 0.)]),
        (Cond::Any(&["client"]), &[("web-programming", 1.1, 0.), ("network-programming", 1.1, 0.), ("parsing", 0.7, 0.), ("visualization", 0.8, 0.), ("rust-patterns", 0.6, 0.), ("web-programming::http-server", 0.9, 0.),
            ("development-tools::cargo-plugins", 0.8, 0.), ("value-formatting", 0.8, 0.)]),
        (Cond::Any(&["kubernetes", "terraform", "coreos"]), &[("web-programming", 1.1, 0.), ("web-programming::http-client", 0.9, 0.), ("network-programming", 1.2, 0.)]),
        (Cond::All(&["http", "client", "server"]), &[("web-programming", 1.2, 0.11)]),
        (Cond::All(&["http", "server"]), &[("web-programming::http-server", 1.2, 0.11), ("rust-patterns", 0.75, 0.)]),
        (Cond::All(&["http", "client"]), &[("web-programming::http-client", 1.2, 0.1), ("web-programming::http-server", 0.9, 0.), ("parsing", 0.8, 0.), ("algorithms", 0.8, 0.), ("data-structures", 0.8, 0.), ("rust-patterns", 0.8, 0.), ("development-tools::procedural-macro-helpers", 0.2, 0.)]),
        (Cond::Any(&["http-client", "graphql-client"]), &[("web-programming::http-client", 1.2, 0.1), ("web-programming::http-server", 0.9, 0.),
            ("compression", 0.9, 0.), ("parsing", 0.8, 0.), ("algorithms", 0.8, 0.), ("text-processing", 0.8, 0.), ("development-tools::procedural-macro-helpers", 0.2, 0.)]),
        (Cond::All(&["rpc", "client"]), &[("web-programming::http-client", 1.2, 0.1), ("parsing", 0.8, 0.), ("algorithms", 0.8, 0.)]),
        (Cond::Any(&["firefox", "chromium"]), &[("web-programming", 1.2, 0.1), ("web-programming::http-server", 0.8, 0.)]),
        (Cond::Any(&["dep:http", "dep:mime"]), &[("web-programming", 1.1, 0.)]),
        (Cond::Any(&["dep:juniper"]), &[("web-programming", 1.1, 0.), ("web-programming::http-server", 1.1, 0.)]),
        (Cond::Any(&["dep:warp", "dep:finchers", "dep:tide"]), &[("web-programming", 1.1, 0.), ("web-programming::http-server", 1.2, 0.), ("embedded", 0.6, 0.), ("development-tools::build-utils", 0.8, 0.)]),
        (Cond::Any(&["http-client", "twitter", "instagram", "tiktok", "facebook", "vkontakte"]), &[("web-programming::http-client", 1.2, 0.1), ("web-programming::http-server", 0.8, 0.), ("no-std", 0.7, 0.),
            ("algorithms", 0.7, 0.), ("command-line-utilities", 0.75, 0.), ("development-tools::build-utils", 0.8, 0.), ("development-tools::procedural-macro-helpers", 0.4, 0.), ("development-tools", 0.75, 0.)]),
        (Cond::All(&["cli", "cloud"]), &[("web-programming::http-client", 1.2, 0.1), ("command-line-utilities", 1.2, 0.2)]),
        (Cond::Any(&["javascript", "web-components", "scss", "sass", "emscripten", "asmjs", "postcss"]),
            &[("web-programming", 1.2, 0.2), ("development-tools", 1.1, 0.), ("gui", 0.9, 0.), ("parsing", 0.8, 0.),  ("parser-implementations", 0.9, 0.), ("embedded", 0.7, 0.),
            ("config", 0.9, 0.), ("no-std", 0.7, 0.), ("date-and-time", 0.8, 0.), ("os", 0.7, 0.), ("command-line-utilities", 0.75, 0.), ("development-tools::testing", 0.6, 0.)]),
        (Cond::Any(&["stdweb", "lodash", "css", "webvr"]),
            &[("web-programming", 1.2, 0.2), ("gui", 0.9, 0.), ("compilers", 0.6, 0.), ("embedded", 0.7, 0.), ("encoding", 0.7, 0.), ("no-std", 0.7, 0.), ("os", 0.7, 0.),
            ("command-line-utilities", 0.75, 0.), ("development-tools::testing", 0.6, 0.)]),
        (Cond::Any(&["frontend", "slack", "url", "uri"]),
            &[("web-programming", 1.2, 0.2), ("gui", 0.9, 0.), ("embedded", 0.8, 0.), ("command-line-utilities", 0.75, 0.), ("development-tools::testing", 0.6, 0.)]),
        (Cond::Any(&["json"]), &[("web-programming", 1.1, 0.1), ("algorithms", 0.8, 0.), ("compilers", 0.7, 0.), ("no-std", 0.8, 0.), ("multimedia", 0.8, 0.), ("multimedia::video", 0.8, 0.),
            ("command-line-utilities", 0.75, 0.), ("text-processing", 0.8, 0.)]),
        (Cond::Any(&["protocol", "network", "socket", "sockets", "wifi", "wi-fi"]), &[
            ("network-programming", 1.2, 0.2), ("parsing", 0.6, 0.), ("algorithms", 0.8, 0.), ("compilers", 0.6, 0.), ("os::windows-apis", 0.9, 0.), ("rendering::data-formats", 0.4, 0.),
            ("command-line-utilities", 0.75, 0.), ("cryptography::cryptocurrencies", 0.8, 0.)]),
        (Cond::Any(&["protobuf", "netcdf", "protocol-buffers", "proto"]),
            &[("encoding", 1.2, 0.2), ("network-programming", 1.15, 0.15), ("compilers", 0.6, 0.), ("parsing", 0.4, 0.), ("rust-patterns", 0.7, 0.), ("command-line-utilities", 0.75, 0.)]),
        (Cond::Any(&["varint"]), &[("encoding", 1.1, 0.1)]),
        (Cond::All(&["protocol", "buffers"]), &[("network-programming", 1.2, 0.1), ("encoding", 1.2, 0.1), ("compilers", 0.6, 0.), ("science", 0.6, 0.), ("parsing", 0.6, 0.)]),
        (Cond::Any(&["p2p", "digitalocean"]), &[("network-programming", 1.4, 0.2), ("command-line-utilities", 0.75, 0.), ("config", 0.9, 0.),  ("filesystem", 0.8, 0.), ("date-and-time", 0.8, 0.), ("compilers", 0.5, 0.), ("rust-patterns", 0.75, 0.),
            ("development-tools", 0.75, 0.), ("multimedia", 0.5, 0.)]),
        (Cond::All(&["ip", "address"]), &[("network-programming", 1.2, 0.1)]),
        (Cond::Any(&["ip", "dep:trust-dns-resolver", "ipsec"]), &[("network-programming", 1.15, 0.), ("compilers", 0.8, 0.), ("web-programming", 1.1, 0.)]),

        (Cond::All(&["graphics", "gpu"]), &[("rendering::graphics-api", 1.34, 0.1), ("parsing", 0.5, 0.), ("compilers", 0.9, 0.), ("science::geo", 0.7, 0.), ("finance", 0.5, 0.),
            ("no-std", 0.5, 0.), ("accessibility", 0.7, 0.), ("algorithms", 0.9, 0.), ("caching", 0.5, 0.), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.), ("multimedia::audio", 0.7, 0.)]),
        (Cond::Any(&["graphics", "wgpu"]), &[("rendering::graphics-api", 1.2, 0.1), ("rendering", 1.1, 0.), ("email", 0.7, 0.), ("config", 0.9, 0.), ("multimedia", 1.1, 0.), ("rust-patterns", 0.7, 0.)]),
        (Cond::Any(&["bezier"]), &[("rendering", 1.1, 0.1), ("multimedia::images", 1.1, 0.), ("compilers", 0.5, 0.), ("config", 0.9, 0.)]),
        (Cond::All(&["surface", "gpu"]), &[("rendering::graphics-api", 1.34, 0.1), ("no-std", 0.5, 0.), ("caching", 0.5, 0.), ("parsing", 0.5, 0.), ("multimedia::audio", 0.8, 0.)]),
        (Cond::Any(&["graphics", "2d", "canvas"]), &[("rendering::graphics-api", 1.1, 0.1), ("rendering", 1.1, 0.), ("multimedia", 1.1, 0.)]),
        (Cond::All(&["luminance"]), &[("rendering::graphics-api", 1.1, 0.), ("rendering", 1.1, 0.)]),
        (Cond::All(&["graphics", "bindings"]), &[("rendering::graphics-api", 1.34, 0.2), ("game-development", 0.9, 0.), ("accessibility", 0.7, 0.)]),
        (Cond::All(&["metal", "api"]), &[("rendering::graphics-api", 1.34, 0.2), ("game-development", 0.8, 0.), ("date-and-time", 0.3, 0.)]),
        (Cond::Any(&["gfx-rs", "sdf", "viewport"]), &[("rendering::graphics-api", 1.15, 0.1), ("compilers", 0.6, 0.), ("caching", 0.75, 0.), ("games", 0.8, 0.), ("parsing", 0.5, 0.), ("development-tools::build-utils", 0.8, 0.)]),
        (Cond::Any(&["polygon", "polygons", "tessellation", "mesh", "polygon-mesh"]), &[("rendering::graphics-api", 1.15, 0.1), ("rendering", 1.15, 0.1), ("science::geo", 1.2, 0.), ("algorithms", 1.1, 0.05), ("compilers", 0.6, 0.), ("games", 0.8, 0.), ("development-tools::build-utils", 0.8, 0.)]),
        (Cond::Any(&["gfx-rs", "webgpu", "wgpu"]), &[("rendering::graphics-api", 1.2, 0.1), ("game-development", 0.9, 0.), ("algorithms", 0.8, 0.), ("text-editors", 0.8, 0.), ("rust-patterns", 0.7, 0.)]),
        (Cond::All(&["graphics", "sdk"]), &[("rendering::graphics-api", 1.2, 0.1), ("no-std", 0.9, 0.), ("rust-patterns", 0.75, 0.)]),
        (Cond::All(&["vr", "sdk"]), &[("rendering::graphics-api", 1.3, 0.2), ("no-std", 0.5, 0.), ("encoding", 0.9, 0.), ("algorithms", 0.8, 0.)]),
        (Cond::All(&["vr", "bindings"]), &[("rendering::graphics-api", 1.1, 0.1)]),
        (Cond::All(&["graphics", "api"]), &[("rendering::graphics-api", 1.3, 0.15), ("parsing", 0.5, 0.), ("compilers", 0.9, 0.), ("game-development", 0.9, 0.), ("games", 0.2, 0.)]),
        (Cond::All(&["input"]), &[("rendering::graphics-api", 0.8, 0.)]),
        (Cond::All(&["marching-cubes"]), &[("rendering", 1.2, 0.1), ("game-development", 1.2, 0.)]),
        (Cond::All(&["pango"]), &[("rendering", 1.2, 0.1), ("algorithms", 0.7, 0.), ("embedded", 0.6, 0.), ("network-programming", 0.6, 0.), ("os::windows-apis", 0.4, 0.)]),
        (Cond::Any(&["direct2d", "glsl", "rendergraph", "vulkan", "drawing-apis", "swsurface", "software-rendered-surface", "glium", "freetype"]),
                &[("rendering::graphics-api", 1.2, 0.1), ("science::math", 0.7, 0.), ("no-std", 0.5, 0.), ("algorithms", 0.8, 0.), ("rendering", 1.1, 0.1), ("rendering::data-formats", 0.9, 0.),
                ("web-programming::websocket", 0.15, 0.), ("rendering::graphics-api", 1.1, 0.1),("rendering::engine", 1.05, 0.05), ("games", 0.8, 0.), ("text-editors", 0.9, 0.),
                ("hardware-support", 0.8, 0.), ("development-tools::build-utils", 0.8, 0.), ("development-tools::testing", 0.6, 0.), ("no-std", 0.5, 0.), ("accessibility", 0.8, 0.), ("memory-management", 0.8, 0.)]),
        (Cond::Any(&["opengl", "opengl-es", "directdraw", "directwrite", "gl", "skia", "d3d12", "sdl2", "spirv", "spir-v", "shader", "directx", "d3d11", "d3d12", "direct-x"]),
                &[("rendering::graphics-api", 1.3, 0.15), ("science::math", 0.8, 0.), ("no-std", 0.9, 0.), ("parsing", 0.75, 0.), ("date-and-time", 0.8, 0.), ("rust-patterns", 0.8, 0.), ("rendering", 1.1, 0.1),
                ("rendering::data-formats", 0.9, 0.), ("web-programming::websocket", 0.15, 0.), ("development-tools::build-utils", 0.8, 0.), ("rendering::graphics-api", 1.1, 0.1),
                ("rendering::engine", 1.05, 0.05), ("games", 0.8, 0.), ("game-development", 1.2, 0.), ("hardware-support", 0.8, 0.), ("development-tools::testing", 0.6, 0.), ("memory-management", 0.8, 0.)]),
        (Cond::Any(&["render", "bresenham", "marching-cubes", "rasterisation", "rasterization", "deferred-rasterisation", "oculus", "graphics-editor", "opengl-based", "gfx",
            "ovr", "vr", "shader", "sprites", "nvidia", "ray", "renderer", "raytracing", "path-tracing", "ray-tracing", "ray-casting"]),
                &[("rendering", 1.2, 0.1), ("rendering::graphics-api", 1.2, 0.1), ("rendering::engine", 1.1, 0.05), ("database", 0.8, 0.), ("rendering::data-formats", 0.8, 0.),
                ("web-programming::websocket", 0.15, 0.), ("games", 0.8, 0.), ("multimedia", 0.8, 0.), ("multimedia::images", 0.8, 0.), ("parsing", 0.5, 0.), ("development-tools::cargo-plugins", 0.9, 0.), ("hardware-support", 0.8, 0.),
                ("development-tools::testing", 0.6, 0.), ("development-tools::build-utils", 0.6, 0.)]),
        (Cond::Any(&["blender", "graphics", "image-processing", "cairo", "dep:cairo-rs"]), &[
            ("multimedia::images", 1.2, 0.05), ("rendering::graphics-api", 1.05, 0.), ("date-and-time", 0.6, 0.), ("rendering", 1.1, 0.1), ("filesystem", 0.7, 0.),
            ("games", 0.8, 0.), ("simulation", 0.5, 0.), ("data-structures", 0.8, 0.), ("text-editors", 0.9, 0.)]),
        (Cond::Any(&["blender", "maya", "autocad", "glsl", "3ds"]), &[("rendering", 1.2, 0.1), ("development-tools::build-utils", 0.8, 0.),
            ("parsing", 0.5, 0.), ("rust-patterns", 0.7, 0.), ("multimedia::audio", 0.8, 0.)]),
        (Cond::Any(&["direct2d", "directx", "direct-x", "directdraw", "directwrite"]), &[("os::windows-apis", 1.2, 0.1), ("caching", 0.5, 0.), ("email", 0.7, 0.)]),
        (Cond::Any(&["renderdoc"]),
            &[("rendering", 1.2, 0.1), ("rendering::graphics-api", 1.2, 0.1), ("development-tools::debugging", 1.2, 0.1), ("parsing", 0.5, 0.), ("web-programming", 0.5, 0.)]),
        (Cond::All(&["ray", "tracing"]), &[("rendering", 1.2, 0.1), ("development-tools::build-utils", 0.8, 0.), ("caching", 0.5, 0.), ("encoding", 0.9, 0.), ("config", 0.9, 0.), ("parsing", 0.75, 0.), ("rust-patterns", 0.7, 0.)]),
        (Cond::All(&["macos", "metal"]), &[("rendering::graphics-api", 1.2, 0.1), ("os::macos-apis", 1.2, 0.1)]),
        (Cond::All(&["ios", "metal"]), &[("rendering::graphics-api", 1.2, 0.1), ("caching", 0.75, 0.), ("os::macos-apis", 1.2, 0.1)]),
        (Cond::All(&["apple", "metal"]), &[("rendering::graphics-api", 1.2, 0.1), ("os::macos-apis", 1.3, 0.1), ("os::windows-apis", 0.8, 0.)]),
        (Cond::Any(&["graphics", "image-processing"]), &[
            ("multimedia::images", 1.2, 0.1), ("rendering", 1.1, 0.1), ("development-tools::testing", 0.7, 0.), ("development-tools::build-utils", 0.7, 0.), ("rust-patterns", 0.7, 0.)]),

        (Cond::Any(&["bvh", "bounding-box"]), &[("algorithms", 1.1, 0.1), ("rendering", 1.2, 0.), ("config", 0.9, 0.), ("parsing", 0.75, 0.), ("rust-patterns", 0.75, 0.), ("multimedia::audio", 0.8, 0.)]),
        (Cond::Any(&["gpgpu", "cudnn"]), &[("asynchronous", 0.2, 0.), ("concurrency", 0.8, 0.), ("accessibility", 0.8, 0.), ("embedded", 0.6, 0.), ("rust-patterns", 0.75, 0.), ("finance", 0.8, 0.), ("no-std", 0.5, 0.),
            ("game-development", 0.8, 0.), ("parsing", 0.5, 0.), ("filesystem", 0.7, 0.), ("development-tools::testing", 0.8, 0.), ("development-tools::cargo-plugins", 0.7, 0.),
            ("date-and-time", 0.6, 0.), ("development-tools::build-utils", 0.6, 0.)]),
        (Cond::Any(&["vk"]), &[("rendering::graphics-api", 1.1, 0.)]),
        (Cond::Any(&["dep:reqwest", "dep:rocket", "has:bin"]), &[("rendering::graphics-api", 0.8, 0.), ("parsing", 0.75, 0.), ("rust-patterns", 0.8, 0.)]),
        (Cond::Any(&["validate", "windowing", "opencl"]), &[("games", 0.2, 0.), ("asynchronous", 0.2, 0.), ("concurrency", 0.8, 0.),
            ("development-tools", 0.9, 0.), ("development-tools::build-utils", 0.8, 0.)]),

        (Cond::Any(&["fontconfig", "stdout"]), &[("web-programming::websocket", 0.25, 0.)]),
        (Cond::Any(&["font", "ttf", "typeface", "typography", "truetype", "opentype", "svg", "tesselation", "exporter", "mesh"]),
            &[("rendering::data-formats", 1.2, 0.1), ("gui", 0.7, 0.), ("rendering::graphics-api", 0.9, 0.), ("caching", 0.8, 0.), ("no-std", 0.7, 0.), ("compression", 0.7, 0.), ("date-and-time", 0.8, 0.), ("compilers", 0.7, 0.), ("parsing", 0.6, 0.), ("filesystem", 0.7, 0.),
            ("memory-management", 0.6, 0.), ("development-tools::build-utils", 0.8, 0.), ("multimedia::audio", 0.8, 0.),
            ("games", 0.5, 0.), ("internationalization", 0.7, 0.), ("web-programming::websocket", 0.25, 0.)]),
        (Cond::Any(&["loading", "loader", "algorithm", "gui", "git"]), &[("rendering::data-formats", 0.2, 0.), ("memory-management", 0.8, 0.)]),
        (Cond::Any(&["parsing", "game", "piston", "ascii"]), &[("rendering::data-formats", 0.7, 0.), ("data-structures", 0.9, 0.)]),
        (Cond::All(&["3d", "format"]), &[("rendering::data-formats", 1.3, 0.3), ("value-formatting", 0.5, 0.), ("parsing", 0.5, 0.), ("date-and-time", 0.5, 0.), ("filesystem", 0.7, 0.), ("development-tools::ffi", 0.8, 0.)]),
        (Cond::Any(&["3d-model", "gltf"]), &[("rendering::data-formats", 1.3, 0.3), ("rendering", 1.2, 0.), ("config", 0.9, 0.), ("caching", 0.75, 0.), ("filesystem", 0.7, 0.), ("development-tools::ffi", 0.8, 0.)]),
        (Cond::Any(&["2d", "3d", "sprite", "sprite3d", "sprite2d"]), &[("rendering::graphics-api", 1.11, 0.), ("data-structures", 1.1, 0.), ("date-and-time", 0.6, 0.), ("rendering::data-formats", 1.2, 0.), ("rendering", 1.1, 0.), ("games", 0.8, 0.), ("multimedia::audio", 0.8, 0.), ("rendering::graphics-api", 1.1, 0.)]),
        (Cond::NotAny(&["no-std", "no_std", "nostd", "hardware", "embedded"]), &[("no-std", 0.8, 0.)]),
        (Cond::Any(&["discord", "telegram", "ostatus", "social-networking", "twitch"]), &[("web-programming", 1.1, 0.1), ("no-std", 0.8, 0.), ("encoding", 0.9, 0.), ("compilers", 0.8, 0.), ("parser-implementations", 0.8, 0.), ("accessibility", 0.8, 0.),
            ("asynchronous", 0.8, 0.), ("parsing", 0.6, 0.), ("web-programming::websocket", 0.7, 0.)]),
        (Cond::All(&["neural", "network"]), &[("network-programming", 0.7, 0.)]),
        (Cond::Any(&["cybersecurity"]), &[("parsing", 0.1, 0.)]),
        (Cond::Any(&["reserved"]), &[("email", 0.1, 0.)]),
];

/// Based on the set of keywords, adjust relevance of given categories
///
/// Returns (weight, slug)
#[must_use] pub fn adjusted_relevance(mut candidates: HashMap<SmolStr, f64>, keywords: &HashSet<&str>, min_category_match_threshold: f64, had_explicit_categories: bool) -> Vec<(f64, SmolStr)> {
    // everyone gets this wrong…
    if let Some(val) = candidates.get("parsing").copied() {
        if !candidates.contains_key("parser-implementations") && !["parser-generator", "lexer", "grammar", "peg", "parser-combinator", "generator"].iter().any(|&k| keywords.contains(k)) {
            candidates.insert("parser-implementations".into(), val*0.9);
        }
    }

    for (cond, actions) in KEYWORD_CATEGORIES {
        let matched_times = match cond {
            Cond::All(reqs) => {
                assert!(reqs.len() < 5);
                if reqs.iter().all(|&k| keywords.contains(k)) {1} else {continue;}
            },
            Cond::NotAny(reqs) => {
                if !reqs.iter().any(|&k| keywords.contains(k)) {1} else {continue;}
            },
            Cond::Any(reqs) => {
                let n = reqs.iter().filter(|&&k| keywords.contains(k)).count();
                if n == 0 {
                    continue;
                }
                n
            },
        };
        trace!("matched rule {}:{} *{matched_times}",
            match cond { Cond::All(_) => "all", Cond::NotAny(_) => "not", Cond::Any(_) => "any" },
            match cond { Cond::All(r) | Cond::Any(r) => r.iter().copied().filter(|k| keywords.contains(k)).collect::<Vec<_>>().join(","), Cond::NotAny(r) => r.join("~") });

        let match_relevance = (matched_times as f64).sqrt();
        for &(slug, mul, add) in actions.iter() {
            debug_assert!(CATEGORIES.from_slug(slug).1, "{}", slug);
            debug_assert!(mul >= 1.0 || add < 0.0000001, "{}", slug);
            let entry = candidates.entry(slug.into());
            if mul <= 1. && add == 0. && matches!(entry, Entry::Vacant(_)) {
                continue;
            }
            let score = entry.or_insert(0.);
            if *score > 0. || add > 0. { trace!(" | {slug} = {score}*{mul}+{add}"); }
            *score *= mul.powf(match_relevance);
            if add > 0. || mul > 1. {
                *score += add * match_relevance + 0.000001;
            }
        }
    }
    candidates.retain(|_, v| *v > 0.0001);
    debug!("keywords {} => candidates {candidates:?}", keywords.iter().copied().collect::<Vec<_>>().join(","));

    if candidates.is_empty() {
        return Vec::new();
    }

    either_or_category(&mut candidates, "text-processing", "algorithms");
    either_or_category(&mut candidates, "algorithms", "rust-patterns");
    either_or_category(&mut candidates, "algorithms", "science");
    either_or_category(&mut candidates, "algorithms", "cryptography");
    either_or_category(&mut candidates, "science::math", "algorithms");
    either_or_category(&mut candidates, "caching", "data-structures");
    either_or_category(&mut candidates, "algorithms", "data-structures");
    either_or_category(&mut candidates, "parser-implementations", "parsing");

    if_this_then_not_that(&mut candidates, "cryptography::cryptocurrencies", "command-line-utilities");
    if_this_then_not_that(&mut candidates, "cryptography::cryptocurrencies", "algorithms");
    if_this_then_not_that(&mut candidates, "cryptography::cryptocurrencies", "wasm");
    if_this_then_not_that(&mut candidates, "cryptography::cryptocurrencies", "finance");
    if_this_then_not_that(&mut candidates, "cryptography::cryptocurrencies", "cryptography");
    if_this_then_not_that(&mut candidates, "cryptography::cryptocurrencies", "database");
    if_this_then_not_that(&mut candidates, "cryptography::cryptocurrencies", "development-tools::procedural-macro-helpers");
    if_this_then_not_that(&mut candidates, "encoding", "parsing");
    if_this_then_not_that(&mut candidates, "development-tools::procedural-macro-helpers", "development-tools");
    if_this_then_not_that(&mut candidates, "development-tools::debugging", "rust-patterns");
    if_this_then_not_that(&mut candidates, "science", "development-tools::debugging");
    if_this_then_not_that(&mut candidates, "science", "parsing");
    if_this_then_not_that(&mut candidates, "rendering", "parsing");
    if_this_then_not_that(&mut candidates, "date-and-time", "rendering");
    if_this_then_not_that(&mut candidates, "date-and-time", "rendering::engine");
    if_this_then_not_that(&mut candidates, "rendering", "data-structures");
    if_this_then_not_that(&mut candidates, "cryptography", "parsing");
    if_this_then_not_that(&mut candidates, "science", "development-tools::profiling");
    if_this_then_not_that(&mut candidates, "science::bio", "development-tools::debugging");
    if_this_then_not_that(&mut candidates, "science::bio", "development-tools");
    if_this_then_not_that(&mut candidates, "parsing", "web-programming");
    if_this_then_not_that(&mut candidates, "concurrency", "development-tools");
    if_this_then_not_that(&mut candidates, "asynchronous", "development-tools");
    if_this_then_not_that(&mut candidates, "web-programming::websocket", "network-programming");
    if_this_then_not_that(&mut candidates, "database", "network-programming");
    if_this_then_not_that(&mut candidates, "value-formatting", "algorithms");
    if_this_then_not_that(&mut candidates, "value-formatting", "data-structures");
    if_this_then_not_that(&mut candidates, "games", "science::geo");
    if_this_then_not_that(&mut candidates, "games", "parsing");
    if_this_then_not_that(&mut candidates, "games", "parser-implementations");
    if_this_then_not_that(&mut candidates, "game-development", "science::geo");
    if_this_then_not_that(&mut candidates, "gui", "rendering::engine");

    relate_subcategory_candidates(&mut candidates, &CATEGORIES.root, 0.);

    either_or_category(&mut candidates, "internationalization", "text-processing");
    either_or_category(&mut candidates, "science::geo", "multimedia::images");
    either_or_category(&mut candidates, "science::geo", "rendering::graphics-api");
    if_this_then_not_that(&mut candidates, "science::geo", "encoding");
    either_or_category(&mut candidates, "science::math", "multimedia::audio");
    either_or_category(&mut candidates, "finance", "science::math");
    either_or_category(&mut candidates, "development-tools::cargo-plugins", "development-tools::build-utils");
    either_or_category(&mut candidates, "command-line-utilities", "development-tools::cargo-plugins");
    either_or_category(&mut candidates, "command-line-utilities", "command-line-interface");
    if_this_then_not_that(&mut candidates, "finance", "command-line-utilities");
    either_or_category(&mut candidates, "development-tools", "wasm");
    either_or_category(&mut candidates, "development-tools", "parsing");
    either_or_category(&mut candidates, "development-tools::procedural-macro-helpers", "rust-patterns");
    either_or_category(&mut candidates, "embedded", "hardware-support");
    either_or_category(&mut candidates, "embedded", "no-std");
    either_or_category(&mut candidates, "hardware-support", "no-std");
    either_or_category(&mut candidates, "games", "game-development");
    either_or_category(&mut candidates, "games", "command-line-utilities");
    either_or_category(&mut candidates, "no-std", "rust-patterns");
    either_or_category(&mut candidates, "os::macos-apis", "os::unix-apis");
    either_or_category(&mut candidates, "parser-implementations", "encoding");
    either_or_category(&mut candidates, "science", "simulation");
    either_or_category(&mut candidates, "science::robotics", "embedded");
    either_or_category(&mut candidates, "science::robotics", "hardware-support");
    either_or_category(&mut candidates, "development-tools", "compilers");
    either_or_category(&mut candidates, "text-processing", "template-engine");
    either_or_category(&mut candidates, "text-processing", "internationalization");
    either_or_category(&mut candidates, "text-processing", "text-editors");
    either_or_category(&mut candidates, "text-processing", "value-formatting");
    either_or_category(&mut candidates, "text-processing", "parser-implementations");
    either_or_category(&mut candidates, "text-processing", "parsing");
    either_or_category(&mut candidates, "text-editors", "parsing");
    either_or_category(&mut candidates, "simulation", "emulators");
    either_or_category(&mut candidates, "embedded", "science::robotics");
    either_or_category(&mut candidates, "hardware-support", "science::robotics");
    either_or_category(&mut candidates, "simulation", "science::robotics");
    either_or_category(&mut candidates, "simulation", "algorithms");
    either_or_category(&mut candidates, "science::robotics", "algorithms");
    either_or_category(&mut candidates, "caching", "memory-management");
    either_or_category(&mut candidates, "config", "development-tools");
    either_or_category(&mut candidates, "internationalization", "value-formatting");
    either_or_category(&mut candidates, "encoding", "value-formatting");
    either_or_category(&mut candidates, "rendering::engine", "game-development");
    either_or_category(&mut candidates, "rendering::graphics-api", "rendering::data-formats");
    either_or_category(&mut candidates, "rendering::graphics-api", "rendering::engine");
    either_or_category(&mut candidates, "encoding", "parser-implementations");
    either_or_category(&mut candidates, "concurrency", "asynchronous");
    either_or_category(&mut candidates, "filesystem", "os");
    either_or_category(&mut candidates, "filesystem", "os::unix-apis");
    either_or_category(&mut candidates, "web-programming::http-server", "web-programming::http-client");
    either_or_category(&mut candidates, "web-programming", "network-programming");
    either_or_category(&mut candidates, "web-programming::http-client", "network-programming");
    // no-std is last resort, and cli is too generic
    if_this_then_not_that(&mut candidates, "parsing", "no-std");
    if_this_then_not_that(&mut candidates, "parser-implementations", "no-std");
    if_this_then_not_that(&mut candidates, "algorithms", "no-std");
    if_this_then_not_that(&mut candidates, "data-structures", "no-std");
    if_this_then_not_that(&mut candidates, "gui", "no-std");
    if_this_then_not_that(&mut candidates, "gui", "parsing");
    if_this_then_not_that(&mut candidates, "value-formatting", "no-std");
    if_this_then_not_that(&mut candidates, "internationalization", "no-std");
    if_this_then_not_that(&mut candidates, "encoding", "no-std");
    if_this_then_not_that(&mut candidates, "rendering::engine", "parsing");
    if_this_then_not_that(&mut candidates, "hardware-support", "no-std");
    if_this_then_not_that(&mut candidates, "embedded", "no-std");
    if_this_then_not_that(&mut candidates, "internationalization", "data-structures");
    if_this_then_not_that(&mut candidates, "email", "parsing");
    if_this_then_not_that(&mut candidates, "email", "cryptography");
    if_this_then_not_that(&mut candidates, "email", "network-programming");
    if_this_then_not_that(&mut candidates, "hardware-support", "rust-patterns");
    if_this_then_not_that(&mut candidates, "hardware-support", "science::geo");
    if_this_then_not_that(&mut candidates, "parser-implementations", "web-programming::http-server");
    if_this_then_not_that(&mut candidates, "parsing", "web-programming::http-server");
    if_this_then_not_that(&mut candidates, "embedded", "wasm");
    if_this_then_not_that(&mut candidates, "gui", "wasm");
    if_this_then_not_that(&mut candidates, "embedded", "finance");
    if_this_then_not_that(&mut candidates, "embedded", "parsing");
    if_this_then_not_that(&mut candidates, "os", "filesystem");
    if_this_then_not_that(&mut candidates, "os", "science::geo");
    if_this_then_not_that(&mut candidates, "network-programming", "algorithms");
    if_this_then_not_that(&mut candidates, "network-programming", "data-structures");
    if_this_then_not_that(&mut candidates, "network-programming", "parsing");
    if_this_then_not_that(&mut candidates, "web-programming", "parsing");
    if_this_then_not_that(&mut candidates, "multimedia", "parsing");
    if_this_then_not_that(&mut candidates, "web-programming", "algorithms");
    if_this_then_not_that(&mut candidates, "databases", "algorithms");
    if_this_then_not_that(&mut candidates, "databases", "compilers");
    if_this_then_not_that(&mut candidates, "authentication", "algorithms");
    if_this_then_not_that(&mut candidates, "concurrency", "algorithms");
    if_this_then_not_that(&mut candidates, "compilers", "algorithms");
    if_this_then_not_that(&mut candidates, "concurrency", "data-structures");
    if_this_then_not_that(&mut candidates, "memory-management", "data-structures");
    if_this_then_not_that(&mut candidates, "memory-management", "parsing");
    if_this_then_not_that(&mut candidates, "web-programming", "data-structures");
    if_this_then_not_that(&mut candidates, "science::ml", "development-tools");
    if_this_then_not_that(&mut candidates, "development-tools", "command-line-utilities");
    if_this_then_not_that(&mut candidates, "development-tools", "algorithms");
    if_this_then_not_that(&mut candidates, "development-tools", "finance");
    if_this_then_not_that(&mut candidates, "development-tools::debugging", "command-line-utilities");
    if_this_then_not_that(&mut candidates, "development-tools::testing", "command-line-utilities");
    if_this_then_not_that(&mut candidates, "development-tools::testing", "algorithms");
    if_this_then_not_that(&mut candidates, "development-tools::testing", "parsing");
    if_this_then_not_that(&mut candidates, "development-tools::profiling", "command-line-utilities");
    if_this_then_not_that(&mut candidates, "development-tools::cargo-plugins", "command-line-utilities");
    if_this_then_not_that(&mut candidates, "database-implementations", "command-line-utilities");
    if_this_then_not_that(&mut candidates, "databases", "command-line-utilities");
    either_or_category_r(&mut candidates, "database", "database-implementations", true);
    if_this_then_not_that(&mut candidates, "algorithms", "command-line-utilities");
    if_this_then_not_that(&mut candidates, "hardware-support", "algorithms");
    if_this_then_not_that(&mut candidates, "embedded", "algorithms");
    if_this_then_not_that(&mut candidates, "data-structures", "command-line-utilities");
    if_this_then_not_that(&mut candidates, "network-programming", "command-line-utilities");
    if_this_then_not_that(&mut candidates, "os", "command-line-utilities");
    if_this_then_not_that(&mut candidates, "os", "compilers");
    if_this_then_not_that(&mut candidates, "os", "parsing");
    if_this_then_not_that(&mut candidates, "multimedia", "command-line-utilities");
    if_this_then_not_that(&mut candidates, "parsing", "command-line-utilities");
    if_this_then_not_that(&mut candidates, "compilers", "parsing");
    if_this_then_not_that(&mut candidates, "asynchronous", "compilers");
    if_this_then_not_that(&mut candidates, "web-programming::http-server", "compilers");
    if_this_then_not_that(&mut candidates, "parser-implementations", "command-line-utilities");
    if_this_then_not_that(&mut candidates, "parser-implementations", "compilers");
    if_this_then_not_that(&mut candidates, "game-development", "command-line-utilities");
    if_this_then_not_that(&mut candidates, "game-development", "compilers");
    if_this_then_not_that(&mut candidates, "game-development", "parsing");
    if_this_then_not_that(&mut candidates, "encoding", "compilers");
    if_this_then_not_that(&mut candidates, "encoding", "parsing");
    if_this_then_not_that(&mut candidates, "simulation", "command-line-utilities");
    if_this_then_not_that(&mut candidates, "config", "command-line-utilities");
    if_this_then_not_that(&mut candidates, "rust-patterns", "command-line-utilities");
    if_this_then_not_that(&mut candidates, "multimedia::video", "command-line-utilities");
    if_this_then_not_that(&mut candidates, "multimedia::video", "parsing");
    if_this_then_not_that(&mut candidates, "multimedia::video", "encoding");
    if_this_then_not_that(&mut candidates, "multimedia::images", "command-line-utilities");
    if_this_then_not_that(&mut candidates, "multimedia::images", "encoding");
    if_this_then_not_that(&mut candidates, "multimedia::images", "multimedia");
    if_this_then_not_that(&mut candidates, "command-line-interface", "command-line-utilities");
    if_this_then_not_that(&mut candidates, "rendering::graphics-api", "accessibility");
    if_this_then_not_that(&mut candidates, "rust-patterns", "accessibility");
    if_this_then_not_that(&mut candidates, "web-programming::websocket", "accessibility");
    if_this_then_not_that(&mut candidates, "command-line-utilities", "accessibility");
    if_this_then_not_that(&mut candidates, "command-line-utilities", "parsing");
    if_this_then_not_that(&mut candidates, "parser-implementations", "command-line-utilities");
    if_this_then_not_that(&mut candidates, "web-programming::http-client", "accessibility");
    if_this_then_not_that(&mut candidates, "web-programming::http-client", "compilers");
    if_this_then_not_that(&mut candidates, "web-programming::http-client", "parsing");
    if_this_then_not_that(&mut candidates, "config", "compilers");
    if_this_then_not_that(&mut candidates, "config", "parsing");
    if_this_then_not_that(&mut candidates, "development-tools::procedural-macro-helpers", "accessibility");
    either_or_category_r(&mut candidates, "parsing", "parser-implementations", true);

    // slightly nudge towards specific, leaf categories over root generic ones
    for (slug, w) in &mut candidates {
        *w *= CATEGORIES.from_slug(slug).0.last().map_or(1., |c| f64::from(c.preference));
    }

    let max_score = candidates.values().copied()
        .max_by(f64::total_cmp)
        .unwrap_or(0.);

    // if guessing categories, pick more than one only if they're close to a tie
    // if it had multiple categories, basically just sort them
    let min_category_match_threshold = min_category_match_threshold.max(max_score * if had_explicit_categories { 0.4 } else { 0.913 });

    let mut already_has = HashSet::<&str>::with_capacity(candidates.len());

    let mut res: Vec<_> = candidates.into_iter()
        .filter(|&(_, v)| v >= min_category_match_threshold)
        .filter(|(k, _)| {
            // avoid having "cat::subcat" and "cat" together
            if already_has.contains(&**k) {
                return false;
            }
            let (slugs, matched) = CATEGORIES.from_slug(k);
            debug_assert!(matched);
            if !matched { return false; }
            if slugs.len() > 1 {
                already_has.extend(slugs[..slugs.len() - 1].iter().map(|c| c.slug.as_str()));
            }
            true
        })
        .map(|(k, v)| (v, k))
        .collect();
    sort_top_n_unstable_by(&mut res, 5, |a, b| b.0.total_cmp(&a.0));

    // don't want cryptocurrencies to spill to other places
    let max_num_categories = if res.first().map_or(false, |(_, slug)| &**slug == "cryptography::cryptocurrencies") {1}
        else if had_explicit_categories {3} else {2};
    res.truncate(max_num_categories);
    res
}

/// Keyword<>category matching works on each category independently, but the
/// categories are related.
/// propagate half of parent category's score to children,
/// and 1/6th of best child category to parent cat.
fn relate_subcategory_candidates(candidates: &mut HashMap<SmolStr, f64>, categories: &BTreeMap<String, crate::Category>, add_to_children: f64) -> f64 {
    let mut max = 0.;
    for cat in categories.values() {
        let propagage = match candidates.get_mut(cat.slug.as_str()) {
            Some(score) => {
                if *score > max {
                    max = *score;
                }
                *score += score.min(add_to_children); // at most double the existing score to avoid creating false child categories
                *score / 2. // propagate half down
            },
            None => 0.,
        };
        let max_of_children = relate_subcategory_candidates(candidates, &cat.sub, propagage);
        if let Some(score) = candidates.get_mut(cat.slug.as_str()) {
            *score += max_of_children / 6.;
        }
    }
    max
}

fn if_this_then_not_that(candidates: &mut HashMap<SmolStr, f64>, if_this: &str, not_that: &str) {
    if let Some(has) = candidates.get(if_this).copied() {
        if let Some(shouldnt) = candidates.get_mut(not_that) {
            *shouldnt -= has.min(*shouldnt * 0.5);
        }
    }
}

/// Sometimes a crate half-fits in two categories, and this can push threshold to one of item.
/// (not for hierarchy of crates)
fn either_or_category(candidates: &mut HashMap<SmolStr, f64>, a_slug: &str, b_slug: &str) {
    either_or_category_r(candidates, a_slug, b_slug, false);
}

fn either_or_category_r(candidates: &mut HashMap<SmolStr, f64>, a_slug: &str, b_slug: &str, remove: bool) {
    if let (Some(a), Some(b)) = (candidates.get(a_slug).copied(), candidates.get(b_slug).copied()) {
        if a > b {
            *candidates.get_mut(a_slug).unwrap() += b/2.;
            if remove {
                candidates.remove(b_slug);
            } else {
                *candidates.get_mut(b_slug).unwrap() *= 0.45;
            }
        } else {
            *candidates.get_mut(b_slug).unwrap() += a/2.;
            if remove {
                candidates.remove(a_slug);
            } else {
                *candidates.get_mut(a_slug).unwrap() *= 0.45;
            }
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub(crate) enum Cond {
    Any(&'static [&'static str]),
    All(&'static [&'static str]),
    NotAny(&'static [&'static str]),
}
