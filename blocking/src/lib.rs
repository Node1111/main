use ahash::HashMap;
use std::cmp::Reverse;
use std::collections::VecDeque;
use std::eprintln;
use std::fmt::Display;
use std::future::Future;
use std::io::stdout;
use std::io::Write;
use std::sync::{Mutex, Arc, Weak, MutexGuard};
use std::time::{Duration, Instant};

type LockMap = HashMap<String, VecDeque<Weak<Instant>>>;
static ACTIVE_WORK: Mutex<Option<LockMap>> = Mutex::new(None);


pub fn block_in_place<F, R>(label: impl Display, cb: F) -> R where F: FnOnce() -> R {
    let mut label = label.to_string();
    label.push('!');
    let _guard = set(label);
    tokio::task::block_in_place(cb)
}

pub fn watch<F, R>(label: impl Display, cb: F) -> R where F: FnOnce() -> R {
    let label = label.to_string();
    let _guard = set(label);
    cb()
}

pub fn closure<F, R>(label: impl Display, cb: F) -> impl FnOnce() -> R
    where F: FnOnce() -> R {
    let label = label.to_string();
    move || {
        let _guard = set(label);
        cb()
    }
}

pub async fn awatch<F, R>(label: impl Display, cb: F) -> R where F: Future<Output=R> {
    let mut label = label.to_string();
    label.push('@');
    let _guard = set(label);
    cb.await
}

fn lock_it() -> MutexGuard<'static, Option<LockMap>> {
    ACTIVE_WORK.lock().unwrap_or_else(|_| {
        eprintln!("block: poison");
        std::process::exit(10)
    })
}

fn set(key: String) -> impl Drop {
    let maybe_map = &mut *lock_it();
    let map = maybe_map.get_or_insert_with(|| {
        spawn_watchdog();
        HashMap::default()
    });

    let a = Arc::new(Instant::now());
    let v = map.entry(key).or_default();

    // quick partial cleanup while we're locked
    while v.front().map_or(false, |old| Weak::strong_count(old) == 0) {
        v.pop_front();
    }
    v.push_back(Arc::downgrade(&a));
    a
}

pub fn dump_all() {
    let mut map = lock_it();
    let Some(map) = map.as_mut() else { return };
    let now = Instant::now();
    for (key, v) in map {
        for w in v {
            let Some(a) = w.upgrade() else { continue };
            let elapsed = now.duration_since(*a);
            eprintln!("blocking: {key} has been running for {}ms", elapsed.as_millis() as u32);
        }
    }
}

fn spawn_watchdog() {
    std::thread::Builder::new().name("block_watch".to_string()).spawn(|| {
        loop {
            std::thread::sleep(Duration::from_secs(3));
            {
                let mut map = lock_it();
                let map = map.as_mut().unwrap();
                let now = Instant::now();
                let min_duration = Duration::from_secs(2);

                let map_len = map.len();
                if map_len > 2000 {
                    eprintln!("blocking: {map_len} tasks running!");
                }
                let mut printed_any = false;
                let mut worst = 0;
                let mut worst_name = String::new();
                let mut to_print = Vec::new();
                map.retain(|key, timers| {
                    let timers_len = timers.len();
                    timers.retain_mut(|w| {
                        let Some(a) = w.upgrade() else { return false };

                        let elapsed = now.duration_since(*a);
                        if elapsed > min_duration {
                            printed_any = true;
                            let millis = elapsed.as_millis() as u32;
                            if millis > 5000 && millis > worst {
                                worst = millis;
                                worst_name = key.clone();
                            }

                            let text = if millis > 10000 {
                                format!("blocking: {key} HAS BEEN RUNNING FOR {}s // {map_len}*{timers_len}\n", millis/1000)
                            } else {
                                format!("blocking: {key} has been running for {millis}ms // {map_len}*{timers_len}\n")
                            };
                            to_print.push((millis, text));
                        }
                        true
                    });
                    if timers.len() > 100 {
                        printed_any = true;
                        eprintln!("blocking: {key} has {} concurrent instances", timers.len());
                    }
                    !timers.is_empty()
                });

                if !to_print.is_empty() {
                    to_print.sort_by_key(|a| Reverse(a.0));
                    let mut out = stdout().lock();
                    let _ = out.write_fmt(format_args!("---- blocking: ({})\n", to_print.len()));
                    for (_, t) in &to_print {
                        let _ = out.write_all(t.as_bytes());
                    }
                }

                if map.capacity() > 1000 && map.len() < 500 {
                    map.shrink_to_fit();
                }
                if printed_any && worst > 5000 {
                    eprintln!("---- there were blocking tasks up to {worst}ms ({worst_name})\n\n");
                }
            }
        }
    }).unwrap();
}
